package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.model.DVNTNote;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.CreateNoteActivity.IntentBuilder;
import com.deviantart.android.damobile.activity.HomeActivity.BottomBarButton;
import com.deviantart.android.damobile.adapter.NotesPagerAdapter;
import com.deviantart.android.damobile.adapter.StatefulPagerAdapter;
import com.deviantart.android.damobile.util.BusStation.OnMCItemChangeEventBase;
import com.deviantart.android.damobile.util.BusStation.OnNoteItemChangeEvent;
import com.deviantart.android.damobile.util.BusStation.OnNotesItemMarkUpdated;
import com.deviantart.android.damobile.util.BusStation.OnNotesRefreshAll;
import com.deviantart.android.damobile.util.notes.Mark;
import com.deviantart.android.damobile.util.notes.NotesDefaultPage;
import com.deviantart.android.damobile.util.notes.NotesFolders;
import com.deviantart.android.damobile.util.notes.NotesFoldersLoader;
import com.deviantart.android.damobile.util.notes.NotesItemData;
import com.deviantart.android.damobile.util.notes.NotesMarkUtils;
import com.deviantart.android.damobile.util.notes.NotesPage;
import com.deviantart.android.damobile.util.opt.ItemDeleteHelperBase;
import com.deviantart.android.damobile.util.opt.ItemDeleteType;
import com.deviantart.android.damobile.util.opt.ItemDeleteUtils;
import com.deviantart.android.damobile.util.opt.NoteItemDeleteHelper;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.util.tracking.pacaya.MobileLava;
import com.deviantart.android.damobile.util.tracking.pacaya.UxTopicEventCreator;
import com.deviantart.android.damobile.view.notes.NotesListAdapter;
import com.deviantart.android.damobile.view.notes.NotesListFrame;
import com.deviantart.android.damobile.view.opt.MCListAdapterBase;
import com.deviantart.android.damobile.view.opt.MCListFrameBase;
import com.deviantart.android.damobile.view.opt.MCListRecyclerView;
import com.deviantart.android.damobile.view.viewpageindicator.DATabIndicator;
import com.deviantart.pacaya.Pacaya;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class NotesFragment
  extends MCBaseFragment
{
  public static HashSet<String> c = new HashSet();
  
  public NotesFragment() {}
  
  private void a(NotesItemData paramNotesItemData, NotesPage paramNotesPage, Mark paramMark, boolean paramBoolean)
  {
    NotesListFrame localNotesListFrame = (NotesListFrame)pager.findViewWithTag(paramNotesPage.getString());
    if (localNotesListFrame == null)
    {
      NotesMarkUtils.a(paramNotesPage, paramNotesItemData, paramMark, paramBoolean);
      return;
    }
    paramNotesPage = (NotesListAdapter)localNotesListFrame.getRecyclerView().getAdapter();
    if (paramNotesPage != null)
    {
      paramNotesPage.b(paramNotesItemData, paramMark, paramBoolean);
      if ((paramBoolean) && (paramNotesPage.e() == 0))
      {
        localNotesListFrame.b();
        return;
      }
      if ((paramBoolean) && (paramMark.e() > 0) && (paramNotesPage.e() > 0))
      {
        if (paramNotesPage.e() == 1) {
          localNotesListFrame.getRecyclerView().smoothScrollToPosition(0);
        }
        localNotesListFrame.c();
      }
    }
  }
  
  private void b(NotesItemData paramNotesItemData, NotesPage paramNotesPage, boolean paramBoolean)
  {
    NotesListFrame localNotesListFrame = (NotesListFrame)pager.findViewWithTag(paramNotesPage.getString());
    NoteItemDeleteHelper localNoteItemDeleteHelper = (NoteItemDeleteHelper)ItemDeleteUtils.a(ItemDeleteType.c);
    if (localNotesListFrame != null)
    {
      localNoteItemDeleteHelper.a(localNotesListFrame, paramNotesItemData, paramBoolean);
      return;
    }
    if (paramBoolean)
    {
      localNoteItemDeleteHelper.a(paramNotesItemData, paramNotesPage);
      return;
    }
    localNoteItemDeleteHelper.get(paramNotesItemData, paramNotesPage);
  }
  
  public static NotesFragment getNextTimePosition()
  {
    return new NotesFragment();
  }
  
  public void b(BusStation.OnNoteItemChangeEvent paramOnNoteItemChangeEvent)
  {
    NotesItemData localNotesItemData = (NotesItemData)paramOnNoteItemChangeEvent.c();
    boolean bool = paramOnNoteItemChangeEvent.b();
    paramOnNoteItemChangeEvent = NoteItemDeleteHelper.a.iterator();
    while (paramOnNoteItemChangeEvent.hasNext()) {
      b(localNotesItemData, (NotesPage)paramOnNoteItemChangeEvent.next(), bool);
    }
    if (bool) {}
    for (int i = 1;; i = -1)
    {
      if (localNotesItemData.getContext().getIsUnread().booleanValue()) {
        NotesFolders.a("2E23AB5F-02FC-0EAD-F27A-BD5A251529FE", i);
      }
      if (localNotesItemData.getContext().getIsStarred().booleanValue()) {
        NotesFolders.a("BC7EADE4-C74C-591A-A8F8-09FD5A57100E", i);
      }
      indicator.notifyDataSetChanged();
      return;
    }
  }
  
  public void b(BusStation.OnNotesItemMarkUpdated paramOnNotesItemMarkUpdated)
  {
    if (pager == null) {
      return;
    }
    NotesItemData localNotesItemData = paramOnNotesItemMarkUpdated.a();
    paramOnNotesItemMarkUpdated = paramOnNotesItemMarkUpdated.e();
    a(localNotesItemData, NotesDefaultPage.o, paramOnNotesItemMarkUpdated, false);
    switch (NotesFragment.1.o[paramOnNotesItemMarkUpdated.ordinal()])
    {
    default: 
      break;
    }
    for (;;)
    {
      indicator.a();
      return;
      a(localNotesItemData, NotesDefaultPage.d, paramOnNotesItemMarkUpdated, false);
      a(localNotesItemData, NotesDefaultPage.l, paramOnNotesItemMarkUpdated, false);
      a(localNotesItemData, NotesDefaultPage.b, paramOnNotesItemMarkUpdated, true);
      continue;
      a(localNotesItemData, NotesDefaultPage.l, paramOnNotesItemMarkUpdated, true);
      a(localNotesItemData, NotesDefaultPage.b, paramOnNotesItemMarkUpdated, false);
    }
  }
  
  public void b(BusStation.OnNotesRefreshAll paramOnNotesRefreshAll)
  {
    if (pager != null)
    {
      if (DVNTContextUtils.isContextDead(getActivity())) {
        return;
      }
      NotesFolders.a().c(getActivity());
      int j = pager.getChildCount();
      int i = 0;
      while (i < j)
      {
        ((NotesListFrame)pager.getChildAt(i)).visitAttribute();
        i += 1;
      }
    }
  }
  
  protected HomeActivity.BottomBarButton c()
  {
    return HomeActivity.BottomBarButton.q;
  }
  
  protected void c(int paramInt)
  {
    NotesDefaultPage localNotesDefaultPage = NotesDefaultPage.values()[paramInt];
    MobileLava.e().setText(new UxTopicEventCreator().get("notes").getString(localNotesDefaultPage.get(getActivity()).toLowerCase()).b());
  }
  
  protected boolean delete4()
  {
    return false;
  }
  
  Set get()
  {
    return c;
  }
  
  protected StatefulPagerAdapter getParentFile(int paramInt)
  {
    return new NotesPagerAdapter(paramInt);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    NotesFolders.a().c(getActivity());
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (DVNTAbstractAsyncAPI.isUserSession(getActivity()))
    {
      paramMenuInflater.inflate(2131820551, paramMenu);
      a(paramMenu, paramMenuInflater);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      break;
    }
    for (;;)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      shareImage();
    }
  }
  
  protected int setupButtons()
  {
    return 2130968711;
  }
  
  public void shareImage()
  {
    DVNTAbstractAsyncAPI.cancelAllRequests();
    TrackerUtil.get(getActivity(), EventKeys.Category.d, "create_note", "notes_view");
    startActivityForResult(new CreateNoteActivity.IntentBuilder().a(getActivity()), 110);
    getActivity().overridePendingTransition(2131034146, 2131034113);
  }
  
  public void visitTableSwitchInsn()
  {
    if (indicator == null) {
      return;
    }
    indicator.a();
  }
}
