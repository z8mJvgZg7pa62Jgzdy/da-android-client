package com.deviantart.android.damobile.util;

import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.package_14.model.DVNTWatchRecommendationItem;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StaticStreamLoader;

public class WatchReco
  extends DefaultDeviationTorpedoPreviewItem
{
  private int id;
  private DVNTWatchRecommendationItem u;
  private WatchRecoUpdateListener v;
  
  public WatchReco(DVNTWatchRecommendationItem paramDVNTWatchRecommendationItem, WatchRecoUpdateListener paramWatchRecoUpdateListener, int paramInt)
  {
    u = paramDVNTWatchRecommendationItem;
    v = paramWatchRecoUpdateListener;
    id = paramInt;
  }
  
  public Stream c()
  {
    StaticStreamLoader localStaticStreamLoader = new StaticStreamLoader("watch_recommendations_deviations_" + u.getUser().getUserName());
    localStaticStreamLoader.a(u.getDeviations());
    return StreamCacher.a(localStaticStreamLoader);
  }
  
  public DVNTWatchRecommendationItem getLogin()
  {
    return u;
  }
  
  public String getPropertyName()
  {
    return "watch_recommendation";
  }
  
  public WatchRecoUpdateListener getValue()
  {
    return v;
  }
  
  public int id()
  {
    return id;
  }
}
