package com.deviantart.android.damobile.util.notifications;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessage;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.facebook.drawee.view.SimpleDraweeView;

public class NotificationsItemViewHolder
  extends RecyclerView.ViewHolder
{
  @Bind({2131689769})
  SimpleDraweeView avatarView;
  @Bind({2131689772})
  TextView dateView;
  @Bind({2131689991})
  LinearLayout iconContainer;
  @Bind({2131689552})
  ImageView iconView;
  private NotificationItemData l;
  @Bind({2131689938})
  TextView textView;
  
  public NotificationsItemViewHolder(View paramView)
  {
    super(paramView);
    ButterKnife.bind(this, paramView);
  }
  
  void onClickAvatar(View paramView)
  {
    paramView = paramView.getContext();
    if ((l.b() == NotificationItemType.B) || (l.b() == NotificationItemType.C)) {}
    for (int i = 1;; i = 0)
    {
      TrackerUtil.a((Activity)paramView, EventKeys.Category.i, "TapUserAvatar");
      if (i == 0) {
        break;
      }
      NotificationItemClick.c(paramView, l);
      return;
    }
    UserUtils.c((Activity)paramView, l.a().getOriginator().getUserName());
  }
  
  void onClickItem(View paramView)
  {
    NotificationItemClick.c(paramView.getContext(), l);
  }
  
  public void onCreateView(NotificationItemData paramNotificationItemData)
  {
    l = paramNotificationItemData;
    Context localContext = itemView.getContext();
    NotificationItemType localNotificationItemType = paramNotificationItemData.b();
    DVNTFeedbackMessage localDVNTFeedbackMessage = paramNotificationItemData.a();
    int i;
    if ((localNotificationItemType == NotificationItemType.B) || (localNotificationItemType == NotificationItemType.C))
    {
      i = 1;
      if (i == 0) {
        break label152;
      }
      Uri localUri = Uri.parse("res:///2130837644");
      avatarView.setImageURI(localUri);
      label63:
      if (!localNotificationItemType.b()) {
        break label173;
      }
      textView.setPadding(0, 0, (int)localContext.getResources().getDimension(2131361956), 0);
      iconContainer.setBackgroundResource(2131558527);
      iconContainer.setGravity(17);
      iconView.setImageResource(2130837793);
    }
    for (;;)
    {
      textView.setText(NotificationItemText.a(localContext, paramNotificationItemData));
      dateView.setText(DAFormatUtils.get(localContext, localDVNTFeedbackMessage.getTimestamp()));
      return;
      i = 0;
      break;
      label152:
      ImageUtils.isEmpty(avatarView, Uri.parse(localDVNTFeedbackMessage.getOriginator().getUserIconURL()));
      break label63;
      label173:
      textView.setPadding(0, 0, 0, 0);
      iconContainer.setBackgroundResource(0);
      iconContainer.setGravity(48);
      iconView.setImageResource(localNotificationItemType.c());
    }
  }
  
  boolean onLongClickItem(View paramView)
  {
    return NotificationItemDeleteClick.a(paramView.getContext(), l);
  }
}
