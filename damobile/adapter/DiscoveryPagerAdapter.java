package com.deviantart.android.damobile.adapter;

import android.app.Activity;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.view.ViewPager;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.damobile.util.SharedPreferenceUtil;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.discovery.pages.DiscoveryPage;
import com.deviantart.android.damobile.util.mainpager.DAMainPagerAdapter;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DiscoveryPagerAdapter
  extends DAMainPagerAdapter<DiscoveryPage>
{
  protected int c = -1;
  protected int d = 0;
  
  public DiscoveryPagerAdapter()
  {
    super(new ArrayList());
  }
  
  private int accept(Activity paramActivity)
  {
    Object localObject = SharedPreferenceUtil.getString(paramActivity, "recent_username", null);
    int i = SharedPreferenceUtil.getKey(paramActivity, "landing_screen", -1);
    if (localObject != null)
    {
      localObject = paramActivity.getSharedPreferences((String)localObject, 0);
      int j = ((SharedPreferences)localObject).getInt("landing_screen", -1);
      if (j != -1) {
        return j;
      }
      if (i != -1)
      {
        ((SharedPreferences)localObject).edit().putInt("landing_screen", i).apply();
        SharedPreferenceUtil.putString(paramActivity, "landing_screen", -1);
        return i;
      }
      return 1;
    }
    if (i != -1)
    {
      if (UserUtils.a == null)
      {
        com.deviantart.android.damobile.fragment.DABaseFragment.d = true;
        return i;
      }
      SharedPreferenceUtil.putString(paramActivity, "recent_username", UserUtils.a);
      paramActivity.getSharedPreferences(UserUtils.a, 0).edit().putInt("landing_screen", i).apply();
      SharedPreferenceUtil.putString(paramActivity, "landing_screen", -1);
      return i;
    }
    return 1;
  }
  
  private int register(Activity paramActivity, int paramInt)
  {
    if (paramInt == 0) {
      return paramActivity.getPreferences(0).getInt("last_visited_screen", 1);
    }
    if (paramInt - 1 < a.size()) {
      return paramInt - 1;
    }
    return 1;
  }
  
  public void a(Activity paramActivity, int paramInt)
  {
    TrackerUtil.run(paramActivity, ((DiscoveryPage)a.get(paramInt)).getDescriptor());
  }
  
  public void a(DiscoveryPage paramDiscoveryPage)
  {
    a.add(paramDiscoveryPage);
    if (paramDiscoveryPage.d()) {
      c = (a.size() - 1);
    }
  }
  
  public void a(String paramString)
  {
    Iterator localIterator = a.iterator();
    while (localIterator.hasNext()) {
      ((DiscoveryPage)localIterator.next()).f(paramString);
    }
  }
  
  public void b(DiscoveryPage paramDiscoveryPage, boolean paramBoolean)
  {
    paramDiscoveryPage.b(d);
    a(paramDiscoveryPage);
  }
  
  public void b(String paramString)
  {
    Iterator localIterator = a.iterator();
    while (localIterator.hasNext()) {
      ((DiscoveryPage)localIterator.next()).b(paramString);
    }
  }
  
  public void d(int paramInt)
  {
    d = paramInt;
  }
  
  public int get(Activity paramActivity)
  {
    if (c != -1) {
      return c;
    }
    if (!DVNTAbstractAsyncAPI.isUserSession(paramActivity)) {
      return 0;
    }
    return register(paramActivity, accept(paramActivity));
  }
  
  public CharSequence getPageTitle(int paramInt)
  {
    return super.getPageTitle(paramInt).toString().toUpperCase();
  }
  
  public Integer read(String paramString)
  {
    int i = 0;
    while (i < a.size())
    {
      if (((DiscoveryPage)a.get(i)).read().equals(paramString)) {
        return Integer.valueOf(i);
      }
      i += 1;
    }
    return null;
  }
  
  public void setViewPager(ViewPager paramViewPager)
  {
    int i = paramViewPager.getCurrentItem();
    paramViewPager.setAdapter(null);
    paramViewPager.setAdapter(this);
    paramViewPager.setCurrentItem(i);
  }
}
