package com.deviantart.android.damobile.adapter;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.ViewGroup;

public abstract class HeaderFooterRecyclerViewAdapter
  extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
  private int a;
  private int b;
  private int c;
  
  public HeaderFooterRecyclerViewAdapter() {}
  
  private int a(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= 1000)) {
      throw new IllegalStateException("viewType must be between 0 and 1000");
    }
    return paramInt;
  }
  
  public final RecyclerView.ViewHolder a(ViewGroup paramViewGroup, int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < 1000)) {
      return onCreateView(paramViewGroup, paramInt + 0);
    }
    if ((paramInt >= 1000) && (paramInt < 2000)) {
      return c(paramViewGroup, paramInt - 1000);
    }
    if ((paramInt >= 2000) && (paramInt < 3000)) {
      return b(paramViewGroup, paramInt - 2000);
    }
    throw new IllegalStateException();
  }
  
  public final void a(int paramInt1, int paramInt2)
  {
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 > b))
    {
      Log.e("HeaderFooterAdapter", "The given range [" + paramInt1 + " - " + (paramInt1 + paramInt2 - 1) + "] is not within the position bounds for footer items [0 - " + (b - 1) + "].");
      c();
      return;
    }
    notifyItemRangeInserted(c + paramInt1 + a, paramInt2);
  }
  
  protected abstract void a(RecyclerView.ViewHolder paramViewHolder, int paramInt);
  
  protected abstract int add();
  
  public final void add(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= a))
    {
      Log.e("HeaderFooterAdapter", "The given position " + paramInt + " is not within the position bounds for content items [0 - " + (a - 1) + "].");
      c();
      return;
    }
    notifyItemInserted(c + paramInt);
  }
  
  public final void add(int paramInt1, int paramInt2)
  {
    if ((paramInt2 <= 0) || (paramInt1 < 0))
    {
      Log.d("HeaderFooterAdapter", "invalid input values: [itemCount: " + paramInt2 + "], [positionStart: " + paramInt1 + "]");
      return;
    }
    a = e();
    if (a <= 0)
    {
      Log.d("HeaderFooterAdapter", "invalid content item count: [contentItemCount: " + a + "]");
      return;
    }
    int i = paramInt2;
    if (paramInt1 + paramInt2 > a)
    {
      Log.d("HeaderFooterAdapter", "The given range [" + paramInt1 + " - " + (paramInt1 + paramInt2 - 1) + "] is not within the position bounds for content items [0 - " + (a - 1) + "]");
      i = a - paramInt1;
    }
    notifyItemRangeInserted(c + paramInt1, i);
  }
  
  public final void append(int paramInt)
  {
    int i = size();
    if ((paramInt < 0) || (paramInt >= i))
    {
      Log.e("HeaderFooterAdapter", "The given position " + paramInt + " is not within the position bounds for header items [0 - " + (i - 1) + "].");
      c();
      return;
    }
    notifyItemChanged(paramInt);
  }
  
  public final void append(int paramInt1, int paramInt2)
  {
    int i = size();
    int j = e();
    if ((paramInt1 < 0) || (paramInt2 < 0) || (paramInt1 + paramInt2 > j))
    {
      Log.e("HeaderFooterAdapter", "The given range [" + paramInt1 + " - " + (paramInt1 + paramInt2 - 1) + "] is not within the position bounds for content items [0 - " + (j - 1) + "].");
      c();
      return;
    }
    notifyItemRangeChanged(i + paramInt1, paramInt2);
  }
  
  protected int b(int paramInt)
  {
    return 0;
  }
  
  protected abstract RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt);
  
  protected abstract void b(RecyclerView.ViewHolder paramViewHolder, int paramInt);
  
  protected abstract RecyclerView.ViewHolder c(ViewGroup paramViewGroup, int paramInt);
  
  public final void c(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= b))
    {
      Log.e("HeaderFooterAdapter", "The given position " + paramInt + " is not within the position bounds for footer items [0 - " + (b - 1) + "].");
      c();
      return;
    }
    notifyItemInserted(c + paramInt + a);
  }
  
  protected abstract void c(RecyclerView.ViewHolder paramViewHolder, int paramInt);
  
  public void clear(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= c))
    {
      Log.e("HeaderFooterAdapter", "The given position " + paramInt + " is not within the position bounds for header items [0 - " + (c - 1) + "].");
      c();
      return;
    }
    notifyItemInserted(paramInt);
  }
  
  public final void close(int paramInt)
  {
    int i = size();
    int j = e();
    if ((paramInt < 0) || (paramInt >= j))
    {
      Log.e("HeaderFooterAdapter", "The given position " + paramInt + " is not within the position bounds for content items [0 - " + (j - 1) + "].");
      c();
      return;
    }
    notifyItemChanged(i + paramInt);
  }
  
  public final void d(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= a))
    {
      Log.e("HeaderFooterAdapter", "The given position " + paramInt + " is not within the position bounds for content items [0 - " + (a - 1) + "].");
      c();
      return;
    }
    notifyItemRemoved(c + paramInt);
  }
  
  protected abstract int e();
  
  public final void e(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= b))
    {
      Log.e("HeaderFooterAdapter", "The given position " + paramInt + " is not within the position bounds for footer items [0 - " + (b - 1) + "].");
      c();
      return;
    }
    notifyItemRemoved(c + paramInt + a);
  }
  
  protected int getIntValue(int paramInt)
  {
    return 0;
  }
  
  public final int getItemCount()
  {
    c = size();
    a = e();
    b = add();
    return c + a + b;
  }
  
  public final int getItemViewType(int paramInt)
  {
    if ((c > 0) && (paramInt < c)) {
      return a(getIntValue(paramInt)) + 0;
    }
    if ((a > 0) && (paramInt - c < a)) {
      return a(b(paramInt - c)) + 2000;
    }
    return a(getShortcut(paramInt - c - a)) + 1000;
  }
  
  protected int getShortcut(int paramInt)
  {
    return 0;
  }
  
  public final void onBindViewHolder(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    if ((c > 0) && (paramInt < c))
    {
      Log.d("HeaderFooterAdapter", "HFRecyclerViewAdapter - onBindViewHolder - onBindHeaderItemViewHolder");
      c(paramViewHolder, paramInt);
      return;
    }
    if ((a > 0) && (paramInt - c < a))
    {
      Log.d("HeaderFooterAdapter", "HFRecyclerViewAdapter - onBindViewHolder - onBindContentItemViewHolder");
      b(paramViewHolder, paramInt - c);
      return;
    }
    Log.d("HeaderFooterAdapter", "HFRecyclerViewAdapter - onBindViewHolder - onBindFooterItemViewHolder");
    a(paramViewHolder, paramInt - c - a);
  }
  
  protected abstract RecyclerView.ViewHolder onCreateView(ViewGroup paramViewGroup, int paramInt);
  
  public final void remove(int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= c))
    {
      Log.e("HeaderFooterAdapter", "The given position " + paramInt + " is not within the position bounds for header items [0 - " + (c - 1) + "].");
      c();
      return;
    }
    notifyItemRemoved(paramInt);
  }
  
  public final void replace(int paramInt)
  {
    int i = size();
    int j = e();
    int k = add();
    if ((paramInt < 0) || (paramInt >= k))
    {
      Log.e("HeaderFooterAdapter", "The given position " + paramInt + " is not within the position bounds for footer items [0 - " + (k - 1) + "].");
      c();
      return;
    }
    notifyItemChanged(i + paramInt + j);
  }
  
  protected abstract int size();
}
