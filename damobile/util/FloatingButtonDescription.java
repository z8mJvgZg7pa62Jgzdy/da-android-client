package com.deviantart.android.damobile.util;

import android.view.View.OnClickListener;
import java.io.Serializable;

public class FloatingButtonDescription
  implements Serializable
{
  private String d;
  private View.OnClickListener instance;
  
  public FloatingButtonDescription(String paramString, View.OnClickListener paramOnClickListener)
  {
    d = paramString;
    instance = paramOnClickListener;
  }
  
  public String e()
  {
    return d;
  }
  
  public View.OnClickListener lambdaFactory$()
  {
    return instance;
  }
}
