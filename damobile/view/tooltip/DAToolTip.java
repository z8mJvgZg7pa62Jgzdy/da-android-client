package com.deviantart.android.damobile.view.tooltip;

import android.view.View;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTip.AnimationType;

public class DAToolTip
  extends ToolTip
{
  private boolean l = true;
  
  public DAToolTip() {}
  
  public DAToolTip a(boolean paramBoolean)
  {
    l = paramBoolean;
    return this;
  }
  
  public boolean getValue()
  {
    return l;
  }
  
  public DAToolTip remove(int paramInt)
  {
    super.removeElement(paramInt);
    return this;
  }
  
  public DAToolTip remove(View paramView)
  {
    super.removeElement(paramView);
    return this;
  }
  
  public DAToolTip remove(ToolTip.AnimationType paramAnimationType)
  {
    super.removeElement(paramAnimationType);
    return this;
  }
}
