package com.deviantart.android.android.package_14;

import android.content.Context;
import com.deviantart.android.android.package_14.model.DVNTEnrichedToken;
import com.deviantart.android.android.package_14.model.DVNTFeedbackType;
import com.deviantart.android.android.package_14.model.DVNTNote.Mark;
import com.deviantart.android.android.package_14.network.request.DVNTAccountValidationRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTCategoryTreeRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTCreateAccountRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTDeleteDeviationRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTDeleteNotesRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTErrorUtilRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFeedbackAllMessagesRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFeedbackMentionsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFeedbackMessagesForStackRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFeedbackStackedMessagesRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFeedbackUnstackedMessagesRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTGetPushNotificationsSettingsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTLoginRequest;
import com.deviantart.android.android.package_14.network.request.DVNTMarkNotesRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTMoveNotesIntoFolderRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTNoteRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTNotesFoldersRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTNotesInFolderRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTRecoverAccountRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTRegisterForPushNotificationsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTResendVerificationEmailRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTSendNoteRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTUpdateAccountRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTUpdatePushNotificationsSettingsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTWatchRecommendationsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTWatchRecommendationsUnsuggestRequestV1;
import com.deviantart.android.android.package_14.session.DVNTTokenSessionManager;
import com.deviantart.android.sdk.api.model.DVNTAccessToken;
import com.deviantart.android.sdk.api.model.DVNTAccountValidateResponse;
import com.deviantart.android.sdk.api.model.DVNTCategory.List;
import com.deviantart.android.sdk.api.model.DVNTFeedbackCursoredPage;
import com.deviantart.android.sdk.api.model.DVNTFeedbackMessages;
import com.deviantart.android.sdk.api.model.DVNTFeedbackStackedPage;
import com.deviantart.android.sdk.api.model.DVNTNote;
import com.deviantart.android.sdk.api.model.DVNTNotes;
import com.deviantart.android.sdk.api.model.DVNTNotesFolders;
import com.deviantart.android.sdk.api.model.DVNTSendNoteResults;
import com.deviantart.android.sdk.api.model.DVNTStatus;
import com.deviantart.android.sdk.api.model.DVNTSuccess;
import com.deviantart.android.sdk.api.model.DVNTWatchRecommendationResponse;
import java.util.List;

public class DVNTAsyncAPI
  extends DVNTCommonAsyncAPI
{
  public DVNTAsyncAPI() {}
  
  public static DVNTContextualizedGroupAPI buildGroupedRequest(Context paramContext)
  {
    if (DVNTAbstractAsyncAPI.asyncAPIInstance == null) {
      throw new RuntimeException("You need to call start() to use the API");
    }
    if (paramContext == null) {
      throw new IllegalArgumentException("Context can't be null.");
    }
    return ((DVNTAsyncAPI)DVNTAbstractAsyncAPI.asyncAPIInstance).createContextualizedGroupAPI(paramContext);
  }
  
  public static DVNTRequestExecutor categoryTree(String paramString, boolean paramBoolean)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTCategoryTreeRequestV1(paramString, Boolean.valueOf(paramBoolean)), DVNTCategory.List.class);
  }
  
  public static DVNTRequestExecutor createAccount(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTCreateAccountRequestV1(paramString1, paramString2, paramString3, paramString4, paramString5, paramBoolean1, paramBoolean2, paramBoolean3), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor deleteDeviation(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTDeleteDeviationRequestV1(paramString), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor deleteNotes(List paramList)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTDeleteNotesRequestV1(paramList), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor errorUtil(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTErrorUtilRequestV1(paramString), DVNTStatus.class);
  }
  
  public static DVNTRequestExecutor feedbackAllStackedMessages(String paramString1, String paramString2, Integer paramInteger)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedbackAllMessagesRequestV1(paramString1, Boolean.valueOf(true), paramString2, paramInteger), DVNTFeedbackCursoredPage.class);
  }
  
  public static DVNTRequestExecutor feedbackMentions(String paramString, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedbackMentionsRequestV1(paramString, paramInteger1, paramInteger2, paramInteger3), DVNTFeedbackMessages.class);
  }
  
  public static DVNTRequestExecutor feedbackMessagesForStack(String paramString, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedbackMessagesForStackRequestV1(paramString, paramInteger1, paramInteger2, paramInteger3), DVNTFeedbackMessages.class);
  }
  
  public static DVNTRequestExecutor feedbackStackedMessages(DVNTFeedbackType paramDVNTFeedbackType, String paramString, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedbackStackedMessagesRequestV1(paramDVNTFeedbackType, paramString, paramInteger1, paramInteger2, paramInteger3), DVNTFeedbackStackedPage.class);
  }
  
  public static DVNTRequestExecutor feedbackUnstackedMessages(DVNTFeedbackType paramDVNTFeedbackType, String paramString, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedbackUnstackedMessagesRequestV1(paramDVNTFeedbackType, paramString, paramInteger1, paramInteger2, paramInteger3), DVNTFeedbackMessages.class);
  }
  
  public static DVNTEnrichedToken getClientToken()
  {
    return asyncAPIInstanceapiClientInstance.clientSessionManager.getToken();
  }
  
  public static DVNTRequestExecutor getNote(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTNoteRequestV1(paramString), DVNTNote.class);
  }
  
  public static DVNTRequestExecutor getPushNotificationsSettings()
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTGetPushNotificationsSettingsRequestV1(), com.deviantart.android.sdk.api.model.DVNTPushNotificationsSettings.class);
  }
  
  public static DVNTEnrichedToken getUserToken()
  {
    return asyncAPIInstanceapiClientInstance.userSessionManager.getToken();
  }
  
  public static DVNTRequestExecutor getWatchRecommendations(String paramString, Integer paramInteger)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTWatchRecommendationsRequestV1(paramString, paramInteger), DVNTWatchRecommendationResponse.class);
  }
  
  public static DVNTRequestExecutor login(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTLoginRequest(paramString1, paramString2, paramString3, paramString4, paramString5, "password"), DVNTAccessToken.class, true);
  }
  
  public static DVNTRequestExecutor markNotes(List paramList, DVNTNote.Mark paramMark)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTMarkNotesRequestV1(paramList, paramMark), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor moveNotesIntoFolder(List paramList, String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTMoveNotesIntoFolderRequestV1(paramList, paramString), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor notesFolders()
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTNotesFoldersRequestV1(), DVNTNotesFolders.class);
  }
  
  public static DVNTRequestExecutor notesInFolder(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTNotesInFolderRequestV1(paramString, paramInteger1, paramInteger2), DVNTNotes.class);
  }
  
  public static DVNTRequestExecutor recoverAccountFromEmail(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTRecoverAccountRequestV1(null, paramString), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor recoverAccountFromUserName(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTRecoverAccountRequestV1(paramString, null), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor registerAppForPushNotifications(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTRegisterForPushNotificationsRequestV1(paramString), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor resendVerificationEmail()
  {
    return resendVerificationEmail(null, null);
  }
  
  public static DVNTRequestExecutor resendVerificationEmail(String paramString1, String paramString2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTResendVerificationEmailRequestV1(paramString1, paramString2), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor sendNote(List paramList, String paramString1, String paramString2, String paramString3)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTSendNoteRequestV1(paramList, paramString1, paramString2, paramString3), DVNTSendNoteResults.class);
  }
  
  public static DVNTRequestExecutor unsuggestArtist(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTWatchRecommendationsUnsuggestRequestV1(paramString), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor updateAccount(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTUpdateAccountRequestV1(paramString1, paramString2, paramString3, paramString4, paramBoolean), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor updatePushNotificationsSettings(com.deviantart.android.android.package_14.model.DVNTPushNotificationsSettings paramDVNTPushNotificationsSettings)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTUpdatePushNotificationsSettingsRequestV1(paramDVNTPushNotificationsSettings), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor validateAccount(String paramString1, String paramString2, String paramString3)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTAccountValidationRequestV1(paramString1, paramString2, paramString3), DVNTAccountValidateResponse.class);
  }
  
  public DVNTContextualizedGroupAPI createContextualizedGroupAPI(Context paramContext)
  {
    return new DVNTContextualizedGroupAPI(DVNTAbstractAsyncAPI.asyncAPIInstance, paramContext);
  }
}
