package com.deviantart.android.damobile.view.thirdparty.deviation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout.LayoutParams;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.view.thirdparty.GomType;
import com.deviantart.android.damobile.view.thirdparty.deviation.viewholder.ImageViewHolder;
import com.facebook.drawee.drawable.ScalingUtils.ScaleType;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.DraweeView;
import com.facebook.drawee.view.SimpleDraweeView;

public class SmallOrAnimatedImageGom
  extends RegularImageGom
{
  public SmallOrAnimatedImageGom() {}
  
  public void a(Context paramContext, DeviationDescription paramDeviationDescription, ImageViewHolder paramImageViewHolder, boolean paramBoolean)
  {
    Object localObject = paramDeviationDescription.a();
    SimpleDraweeView localSimpleDraweeView = draweeView;
    if (a(paramContext, (DVNTDeviation)localObject, localSimpleDraweeView)) {
      return;
    }
    localObject = a(paramContext, (DVNTDeviation)localObject);
    if ((((DVNTImage)localObject).getWidth() < 50) && (((DVNTImage)localObject).getHeight() < 50))
    {
      int i = Float.valueOf(Graphics.toString(paramContext)).intValue() * 2;
      localSimpleDraweeView.setLayoutParams(new LinearLayout.LayoutParams(((DVNTImage)localObject).getWidth() * i, ((DVNTImage)localObject).getHeight() * i));
    }
    for (;;)
    {
      localSimpleDraweeView.invalidate();
      super.a(paramContext, paramDeviationDescription, paramImageViewHolder, paramBoolean);
      ((GenericDraweeHierarchy)localSimpleDraweeView.getHierarchy()).c(ScalingUtils.ScaleType.g);
      return;
      localObject = new LinearLayout.LayoutParams(-1, 0);
      weight = 0.5F;
      localSimpleDraweeView.setLayoutParams((ViewGroup.LayoutParams)localObject);
    }
  }
  
  public GomType b()
  {
    return GomType.d;
  }
  
  public ImageViewHolder inflate(Context paramContext, ViewGroup paramViewGroup)
  {
    return new ImageViewHolder(LayoutInflater.from(paramContext).inflate(2130968702, paramViewGroup, false));
  }
}
