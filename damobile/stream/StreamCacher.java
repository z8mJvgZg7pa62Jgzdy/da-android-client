package com.deviantart.android.damobile.stream;

import com.deviantart.android.damobile.stream.loader.StreamLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

public class StreamCacher
{
  public static final StreamCacher d = new StreamCacher();
  private LinkedHashMap<String, StreamCacheStrategy> a = new LinkedHashMap();
  private LinkedHashMap<String, Stream> c = new LinkedHashMap();
  
  private StreamCacher() {}
  
  public static Stream a(StreamLoader paramStreamLoader)
  {
    return a(paramStreamLoader, null);
  }
  
  public static Stream a(StreamLoader paramStreamLoader, StreamCacheStrategy paramStreamCacheStrategy)
  {
    String str = paramStreamLoader.b();
    Object localObject = (Stream)dc.get(str);
    if (localObject != null)
    {
      paramStreamLoader = (StreamLoader)localObject;
      if (paramStreamCacheStrategy != null)
      {
        b(str, paramStreamCacheStrategy);
        return localObject;
      }
    }
    else
    {
      localObject = paramStreamCacheStrategy;
      if (paramStreamCacheStrategy == null) {
        localObject = StreamCacheStrategy.i;
      }
      paramStreamCacheStrategy = new Stream(paramStreamLoader);
      paramStreamLoader = paramStreamCacheStrategy;
      if (str != null)
      {
        a(str, paramStreamCacheStrategy, (StreamCacheStrategy)localObject);
        paramStreamLoader = paramStreamCacheStrategy;
      }
    }
    return paramStreamLoader;
  }
  
  public static void a()
  {
    StreamCacheStrategy[] arrayOfStreamCacheStrategy = StreamCacheStrategy.values();
    int j = arrayOfStreamCacheStrategy.length;
    int i = 0;
    while (i < j)
    {
      Iterator localIterator = arrayOfStreamCacheStrategy[i].getValue().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        ((Stream)dc.get(str)).close();
      }
      i += 1;
    }
  }
  
  private static void a(String paramString, Stream paramStream, StreamCacheStrategy paramStreamCacheStrategy)
  {
    a(paramString, paramStreamCacheStrategy);
    dc.put(paramString, paramStream);
  }
  
  private static void a(String paramString, StreamCacheStrategy paramStreamCacheStrategy)
  {
    int i = paramStreamCacheStrategy.d();
    ArrayList localArrayList = paramStreamCacheStrategy.getValue();
    if (localArrayList.size() >= i)
    {
      String str = (String)localArrayList.get(0);
      dc.remove(str);
      da.remove(str);
      localArrayList.remove(0);
    }
    localArrayList.add(paramString);
    da.put(paramString, paramStreamCacheStrategy);
  }
  
  public static void b(StreamCacheStrategy paramStreamCacheStrategy)
  {
    Object localObject = new LinkedList();
    Iterator localIterator = da.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if (((StreamCacheStrategy)localEntry.getValue()).equals(paramStreamCacheStrategy))
      {
        ((LinkedList)localObject).add(localEntry.getKey());
        ((StreamCacheStrategy)localEntry.getValue()).b((String)localEntry.getKey());
      }
    }
    paramStreamCacheStrategy = ((LinkedList)localObject).iterator();
    while (paramStreamCacheStrategy.hasNext())
    {
      localObject = (String)paramStreamCacheStrategy.next();
      dc.remove(localObject);
      da.remove(localObject);
    }
  }
  
  private static void b(String paramString, StreamCacheStrategy paramStreamCacheStrategy)
  {
    StreamCacheStrategy localStreamCacheStrategy = (StreamCacheStrategy)da.get(paramString);
    if (localStreamCacheStrategy == paramStreamCacheStrategy) {
      return;
    }
    localStreamCacheStrategy.getValue().remove(paramString);
    a(paramString, paramStreamCacheStrategy);
  }
  
  public static boolean b(StreamLoader paramStreamLoader)
  {
    return dc.containsKey(paramStreamLoader.b());
  }
  
  public static void clear()
  {
    dc.clear();
    StreamCacheStrategy.forget();
  }
  
  public static void d(String paramString)
  {
    dc.remove(paramString);
    StreamCacheStrategy localStreamCacheStrategy = (StreamCacheStrategy)da.get(paramString);
    if (localStreamCacheStrategy == null) {
      return;
    }
    localStreamCacheStrategy.b(paramString);
  }
  
  public static void loadAllResources(StreamLoader paramStreamLoader)
  {
    d(paramStreamLoader.b());
  }
}
