package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTFeedbackDeleteMessageRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  private String messageFolderId;
  private String messageId;
  private String stackId;
  
  public DVNTFeedbackDeleteMessageRequestV1(String paramString1, String paramString2, String paramString3)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    messageFolderId = paramString1;
    messageId = paramString2;
    stackId = paramString3;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTFeedbackDeleteMessageRequestV1)paramObject;
    return (Objects.append(messageFolderId, messageFolderId)) && (Objects.append(messageId, messageId)) && (Objects.append(stackId, stackId));
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), messageFolderId, messageId, stackId });
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).deleteMessage(paramString, messageFolderId, messageId, stackId);
  }
}
