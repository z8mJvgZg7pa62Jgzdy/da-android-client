package com.deviantart.android.android.package_14.model;

import java.io.Serializable;

public class DVNTWatchingInfo
  implements Serializable
{
  boolean watching;
  
  public DVNTWatchingInfo() {}
  
  public boolean isWatching()
  {
    return watching;
  }
  
  public void setWatching(boolean paramBoolean)
  {
    watching = paramBoolean;
  }
}
