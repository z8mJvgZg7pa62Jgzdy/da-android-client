package com.deviantart.android.android.package_14.model;

import java.io.Serializable;

public class DVNTRepostItem
  implements Serializable
{
  DVNTDeviation deviation;
  DVNTUserStatus status;
  String type;
  
  public DVNTRepostItem() {}
  
  public DVNTDeviation getDeviation()
  {
    return deviation;
  }
  
  public DVNTUserStatus getStatus()
  {
    return status;
  }
  
  public String getType()
  {
    return type;
  }
  
  public void setDeviation(DVNTDeviation paramDVNTDeviation)
  {
    deviation = paramDVNTDeviation;
  }
  
  public void setStatus(DVNTUserStatus paramDVNTUserStatus)
  {
    status = paramDVNTUserStatus;
  }
  
  public void setType(String paramString)
  {
    type = paramString;
  }
}
