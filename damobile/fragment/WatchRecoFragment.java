package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.activity.HomeActivity.BottomBarButton;
import com.deviantart.android.damobile.adapter.recyclerview.WatchRecoListAdapter;
import com.deviantart.android.damobile.stream.FilteredStream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.filter.StreamFilter;
import com.deviantart.android.damobile.stream.filter.WatchRecoFilter;
import com.deviantart.android.damobile.stream.loader.APIWatchRecoLoader;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.view.DAStateRecyclerView;

public class WatchRecoFragment
  extends HomeBaseFragment
{
  @Bind({2131689916})
  DAStateRecyclerView daStateRecyclerView;
  
  public WatchRecoFragment() {}
  
  protected HomeActivity.BottomBarButton c()
  {
    return HomeActivity.BottomBarButton.b;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = paramLayoutInflater.inflate(2130968696, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    new APIWatchRecoLoader().add(5);
    paramViewGroup = new WatchRecoListAdapter(new FilteredStream(StreamCacher.a(new APIWatchRecoLoader(), StreamCacheStrategy.b), new StreamFilter[] { new WatchRecoFilter() }));
    daStateRecyclerView.setAdapter(paramViewGroup);
    daStateRecyclerView.setAdapter();
    daStateRecyclerView.onViewCreated();
    initUI();
    ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(true);
    ((BaseActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
    ((BaseActivity)getActivity()).getSupportActionBar().setCustomView(2130968714);
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      break;
    }
    for (;;)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      getActivity().onBackPressed();
    }
  }
}
