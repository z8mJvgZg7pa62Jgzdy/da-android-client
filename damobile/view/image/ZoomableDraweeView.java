package com.deviantart.android.damobile.view.image;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.widget.ImageView;
import com.facebook.common.internal.Preconditions;
import com.facebook.common.logging.FLog;
import com.facebook.drawee.controller.AbstractDraweeController;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.DraweeView;
import com.facebook.drawee.view.GenericDraweeView;

public class ZoomableDraweeView
  extends GenericDraweeView
  implements ZoomableController.Listener
{
  private final RectF b = new RectF();
  ZoomableDraweeView.DataSourceReadyListener d;
  private final RectF g = new RectF();
  private DraweeController i;
  private DefaultZoomableController l = DefaultZoomableController.getContentProvider(getContext());
  private DefaultZoomableController.ZoomEventListener o;
  private boolean r = false;
  private final ControllerListener u = new ZoomableDraweeView.1(this);
  
  public ZoomableDraweeView(Context paramContext)
  {
    super(paramContext);
    setShortcut();
  }
  
  public ZoomableDraweeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setShortcut();
  }
  
  public ZoomableDraweeView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    setShortcut();
  }
  
  private void a()
  {
    ((GenericDraweeHierarchy)getHierarchy()).a(g);
    b.set(0.0F, 0.0F, getWidth(), getHeight());
    l.a(g);
    l.inflate(b);
    l.b();
    FLog.a("ZoomableDraweeView", "updateZoomableControllerBounds: view %x, view bounds: %s, image bounds: %s", Integer.valueOf(hashCode()), b, g);
  }
  
  private void add()
  {
    FLog.a("ZoomableDraweeView", "onFinalImageSet: view %x", Integer.valueOf(hashCode()));
    e();
    if (!r) {
      return;
    }
    d.a();
  }
  
  private void b()
  {
    if (l.d() == l.c())
    {
      o.a(DefaultZoomableController.DVNTZoomMode.o);
      return;
    }
    o.a(DefaultZoomableController.DVNTZoomMode.b);
  }
  
  private void b(DraweeController paramDraweeController)
  {
    if ((paramDraweeController instanceof AbstractDraweeController)) {
      ((AbstractDraweeController)paramDraweeController).multiply(u);
    }
  }
  
  private void b(DraweeController paramDraweeController1, DraweeController paramDraweeController2)
  {
    b(getController());
    f(paramDraweeController1);
    i = paramDraweeController2;
    super.setController(paramDraweeController1);
  }
  
  private void c()
  {
    if ((i != null) && (l.d() > 1.1F)) {
      b(i, null);
    }
  }
  
  private void e()
  {
    if (l.a()) {
      return;
    }
    a();
    l.a(true);
  }
  
  private void f(DraweeController paramDraweeController)
  {
    if ((paramDraweeController instanceof AbstractDraweeController)) {
      ((AbstractDraweeController)paramDraweeController).a(u);
    }
  }
  
  private void i()
  {
    FLog.a("ZoomableDraweeView", "onRelease: view %x", Integer.valueOf(hashCode()));
    l.a(false);
  }
  
  private void setShortcut()
  {
    l.b(this);
  }
  
  public void a(Matrix paramMatrix)
  {
    FLog.a("ZoomableDraweeView", "onTransformChanged: view %x", Integer.valueOf(hashCode()));
    c();
    if (o != null) {
      b();
    }
    invalidate();
  }
  
  public void a(Runnable paramRunnable)
  {
    if (Build.VERSION.SDK_INT >= 16)
    {
      postOnAnimation(paramRunnable);
      return;
    }
    postDelayed(paramRunnable, 16L);
  }
  
  public void c(DraweeController paramDraweeController1, DraweeController paramDraweeController2)
  {
    b(null, null);
    l.a(false);
    b(paramDraweeController1, paramDraweeController2);
  }
  
  public PointF getCenter()
  {
    return l.draw(new PointF(getWidth() / 2, getHeight() / 2));
  }
  
  public float getScaleFactor()
  {
    return l.d();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    int j = paramCanvas.save();
    paramCanvas.concat(l.get());
    super.onDraw(paramCanvas);
    paramCanvas.restoreToCount(j);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    FLog.a("ZoomableDraweeView", "onLayout: view %x", Integer.valueOf(hashCode()));
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    a();
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (l.d(paramMotionEvent))
    {
      if (l.d() > 1.0F) {
        getParent().requestDisallowInterceptTouchEvent(true);
      }
      Log.v("ZoomableDraweeView", "onTouchEvent: view %x, handled by zoomable controller");
      return true;
    }
    Log.v("ZoomableDraweeView", "onTouchEvent: view %x, handled by the super");
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public void setController(DraweeController paramDraweeController)
  {
    c(paramDraweeController, null);
  }
  
  public void setDataSourceReadyListener(ZoomableDraweeView.DataSourceReadyListener paramDataSourceReadyListener)
  {
    d = paramDataSourceReadyListener;
  }
  
  public void setMaxZoom(float paramFloat)
  {
    l.b(paramFloat);
  }
  
  public void setUseSubsampling(boolean paramBoolean)
  {
    r = paramBoolean;
  }
  
  public void setZoomEventListener(DefaultZoomableController.ZoomEventListener paramZoomEventListener)
  {
    o = paramZoomEventListener;
  }
  
  public void setZoomableController(DefaultZoomableController paramDefaultZoomableController)
  {
    Preconditions.append(paramDefaultZoomableController);
    l.b(null);
    l = paramDefaultZoomableController;
    l.b(this);
  }
}
