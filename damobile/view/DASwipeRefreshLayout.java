package com.deviantart.android.damobile.view;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.View;

public class DASwipeRefreshLayout
  extends SwipeRefreshLayout
{
  public DASwipeRefreshLayout(Context paramContext)
  {
    super(paramContext);
    onCreate();
  }
  
  public DASwipeRefreshLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    onCreate();
  }
  
  private void onCreate()
  {
    setColorSchemeResources(new int[] { 2131558555 });
    setProgressViewOffset(0);
  }
  
  public void setProgressViewOffset(int paramInt)
  {
    setProgressViewOffset(false, paramInt, (int)getContext().getResources().getDimension(2131361973) + paramInt);
  }
}
