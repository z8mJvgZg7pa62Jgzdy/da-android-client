package com.deviantart.android.android.package_14.model;

import com.deviantart.android.sdk.api.model.DVNTNotesFolder.List;
import com.deviantart.android.sdk.api.network.wrapper.DVNTResultsWrapper;

public class DVNTNotesFolders
  extends DVNTResultsWrapper<DVNTNotesFolder.List>
{
  public DVNTNotesFolders() {}
}
