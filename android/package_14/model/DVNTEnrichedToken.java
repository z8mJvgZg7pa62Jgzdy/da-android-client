package com.deviantart.android.android.package_14.model;

import com.google.common.base.Objects;
import java.util.Date;

public class DVNTEnrichedToken
{
  private Long lifeTimeInSeconds;
  private String refreshToken;
  private Long retrievalTimeInMillis;
  private String scope;
  private final String secret;
  private final String token;
  
  public DVNTEnrichedToken(DVNTAccessToken paramDVNTAccessToken)
  {
    this(paramDVNTAccessToken.getRawAccessToken(), paramDVNTAccessToken.getRefreshToken(), paramDVNTAccessToken.getExpiresIn(), Long.valueOf(new Date().getTime()));
    scope = paramDVNTAccessToken.getScope();
  }
  
  public DVNTEnrichedToken(String paramString1, String paramString2)
  {
    token = paramString1;
    secret = paramString2;
  }
  
  public DVNTEnrichedToken(String paramString1, String paramString2, Long paramLong1, Long paramLong2)
  {
    this(paramString1, "");
    refreshToken = paramString2;
    retrievalTimeInMillis = paramLong2;
    lifeTimeInSeconds = paramLong1;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    paramObject = (DVNTEnrichedToken)paramObject;
    return (Objects.append(retrievalTimeInMillis, retrievalTimeInMillis)) && (Objects.append(lifeTimeInSeconds, lifeTimeInSeconds)) && (Objects.append(refreshToken, refreshToken)) && (Objects.append(scope, scope)) && (Objects.append(token, token)) && (Objects.append(secret, secret));
  }
  
  public Long getLifeTimeInSeconds()
  {
    return lifeTimeInSeconds;
  }
  
  public String getRefreshToken()
  {
    return refreshToken;
  }
  
  public Long getRetrievalTimeInMillis()
  {
    return retrievalTimeInMillis;
  }
  
  public String getScope()
  {
    return scope;
  }
  
  public String getSecret()
  {
    return secret;
  }
  
  public String getToken()
  {
    return token;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { retrievalTimeInMillis, lifeTimeInSeconds, refreshToken, scope, token, secret });
  }
  
  public DVNTEnrichedToken setScope(String paramString)
  {
    scope = paramString;
    return this;
  }
  
  public String toString()
  {
    return "DVNTEnrichedToken{retrievalTimeInMillis=" + retrievalTimeInMillis + ", lifeTimeInSeconds=" + lifeTimeInSeconds + ", refreshToken='" + refreshToken + '\'' + ", scope='" + scope + '\'' + ", token='" + token + '\'' + ", secret='" + secret + '\'' + '}';
  }
}
