package com.deviantart.android.damobile.view.deviationtab;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTChallengeEntry;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APIChallengeEntryLoader;
import com.deviantart.android.damobile.stream.loader.APIMoreFromThisArtistLoader;
import com.deviantart.android.damobile.stream.loader.APIMoreLikeThisLoader;
import com.deviantart.android.damobile.stream.loader.StaticStreamLoader;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.view.DAStateRecyclerView;

public class PivotLayout
  extends DAStateRecyclerView
{
  public PivotLayout(Context paramContext, DVNTDeviation paramDVNTDeviation)
  {
    super(paramContext);
    setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    setPadding(getPaddingLeft() + (int)getResources().getDimension(2131361990), getPaddingTop(), getPaddingRight() + (int)getResources().getDimension(2131361990), getPaddingBottom());
    setBackgroundColor(ContextCompat.getColor(paramContext, 2131558417));
    setVerticalFadingEdgeEnabled(true);
    setFadingEdgeLength(Graphics.add(paramContext, 75));
    String str = paramDVNTDeviation.getId();
    StaticStreamLoader localStaticStreamLoader = new StaticStreamLoader("deviation_pivot" + str);
    if (paramDVNTDeviation.getChallengeEntry() != null) {
      localStaticStreamLoader.a(new PivotLayout.PivotModuleInfo(paramContext, paramDVNTDeviation, new APIChallengeEntryLoader(paramDVNTDeviation.getChallengeEntry().getChallengeDeviationId()), "more_from_cahllenge", paramContext.getString(2131231088) + paramDVNTDeviation.getChallengeEntry().getChallengeTitle()));
    }
    localStaticStreamLoader.a(new PivotLayout.PivotModuleInfo(paramContext, paramDVNTDeviation, new APIMoreFromThisArtistLoader(str), "more_from_this_artist", getResources().getString(2131231089)));
    localStaticStreamLoader.a(new PivotLayout.PivotModuleInfo(paramContext, paramDVNTDeviation, new APIMoreLikeThisLoader(str, null), "more_like_this", getResources().getString(2131231090)));
    setAdapter(new PivotLayout.DeviationPivotAdapter(StreamCacher.a(localStaticStreamLoader)));
  }
}
