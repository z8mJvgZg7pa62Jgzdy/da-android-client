package com.deviantart.android.android.package_14;

import android.content.Context;
import com.deviantart.android.android.package_14.config.DVNTAPIConfig;
import com.deviantart.android.android.package_14.config.DVNTAPIModule;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig;
import com.deviantart.android.android.package_14.config.DVNTGraduateHandler;
import com.deviantart.android.android.package_14.model.DVNTEnrichedToken;
import com.deviantart.datoolkit.logger.DVNTLog;
import java.util.UUID;
import javax.inject.Inject;
import org.slf4j.Logger;

public abstract class DVNTAbstractAsyncAPI
{
  protected static DVNTAbstractAsyncAPI asyncAPIInstance;
  static DVNTMockRegistry mockedAPI;
  @Inject
  DVNTAPIClient apiClientInstance;
  DVNTAPIConfig apiConfig;
  @Inject
  Logger logger;
  
  public DVNTAbstractAsyncAPI() {}
  
  public static void cancelAllRequests()
  {
    if (asyncAPIInstance == null)
    {
      DVNTLog.append("Cache", new Object[] { "tried to cancel request but no session has been opened yet" });
      return;
    }
    asyncAPIInstanceapiClientInstance.cancelAllRequests();
  }
  
  public static void cancelRequest(UUID paramUUID)
  {
    if (asyncAPIInstance == null)
    {
      DVNTLog.append("Cache", new Object[] { "tried to cancel request but no session has been opened yet" });
      return;
    }
    asyncAPIInstanceapiClientInstance.cancelRequest(paramUUID);
  }
  
  public static void clearCache()
  {
    if (asyncAPIInstance == null)
    {
      DVNTLog.append("Cache", new Object[] { "tried to invalidate the cache but no session has been opened yet" });
      return;
    }
    asyncAPIInstanceapiClientInstance.clearCache();
  }
  
  public static void clearCache(UUID paramUUID)
  {
    if (asyncAPIInstance == null)
    {
      DVNTLog.append("Cache", new Object[] { "tried to invalidate the cache but no session has been opened yet" });
      return;
    }
    asyncAPIInstanceapiClientInstance.clearCache(paramUUID);
  }
  
  public static DVNTAPIConfig getConfig()
  {
    if (asyncAPIInstance == null) {
      throw new RuntimeException("You need to start a session before trying to read the config");
    }
    return asyncAPIInstanceapiConfig;
  }
  
  public static void graduate(Context paramContext)
  {
    graduate(paramContext, null);
  }
  
  public static void graduate(Context paramContext, DVNTEnrichedToken paramDVNTEnrichedToken)
  {
    if (asyncAPIInstance == null) {
      throw new RuntimeException("You need to start a session before trying to graduate");
    }
    if (paramDVNTEnrichedToken != null) {
      asyncAPIInstanceapiConfig.getGraduateHandler().graduateWithToken(paramContext, paramDVNTEnrichedToken);
    }
    asyncAPIInstance.openOAuthSession(paramContext, true);
  }
  
  public static boolean isUserSession(Context paramContext)
  {
    return (asyncAPIInstance != null) && (asyncAPIInstanceapiClientInstance != null) && (asyncAPIInstanceapiClientInstance.hasGraduated(paramContext, true));
  }
  
  public static void logout(Context paramContext)
  {
    if (asyncAPIInstance != null)
    {
      if (asyncAPIInstanceapiClientInstance == null) {
        return;
      }
      asyncAPIInstanceapiClientInstance.closeUserSession(paramContext);
      if (asyncAPIInstanceapiClientInstance != null) {
        asyncAPIInstanceapiClientInstance.resetUserSession();
      }
    }
  }
  
  public static void start(Context paramContext, DVNTAPIConfig paramDVNTAPIConfig)
  {
    startWithModule(paramContext, paramDVNTAPIConfig, null);
  }
  
  public static void start(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    DVNTAPIConfig localDVNTAPIConfig = new DVNTAPIConfig();
    localDVNTAPIConfig.setClientId(paramString1);
    localDVNTAPIConfig.setClientSecret(paramString2);
    localDVNTAPIConfig.setScope(paramString3);
    startWithModule(paramContext, localDVNTAPIConfig, null);
  }
  
  static void startWithModule(Context paramContext, DVNTAPIConfig paramDVNTAPIConfig, DVNTAPIModule paramDVNTAPIModule)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: fail exe a4 = a3\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.exec(BaseAnalyze.java:92)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.exec(BaseAnalyze.java:1)\n\tat com.googlecode.dex2jar.ir.ts.Cfg.dfs(Cfg.java:255)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.analyze0(BaseAnalyze.java:75)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.analyze(BaseAnalyze.java:69)\n\tat com.googlecode.dex2jar.ir.ts.UnSSATransformer.transform(UnSSATransformer.java:274)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:163)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\nCaused by: java.lang.NullPointerException\n\tat com.googlecode.dex2jar.ir.ts.UnSSATransformer$LiveA.onUseLocal(UnSSATransformer.java:552)\n\tat com.googlecode.dex2jar.ir.ts.UnSSATransformer$LiveA.onUseLocal(UnSSATransformer.java:1)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.onUse(BaseAnalyze.java:166)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.onUse(BaseAnalyze.java:1)\n\tat com.googlecode.dex2jar.ir.ts.Cfg.travel(Cfg.java:331)\n\tat com.googlecode.dex2jar.ir.ts.Cfg.travel(Cfg.java:387)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.exec(BaseAnalyze.java:90)\n\t... 17 more\n");
  }
  
  public static void stop(Context paramContext)
  {
    asyncAPIInstance.closeOAuthSession(paramContext);
    asyncAPIInstance = null;
  }
  
  void closeOAuthSession(Context paramContext)
  {
    apiClientInstance.stopListening();
  }
  
  public DVNTMockRegistry getMockRegistry()
  {
    if (mockedAPI == null) {
      mockedAPI = new DVNTMockRegistry();
    }
    return mockedAPI;
  }
  
  void openOAuthSession(Context paramContext, boolean paramBoolean)
  {
    apiClientInstance.getSession(paramContext, paramBoolean);
  }
}
