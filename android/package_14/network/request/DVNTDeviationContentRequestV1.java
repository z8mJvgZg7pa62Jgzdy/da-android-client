package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTDeviationContentRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTDeviationContent>
{
  private final String deviationId;
  
  public DVNTDeviationContentRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTDeviationContent.class);
    deviationId = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTDeviationContentRequestV1 localDVNTDeviationContentRequestV1 = (DVNTDeviationContentRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(deviationId, deviationId).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "browsedeviationcontent" + deviationId;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(deviationId).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTDeviationContent sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).deviationContent(paramString, deviationId);
  }
}
