package com.deviantart.android.damobile.view.thirdparty.comment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.deviantart.android.android.package_14.model.DVNTComment;
import com.deviantart.android.damobile.util.threaditem.CommentItem;
import com.deviantart.android.damobile.view.thirdparty.GomType;

public class HiddenCommentGom
  extends com.deviantart.android.damobile.view.gom.comment.CommentGom<com.deviantart.android.damobile.view.gom.comment.viewholder.HiddenCommentViewHolder>
{
  public HiddenCommentGom() {}
  
  public com.deviantart.android.damobile.view.thirdparty.comment.viewholder.HiddenCommentViewHolder a(Context paramContext, ViewGroup paramViewGroup)
  {
    return new com.deviantart.android.damobile.view.thirdparty.comment.viewholder.HiddenCommentViewHolder(LayoutInflater.from(paramContext).inflate(2130968618, paramViewGroup, false));
  }
  
  public GomType b()
  {
    return GomType.l;
  }
  
  public void b(Context paramContext, CommentItem paramCommentItem, com.deviantart.android.damobile.view.thirdparty.comment.viewholder.HiddenCommentViewHolder paramHiddenCommentViewHolder)
  {
    super.a(paramContext, paramCommentItem, paramHiddenCommentViewHolder);
    paramCommentItem = paramCommentItem.intValue().getHidden();
    if (paramCommentItem == null) {
      return;
    }
    int i = -1;
    switch (paramCommentItem.hashCode())
    {
    default: 
      break;
    }
    for (;;)
    {
      switch (i)
      {
      default: 
        return;
      case 0: 
        commentHidden.setText(paramContext.getString(2131231014));
        return;
        if (paramCommentItem.equals("hidden_by_owner"))
        {
          i = 0;
          continue;
          if (paramCommentItem.equals("hidden_by_commenter"))
          {
            i = 1;
            continue;
            if (paramCommentItem.equals("hidden_by_admin")) {
              i = 2;
            }
          }
        }
        break;
      }
    }
    commentHidden.setText(paramContext.getString(2131231013));
    return;
    commentHidden.setText(paramContext.getString(2131231012));
  }
}
