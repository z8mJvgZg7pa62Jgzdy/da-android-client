package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTDAMLComponent
  implements Serializable
{
  @SerializedName("id")
  String Id;
  @SerializedName("deviation")
  DVNTDeviation deviation;
  @SerializedName("fallback")
  String fallbackHTML;
  @SerializedName("height")
  Integer height;
  @SerializedName("html")
  String html;
  @SerializedName("profile")
  String profile;
  @SerializedName("stash_metadata")
  DVNTStashItem stashMetadata;
  @SerializedName("type")
  DVNTDAMLComponent.Type type;
  @SerializedName("width")
  Integer width;
  
  public DVNTDAMLComponent() {}
  
  public DVNTDeviation getDeviation()
  {
    return deviation;
  }
  
  public String getFallbackHTML()
  {
    return fallbackHTML;
  }
  
  public Integer getHeight()
  {
    return height;
  }
  
  public String getHtml()
  {
    return html;
  }
  
  public String getId()
  {
    return Id;
  }
  
  public String getProfile()
  {
    return profile;
  }
  
  public DVNTStashItem getStashMetadata()
  {
    return stashMetadata;
  }
  
  public DVNTDAMLComponent.Type getType()
  {
    return type;
  }
  
  public Integer getWidth()
  {
    return width;
  }
  
  public void setDeviation(DVNTDeviation paramDVNTDeviation)
  {
    deviation = paramDVNTDeviation;
  }
  
  public void setFallbackHTML(String paramString)
  {
    fallbackHTML = paramString;
  }
  
  public void setHeight(Integer paramInteger)
  {
    height = paramInteger;
  }
  
  public void setHtml(String paramString)
  {
    html = paramString;
  }
  
  public void setId(String paramString)
  {
    Id = paramString;
  }
  
  public void setProfile(String paramString)
  {
    profile = paramString;
  }
  
  public void setStashMetadata(DVNTStashItem paramDVNTStashItem)
  {
    stashMetadata = paramDVNTStashItem;
  }
  
  public void setType(DVNTDAMLComponent.Type paramType)
  {
    type = paramType;
  }
  
  public void setWidth(Integer paramInteger)
  {
    width = paramInteger;
  }
}
