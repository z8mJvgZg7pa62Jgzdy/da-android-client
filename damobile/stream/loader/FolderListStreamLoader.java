package com.deviantart.android.damobile.stream.loader;

import java.util.Iterator;
import java.util.List;

public abstract class FolderListStreamLoader<T extends com.deviantart.android.sdk.api.model.DVNTGallection>
  extends StreamLoader<T>
{
  public FolderListStreamLoader() {}
  
  protected boolean add(List paramList)
  {
    if (paramList.isEmpty()) {
      return true;
    }
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      com.deviantart.android.android.package_14.model.DVNTGallection localDVNTGallection = (com.deviantart.android.android.package_14.model.DVNTGallection)paramList.next();
      if ((localDVNTGallection != null) && (localDVNTGallection.getSize() != null) && (localDVNTGallection.getSize().intValue() > 0)) {
        return false;
      }
    }
    return true;
  }
}
