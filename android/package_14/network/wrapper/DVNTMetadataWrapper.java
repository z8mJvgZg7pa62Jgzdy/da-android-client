package com.deviantart.android.android.package_14.network.wrapper;

import com.google.gson.annotations.SerializedName;

public class DVNTMetadataWrapper<T>
{
  @SerializedName("metadata")
  private T results;
  
  public DVNTMetadataWrapper() {}
  
  public Object getResults()
  {
    return results;
  }
}
