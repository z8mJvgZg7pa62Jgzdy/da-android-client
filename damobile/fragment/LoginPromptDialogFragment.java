package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.os.BaseBundle;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.damobile.activity.HomeActivity;
import com.deviantart.android.damobile.activity.SignUpActivity;

public class LoginPromptDialogFragment
  extends DialogFragment
{
  private static final String NULL_ARG = LoginPromptDialogFragment.class.getName();
  @Bind({2131689971})
  TextView reasonView;
  
  public LoginPromptDialogFragment() {}
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setStyle(2, 2131427541);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = paramLayoutInflater.inflate(2130968737, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    paramViewGroup = getArguments();
    reasonView.setText(paramViewGroup.getString("reason"));
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public void onJoinClick()
  {
    Intent localIntent = new Intent(getActivity(), SignUpActivity.class);
    if (getActivity().getClass() == HomeActivity.class) {
      getActivity().startActivityForResult(localIntent, 104);
    }
    for (;;)
    {
      dismiss();
      return;
      getActivity().startActivity(localIntent);
    }
  }
  
  public void onLoginClick()
  {
    DVNTAbstractAsyncAPI.graduate(getActivity());
    dismiss();
  }
}
