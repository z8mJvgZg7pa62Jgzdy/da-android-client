package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTNotesInFolderRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTNotes>
{
  private final String folderId;
  private final Integer limit;
  private final Integer offset;
  
  public DVNTNotesInFolderRequestV1(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTNotes.class);
    folderId = paramString;
    offset = paramInteger1;
    limit = paramInteger2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTNotesInFolderRequestV1)paramObject;
    return (Objects.append(folderId, folderId)) && (Objects.append(offset, offset)) && (Objects.append(limit, limit));
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), folderId, offset, limit });
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTNotes sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).getNotesInFolder(paramString, folderId, offset, limit, Boolean.valueOf(true));
  }
}
