package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

public class DVNTDeviationContent
  implements Serializable
{
  @SerializedName("css_fonts")
  private ArrayList<String> cssFonts;
  private String html;
  private String orgName;
  
  public DVNTDeviationContent() {}
  
  public String getCss()
  {
    return orgName;
  }
  
  public ArrayList getCssFonts()
  {
    return cssFonts;
  }
  
  public String getHtml()
  {
    return html;
  }
  
  public void setCss(String paramString)
  {
    orgName = paramString;
  }
  
  public void setCssFonts(ArrayList paramArrayList)
  {
    cssFonts = paramArrayList;
  }
  
  public void setHtml(String paramString)
  {
    html = paramString;
  }
}
