package com.deviantart.android.damobile.adapter.recyclerview;

import android.os.Handler;
import com.deviantart.android.damobile.adapter.HeaderFooterRecyclerViewAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.threaditem.ChildStreamListener;
import com.deviantart.android.damobile.util.threaditem.ThreadItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public abstract class ThreadedItemsAdapter<STREAM_ITEM, CHILDREN_STREAM_ITEM, THREAD_ITEM extends ThreadItem>
  extends DAStateRecyclerViewAdapter<STREAM_ITEM>
  implements ChildStreamListener<THREAD_ITEM>
{
  protected final HashMap<String, Stream<CHILDREN_STREAM_ITEM>> a = new HashMap();
  protected final HashMap<String, Integer> b = new HashMap();
  protected final ArrayList<THREAD_ITEM> c = new ArrayList();
  protected int d = 0;
  
  public ThreadedItemsAdapter(Stream paramStream)
  {
    super(paramStream);
    b();
    b.clear();
  }
  
  public abstract int a(List paramList);
  
  public void a()
  {
    int i = d;
    try
    {
      add(0, i);
      e = 0;
      c.a();
      d = 0;
      c.clear();
      b.clear();
      a.clear();
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        new Handler().post(ThreadedItemsAdapter..Lambda.3.c(this));
      }
    }
  }
  
  public void a(ThreadItem paramThreadItem, int paramInt)
  {
    paramInt = b(paramThreadItem, paramInt);
    paramThreadItem.d(Boolean.valueOf(false));
    Object localObject = new ArrayList();
    a((ArrayList)localObject, paramThreadItem.c());
    b.remove(paramThreadItem.f());
    d -= ((ArrayList)localObject).size();
    try
    {
      add(paramInt + 1, ((ArrayList)localObject).size());
      paramThreadItem = ((ArrayList)localObject).iterator();
      while (paramThreadItem.hasNext())
      {
        localObject = (ThreadItem)paramThreadItem.next();
        a.remove(((ThreadItem)localObject).f());
        b.remove(((ThreadItem)localObject).f());
        c.remove(localObject);
      }
    }
    catch (Exception paramThreadItem)
    {
      for (;;)
      {
        new Handler().post(ThreadedItemsAdapter..Lambda.2.c(this));
      }
    }
  }
  
  protected void a(ArrayList paramArrayList, String paramString)
  {
    int i = 0;
    while (i < e())
    {
      ThreadItem localThreadItem = (ThreadItem)c.get(i);
      if ((localThreadItem.b() != null) && (localThreadItem.b().equals(paramString)))
      {
        if (localThreadItem.visitAnnotation().booleanValue()) {
          a(paramArrayList, localThreadItem.c());
        }
        paramArrayList.add(localThreadItem);
      }
      i += 1;
    }
  }
  
  public int b(ThreadItem paramThreadItem, int paramInt)
  {
    if ((paramThreadItem == null) || (c == null)) {
      return 0;
    }
    int i;
    if ((c.size() > paramInt) && (c.get(paramInt) != null))
    {
      i = paramInt;
      if (((ThreadItem)c.get(paramInt)).equals(paramThreadItem)) {}
    }
    else
    {
      i = Math.max(0, c.indexOf(paramThreadItem));
    }
    return i;
  }
  
  public void b()
  {
    if ((c != null) && (e < c.size()))
    {
      if (c == null) {
        return;
      }
      int i = a(c.get().subList(e, c.size()));
      int j = d;
      try
      {
        append(j, i);
        e = c.size();
        d = (i + d);
        return;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          new Handler().post(ThreadedItemsAdapter..Lambda.1.c(this));
        }
      }
    }
  }
  
  public int e()
  {
    return c.size();
  }
}
