package com.deviantart.android.damobile.view.thirdparty.deviation;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.ProcessMenuListener;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.view.LongPressItem;
import com.deviantart.android.damobile.view.gom.Gom;
import java.lang.ref.WeakReference;

public abstract class DeviationGom<VIEW_HOLDER extends RecyclerView.ViewHolder>
  implements Gom<DeviationDescription, VIEW_HOLDER>
{
  protected int b;
  protected int c;
  private WeakReference<ProcessMenuListener> l;
  
  public DeviationGom() {}
  
  public void a(Context paramContext, DeviationDescription paramDeviationDescription, RecyclerView.ViewHolder paramViewHolder)
  {
    b(paramContext, paramDeviationDescription, paramViewHolder, true);
  }
  
  public void b(int paramInt1, int paramInt2)
  {
    c = paramInt1;
    b = paramInt2;
  }
  
  public void b(Context paramContext, DeviationDescription paramDeviationDescription, RecyclerView.ViewHolder paramViewHolder, boolean paramBoolean)
  {
    if (!DVNTContextUtils.isContextDead(paramContext))
    {
      if (!(paramContext instanceof Activity)) {
        return;
      }
      paramContext = (Activity)paramContext;
      Stream localStream = StreamCacher.a(paramDeviationDescription.d());
      int i = paramDeviationDescription.c();
      paramDeviationDescription = paramDeviationDescription.a();
      paramDeviationDescription = LongPressItem.a(itemView, DeviationUtils.b(paramContext, paramDeviationDescription));
      paramDeviationDescription.b(new DeviationGom.1(this, paramContext, localStream, i));
      if ((l != null) && (l.get() != null)) {
        paramDeviationDescription.b((ProcessMenuListener)l.get());
      }
    }
  }
  
  public void b(ProcessMenuListener paramProcessMenuListener)
  {
    l = new WeakReference(paramProcessMenuListener);
  }
}
