package com.deviantart.android.damobile.stream.filter;

import android.content.Context;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation.List;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.DAMobileApplication;
import com.deviantart.android.damobile.util.UserUtils;
import java.util.ArrayList;
import java.util.Iterator;

public class WatchRecoFilter
  implements StreamFilter<com.deviantart.android.sdk.api.model.DVNTWatchRecommendationItem>
{
  public WatchRecoFilter() {}
  
  public boolean trim(com.deviantart.android.android.package_14.model.DVNTWatchRecommendationItem paramDVNTWatchRecommendationItem)
  {
    Object localObject = DAMobileApplication.getContext();
    if (DVNTContextUtils.isContextDead((Context)localObject)) {
      return false;
    }
    if ((paramDVNTWatchRecommendationItem.getUser().getIsWatching() != null) && (paramDVNTWatchRecommendationItem.getUser().getIsWatching().booleanValue())) {
      return true;
    }
    DVNTAbstractDeviation.List localList;
    if (!UserUtils.get((Context)localObject))
    {
      localList = new DVNTAbstractDeviation.List();
      Iterator localIterator = paramDVNTWatchRecommendationItem.getDeviations().iterator();
      do
      {
        do
        {
          localObject = localList;
          if (!localIterator.hasNext()) {
            break;
          }
          localObject = (DVNTDeviation)localIterator.next();
        } while (((DVNTAbstractDeviation)localObject).isMature().booleanValue());
        localList.add(localObject);
      } while (localList.size() < 4);
    }
    for (localObject = localList; ((ArrayList)localObject).size() < 2; localObject = paramDVNTWatchRecommendationItem.getDeviations()) {
      return true;
    }
    paramDVNTWatchRecommendationItem.setDeviations((DVNTAbstractDeviation.List)localObject);
    return false;
  }
}
