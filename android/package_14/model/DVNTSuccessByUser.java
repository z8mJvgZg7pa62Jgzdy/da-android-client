package com.deviantart.android.android.package_14.model;

import java.io.Serializable;

public class DVNTSuccessByUser
  implements Serializable
{
  Boolean success;
  DVNTUser user;
  
  public DVNTSuccessByUser() {}
  
  public DVNTUser getUser()
  {
    return user;
  }
  
  public Boolean isSuccess()
  {
    return success;
  }
  
  public DVNTSuccessByUser setSuccess(Boolean paramBoolean)
  {
    success = paramBoolean;
    return this;
  }
  
  public DVNTSuccessByUser setUser(DVNTUser paramDVNTUser)
  {
    user = paramDVNTUser;
    return this;
  }
}
