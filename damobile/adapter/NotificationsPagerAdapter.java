package com.deviantart.android.damobile.adapter;

import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.DAMobileApplication;
import com.deviantart.android.damobile.util.notifications.NotificationsPage;
import com.deviantart.android.damobile.view.notifications.NotificationsListFrame;

public class NotificationsPagerAdapter
  extends StatefulPagerAdapter
{
  private int o;
  
  public NotificationsPagerAdapter(int paramInt)
  {
    o = paramInt;
  }
  
  public View b(ViewGroup paramViewGroup, int paramInt)
  {
    NotificationsPage localNotificationsPage = NotificationsPage.c[paramInt];
    NotificationsListFrame localNotificationsListFrame = localNotificationsPage.a(paramViewGroup.getContext(), 20, o);
    b(localNotificationsListFrame, localNotificationsPage.getString());
    paramViewGroup.addView(localNotificationsListFrame);
    return localNotificationsListFrame;
  }
  
  public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    paramViewGroup.removeView((View)paramObject);
  }
  
  public int getCount()
  {
    return NotificationsPage.c.length;
  }
  
  public CharSequence getPageTitle(int paramInt)
  {
    return NotificationsPage.c[paramInt].get(DAMobileApplication.getContext()).toUpperCase();
  }
  
  public boolean isViewFromObject(View paramView, Object paramObject)
  {
    return paramView == paramObject;
  }
}
