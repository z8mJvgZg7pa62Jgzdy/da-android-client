package com.deviantart.android.damobile.view.viewpageindicator;

import com.viewpagerindicator.IconPagerAdapter;

public abstract interface DAIconPagerAdapter
  extends IconPagerAdapter
{
  public abstract CharSequence b(int paramInt);
  
  public abstract CharSequence getPageTitle(int paramInt);
}
