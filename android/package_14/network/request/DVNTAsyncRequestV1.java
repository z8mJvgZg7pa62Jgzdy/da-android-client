package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.sdk.api.network.service.DVNTServiceV1;

public abstract class DVNTAsyncRequestV1<T>
  extends com.deviantart.android.sdk.api.network.request.DVNTBaseAsyncRequest<T, DVNTServiceV1>
{
  protected DVNTAsyncRequestV1(Class paramClass)
  {
    super(paramClass, DVNTServiceV1.class);
  }
}
