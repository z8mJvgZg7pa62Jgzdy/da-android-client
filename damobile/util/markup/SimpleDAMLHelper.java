package com.deviantart.android.damobile.util.markup;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTStashItem;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StaticStreamLoader;
import com.facebook.drawee.view.GenericDraweeView;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;
import java.util.Iterator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

public class SimpleDAMLHelper
{
  public SimpleDAMLHelper() {}
  
  private static boolean a(Activity paramActivity, SimpleDAMLContent paramSimpleDAMLContent, SimpleDraweeView paramSimpleDraweeView)
  {
    StaticStreamLoader localStaticStreamLoader = new StaticStreamLoader(StaticStreamLoader.K + paramSimpleDAMLContent.hashCode());
    Stream localStream = StreamCacher.a(localStaticStreamLoader);
    localStaticStreamLoader.a(paramSimpleDAMLContent.e());
    if (localStream.size() <= 0) {
      return false;
    }
    return MarkupHelper.b(paramActivity, paramSimpleDraweeView, (DVNTDeviation)localStream.get(0), localStream, 0);
  }
  
  private static boolean b(Activity paramActivity, SimpleDAMLContent paramSimpleDAMLContent, SimpleDraweeView paramSimpleDraweeView)
  {
    if (!paramSimpleDAMLContent.b()) {
      return false;
    }
    paramSimpleDAMLContent = paramSimpleDAMLContent.getResources();
    if ((paramSimpleDAMLContent != null) && (!paramSimpleDAMLContent.isEmpty())) {
      return MarkupHelper.a(paramActivity, paramSimpleDraweeView, (DVNTStashItem)paramSimpleDAMLContent.get(0));
    }
    return false;
  }
  
  private static boolean b(Activity paramActivity, SimpleDAMLContent paramSimpleDAMLContent, SimpleDraweeView paramSimpleDraweeView, TextView paramTextView)
  {
    if (!paramSimpleDAMLContent.b()) {
      return false;
    }
    paramSimpleDraweeView.setVisibility(8);
    paramTextView.setText(2131231216);
    paramTextView.setVisibility(0);
    return true;
  }
  
  public static void clear(Activity paramActivity, SimpleDAMLContent paramSimpleDAMLContent, SimpleDraweeView paramSimpleDraweeView, TextView paramTextView)
  {
    paramSimpleDraweeView.setVisibility(8);
    if (a(paramActivity, paramSimpleDAMLContent, paramSimpleDraweeView)) {
      return;
    }
    if ((!b(paramActivity, paramSimpleDAMLContent, paramSimpleDraweeView)) && (!d(paramActivity, paramSimpleDAMLContent, paramSimpleDraweeView)) && (!b(paramActivity, paramSimpleDAMLContent, paramSimpleDraweeView, paramTextView)))
    {
      paramTextView = Jsoup.get(paramSimpleDAMLContent.c());
      paramSimpleDAMLContent = paramTextView;
      if (!DVNTAbstractAsyncAPI.getConfig().getShowMatureContent().booleanValue()) {
        paramSimpleDAMLContent = MarkupHelper.onPostExecute(paramTextView);
      }
      if (!update(paramActivity, paramSimpleDAMLContent, paramSimpleDraweeView)) {
        onPostExecute(paramActivity, paramSimpleDAMLContent, paramSimpleDraweeView);
      }
    }
  }
  
  private static boolean d(Activity paramActivity, SimpleDAMLContent paramSimpleDAMLContent, SimpleDraweeView paramSimpleDraweeView)
  {
    StaticStreamLoader localStaticStreamLoader = new StaticStreamLoader(StaticStreamLoader.H + paramSimpleDAMLContent.hashCode());
    Stream localStream = StreamCacher.a(localStaticStreamLoader);
    localStaticStreamLoader.a(paramSimpleDAMLContent.a());
    if (localStream.size() <= 0) {
      return false;
    }
    return MarkupHelper.b(paramActivity, paramSimpleDraweeView, (DVNTDeviation)localStream.get(0), localStream, 0);
  }
  
  private static void onPostExecute(Activity paramActivity, Document paramDocument, SimpleDraweeView paramSimpleDraweeView)
  {
    paramDocument = paramDocument.select("span[data-embed-type=deviation]");
    if (paramDocument.isEmpty()) {
      return;
    }
    Iterator localIterator1 = paramDocument.iterator();
    label60:
    String str2;
    do
    {
      do
      {
        break label60;
        Iterator localIterator2;
        while (!localIterator2.hasNext())
        {
          do
          {
            if (!localIterator1.hasNext()) {
              break;
            }
            paramDocument = ((Element)localIterator1.next()).getElementsByTag("a");
          } while (paramDocument.isEmpty());
          localIterator2 = paramDocument.iterator();
        }
        paramDocument = (Element)localIterator2.next();
      } while ((paramDocument.attr("class") == null) || (paramDocument.attr("class").contains("lit")) || (!paramDocument.attr("class").contains("thumb")));
      str2 = paramDocument.attr("href");
      paramDocument = paramDocument.getElementsByTag("img");
    } while (paramDocument.isEmpty());
    Iterator localIterator3 = paramDocument.iterator();
    while (localIterator3.hasNext())
    {
      Element localElement = (Element)localIterator3.next();
      String str1 = localElement.attr("src");
      try
      {
        paramDocument = Integer.valueOf(localElement.attr("width"));
        localInteger = Integer.valueOf(localElement.attr("height"));
        if ((str1 == null) || (str1.isEmpty()))
        {
          str1 = localElement.attr("data-src");
          if ((str1 == null) || (str1.isEmpty())) {
            continue;
          }
          if ((paramDocument != null) && (localInteger != null))
          {
            paramSimpleDraweeView.setAspectRatio(paramDocument.intValue() / localInteger.intValue());
            paramSimpleDraweeView.setVisibility(0);
            MarkupHelper.replaceAll(paramActivity, paramSimpleDraweeView, str2);
            if (!MarkupHelper.update(paramActivity, paramSimpleDraweeView, str1)) {
              continue;
            }
            if ((str2 == null) || (!str2.matches("DeviantArt(\\-(?:Preview|RC))?://.+"))) {
              return;
            }
            paramSimpleDraweeView.setOnClickListener(new SimpleDAMLHelper.2(str2, paramActivity));
            return;
          }
        }
      }
      catch (NumberFormatException paramDocument)
      {
        for (;;)
        {
          Integer localInteger = null;
          paramDocument = null;
          continue;
          paramSimpleDraweeView.setAspectRatio(1.33F);
        }
      }
    }
  }
  
  private static boolean update(Activity paramActivity, Document paramDocument, SimpleDraweeView paramSimpleDraweeView)
  {
    paramDocument = paramDocument.select("a[class^=embedded-deviation]");
    if (paramDocument.isEmpty()) {
      return false;
    }
    Iterator localIterator1 = paramDocument.iterator();
    Object localObject1;
    Object localObject2;
    String str;
    while (localIterator1.hasNext())
    {
      localObject1 = (Element)localIterator1.next();
      localObject2 = ((Element)localObject1).getElementsByTag("img");
      if (!((Elements)localObject2).isEmpty()) {
        str = ((Node)localObject1).attr("href");
      }
    }
    for (;;)
    {
      try
      {
        paramDocument = Integer.valueOf(((Node)localObject1).attr("data-fullview-width"));
        localObject1 = Integer.valueOf(((Node)localObject1).attr("data-fullview-height"));
        Iterator localIterator2 = ((Elements)localObject2).iterator();
        if (!localIterator2.hasNext()) {
          break label224;
        }
        localObject2 = (Element)localIterator2.next();
        if (localObject2 == null) {
          continue;
        }
        if (localObject2 == null) {
          break;
        }
        if ((str != null) && (str.matches("DeviantArt(\\-(?:Preview|RC))?://.+"))) {
          paramSimpleDraweeView.setOnClickListener(new SimpleDAMLHelper.1(str, paramActivity));
        }
        MarkupHelper.replaceAll(paramActivity, paramSimpleDraweeView, str);
        if ((paramDocument != null) && (localObject1 != null))
        {
          paramSimpleDraweeView.setAspectRatio(paramDocument.intValue() / ((Integer)localObject1).intValue());
          if (!MarkupHelper.update(paramActivity, paramSimpleDraweeView, ((Node)localObject2).attr("src"))) {
            break;
          }
          return true;
        }
      }
      catch (NumberFormatException paramDocument)
      {
        localObject1 = null;
        paramDocument = null;
        continue;
        paramSimpleDraweeView.setAspectRatio(1.33F);
        continue;
      }
      return false;
      label224:
      localObject2 = null;
    }
  }
}
