package com.deviantart.android.damobile.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.adapter.BasicFragmentPagerAdapter;
import com.deviantart.android.damobile.fragment.MultiStepFragment;
import com.deviantart.android.damobile.fragment.SignUpLoginInfoFragment;
import com.deviantart.android.damobile.fragment.SignUpPersonalInfoFragment;
import com.deviantart.android.damobile.fragment.SignUpRegistrationFragment;
import com.deviantart.android.damobile.view.NonSwipeViewPager;
import java.util.ArrayList;

public class SignUpActivity
  extends MultiStepActivity
{
  private BasicFragmentPagerAdapter adapter;
  private int index = 0;
  @Bind({2131689606})
  NonSwipeViewPager pager;
  
  public SignUpActivity() {}
  
  public void a()
  {
    super.a();
    if (index == adapter.getCount() - 1) {
      ((SignUpRegistrationFragment)adapter.getItem(index)).b();
    }
  }
  
  public void onBackPressed()
  {
    ((MultiStepFragment)adapter.getItem(index)).saveAndExit();
    if (index == 0)
    {
      finish();
      super.onBackPressed();
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968607);
    ButterKnife.bind(this);
    paramBundle = new ArrayList();
    paramBundle.add(new SignUpLoginInfoFragment());
    paramBundle.add(new SignUpPersonalInfoFragment());
    paramBundle.add(new SignUpRegistrationFragment());
    adapter = new BasicFragmentPagerAdapter(getFragmentManager(), paramBundle);
    pager.setAdapter(adapter);
  }
  
  public void onLoadFinished(String paramString)
  {
    index = 0;
    pager.setCurrentItem(index);
    if (paramString == null)
    {
      Toast.makeText(this, 2131231337, 1).show();
      return;
    }
    Toast.makeText(this, paramString, 1).show();
  }
  
  public void onOptionsItemSelected()
  {
    Bundle localBundle = ((MultiStepFragment)adapter.getItem(index)).updateSettings();
    if (localBundle == null)
    {
      Log.e("SignUp", "Null fragment args");
      return;
    }
    if (index < adapter.getCount() - 1) {
      index += 1;
    }
    ((MultiStepFragment)adapter.getItem(index)).b(localBundle);
    pager.setCurrentItem(index);
  }
  
  public void showImage()
  {
    if (index == 0)
    {
      super.onBackPressed();
      return;
    }
    index -= 1;
    pager.setCurrentItem(index);
  }
}
