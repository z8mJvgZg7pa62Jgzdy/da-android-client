package com.deviantart.android.damobile.util.discovery.pages;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.discovery.DiscoveryPageInfo;
import com.deviantart.android.damobile.util.torpedo.TorpedoAdapter;
import com.deviantart.android.damobile.util.torpedo.TorpedoItemDecoration;
import com.deviantart.android.damobile.util.torpedo.TorpedoLayout;
import com.deviantart.android.damobile.util.torpedo.TorpedoRecyclerView;
import com.deviantart.android.damobile.util.torpedo.TorpedoUtils.TorpedoEndlessScrollListener;
import com.deviantart.android.damobile.util.torpedo.TorpedoUtils.TorpedoNotifiable;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil.ScrollEventHelper;

public class DiscoveryTorpedoPage
  extends DiscoveryPage
{
  private String E;
  private TorpedoLayout a;
  private TorpedoAdapter b;
  protected Class c;
  protected String d;
  private EventKeys.Category j;
  private String l;
  private String p;
  
  public DiscoveryTorpedoPage(Activity paramActivity, String paramString1, String paramString2, String paramString3, Class paramClass, String paramString4)
  {
    super(paramString1, paramString2, paramString3);
    c = paramClass;
    d = paramString4;
  }
  
  public DiscoveryTorpedoPage(Context paramContext, DiscoveryPageInfo paramDiscoveryPageInfo, String paramString, boolean paramBoolean, Class paramClass)
  {
    this(paramDiscoveryPageInfo.getString(), paramDiscoveryPageInfo.get(paramContext), paramString, paramBoolean, paramClass);
  }
  
  public DiscoveryTorpedoPage(Context paramContext, DiscoveryPageInfo paramDiscoveryPageInfo, String paramString1, boolean paramBoolean, Class paramClass, String paramString2)
  {
    this(paramDiscoveryPageInfo.getString(), paramDiscoveryPageInfo.get(paramContext), paramString1, paramBoolean, paramClass, paramString2);
  }
  
  public DiscoveryTorpedoPage(String paramString1, String paramString2, String paramString3, Class paramClass)
  {
    super(paramString1, paramString2, paramString3);
    c = paramClass;
  }
  
  public DiscoveryTorpedoPage(String paramString1, String paramString2, String paramString3, boolean paramBoolean, Class paramClass)
  {
    super(paramString1, paramString2, paramString3, paramBoolean);
    c = paramClass;
  }
  
  public DiscoveryTorpedoPage(String paramString1, String paramString2, String paramString3, boolean paramBoolean, Class paramClass, String paramString4)
  {
    super(paramString1, paramString2, paramString3, paramBoolean);
    c = paramClass;
    d = paramString4;
  }
  
  private void b(TorpedoRecyclerView paramTorpedoRecyclerView, Stream paramStream)
  {
    b = new TorpedoAdapter(paramTorpedoRecyclerView, paramStream);
    b.a(k);
    b.a(E);
    if (next()) {
      b.a(l, p);
    }
  }
  
  private boolean next()
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(get());
    return (l != null) && (localSharedPreferences.getBoolean(p, true));
  }
  
  public View a(ViewGroup paramViewGroup)
  {
    super.a(paramViewGroup);
    int k = get().getResources().getInteger(2131492889);
    Object localObject = a();
    int i;
    if ((a == null) || (!StreamCacher.b((StreamLoader)localObject)))
    {
      i = 1;
      localObject = StreamCacher.a((StreamLoader)localObject, StreamCacheStrategy.b);
      if (i == 0) {
        break label360;
      }
      a = ((TorpedoLayout)LayoutInflater.from(get()).inflate(2130968698, paramViewGroup, false));
      ((TorpedoRecyclerView)a.findViewById(2131690078)).addItemDecoration(new TorpedoItemDecoration(get()));
      a.setProgressViewOffset(this.k);
      b = null;
      i = 1;
    }
    label148:
    label342:
    label360:
    for (;;)
    {
      paramViewGroup = (TorpedoRecyclerView)a.findViewById(2131690078);
      if (b == null)
      {
        b(paramViewGroup, (Stream)localObject);
        if (i == 0) {
          break label342;
        }
        paramViewGroup.setAdapter(b);
      }
      for (;;)
      {
        TorpedoUtils.TorpedoNotifiable localTorpedoNotifiable = new TorpedoUtils.TorpedoNotifiable(a);
        if (((Stream)localObject).size() <= 0) {
          ((Stream)localObject).a(get(), localTorpedoNotifiable);
        }
        TorpedoUtils.TorpedoEndlessScrollListener localTorpedoEndlessScrollListener = new TorpedoUtils.TorpedoEndlessScrollListener((Stream)localObject, get());
        localTorpedoEndlessScrollListener.b(localTorpedoNotifiable);
        paramViewGroup.b(k, localTorpedoEndlessScrollListener);
        a.setRefreshListener(new DiscoveryTorpedoPage.1(this, localTorpedoNotifiable, (Stream)localObject));
        if (j != null)
        {
          paramViewGroup.setCategory(j);
          paramViewGroup.setScrollEventHelper(new TrackerUtil.ScrollEventHelper(new Integer[] { Integer.valueOf(15), Integer.valueOf(30), Integer.valueOf(45), Integer.valueOf(60), Integer.valueOf(75), Integer.valueOf(90) }));
        }
        return a;
        i = 0;
        break;
        b.b(paramViewGroup, (Stream)localObject);
        break label148;
        b.b();
        a.setIsRefreshing(false);
      }
    }
  }
  
  public StreamLoader a()
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: fail exe a2 = a1\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.exec(BaseAnalyze.java:92)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.exec(BaseAnalyze.java:1)\n\tat com.googlecode.dex2jar.ir.ts.Cfg.dfs(Cfg.java:255)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.analyze0(BaseAnalyze.java:75)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.analyze(BaseAnalyze.java:69)\n\tat com.googlecode.dex2jar.ir.ts.UnSSATransformer.transform(UnSSATransformer.java:274)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:163)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\nCaused by: java.lang.NullPointerException\n\tat com.googlecode.dex2jar.ir.ts.UnSSATransformer$LiveA.onUseLocal(UnSSATransformer.java:552)\n\tat com.googlecode.dex2jar.ir.ts.UnSSATransformer$LiveA.onUseLocal(UnSSATransformer.java:1)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.onUse(BaseAnalyze.java:166)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.onUse(BaseAnalyze.java:1)\n\tat com.googlecode.dex2jar.ir.ts.Cfg.travel(Cfg.java:331)\n\tat com.googlecode.dex2jar.ir.ts.Cfg.travel(Cfg.java:387)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.exec(BaseAnalyze.java:90)\n\t... 17 more\n");
  }
  
  public void a(EventKeys.Category paramCategory)
  {
    j = paramCategory;
  }
  
  public void a(String paramString)
  {
    E = paramString;
  }
  
  public void b()
  {
    super.b();
    b = null;
    a = null;
  }
  
  public void c(String paramString)
  {
    l = paramString;
  }
  
  public void d(String paramString)
  {
    p = paramString;
  }
}
