package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTStats
  implements Serializable
{
  Integer comments;
  Integer downloads;
  @SerializedName("downloads_today")
  Integer downloadsToday;
  Integer favourites;
  Integer views;
  @SerializedName("views_today")
  Integer viewsToday;
  
  public DVNTStats() {}
  
  public Integer getComments()
  {
    return comments;
  }
  
  public Integer getDownloads()
  {
    return downloads;
  }
  
  public Integer getDownloadsToday()
  {
    return downloadsToday;
  }
  
  public Integer getFavourites()
  {
    return favourites;
  }
  
  public Integer getViews()
  {
    return views;
  }
  
  public Integer getViewsToday()
  {
    return viewsToday;
  }
  
  public void setComments(Integer paramInteger)
  {
    comments = paramInteger;
  }
  
  public void setDownloads(Integer paramInteger)
  {
    downloads = paramInteger;
  }
  
  public void setDownloadsToday(Integer paramInteger)
  {
    downloadsToday = paramInteger;
  }
  
  public void setFavourites(Integer paramInteger)
  {
    favourites = paramInteger;
  }
  
  public void setViews(Integer paramInteger)
  {
    views = paramInteger;
  }
  
  public void setViewsToday(Integer paramInteger)
  {
    viewsToday = paramInteger;
  }
}
