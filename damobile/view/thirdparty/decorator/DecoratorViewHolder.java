package com.deviantart.android.damobile.view.thirdparty.decorator;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;

public abstract class DecoratorViewHolder
  extends RecyclerView.ViewHolder
{
  private RecyclerView.ViewHolder o;
  
  public DecoratorViewHolder(RecyclerView.ViewHolder paramViewHolder, View paramView)
  {
    super(paramView);
    o = paramViewHolder;
  }
  
  public RecyclerView.ViewHolder c()
  {
    return o;
  }
}
