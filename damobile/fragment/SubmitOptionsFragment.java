package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.activity.SubmitActivity;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.submit.Submission;
import com.deviantart.android.damobile.util.submit.SubmitLicenseOptions;
import com.deviantart.android.damobile.util.submit.SubmitOptions;
import com.deviantart.android.damobile.util.submit.SubmitType;

public class SubmitOptionsFragment
  extends DABaseFragment
{
  @Bind({2131689821})
  SwitchCompat addWatermarkSwitch;
  @Bind({2131689820})
  SwitchCompat allowCommentsSwitch;
  @Bind({2131689822})
  SwitchCompat allowFreeDownloadSwitch;
  @Bind({2131689825})
  TextView licenseValue;
  @Bind({2131689826})
  SwitchCompat rememberSettingsSwitch;
  private SubmitOptions viewer;
  
  public SubmitOptionsFragment() {}
  
  public void onAddWatermarkCheckedChanged(boolean paramBoolean)
  {
    if (paramBoolean) {
      allowFreeDownloadSwitch.setChecked(false);
    }
    viewer.setPinned(Boolean.valueOf(paramBoolean));
  }
  
  public void onAllowCommentsCheckedChanged(boolean paramBoolean)
  {
    viewer.setUnread(Boolean.valueOf(paramBoolean));
  }
  
  public void onAllowFreeDownloadCheckedChanged(boolean paramBoolean)
  {
    if (paramBoolean) {
      addWatermarkSwitch.setChecked(false);
    }
    viewer.setSystem(Boolean.valueOf(paramBoolean));
  }
  
  public void onClickLicenseBlock()
  {
    ScreenFlowManager.a(getActivity(), new SubmitOptionsLicenseFragment(), "licenseOptions fragment");
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = (ScrollView)paramLayoutInflater.inflate(2130968685, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    paramViewGroup = Submission.getInstance();
    viewer = paramViewGroup.getList();
    paramViewGroup.setTitle(true);
    if (SubmitType.a.equals(Submission.getInstance().getString()))
    {
      addWatermarkSwitch.setVisibility(0);
      addWatermarkSwitch.setChecked(viewer.isPinned().booleanValue());
    }
    for (;;)
    {
      allowCommentsSwitch.setChecked(viewer.isVisible().booleanValue());
      allowFreeDownloadSwitch.setChecked(viewer.getSize().booleanValue());
      rememberSettingsSwitch.setChecked(viewer.get().booleanValue());
      ((SubmitActivity)getActivity()).getSupportActionBar().setTitle(2131231353);
      return paramLayoutInflater;
      addWatermarkSwitch.setVisibility(8);
      allowFreeDownloadSwitch.setVisibility(8);
    }
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public void onRememberSettingsCheckedChanged(boolean paramBoolean)
  {
    viewer.setIs24HourView(Boolean.valueOf(paramBoolean));
  }
  
  public void onResume()
  {
    super.onResume();
    if (viewer.getString().equals())
    {
      licenseValue.setText(getString(2131230842));
      return;
    }
    licenseValue.setText(getString(2131230851));
  }
}
