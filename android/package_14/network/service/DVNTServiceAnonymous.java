package com.deviantart.android.android.package_14.network.service;

import com.deviantart.android.android.package_14.model.DVNTAccessToken;

public abstract interface DVNTServiceAnonymous
{
  public abstract DVNTAccessToken login(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5);
}
