package com.deviantart.android.damobile.util.notifications;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTCollection;
import com.deviantart.android.android.package_14.model.DVNTComment;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessage;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessageStack;
import com.deviantart.android.android.package_14.model.DVNTFeedbackSubject;
import com.deviantart.android.android.package_14.model.DVNTGallection;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.package_14.model.DVNTUserStatus;
import com.deviantart.android.damobile.fragment.DeviationFullViewFragment;
import com.deviantart.android.damobile.fragment.DeviationFullViewFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.HomeBaseFragment.HomeFragmentInstanceBuilder;
import com.deviantart.android.damobile.fragment.NotificationsRollUpFragment;
import com.deviantart.android.damobile.fragment.StatusFullViewFragment;
import com.deviantart.android.damobile.fragment.StatusFullViewFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.UserProfileFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.UserProfileFragment.UserProfileTab;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StaticStreamLoader;
import com.deviantart.android.damobile.util.CommentUtils;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.deviation.DeviationPanelTab;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;

public class NotificationItemClick
{
  public NotificationItemClick() {}
  
  private static void a(Context paramContext, DVNTUserStatus paramDVNTUserStatus)
  {
    if ((paramDVNTUserStatus == null) || (paramDVNTUserStatus.isDeleted().booleanValue()))
    {
      Toast.makeText(paramContext, paramContext.getString(2131230910), 0).show();
      return;
    }
    StatusFullViewFragment localStatusFullViewFragment = (StatusFullViewFragment)new StatusFullViewFragment.InstanceBuilder().b(paramDVNTUserStatus).a();
    ScreenFlowManager.a((Activity)paramContext, localStatusFullViewFragment, "status_full_view" + paramDVNTUserStatus.getStatusId());
  }
  
  private static void a(Context paramContext, String paramString, UserProfileFragment.UserProfileTab paramUserProfileTab)
  {
    UserUtils.c((Activity)paramContext, paramString, paramUserProfileTab);
  }
  
  private static void a(Context paramContext, String paramString1, NotificationItemType paramNotificationItemType, String paramString2)
  {
    if (paramString1 == null) {
      return;
    }
    TrackerUtil.get((Activity)paramContext, EventKeys.Category.i, "TapStack", paramString2);
    ScreenFlowManager.add((Activity)paramContext, NotificationsRollUpFragment.getItem(paramString1, paramNotificationItemType), "notification_rollup_" + paramString1, true, true);
  }
  
  private static void b(Context paramContext, DVNTDeviation paramDVNTDeviation, DVNTComment paramDVNTComment)
  {
    if ((paramDVNTDeviation == null) || (paramDVNTDeviation.isDeleted().booleanValue()))
    {
      Toast.makeText(paramContext, paramContext.getString(2131230909), 0).show();
      return;
    }
    if (CommentUtils.i(paramDVNTComment, paramDVNTDeviation.getAuthor().getUserName()))
    {
      delete(paramContext);
      return;
    }
    Stream localStream = StreamCacher.a(new StaticStreamLoader("staticstreamloader-deviation-" + paramDVNTDeviation.getId()));
    if (!localStream.equals())
    {
      localStream.close(paramDVNTDeviation);
      localStream.c();
    }
    paramDVNTComment = (DeviationFullViewFragment)new DeviationFullViewFragment.InstanceBuilder().a(false).a(localStream).a(DeviationPanelTab.i).a(paramDVNTComment.getCommentId()).a();
    ScreenFlowManager.a((Activity)paramContext, paramDVNTComment, "deviation_fullview_single" + paramDVNTDeviation.getId());
  }
  
  private static void b(Context paramContext, DVNTDeviation paramDVNTDeviation, DeviationPanelTab paramDeviationPanelTab)
  {
    if ((paramDVNTDeviation == null) || (paramDVNTDeviation.isDeleted().booleanValue()))
    {
      Toast.makeText(paramContext, paramContext.getString(2131230909), 0).show();
      return;
    }
    Stream localStream = StreamCacher.a(new StaticStreamLoader("staticstreamloader-deviation-" + paramDVNTDeviation.getId()));
    if (!localStream.equals())
    {
      localStream.close(paramDVNTDeviation);
      localStream.c();
    }
    paramDeviationPanelTab = (DeviationFullViewFragment)new DeviationFullViewFragment.InstanceBuilder().a(false).a(localStream).a(paramDeviationPanelTab).a();
    ScreenFlowManager.a((Activity)paramContext, paramDeviationPanelTab, "deviation_fullview_single" + paramDVNTDeviation.getId());
  }
  
  private static void b(Context paramContext, DVNTUserStatus paramDVNTUserStatus, DVNTComment paramDVNTComment)
  {
    if ((paramDVNTUserStatus == null) || (paramDVNTUserStatus.isDeleted().booleanValue()))
    {
      Toast.makeText(paramContext, paramContext.getString(2131230910), 0).show();
      return;
    }
    if (CommentUtils.i(paramDVNTComment, paramDVNTUserStatus.getAuthor().getUserName()))
    {
      delete(paramContext);
      return;
    }
    paramDVNTComment = (StatusFullViewFragment)new StatusFullViewFragment.InstanceBuilder().b(paramDVNTUserStatus).a(paramDVNTComment.getCommentId()).a();
    ScreenFlowManager.a((Activity)paramContext, paramDVNTComment, "status_full_view" + paramDVNTUserStatus.getStatusId());
  }
  
  public static void c(Context paramContext, NotificationItemData paramNotificationItemData)
  {
    NotificationItemType localNotificationItemType = paramNotificationItemData.b();
    if (localNotificationItemType.b())
    {
      a(paramContext, paramNotificationItemData.getString().getStackId(), paramNotificationItemData.b(), paramNotificationItemData.c());
      return;
    }
    TrackerUtil.get((Activity)paramContext, EventKeys.Category.i, "TapItem", paramNotificationItemData.c());
    switch (NotificationItemClick.1.a[localNotificationItemType.ordinal()])
    {
    default: 
      Log.e("NotificationItem", "Unhandled behaviour for type " + localNotificationItemType);
      return;
    case 1: 
      b(paramContext, paramNotificationItemData.a().getSubject().getSubjectDeviation(), null);
      return;
    case 2: 
      start(paramContext, paramNotificationItemData.a().getSubject().getSubjectDeviation(), paramNotificationItemData.a().getOriginator(), paramNotificationItemData.a().getCollection());
      return;
    case 3: 
      b(paramContext, paramNotificationItemData.a().getSubject().getSubjectDeviation(), paramNotificationItemData.a().getComment());
      return;
    case 4: 
      c(paramContext, paramNotificationItemData.a().getSubject().getSubjectProfile().getUserName(), paramNotificationItemData.a().getComment());
      return;
    case 5: 
      b(paramContext, paramNotificationItemData.a().getSubject().getSubjectStatus(), paramNotificationItemData.a().getComment());
      return;
    case 6: 
      b(paramContext, paramNotificationItemData.a().getDeviation(), paramNotificationItemData.a().getComment());
      return;
    case 7: 
    case 8: 
      c(paramContext, paramNotificationItemData.a().getProfile().getUserName(), paramNotificationItemData.a().getComment());
      return;
    case 9: 
    case 10: 
      b(paramContext, paramNotificationItemData.a().getStatus(), paramNotificationItemData.a().getComment());
      return;
    case 11: 
      b(paramContext, paramNotificationItemData.a().getDeviation(), paramNotificationItemData.a().getComment());
      return;
    case 12: 
      c(paramContext, paramNotificationItemData.a().getProfile().getUserName(), paramNotificationItemData.a().getComment());
      return;
    case 13: 
      b(paramContext, paramNotificationItemData.a().getStatus(), paramNotificationItemData.a().getComment());
      return;
    case 14: 
      a(paramContext, paramNotificationItemData.a().getStatus());
      return;
    case 15: 
      b(paramContext, paramNotificationItemData.a().getDeviation(), DeviationPanelTab.e);
      return;
    case 16: 
      b(paramContext, paramNotificationItemData.a().getDeviation(), paramNotificationItemData.a().getComment());
      return;
    case 17: 
      c(paramContext, paramNotificationItemData.a().getProfile().getUserName(), paramNotificationItemData.a().getComment());
      return;
    case 18: 
      b(paramContext, paramNotificationItemData.a().getStatus(), paramNotificationItemData.a().getComment());
      return;
    case 19: 
      b(paramContext, paramNotificationItemData.a().getDeviation(), DeviationPanelTab.e);
      return;
    case 20: 
      a(paramContext, paramNotificationItemData.a().getStatus());
      return;
    case 21: 
      a(paramContext, paramNotificationItemData.a().getOriginator().getUserName(), null);
      return;
    case 22: 
      b(paramContext, paramNotificationItemData.a().getDeviation(), null);
      return;
    }
    b(paramContext, paramNotificationItemData.a().getDeviation(), null);
  }
  
  private static void c(Context paramContext, String paramString, DVNTComment paramDVNTComment)
  {
    if (CommentUtils.i(paramDVNTComment, paramString))
    {
      delete(paramContext);
      return;
    }
    paramContext = (Activity)paramContext;
    UserProfileFragment.InstanceBuilder localInstanceBuilder = new UserProfileFragment.InstanceBuilder().a(paramString);
    if ((UserUtils.a != null) && (UserUtils.a.equals(paramString))) {}
    for (boolean bool = true;; bool = false)
    {
      ScreenFlowManager.a(paramContext, (Fragment)localInstanceBuilder.a(bool).b(paramDVNTComment.getCommentId()).a(), "user_profile" + paramString);
      return;
    }
  }
  
  private static void delete(Context paramContext)
  {
    Toast.makeText(paramContext, paramContext.getString(2131230828), 0).show();
  }
  
  private static void start(Context paramContext, DVNTDeviation paramDVNTDeviation, DVNTUser paramDVNTUser, DVNTCollection paramDVNTCollection)
  {
    if ((paramDVNTDeviation == null) || (paramDVNTDeviation.isDeleted().booleanValue()))
    {
      Toast.makeText(paramContext, paramContext.getString(2131230909), 0).show();
      return;
    }
    NavigationUtils.e((Activity)paramContext, paramDVNTUser.getUserName(), paramDVNTCollection.getFolderId(), paramDVNTCollection.getName(), true);
  }
}
