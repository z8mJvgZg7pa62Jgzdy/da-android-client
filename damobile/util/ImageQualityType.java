package com.deviantart.android.damobile.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.ArrayList;

public enum ImageQualityType
{
  private int block;
  private int index;
  
  static
  {
    c = new ImageQualityType("HIGH", 1, 2131231016, 0);
    b = new ImageQualityType("MEDIUM", 2, 2131231018, 1);
    d = new ImageQualityType("LOW", 3, 2131231017, 2);
  }
  
  private ImageQualityType(int paramInt1, int paramInt2)
  {
    index = paramInt1;
    block = paramInt2;
  }
  
  public static void delete(Context paramContext, ImageQualityType paramImageQualityType)
  {
    paramContext.getSharedPreferences(UserUtils.a, 0).edit().putInt("photo_upload_quality", paramImageQualityType.get()).apply();
  }
  
  public static ImageQualityType findByName(Context paramContext, String paramString)
  {
    ImageQualityType[] arrayOfImageQualityType = values();
    int j = arrayOfImageQualityType.length;
    int i = 0;
    while (i < j)
    {
      ImageQualityType localImageQualityType = arrayOfImageQualityType[i];
      if (localImageQualityType.getTitle(paramContext).equals(paramString)) {
        return localImageQualityType;
      }
      i += 1;
    }
    return null;
  }
  
  public static ImageQualityType get(Context paramContext)
  {
    return getString(paramContext.getSharedPreferences(UserUtils.a, 0).getInt("photo_upload_quality", 0));
  }
  
  public static ImageQualityType getString(int paramInt)
  {
    ImageQualityType[] arrayOfImageQualityType = values();
    int j = arrayOfImageQualityType.length;
    int i = 0;
    while (i < j)
    {
      ImageQualityType localImageQualityType = arrayOfImageQualityType[i];
      if (localImageQualityType.get() == paramInt) {
        return localImageQualityType;
      }
      i += 1;
    }
    return s;
  }
  
  public static CharSequence[] getTitles(Context paramContext)
  {
    ArrayList localArrayList = new ArrayList();
    ImageQualityType[] arrayOfImageQualityType = values();
    int j = arrayOfImageQualityType.length;
    int i = 0;
    while (i < j)
    {
      localArrayList.add(arrayOfImageQualityType[i].getTitle(paramContext));
      i += 1;
    }
    return (CharSequence[])localArrayList.toArray(new CharSequence[localArrayList.size()]);
  }
  
  public int get()
  {
    return block;
  }
  
  public String getTitle(Context paramContext)
  {
    return paramContext.getString(index);
  }
}
