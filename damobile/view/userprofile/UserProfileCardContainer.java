package com.deviantart.android.damobile.view.userprofile;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.PhotoUtils;
import com.deviantart.android.damobile.util.PhotoUtils.PictureType;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.view.NonSwipeViewPager;
import com.deviantart.android.damobile.view.viewpageindicator.UserProfileCardIndicator;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;

public class UserProfileCardContainer
  extends FrameLayout
{
  private boolean a;
  private boolean i = false;
  @Bind({2131690095})
  UserProfileCardIndicator indicator;
  @Bind({2131690093})
  ProgressBar loader;
  private ArrayList<UserProfileCardContainer.UserProfileCard> m = new ArrayList();
  @Bind({2131690094})
  NonSwipeViewPager pager;
  private UserProfileCardContainer.ProfileCardAdapter pagerAdapter;
  @Bind({2131689871})
  SimpleDraweeView profilePic;
  private String s;
  private UserCardData show;
  
  public UserProfileCardContainer(Context paramContext)
  {
    super(paramContext);
    initLayout(paramContext);
  }
  
  public UserProfileCardContainer(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initLayout(paramContext);
  }
  
  private void a()
  {
    m.clear();
    m.add(UserProfileCardContainer.UserProfileCard.a);
    m.add(UserProfileCardContainer.UserProfileCard.c);
    if ((!TextUtils.isEmpty(show.getId())) || (!TextUtils.isEmpty(show.getOverview()))) {
      m.add(UserProfileCardContainer.UserProfileCard.b);
    }
    m.add(UserProfileCardContainer.UserProfileCard.g);
  }
  
  private void bindView()
  {
    pager.setVisibility(8);
    indicator.setVisibility(8);
    loader.setVisibility(0);
  }
  
  private void initLayout(Context paramContext)
  {
    LayoutInflater.from(paramContext).inflate(2130968790, this, true);
    ButterKnife.bind(this);
    pager.setDisableSwipe(false);
    bindView();
  }
  
  private void setProfilePicVisibility(int paramInt)
  {
    if (paramInt > 0)
    {
      profilePic.setVisibility(0);
      return;
    }
    profilePic.setVisibility(8);
  }
  
  public void d()
  {
    Uri localUri = null;
    if ((a) && (UserUtils.d() != null))
    {
      i = true;
      localUri = Uri.fromFile(UserUtils.d());
    }
    while (localUri != null)
    {
      ImageUtils.isEmpty(profilePic, localUri);
      return;
      if (show.getLocation() != null)
      {
        i = true;
        s = show.getLocation();
        localUri = Uri.parse(show.getLocation());
      }
      else if (!UserUtils.get(show.format()))
      {
        localUri = Uri.parse(UserUtils.get());
      }
    }
    ImageUtils.a(profilePic, 2130837770);
  }
  
  public void dismiss()
  {
    pager.setVisibility(0);
    indicator.setVisibility(0);
    loader.setVisibility(8);
  }
  
  public void handleResult()
  {
    if (UserUtils.d() != null)
    {
      if (!a) {
        return;
      }
      ImageUtils.isEmpty(profilePic, Uri.fromFile(UserUtils.d()));
    }
  }
  
  public void onClickProfilePicture()
  {
    if (!a) {
      return;
    }
    PhotoUtils.a((Activity)getContext(), profilePic, i, s, PhotoUtils.PictureType.N);
  }
  
  public void onLoadFinished()
  {
    if (pagerAdapter == null)
    {
      Log.e("UserProfileCard", "Null adapter when view resumed.");
      return;
    }
    int j = pager.getCurrentItem();
    pager.setAdapter(pagerAdapter);
    pager.setCurrentItem(j);
  }
  
  public void onLoadFinished(UserCardData paramUserCardData, boolean paramBoolean)
  {
    a = paramBoolean;
    show = paramUserCardData;
    a();
    pagerAdapter = new UserProfileCardContainer.ProfileCardAdapter(this, null);
    pager.setAdapter(pagerAdapter);
    pager.setOffscreenPageLimit(3);
    indicator.setViewPager(pager);
    indicator.setRight(true);
    d();
    indicator.setOnPageChangeListener(new UserProfileCardContainer.1(this));
    setProfilePicVisibility(pager.getCurrentItem());
    dismiss();
  }
  
  public void setEnabled(boolean paramBoolean)
  {
    super.setEnabled(paramBoolean);
    NonSwipeViewPager localNonSwipeViewPager = pager;
    if (!paramBoolean) {}
    for (paramBoolean = true;; paramBoolean = false)
    {
      localNonSwipeViewPager.setDisableSwipe(paramBoolean);
      return;
    }
  }
}
