package com.deviantart.android.android.package_14.config;

import android.content.Context;
import com.deviantart.android.android.package_14.model.DVNTAccessToken;
import com.deviantart.android.android.package_14.model.DVNTEnrichedToken;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthHelper;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthPreferencesManager;
import com.deviantart.android.android.package_14.session.DVNTUserSessionManager;
import java.lang.ref.WeakReference;

public abstract class DVNTGraduateHandler
{
  private DVNTGraduateHandler.DVNTOnGraduationEndedListener listener;
  DVNTOAuthHelper oAuthHelper;
  DVNTOAuthPreferencesManager oAuthPreferencesManager;
  private WeakReference<Context> weakContext;
  
  public DVNTGraduateHandler() {}
  
  private Context getContext()
  {
    if (weakContext != null) {
      return (Context)weakContext.get();
    }
    return null;
  }
  
  protected final void cancel()
  {
    listener.onGraduationCancelled(getContext());
  }
  
  protected final void fail(String paramString)
  {
    listener.onGraduationFailed(getContext(), paramString);
  }
  
  public DVNTOAuthPreferencesManager getoAuthPreferencesManager()
  {
    return oAuthPreferencesManager;
  }
  
  public abstract void graduate(Context paramContext);
  
  public void graduateWithToken(Context paramContext, DVNTEnrichedToken paramDVNTEnrichedToken)
  {
    weakContext = new WeakReference(paramContext);
    successWithToken(paramDVNTEnrichedToken);
  }
  
  public final void handle(Context paramContext)
  {
    weakContext = new WeakReference(paramContext);
    graduate(getContext());
  }
  
  public final void init(DVNTOAuthPreferencesManager paramDVNTOAuthPreferencesManager, DVNTOAuthHelper paramDVNTOAuthHelper, DVNTUserSessionManager paramDVNTUserSessionManager)
  {
    listener = paramDVNTUserSessionManager;
    oAuthPreferencesManager = paramDVNTOAuthPreferencesManager;
    oAuthHelper = paramDVNTOAuthHelper;
  }
  
  public abstract void onGraduateCancelled(Context paramContext);
  
  public abstract void onGraduationFail(Context paramContext, String paramString);
  
  public abstract void onGraduationSuccess(Context paramContext);
  
  public final void successWithAuthorizationCode(String paramString)
  {
    if (listener == null) {
      return;
    }
    listener.onGraduationSucceededdWithAuthorizationCode(getContext(), paramString);
  }
  
  public final void successWithToken(DVNTAccessToken paramDVNTAccessToken)
  {
    successWithToken(new DVNTEnrichedToken(paramDVNTAccessToken));
  }
  
  public final void successWithToken(DVNTEnrichedToken paramDVNTEnrichedToken)
  {
    if (listener == null) {
      return;
    }
    listener.onGraduationSucceededWithToken(getContext(), paramDVNTEnrichedToken);
  }
}
