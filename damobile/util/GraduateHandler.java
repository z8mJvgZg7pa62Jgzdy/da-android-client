package com.deviantart.android.damobile.util;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.util.Log;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.config.DVNTGraduateHandler;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.fragment.LoginDialogFragment;

public class GraduateHandler
  extends DVNTGraduateHandler
{
  private static final String CLASS_TAG = GraduateHandler.class.getSimpleName();
  private boolean d = false;
  
  public GraduateHandler() {}
  
  public void graduate(Context paramContext)
  {
    if (!DVNTContextUtils.isContextDead(paramContext))
    {
      if (d) {
        return;
      }
      d = false;
      LoginDialogFragment localLoginDialogFragment = new LoginDialogFragment();
      localLoginDialogFragment.setSongs(new GraduateHandler.1(this));
      paramContext = ((Activity)paramContext).getFragmentManager().beginTransaction();
      paramContext.add(localLoginDialogFragment, "login_fragment");
      paramContext.commitAllowingStateLoss();
    }
  }
  
  public void onGraduateCancelled(Context paramContext)
  {
    if (DVNTContextUtils.isContextDead(paramContext)) {
      return;
    }
    ((BaseActivity)paramContext).setCurrentTheme();
  }
  
  public void onGraduationFail(Context paramContext, String paramString)
  {
    Log.e(CLASS_TAG, "Graduation miserably failed: " + paramString);
  }
  
  public void onGraduationSuccess(Context paramContext)
  {
    if (DVNTContextUtils.isContextDead(paramContext)) {
      return;
    }
    ((BaseActivity)paramContext).a();
  }
  
  public void showInfoDialog(Activity paramActivity, String paramString1, String paramString2)
  {
    d = true;
    LoginDialogFragment localLoginDialogFragment = (LoginDialogFragment)paramActivity.getFragmentManager().findFragmentByTag("login_fragment");
    localLoginDialogFragment.updateTabStyles();
    localLoginDialogFragment.addTab(true);
    DVNTAsyncAPI.login(paramString1, paramString2, "1700", "9f66b7b5b0b2eaf9fa776c8ad3ec9428", "basic email push publish group daprivate user stash account browse browse.mlt collection comment.manage comment.post feed gallery message note user.manage deviation.manage challenge").call(paramActivity, new GraduateHandler.2(this, paramActivity, paramString1, localLoginDialogFragment));
  }
}
