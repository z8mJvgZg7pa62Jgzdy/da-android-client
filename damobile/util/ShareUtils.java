package com.deviantart.android.damobile.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

public class ShareUtils
{
  public ShareUtils() {}
  
  private static File create(String paramString, File paramFile)
  {
    String str;
    if (TextUtils.isEmpty(paramString))
    {
      str = "" + new Date().getTime();
      str = "SHARE_" + str + "_";
      paramString = FilenameUtils.getExtension(paramString);
      if (!TextUtils.isEmpty(paramString)) {
        break label90;
      }
    }
    label90:
    for (paramString = null;; paramString = "." + paramString)
    {
      return File.createTempFile(str, paramString, paramFile);
      str = FilenameUtils.getBaseName(paramString);
      break;
    }
  }
  
  public static Object fromJson(String paramString, Class paramClass)
  {
    return new Gson().fromJson(paramString, paramClass);
  }
  
  public static File get(Context paramContext, Uri paramUri)
  {
    File localFile = create(paramUri.getPath(), FileCache.getFile(paramContext));
    IOUtils.copy(paramContext.getContentResolver().openInputStream(paramUri), new FileOutputStream(localFile));
    return localFile;
  }
  
  public static String get(Object paramObject)
  {
    return new Gson().toJson(paramObject);
  }
  
  public static Intent getShareIntent(Context paramContext, DVNTDeviation paramDVNTDeviation)
  {
    Intent localIntent = new Intent();
    localIntent.setAction("android.intent.action.SEND");
    localIntent.putExtra("android.intent.extra.TEXT", DeviationUtils.e(paramDVNTDeviation));
    localIntent.setType("text/plain");
    return Intent.createChooser(localIntent, paramContext.getResources().getText(2131231334));
  }
}
