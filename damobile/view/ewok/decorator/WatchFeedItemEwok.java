package com.deviantart.android.damobile.view.ewok.decorator;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.deviantart.android.android.package_14.model.DVNTFeedItem;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.FeedItemType;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.MemberType;
import com.deviantart.android.damobile.util.ProcessMenuListener;
import com.deviantart.android.damobile.util.UserDisplay;
import com.facebook.drawee.view.GenericDraweeView;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;

public abstract class WatchFeedItemEwok
{
  protected ProcessMenuListener b;
  
  public WatchFeedItemEwok() {}
  
  protected void a(Activity paramActivity, View paramView, DVNTFeedItem paramDVNTFeedItem)
  {
    if ((paramDVNTFeedItem == null) || (paramDVNTFeedItem.getUser() == null))
    {
      paramView.findViewById(2131689617).setVisibility(8);
      return;
    }
    Object localObject2 = MemberType.toString(paramDVNTFeedItem.getUser().getType());
    if (localObject2 == null)
    {
      Log.e("WatchFeedItemEwok", "Found unhandled member type");
      paramView.findViewById(2131689617).setVisibility(8);
      return;
    }
    Object localObject1 = (SimpleDraweeView)paramView.findViewById(2131689703);
    SimpleDraweeView localSimpleDraweeView = (SimpleDraweeView)paramView.findViewById(2131689704);
    Uri localUri = Uri.parse(paramDVNTFeedItem.getUser().getUserIconURL());
    if (((MemberType)localObject2).b())
    {
      ImageUtils.isEmpty(localSimpleDraweeView, localUri);
      ((ImageView)localObject1).setVisibility(8);
      localSimpleDraweeView.setVisibility(0);
      ((View)localObject1).setOnClickListener(new WatchFeedItemEwok.1(this, (MemberType)localObject2, paramActivity, paramDVNTFeedItem.getUser().getUserName()));
      localObject2 = (TextView)paramView.findViewById(2131689705);
      ((TextView)localObject2).setText(UserDisplay.add(paramActivity, paramDVNTFeedItem.getUser()));
      ((View)localObject2).setOnClickListener(new WatchFeedItemEwok.2(this, (SimpleDraweeView)localObject1));
      localObject1 = (TextView)paramView.findViewById(2131689706);
      localObject2 = FeedItemType.getValue(paramDVNTFeedItem.getType());
      if ((localObject2 != FeedItemType.e) || (paramDVNTFeedItem.getDeviations().size() <= 1)) {
        break label298;
      }
      ((TextView)localObject1).setText(paramActivity.getString(2131231383));
    }
    for (;;)
    {
      paramView = (TextView)paramView.findViewById(2131689707);
      paramActivity = DAFormatUtils.format(paramActivity, paramDVNTFeedItem.getTime());
      if ((paramActivity != null) && (!paramActivity.equals(paramDVNTFeedItem.getTime()))) {
        break label315;
      }
      paramView.setVisibility(8);
      return;
      ImageUtils.isEmpty((GenericDraweeView)localObject1, localUri);
      ((ImageView)localObject1).setVisibility(0);
      localSimpleDraweeView.setVisibility(8);
      break;
      label298:
      ((TextView)localObject1).setText(paramActivity.getString(((FeedItemType)localObject2).a()));
    }
    label315:
    paramView.setVisibility(0);
    paramView.setText(paramActivity);
  }
  
  public void b(ProcessMenuListener paramProcessMenuListener)
  {
    b = paramProcessMenuListener;
  }
  
  public abstract void onCreateView(Activity paramActivity, ViewGroup paramViewGroup1, ViewGroup paramViewGroup2, boolean paramBoolean);
}
