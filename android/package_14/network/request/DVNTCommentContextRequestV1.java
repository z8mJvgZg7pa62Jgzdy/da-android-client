package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTCommentContextRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTCommentContextResponse>
{
  private final String commentId;
  private final boolean fetchContainingItem;
  private final Integer limit;
  private final Integer offset;
  
  public DVNTCommentContextRequestV1(String paramString, boolean paramBoolean, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTCommentContextResponse.class);
    commentId = paramString;
    fetchContainingItem = paramBoolean;
    offset = paramInteger1;
    limit = paramInteger2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTCommentContextRequestV1 localDVNTCommentContextRequestV1 = (DVNTCommentContextRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(commentId, commentId).append(fetchContainingItem, fetchContainingItem).append(offset, offset).append(limit, limit).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(commentId).append(fetchContainingItem).append(offset).append(limit).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTCommentContextResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).commentContext(paramString, commentId, fetchContainingItem, offset, limit, Boolean.valueOf(true));
  }
}
