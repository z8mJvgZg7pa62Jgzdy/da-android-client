package com.deviantart.android.damobile.view.thirdparty.decorator;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;

public abstract interface DecoratorGom<VIEW_HOLDER extends com.deviantart.android.damobile.view.gom.decorator.DecoratorViewHolder, DATA_TYPE>
{
  public abstract DecoratorViewHolder a(Context paramContext, ViewGroup paramViewGroup, RecyclerView.ViewHolder paramViewHolder);
  
  public abstract void f(Context paramContext, DecoratorViewHolder paramDecoratorViewHolder, Object paramObject);
  
  public abstract Class getComponentType();
}
