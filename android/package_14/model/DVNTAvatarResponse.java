package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTAvatarResponse
  implements Serializable
{
  String avatar;
  boolean ready;
  @SerializedName("uploadid")
  String uploadId;
  
  public DVNTAvatarResponse() {}
  
  public String getAvatar()
  {
    return avatar;
  }
  
  public String getUploadID()
  {
    return uploadId;
  }
  
  public String getUploadId()
  {
    return uploadId;
  }
  
  public boolean isReady()
  {
    return ready;
  }
  
  public void setAvatar(String paramString)
  {
    avatar = paramString;
  }
  
  public void setReady(boolean paramBoolean)
  {
    ready = paramBoolean;
  }
  
  public void setUploadId(String paramString)
  {
    uploadId = paramString;
  }
}
