package com.deviantart.android.damobile.util.submit.pages;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.util.submit.SubmitType;
import com.facebook.drawee.view.SimpleDraweeView;

public class SubmitPhotoPage
  extends SubmitPage
{
  @Bind({2131690016})
  SimpleDraweeView photoPreview;
  
  public SubmitPhotoPage(String paramString)
  {
    super(paramString);
  }
  
  public View a(ViewGroup paramViewGroup)
  {
    b(paramViewGroup);
    paramViewGroup = LayoutInflater.from(getContext()).inflate(2130968754, paramViewGroup, false);
    ButterKnife.bind(this, paramViewGroup);
    photoPlaceholder.setOnClickListener(new SubmitPhotoPage.1(this));
    onBindViewHolder();
    return paramViewGroup;
  }
  
  public void b()
  {
    photoPlaceholder.setOnClickListener(null);
    super.b();
    ButterKnife.unbind(this);
  }
  
  public SubmitType c()
  {
    return SubmitType.a;
  }
  
  public boolean g()
  {
    return true;
  }
  
  public CharSequence getFormatted()
  {
    return "SubmitPhotoTab";
  }
  
  public SimpleDraweeView getString()
  {
    return photoPreview;
  }
  
  public boolean setContentHeight()
  {
    return false;
  }
}
