package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTFeedBucketRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTFeedBucketResponse>
{
  private String bucketId;
  private Integer limit;
  private Integer offset;
  
  public DVNTFeedBucketRequestV1(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTFeedBucketResponse.class);
    bucketId = paramString;
    offset = paramInteger1;
    limit = paramInteger2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTFeedBucketRequestV1 localDVNTFeedBucketRequestV1 = (DVNTFeedBucketRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(bucketId, bucketId).append(offset, offset).append(limit, limit).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(bucketId).append(offset).append(limit).toHashCode();
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTFeedBucketResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).feedBucket(paramString, bucketId, offset, limit);
  }
}
