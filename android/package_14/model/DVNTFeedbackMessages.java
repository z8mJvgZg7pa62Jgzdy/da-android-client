package com.deviantart.android.android.package_14.model;

import com.deviantart.android.sdk.api.model.DVNTFeedbackMessage.List;
import com.deviantart.android.sdk.api.model.DVNTPaginatedResult;

public class DVNTFeedbackMessages
  extends DVNTPaginatedResult<DVNTFeedbackMessage.List>
{
  public DVNTFeedbackMessages() {}
}
