package com.deviantart.android.damobile.util.torpedo;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;

public class TorpedoLayoutManager
  extends GridLayoutManager
{
  public TorpedoLayoutManager(Context paramContext, int paramInt1, int paramInt2)
  {
    super(paramContext, paramInt1, paramInt2, false);
  }
}
