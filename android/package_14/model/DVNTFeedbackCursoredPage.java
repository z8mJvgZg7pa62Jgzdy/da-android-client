package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTFeedbackCursoredPage
  implements Serializable
{
  @SerializedName("cursor")
  String cursor;
  @SerializedName("has_more")
  Boolean hasMore;
  @SerializedName("results")
  DVNTFeedbackMessageStack.ListO results;
  
  public DVNTFeedbackCursoredPage() {}
  
  public String getCursor()
  {
    return cursor;
  }
  
  public DVNTFeedbackMessageStack.ListO getResults()
  {
    return results;
  }
  
  public Boolean isHasMore()
  {
    return hasMore;
  }
  
  public void setCursor(String paramString)
  {
    cursor = paramString;
  }
  
  public void setHasMore(Boolean paramBoolean)
  {
    hasMore = paramBoolean;
  }
  
  public void setResults(DVNTFeedbackMessageStack.ListO paramListO)
  {
    results = paramListO;
  }
}
