package com.deviantart.android.sdk.api;

import android.content.Context;
import com.deviantart.android.sdk.api.config.DVNTAPIConfig;
import com.deviantart.android.sdk.api.listener.DVNTAsyncRequestListener;
import com.deviantart.android.sdk.api.model.DVNTEndpointError;
import com.deviantart.android.sdk.api.network.request.DVNTBaseAsyncRequest;
import java.util.UUID;

public class DVNTRequestExecutor<REQUEST_TYPE extends DVNTBaseAsyncRequest, RESPONSE_TYPE>
{
  DVNTAbstractAsyncAPI asyncAPIInstance;
  boolean forced = false;
  boolean needGraduation = false;
  REQUEST_TYPE request;
  Class<RESPONSE_TYPE> responseClass;
  boolean wipeRequestCacheFirst = false;
  
  public DVNTRequestExecutor(DVNTAbstractAsyncAPI paramDVNTAbstractAsyncAPI, REQUEST_TYPE paramREQUEST_TYPE, Class<RESPONSE_TYPE> paramClass)
  {
    this(paramDVNTAbstractAsyncAPI, paramREQUEST_TYPE, paramClass, false);
  }
  
  public DVNTRequestExecutor(DVNTAbstractAsyncAPI paramDVNTAbstractAsyncAPI, REQUEST_TYPE paramREQUEST_TYPE, Class<RESPONSE_TYPE> paramClass, boolean paramBoolean)
  {
    request = paramREQUEST_TYPE;
    responseClass = paramClass;
    asyncAPIInstance = paramDVNTAbstractAsyncAPI;
    needGraduation = paramREQUEST_TYPE.needsGraduation();
    forced = paramBoolean;
  }
  
  private boolean consumeMock(DVNTBaseAsyncRequest paramDVNTBaseAsyncRequest, DVNTAsyncRequestListener paramDVNTAsyncRequestListener)
  {
    DVNTMockRegistry localDVNTMockRegistry = asyncAPIInstance.getMockRegistry();
    if (localDVNTMockRegistry.hasMocksFor(paramDVNTBaseAsyncRequest)) {
      paramDVNTBaseAsyncRequest = localDVNTMockRegistry.findNextMock(paramDVNTBaseAsyncRequest);
    }
    switch (DVNTRequestExecutor.1.$SwitchMap$com$deviantart$android$sdk$api$DVNTMockRegistry$MockType[paramDVNTBaseAsyncRequest.getType().ordinal()])
    {
    default: 
      return false;
    case 1: 
      paramDVNTAsyncRequestListener.onSuccess(paramDVNTBaseAsyncRequest.getData());
      return true;
    case 2: 
      paramDVNTAsyncRequestListener.onFailure((DVNTEndpointError)paramDVNTBaseAsyncRequest.getData());
      return true;
    }
    paramDVNTAsyncRequestListener.onException((Exception)paramDVNTBaseAsyncRequest.getData());
    return true;
  }
  
  public UUID call(Context paramContext, DVNTAsyncRequestListener<RESPONSE_TYPE> paramDVNTAsyncRequestListener)
  {
    if (asyncAPIInstance == null) {
      throw new RuntimeException("You need to call start() to use the API");
    }
    if (paramContext == null) {
      throw new IllegalArgumentException("Context can't be null.");
    }
    request.setWipeCacheFirst(wipeRequestCacheFirst);
    request.setVersion(asyncAPIInstance.apiConfig.buildRequestVersion());
    if (consumeMock(request, paramDVNTAsyncRequestListener)) {
      return null;
    }
    if (forced) {
      return asyncAPIInstance.apiClientInstance.anonymousCallAsync(paramContext, request, paramDVNTAsyncRequestListener);
    }
    asyncAPIInstance.openOAuthSession(paramContext, needGraduation);
    boolean bool = wipeRequestCacheFirst;
    wipeRequestCacheFirst = false;
    if ((needGraduation) || (asyncAPIInstance.apiClientInstance.hasGraduated(paramContext, false))) {
      return asyncAPIInstance.apiClientInstance.userCallAsync(paramContext, request, paramDVNTAsyncRequestListener);
    }
    return asyncAPIInstance.apiClientInstance.clientCallAsync(paramContext, request, paramDVNTAsyncRequestListener);
  }
  
  public DVNTMockRegistry.DVNTMockBuilder<RESPONSE_TYPE> mock()
  {
    return asyncAPIInstance.getMockRegistry().buildMocks(request, responseClass);
  }
  
  public DVNTRequestExecutor<REQUEST_TYPE, RESPONSE_TYPE> noCache()
  {
    wipeRequestCacheFirst = true;
    return this;
  }
}
