package com.deviantart.android.damobile.util;

import android.app.Activity;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTContextualizedGroupAPI;
import com.deviantart.android.android.utils.DVNTContextUtils;
import java.util.Arrays;

public class SearchUtils
{
  public SearchUtils() {}
  
  public static void getLogs(Activity paramActivity, String paramString, SearchUtils.UserSearchResultListener paramUserSearchResultListener)
  {
    if (DVNTContextUtils.isContextDead(paramActivity)) {
      return;
    }
    DVNTAsyncAPI.buildGroupedRequest(paramActivity).addSearchFriends("search_friend_request", paramString).addWhoIs("validate_user_request", Arrays.asList(new String[] { paramString })).execute(new SearchUtils.1(paramString, paramUserSearchResultListener));
  }
}
