package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.content.Context;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.adapter.recyclerview.DAStateRecyclerViewAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.Stream.Notifiable;
import com.deviantart.android.damobile.stream.loader.StreamLoader.ErrorType;
import com.deviantart.android.damobile.util.scrolling.RecyclerViewEndlessScrollListener;
import com.deviantart.android.damobile.util.scrolling.RecyclerViewSignificantScrollListener;

public class DAStateRecyclerView
  extends FrameLayout
  implements SwipeRefreshLayout.OnRefreshListener, Stream.Notifiable, EndlessScrollListener
{
  private View a;
  protected ViewState b = new ViewState();
  protected RecyclerView.OnScrollListener d;
  private boolean e = true;
  private boolean f = true;
  protected DAStateRecyclerViewAdapter l;
  private RecyclerView.OnScrollListener mOnScrollListener;
  @Bind({2131689640})
  RecyclerView recyclerView;
  @Bind({2131689639})
  DASwipeRefreshLayout refreshLayout;
  private RecyclerView.OnScrollListener searchResults;
  private View v;
  
  public DAStateRecyclerView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public DAStateRecyclerView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public DAStateRecyclerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    LayoutInflater.from(paramContext).inflate(2130968623, this, true);
    ButterKnife.bind(this);
    recyclerView.setLayoutManager(new LinearLayoutManager(paramContext));
    d = new RecyclerViewSignificantScrollListener();
    recyclerView.addOnScrollListener(d);
    refreshLayout.setEnabled(false);
    setProgressViewOffset(0);
  }
  
  public void a()
  {
    if (!DVNTContextUtils.isContextDead(getContext()))
    {
      if (l == null) {
        return;
      }
      l.get().a(getContext(), this);
    }
  }
  
  public void a(int paramInt)
  {
    recyclerView.scrollToPosition(paramInt);
  }
  
  public void a(View paramView)
  {
    if (l == null) {
      return;
    }
    l.a(paramView);
  }
  
  public void addItemDecoration(RecyclerView.ItemDecoration paramItemDecoration)
  {
    recyclerView.addItemDecoration(paramItemDecoration);
  }
  
  public void b()
  {
    b.a(ViewState.State.b);
    if (v != null)
    {
      a = v;
      setStateView(v);
      return;
    }
    a = ViewState.Helper.a((Activity)getContext(), a, this, b);
    setStateView(a);
  }
  
  public void b(View paramView)
  {
    if (l == null) {
      return;
    }
    l.clear(paramView);
  }
  
  public void b(StreamLoader.ErrorType paramErrorType, String paramString)
  {
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("state_error", paramErrorType);
    localBundle.putString("state_msg", paramString);
    b.a(ViewState.State.i);
    b.a(ViewState.State.i, localBundle);
    a = ViewState.Helper.b((Activity)getContext(), a, this, b);
    setStateView(a);
  }
  
  public void b(boolean paramBoolean)
  {
    if ((paramBoolean) && (!e))
    {
      recyclerView.addOnScrollListener(d);
      e = true;
    }
    if ((!paramBoolean) && (e))
    {
      recyclerView.removeOnScrollListener(d);
      e = false;
    }
  }
  
  public void c()
  {
    if (l == null) {
      return;
    }
    setStateView(null);
    if ((l.e() <= 0) && (recyclerView != null) && (recyclerView.getLayoutManager() != null)) {
      recyclerView.getLayoutManager().scrollToPosition(0);
    }
    l.b();
  }
  
  public void d()
  {
    if (l != null) {
      l.clear();
    }
  }
  
  public void d(boolean paramBoolean)
  {
    if (l == null) {
      return;
    }
    f = paramBoolean;
    l.a();
    l.get().add(getContext(), this, true);
  }
  
  public void e()
  {
    if (f)
    {
      b.a(ViewState.State.d);
      a = ViewState.Helper.a((Activity)getContext(), a, this);
      setStateView(a);
      return;
    }
    f = true;
  }
  
  public void f()
  {
    if (refreshLayout == null) {
      return;
    }
    refreshLayout.setRefreshing(false);
  }
  
  public RecyclerView.Adapter getAdapter()
  {
    return recyclerView.getAdapter();
  }
  
  public int getHeaderItemCount()
  {
    return l.size();
  }
  
  public void onViewCreated()
  {
    setOnRefreshListener(this);
  }
  
  public void refreshView()
  {
    d(false);
  }
  
  public void setAdapter()
  {
    setEndlessScrollListener(3);
  }
  
  public void setAdapter(DAStateRecyclerViewAdapter paramDAStateRecyclerViewAdapter)
  {
    if (l != null)
    {
      l.readMessage();
      l.clear();
      l.visitMaxs();
    }
    l = paramDAStateRecyclerViewAdapter;
    recyclerView.setAdapter(paramDAStateRecyclerViewAdapter);
    if (!paramDAStateRecyclerViewAdapter.next()) {
      return;
    }
    if (!paramDAStateRecyclerViewAdapter.get().equals())
    {
      paramDAStateRecyclerViewAdapter.get().add(getContext(), this, true);
      return;
    }
    b();
  }
  
  public void setEmptyMessage(int paramInt)
  {
    b.a(paramInt);
  }
  
  public void setEmptyMessage(String paramString)
  {
    b.a(paramString);
  }
  
  public void setEmptyStateView(View paramView)
  {
    v = paramView;
  }
  
  public void setEndlessScrollListener(int paramInt)
  {
    if (searchResults != null) {
      recyclerView.removeOnScrollListener(searchResults);
    }
    searchResults = new RecyclerViewEndlessScrollListener(new EndlessScrollOptions(paramInt, this));
    recyclerView.addOnScrollListener(searchResults);
  }
  
  public void setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener paramOnRefreshListener)
  {
    DASwipeRefreshLayout localDASwipeRefreshLayout = refreshLayout;
    if (paramOnRefreshListener != null) {}
    for (boolean bool = true;; bool = false)
    {
      localDASwipeRefreshLayout.setEnabled(bool);
      refreshLayout.setOnRefreshListener(paramOnRefreshListener);
      return;
    }
  }
  
  public void setOnScrollListener(RecyclerView.OnScrollListener paramOnScrollListener)
  {
    if (mOnScrollListener != null) {
      recyclerView.removeOnScrollListener(mOnScrollListener);
    }
    mOnScrollListener = paramOnScrollListener;
    recyclerView.addOnScrollListener(paramOnScrollListener);
  }
  
  public void setProgressViewOffset(int paramInt)
  {
    refreshLayout.setProgressViewOffset(paramInt);
  }
  
  public void setStateView(View paramView)
  {
    if (l == null) {
      return;
    }
    l.b(paramView);
  }
}
