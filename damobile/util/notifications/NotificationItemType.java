package com.deviantart.android.damobile.util.notifications;

import android.util.Log;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessage;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessageStack;
import com.deviantart.android.android.package_14.model.DVNTFeedbackSubject;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.package_14.model.DVNTUserStatus;
import com.deviantart.android.damobile.util.UserUtils;

public enum NotificationItemType
{
  private NotificationItemText.TextParam[] buffer;
  private int k;
  private NotificationItemType m;
  private int o;
  private NotificationsPage s;
  private NotificationItemType t;
  
  static
  {
    NotificationItemText.TextParam localTextParam = NotificationItemText.TextParam.e;
    Object localObject1 = NotificationItemText.TextParam.q;
    Object localObject2 = NotificationsPage.b;
    x = new NotificationItemType("FAVED_DEVIATION", 0, 2131231159, 2130837749, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    localTextParam = NotificationItemText.TextParam.b;
    localObject1 = NotificationItemText.TextParam.q;
    localObject2 = NotificationsPage.b;
    z = new NotificationItemType("FAVED_DEVIATION_ROLLUP", 1, 2131231160, 0, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.q;
    localObject2 = NotificationItemText.TextParam.g;
    NotificationsPage localNotificationsPage = NotificationsPage.b;
    v = new NotificationItemType("COLLECTED_DEVIATION", 2, 2131231147, 2130837746, new NotificationItemText.TextParam[] { localTextParam, localObject1, localObject2 }, localNotificationsPage);
    localTextParam = NotificationItemText.TextParam.b;
    localObject1 = NotificationItemText.TextParam.q;
    localObject2 = NotificationsPage.b;
    w = new NotificationItemType("COLLECTED_DEVIATION_ROLLUP", 3, 2131231148, 0, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.q;
    localObject2 = NotificationsPage.d;
    h = new NotificationItemType("COMMENTED_ON_DEVIATION", 4, 2131231149, 2130837747, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    localTextParam = NotificationItemText.TextParam.b;
    localObject1 = NotificationItemText.TextParam.q;
    localObject2 = NotificationsPage.d;
    i = new NotificationItemType("COMMENTED_ON_DEVIATION_ROLLUP", 5, 2131231150, 0, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationsPage.d;
    r = new NotificationItemType("COMMENTED_ON_PROFILE", 6, 2131231151, 2130837747, new NotificationItemText.TextParam[] { localTextParam }, (NotificationsPage)localObject1);
    localTextParam = NotificationItemText.TextParam.b;
    localObject1 = NotificationsPage.d;
    E = new NotificationItemType("COMMENTED_ON_PROFILE_ROLLUP", 7, 2131231152, 0, new NotificationItemText.TextParam[] { localTextParam }, (NotificationsPage)localObject1);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationsPage.d;
    b = new NotificationItemType("COMMENTED_ON_STATUS", 8, 2131231153, 2130837747, new NotificationItemText.TextParam[] { localTextParam }, (NotificationsPage)localObject1);
    localTextParam = NotificationItemText.TextParam.b;
    localObject1 = NotificationsPage.d;
    q = new NotificationItemType("COMMENTED_ON_STATUS_ROLLUP", 9, 2131231154, 0, new NotificationItemText.TextParam[] { localTextParam }, (NotificationsPage)localObject1);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.a;
    localObject2 = NotificationsPage.g;
    u = new NotificationItemType("REPLIED_ON_DEVIATION", 10, 2131231173, 2130837751, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.c;
    localObject2 = NotificationsPage.g;
    l = new NotificationItemType("REPLIED_ON_PROFILE", 11, 2131231174, 2130837751, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationsPage.g;
    g = new NotificationItemType("REPLIED_ON_YOUR_PROFILE", 12, 2131231176, 2130837751, new NotificationItemText.TextParam[] { localTextParam }, (NotificationsPage)localObject1);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationsPage.g;
    P = new NotificationItemType("REPLIED_ON_STATUS", 13, 2131231175, 2130837751, new NotificationItemText.TextParam[] { localTextParam }, (NotificationsPage)localObject1);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationsPage.g;
    Q = new NotificationItemType("REPLIED_ON_YOUR_STATUS", 14, 2131231177, 2130837751, new NotificationItemText.TextParam[] { localTextParam }, (NotificationsPage)localObject1);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.a;
    localObject2 = NotificationsPage.e;
    e = new NotificationItemType("MENTIONED_YOU_IN_COMMENT_ON_OTHERS_DEVIATION", 15, 2131231166, 2130837750, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.c;
    localObject2 = NotificationsPage.e;
    d = new NotificationItemType("MENTIONED_YOU_IN_COMMENT_ON_OTHERS_PROFILE", 16, 2131231167, 2130837750, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.d;
    localObject2 = NotificationsPage.e;
    a = new NotificationItemType("MENTIONED_YOU_IN_COMMENT_ON_OTHERS_STATUS", 17, 2131231168, 2130837750, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationsPage.e;
    f = new NotificationItemType("MENTIONED_YOU_IN_STATUS", 18, 2131231170, 2130837750, new NotificationItemText.TextParam[] { localTextParam }, (NotificationsPage)localObject1);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.a;
    localObject2 = NotificationsPage.e;
    p = new NotificationItemType("MENTIONED_YOU_IN_DEVIATION", 19, 2131231169, 2130837750, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.q;
    localObject2 = NotificationItemText.TextParam.a;
    localNotificationsPage = NotificationsPage.e;
    y = new NotificationItemType("MENTIONED_DEVIATION_IN_COMMENT_ON_DEVIATION", 20, 2131231161, 2130837750, new NotificationItemText.TextParam[] { localTextParam, localObject1, localObject2 }, localNotificationsPage);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.q;
    localObject2 = NotificationItemText.TextParam.c;
    localNotificationsPage = NotificationsPage.e;
    I = new NotificationItemType("MENTIONED_DEVIATION_IN_COMMENT_ON_PROFILE", 21, 2131231162, 2130837750, new NotificationItemText.TextParam[] { localTextParam, localObject1, localObject2 }, localNotificationsPage);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.q;
    localObject2 = NotificationItemText.TextParam.d;
    localNotificationsPage = NotificationsPage.e;
    H = new NotificationItemType("MENTIONED_DEVIATION_IN_COMMENT_ON_STATUS", 22, 2131231163, 2130837750, new NotificationItemText.TextParam[] { localTextParam, localObject1, localObject2 }, localNotificationsPage);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.q;
    localObject2 = NotificationItemText.TextParam.a;
    localNotificationsPage = NotificationsPage.e;
    G = new NotificationItemType("MENTIONED_DEVIATION_IN_DEVIATION", 23, 2131231164, 2130837750, new NotificationItemText.TextParam[] { localTextParam, localObject1, localObject2 }, localNotificationsPage);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationItemText.TextParam.q;
    localObject2 = NotificationsPage.e;
    c = new NotificationItemType("MENTIONED_DEVIATION_IN_STATUS", 24, 2131231165, 2130837750, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    localTextParam = NotificationItemText.TextParam.e;
    localObject1 = NotificationsPage.b;
    N = new NotificationItemType("WATCHED", 25, 2131231181, 2130837754, new NotificationItemText.TextParam[] { localTextParam }, (NotificationsPage)localObject1);
    localTextParam = NotificationItemText.TextParam.b;
    localObject1 = NotificationsPage.b;
    A = new NotificationItemType("WATCHED_ROLLUP", 26, 2131231182, 0, new NotificationItemText.TextParam[] { localTextParam }, (NotificationsPage)localObject1);
    localTextParam = NotificationItemText.TextParam.a;
    localObject1 = NotificationsPage.b;
    B = new NotificationItemType("DD_RECEIVED", 27, 2131231157, 2130837748, new NotificationItemText.TextParam[] { localTextParam }, (NotificationsPage)localObject1);
    localTextParam = NotificationItemText.TextParam.a;
    localObject1 = NotificationItemText.TextParam.e;
    localObject2 = NotificationsPage.b;
    C = new NotificationItemType("DD_SUGGESTION_ACCEPTED", 28, 2131231158, 2130837748, new NotificationItemText.TextParam[] { localTextParam, localObject1 }, (NotificationsPage)localObject2);
    values = new NotificationItemType[] { x, z, v, w, h, i, r, E, b, q, u, l, g, P, Q, e, d, a, f, p, y, I, H, G, c, N, A, B, C };
    xm = z;
    vm = w;
    hm = i;
    rm = E;
    bm = q;
    Nm = A;
    zt = x;
    wt = v;
    it = h;
    Et = r;
    qt = b;
    At = N;
  }
  
  private NotificationItemType(int paramInt1, int paramInt2, NotificationItemText.TextParam[] paramArrayOfTextParam, NotificationsPage paramNotificationsPage)
  {
    k = paramInt1;
    o = paramInt2;
    buffer = paramArrayOfTextParam;
    s = paramNotificationsPage;
  }
  
  private static NotificationItemType a(DVNTFeedbackMessage paramDVNTFeedbackMessage, boolean paramBoolean)
  {
    Object localObject = paramDVNTFeedbackMessage.getType();
    DVNTFeedbackSubject localDVNTFeedbackSubject = paramDVNTFeedbackMessage.getSubject();
    switch (NotificationItemType.1.NAMED[localObject.ordinal()])
    {
    default: 
      paramDVNTFeedbackMessage = null;
    }
    for (;;)
    {
      if (paramDVNTFeedbackMessage != null) {
        break label489;
      }
      Log.e("NotificationType", "Unhandled " + localObject + " message type");
      return null;
      paramDVNTFeedbackMessage = N;
      continue;
      if (localDVNTFeedbackSubject == null)
      {
        Log.e("NotificationType", "Expecting subject for FEEDBACK_COMMENT message type");
        paramDVNTFeedbackMessage = null;
      }
      else if (localDVNTFeedbackSubject.getSubjectDeviation() != null)
      {
        paramDVNTFeedbackMessage = h;
      }
      else if (localDVNTFeedbackSubject.getSubjectProfile() != null)
      {
        paramDVNTFeedbackMessage = r;
      }
      else
      {
        if (localDVNTFeedbackSubject.getSubjectStatus() == null) {
          break;
        }
        paramDVNTFeedbackMessage = b;
        continue;
        if (paramDVNTFeedbackMessage.getDeviation() != null)
        {
          paramDVNTFeedbackMessage = u;
        }
        else if (paramDVNTFeedbackMessage.getProfile() != null)
        {
          if (UserUtils.get(paramDVNTFeedbackMessage.getProfile().getUserName())) {
            paramDVNTFeedbackMessage = g;
          } else {
            paramDVNTFeedbackMessage = l;
          }
        }
        else
        {
          if (paramDVNTFeedbackMessage.getStatus() == null) {
            break;
          }
          paramDVNTFeedbackMessage = paramDVNTFeedbackMessage.getStatus().getAuthor();
          if ((paramDVNTFeedbackMessage != null) && (UserUtils.get(paramDVNTFeedbackMessage.getUserName())))
          {
            paramDVNTFeedbackMessage = Q;
          }
          else
          {
            paramDVNTFeedbackMessage = P;
            continue;
            paramDVNTFeedbackMessage = x;
            continue;
            paramDVNTFeedbackMessage = v;
            continue;
            if (paramDVNTFeedbackMessage.getDeviation() != null)
            {
              paramDVNTFeedbackMessage = y;
            }
            else if (paramDVNTFeedbackMessage.getProfile() != null)
            {
              paramDVNTFeedbackMessage = I;
            }
            else
            {
              if (paramDVNTFeedbackMessage.getStatus() == null) {
                break;
              }
              paramDVNTFeedbackMessage = H;
              continue;
              if (paramDVNTFeedbackMessage.getDeviation() != null)
              {
                paramDVNTFeedbackMessage = e;
              }
              else if (paramDVNTFeedbackMessage.getProfile() != null)
              {
                paramDVNTFeedbackMessage = d;
              }
              else
              {
                if (paramDVNTFeedbackMessage.getStatus() == null) {
                  break;
                }
                paramDVNTFeedbackMessage = a;
                continue;
                if (paramDVNTFeedbackMessage.getDeviation() == null) {
                  break;
                }
                paramDVNTFeedbackMessage = G;
                continue;
                if (paramDVNTFeedbackMessage.getDeviation() == null) {
                  break;
                }
                paramDVNTFeedbackMessage = p;
                continue;
                paramDVNTFeedbackMessage = c;
                continue;
                paramDVNTFeedbackMessage = f;
                continue;
                paramDVNTFeedbackMessage = C;
                continue;
                paramDVNTFeedbackMessage = B;
              }
            }
          }
        }
      }
    }
    label489:
    localObject = paramDVNTFeedbackMessage;
    if (paramBoolean)
    {
      if (m == null)
      {
        Log.e("NotificationType", "Expecting corresponding rollup type for " + paramDVNTFeedbackMessage);
        return null;
      }
      localObject = m;
    }
    return localObject;
  }
  
  public static NotificationItemType a(DVNTFeedbackMessageStack paramDVNTFeedbackMessageStack)
  {
    boolean bool = true;
    if ((paramDVNTFeedbackMessageStack.getCount() != null) && (paramDVNTFeedbackMessageStack.getCount().intValue() > 1)) {}
    for (;;)
    {
      return a(paramDVNTFeedbackMessageStack, bool);
      bool = false;
    }
  }
  
  public static NotificationItemType findItem(DVNTFeedbackMessage paramDVNTFeedbackMessage)
  {
    return a(paramDVNTFeedbackMessage, false);
  }
  
  public int a()
  {
    return k;
  }
  
  public boolean b()
  {
    return t != null;
  }
  
  public int c()
  {
    return o;
  }
  
  public NotificationItemType e()
  {
    return t;
  }
  
  public NotificationItemText.TextParam[] getItem()
  {
    return buffer;
  }
  
  public NotificationsPage getString()
  {
    return s;
  }
}
