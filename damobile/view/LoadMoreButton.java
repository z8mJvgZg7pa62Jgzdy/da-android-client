package com.deviantart.android.damobile.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;

public class LoadMoreButton
  extends FrameLayout
{
  @Bind({2131689611})
  ProgressBar progressBar;
  @Bind({2131689610})
  TextView textView;
  
  public LoadMoreButton(Context paramContext)
  {
    super(paramContext);
    init();
  }
  
  public LoadMoreButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init();
  }
  
  public LoadMoreButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init();
  }
  
  private void init()
  {
    LayoutInflater.from(getContext()).inflate(2130968613, this, true);
    ButterKnife.bind(this);
  }
  
  public void showProgress()
  {
    progressBar.setVisibility(0);
    textView.setVisibility(8);
  }
}
