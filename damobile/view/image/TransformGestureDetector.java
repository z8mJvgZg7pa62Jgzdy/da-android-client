package com.deviantart.android.damobile.view.image;

import android.view.MotionEvent;

public class TransformGestureDetector
  implements MultiPointerGestureDetector.Listener
{
  private final MultiPointerGestureDetector a;
  private TransformGestureDetector.Listener b = null;
  
  public TransformGestureDetector(MultiPointerGestureDetector paramMultiPointerGestureDetector)
  {
    a = paramMultiPointerGestureDetector;
    a.b(this);
  }
  
  private float getValue(float[] paramArrayOfFloat, int paramInt)
  {
    int i = 0;
    float f = 0.0F;
    while (i < paramInt)
    {
      f += paramArrayOfFloat[i];
      i += 1;
    }
    if (paramInt > 0) {
      return f / paramInt;
    }
    return 0.0F;
  }
  
  public static TransformGestureDetector scalb()
  {
    return new TransformGestureDetector(MultiPointerGestureDetector.scalb());
  }
  
  public void a()
  {
    a.b();
  }
  
  public void a(MultiPointerGestureDetector paramMultiPointerGestureDetector)
  {
    if (b != null) {
      b.draw(this);
    }
  }
  
  public boolean a(MotionEvent paramMotionEvent)
  {
    return a.a(paramMotionEvent);
  }
  
  public void b()
  {
    a.a();
  }
  
  public void b(MultiPointerGestureDetector paramMultiPointerGestureDetector)
  {
    if (b != null) {
      b.f(this);
    }
  }
  
  public void b(TransformGestureDetector.Listener paramListener)
  {
    b = paramListener;
  }
  
  public void c(MultiPointerGestureDetector paramMultiPointerGestureDetector)
  {
    if (b != null) {
      b.b(this);
    }
  }
  
  public float component2()
  {
    return getValue(a.getValue(), a.get());
  }
  
  public boolean d()
  {
    return a.d();
  }
  
  public float f()
  {
    return getValue(a.add(), a.get());
  }
  
  public float getAngle()
  {
    if (a.get() < 2) {
      return 0.0F;
    }
    float f5 = a.add()[1];
    float f6 = a.add()[0];
    float f7 = a.getValue()[1];
    float f8 = a.getValue()[0];
    float f1 = a.removeFirst()[1];
    float f2 = a.removeFirst()[0];
    float f3 = a.size()[1];
    float f4 = a.size()[0];
    f5 = (float)Math.atan2(f7 - f8, f5 - f6);
    return (float)Math.atan2(f3 - f4, f1 - f2) - f5;
  }
  
  public float getCount()
  {
    return getValue(a.size(), a.get()) - getValue(a.getValue(), a.get());
  }
  
  public float getScale()
  {
    if (a.get() < 2) {
      return 1.0F;
    }
    float f5 = a.add()[1];
    float f6 = a.add()[0];
    float f7 = a.getValue()[1];
    float f8 = a.getValue()[0];
    float f1 = a.removeFirst()[1];
    float f2 = a.removeFirst()[0];
    float f3 = a.size()[1];
    float f4 = a.size()[0];
    f5 = (float)Math.hypot(f5 - f6, f7 - f8);
    return (float)Math.hypot(f1 - f2, f3 - f4) / f5;
  }
  
  public float remove()
  {
    return getValue(a.removeFirst(), a.get()) - getValue(a.add(), a.get());
  }
}
