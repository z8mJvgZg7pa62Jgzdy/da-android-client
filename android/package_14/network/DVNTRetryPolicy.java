package com.deviantart.android.android.package_14.network;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.retry.RetryPolicy;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DVNTRetryPolicy
  implements RetryPolicy
{
  public static final float DEFAULT_BACKOFF_MULT = 1.0F;
  public static final long DEFAULT_DELAY_BEFORE_RETRY = 2500L;
  public static final int DEFAULT_RETRY_COUNT = 3;
  private float backOffMultiplier = 1.0F;
  private long delayBeforeRetry = 2500L;
  private int retryCount = 3;
  
  public DVNTRetryPolicy()
  {
    this(3, 2500L, 1.0F);
  }
  
  public DVNTRetryPolicy(int paramInt, long paramLong, float paramFloat)
  {
    retryCount = paramInt;
    delayBeforeRetry = paramLong;
    backOffMultiplier = paramFloat;
  }
  
  public long getDelayBeforeRetry()
  {
    return delayBeforeRetry;
  }
  
  public int getRetryCount()
  {
    return retryCount;
  }
  
  public void retry(SpiceException paramSpiceException)
  {
    if (!(paramSpiceException.getCause() instanceof RetrofitError))
    {
      retryCount = 0;
      return;
    }
    paramSpiceException = (RetrofitError)paramSpiceException.getCause();
    if ((paramSpiceException.getResponse() == null) || (paramSpiceException.getResponse().getStatus() <= 0))
    {
      retryCount = 0;
      return;
    }
    switch (paramSpiceException.getResponse().getStatus())
    {
    default: 
      retryCount = 0;
      return;
    }
    retryCount -= 1;
    delayBeforeRetry = (((float)delayBeforeRetry * backOffMultiplier));
  }
}
