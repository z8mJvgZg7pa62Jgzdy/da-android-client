package com.deviantart.android.android.package_14.model;

import com.deviantart.android.sdk.api.model.DVNTAbstractDeviation.List;
import com.deviantart.android.sdk.api.model.DVNTPaginatedResult;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTPaginatedResultResponse
  extends DVNTPaginatedResult<DVNTAbstractDeviation.List>
  implements Serializable
{
  @SerializedName("has_less")
  private boolean hasLess;
  @SerializedName("name")
  private String name;
  @SerializedName("prev_offset")
  private Integer previousOffset;
  
  public DVNTPaginatedResultResponse() {}
  
  public boolean getHasLess()
  {
    return hasLess;
  }
  
  public String getName()
  {
    return name;
  }
  
  public Integer getPreviousOffset()
  {
    return previousOffset;
  }
  
  public boolean isHasLess()
  {
    return hasLess;
  }
  
  public void setHasLess(boolean paramBoolean)
  {
    hasLess = paramBoolean;
  }
  
  public void setName(String paramString)
  {
    name = paramString;
  }
  
  public void setPreviousOffset(Integer paramInteger)
  {
    previousOffset = paramInteger;
  }
}
