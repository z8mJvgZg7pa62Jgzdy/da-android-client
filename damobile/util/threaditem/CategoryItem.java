package com.deviantart.android.damobile.util.threaditem;

import com.google.common.base.Objects;

public class CategoryItem
  implements ThreadItem<com.deviantart.android.sdk.api.model.DVNTCategory>
{
  protected boolean a = false;
  protected Integer b = Integer.valueOf(0);
  protected com.deviantart.android.android.package_14.model.DVNTCategory c;
  protected boolean d = false;
  
  public CategoryItem(com.deviantart.android.android.package_14.model.DVNTCategory paramDVNTCategory)
  {
    c = paramDVNTCategory;
  }
  
  public void a(Integer paramInteger)
  {
    b = paramInteger;
  }
  
  public boolean a()
  {
    return d;
  }
  
  public String b()
  {
    return c.getParentPath();
  }
  
  public void b(boolean paramBoolean)
  {
    d = paramBoolean;
  }
  
  public String c()
  {
    if (d) {
      return c.getCatPath() + "_see_all";
    }
    return c.getCatPath();
  }
  
  public void d(Boolean paramBoolean)
  {
    a = paramBoolean.booleanValue();
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    paramObject = (CategoryItem)paramObject;
    return Objects.append(c(), paramObject.c());
  }
  
  public String f()
  {
    return "stream_" + c();
  }
  
  public com.deviantart.android.android.package_14.model.DVNTCategory get()
  {
    return c;
  }
  
  public Integer getValue()
  {
    return b;
  }
  
  public Integer set()
  {
    if ((c != null) && (c.getHasSubCategory() != null) && (c.getHasSubCategory().booleanValue())) {}
    for (int i = 1;; i = 0) {
      return Integer.valueOf(i);
    }
  }
  
  public Boolean visitAnnotation()
  {
    return Boolean.valueOf(a);
  }
}
