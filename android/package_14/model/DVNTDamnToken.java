package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTDamnToken
  implements Serializable
{
  @SerializedName("damntoken")
  String value;
  
  public DVNTDamnToken() {}
  
  public String getValue()
  {
    return value;
  }
  
  public void setValue(String paramString)
  {
    value = paramString;
  }
}
