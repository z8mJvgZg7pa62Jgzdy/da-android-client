package com.deviantart.android.damobile.view.userprofile;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.text.SpannableString;
import android.text.SpannableStringInternal;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.activity.CreateNoteActivity.IntentBuilder;
import com.deviantart.android.damobile.util.DAFont;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.MemberType;
import com.deviantart.android.damobile.util.PhotoUtils;
import com.deviantart.android.damobile.util.PhotoUtils.PictureType;
import com.deviantart.android.damobile.util.UserDisplay;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.ViewHelper;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.view.AbstractWatchButton;
import com.deviantart.android.damobile.view.WatchButton;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;

public class UserProfileCardOverview
  extends FrameLayout
  implements UserProfileCardBase
{
  @Bind({2131690096})
  TextView artistDetails;
  @Bind({2131689769})
  SimpleDraweeView avatar;
  boolean c = false;
  String l;
  private DVNTUser o;
  @Bind({2131689841})
  TextView realName;
  boolean refresh;
  @Bind({2131690099})
  Button sendNoteButton;
  @Bind({2131690097})
  TextView tagLine;
  @Bind({2131689840})
  TextView userNameText;
  @Bind({2131690098})
  WatchButton watchButton;
  
  public UserProfileCardOverview(Context paramContext, boolean paramBoolean)
  {
    super(paramContext);
    LayoutInflater.from(paramContext).inflate(2130968791, this, true);
    ButterKnife.bind(this);
    userNameText.setSelected(true);
    if (paramBoolean)
    {
      watchButton.setVisibility(4);
      sendNoteButton.setVisibility(4);
    }
    refresh = paramBoolean;
  }
  
  private static SpannableString a(Context paramContext, UserCardData paramUserCardData)
  {
    int i = 0;
    ArrayList localArrayList = new ArrayList();
    if (!TextUtils.isEmpty(paramUserCardData.visitFrame())) {
      localArrayList.add(paramUserCardData.visitFrame());
    }
    if (!TextUtils.isEmpty(paramUserCardData.getDeveloperConnection())) {
      localArrayList.add(paramUserCardData.getDeveloperConnection());
    }
    paramUserCardData = new SpannableString(StringUtils.join(localArrayList, " | ").toUpperCase());
    paramContext = DAFont.o.get(paramContext);
    int m = 0;
    int k;
    for (int j = 0; i < paramUserCardData.length(); j = k)
    {
      if (paramUserCardData.charAt(i) != ' ')
      {
        k = j;
        if (paramUserCardData.charAt(i) != '|') {}
      }
      else
      {
        if (j != m) {
          paramUserCardData.setSpan(new CalligraphyTypefaceSpan(paramContext), j, m + 1, 33);
        }
        k = i;
      }
      m = i;
      i += 1;
    }
    if (j != m) {
      paramUserCardData.setSpan(new CalligraphyTypefaceSpan(paramContext), j, m + 1, 33);
    }
    return paramUserCardData;
  }
  
  public void onClickAvatar()
  {
    if (!refresh) {
      return;
    }
    PhotoUtils.a((Activity)getContext(), avatar, c, l, PhotoUtils.PictureType.C);
  }
  
  public void onClickSendNoteButton()
  {
    if (o == null) {
      return;
    }
    DVNTAbstractAsyncAPI.cancelAllRequests();
    Activity localActivity = (Activity)getContext();
    TrackerUtil.get(localActivity, EventKeys.Category.d, "create_note", "user_profile");
    localActivity.startActivityForResult(new CreateNoteActivity.IntentBuilder().a(o).a(localActivity), 110);
    localActivity.overridePendingTransition(2131034146, 2131034113);
  }
  
  public void onCreateView(UserCardData paramUserCardData)
  {
    Object localObject;
    if ((refresh) && (UserUtils.a() != null))
    {
      c = true;
      localObject = Uri.fromFile(UserUtils.a());
    }
    for (;;)
    {
      ImageUtils.isEmpty(avatar, (Uri)localObject);
      o = new DVNTUser();
      o.setUserIconURL(paramUserCardData.getString());
      o.setUserName(paramUserCardData.format());
      o.setType(paramUserCardData.a().code());
      userNameText.setText(UserDisplay.a(getContext(), o, true));
      ViewHelper.append(realName, paramUserCardData.getColor());
      if (watchButton.getVisibility() == 0)
      {
        watchButton.a(o.getUserName(), paramUserCardData.a(), paramUserCardData.b());
        watchButton.setEventSource("deviant_profile");
      }
      if (paramUserCardData.isStarred().booleanValue()) {
        artistDetails.setText(a(getContext(), paramUserCardData), TextView.BufferType.SPANNABLE);
      }
      ViewHelper.append(tagLine, paramUserCardData.getPost());
      return;
      Uri localUri = Uri.parse(paramUserCardData.getString());
      localObject = localUri;
      if (!UserUtils.get().equals(paramUserCardData.getString()))
      {
        l = paramUserCardData.getString();
        c = true;
        localObject = localUri;
      }
    }
  }
}
