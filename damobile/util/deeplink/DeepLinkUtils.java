package com.deviantart.android.damobile.util.deeplink;

import android.content.Context;
import android.net.Uri;
import com.deviantart.datoolkit.logger.DVNTLog;

public class DeepLinkUtils
{
  public DeepLinkUtils() {}
  
  public static String getConfigFile(Context paramContext)
  {
    return "com.deviantart.android.damobile/" + paramContext.getString(2131230844);
  }
  
  public static Uri remove(Context paramContext, Long paramLong)
  {
    return Uri.parse(toString(paramContext, DeepLinkController.Route.c, new String[] { paramLong.toString() }));
  }
  
  public static String toString(Context paramContext, DeepLinkController.Route paramRoute, String... paramVarArgs)
  {
    paramContext = new StringBuilder(paramContext.getString(2131230844));
    paramContext.append("://").append(paramRoute.f());
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      paramRoute = paramVarArgs[i];
      paramContext.append("/").append(paramRoute);
      i += 1;
    }
    paramContext = paramContext.toString();
    DVNTLog.get("DPL - uriString : " + paramContext, new Object[0]);
    return paramContext;
  }
}
