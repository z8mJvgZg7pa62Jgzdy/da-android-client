package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTAccessToken
  implements Serializable
{
  @SerializedName("expires_in")
  Long expiresIn;
  @SerializedName("access_token")
  String rawAccessToken;
  @SerializedName("refresh_token")
  String refreshToken;
  String scope;
  String status;
  @SerializedName("token_type")
  String tokenType;
  
  public DVNTAccessToken() {}
  
  public Long getExpiresIn()
  {
    return expiresIn;
  }
  
  public String getRawAccessToken()
  {
    return rawAccessToken;
  }
  
  public String getRefreshToken()
  {
    return refreshToken;
  }
  
  public String getScope()
  {
    return scope;
  }
  
  public String getStatus()
  {
    return status;
  }
  
  public String getTokenType()
  {
    return tokenType;
  }
  
  public void setExpiresIn(Long paramLong)
  {
    expiresIn = paramLong;
  }
  
  public void setRawAccessToken(String paramString)
  {
    rawAccessToken = paramString;
  }
  
  public void setRefreshToken(String paramString)
  {
    refreshToken = paramString;
  }
  
  public void setScope(String paramString)
  {
    scope = paramString;
  }
  
  public void setStatus(String paramString)
  {
    status = paramString;
  }
  
  public void setTokenType(String paramString)
  {
    tokenType = paramString;
  }
}
