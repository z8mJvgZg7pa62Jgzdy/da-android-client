package com.deviantart.android.damobile.util.submit.pages;

import android.content.ContextWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.util.submit.MimeType;
import com.deviantart.android.damobile.util.submit.Submission;
import com.facebook.drawee.view.SimpleDraweeView;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public abstract class SubmitTextPage
  extends SubmitPage
{
  @Bind({2131690044})
  SimpleDraweeView submitExtraPhoto;
  
  public SubmitTextPage(String paramString)
  {
    super(paramString);
  }
  
  public View a(ViewGroup paramViewGroup)
  {
    b(paramViewGroup);
    paramViewGroup = LayoutInflater.from(getContext()).inflate(newHandle(), paramViewGroup, false);
    ButterKnife.bind(this, paramViewGroup);
    onBindViewHolder();
    return paramViewGroup;
  }
  
  public void b()
  {
    super.b();
    ButterKnife.unbind(this);
  }
  
  public SimpleDraweeView getString()
  {
    return submitExtraPhoto;
  }
  
  abstract int newHandle();
  
  public void onTextContentChanged(CharSequence paramCharSequence)
  {
    Submission localSubmission = Submission.getInstance();
    Object localObject2 = localSubmission.getParent();
    Object localObject1 = localObject2;
    if (localObject2 != null) {}
    try
    {
      boolean bool = ((File)localObject2).exists();
      if (!bool)
      {
        localObject2 = File.createTempFile("prefix", "txt", getContext().getCacheDir());
        localObject1 = localObject2;
        localSubmission.download((File)localObject2);
        localObject2 = MimeType.c;
        localSubmission.setUrl((MimeType)localObject2);
      }
      localObject1 = new FileWriter((File)localObject1);
      ((FileWriter)localObject1).write(paramCharSequence.toString());
      ((FileWriter)localObject1).close();
    }
    catch (IOException paramCharSequence)
    {
      for (;;)
      {
        paramCharSequence.printStackTrace();
      }
    }
    localSubmission.setTitle();
  }
}
