package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.HashMap;

public class DVNTEndpointError
  implements Serializable
{
  @SerializedName("error")
  public String error;
  @SerializedName("error_code")
  public String errorCode;
  @SerializedName("error_description")
  public String errorDescription;
  @SerializedName("error_details")
  public HashMap<String, String> errorDetails;
  public Integer httpStatusCode;
  
  public DVNTEndpointError() {}
  
  public DVNTEndpointError(String paramString1, String paramString2)
  {
    error = paramString1;
    errorDescription = paramString2;
  }
  
  public String getError()
  {
    return error;
  }
  
  public String getErrorCode()
  {
    return errorCode;
  }
  
  public String getErrorDescription()
  {
    return errorDescription;
  }
  
  public HashMap getErrorDetails()
  {
    return errorDetails;
  }
  
  public Integer getHttpStatusCode()
  {
    return httpStatusCode;
  }
  
  public void setError(String paramString)
  {
    error = paramString;
  }
  
  public void setErrorCode(String paramString)
  {
    errorCode = paramString;
  }
  
  public void setErrorDescription(String paramString)
  {
    errorDescription = paramString;
  }
  
  public void setErrorDetails(HashMap paramHashMap)
  {
    errorDetails = paramHashMap;
  }
  
  public void setHttpStatusCode(Integer paramInteger)
  {
    httpStatusCode = paramInteger;
  }
}
