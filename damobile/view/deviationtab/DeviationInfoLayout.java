package com.deviantart.android.damobile.view.deviationtab;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTChallengeEntry;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviationMetadata;
import com.deviantart.android.android.package_14.model.DVNTStats;
import com.deviantart.android.android.package_14.model.DVNTSubmission;
import com.deviantart.android.android.package_14.model.DVNTTag;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.CreateNoteActivity.IntentBuilder;
import com.deviantart.android.damobile.fragment.HomeBaseFragment.HomeFragmentInstanceBuilder;
import com.deviantart.android.damobile.fragment.UserProfileFragment.InstanceBuilder;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.DAFormatUtils.DateFormats;
import com.deviantart.android.damobile.util.DAWebViewHelper;
import com.deviantart.android.damobile.util.DAWebViewHelper.WebViewParams.Builder;
import com.deviantart.android.damobile.util.DeviationCompositeModel;
import com.deviantart.android.damobile.util.DeviationCompositeModel.MetadataDeferred;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.MemberType;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.UserDisplay;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.ViewHelper;
import com.deviantart.android.damobile.util.markup.FallbackDAMLContent;
import com.deviantart.android.damobile.util.markup.FallbackDAMLContent.Builder;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.util.tracking.TrackerUtil.EventLabelBuilder;
import com.deviantart.android.damobile.view.AbstractWatchButton;
import com.deviantart.android.damobile.view.TagFlowLayout;
import com.deviantart.android.damobile.view.WatchButton;
import com.facebook.drawee.view.SimpleDraweeView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import samples.grantland.widget.AutofitHelper;

public class DeviationInfoLayout
  extends FrameLayout
{
  private DVNTDeviationMetadata c;
  private final DeviationCompositeModel walker;
  
  public DeviationInfoLayout(Context paramContext, DeviationCompositeModel paramDeviationCompositeModel, ViewGroup paramViewGroup)
  {
    super(paramContext);
    walker = paramDeviationCompositeModel;
    View localView = LayoutInflater.from(paramContext).inflate(2130968645, paramViewGroup, false);
    ButterKnife.bind(this, localView);
    AutofitHelper.doInit((TextView)ButterKnife.findById(localView, 2131689673));
    ButterKnife.findById(localView, 2131689668).setVisibility(0);
    initLayout(localView, paramDeviationCompositeModel);
    paramDeviationCompositeModel.a(paramContext).b(new DeviationInfoLayout.1(this, localView, paramViewGroup));
    ViewHelper.init(this, true);
    addView(localView);
  }
  
  private String a(String paramString)
  {
    if (!DeviationInfoLayout.ExifData.b(paramString))
    {
      Log.w("DeviationInfoLayout", "No string resource for camera metadata key: " + paramString + ". Using generic title formatting.");
      return DAFormatUtils.format(paramString);
    }
    return getResources().getString(DeviationInfoLayout.ExifData.a(paramString).a());
  }
  
  private void a(View paramView, ViewGroup paramViewGroup)
  {
    if (c.getCamera() != null)
    {
      if (c.getCamera().isEmpty()) {
        return;
      }
      paramView = (LinearLayout)ButterKnife.findById(paramView, 2131689684);
      paramView.setVisibility(0);
      HashMap localHashMap = c.getCamera();
      Object localObject = new ArrayList();
      ((ArrayList)localObject).addAll(localHashMap.keySet());
      Collections.sort((List)localObject, new DeviationInfoLayout.ExifDataRanker(null));
      localObject = ((ArrayList)localObject).iterator();
      while (((Iterator)localObject).hasNext())
      {
        String str = (String)((Iterator)localObject).next();
        if (DeviationInfoLayout.ExifData.b(str)) {
          setText(paramView, paramViewGroup, a(str), (String)localHashMap.get(str), f(str));
        }
      }
    }
  }
  
  private void b(String paramString)
  {
    if ((c != null) && (c.getAuthor() != null))
    {
      if (c.getAuthor().getUserName() == null) {
        return;
      }
      TrackerUtil.get((Activity)getContext(), EventKeys.Category.a, "tap_artist_profile", paramString);
      paramString = c.getAuthor().getUserName();
      ScreenFlowManager.a((Activity)getContext(), (Fragment)new UserProfileFragment.InstanceBuilder().a(paramString).a(paramString.equals(UserUtils.a)).a(), "user_profile" + paramString);
    }
  }
  
  private int f(String paramString)
  {
    if ((DeviationInfoLayout.ExifData.b(paramString)) && (DeviationInfoLayout.ExifData.a(paramString).b() == 0)) {
      return getResources().getColor(2131558484);
    }
    return getResources().getColor(17170443);
  }
  
  private String getVersion(String paramString)
  {
    paramString = DAFormatUtils.DateFormats.d;
    SimpleDateFormat localSimpleDateFormat = DAFormatUtils.DateFormats.c;
    DVNTDeviationMetadata localDVNTDeviationMetadata = c;
    try
    {
      paramString = paramString.format(localSimpleDateFormat.parse(localDVNTDeviationMetadata.getSubmission().getCreationTime()));
      return paramString;
    }
    catch (ParseException paramString) {}
    return "Unknown";
  }
  
  private void init(String paramString1, String paramString2, boolean paramBoolean, int paramInt, ViewGroup paramViewGroup1, ViewGroup paramViewGroup2)
  {
    paramViewGroup2 = LayoutInflater.from(getContext()).inflate(2130968647, paramViewGroup2, false);
    TextView localTextView1 = (TextView)paramViewGroup2.findViewById(2131689686);
    TextView localTextView2 = (TextView)paramViewGroup2.findViewById(2131689687);
    localTextView1.setText(paramString1 + ":");
    localTextView2.setText(paramString2);
    if (paramBoolean)
    {
      localTextView1.setTextSize(0, getResources().getDimension(2131361978));
      localTextView2.setTextSize(0, getResources().getDimension(2131361978));
    }
    localTextView2.setTextColor(paramInt);
    paramViewGroup1.addView(paramViewGroup2);
  }
  
  private void initLayout(View paramView, DeviationCompositeModel paramDeviationCompositeModel)
  {
    if ((paramDeviationCompositeModel == null) || (paramDeviationCompositeModel.getItem() == null) || (paramDeviationCompositeModel.getItem().getChallengeEntry() == null))
    {
      paramView.findViewById(2131689677).setVisibility(8);
      return;
    }
    DVNTChallengeEntry localDVNTChallengeEntry = paramDeviationCompositeModel.getItem().getChallengeEntry();
    ImageView localImageView = (ImageView)paramView.findViewById(2131689663);
    paramView = (TextView)paramView.findViewById(2131689664);
    localImageView.setOnClickListener(new DeviationInfoLayout.2(this, paramDeviationCompositeModel, localDVNTChallengeEntry));
    paramView.setText(localDVNTChallengeEntry.getChallengeTitle());
  }
  
  private void initUI(View paramView, ViewGroup paramViewGroup)
  {
    paramView = (TagFlowLayout)ButterKnife.findById(paramView, 2131689679);
    Object localObject = c.getTags();
    if ((localObject == null) || (((ArrayList)localObject).isEmpty()))
    {
      View.inflate(getContext(), 2130968646, paramView);
      return;
    }
    localObject = ((ArrayList)localObject).iterator();
    while (((Iterator)localObject).hasNext())
    {
      DVNTTag localDVNTTag = (DVNTTag)((Iterator)localObject).next();
      View localView = LayoutInflater.from(getContext()).inflate(2130968782, paramViewGroup, false);
      Button localButton = (Button)localView.findViewById(2131690064);
      localButton.setText(localDVNTTag.getTagName());
      localButton.setOnClickListener(new DeviationInfoLayout.3(this, localDVNTTag));
      paramView.addView(localView);
    }
  }
  
  private void onCreateView(View paramView)
  {
    paramView = (WebView)paramView.findViewById(2131689678);
    paramView.setFocusableInTouchMode(false);
    paramView.setFocusable(false);
    if (!c.getDescription().isEmpty())
    {
      DAWebViewHelper.WebViewParams.Builder localBuilder = new DAWebViewHelper.WebViewParams.Builder();
      localBuilder.b(Integer.valueOf(20));
      DAWebViewHelper.onCreateView(paramView, new FallbackDAMLContent.Builder().c(c.getDescription()).a().c(), "", localBuilder.a());
      return;
    }
    paramView.setVisibility(8);
  }
  
  private void onCreateView(View paramView, ViewGroup paramViewGroup)
  {
    paramView = (LinearLayout)ButterKnife.findById(paramView, 2131689683);
    setText(paramView, paramViewGroup, getContext().getString(2131231045), toString(c.getLicense()));
    setText(paramView, paramViewGroup, getContext().getString(2131231381), getVersion(c.getSubmission().getCreationTime()));
  }
  
  private void setText(ViewGroup paramViewGroup1, ViewGroup paramViewGroup2, String paramString1, String paramString2)
  {
    init(paramString1, paramString2, true, getResources().getColor(17170443), paramViewGroup1, paramViewGroup2);
  }
  
  private void setText(ViewGroup paramViewGroup1, ViewGroup paramViewGroup2, String paramString1, String paramString2, int paramInt)
  {
    init(paramString1, paramString2, false, paramInt, paramViewGroup1, paramViewGroup2);
  }
  
  private String toString(String paramString)
  {
    paramString = new StringBuilder("?");
    Object localObject1 = DAFormatUtils.DateFormats.a;
    Object localObject2 = DAFormatUtils.DateFormats.c;
    DVNTDeviationMetadata localDVNTDeviationMetadata = c;
    for (;;)
    {
      try
      {
        localObject1 = ((SimpleDateFormat)localObject1).format(((SimpleDateFormat)localObject2).parse(localDVNTDeviationMetadata.getSubmission().getCreationTime()));
        localObject2 = DAFormatUtils.DateFormats.a;
        localObject2 = ((SimpleDateFormat)localObject2).format(new Date());
        boolean bool = ((String)localObject1).equalsIgnoreCase((String)localObject2);
        if (!bool) {
          continue;
        }
        localObject1 = paramString.append((String)localObject2).append(" ");
        localObject2 = c;
        ((StringBuilder)localObject1).append(((DVNTDeviationMetadata)localObject2).getAuthor().getUserName());
      }
      catch (ParseException localParseException)
      {
        Log.e("makeLicense", "Failed to format date for copyright");
        paramString.append(" ").append(c.getAuthor().getUserName());
        continue;
      }
      if (!c.getLicense().equalsIgnoreCase("no license")) {
        paramString.append("\n").append(c.getLicense());
      }
      return paramString.toString();
      localObject1 = paramString.append((String)localObject1).append("-").append((String)localObject2).append(" ");
      localObject2 = c;
      ((StringBuilder)localObject1).append(((DVNTDeviationMetadata)localObject2).getAuthor().getUserName());
    }
  }
  
  private void update(TextView paramTextView, String paramString)
  {
    Drawable[] arrayOfDrawable = paramTextView.getCompoundDrawables();
    int j = arrayOfDrawable.length;
    int i = 0;
    if (i < j)
    {
      Drawable localDrawable = arrayOfDrawable[i];
      if (localDrawable == null) {}
      for (;;)
      {
        i += 1;
        break;
        localDrawable.setAlpha(77);
      }
    }
    paramTextView.setText(paramString);
  }
  
  public void clickAuthorAvatar()
  {
    b(new TrackerUtil.EventLabelBuilder().get("source", "avatar").getValue());
  }
  
  public void clickAuthorName()
  {
    b(new TrackerUtil.EventLabelBuilder().get("source", "username").getValue());
  }
  
  public void clickReportDeviation()
  {
    NavigationUtils.openBrowser(getContext(), getContext().getString(2131231237) + c.getDeviationId());
  }
  
  public void clickSendNoteButton()
  {
    DVNTAbstractAsyncAPI.cancelAllRequests();
    Activity localActivity = (Activity)getContext();
    DVNTDeviation localDVNTDeviation = walker.getItem();
    TrackerUtil.get(localActivity, EventKeys.Category.d, "create_note", "deviation_view");
    localActivity.startActivityForResult(new CreateNoteActivity.IntentBuilder().a(localDVNTDeviation.getAuthor()).a(localActivity), 110);
    localActivity.overridePendingTransition(2131034146, 2131034113);
  }
  
  public void onCreateView(DVNTDeviationMetadata paramDVNTDeviationMetadata, View paramView, ViewGroup paramViewGroup)
  {
    RelativeLayout localRelativeLayout = (RelativeLayout)ButterKnife.findById(paramView, 2131689670);
    Object localObject = (LinearLayout)ButterKnife.findById(paramView, 2131689674);
    WatchButton localWatchButton = (WatchButton)ButterKnife.findById(paramView, 2131689675);
    SimpleDraweeView localSimpleDraweeView = (SimpleDraweeView)ButterKnife.findById(paramView, 2131689672);
    TextView localTextView4 = (TextView)ButterKnife.findById(paramView, 2131689673);
    TextView localTextView5 = (TextView)ButterKnife.findById(paramView, 2131689671);
    TextView localTextView1 = (TextView)ButterKnife.findById(paramView, 2131689680);
    TextView localTextView2 = (TextView)ButterKnife.findById(paramView, 2131689681);
    TextView localTextView3 = (TextView)ButterKnife.findById(paramView, 2131689682);
    LinearLayout localLinearLayout = (LinearLayout)ButterKnife.findById(paramView, 2131689668);
    TextView localTextView6 = (TextView)ButterKnife.findById(paramView, 2131689685);
    if ((UserUtils.a != null) && (localTextView6 != null)) {
      localTextView6.setVisibility(0);
    }
    c = paramDVNTDeviationMetadata;
    if (DVNTContextUtils.isContextDead(getContext())) {
      return;
    }
    ImageUtils.isEmpty(localSimpleDraweeView, Uri.parse(paramDVNTDeviationMetadata.getAuthor().getUserIconURL()));
    localTextView5.setText(paramDVNTDeviationMetadata.getTitle());
    localTextView4.setText(UserDisplay.a(getContext(), paramDVNTDeviationMetadata.getAuthor(), true));
    if ((UserUtils.a == null) || (!UserUtils.a.equals(paramDVNTDeviationMetadata.getAuthor().getUserName())))
    {
      ((View)localObject).setVisibility(0);
      localObject = MemberType.toString(paramDVNTDeviationMetadata.getAuthor().getType());
      localWatchButton.a(paramDVNTDeviationMetadata.getAuthor().getUserName(), (MemberType)localObject, paramDVNTDeviationMetadata.isWatchingAuthor());
      localWatchButton.setEventSource("deviation_info");
    }
    onCreateView(paramView);
    update(localTextView1, DAFormatUtils.format(paramDVNTDeviationMetadata.getStats().getViews()));
    update(localTextView2, DAFormatUtils.format(paramDVNTDeviationMetadata.getStats().getFavourites()));
    update(localTextView3, DAFormatUtils.format(paramDVNTDeviationMetadata.getStats().getComments()));
    initUI(paramView, paramViewGroup);
    onCreateView(paramView, paramViewGroup);
    a(paramView, paramViewGroup);
    localLinearLayout.setVisibility(8);
    localRelativeLayout.setVisibility(0);
  }
}
