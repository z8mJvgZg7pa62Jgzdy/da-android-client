package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.util.MemberType;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import java.util.HashMap;

public abstract class AbstractWatchButton
  extends FrameLayout
  implements View.OnClickListener
{
  private static final String a = WatchButton.class.getName();
  public static final HashMap<String, Boolean> c = new HashMap();
  private MemberType b;
  private String d;
  AbstractWatchButton.ButtonState i;
  protected boolean k = false;
  private String l;
  private View.OnClickListener mClickListener;
  
  public AbstractWatchButton(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public AbstractWatchButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public AbstractWatchButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void a(boolean paramBoolean)
  {
    c.put(l, Boolean.valueOf(paramBoolean));
    UserUtils.connect(getContext(), paramBoolean);
  }
  
  private void b()
  {
    setButtonState(AbstractWatchButton.ButtonState.i);
    TrackerUtil.b((Activity)getContext(), b, "unwatch", d);
    if (!l()) {
      a(false);
    }
    String str = l;
    DVNTCommonAsyncAPI.unwatch(str).call(getContext(), new AbstractWatchButton.2(this, str));
  }
  
  private void c()
  {
    setButtonState(AbstractWatchButton.ButtonState.i);
    TrackerUtil.b((Activity)getContext(), b, "watch", d);
    if (!l()) {
      a(true);
    }
    String str = l;
    DVNTCommonAsyncAPI.watch(str).call(getContext(), new AbstractWatchButton.1(this, str));
  }
  
  private void d()
  {
    setButtonState(AbstractWatchButton.ButtonState.i);
    DVNTCommonAsyncAPI.watching(l).call(getContext(), new AbstractWatchButton.3(this));
  }
  
  private void init(Context paramContext)
  {
    LayoutInflater.from(paramContext).inflate(getLayoutRes(), this, true);
    ButterKnife.bind(this);
    i = AbstractWatchButton.ButtonState.i;
    setOnClickListener(this);
    setVisibility();
  }
  
  public void a(View.OnClickListener paramOnClickListener)
  {
    mClickListener = paramOnClickListener;
  }
  
  public void a(String paramString, MemberType paramMemberType, Boolean paramBoolean)
  {
    k = false;
    if ((paramString == null) || (paramString.isEmpty()))
    {
      Log.e(a, "Need a valid username");
      return;
    }
    l = paramString;
    b = paramMemberType;
    if (!DVNTAbstractAsyncAPI.isUserSession(getContext()))
    {
      setButtonState(AbstractWatchButton.ButtonState.b);
      return;
    }
    if (c.containsKey(paramString)) {
      paramBoolean = (Boolean)c.get(paramString);
    }
    for (;;)
    {
      if (paramBoolean == null)
      {
        Log.w(a, "No watch state provided, calling watching endpoint. Ideally you should pre-populate.");
        d();
        return;
      }
      if (paramBoolean.booleanValue()) {
        setButtonState(AbstractWatchButton.ButtonState.d);
      }
      for (;;)
      {
        k = true;
        return;
        setButtonState(AbstractWatchButton.ButtonState.c);
      }
    }
  }
  
  protected abstract void b(AbstractWatchButton.ButtonState paramButtonState1, AbstractWatchButton.ButtonState paramButtonState2);
  
  protected abstract int getLayoutRes();
  
  protected boolean l()
  {
    return true;
  }
  
  public void onClick(View paramView)
  {
    if (!i.o) {
      return;
    }
    switch (AbstractWatchButton.4.d[i.ordinal()])
    {
    default: 
      break;
    }
    while (mClickListener != null)
    {
      mClickListener.onClick(paramView);
      return;
      b();
      continue;
      c();
    }
  }
  
  public void setButtonState(AbstractWatchButton.ButtonState paramButtonState)
  {
    AbstractWatchButton.ButtonState localButtonState = i;
    i = paramButtonState;
    if ((!o) && (o)) {
      setVisibility();
    }
    for (;;)
    {
      b(localButtonState, paramButtonState);
      return;
      if ((o) && (!o)) {
        switchToLoadingView();
      }
    }
  }
  
  public void setEventSource(String paramString)
  {
    d = paramString;
  }
  
  protected abstract void setVisibility();
  
  protected abstract void switchToLoadingView();
}
