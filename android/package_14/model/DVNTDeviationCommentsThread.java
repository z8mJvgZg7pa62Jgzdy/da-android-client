package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTDeviationCommentsThread
  implements Serializable
{
  @SerializedName("thread")
  private DVNTComment.List comments;
  @SerializedName("has_more")
  private Boolean hasMore;
  @SerializedName("next_offset")
  private Integer nextOffset;
  
  public DVNTDeviationCommentsThread() {}
  
  public DVNTComment.List getComments()
  {
    return comments;
  }
  
  public Boolean getHasMore()
  {
    return hasMore;
  }
  
  public Integer getNextOffset()
  {
    return nextOffset;
  }
  
  public void setComments(DVNTComment.List paramList)
  {
    comments = paramList;
  }
  
  public void setHasMore(Boolean paramBoolean)
  {
    hasMore = paramBoolean;
  }
  
  public void setNextOffset(Integer paramInteger)
  {
    nextOffset = paramInteger;
  }
}
