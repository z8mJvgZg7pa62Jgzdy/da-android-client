package com.deviantart.android.damobile.view.thirdparty;

import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTComment;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.util.DailyDeviationItem;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.TodayDailyDeviationItem;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.view.gom.Gom;
import com.deviantart.android.damobile.view.thirdparty.comment.CommentGom;
import com.deviantart.android.damobile.view.thirdparty.comment.HiddenCommentGom;
import com.deviantart.android.damobile.view.thirdparty.comment.RegularCommentGom;
import com.deviantart.android.damobile.view.thirdparty.deviation.DeviationGom;
import com.deviantart.android.damobile.view.thirdparty.deviation.JournalGom;
import com.deviantart.android.damobile.view.thirdparty.deviation.LiteratureGom;
import com.deviantart.android.damobile.view.thirdparty.deviation.PlaceholderGom;
import com.deviantart.android.damobile.view.thirdparty.deviation.RegularImageGom;
import com.deviantart.android.damobile.view.thirdparty.deviation.SmallOrAnimatedImageGom;
import com.deviantart.android.damobile.view.thirdparty.deviation.UnknownGom;
import com.deviantart.android.damobile.view.thirdparty.preview.DeviationPreviewGom;
import java.util.HashMap;

public class GomFactory
{
  private static final HashMap<com.deviantart.android.damobile.view.gom.GomType, Gom> m = new HashMap();
  private static final String[] s = { "journals", "personaljournal" };
  
  public GomFactory() {}
  
  public static ByteVector a(GomType paramGomType)
  {
    if (m.containsKey(paramGomType)) {
      return (ByteVector)m.get(paramGomType);
    }
    switch (GomFactory.1.a[paramGomType.ordinal()])
    {
    default: 
      throw new RuntimeException("Missing GomType in GomFactory");
    case 1: 
      m.put(paramGomType, new RegularImageGom());
    }
    for (;;)
    {
      return (ByteVector)m.get(paramGomType);
      m.put(paramGomType, new SmallOrAnimatedImageGom());
      continue;
      m.put(paramGomType, new JournalGom());
      continue;
      m.put(paramGomType, new LiteratureGom());
      continue;
      m.put(paramGomType, new PlaceholderGom());
      continue;
      m.put(paramGomType, new UnknownGom());
      continue;
      m.put(paramGomType, new RegularCommentGom());
      continue;
      m.put(paramGomType, new HiddenCommentGom());
      continue;
      m.put(paramGomType, new DeviationPreviewGom());
    }
  }
  
  public static CommentGom a(DVNTComment paramDVNTComment)
  {
    if (paramDVNTComment.getHidden() == null) {
      return (CommentGom)a(GomType.g);
    }
    return (CommentGom)a(GomType.l);
  }
  
  public static DeviationGom a(DVNTDeviation paramDVNTDeviation)
  {
    if (paramDVNTDeviation.getId().equals("placeholder")) {
      return (DeviationGom)a(GomType.i);
    }
    if (paramDVNTDeviation.getVideos() != null) {
      return (DeviationGom)a(GomType.r);
    }
    if (paramDVNTDeviation.getContent() != null)
    {
      if (ImageUtils.a(DeviationUtils.b(paramDVNTDeviation))) {
        return (DeviationGom)a(GomType.d);
      }
      return (DeviationGom)a(GomType.c);
    }
    if (paramDVNTDeviation.getExcerpt() != null)
    {
      String[] arrayOfString = s;
      int j = arrayOfString.length;
      int i = 0;
      while (i < j)
      {
        String str = arrayOfString[i];
        if (paramDVNTDeviation.getCategoryPath().startsWith(str)) {
          return (DeviationGom)a(GomType.e);
        }
        i += 1;
      }
      return (DeviationGom)a(GomType.a);
    }
    return (DeviationGom)a(GomType.r);
  }
  
  public static ByteVector b(DailyDeviationItem paramDailyDeviationItem)
  {
    switch (GomFactory.1.d[paramDailyDeviationItem.getState().ordinal()])
    {
    default: 
      throw new RuntimeException("Unexpected MixedDailyDeviationItem ItemType");
    case 1: 
      return a(((TodayDailyDeviationItem)paramDailyDeviationItem).a());
    }
    return a(GomType.b);
  }
}
