package com.deviantart.android.damobile.util;

import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.widget.Toast;
import com.deviantart.android.android.utils.DVNTContextUtils;
import java.util.ArrayList;

public class DeviationDownloadReceiver
  extends BroadcastReceiver
{
  public DeviationDownloadReceiver() {}
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (DVNTContextUtils.isContextDead(paramContext)) {
      return;
    }
    paramIntent = Long.valueOf(paramIntent.getLongExtra("extra_download_id", 0L));
    if (DeviationDownloadManager.this$0.contains(paramIntent))
    {
      DeviationDownloadManager.this$0.remove(paramIntent);
      DownloadManager localDownloadManager = (DownloadManager)paramContext.getSystemService("download");
      DownloadManager.Query localQuery = new DownloadManager.Query();
      localQuery.setFilterById(new long[] { paramIntent.longValue() });
      paramIntent = localDownloadManager.query(localQuery);
      if ((paramIntent.moveToFirst()) && (8 == paramIntent.getInt(paramIntent.getColumnIndex("status")))) {
        Toast.makeText(paramContext, 2131230866, 0).show();
      }
    }
  }
}
