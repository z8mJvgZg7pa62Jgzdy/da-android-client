package com.deviantart.android.damobile.util.submit;

import com.deviantart.android.android.package_14.model.DVNTPublishOptions.DVNTLicenseModificationOption;

public class SubmitLicenseOptions
{
  private boolean b = false;
  private boolean l = false;
  private DVNTPublishOptions.DVNTLicenseModificationOption tail = DVNTPublishOptions.DVNTLicenseModificationOption.UNSAVE;
  
  public SubmitLicenseOptions() {}
  
  public boolean a()
  {
    return b;
  }
  
  public boolean equals()
  {
    return l;
  }
  
  public DVNTPublishOptions.DVNTLicenseModificationOption equalsIgnoreCase()
  {
    return tail;
  }
  
  public void goTo(boolean paramBoolean)
  {
    b = paramBoolean;
  }
  
  public void inc(boolean paramBoolean)
  {
    l = paramBoolean;
  }
  
  public void put(DVNTPublishOptions.DVNTLicenseModificationOption paramDVNTLicenseModificationOption)
  {
    tail = paramDVNTLicenseModificationOption;
  }
}
