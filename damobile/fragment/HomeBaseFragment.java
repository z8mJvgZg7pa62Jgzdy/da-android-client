package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.activity.HomeActivity;
import com.deviantart.android.damobile.activity.HomeActivity.BottomBarButton;
import com.deviantart.android.damobile.activity.HomeActivity.HomeActivityPages;
import com.deviantart.android.damobile.activity.UserSettingsActivity;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.util.AppBarStateChangeListener;
import com.deviantart.android.damobile.util.AppBarStateChangeListener.AppBarStateChangeNotifier;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnCommentPostedEvent;
import com.deviantart.android.damobile.util.BusStation.OnNoteItemChangeEvent;
import com.deviantart.android.damobile.util.BusStation.OnNotesItemMarkUpdated;
import com.deviantart.android.damobile.util.BusStation.OnNotesRefreshAll;
import com.deviantart.android.damobile.util.BusStation.OnNotificationItemChangeEvent;
import com.deviantart.android.damobile.util.BusStation.OnSignificantScrollEvent;
import com.deviantart.android.damobile.util.BusStation.OnUserSettingChangeEvent;
import com.deviantart.android.damobile.util.DAAnimationUtils;
import com.deviantart.android.damobile.util.DAAnimationUtils.AnimationEndListener;
import com.deviantart.android.damobile.util.FloatingButtonDescription;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.UserSettingUpdateType;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.notes.NotesFolders;
import com.deviantart.android.damobile.util.notes.NotesFoldersLoader;
import com.deviantart.android.damobile.view.tooltip.DAToolTipRelativeLayout;
import com.squareup.otto.Bus;

public abstract class HomeBaseFragment
  extends DABaseFragment
  implements AppBarStateChangeListener.AppBarStateChangeNotifier
{
  private HomeBaseFragment.Subscriber TAG = new HomeBaseFragment.Subscriber(this, null);
  @Bind({2131689754})
  AppBarLayout appBarLayout;
  private boolean b;
  private boolean c = false;
  private DAAnimationUtils.AnimationEndListener m = HomeBaseFragment..Lambda.1.getColor(this);
  protected FloatingButtonDescription t;
  @Bind({2131689752})
  Toolbar toolbar;
  
  public HomeBaseFragment() {}
  
  public void a()
  {
    setTitle();
  }
  
  protected void a(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    paramMenuInflater.inflate(2131820550, paramMenu);
    MenuItemCompat.getActionView(paramMenu.findItem(2131690157)).setOnClickListener(HomeBaseFragment..Lambda.3.finish(this));
  }
  
  public void addClicked() {}
  
  public void b(BusStation.OnCommentPostedEvent paramOnCommentPostedEvent) {}
  
  public void b(BusStation.OnNoteItemChangeEvent paramOnNoteItemChangeEvent) {}
  
  public void b(BusStation.OnNotesItemMarkUpdated paramOnNotesItemMarkUpdated) {}
  
  public void b(BusStation.OnNotesRefreshAll paramOnNotesRefreshAll) {}
  
  public void b(BusStation.OnNotificationItemChangeEvent paramOnNotificationItemChangeEvent) {}
  
  public boolean b()
  {
    return false;
  }
  
  protected abstract HomeActivity.BottomBarButton c();
  
  protected void clearVolume() {}
  
  protected boolean delete4()
  {
    return true;
  }
  
  public FrameLayout getItem()
  {
    return ((HomeActivity)getActivity()).createFragment();
  }
  
  public void goTo()
  {
    BusStation.get().post(new BusStation.OnSignificantScrollEvent(true));
  }
  
  protected void init()
  {
    if (HomeBaseFragment.Subscriber.d(TAG)) {
      return;
    }
    Bundle localBundle = getArguments();
    FrameLayout localFrameLayout = getItem();
    if ((localBundle != null) && (t == null)) {
      t = ((FloatingButtonDescription)localBundle.getSerializable("home_floating_bottom_button"));
    }
    if (t != null) {}
    for (boolean bool = true;; bool = false)
    {
      b = bool;
      if (!b) {
        break;
      }
      ((TextView)ButterKnife.findById(localFrameLayout, 2131690146)).setText(t.e());
      localFrameLayout.setOnClickListener(t.lambdaFactory$());
      c = true;
      DAAnimationUtils.show(localFrameLayout, 2131034122, true, m);
      return;
    }
    localFrameLayout.setVisibility(8);
  }
  
  protected void initUI()
  {
    if (toolbar == null) {
      return;
    }
    setHasOptionsMenu(true);
    ((BaseActivity)getActivity()).setSupportActionBar(toolbar);
    toolbar.setOverflowIcon(ContextCompat.getDrawable(getActivity(), 2130837736));
  }
  
  public void loadQuery()
  {
    ScreenFlowManager.get(getActivity(), new SearchFragment(), HomeActivity.HomeActivityPages.i.a(), true);
  }
  
  public void logPreferences() {}
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    BusStation.get().register(TAG);
    init();
    ((HomeActivity)getActivity()).onCreate();
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    BusStation.get().unregister(TAG);
    StreamCacher.a();
  }
  
  public void onOptionsItemSelected()
  {
    if (UserUtils.a == null) {
      load();
    }
    getActivity().invalidateOptionsMenu();
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    case 2131690155: 
      loadQuery();
      return true;
    case 2131690154: 
      openRandomArticle();
      return true;
    case 2131690156: 
      showAbout();
      return true;
    }
    DVNTAbstractAsyncAPI.clearCache();
    return true;
  }
  
  public void onPostExecute(BusStation.OnUserSettingChangeEvent paramOnUserSettingChangeEvent)
  {
    if (DVNTContextUtils.isContextDead(getActivity())) {
      return;
    }
    if (paramOnUserSettingChangeEvent.getValue().equals(UserSettingUpdateType.c)) {
      getActivity().invalidateOptionsMenu();
    }
  }
  
  public void onResume()
  {
    ((HomeActivity)getActivity()).showTabs(refresh());
    if (UserUtils.c(getActivity())) {
      load();
    }
    ((HomeActivity)getActivity()).a(c());
    NotesFoldersLoader localNotesFoldersLoader = NotesFolders.a();
    if ((DVNTAbstractAsyncAPI.isUserSession(getActivity())) && (delete4()))
    {
      if (localNotesFoldersLoader.a()) {
        break label83;
      }
      localNotesFoldersLoader.b(getActivity());
    }
    for (;;)
    {
      super.onResume();
      return;
      label83:
      if (localNotesFoldersLoader.delete(getActivity())) {
        localNotesFoldersLoader.c(getActivity());
      }
    }
  }
  
  public DAToolTipRelativeLayout onSuccess()
  {
    return ((HomeActivity)getActivity()).getPagerAdapter();
  }
  
  public void openRandomArticle()
  {
    DVNTAbstractAsyncAPI.graduate(getActivity());
  }
  
  protected boolean refresh()
  {
    return true;
  }
  
  public void sendComment() {}
  
  protected void setExpanded()
  {
    if (appBarLayout == null) {
      return;
    }
    appBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener(this));
  }
  
  public void setTitle()
  {
    getActivity().invalidateOptionsMenu();
  }
  
  public void showAbout()
  {
    if (DVNTAbstractAsyncAPI.isUserSession(getActivity()))
    {
      getActivity().startActivityForResult(UserSettingsActivity.startActivity(getActivity()), 105);
      getActivity().overridePendingTransition(2131034138, 2131034142);
      return;
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
    localBuilder.setTitle(getString(2131231265)).setMessage(getString(2131231266)).setNeutralButton(getString(2131231183), HomeBaseFragment..Lambda.2.lambdaFactory$());
    localBuilder.create().show();
  }
  
  public void updateContent()
  {
    getActivity().invalidateOptionsMenu();
  }
  
  public void validateInput()
  {
    BusStation.get().post(new BusStation.OnSignificantScrollEvent(false));
  }
  
  public void visitFrame()
  {
    if (UserUtils.a == null) {}
    for (String str = "";; str = UserUtils.a)
    {
      DVNTAbstractAsyncAPI.cancelAllRequests();
      ScreenFlowManager.a(getActivity(), (Fragment)new UserProfileFragment.InstanceBuilder().a(str).a(true).a(), HomeActivity.HomeActivityPages.b.a() + UserUtils.a);
      return;
    }
  }
  
  public void visitTableSwitchInsn() {}
  
  protected void writeNew() {}
}
