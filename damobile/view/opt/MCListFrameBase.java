package com.deviantart.android.damobile.view.opt;

import android.app.Activity;
import android.content.Context;
import android.os.BaseBundle;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.adapter.StatefulPagerAdapter.PageView;
import com.deviantart.android.damobile.stream.Stream.Notifiable;
import com.deviantart.android.damobile.stream.loader.StreamLoader.ErrorType;
import com.deviantart.android.damobile.view.DASwipeRefreshLayout;
import com.deviantart.android.damobile.view.ViewState;
import com.deviantart.android.damobile.view.ViewState.Helper;
import com.deviantart.android.damobile.view.ViewState.State;

public abstract class MCListFrameBase
  extends FrameLayout
  implements SwipeRefreshLayout.OnRefreshListener, StatefulPagerAdapter.PageView, Stream.Notifiable
{
  View a;
  protected MCListRecyclerView d;
  private ViewState i = new ViewState();
  @Bind({2131689982})
  protected DASwipeRefreshLayout refreshLayout;
  @Bind({2131689983})
  FrameLayout stateLayout;
  
  public MCListFrameBase(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public MCListFrameBase(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  private void setViewState(ViewState.State paramState)
  {
    stateLayout.removeAllViews();
    stateLayout.setVisibility(8);
    i.a(paramState);
    if (paramState != null)
    {
      if (DVNTContextUtils.isContextDead(getContext())) {
        return;
      }
      switch (MCListFrameBase.1.u[paramState.ordinal()])
      {
      default: 
        break;
      }
      for (;;)
      {
        stateLayout.addView(a);
        stateLayout.setVisibility(0);
        return;
        a = ViewState.Helper.a((Activity)getContext(), a, this, i);
        continue;
        a = ViewState.Helper.b((Activity)getContext(), a, this, i);
      }
    }
  }
  
  public void b()
  {
    if (!refreshLayout.remove()) {
      d.setLoading(false);
    }
    setViewState(ViewState.State.b);
  }
  
  public void b(StreamLoader.ErrorType paramErrorType, String paramString)
  {
    if (!refreshLayout.remove()) {
      d.setLoading(false);
    }
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("state_error", paramErrorType);
    localBundle.putString("state_msg", paramString);
    b(ViewState.State.i, localBundle);
    setViewState(ViewState.State.i);
  }
  
  public void b(ViewState.State paramState, Bundle paramBundle)
  {
    i.a(paramState, paramBundle);
  }
  
  public void c()
  {
    setViewState(null);
    if (!refreshLayout.remove()) {
      d.setLoading(false);
    }
    d.getAdapter().b();
    if (d.getAdapter().e() <= 0) {
      b();
    }
  }
  
  public void c(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      d.d();
      return;
    }
    d.a();
  }
  
  public void e()
  {
    if (refreshLayout.remove()) {
      return;
    }
    d.setLoading(true);
  }
  
  public void f()
  {
    if (refreshLayout.remove()) {
      refreshLayout.setRefreshing(false);
    }
  }
  
  public Parcelable getPageState()
  {
    return d.getLayoutManager().onSaveInstanceState();
  }
  
  public String getPageTag()
  {
    return (String)super.getTag();
  }
  
  public abstract MCListRecyclerView getRecyclerView();
  
  public void init(Context paramContext)
  {
    View.inflate(paramContext, 2130968740, this);
    ButterKnife.bind(this);
    d = unread(paramContext);
    refreshLayout.addView(d);
    refreshLayout.setOnRefreshListener(this);
    d.setNotifiable(this);
  }
  
  public void onSaveInstanceState(Parcelable paramParcelable)
  {
    d.getLayoutManager().onRestoreInstanceState(paramParcelable);
  }
  
  public void refreshView()
  {
    refreshLayout.setRefreshing(true);
    d.d();
  }
  
  public void setEmptyMessage(String paramString)
  {
    i.a(paramString);
  }
  
  public void setScrollUnderSize(int paramInt)
  {
    refreshLayout.setProgressViewOffset(paramInt);
    if (d != null)
    {
      if (d.getAdapter() == null) {
        return;
      }
      d.getAdapter().putShort(paramInt);
    }
  }
  
  public abstract MCListRecyclerView unread(Context paramContext);
  
  public void visitFrame()
  {
    c(false);
  }
}
