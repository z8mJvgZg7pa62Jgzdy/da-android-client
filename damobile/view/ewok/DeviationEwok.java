package com.deviantart.android.damobile.view.ewok;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StaticStreamLoader;
import com.deviantart.android.damobile.util.DeviationType;

public abstract class DeviationEwok
  implements Ewok
{
  private Stream<com.deviantart.android.sdk.api.model.DVNTDeviation> c;
  protected com.deviantart.android.android.package_14.model.DVNTDeviation d;
  protected Integer issue_id;
  
  public DeviationEwok(com.deviantart.android.android.package_14.model.DVNTDeviation paramDVNTDeviation)
  {
    d = paramDVNTDeviation;
    c = null;
  }
  
  public com.deviantart.android.android.package_14.model.DVNTDeviation a()
  {
    return d;
  }
  
  public abstract View b(Context paramContext);
  
  public DVNTImage b(Activity paramActivity)
  {
    return null;
  }
  
  public abstract DeviationType b();
  
  public Stream c()
  {
    if (c != null) {
      return c;
    }
    if (d != null) {
      c = StreamCacher.a(new StaticStreamLoader(d, "staticstreamloader-deviation-" + d.getId()));
    }
    return c;
  }
  
  public Integer getIssueId()
  {
    return issue_id;
  }
}
