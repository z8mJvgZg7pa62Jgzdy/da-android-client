package com.deviantart.android.damobile.util.torpedo;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.os.BaseBundle;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.R.styleable;
import com.deviantart.android.damobile.fragment.FullTorpedoFragment;
import com.deviantart.android.damobile.fragment.FullTorpedoFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.HomeBaseFragment.HomeFragmentInstanceBuilder;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.Stream.Notifiable;
import com.deviantart.android.damobile.stream.loader.StreamLoader.ErrorType;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.DeviationUtils.ProcessMenuAddable;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.util.ProcessMenuListener;
import com.deviantart.android.damobile.util.ProcessMenuType;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.view.ViewState;
import com.deviantart.android.damobile.view.ViewState.Helper;
import com.deviantart.android.damobile.view.ViewState.State;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TorpedoPreview
  extends FrameLayout
  implements Stream.Notifiable, DeviationUtils.ProcessMenuAddable, ProcessMenuListener
{
  private static int height = -1;
  private Stream<com.deviantart.android.sdk.api.model.DVNTDeviation> a;
  private ViewState b;
  private String c;
  private String d;
  private String e;
  private boolean h = true;
  private int k;
  private int l;
  private int m;
  private ProcessMenuListener o;
  private boolean p = true;
  private String q;
  private boolean r = false;
  @Bind({2131690072})
  View titleContainer;
  @Bind({2131690075})
  TextView torpedoDetail;
  @Bind({2131690076})
  LinearLayout torpedoMainView;
  @Bind({2131690074})
  TextView torpedoSubTitle;
  @Bind({2131690073})
  TextView torpedoTitle;
  private int u;
  private String v;
  private int x;
  private String z;
  
  public TorpedoPreview(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public TorpedoPreview(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    LayoutInflater.from(paramContext).inflate(2130968785, this, true);
    ButterKnife.bind(this);
    putByte();
    setWillNotDraw(false);
    paramAttributeSet = paramContext.getTheme().obtainStyledAttributes(paramAttributeSet, R.styleable.TorpedoGrid, 0, 0);
    try
    {
      String str = paramAttributeSet.getString(1);
      u = paramAttributeSet.getLayoutDimension(5, paramContext.getResources().getDimensionPixelOffset(2131361990));
      l = paramAttributeSet.getInt(4, 2);
      x = paramAttributeSet.getInt(6, 2);
      m = paramAttributeSet.getLayoutDimension(2, Graphics.add(paramContext, 200));
      h = paramAttributeSet.getBoolean(7, true);
      paramAttributeSet.recycle();
      if (str != null)
      {
        setTitle(str);
        return;
      }
    }
    catch (Throwable paramContext)
    {
      paramAttributeSet.recycle();
      throw paramContext;
    }
  }
  
  private View a(View paramView)
  {
    paramView.setLayoutParams(new LinearLayout.LayoutParams(-1, Graphics.add(getContext(), 200)));
    paramView.setPadding(8, 8, 8, 8);
    return paramView;
  }
  
  private void putByte()
  {
    b = new ViewState();
    b.a(2131230884);
  }
  
  private void setDueDate(String paramString, TextView paramTextView)
  {
    if ((paramString != null) && (!paramString.isEmpty()))
    {
      paramTextView.setText(paramString);
      paramTextView.setVisibility(0);
      return;
    }
    paramTextView.setVisibility(8);
  }
  
  private String text(TextView paramTextView)
  {
    if ((paramTextView == null) || (paramTextView.getText() == null)) {
      return null;
    }
    return paramTextView.getText().toString();
  }
  
  public TorpedoPreview a(String paramString)
  {
    d = paramString;
    return this;
  }
  
  public void a()
  {
    if (a.get().isEmpty()) {
      return;
    }
    if ((a.get().size() == 1) && (a.equals()))
    {
      DeviationUtils.c((Activity)getContext(), (com.deviantart.android.android.package_14.model.DVNTDeviation)a.get(0), null);
      return;
    }
    TrackerUtil.a(getActivity(), "tap_collection");
    if (q != null) {
      TrackerUtil.a(getActivity(), "featuredtags_tap_tag_explore", q);
    }
    long l1 = a.getValue();
    if (c != null) {}
    for (Object localObject = c;; localObject = text(torpedoTitle))
    {
      String str = String.valueOf(l1) + (String)localObject + z + v + e;
      localObject = (FullTorpedoFragment)new FullTorpedoFragment.InstanceBuilder().a(a).a((String)localObject).putShort(z).putInt(v).c(e).b(d).a(p).a();
      ScreenFlowManager.a(getActivity(), (Fragment)localObject, "open_gallery_title" + str);
      return;
    }
  }
  
  public void a(ProcessMenuType paramProcessMenuType, String paramString)
  {
    if (o != null) {
      o.a(paramProcessMenuType, paramString);
    }
    DeviationUtils.c = paramString;
    DeviationUtils.b = paramProcessMenuType;
  }
  
  public void b()
  {
    b.a(ViewState.State.b);
    if (findViewWithTag(ViewState.State.b) != null) {
      return;
    }
    torpedoMainView.removeAllViews();
    torpedoMainView.addView(a(ViewState.Helper.a((Activity)getContext(), null, this, b)));
  }
  
  public void b(StreamLoader.ErrorType paramErrorType, String paramString)
  {
    b.a(ViewState.State.i);
    if (findViewWithTag(ViewState.State.i) != null) {
      return;
    }
    torpedoMainView.removeAllViews();
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("state_error", paramErrorType);
    localBundle.putString("state_msg", paramString);
    b.a(ViewState.State.i, localBundle);
    torpedoMainView.addView(a(ViewState.Helper.b((Activity)getContext(), null, this, b)));
  }
  
  public void c()
  {
    draw();
  }
  
  public void d()
  {
    torpedoMainView.removeAllViews();
    q = null;
    z = null;
    v = null;
    e = null;
    c = null;
    torpedoTitle.setText(null);
    torpedoSubTitle.setText(null);
    torpedoDetail.setText(null);
    putByte();
  }
  
  public void draw()
  {
    if (a != null)
    {
      if (getWidth() == 0) {
        return;
      }
      if (a.d())
      {
        b();
        return;
      }
      if (a.size() == 0)
      {
        e();
        a.add(getContext(), this, true);
        return;
      }
      a.a(false);
      torpedoMainView.removeAllViews();
      TorpedoPreview.PreviewTorpedoDescriptor localPreviewTorpedoDescriptor = new TorpedoPreview.PreviewTorpedoDescriptor(this, l);
      localPreviewTorpedoDescriptor.d(x);
      localPreviewTorpedoDescriptor.a(x);
      localPreviewTorpedoDescriptor.set(x);
      localPreviewTorpedoDescriptor.d(false);
      ArrayList localArrayList = a.get();
      int j = Math.min(localArrayList.size(), 10);
      int i = 0;
      while (i < j)
      {
        localPreviewTorpedoDescriptor.a(new DeviationDescription((com.deviantart.android.android.package_14.model.DVNTDeviation)localArrayList.get(i), a.length(), i));
        i += 1;
      }
      localPreviewTorpedoDescriptor.e();
      localArrayList = localPreviewTorpedoDescriptor.d().get();
      int n = Math.min(x, localArrayList.size());
      i = 0;
      j = 0;
      while (i < n)
      {
        Object localObject1 = (Torpedo.TorpedoLane)localArrayList.get(i);
        LinearLayout localLinearLayout = new LinearLayout(getContext());
        int i1 = getWidth() / ((Torpedo.TorpedoLane)localObject1).getValue() * ((Torpedo.TorpedoLane)localObject1).e();
        localLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, i1));
        localLinearLayout.setOrientation(0);
        torpedoMainView.addView(localLinearLayout);
        localObject1 = ((Torpedo.TorpedoLane)localObject1).get().iterator();
        while (((Iterator)localObject1).hasNext())
        {
          Object localObject2 = (Torpedo.TorpedoThumb)((Iterator)localObject1).next();
          if (localObject2 != null)
          {
            int i2 = (int)(x / 50.0F * getWidth());
            localObject2 = localPreviewTorpedoDescriptor.a(getContext(), i2, i1, j);
            j += 1;
            if (localObject2 != null)
            {
              ((View)localObject2).setPadding(u / 2, u / 2, u / 2, u / 2);
              localLinearLayout.addView((View)localObject2, new LinearLayout.LayoutParams(i2, i1));
            }
          }
        }
        i += 1;
      }
    }
  }
  
  public void e()
  {
    b.a(ViewState.State.d);
    if (findViewWithTag(ViewState.State.d) != null) {
      return;
    }
    torpedoMainView.removeAllViews();
    torpedoMainView.addView(a(ViewState.Helper.a((Activity)getContext(), null, this)));
  }
  
  public void f() {}
  
  public Activity getActivity()
  {
    return (Activity)super.getContext();
  }
  
  public int getBorderSize()
  {
    return u;
  }
  
  public String getDetail()
  {
    return text(torpedoDetail);
  }
  
  public int getMaxColumns()
  {
    return l;
  }
  
  public int getRowLimit()
  {
    return x;
  }
  
  public String getSubTitle()
  {
    return text(torpedoSubTitle);
  }
  
  public String getTitle()
  {
    return text(torpedoTitle);
  }
  
  public void onClickTitleContainer()
  {
    a();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (!r)
    {
      r = true;
      draw();
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (paramInt1 != 0)
    {
      if (paramInt2 == 0) {
        return;
      }
      paramInt3 = x * m;
      if ((height == -1) && (!DVNTContextUtils.isContextDead(getContext()))) {
        height = Graphics.width(getContext()) / 3;
      }
      paramInt2 = paramInt3;
      if (height != -1) {
        paramInt2 = Math.min(paramInt3, height);
      }
      k = (paramInt2 * 50 / paramInt1);
    }
  }
  
  public void setAddFaveMark(boolean paramBoolean)
  {
    p = paramBoolean;
  }
  
  public void setDeviationStream(Stream paramStream)
  {
    a = paramStream;
    draw();
  }
  
  public void setEventLabel(String paramString)
  {
    q = paramString;
  }
  
  public void setGridTitle(String paramString)
  {
    c = paramString;
  }
  
  public void setHeaderImageURL(String paramString)
  {
    e = paramString;
  }
  
  public void setHeaderSubTitle(String paramString)
  {
    v = paramString;
  }
  
  public void setHeaderTitle(String paramString)
  {
    z = paramString;
  }
  
  public void setProcessMenuListener(ProcessMenuListener paramProcessMenuListener)
  {
    o = paramProcessMenuListener;
  }
  
  public void setSubTitle(String paramString)
  {
    setDueDate(paramString, torpedoSubTitle);
    showAttachments();
  }
  
  public void setTitle(String paramString)
  {
    setDueDate(paramString, torpedoTitle);
    showAttachments();
  }
  
  public void setTorpedoDetail(String paramString)
  {
    setDueDate(paramString, torpedoDetail);
  }
  
  public void showAttachments()
  {
    if ((torpedoTitle.getVisibility() == 8) && (torpedoSubTitle.getVisibility() == 8))
    {
      titleContainer.setVisibility(8);
      return;
    }
    titleContainer.setVisibility(0);
  }
}
