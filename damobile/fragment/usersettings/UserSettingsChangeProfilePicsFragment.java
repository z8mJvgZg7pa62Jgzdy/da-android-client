package com.deviantart.android.damobile.fragment.usersettings;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.android.package_14.model.DVNTUserProfile;
import com.deviantart.android.android.package_14.model.DVNTWhoAmIResponse;
import com.deviantart.android.android.package_14.model.DVNTWhoIsResponse;
import com.deviantart.android.damobile.activity.UserSettingsActivity;
import com.deviantart.android.damobile.fragment.PhotoFragment;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.PhotoUtils.PictureType;
import com.deviantart.android.damobile.util.UserUtils;
import com.facebook.drawee.view.SimpleDraweeView;

public class UserSettingsChangeProfilePicsFragment
  extends UserSettingsBaseFragment
{
  @Bind({2131689769})
  SimpleDraweeView avatar;
  @Bind({2131689869})
  View changeAvatarView;
  @Bind({2131689870})
  View changeProfilePictureView;
  boolean d = false;
  boolean f = false;
  String i;
  String p;
  @Bind({2131689871})
  SimpleDraweeView profilePic;
  
  public UserSettingsChangeProfilePicsFragment() {}
  
  public void b(DVNTWhoAmIResponse paramDVNTWhoAmIResponse)
  {
    if (UserUtils.a() != null) {
      d = true;
    }
    for (paramDVNTWhoAmIResponse = Uri.fromFile(UserUtils.a());; paramDVNTWhoAmIResponse = Uri.parse(paramDVNTWhoAmIResponse))
    {
      ImageUtils.isEmpty(avatar, paramDVNTWhoAmIResponse);
      return;
      paramDVNTWhoAmIResponse = paramDVNTWhoAmIResponse.getUserIconURL();
      if (!UserUtils.get().equals(paramDVNTWhoAmIResponse))
      {
        d = true;
        p = paramDVNTWhoAmIResponse;
      }
    }
  }
  
  public void onClick(DVNTUserProfile paramDVNTUserProfile)
  {
    if (UserUtils.d() != null)
    {
      f = true;
      paramDVNTUserProfile = Uri.fromFile(UserUtils.d());
    }
    for (;;)
    {
      ImageUtils.isEmpty(profilePic, paramDVNTUserProfile);
      return;
      paramDVNTUserProfile = paramDVNTUserProfile.getProfilePicture();
      if ((paramDVNTUserProfile != null) && (paramDVNTUserProfile.getContent() != null))
      {
        f = true;
        i = paramDVNTUserProfile.getContent().getSrc();
        paramDVNTUserProfile = Uri.parse(paramDVNTUserProfile.getContent().getSrc());
      }
      else
      {
        paramDVNTUserProfile = Uri.parse(UserUtils.get());
      }
    }
  }
  
  public void onClickChangeAvatar()
  {
    a(changeAvatarView, d, new UserSettingsChangeProfilePicsFragment.SettingsOnImageFileReadyListener(this, getActivity(), avatar, p, PhotoUtils.PictureType.C));
  }
  
  public void onClickProfilePicture()
  {
    a(changeProfilePictureView, f, new UserSettingsChangeProfilePicsFragment.SettingsOnImageFileReadyListener(this, getActivity(), profilePic, i, PhotoUtils.PictureType.N));
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setRetainInstance(true);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = (LinearLayout)paramLayoutInflater.inflate(2130968692, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    filterSelected();
    loadNotifications();
    return paramLayoutInflater;
  }
  
  public void onResume()
  {
    super.onResume();
    ((UserSettingsActivity)getActivity()).onProgressChanged(2131231269);
  }
}
