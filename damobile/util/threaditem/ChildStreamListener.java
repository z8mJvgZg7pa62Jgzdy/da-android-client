package com.deviantart.android.damobile.util.threaditem;

import com.deviantart.android.damobile.stream.loader.StreamLoader.ErrorType;

public abstract interface ChildStreamListener<THREAD_ITEM extends ThreadItem>
{
  public abstract void a(ThreadItem paramThreadItem, int paramInt, StreamLoader.ErrorType paramErrorType, String paramString);
  
  public abstract void a(String paramString, ThreadItem paramThreadItem, int paramInt);
  
  public abstract void setText(ThreadItem paramThreadItem, int paramInt);
}
