package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTGetStatusRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTUserStatus>
{
  private final String statusId;
  
  public DVNTGetStatusRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTUserStatus.class);
    statusId = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTGetStatusRequestV1 localDVNTGetStatusRequestV1 = (DVNTGetStatusRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(statusId, statusId).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "getstatus" + statusId;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 600000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(statusId).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTUserStatus sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).getStatus(paramString, statusId, Boolean.valueOf(true));
  }
}
