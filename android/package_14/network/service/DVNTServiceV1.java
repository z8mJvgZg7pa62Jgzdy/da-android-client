package com.deviantart.android.android.package_14.network.service;

import com.deviantart.android.android.package_14.model.DVNTAvatarResponse;
import com.deviantart.android.android.package_14.model.DVNTComment;
import com.deviantart.android.android.package_14.model.DVNTCommentContextResponse;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviationCommentsThread;
import com.deviantart.android.android.package_14.model.DVNTDeviationContent;
import com.deviantart.android.android.package_14.model.DVNTDeviationDownloadInfo;
import com.deviantart.android.android.package_14.model.DVNTFeaturesAndAgreements;
import com.deviantart.android.android.package_14.model.DVNTFeedBucketResponse;
import com.deviantart.android.android.package_14.model.DVNTFeedResponse;
import com.deviantart.android.android.package_14.model.DVNTFriendsResponse;
import com.deviantart.android.android.package_14.model.DVNTMoreLikeThisPreviewResults;
import com.deviantart.android.android.package_14.model.DVNTPaginatedCollectionFolders;
import com.deviantart.android.android.package_14.model.DVNTPaginatedGalleryFolders;
import com.deviantart.android.android.package_14.model.DVNTPaginatedResultResponse;
import com.deviantart.android.android.package_14.model.DVNTPostStatusResponse;
import com.deviantart.android.android.package_14.model.DVNTPublishOptions.DVNTLicenseModificationOption;
import com.deviantart.android.android.package_14.model.DVNTPublishOptions.DVNTMatureLevel;
import com.deviantart.android.android.package_14.model.DVNTPublishStashItemResponse;
import com.deviantart.android.android.package_14.model.DVNTStashDelta;
import com.deviantart.android.android.package_14.model.DVNTStashItem;
import com.deviantart.android.android.package_14.model.DVNTStashMoveStackResponse;
import com.deviantart.android.android.package_14.model.DVNTStashStackContents;
import com.deviantart.android.android.package_14.model.DVNTStashStackMetadata;
import com.deviantart.android.android.package_14.model.DVNTStashSubmitResponse;
import com.deviantart.android.android.package_14.model.DVNTStatus;
import com.deviantart.android.android.package_14.model.DVNTSuccess;
import com.deviantart.android.android.package_14.model.DVNTUserProfile;
import com.deviantart.android.android.package_14.model.DVNTUserStatus;
import com.deviantart.android.android.package_14.model.DVNTUserStatusResponse;
import com.deviantart.android.android.package_14.model.DVNTWatchingInfo;
import com.deviantart.android.android.package_14.model.DVNTWhoAmIResponse;
import com.deviantart.android.android.package_14.model.DVNTWhoIsResponses;
import com.deviantart.android.android.package_14.network.wrapper.DVNTCategoriesWrapper;
import com.deviantart.android.android.package_14.network.wrapper.DVNTIncludeWrapper;
import com.deviantart.android.android.package_14.network.wrapper.DVNTMetadataWrapper;
import com.deviantart.android.android.package_14.network.wrapper.DVNTResultsWrapper;
import java.util.ArrayList;
import java.util.List;
import retrofit.mime.TypedFile;

public abstract interface DVNTServiceV1
{
  public static final String COLLECTION_FOLDERS_ENDPOINT = "/api/v1/oauth2/collections/folders";
  public static final String DEVIATION_METADATA_ENDPOINT = "/api/v1/oauth2/deviation/metadata";
  public static final String GALLERY_FOLDERS_ENDPOINT = "/api/v1/oauth2/gallery/folders";
  public static final String MORE_LIKE_THIS_ENDPOINT = "/api/v1/oauth2/browse/morelikethis";
  public static final String MORE_LIKE_THIS_PREVIEW_ENDPOINT = "/api/v1/oauth2/browse/morelikethis/preview";
  public static final String PLACEBO_ENDPOINT = "/api/v1/oauth2/placebo";
  public static final String SEARCH_FRIENDS_ENDPOINT = "/api/v1/oauth2/user/friends/search";
  public static final String VERSION_PREFIX = "/api/v1/oauth2";
  public static final String WHOIS_ENDPOINT = "/api/v1/oauth2/user/whois";
  
  public abstract DVNTResultsWrapper browseDailyDeviations(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTPaginatedResultResponse browseHot(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTPaginatedResultResponse browseNewest(String paramString1, String paramString2, String paramString3, Boolean paramBoolean, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTPaginatedResultResponse browsePopular(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTResultsWrapper browseSearchTags(String paramString1, String paramString2);
  
  public abstract DVNTPaginatedResultResponse browseTag(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2, Boolean paramBoolean);
  
  public abstract DVNTPaginatedResultResponse browseUndiscovered(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTPaginatedResultResponse browseUserJournals(String paramString1, String paramString2, Boolean paramBoolean, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTDeviationContent challengeContent(String paramString1, String paramString2);
  
  public abstract DVNTPaginatedResultResponse challengeEntries(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTPaginatedResultResponse challengeNewest(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTResultsWrapper challengeProgressional(String paramString1, String paramString2);
  
  public abstract DVNTPaginatedResultResponse collection(String paramString1, String paramString2, String paramString3, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTPaginatedCollectionFolders collectionFolders(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTCommentContextResponse commentContext(String paramString1, String paramString2, boolean paramBoolean, Integer paramInteger1, Integer paramInteger2, Boolean paramBoolean1);
  
  public abstract DVNTDeviationCommentsThread commentsDeviation(String paramString1, String paramString2, String paramString3, Integer paramInteger1, Integer paramInteger2, Boolean paramBoolean);
  
  public abstract DVNTComment commentsDeviationPost(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean);
  
  public abstract DVNTDeviationCommentsThread commentsStatus(String paramString1, String paramString2, String paramString3, Integer paramInteger1, Integer paramInteger2, Boolean paramBoolean);
  
  public abstract DVNTComment commentsStatusPost(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean);
  
  public abstract DVNTDeviationCommentsThread commentsUserProfile(String paramString1, String paramString2, String paramString3, Integer paramInteger1, Integer paramInteger2, Boolean paramBoolean);
  
  public abstract DVNTComment commentsUserProfilePost(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean);
  
  public abstract DVNTResultsWrapper curatedTag(String paramString, boolean paramBoolean);
  
  public abstract DVNTSuccess deleteMessage(String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract DVNTDeviationContent deviationContent(String paramString1, String paramString2);
  
  public abstract DVNTDeviationDownloadInfo deviationDownload(String paramString1, String paramString2);
  
  public abstract DVNTPaginatedResultResponse deviationEmbedded(String paramString1, String paramString2, String paramString3, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTDeviation deviationInfo(String paramString1, String paramString2);
  
  public abstract DVNTMetadataWrapper deviationMetadata(String paramString, List paramList, Boolean paramBoolean1, Boolean paramBoolean2, Boolean paramBoolean3, Boolean paramBoolean4, Boolean paramBoolean5);
  
  public abstract DVNTSuccess fave(String paramString1, String paramString2, String paramString3);
  
  public abstract DVNTFeedBucketResponse feedBucket(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTFeedResponse feedHome(String paramString1, String paramString2, Boolean paramBoolean);
  
  public abstract DVNTFeedResponse feedProfile(String paramString1, String paramString2, Boolean paramBoolean);
  
  public abstract DVNTIncludeWrapper feedSettings(String paramString);
  
  public abstract DVNTSuccess feedSettingsUpdate(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5);
  
  public abstract DVNTSuccess feedbackDelete(String paramString1, String paramString2, String paramString3);
  
  public abstract DVNTFriendsResponse friends(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2, String paramString3);
  
  public abstract DVNTPaginatedResultResponse gallery(String paramString1, String paramString2, String paramString3, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTPaginatedResultResponse galleryAll(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTPaginatedGalleryFolders galleryFolders(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTAvatarResponse getAvatarUploadStatus(String paramString1, String paramString2);
  
  public abstract DVNTUserStatus getStatus(String paramString1, String paramString2, Boolean paramBoolean);
  
  public abstract DVNTCategoriesWrapper getSubmissionCategoryTree(String paramString1, String paramString2, String paramString3, boolean paramBoolean);
  
  public abstract DVNTUserStatusResponse getUserStatuses(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2, Boolean paramBoolean);
  
  public abstract DVNTPaginatedResultResponse moreLikeThis(String paramString1, String paramString2, String paramString3, Integer paramInteger1, Integer paramInteger2);
  
  public abstract DVNTMoreLikeThisPreviewResults moreLikeThisPreview(String paramString1, String paramString2);
  
  public abstract DVNTFeedResponse notifications(String paramString1, String paramString2, Boolean paramBoolean);
  
  public abstract DVNTStatus placebo(String paramString);
  
  public abstract DVNTPostStatusResponse postStatus(String paramString1, String paramString2, String paramString3, String paramString4);
  
  public abstract DVNTPublishStashItemResponse publishStashItem(String paramString1, Long paramLong, Boolean paramBoolean1, DVNTPublishOptions.DVNTMatureLevel paramDVNTMatureLevel, List paramList, String paramString2, Boolean paramBoolean2, Boolean paramBoolean3, Boolean paramBoolean4, Boolean paramBoolean5, ArrayList paramArrayList, Boolean paramBoolean6, Integer paramInteger1, Boolean paramBoolean7, Boolean paramBoolean8, DVNTPublishOptions.DVNTLicenseModificationOption paramDVNTLicenseModificationOption, String paramString3, Integer paramInteger2);
  
  public abstract DVNTResultsWrapper searchFriends(String paramString1, String paramString2);
  
  public abstract DVNTSuccess stashChangeStackPosition(String paramString, Long paramLong, Integer paramInteger);
  
  public abstract DVNTSuccess stashDelete(String paramString, Long paramLong);
  
  public abstract DVNTStashDelta stashDelta(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2, Boolean paramBoolean1, Boolean paramBoolean2, Boolean paramBoolean3, Boolean paramBoolean4);
  
  public abstract DVNTStashItem stashFetchItem(String paramString, Long paramLong, Boolean paramBoolean1, Boolean paramBoolean2, Boolean paramBoolean3, Boolean paramBoolean4);
  
  public abstract DVNTStashStackContents stashFetchStackContents(String paramString, Long paramLong);
  
  public abstract DVNTStashStackMetadata stashFetchStackMetadata(String paramString, Long paramLong);
  
  public abstract DVNTStashMoveStackResponse stashMoveStack(String paramString, Long paramLong1, Long paramLong2);
  
  public abstract DVNTFeaturesAndAgreements stashPublishUserData(String paramString);
  
  public abstract DVNTStashSubmitResponse stashSubmit(String paramString1, String paramString2, String paramString3, String paramString4, ArrayList paramArrayList, TypedFile paramTypedFile1, TypedFile paramTypedFile2);
  
  public abstract DVNTSuccess stashUpdateStack(String paramString1, Long paramLong, String paramString2, String paramString3);
  
  public abstract DVNTSuccess suggestFaveToGroup(String paramString1, String paramString2, String paramString3, Integer paramInteger);
  
  public abstract DVNTSuccess unfave(String paramString1, String paramString2, String paramString3);
  
  public abstract DVNTSuccess unwatch(String paramString1, String paramString2);
  
  public abstract DVNTAvatarResponse updateAvatar(String paramString, TypedFile paramTypedFile);
  
  public abstract DVNTAvatarResponse updateProfilePicture(String paramString, TypedFile paramTypedFile);
  
  public abstract DVNTSuccess updateUserProfile(String paramString1, Boolean paramBoolean, Integer paramInteger1, Integer paramInteger2, String paramString2, String paramString3, Integer paramInteger3, String paramString4, String paramString5);
  
  public abstract DVNTUserProfile userProfile(String paramString1, String paramString2, String paramString3, Boolean paramBoolean);
  
  public abstract DVNTSuccess watch(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8);
  
  public abstract DVNTFriendsResponse watchers(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2, String paramString3);
  
  public abstract DVNTWatchingInfo watching(String paramString1, String paramString2);
  
  public abstract DVNTWhoAmIResponse whoAmI(String paramString);
  
  public abstract DVNTWhoIsResponses whoIs(String paramString, List paramList);
}
