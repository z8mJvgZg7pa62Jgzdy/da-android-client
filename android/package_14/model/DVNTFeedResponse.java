package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTFeedResponse
  implements Serializable
{
  String cursor;
  @SerializedName("has_more")
  boolean hasMore;
  DVNTFeedItem.List items;
  
  public DVNTFeedResponse() {}
  
  public String getCursor()
  {
    return cursor;
  }
  
  public boolean getHasMore()
  {
    return hasMore;
  }
  
  public DVNTFeedItem.List getItems()
  {
    return items;
  }
  
  public boolean isHasMore()
  {
    return hasMore;
  }
  
  public void setCursor(String paramString)
  {
    cursor = paramString;
  }
  
  public void setHasMore(boolean paramBoolean)
  {
    hasMore = paramBoolean;
  }
  
  public void setItems(DVNTFeedItem.List paramList)
  {
    items = paramList;
  }
}
