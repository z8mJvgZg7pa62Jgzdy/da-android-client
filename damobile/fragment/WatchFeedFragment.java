package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.activity.HomeActivity.BottomBarButton;
import com.deviantart.android.damobile.activity.UserSettingsActivity;
import com.deviantart.android.damobile.adapter.recyclerview.WatchFeedAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APIWatchFeedLoader;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.scrolling.LinearViewEventScrollListener;
import com.deviantart.android.damobile.util.scrolling.LinearViewEventScrollListener.LinearScrollEventListener;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.util.tracking.pacaya.MobileLava;
import com.deviantart.android.damobile.util.tracking.pacaya.UxTopicEventCreator;
import com.deviantart.android.damobile.view.DAStateRecyclerView;
import com.deviantart.android.damobile.view.UserAvatar;
import com.deviantart.android.damobile.view.WatchPageEmptyStateView;
import com.deviantart.android.damobile.view.WatchPageEmptyStateView.State;
import com.deviantart.pacaya.Pacaya;

public class WatchFeedFragment
  extends HomeBaseFragment
  implements SwipeRefreshLayout.OnRefreshListener, LinearViewEventScrollListener.LinearScrollEventListener
{
  public static boolean p = false;
  UserAvatar l;
  @Bind({2131689915})
  DAStateRecyclerView recyclerView;
  
  public WatchFeedFragment() {}
  
  private void updateData()
  {
    if (UserUtils.b > 0)
    {
      recyclerView.setEmptyStateView(new WatchPageEmptyStateView(getActivity(), WatchPageEmptyStateView.State.c));
      return;
    }
    recyclerView.setEmptyStateView(new WatchPageEmptyStateView(getActivity(), WatchPageEmptyStateView.State.a));
  }
  
  public void a()
  {
    super.a();
    if (l != null) {
      l.b(getActivity());
    }
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    TrackerUtil.append(getActivity(), EventKeys.Category.b, paramInt1, paramInt2);
  }
  
  public void addClicked()
  {
    if (DVNTContextUtils.isContextDead(getActivity())) {
      return;
    }
    Toast.makeText(getActivity(), 2131231276, 0).show();
    d(true);
  }
  
  public boolean b()
  {
    if (!isResumed()) {
      return false;
    }
    return NavigationUtils.c(getActivity());
  }
  
  protected HomeActivity.BottomBarButton c()
  {
    return HomeActivity.BottomBarButton.e;
  }
  
  public void d(boolean paramBoolean)
  {
    p = false;
    StreamCacher.b(StreamCacheStrategy.a);
    recyclerView.d(paramBoolean);
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (DVNTAbstractAsyncAPI.isUserSession(getActivity()))
    {
      a(paramMenu, paramMenuInflater);
      paramMenuInflater.inflate(2131820555, paramMenu);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramViewGroup = paramLayoutInflater.inflate(2130968695, paramViewGroup, false);
    ButterKnife.bind(this, paramViewGroup);
    paramBundle = StreamCacher.a(new APIWatchFeedLoader(), StreamCacheStrategy.b);
    WatchFeedAdapter localWatchFeedAdapter = new WatchFeedAdapter(paramBundle);
    updateData();
    recyclerView.setAdapter(localWatchFeedAdapter);
    if (recyclerView.getHeaderItemCount() == 0)
    {
      paramLayoutInflater = (ViewGroup)paramLayoutInflater.inflate(2130968805, recyclerView, false);
      paramLayoutInflater.setOnClickListener(new WatchFeedFragment.1(this));
      recyclerView.a(paramLayoutInflater);
      l = ((UserAvatar)ButterKnife.findById(paramLayoutInflater, 2131690145));
    }
    recyclerView.setOnRefreshListener(this);
    recyclerView.setAdapter();
    recyclerView.setOnScrollListener(new LinearViewEventScrollListener(this));
    MobileLava.e().setText(new UxTopicEventCreator().get("watch").b());
    if ((paramBundle.equals()) && (paramBundle.size() == 0)) {
      d(true);
    }
    initUI();
    ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(true);
    ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
    ((BaseActivity)getActivity()).getSupportActionBar().setCustomView(2130968713);
    return paramViewGroup;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      break;
    }
    for (;;)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      openFile();
    }
  }
  
  public void onPause()
  {
    super.onPause();
    TrackerUtil.b = null;
  }
  
  public void onResume()
  {
    super.onResume();
    if (p)
    {
      updateData();
      d(true);
    }
    TrackerUtil.b = EventKeys.Category.b;
  }
  
  protected void openFile()
  {
    TrackerUtil.a(getActivity(), "open_manage_watchfeed");
    getActivity().startActivityForResult(UserSettingsActivity.createIntent(getActivity(), true), 105);
  }
  
  public void refreshView()
  {
    d(false);
  }
}
