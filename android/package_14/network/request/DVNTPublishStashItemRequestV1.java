package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.model.DVNTPublishOptions;
import com.deviantart.android.android.package_14.model.DVNTPublishOptions.DVNTLicenseModificationOption;
import com.deviantart.android.android.package_14.model.DVNTPublishOptions.DVNTMatureLevel;
import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.sdk.api.model.DVNTPublishOptions.DVNTMatureClassification;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTPublishStashItemRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTPublishStashItemResponse>
{
  private final Boolean agreeSubmission;
  private final Boolean agreeTOS;
  private final Boolean allowComments;
  private final Boolean allowFreeDownloads;
  private final String categoryPath;
  private Integer challengeDuration = null;
  private String challengeId = null;
  private Integer displayResolution = null;
  private final ArrayList<String> galleries;
  private Boolean isCCLicense = null;
  private final Boolean isMature;
  private Boolean isOkForCommercialUse = null;
  private DVNTPublishOptions.DVNTLicenseModificationOption licenseModification = null;
  private List<DVNTPublishOptions.DVNTMatureClassification> matureClassificationList = null;
  private DVNTPublishOptions.DVNTMatureLevel matureLevel = null;
  private final Long stashId;
  private final Boolean watermark;
  
  public DVNTPublishStashItemRequestV1(Long paramLong, String paramString, ArrayList paramArrayList, DVNTPublishOptions paramDVNTPublishOptions)
  {
    super(com.deviantart.android.sdk.api.model.DVNTPublishStashItemResponse.class);
    stashId = paramLong;
    isMature = paramDVNTPublishOptions.getIsMature();
    categoryPath = paramString;
    allowComments = paramDVNTPublishOptions.getAllowComments();
    allowFreeDownloads = paramDVNTPublishOptions.getAllowFreeDownloads();
    agreeSubmission = paramDVNTPublishOptions.getAgreeSubmissionTerms();
    agreeTOS = paramDVNTPublishOptions.getAgreeTOS();
    galleries = paramArrayList;
    watermark = paramDVNTPublishOptions.getWatermark();
    challengeId = paramDVNTPublishOptions.getChallengeId();
    challengeDuration = paramDVNTPublishOptions.getChallengeDuration();
    if ((watermark != null) && (watermark.booleanValue())) {
      displayResolution = Integer.valueOf(0);
    }
    paramLong = paramDVNTPublishOptions.getIsLicenseCreativeCommons();
    if ((paramLong != null) && (paramLong.booleanValue()))
    {
      isCCLicense = Boolean.valueOf(true);
      isOkForCommercialUse = paramDVNTPublishOptions.getIsLicenseOkforCommercialUse();
      paramLong = paramDVNTPublishOptions.getLicenseModification();
      if (paramLong != null) {
        licenseModification = paramLong;
      }
    }
    if (!isMature.booleanValue()) {
      return;
    }
    if (paramDVNTPublishOptions.getMatureLevel() != null) {
      matureLevel = paramDVNTPublishOptions.getMatureLevel();
    }
    if ((paramDVNTPublishOptions.getMatureClassificationList() != null) && (!paramDVNTPublishOptions.getMatureClassificationList().isEmpty())) {
      matureClassificationList = paramDVNTPublishOptions.getMatureClassificationList();
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTPublishStashItemRequestV1 localDVNTPublishStashItemRequestV1 = (DVNTPublishStashItemRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(stashId, stashId).append(isMature, isMature).append(categoryPath, categoryPath).append(allowComments, allowComments).append(allowFreeDownloads, allowFreeDownloads).append(agreeSubmission, agreeSubmission).append(agreeTOS, agreeTOS).append(watermark, watermark).append(displayResolution, displayResolution).append(isCCLicense, isCCLicense).append(isOkForCommercialUse, isOkForCommercialUse).append(licenseModification, licenseModification).append(challengeId, challengeId).append(galleries, galleries).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(stashId).append(isMature).append(categoryPath).append(allowComments).append(allowFreeDownloads).append(agreeSubmission).append(agreeTOS).append(watermark).append(displayResolution).append(isCCLicense).append(isOkForCommercialUse).append(licenseModification).append(challengeId).append(galleries).toHashCode();
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTPublishStashItemResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).publishStashItem(paramString, stashId, isMature, matureLevel, matureClassificationList, categoryPath, allowComments, allowFreeDownloads, agreeSubmission, agreeTOS, galleries, watermark, displayResolution, isCCLicense, isOkForCommercialUse, licenseModification, challengeId, challengeDuration);
  }
}
