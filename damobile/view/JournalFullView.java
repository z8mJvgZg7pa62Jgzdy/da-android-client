package com.deviantart.android.damobile.view;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.util.ViewHelper;

public class JournalFullView
  extends LinearLayout
{
  @Bind({2131689726})
  ProgressBar loader;
  private DVNTDeviation plugin;
  @Bind({2131689723})
  ScrollDetectableScrollView scrollView;
  @Bind({2131689724})
  ViewGroup warningBanner;
  @Bind({2131689725})
  WebView webView;
  
  public JournalFullView(Context paramContext, DVNTDeviation paramDVNTDeviation)
  {
    super(paramContext);
    plugin = paramDVNTDeviation;
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130968660, this, true);
    ButterKnife.bind(this);
    WebSettings localWebSettings = webView.getSettings();
    localWebSettings.setUseWideViewPort(true);
    localWebSettings.setLoadWithOverviewMode(true);
    warningBanner.setVisibility(8);
    loader.setVisibility(0);
    ViewHelper.format(this);
    DVNTCommonAsyncAPI.deviationContent(paramDVNTDeviation.getId()).call(paramContext, new JournalFullView.1(this, paramContext));
  }
  
  private boolean onCreateView()
  {
    return PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean("journal_warning_visible", true);
  }
  
  private void savePrefs()
  {
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();
    localEditor.putBoolean("journal_warning_visible", false);
    localEditor.apply();
  }
  
  public void onFaqClick()
  {
    NavigationUtils.openBrowser(getContext(), getContext().getString(2131231041));
  }
  
  public void onOpenInBrowserClick()
  {
    NavigationUtils.openBrowser(getContext(), plugin.getUrl());
  }
  
  public void onWarningCloseClick()
  {
    warningBanner.setVisibility(8);
    savePrefs();
  }
  
  public void setScrollDetectedListener(ScrollDetectedListener paramScrollDetectedListener)
  {
    if (scrollView == null) {
      return;
    }
    scrollView.setScrollDetectedListener(paramScrollDetectedListener);
  }
}
