package com.deviantart.android.android.package_14.model;

import com.deviantart.android.sdk.api.model.DVNTGallery.List;
import com.deviantart.android.sdk.api.model.DVNTPaginatedResult;
import java.io.Serializable;

public class DVNTPaginatedGalleryFolders
  extends DVNTPaginatedResult<DVNTGallery.List>
  implements Serializable
{
  public DVNTPaginatedGalleryFolders() {}
}
