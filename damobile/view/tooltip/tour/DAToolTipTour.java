package com.deviantart.android.damobile.view.tooltip.tour;

import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.view.tooltip.DAToolTip;
import com.deviantart.android.damobile.view.tooltip.DAToolTipRelativeLayout;
import com.deviantart.android.damobile.view.tooltip.DAToolTipView;
import com.nhaarman.supertooltips.ToolTip.AnimationType;

public class DAToolTipTour
{
  DATourSlideDescriptor[] a;
  DAToolTipRelativeLayout b;
  View c;
  DAToolTipTour.TourListener j;
  DAToolTipView t;
  
  public DAToolTipTour(DATourSlideDescriptor[] paramArrayOfDATourSlideDescriptor, DAToolTipTour.TourListener paramTourListener, View paramView, DAToolTipRelativeLayout paramDAToolTipRelativeLayout)
  {
    a = paramArrayOfDATourSlideDescriptor;
    j = paramTourListener;
    c = paramView;
    b = paramDAToolTipRelativeLayout;
  }
  
  private DAToolTipRelativeLayout c()
  {
    return b;
  }
  
  private void d(int paramInt)
  {
    if (t != null)
    {
      t.remove();
      t = null;
    }
    Object localObject = new DATourSlideView(c.getContext(), this);
    ((DATourSlideView)localObject).onCreateView(paramInt);
    DATourSlideDescriptor localDATourSlideDescriptor = a[paramInt];
    localObject = new DAToolTip().remove((View)localObject).remove(-1).remove(ToolTip.AnimationType.d).a(d);
    t = c().a((DAToolTip)localObject, c.findViewById(b.intValue()));
    t.setOnToolTipViewClickedListener(new DAToolTipTour.2(this, paramInt));
  }
  
  public void a()
  {
    t = null;
    a = null;
    c().removeAllViews();
    c().setOnClickListener(null);
    c().setClickable(false);
    c().setSoundEffectsEnabled(true);
    c().setBackgroundResource(0);
    b = null;
    c = null;
  }
  
  void a(int paramInt)
  {
    c().removeView(t);
    if (b(paramInt))
    {
      a();
      j.putLong();
      return;
    }
    d(paramInt + 1);
  }
  
  public boolean b()
  {
    return t != null;
  }
  
  boolean b(int paramInt)
  {
    return paramInt == a.length - 1;
  }
  
  public void f()
  {
    c().setOnClickListener(new DAToolTipTour.1(this));
    c().setSoundEffectsEnabled(false);
    c().setBackgroundResource(2131558422);
    d(0);
  }
}
