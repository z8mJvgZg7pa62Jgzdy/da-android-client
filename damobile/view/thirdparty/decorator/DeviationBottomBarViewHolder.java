package com.deviantart.android.damobile.view.thirdparty.decorator;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.view.DeviationBottomBarMenu;
import samples.grantland.widget.AutofitTextView;

public class DeviationBottomBarViewHolder
  extends DecoratorViewHolder
{
  @Bind({2131689661})
  DeviationBottomBarMenu bottomBarMenu;
  @Bind({2131689660})
  public AutofitTextView title;
  
  public DeviationBottomBarViewHolder(RecyclerView.ViewHolder paramViewHolder, View paramView)
  {
    super(paramViewHolder, paramView);
    ButterKnife.bind(this, paramView);
  }
}
