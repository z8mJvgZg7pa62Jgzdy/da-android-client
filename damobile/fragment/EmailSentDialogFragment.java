package com.deviantart.android.damobile.fragment;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.SpannableStringInternal;
import android.text.style.StyleSpan;
import com.deviantart.android.damobile.util.UserUtils;

public class EmailSentDialogFragment
  extends DialogFragment
{
  public EmailSentDialogFragment() {}
  
  public static Spannable getTitle(Context paramContext)
  {
    if (UserUtils.c == null) {
      return new SpannableStringBuilder(paramContext.getString(2131231415) + "\n");
    }
    SpannableString localSpannableString = new SpannableString(UserUtils.c);
    localSpannableString.setSpan(new StyleSpan(1), 0, localSpannableString.length(), 33);
    paramContext = new SpannableStringBuilder(paramContext.getString(2131231414) + "\n");
    paramContext.append(localSpannableString);
    paramContext.append("\n");
    return paramContext;
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new AlertDialog.Builder(getActivity());
    paramBundle.setTitle(2131231413).setMessage(getTitle(getActivity())).setPositiveButton(2131231183, new EmailSentDialogFragment.1(this));
    return paramBundle.create();
  }
}
