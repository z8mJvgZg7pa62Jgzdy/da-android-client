package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTNotesFoldersRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTNotesFolders>
{
  public DVNTNotesFoldersRequestV1()
  {
    super(com.deviantart.android.sdk.api.model.DVNTNotesFolders.class);
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTNotesFolders sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).getNotesFolders(paramString);
  }
}
