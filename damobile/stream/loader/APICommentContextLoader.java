package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTCommentContextResponse;

public class APICommentContextLoader
  extends StreamLoader<DVNTCommentContextResponse>
{
  private String d;
  
  public APICommentContextLoader(String paramString)
  {
    d = paramString;
  }
  
  public String b()
  {
    return "commentcontextloader" + d;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    paramStreamLoadListener = new APICommentContextLoader.1(this, paramStreamLoadListener);
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.contextForComment(d, true, Integer.valueOf(paramInt), Integer.valueOf(a));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, paramStreamLoadListener);
  }
}
