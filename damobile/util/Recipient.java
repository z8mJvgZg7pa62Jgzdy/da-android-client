package com.deviantart.android.damobile.util;

import com.deviantart.android.android.package_14.model.DVNTUser;
import java.io.Serializable;

public class Recipient
  implements Serializable
{
  private Recipient.RecipientStatus key;
  private boolean s;
  private DVNTUser values;
  
  public Recipient(DVNTUser paramDVNTUser, Recipient.RecipientStatus paramRecipientStatus)
  {
    this(paramDVNTUser, paramRecipientStatus, false);
  }
  
  public Recipient(DVNTUser paramDVNTUser, Recipient.RecipientStatus paramRecipientStatus, boolean paramBoolean)
  {
    values = paramDVNTUser;
    key = paramRecipientStatus;
    s = paramBoolean;
  }
  
  public Recipient copy(Recipient.RecipientStatus paramRecipientStatus)
  {
    key = paramRecipientStatus;
    return this;
  }
  
  public Recipient.RecipientStatus get()
  {
    return key;
  }
  
  public String getAttributeValue()
  {
    return values.getUserIconURL();
  }
  
  public boolean getSocket()
  {
    return s;
  }
  
  public String getValue()
  {
    return values.getUserName();
  }
  
  public DVNTUser values()
  {
    return values;
  }
}
