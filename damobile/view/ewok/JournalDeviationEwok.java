package com.deviantart.android.damobile.view.ewok;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.util.DeviationType;
import com.deviantart.android.damobile.view.JournalFullView;
import com.deviantart.android.damobile.view.JournalThumbView;

public class JournalDeviationEwok
  extends DeviationEwok
{
  public JournalDeviationEwok(DVNTDeviation paramDVNTDeviation)
  {
    super(paramDVNTDeviation);
  }
  
  public View a(Activity paramActivity, ViewGroup paramViewGroup)
  {
    return b(paramActivity, null, 0, 0);
  }
  
  public View b(Activity paramActivity, ViewGroup paramViewGroup, int paramInt1, int paramInt2)
  {
    return new JournalThumbView(paramActivity, d);
  }
  
  public View b(Context paramContext)
  {
    return new JournalFullView(paramContext, d);
  }
  
  public DeviationType b()
  {
    return DeviationType.e;
  }
}
