package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.android.utils.DVNTExpandableArgumentBuilder;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTWatchersRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTFriendsResponse>
{
  private String expandArguments;
  private Integer limit;
  private Integer offset;
  private String userName;
  
  public DVNTWatchersRequestV1(String paramString, Integer paramInteger1, Integer paramInteger2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    super(com.deviantart.android.sdk.api.model.DVNTFriendsResponse.class);
    userName = paramString;
    userName = paramString;
    offset = paramInteger1;
    limit = paramInteger2;
    expandArguments = new DVNTExpandableArgumentBuilder().loadInfo(DVNTExpandUserArgument.EXPAND_USER_DETAILS, paramBoolean1).loadInfo(DVNTExpandUserArgument.EXPAND_USER_STATS, paramBoolean2).loadInfo(DVNTExpandUserArgument.EXPAND_USER_GEO, paramBoolean3).loadInfo(DVNTExpandUserArgument.EXPAND_USER_PROFILE, paramBoolean4).build();
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTWatchersRequestV1 localDVNTWatchersRequestV1 = (DVNTWatchersRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(userName, userName).append(offset, offset).append(limit, limit).append(expandArguments, expandArguments).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(userName).append(offset).append(limit).append(expandArguments).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTFriendsResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).watchers(paramString, userName, offset, limit, expandArguments);
  }
}
