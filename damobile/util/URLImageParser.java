package com.deviantart.android.damobile.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html.ImageGetter;
import android.view.View;

public class URLImageParser
  implements Html.ImageGetter
{
  View c;
  Context j;
  
  public URLImageParser(View paramView, Context paramContext)
  {
    j = paramContext;
    c = paramView;
  }
  
  public Drawable getDrawable(String paramString)
  {
    URLImageParser.ImageGetterAsync localImageGetterAsync = new URLImageParser.ImageGetterAsync(this, new URLImageParser.WebDrawable());
    localImageGetterAsync.execute(new String[] { paramString });
    return localImageGetterAsync.createFromStream();
  }
}
