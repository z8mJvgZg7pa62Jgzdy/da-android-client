package com.deviantart.android.damobile.stream.filter;

import com.deviantart.android.damobile.util.notes.NotesItemData;
import com.deviantart.android.damobile.util.opt.ItemDeleteHelperBase;
import com.deviantart.android.damobile.util.opt.ItemDeleteType;
import com.deviantart.android.damobile.util.opt.ItemDeleteUtils;
import com.deviantart.android.damobile.util.opt.NoteItemDeleteHelper;
import java.util.ArrayList;

public class NotesModelFilter
  implements StreamFilter<NotesItemData>
{
  public NotesModelFilter() {}
  
  public boolean c(NotesItemData paramNotesItemData)
  {
    return ((NoteItemDeleteHelper)ItemDeleteUtils.a(ItemDeleteType.c)).i().contains(paramNotesItemData);
  }
}
