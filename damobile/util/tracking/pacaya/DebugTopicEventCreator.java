package com.deviantart.android.damobile.util.tracking.pacaya;

public class DebugTopicEventCreator
  extends TopicEventCreator
{
  public DebugTopicEventCreator()
  {
    super("dbg");
  }
  
  public DebugTopicEventCreator a(String paramString)
  {
    add("type", paramString);
    return this;
  }
}
