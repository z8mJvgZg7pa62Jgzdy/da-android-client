package com.deviantart.android.damobile.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.google.common.base.Objects;
import java.io.Serializable;

public class SubmitGallery
  implements SubmitSecondaryFragment.SubmitOptionalElement, Serializable
{
  private String b;
  private String d;
  
  public SubmitGallery(String paramString1, String paramString2)
  {
    b = paramString1;
    d = paramString2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    paramObject = (SubmitGallery)paramObject;
    return (Objects.append(b, b)) && (Objects.append(d, d));
  }
  
  public String getValue()
  {
    return b;
  }
  
  public View getView(Context paramContext)
  {
    TextView localTextView = (TextView)LayoutInflater.from(paramContext).inflate(2130968778, null, false);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-2, -2);
    localLayoutParams.setMargins(0, 0, paramContext.getResources().getDimensionPixelSize(2131361974), 0);
    localTextView.setLayoutParams(localLayoutParams);
    localTextView.setText(b);
    return localTextView;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { b, d });
  }
  
  public String s()
  {
    return d;
  }
}
