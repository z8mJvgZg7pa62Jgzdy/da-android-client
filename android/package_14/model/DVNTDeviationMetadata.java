package com.deviantart.android.android.package_14.model;

import com.deviantart.android.sdk.api.model.DVNTTag;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class DVNTDeviationMetadata
  implements Serializable
{
  @SerializedName("allows_comments")
  Boolean allowsComments;
  DVNTUser author;
  HashMap<String, String> camera;
  DVNTCollection.List collections;
  DVNTDAMLComponent.List description;
  @SerializedName("deviationid")
  String deviationId;
  @SerializedName("is_favourited")
  boolean favourited;
  @SerializedName("is_watching")
  Boolean isWatching;
  String license;
  @SerializedName("printid")
  String printId;
  DVNTStats stats;
  DVNTSubmission submission;
  ArrayList<DVNTTag> tags;
  String title;
  
  public DVNTDeviationMetadata() {}
  
  public Boolean getAllowsComments()
  {
    return allowsComments;
  }
  
  public DVNTUser getAuthor()
  {
    return author;
  }
  
  public HashMap getCamera()
  {
    return camera;
  }
  
  public DVNTCollection.List getCollections()
  {
    return collections;
  }
  
  public DVNTDAMLComponent.List getDescription()
  {
    return description;
  }
  
  public String getDeviationId()
  {
    return deviationId;
  }
  
  public Boolean getIsWatching()
  {
    return isWatching;
  }
  
  public String getLicense()
  {
    return license;
  }
  
  public String getPrintId()
  {
    return printId;
  }
  
  public DVNTStats getStats()
  {
    return stats;
  }
  
  public DVNTSubmission getSubmission()
  {
    return submission;
  }
  
  public ArrayList getTags()
  {
    return tags;
  }
  
  public String getTitle()
  {
    return title;
  }
  
  public Boolean isFavourited()
  {
    return Boolean.valueOf(favourited);
  }
  
  public Boolean isWatchingAuthor()
  {
    return isWatching;
  }
  
  public void setAllowsComments(Boolean paramBoolean)
  {
    allowsComments = paramBoolean;
  }
  
  public void setAuthor(DVNTUser paramDVNTUser)
  {
    author = paramDVNTUser;
  }
  
  public void setCamera(HashMap paramHashMap)
  {
    camera = paramHashMap;
  }
  
  public void setCollections(DVNTCollection.List paramList)
  {
    collections = paramList;
  }
  
  public void setDescription(DVNTDAMLComponent.List paramList)
  {
    description = paramList;
  }
  
  public void setDeviationId(String paramString)
  {
    deviationId = paramString;
  }
  
  public void setFavourited(Boolean paramBoolean)
  {
    favourited = paramBoolean.booleanValue();
  }
  
  public void setFavourited(boolean paramBoolean)
  {
    favourited = paramBoolean;
  }
  
  public void setIsWatching(Boolean paramBoolean)
  {
    isWatching = paramBoolean;
  }
  
  public void setLicense(String paramString)
  {
    license = paramString;
  }
  
  public void setPrintId(String paramString)
  {
    printId = paramString;
  }
  
  public void setStats(DVNTStats paramDVNTStats)
  {
    stats = paramDVNTStats;
  }
  
  public void setSubmission(DVNTSubmission paramDVNTSubmission)
  {
    submission = paramDVNTSubmission;
  }
  
  public void setTags(ArrayList paramArrayList)
  {
    tags = paramArrayList;
  }
  
  public void setTitle(String paramString)
  {
    title = paramString;
  }
}
