package com.deviantart.android.damobile.util.notes;

import com.deviantart.android.android.package_14.model.DVNTNotesFolder.List;
import com.deviantart.android.android.package_14.model.DVNTNotesFolders;
import com.deviantart.android.android.package_14.network.wrapper.DVNTResultsWrapper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class NotesFolders
{
  private static final HashMap<String, com.deviantart.android.sdk.api.model.DVNTNotesFolder> c = new HashMap();
  private static final NotesFoldersLoader d = new NotesFoldersLoader();
  
  public NotesFolders() {}
  
  public static NotesFoldersLoader a()
  {
    return d;
  }
  
  public static void a(String paramString, int paramInt)
  {
    if (!get().containsKey(paramString)) {
      return;
    }
    paramString = (com.deviantart.android.android.package_14.model.DVNTNotesFolder)get().get(paramString);
    paramString.setCount(Integer.valueOf(paramString.getCount().intValue() + paramInt));
  }
  
  public static void b()
  {
    c.clear();
    if (d.c()) {
      d.b();
    }
    d.b(false);
  }
  
  public static HashMap get()
  {
    return c;
  }
  
  public static void onRender(DVNTNotesFolders paramDVNTNotesFolders)
  {
    c.clear();
    paramDVNTNotesFolders = ((DVNTNotesFolder.List)paramDVNTNotesFolders.getResults()).iterator();
    while (paramDVNTNotesFolders.hasNext())
    {
      com.deviantart.android.android.package_14.model.DVNTNotesFolder localDVNTNotesFolder = (com.deviantart.android.android.package_14.model.DVNTNotesFolder)paramDVNTNotesFolders.next();
      c.put(localDVNTNotesFolder.getFolderId(), localDVNTNotesFolder);
    }
  }
}
