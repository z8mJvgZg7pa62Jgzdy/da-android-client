package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.DAMobileApplication;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.util.SharedPreferenceUtil;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.caccao.Caccao;
import com.deviantart.datoolkit.logger.DVNTLog;
import com.google.android.com.common.aimsicd.GoogleApiClient;
import com.squareup.leakcanary.RefWatcher;
import rx.Observable;
import rx.schedulers.Schedulers;

public abstract class DABaseFragment
  extends Fragment
{
  public static boolean d = false;
  private static final Class[] id = { DiscoveryFragment.class, DiscoverySearchFragment.class };
  public static Class type;
  private boolean showIcons = false;
  
  public DABaseFragment() {}
  
  private void visitFrame(Activity paramActivity)
  {
    if ((!DVNTContextUtils.isContextDead(paramActivity)) && (DVNTAbstractAsyncAPI.isUserSession(paramActivity)))
    {
      if (UserUtils.c == null) {
        return;
      }
      paramActivity = DAMobileApplication.b();
      if (paramActivity == null)
      {
        DVNTLog.append("caccao is null", new Object[0]);
        return;
      }
      paramActivity.run(UserUtils.c).subscribeOn(Schedulers.computation()).subscribe(DABaseFragment..Lambda.1.getCovers(), DABaseFragment..Lambda.2.getCovers());
    }
  }
  
  protected GoogleApiClient getApplicationContext()
  {
    return ((BaseActivity)getActivity()).getToolbar();
  }
  
  protected String getString()
  {
    return "/" + getActivity().getClass().getSimpleName() + "/" + getClass().getSimpleName();
  }
  
  protected boolean isAuthenticatedUser()
  {
    return false;
  }
  
  protected void load()
  {
    DVNTCommonAsyncAPI.whoAmI().noCache().call(getActivity(), new DABaseFragment.1(this));
    UserUtils.b = SharedPreferenceUtil.get(getActivity()).getInt("num_watchers", -1);
    DVNTCommonAsyncAPI.userProfile("", false, true).noCache().call(getActivity(), new DABaseFragment.2(this));
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    DAMobileApplication.getRefWatcher(getActivity()).watch(this);
  }
  
  public void onResume()
  {
    int j = 0;
    type = getClass();
    Object localObject1 = id;
    int k = localObject1.length;
    int i = 0;
    if (i < k)
    {
      Object localObject2 = localObject1[i];
      if (!type.equals(localObject2)) {}
    }
    for (i = j;; i = 1)
    {
      if (i != 0)
      {
        localObject1 = "/" + getActivity().getClass().getSimpleName() + "/" + type.getSimpleName();
        TrackerUtil.run(getActivity(), (String)localObject1);
      }
      super.onResume();
      return;
      i += 1;
      break;
    }
  }
  
  public void onStart()
  {
    super.onStart();
    if (isAuthenticatedUser()) {
      getApplicationContext().connect();
    }
  }
  
  public void onStop()
  {
    if (isAuthenticatedUser()) {
      getApplicationContext().disconnect();
    }
    super.onStop();
  }
}
