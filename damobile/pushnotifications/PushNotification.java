package com.deviantart.android.damobile.pushnotifications;

import android.content.Context;
import android.content.Intent;
import android.os.BaseBundle;
import android.os.Bundle;
import com.deviantart.android.damobile.activity.HomeActivity;
import com.deviantart.android.damobile.util.deeplink.DeepLinkController.Route;
import com.deviantart.android.damobile.util.deeplink.DeepLinkUtils;
import com.deviantart.datoolkit.logger.DVNTLog;

public class PushNotification
{
  private PushNotification.PNType j;
  private String l;
  private Intent mIntent;
  private String mProxy;
  
  private PushNotification(PushNotification.Builder paramBuilder)
  {
    b(PushNotification.Builder.a(paramBuilder));
    mProxy = PushNotification.Builder.b(paramBuilder);
    mIntent = PushNotification.Builder.getIntent(paramBuilder);
    l = PushNotification.Builder.d(paramBuilder);
  }
  
  private static Intent a(Context paramContext, DeepLinkController.Route paramRoute, Bundle paramBundle, String... paramVarArgs)
  {
    String[] arrayOfString = new String[paramVarArgs.length];
    int i = 0;
    while (i < paramVarArgs.length)
    {
      String str1 = paramVarArgs[i];
      String str2 = paramBundle.getString(str1);
      if (str2 == null)
      {
        DVNTLog.append("push notification - missing value for key {}, {}", new Object[] { str1, paramBundle });
        return null;
      }
      arrayOfString[i] = str2;
      i += 1;
    }
    paramBundle = new Bundle();
    paramBundle.putString("pn_uri", DeepLinkUtils.toString(paramContext, paramRoute, arrayOfString));
    return new Intent(paramContext, HomeActivity.class).putExtras(paramBundle);
  }
  
  static PushNotification b(Context paramContext, Bundle paramBundle)
  {
    if (paramBundle == null) {
      return null;
    }
    Object localObject2 = paramBundle.getString("type");
    Object localObject1 = paramBundle.getString("message");
    if ((localObject2 == null) || (localObject1 == null))
    {
      DVNTLog.append("push notification error : type or message is null {} ", new Object[] { paramBundle });
      return null;
    }
    for (;;)
    {
      try
      {
        localObject2 = PushNotification.PNType.valueOf(((String)localObject2).toUpperCase());
        localObject1 = new PushNotification.Builder().b((String)localObject1).a("DeviantART");
        switch (PushNotification.1.this$0[localObject2.ordinal()])
        {
        default: 
          paramContext = null;
          DVNTLog.get("push notification : received bundle {} ", new Object[] { paramBundle });
          if (paramContext != null) {
            break label520;
          }
          DVNTLog.append("push notification : could not build intent from this bundle", new Object[0]);
          return null;
        }
      }
      catch (IllegalArgumentException paramContext)
      {
        paramContext.printStackTrace();
        return null;
      }
      paramContext = a(paramContext, DeepLinkController.Route.i, paramBundle, new String[] { "noteid" });
      continue;
      paramContext = a(paramContext, DeepLinkController.Route.d, paramBundle, new String[] { "username" });
      continue;
      paramContext = a(paramContext, DeepLinkController.Route.r, paramBundle, new String[] { "statusid" });
      continue;
      paramContext = a(paramContext, DeepLinkController.Route.g, paramBundle, new String[] { "deviationid" });
      continue;
      paramBundle.putString("type", "deviation");
      paramContext = a(paramContext, DeepLinkController.Route.x, paramBundle, new String[] { "type", "deviationid", "commentid" });
      continue;
      paramBundle.putString("type", "status");
      paramContext = a(paramContext, DeepLinkController.Route.x, paramBundle, new String[] { "type", "statusid", "commentid" });
      continue;
      paramBundle.putString("type", "profile");
      paramContext = a(paramContext, DeepLinkController.Route.x, paramBundle, new String[] { "type", "username", "commentid" });
      continue;
      paramContext = a(paramContext, DeepLinkController.Route.g, paramBundle, new String[] { "deviationid" });
      continue;
      paramContext = a(paramContext, DeepLinkController.Route.a, paramBundle, new String[] { "username", "collectionid" });
    }
    label520:
    return ((PushNotification.Builder)localObject1).a(paramContext).getThemedContext();
  }
  
  public PushNotification b(PushNotification.PNType paramPNType)
  {
    j = paramPNType;
    return this;
  }
  
  public String c()
  {
    return l;
  }
  
  public Intent getIntent()
  {
    return mIntent;
  }
  
  public String replace()
  {
    return mProxy;
  }
}
