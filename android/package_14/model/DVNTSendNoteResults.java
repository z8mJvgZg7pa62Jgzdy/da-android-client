package com.deviantart.android.android.package_14.model;

import com.deviantart.android.sdk.api.model.DVNTSuccessByUser.List;
import com.deviantart.android.sdk.api.network.wrapper.DVNTResultsWrapper;

public class DVNTSendNoteResults
  extends DVNTResultsWrapper<DVNTSuccessByUser.List>
{
  public DVNTSendNoteResults() {}
}
