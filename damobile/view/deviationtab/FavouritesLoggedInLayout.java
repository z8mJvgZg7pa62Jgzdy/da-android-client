package com.deviantart.android.damobile.view.deviationtab;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.widget.Toast;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.listener.DVNTAsyncRequestListener;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTCollection;
import com.deviantart.android.android.package_14.model.DVNTDeviationMetadata;
import com.deviantart.android.android.package_14.model.DVNTGallection;
import com.deviantart.android.damobile.adapter.recyclerview.DAStateRecyclerViewAdapter;
import com.deviantart.android.damobile.adapter.recyclerview.DeviationFavePanelAdapter;
import com.deviantart.android.damobile.adapter.recyclerview.DeviationFavePanelAdapter.ItemSelectedListener;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.loader.APIDeviationFavTabLoader;
import com.deviantart.android.damobile.stream.loader.APIDeviationFavTabLoader.MetadataReadyListener;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.DeviationCompositeModel;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.view.DAStateRecyclerView;
import java.util.ArrayList;
import java.util.Iterator;

public class FavouritesLoggedInLayout
  extends DAStateRecyclerView
  implements DeviationFavePanelAdapter.ItemSelectedListener, APIDeviationFavTabLoader.MetadataReadyListener
{
  private boolean b = false;
  private DeviationCompositeModel d;
  private boolean e;
  
  public FavouritesLoggedInLayout(Context paramContext, DeviationCompositeModel paramDeviationCompositeModel, boolean paramBoolean)
  {
    super(paramContext);
    d = paramDeviationCompositeModel;
    e = paramBoolean;
    paramDeviationCompositeModel = paramDeviationCompositeModel.getItem().getId();
    setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
    int i = Graphics.add(paramContext, 20);
    setPadding(i, 0, i, 0);
    setBackgroundColor(ContextCompat.getColor(paramContext, 2131558417));
    setAdapter();
    paramContext = new APIDeviationFavTabLoader(this, paramDeviationCompositeModel);
    paramContext.add(50);
    paramContext = new Stream(paramContext);
    if (getAdapter() != null)
    {
      DVNTDeviationMetadata localDVNTDeviationMetadata = getAdapter().getOrigin();
      if (localDVNTDeviationMetadata != null)
      {
        paramBoolean = bool;
        if (localDVNTDeviationMetadata.getDeviationId().equals(paramDeviationCompositeModel)) {
          break label139;
        }
      }
    }
    for (paramBoolean = true; paramBoolean; paramBoolean = true)
    {
      label139:
      paramContext.a();
      ((APIDeviationFavTabLoader)paramContext.length()).goTo(paramBoolean);
      setAdapter(new DeviationFavePanelAdapter(paramContext, this));
      d(true);
      return;
    }
  }
  
  private void a(DVNTDeviationMetadata paramDVNTDeviationMetadata, DVNTCollection paramDVNTCollection)
  {
    if (paramDVNTDeviationMetadata.getCollections().contains(paramDVNTCollection))
    {
      paramDVNTDeviationMetadata.getCollections().remove(paramDVNTCollection);
      Toast.makeText(getContext(), getContext().getString(2131230998), 0).show();
      if (paramDVNTDeviationMetadata.getCollections().size() == 0) {
        d.b(false);
      }
    }
    for (;;)
    {
      getAdapter().a(paramDVNTDeviationMetadata);
      l.c();
      return;
      paramDVNTDeviationMetadata.getCollections().add(paramDVNTCollection);
      d.b(true);
      Toast.makeText(getContext(), getContext().getString(2131230997), 0).show();
    }
  }
  
  public void b(DVNTDeviationMetadata paramDVNTDeviationMetadata)
  {
    getAdapter().a(paramDVNTDeviationMetadata);
  }
  
  public void b(DVNTDeviationMetadata paramDVNTDeviationMetadata, DVNTCollection paramDVNTCollection)
  {
    if (b) {
      return;
    }
    b = true;
    boolean bool = paramDVNTDeviationMetadata.getCollections().contains(paramDVNTCollection);
    if (bool) {}
    for (Object localObject = getContext().getString(2131230971);; localObject = getContext().getString(2131230923))
    {
      localObject = new FavouritesLoggedInLayout.1(this, (String)localObject, paramDVNTDeviationMetadata, paramDVNTCollection);
      a(paramDVNTDeviationMetadata, paramDVNTCollection);
      if (!bool) {
        break;
      }
      DVNTCommonAsyncAPI.removeFromFavourites(paramDVNTDeviationMetadata.getDeviationId(), paramDVNTCollection.getFolderId()).call(getContext(), (DVNTAsyncRequestListener)localObject);
      return;
    }
    DVNTCommonAsyncAPI.addToFavourites(paramDVNTDeviationMetadata.getDeviationId(), paramDVNTCollection.getFolderId()).call(getContext(), (DVNTAsyncRequestListener)localObject);
  }
  
  public void c()
  {
    super.c();
    if (e)
    {
      e = false;
      doInBackground();
    }
  }
  
  public void doInBackground()
  {
    if (getAdapter() == null)
    {
      e = true;
      return;
    }
    DVNTDeviationMetadata localDVNTDeviationMetadata = getAdapter().getOrigin();
    if (localDVNTDeviationMetadata == null)
    {
      e = true;
      return;
    }
    if (localDVNTDeviationMetadata.getCollections().size() == 0)
    {
      Iterator localIterator = getAdapter().get().get().iterator();
      DVNTCollection localDVNTCollection;
      do
      {
        if (!localIterator.hasNext()) {
          break;
        }
        localDVNTCollection = (DVNTCollection)localIterator.next();
      } while (!"Featured".equals(localDVNTCollection.getName()));
      for (;;)
      {
        if (localDVNTCollection == null)
        {
          e = true;
          return;
        }
        if (b) {
          break;
        }
        b = true;
        a(localDVNTDeviationMetadata, localDVNTCollection);
        DVNTCommonAsyncAPI.addToFavourites(localDVNTDeviationMetadata.getDeviationId(), localDVNTCollection.getFolderId()).call(getContext(), new FavouritesLoggedInLayout.2(this, localDVNTDeviationMetadata, localDVNTCollection));
        return;
        localDVNTCollection = null;
      }
    }
  }
  
  public DeviationFavePanelAdapter getAdapter()
  {
    return (DeviationFavePanelAdapter)super.getAdapter();
  }
}
