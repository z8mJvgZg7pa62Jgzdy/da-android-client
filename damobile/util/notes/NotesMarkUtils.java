package com.deviantart.android.damobile.util.notes;

import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.opt.ItemDeleteType;
import com.deviantart.android.damobile.util.opt.ItemDeleteUtils;
import com.deviantart.android.damobile.util.opt.NoteItemDeleteHelper;

public class NotesMarkUtils
{
  public NotesMarkUtils() {}
  
  public static void a(NotesPage paramNotesPage, NotesItemData paramNotesItemData, Mark paramMark, boolean paramBoolean)
  {
    paramNotesPage = ((NoteItemDeleteHelper)ItemDeleteUtils.a(ItemDeleteType.c)).b(paramNotesPage);
    if (paramNotesPage == null) {
      return;
    }
    int j = paramNotesPage.a(paramNotesItemData);
    if (paramBoolean)
    {
      if (paramMark.e() < 0) {}
      for (int i = 1; (j == -1) && (i == 0); i = 0)
      {
        paramNotesPage.a(0, paramNotesItemData);
        return;
      }
      if ((j != -1) && (i != 0)) {
        paramNotesPage.close(j);
      }
    }
    else if (j != -1)
    {
      paramNotesPage = (NotesItemData)paramNotesPage.get(j);
      paramMark.b().b(paramNotesPage);
    }
  }
}
