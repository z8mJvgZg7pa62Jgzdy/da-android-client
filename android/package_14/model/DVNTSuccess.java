package com.deviantart.android.android.package_14.model;

import java.io.Serializable;

public class DVNTSuccess
  implements Serializable
{
  boolean success;
  
  public DVNTSuccess() {}
  
  public boolean isSuccess()
  {
    return success;
  }
  
  public void setSuccess(boolean paramBoolean)
  {
    success = paramBoolean;
  }
}
