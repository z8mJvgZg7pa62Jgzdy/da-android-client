package com.deviantart.android.damobile.view.userprofile;

import android.content.Context;
import com.deviantart.android.damobile.adapter.recyclerview.BaseGallectionAdapter;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APICollectionFoldersLoader;

public class UserProfileCollectionLayout
  extends UserProfileGallectionBaseLayout
{
  public UserProfileCollectionLayout(Context paramContext, String paramString, boolean paramBoolean)
  {
    super(paramContext, paramString, paramBoolean, null);
  }
  
  public String b(Context paramContext)
  {
    return a + " " + paramContext.getString(2131230887);
  }
  
  public String c(Context paramContext)
  {
    return paramContext.getString(2131230886);
  }
  
  protected String getType()
  {
    return "collection";
  }
  
  public BaseGallectionAdapter visitLabel()
  {
    return new UserProfileCollectionLayout.CollectionAdapter(StreamCacher.a(new APICollectionFoldersLoader(a, true, true)), a, getType(), c);
  }
}
