package com.deviantart.android.damobile.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;
import android.widget.ScrollView;

public class MaxHeightScrollView
  extends ScrollView
{
  private int mMaxHeight;
  
  public MaxHeightScrollView(Context paramContext)
  {
    super(paramContext);
  }
  
  public MaxHeightScrollView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public MaxHeightScrollView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, View.MeasureSpec.makeMeasureSpec(mMaxHeight, Integer.MIN_VALUE));
  }
  
  public void setMaxHeight(int paramInt)
  {
    mMaxHeight = paramInt;
  }
}
