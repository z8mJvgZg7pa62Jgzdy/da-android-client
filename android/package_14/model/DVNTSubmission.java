package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTSubmission
  implements Serializable
{
  String category;
  @SerializedName("creation_time")
  String creationTime;
  @SerializedName("file_size")
  String fileSize;
  String resolution;
  @SerializedName("submitted_with")
  DVNTSubmission.DVNTSubmittedWith submittedWith;
  
  public DVNTSubmission() {}
  
  public String getCategory()
  {
    return category;
  }
  
  public String getCreationTime()
  {
    return creationTime;
  }
  
  public String getFileSize()
  {
    return fileSize;
  }
  
  public String getResolution()
  {
    return resolution;
  }
  
  public DVNTSubmission.DVNTSubmittedWith getSubmittedWith()
  {
    return submittedWith;
  }
  
  public void setCategory(String paramString)
  {
    category = paramString;
  }
  
  public void setCreationTime(String paramString)
  {
    creationTime = paramString;
  }
  
  public void setFileSize(String paramString)
  {
    fileSize = paramString;
  }
  
  public void setResolution(String paramString)
  {
    resolution = paramString;
  }
  
  public void setSubmittedWith(DVNTSubmission.DVNTSubmittedWith paramDVNTSubmittedWith)
  {
    submittedWith = paramDVNTSubmittedWith;
  }
}
