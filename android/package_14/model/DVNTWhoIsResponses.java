package com.deviantart.android.android.package_14.model;

import com.deviantart.android.sdk.api.model.DVNTWhoIsResponse.List;
import com.deviantart.android.sdk.api.network.wrapper.DVNTResultsWrapper;

public class DVNTWhoIsResponses
  extends DVNTResultsWrapper<DVNTWhoIsResponse.List>
{
  public DVNTWhoIsResponses() {}
}
