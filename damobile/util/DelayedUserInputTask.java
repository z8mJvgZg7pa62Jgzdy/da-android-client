package com.deviantart.android.damobile.util;

import android.os.AsyncTask;

public abstract class DelayedUserInputTask
  extends AsyncTask<String, Void, String>
{
  private static int lastLevel = 1000;
  
  public DelayedUserInputTask() {}
  
  protected String dump(String... paramVarArgs)
  {
    long l = lastLevel;
    try
    {
      Thread.sleep(l);
      return paramVarArgs[0];
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;)
      {
        Thread.interrupted();
      }
    }
  }
  
  protected void onPreExecute() {}
  
  protected void onProgressUpdate(Void... paramVarArgs) {}
  
  protected abstract void onRefresh(String paramString);
}
