package com.deviantart.android.damobile.util;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.HomeActivity.HomeActivityPages;
import com.deviantart.android.damobile.fragment.HomeBaseFragment.HomeFragmentInstanceBuilder;
import com.deviantart.android.damobile.fragment.UserProfileFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.UserProfileFragment.UserProfileTab;
import com.deviantart.android.damobile.fragment.VerifyEmailDialogFragment;
import com.deviantart.android.damobile.pushnotifications.GCMRegistration;
import com.deviantart.android.damobile.view.dialogs.AlertDialogsBuilders;
import com.facebook.drawee.view.GenericDraweeView;
import com.facebook.drawee.view.SimpleDraweeView;
import java.io.File;
import org.apmem.tools.layouts.FlowLayout.LayoutParams;

public class UserUtils
{
  public static String a;
  public static int b = -1;
  public static String c;
  public static boolean d;
  public static boolean e;
  public static String f;
  public static MemberType g;
  public static boolean h;
  public static boolean i = true;
  private static File k = null;
  private static File o = null;
  private static String p;
  public static boolean running;
  
  static
  {
    e = false;
    d = false;
    h = true;
    running = true;
  }
  
  public UserUtils() {}
  
  public static LinearLayout a(Context paramContext, String paramString1, String paramString2)
  {
    return onCreateView(paramContext, paramString1, paramString2, false);
  }
  
  public static File a()
  {
    return k;
  }
  
  public static void a(File paramFile)
  {
    o = paramFile;
  }
  
  public static void a(String paramString)
  {
    p = paramString;
  }
  
  public static void b(File paramFile)
  {
    k = paramFile;
  }
  
  public static void b(String paramString)
  {
    g = MemberType.toString(paramString);
  }
  
  public static void c(Activity paramActivity, String paramString)
  {
    if (DVNTContextUtils.isContextDead(paramActivity)) {
      return;
    }
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (a != null)
    {
      bool1 = bool2;
      if (a.equals(paramString)) {
        bool1 = true;
      }
    }
    ScreenFlowManager.a(paramActivity, (Fragment)new UserProfileFragment.InstanceBuilder().a(paramString).a(bool1).a(), "user_profile" + paramString);
  }
  
  public static void c(Activity paramActivity, String paramString, UserProfileFragment.UserProfileTab paramUserProfileTab)
  {
    boolean bool = false;
    if (((a != null) && (a.equals(paramString))) || (paramString.equals(""))) {
      bool = true;
    }
    ScreenFlowManager.a(paramActivity, (Fragment)new UserProfileFragment.InstanceBuilder().a(paramString).a(bool).b(paramUserProfileTab).a(), "user_profile" + paramString);
  }
  
  public static void c(Activity paramActivity, String paramString1, String paramString2, boolean paramBoolean)
  {
    String str2 = a;
    String str1 = str2;
    if (str2 == null) {
      str1 = SharedPreferenceUtil.getString(paramActivity, "recent_username", null);
    }
    if ((str1 != null) && (str1.equals(paramString1))) {}
    for (boolean bool = true;; bool = false)
    {
      ScreenFlowManager.get(paramActivity, (Fragment)new UserProfileFragment.InstanceBuilder().a(paramString1).a(bool).b(paramString2).a(), HomeActivity.HomeActivityPages.b.a() + paramString1, paramBoolean);
      return;
    }
  }
  
  public static boolean c(Context paramContext)
  {
    return (DVNTAbstractAsyncAPI.isUserSession(paramContext)) && (h);
  }
  
  public static void connect(Context paramContext, boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (b += 1;; b -= 1)
    {
      paramContext = SharedPreferenceUtil.get(paramContext).edit();
      paramContext.putInt("num_watchers", b);
      paramContext.apply();
      return;
    }
  }
  
  public static File d()
  {
    return o;
  }
  
  public static String get()
  {
    return "http://a.deviantart.net/avatars/default.gif";
  }
  
  public static boolean get(Context paramContext)
  {
    return paramContext.getSharedPreferences("setting_mature" + a, 0).getBoolean("setting_mature" + a, false);
  }
  
  public static boolean get(String paramString)
  {
    return (a != null) && (a.equals(paramString));
  }
  
  public static String getP()
  {
    return p;
  }
  
  public static boolean getValue(Context paramContext)
  {
    return SharedPreferenceUtil.getValue(paramContext, "setting_rc_only_content", false);
  }
  
  public static void onClick(Activity paramActivity)
  {
    if (!DVNTAbstractAsyncAPI.isUserSession(paramActivity))
    {
      AlertDialogsBuilders.deleteAlarm(paramActivity).create().show();
      return;
    }
    if (i)
    {
      AlertDialogsBuilders.showConfirmationDialog(paramActivity).create().show();
      return;
    }
    if (!get(paramActivity)) {
      AlertDialogsBuilders.createDialog(paramActivity).create().show();
    }
  }
  
  public static LinearLayout onCreateView(Context paramContext, String paramString1, String paramString2, boolean paramBoolean)
  {
    LinearLayout localLinearLayout = (LinearLayout)LayoutInflater.from(paramContext).inflate(2130968756, null, false);
    Object localObject;
    if (paramBoolean)
    {
      localObject = new FlowLayout.LayoutParams(-2, -2);
      ((ViewGroup.MarginLayoutParams)localObject).setMargins(0, 0, paramContext.getResources().getDimensionPixelSize(2131361959), paramContext.getResources().getDimensionPixelSize(2131361958));
      localLinearLayout.setLayoutParams((ViewGroup.LayoutParams)localObject);
    }
    for (;;)
    {
      paramContext = (TextView)ButterKnife.findById(localLinearLayout, 2131690021);
      localObject = (SimpleDraweeView)ButterKnife.findById(localLinearLayout, 2131690019);
      paramContext.setText(paramString1);
      if (paramString2 == null) {
        break;
      }
      ImageUtils.isEmpty((GenericDraweeView)localObject, Uri.parse(paramString2));
      return localLinearLayout;
      localObject = new LinearLayout.LayoutParams(-2, -2);
      ((ViewGroup.MarginLayoutParams)localObject).setMargins(0, 0, paramContext.getResources().getDimensionPixelSize(2131361974), 0);
      localLinearLayout.setLayoutParams((ViewGroup.LayoutParams)localObject);
    }
    return localLinearLayout;
  }
  
  public static void reconfigure(Context paramContext)
  {
    DVNTAbstractAsyncAPI.logout(paramContext);
    GCMRegistration.clearNotification(paramContext);
  }
  
  public static void saveBoolean(Context paramContext, boolean paramBoolean)
  {
    paramContext.getSharedPreferences("setting_mature" + a, 0).edit().putBoolean("setting_mature" + a, paramBoolean).apply();
  }
  
  public static void showInfoDialog(Activity paramActivity)
  {
    new VerifyEmailDialogFragment().show(paramActivity.getFragmentManager(), "VerifyEmailDialogFragment");
  }
}
