package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.util.WatchFeedContentsSetting;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.util.tracking.TrackerUtil.EventLabelBuilder;

public class WatchFeedManageContentLayout
  extends FrameLayout
{
  @Bind({2131690137})
  SwitchCompat collectionSwitch;
  @Bind({2131690136})
  SwitchCompat deviationsSwitch;
  @Bind({2131690139})
  SwitchCompat groupDeviationsSwitch;
  @Bind({2131690138})
  SwitchCompat journalsSwitch;
  @Bind({2131690093})
  ProgressBar loader;
  @Bind({2131689779})
  ScrollView mainView;
  WatchFeedContentsSetting o;
  boolean r = false;
  @Bind({2131690135})
  SwitchCompat statusSwitch;
  
  public WatchFeedManageContentLayout(Context paramContext)
  {
    super(paramContext);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130968802, this, true);
    ButterKnife.bind(this);
    hide();
    readMessage();
  }
  
  private String a(String paramString, boolean paramBoolean)
  {
    if (paramBoolean) {
      return new TrackerUtil.EventLabelBuilder().get(paramString, "On").getValue();
    }
    return new TrackerUtil.EventLabelBuilder().get(paramString, "Off").getValue();
  }
  
  private void b()
  {
    statusSwitch.setChecked(o.c());
    deviationsSwitch.setChecked(o.a());
    journalsSwitch.setChecked(o.getValue());
    groupDeviationsSwitch.setChecked(o.f());
    collectionSwitch.setChecked(o.q());
    r = true;
  }
  
  private void hide()
  {
    mainView.setVisibility(8);
    loader.setVisibility(0);
  }
  
  private void onResume()
  {
    mainView.setVisibility(0);
    loader.setVisibility(8);
  }
  
  private void readMessage()
  {
    DVNTCommonAsyncAPI.feedSettings().noCache().call(getContext(), new WatchFeedManageContentLayout.1(this));
  }
  
  public boolean c()
  {
    return (o != null) && (o.b());
  }
  
  public WatchFeedContentsSetting getContentsSetting()
  {
    return o;
  }
  
  public void onChangeCollections(boolean paramBoolean)
  {
    if (!r) {
      return;
    }
    o.a(paramBoolean);
    TrackerUtil.a((Activity)getContext(), "toggle_settings_button", a("Collections", paramBoolean));
  }
  
  public void onChangeDeviations(boolean paramBoolean)
  {
    if (!r) {
      return;
    }
    o.f(paramBoolean);
    TrackerUtil.a((Activity)getContext(), "toggle_settings_button", a("Deviations", paramBoolean));
  }
  
  public void onChangeGroups(boolean paramBoolean)
  {
    if (!r) {
      return;
    }
    o.d(paramBoolean);
    TrackerUtil.a((Activity)getContext(), "toggle_settings_button", a("Groups", paramBoolean));
  }
  
  public void onChangeJournals(boolean paramBoolean)
  {
    if (!r) {
      return;
    }
    o.b(paramBoolean);
    TrackerUtil.a((Activity)getContext(), "toggle_settings_button", a("Journal", paramBoolean));
  }
  
  public void onChangeStatus(boolean paramBoolean)
  {
    if (!r) {
      return;
    }
    o.c(paramBoolean);
    TrackerUtil.a((Activity)getContext(), "toggle_settings_button", a("Status", paramBoolean));
  }
}
