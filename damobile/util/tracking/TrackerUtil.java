package com.deviantart.android.damobile.util.tracking;

import android.app.Activity;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.DAMobileApplication;
import com.deviantart.android.damobile.DAMobileApplication.TrackerName;
import com.deviantart.android.damobile.util.MemberType;
import com.google.android.com.analytics.HitBuilders.AppViewBuilder;
import com.google.android.com.analytics.HitBuilders.EventBuilder;
import com.google.android.com.analytics.HitBuilders.HitBuilder;
import com.google.android.com.analytics.Tracker;

public class TrackerUtil
{
  public static EventKeys.Category b;
  
  public TrackerUtil() {}
  
  public static void a(Activity paramActivity, EventKeys.Category paramCategory, String paramString)
  {
    read(paramActivity, paramCategory, paramString, null, null);
  }
  
  public static void a(Activity paramActivity, TrackerUtil.TrackerDimension paramTrackerDimension, EventKeys.Category paramCategory, String paramString1, String paramString2)
  {
    run(paramActivity, paramTrackerDimension, paramCategory, paramString1, paramString2, null);
  }
  
  public static void a(Activity paramActivity, String paramString)
  {
    a(paramActivity, paramString, null, null);
  }
  
  public static void a(Activity paramActivity, String paramString1, String paramString2)
  {
    a(paramActivity, paramString1, paramString2, null);
  }
  
  public static void a(Activity paramActivity, String paramString1, String paramString2, Long paramLong)
  {
    read(paramActivity, b, paramString1, paramString2, paramLong);
  }
  
  public static void append(Activity paramActivity, EventKeys.Category paramCategory, int paramInt1, int paramInt2)
  {
    String str2 = String.valueOf(paramInt1);
    String str1 = str2;
    if (paramInt1 == paramInt2) {
      str1 = str2 + "+";
    }
    get(paramActivity, paramCategory, "scroll_down", new TrackerUtil.EventLabelBuilder().get("offset", str1).getValue());
  }
  
  public static void b(Activity paramActivity, MemberType paramMemberType, String paramString1, String paramString2)
  {
    if (paramString2 != null)
    {
      if (paramMemberType == null) {
        return;
      }
      TrackerUtil.TrackerDimension localTrackerDimension = TrackerUtil.TrackerDimension.o;
      if (paramMemberType.b()) {
        localTrackerDimension.d("Group");
      }
      for (;;)
      {
        a(paramActivity, localTrackerDimension, EventKeys.Category.q, paramString1, "source=" + paramString2);
        return;
        localTrackerDimension.d("Deviant");
      }
    }
  }
  
  public static void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      TrackerUtil.TrackerDimension.l.d("logged_in");
      return;
    }
    TrackerUtil.TrackerDimension.l.d("logged_out");
  }
  
  public static void get(Activity paramActivity, EventKeys.Category paramCategory, String paramString1, String paramString2)
  {
    read(paramActivity, paramCategory, paramString1, paramString2, null);
  }
  
  public static void invoke(Activity paramActivity, String paramString)
  {
    ((DAMobileApplication)paramActivity.getApplication()).get(DAMobileApplication.TrackerName.i).put("&uid", paramString);
  }
  
  public static void read(Activity paramActivity, EventKeys.Category paramCategory, String paramString1, String paramString2, Long paramLong)
  {
    run(paramActivity, null, paramCategory, paramString1, paramString2, paramLong);
  }
  
  public static void run(Activity paramActivity, TrackerUtil.TrackerDimension paramTrackerDimension, EventKeys.Category paramCategory, String paramString1, String paramString2, Long paramLong)
  {
    if ((paramCategory != null) && (paramString1 != null) && (paramCategory.execute(paramString1)))
    {
      if (DVNTContextUtils.isContextDead(paramActivity)) {
        return;
      }
      paramActivity = ((DAMobileApplication)paramActivity.getApplication()).get(DAMobileApplication.TrackerName.i);
      HitBuilders.EventBuilder localEventBuilder = new HitBuilders.EventBuilder();
      if ((paramTrackerDimension != null) && (paramTrackerDimension.getName() != null)) {
        localEventBuilder.setCustomDimension(paramTrackerDimension.getTitle(), paramTrackerDimension.getName());
      }
      localEventBuilder.setCategory(paramCategory.text()).setAction(paramString1);
      if (paramString2 != null) {
        localEventBuilder.setLabel(paramString2);
      }
      if (paramLong != null) {
        localEventBuilder.setValue(paramLong.longValue());
      }
      paramActivity.send(localEventBuilder.build());
    }
  }
  
  public static void run(Activity paramActivity, String paramString)
  {
    paramActivity = ((DAMobileApplication)paramActivity.getApplication()).get(DAMobileApplication.TrackerName.i);
    paramActivity.setScreenName(paramString);
    paramString = new HitBuilders.AppViewBuilder();
    TrackerUtil.TrackerDimension[] arrayOfTrackerDimension = TrackerUtil.TrackerDimension.values();
    int j = arrayOfTrackerDimension.length;
    int i = 0;
    if (i < j)
    {
      TrackerUtil.TrackerDimension localTrackerDimension = arrayOfTrackerDimension[i];
      if (!localTrackerDimension.getValue()) {}
      for (;;)
      {
        i += 1;
        break;
        paramString.setCustomDimension(localTrackerDimension.getTitle(), localTrackerDimension.getName());
      }
    }
    paramActivity.send(paramString.build());
  }
}
