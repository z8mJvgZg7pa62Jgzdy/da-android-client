package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTFeedbackType;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.damobile.util.notifications.NotificationItemData;

public class APINotificationsLoader
  extends StreamLoader<NotificationItemData>
{
  private final DVNTFeedbackType d;
  
  public APINotificationsLoader(DVNTFeedbackType paramDVNTFeedbackType)
  {
    d = paramDVNTFeedbackType;
  }
  
  public String b()
  {
    return "notifications" + d.toString();
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTAsyncAPI.feedbackStackedMessages(d, null, Integer.valueOf(paramInt), Integer.valueOf(a), Integer.valueOf(2));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APINotificationsLoader.1(this, paramStreamLoadListener));
  }
}
