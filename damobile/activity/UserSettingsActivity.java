package com.deviantart.android.damobile.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.fragment.usersettings.UserSettingsBaseFragment;
import com.deviantart.android.damobile.fragment.usersettings.UserSettingsMainMenuFragment;
import com.deviantart.android.damobile.fragment.usersettings.UserSettingsWatchFeedManageFragment;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnGetUserInfoEvent;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.UserSettingUpdateType;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.WatchFeedContentsSetting;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.view.VerificationBanner;
import com.squareup.otto.Bus;

public class UserSettingsActivity
  extends LoggedInActivity
{
  private UserSettingsBaseFragment a;
  private boolean b = false;
  private boolean c = false;
  private boolean e = false;
  private WatchFeedContentsSetting f;
  private boolean g = false;
  @Bind({2131689609})
  VerificationBanner verificationBanner;
  
  public UserSettingsActivity() {}
  
  public static Intent createIntent(Context paramContext, boolean paramBoolean)
  {
    paramContext = new Intent(paramContext, UserSettingsActivity.class);
    paramContext.putExtra("is_from_watch_feed", paramBoolean);
    return paramContext;
  }
  
  private void reloadPrefs()
  {
    if (UserUtils.e) {
      verificationBanner.setVisibility(8);
    }
  }
  
  public static Intent startActivity(Context paramContext)
  {
    return createIntent(paramContext, false);
  }
  
  public void a(UserSettingsBaseFragment paramUserSettingsBaseFragment)
  {
    a = paramUserSettingsBaseFragment;
  }
  
  public void b(UserSettingUpdateType paramUserSettingUpdateType)
  {
    switch (UserSettingsActivity.1.d[paramUserSettingUpdateType.ordinal()])
    {
    default: 
      return;
    case 1: 
      e = true;
      return;
    }
    c = true;
  }
  
  public void b(WatchFeedContentsSetting paramWatchFeedContentsSetting)
  {
    f = paramWatchFeedContentsSetting;
  }
  
  public void onBackPressed()
  {
    if ((a.a() == UserSettingsActivity.OnBackResult.d) || ((a.getClass().equals(UserSettingsWatchFeedManageFragment.class)) && (b)) || (a.getClass() == UserSettingsMainMenuFragment.class))
    {
      onPostExecute();
      return;
    }
    super.onBackPressed();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968610);
    setSupportActionBar((Toolbar)findViewById(2131689845));
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    ButterKnife.bind(this);
    BusStation.get().register(this);
    reloadPrefs();
    b = getIntent().getBooleanExtra("is_from_watch_feed", false);
    if (b)
    {
      com.deviantart.android.damobile.util.tracking.TrackerUtil.b = EventKeys.Category.b;
      a = new UserSettingsWatchFeedManageFragment();
    }
    for (paramBundle = "manage_watch_feed";; paramBundle = "user_settings_main_menu")
    {
      ScreenFlowManager.get(this, a, paramBundle, false);
      return;
      com.deviantart.android.damobile.util.tracking.TrackerUtil.b = EventKeys.Category.e;
      a = new UserSettingsMainMenuFragment();
    }
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    BusStation.get().unregister(this);
  }
  
  public void onGetUserInfoBusEvent(BusStation.OnGetUserInfoEvent paramOnGetUserInfoEvent)
  {
    reloadPrefs();
  }
  
  public void onPostExecute()
  {
    Intent localIntent = new Intent();
    localIntent.putExtra("profile_picture_updated", c);
    localIntent.putExtra("avatar_updated", e);
    localIntent.putExtra("profile_updated", g);
    if (f != null) {
      localIntent.putExtra("updated_watch_feed_setting", f);
    }
    setResult(-1, localIntent);
    com.deviantart.android.damobile.util.tracking.TrackerUtil.b = null;
    finish();
    overridePendingTransition(2131034140, 2131034144);
  }
  
  public void onProgressChanged(int paramInt)
  {
    getSupportActionBar().setTitle(paramInt);
  }
  
  protected void onResume()
  {
    super.onResume();
    reloadPrefs();
  }
  
  public boolean onSupportNavigateUp()
  {
    onBackPressed();
    return true;
  }
  
  public void setColor(boolean paramBoolean)
  {
    g = paramBoolean;
  }
}
