package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTWatchRecommendationsRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTWatchRecommendationResponse>
{
  private static final int MAX_LIMIT = 5;
  private final String cursor;
  private final Integer limit;
  
  public DVNTWatchRecommendationsRequestV1(String paramString, Integer paramInteger)
  {
    super(com.deviantart.android.sdk.api.model.DVNTWatchRecommendationResponse.class);
    cursor = paramString;
    limit = Integer.valueOf(Math.min(paramInteger.intValue(), 5));
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTWatchRecommendationsRequestV1)paramObject;
    return (Objects.append(cursor, cursor)) && (Objects.append(limit, limit));
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), cursor, limit });
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTWatchRecommendationResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).getWatchRecommendations(paramString, null, cursor, limit);
  }
}
