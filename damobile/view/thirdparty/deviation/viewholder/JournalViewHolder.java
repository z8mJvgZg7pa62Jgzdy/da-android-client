package com.deviantart.android.damobile.view.thirdparty.deviation.viewholder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.view.DAFaveView;

public class JournalViewHolder
  extends RecyclerView.ViewHolder
{
  @Bind({2131689919})
  public DAFaveView faveView;
  @Bind({2131689943})
  public TextView preview;
  @Bind({2131689942})
  public TextView title;
  
  public JournalViewHolder(View paramView)
  {
    super(paramView);
    ButterKnife.bind(this, paramView);
  }
}
