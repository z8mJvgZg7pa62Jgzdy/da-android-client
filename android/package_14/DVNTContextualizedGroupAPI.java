package com.deviantart.android.android.package_14;

import android.content.Context;
import com.deviantart.android.android.package_14.config.DVNTAPIConfig;
import com.deviantart.android.android.package_14.listener.DVNTAsyncRequestListener;
import com.deviantart.android.android.package_14.network.request.DVNTAccountValidationRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTAsyncGroupedRequest;
import com.deviantart.android.android.package_14.network.request.DVNTCollectionFoldersRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTDeviationMetadataRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTGalleryFoldersRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTMoreLikeThisPreviewRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTPlaceboRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTSearchFriendsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTWhoIsRequestV1;
import com.deviantart.android.android.package_14.network.request.VersionedRequest;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class DVNTContextualizedGroupAPI
{
  private DVNTAbstractAsyncAPI dvntAsyncAPI;
  private HashMap<String, com.deviantart.android.sdk.api.network.request.GroupableRequest> requests;
  private WeakReference<Context> weakContext;
  
  public DVNTContextualizedGroupAPI(DVNTAbstractAsyncAPI paramDVNTAbstractAsyncAPI, Context paramContext)
  {
    weakContext = new WeakReference(paramContext);
    requests = new HashMap();
    dvntAsyncAPI = paramDVNTAbstractAsyncAPI;
  }
  
  public DVNTContextualizedGroupAPI addCollectionFolders(String paramString1, boolean paramBoolean1, boolean paramBoolean2, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    requests.put(paramString1, new DVNTCollectionFoldersRequestV1(paramString2, paramBoolean1, paramBoolean2, paramInteger1, paramInteger2));
    return this;
  }
  
  public DVNTContextualizedGroupAPI addDeviationMetadata(String paramString1, String paramString2, Boolean paramBoolean1, Boolean paramBoolean2, Boolean paramBoolean3, Boolean paramBoolean4)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramString2);
    requests.put(paramString1, new DVNTDeviationMetadataRequestV1(localArrayList, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4));
    return this;
  }
  
  public DVNTContextualizedGroupAPI addGalleryFolders(String paramString1, boolean paramBoolean1, boolean paramBoolean2, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    requests.put(paramString1, new DVNTGalleryFoldersRequestV1(paramString2, paramBoolean1, paramBoolean2, paramInteger1, paramInteger2));
    return this;
  }
  
  public DVNTContextualizedGroupAPI addMoreLikeThisPreview(String paramString1, String paramString2)
  {
    requests.put(paramString1, new DVNTMoreLikeThisPreviewRequestV1(paramString2));
    return this;
  }
  
  public DVNTContextualizedGroupAPI addPlacebo(String paramString)
  {
    requests.put(paramString, new DVNTPlaceboRequestV1());
    return this;
  }
  
  public DVNTContextualizedGroupAPI addSearchFriends(String paramString1, String paramString2)
  {
    requests.put(paramString1, new DVNTSearchFriendsRequestV1(paramString2));
    return this;
  }
  
  public DVNTContextualizedGroupAPI addValidateUser(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    requests.put(paramString1, new DVNTAccountValidationRequestV1(paramString2, paramString3, paramString4));
    return this;
  }
  
  public DVNTContextualizedGroupAPI addWhoIs(String paramString, List paramList)
  {
    requests.put(paramString, new DVNTWhoIsRequestV1(paramList));
    return this;
  }
  
  public UUID execute(DVNTAsyncRequestListener paramDVNTAsyncRequestListener)
  {
    Iterator localIterator = requests.values().iterator();
    while (localIterator.hasNext()) {
      ((com.deviantart.android.android.package_14.network.request.GroupableRequest)localIterator.next()).setVersion(dvntAsyncAPI.apiConfig.buildRequestVersion());
    }
    return dvntAsyncAPI.apiClientInstance.userCallAsync((Context)weakContext.get(), new DVNTAsyncGroupedRequest(requests), paramDVNTAsyncRequestListener);
  }
}
