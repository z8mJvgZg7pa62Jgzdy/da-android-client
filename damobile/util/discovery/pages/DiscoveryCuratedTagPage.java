package com.deviantart.android.damobile.util.discovery.pages;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.adapter.recyclerview.DeviationPreviewAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APICuratedTagsLoader;
import com.deviantart.android.damobile.view.DAStateRecyclerView;

public class DiscoveryCuratedTagPage
  extends DiscoveryPage
{
  DAStateRecyclerView d;
  
  public DiscoveryCuratedTagPage(String paramString1, CharSequence paramCharSequence, String paramString2)
  {
    super(paramString1, paramCharSequence, paramString2);
  }
  
  private boolean isHeadsetConnected(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("explanation_explore_visible", true);
  }
  
  private void setStartOnBoot(Context paramContext, boolean paramBoolean)
  {
    paramContext = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    paramContext.putBoolean("explanation_explore_visible", paramBoolean);
    paramContext.apply();
  }
  
  public View a(ViewGroup paramViewGroup)
  {
    super.a(paramViewGroup);
    paramViewGroup = StreamCacher.a(new APICuratedTagsLoader(), StreamCacheStrategy.b);
    d = new DAStateRecyclerView(a);
    d.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    d.setAdapter(new DeviationPreviewAdapter(paramViewGroup));
    a();
    d.setAdapter();
    d.b(true);
    d.a(paramViewGroup.e());
    d.setOnScrollListener(new DiscoveryCuratedTagPage.1(this, paramViewGroup));
    d.setPadding(d.getPaddingLeft() + (int)get().getResources().getDimension(2131361964), d.getPaddingTop(), d.getPaddingRight() + (int)get().getResources().getDimension(2131361964), d.getPaddingBottom());
    return d;
  }
  
  public void a()
  {
    if (d != null)
    {
      if (DVNTContextUtils.isContextDead(d.getContext())) {
        return;
      }
      Object localObject1 = d.getContext();
      d.d();
      Object localObject2 = new View((Context)localObject1);
      ((View)localObject2).setLayoutParams(new ViewGroup.LayoutParams(-1, (int)((Context)localObject1).getResources().getDimension(2131361971)));
      d.a((View)localObject2);
      if (isHeadsetConnected((Context)localObject1))
      {
        localObject1 = (ViewGroup)LayoutInflater.from((Context)localObject1).inflate(2130968650, d, false);
        localObject2 = new DiscoveryCuratedTagPage.ExplanationHeaderViewBinder(this, (View)localObject1);
        explanationTextView.setText(2131230993);
        cancelButton.setOnClickListener(new DiscoveryCuratedTagPage.2(this));
        d.a((View)localObject1);
      }
    }
  }
  
  public void b()
  {
    super.b();
    d = null;
  }
}
