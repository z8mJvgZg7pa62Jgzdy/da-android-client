package com.deviantart.android.damobile.view.thirdparty.decorator;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.view.WatchRecoWatchButton;
import com.facebook.drawee.view.SimpleDraweeView;

public class WatchRecoDecoratorViewHolder
  extends DecoratorViewHolder
{
  @Bind({2131689769})
  SimpleDraweeView avatar;
  @Bind({2131690140})
  View dismiss;
  @Bind({2131689840})
  TextView userName;
  @Bind({2131690098})
  WatchRecoWatchButton watchButton;
  
  public WatchRecoDecoratorViewHolder(RecyclerView.ViewHolder paramViewHolder, View paramView)
  {
    super(paramViewHolder, paramView);
    ButterKnife.bind(this, paramView);
  }
}
