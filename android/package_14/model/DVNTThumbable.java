package com.deviantart.android.android.package_14.model;

public abstract interface DVNTThumbable
{
  public abstract DVNTImage.List getThumbs();
}
