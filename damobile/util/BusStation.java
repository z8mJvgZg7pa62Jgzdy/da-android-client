package com.deviantart.android.damobile.util;

import com.squareup.otto.Bus;

public class BusStation
{
  public static final Bus sInstance = new Bus();
  
  private BusStation() {}
  
  public static Bus get()
  {
    return sInstance;
  }
}
