package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTGetPushNotificationsSettingsRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTPushNotificationsSettings>
{
  public DVNTGetPushNotificationsSettingsRequestV1()
  {
    super(com.deviantart.android.sdk.api.model.DVNTPushNotificationsSettings.class);
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTPushNotificationsSettings sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).getPushNotificationsSettings(paramString);
  }
}
