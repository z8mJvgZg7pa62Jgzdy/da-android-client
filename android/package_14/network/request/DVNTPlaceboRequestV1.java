package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.sdk.api.network.request.GroupableRequest;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.HashMap;

public class DVNTPlaceboRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTStatus>
  implements GroupableRequest
{
  public DVNTPlaceboRequestV1()
  {
    super(com.deviantart.android.sdk.api.model.DVNTStatus.class);
  }
  
  public HashMap getGETArguments()
  {
    return null;
  }
  
  public String getGroupEndpoint()
  {
    return "/api/v1/oauth2/placebo";
  }
  
  public HashMap getPOSTArguments()
  {
    return null;
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTStatus sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).placebo(paramString);
  }
}
