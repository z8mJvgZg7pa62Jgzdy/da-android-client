package com.deviantart.android.damobile.util.torpedo;

import com.deviantart.android.damobile.stream.loader.StreamLoader;

public class DeviationDescription
{
  private com.deviantart.android.android.package_14.model.DVNTDeviation j;
  private StreamLoader<com.deviantart.android.sdk.api.model.DVNTDeviation> k;
  private int o;
  
  public DeviationDescription(com.deviantart.android.android.package_14.model.DVNTDeviation paramDVNTDeviation, StreamLoader paramStreamLoader, int paramInt)
  {
    j = paramDVNTDeviation;
    k = paramStreamLoader;
    o = paramInt;
  }
  
  public com.deviantart.android.android.package_14.model.DVNTDeviation a()
  {
    return j;
  }
  
  public int c()
  {
    return o;
  }
  
  public StreamLoader d()
  {
    return k;
  }
}
