package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import java.io.Serializable;

public abstract class StreamLoader<T>
  implements Serializable
{
  protected int a = 10;
  
  public StreamLoader() {}
  
  public int a()
  {
    return a;
  }
  
  public void add(int paramInt)
  {
    a = paramInt;
  }
  
  public abstract String b();
  
  public abstract void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener);
}
