package com.deviantart.android.damobile.view.viewpageindicator;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.deviantart.android.damobile.util.DAFont;

public class DATextTabIndicator
  extends DATabIndicator
{
  public DATextTabIndicator(Context paramContext)
  {
    super(paramContext);
  }
  
  public DATextTabIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  private Typeface getTypeface()
  {
    return DAFont.o.get(getContext());
  }
  
  protected LinearLayout.LayoutParams b(int paramInt1, CharSequence paramCharSequence, int paramInt2)
  {
    return new LinearLayout.LayoutParams(-2, -1, 1.0F);
  }
  
  protected CustomizableTabPageIndicator.TextViewTab b()
  {
    CustomizableTabPageIndicator.TextViewTab localTextViewTab = super.b();
    localTextViewTab.setTypeface(getTypeface(), 0);
    return localTextViewTab;
  }
  
  protected int getSelectedColorResId()
  {
    return 17170443;
  }
  
  protected int getUnselectedColorResId()
  {
    return 2131558454;
  }
  
  protected int measureHeight(int paramInt1, int paramInt2)
  {
    return View.MeasureSpec.getSize(paramInt2);
  }
  
  public void setCurrentItem(int paramInt)
  {
    if (mViewPager == null) {
      throw new IllegalStateException("ViewPager has not been bound.");
    }
    mSelectedTabIndex = paramInt;
    mViewPager.setCurrentItem(paramInt);
    int j = mTabLayout.getChildCount();
    int i = 0;
    if (i < j)
    {
      CustomizableTabPageIndicator.TextViewTab localTextViewTab = (CustomizableTabPageIndicator.TextViewTab)mTabLayout.getChildAt(i);
      boolean bool;
      if (i == paramInt)
      {
        bool = true;
        label66:
        localTextViewTab.setSelected(bool);
        if (!bool) {
          break label112;
        }
        localTextViewTab.setTextColor(getResources().getColor(getSelectedColorResId()));
        animateToTab(paramInt);
      }
      for (;;)
      {
        i += 1;
        break;
        bool = false;
        break label66;
        label112:
        localTextViewTab.setTextColor(getResources().getColor(getUnselectedColorResId()));
      }
    }
  }
}
