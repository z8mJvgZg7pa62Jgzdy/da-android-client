package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTContextualizedGroupAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTCollection;

public class APIDeviationFavTabLoader
  extends StreamLoader<DVNTCollection>
{
  private boolean b = true;
  private String c;
  private APIDeviationFavTabLoader.MetadataReadyListener d = null;
  
  public APIDeviationFavTabLoader(APIDeviationFavTabLoader.MetadataReadyListener paramMetadataReadyListener, String paramString)
  {
    d = paramMetadataReadyListener;
    c = paramString;
  }
  
  public String b()
  {
    return "deviation_fave_tab_loader" + c;
  }
  
  public void f(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTAsyncAPI.buildGroupedRequest(paramContext).addCollectionFolders("folders", false, false, null, Integer.valueOf(paramInt), Integer.valueOf(a())).addDeviationMetadata("deviationmetadata", c, Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(true)).execute(new APIDeviationFavTabLoader.2(this, paramContext, paramStreamLoadListener));
  }
  
  public void goTo(boolean paramBoolean)
  {
    b = paramBoolean;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    if (b)
    {
      b = false;
      f(paramContext, paramInt, paramBoolean, paramStreamLoadListener);
      return;
    }
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.getCollectionFolders(null, false, false, Integer.valueOf(paramInt), Integer.valueOf(a));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APIDeviationFavTabLoader.1(this, paramStreamLoadListener));
  }
}
