package com.deviantart.android.damobile.util.submit.pages;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.util.submit.SubmitType;
import com.deviantart.android.damobile.view.RepostView;

public class SubmitStatusPage
  extends SubmitTextPage
  implements SubmitRepostPage
{
  RepostView j;
  @Bind({2131690017})
  FrameLayout repostedItem;
  
  public SubmitStatusPage(String paramString)
  {
    super(paramString);
  }
  
  public View a(ViewGroup paramViewGroup)
  {
    return b(paramViewGroup, null);
  }
  
  public View b(ViewGroup paramViewGroup, RepostView paramRepostView)
  {
    j = paramRepostView;
    paramViewGroup = super.a(paramViewGroup);
    categoryContainer.setVisibility(8);
    matureContentContainer.setVisibility(8);
    submitText.setHint(getContext().getString(2131231372));
    submitTextLabel.setVisibility(8);
    termsOfUse.setVisibility(8);
    if (paramRepostView != null)
    {
      submitText.setHint(getContext().getString(2131231371));
      repostedItem.addView(paramRepostView);
      repostedItem.setVisibility(0);
      return paramViewGroup;
    }
    repostedItem.setVisibility(8);
    return paramViewGroup;
  }
  
  public void b()
  {
    super.b();
    ButterKnife.unbind(this);
    j = null;
  }
  
  public SubmitType c()
  {
    return SubmitType.c;
  }
  
  public CharSequence getFormatted()
  {
    return "SubmitStatusTab";
  }
  
  public boolean isDigit()
  {
    return false;
  }
  
  protected int newHandle()
  {
    return 2130968755;
  }
  
  public boolean setContentHeight()
  {
    return true;
  }
}
