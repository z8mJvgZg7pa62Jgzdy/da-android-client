package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import android.widget.ViewFlipper;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation.DVNTDeviationBaseStats;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.SubmitActivity.IntentBuilder;
import com.deviantart.android.damobile.fragment.FullTorpedoFragment;
import com.deviantart.android.damobile.fragment.FullTorpedoFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.HomeBaseFragment.HomeFragmentInstanceBuilder;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APIMoreLikeThisLoader;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.MemberType;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.deviation.DeviationPanelTab;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.facebook.drawee.view.SimpleDraweeView;

public class DeviationBottomBarMenu
  extends FrameLayout
{
  @Bind({2131689708})
  SimpleDraweeView authorAvatar;
  @Bind({2131689711})
  TextView commentCount;
  @Bind({2131689712})
  TextView favCount;
  @Bind({2131689710})
  ViewFlipper flipper;
  private DeviationDescription i;
  @Bind({2131689709})
  TextView username;
  
  public DeviationBottomBarMenu(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public DeviationBottomBarMenu(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public DeviationBottomBarMenu(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    LayoutInflater.from(paramContext).inflate(2130968656, this, true);
    ButterKnife.bind(this);
  }
  
  public void onClickAvatar()
  {
    if (i != null)
    {
      if (DVNTContextUtils.isContextDead(getContext())) {
        return;
      }
      Object localObject = i.a().getAuthor();
      if (localObject != null)
      {
        String str = ((DVNTUser)localObject).getUserName();
        localObject = MemberType.toString(((DVNTUser)localObject).getType());
        if ((str != null) && (localObject != null)) {
          UserUtils.c((Activity)getContext(), str);
        }
      }
    }
  }
  
  public void onClickComments()
  {
    if (!DVNTContextUtils.isContextDead(getContext()))
    {
      if (i == null) {
        return;
      }
      NavigationUtils.b((Activity)getContext(), i, DeviationPanelTab.i);
    }
  }
  
  public void onClickFavs()
  {
    if (!DVNTContextUtils.isContextDead(getContext()))
    {
      if (i == null) {
        return;
      }
      TrackerUtil.a((Activity)getContext(), "tap_favourite_star");
      NavigationUtils.b((Activity)getContext(), i, DeviationPanelTab.b);
    }
  }
  
  public void onClickMlt()
  {
    String str = null;
    Activity localActivity = (Activity)getContext();
    if (i != null)
    {
      if (DVNTContextUtils.isContextDead(localActivity)) {
        return;
      }
      DVNTDeviation localDVNTDeviation = i.a();
      if (localDVNTDeviation.getAuthor() != null)
      {
        Stream localStream = StreamCacher.a(new APIMoreLikeThisLoader(localDVNTDeviation.getId(), null), StreamCacheStrategy.d);
        DVNTImage localDVNTImage2 = DeviationUtils.a(localDVNTDeviation);
        DVNTImage localDVNTImage1 = localDVNTImage2;
        if (localDVNTImage2 == null) {
          localDVNTImage1 = DeviationUtils.b(localDVNTDeviation);
        }
        if (localDVNTImage1 != null) {
          str = localDVNTImage1.getSrc();
        }
        ScreenFlowManager.a(localActivity, (FullTorpedoFragment)new FullTorpedoFragment.InstanceBuilder().a(localStream).a(getContext().getString(2131231090)).putShort(localDVNTDeviation.getTitle()).putInt(localDVNTDeviation.getAuthor().getUserName()).c(str).a(), "fullmlt" + localDVNTDeviation.getId());
      }
    }
  }
  
  public void onClickNext()
  {
    if (flipper == null) {
      return;
    }
    flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), 2131034136));
    flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), 2131034135));
    flipper.showNext();
  }
  
  public void onClickPrev()
  {
    if (flipper == null) {
      return;
    }
    flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), 2131034137));
    flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), 2131034134));
    flipper.showPrevious();
  }
  
  public void onClickShare()
  {
    if (!DVNTContextUtils.isContextDead(getContext()))
    {
      if (i == null) {
        return;
      }
      TrackerUtil.a((Activity)getContext(), "tap_share");
      Intent localIntent = new SubmitActivity.IntentBuilder().a(i.a()).a(getContext());
      ((Activity)getContext()).startActivityForResult(localIntent, 108);
    }
  }
  
  public void onClickUsername()
  {
    onClickAvatar();
  }
  
  public void onCreateView(DeviationDescription paramDeviationDescription)
  {
    i = paramDeviationDescription;
    paramDeviationDescription = paramDeviationDescription.a();
    if (paramDeviationDescription.getStats() != null)
    {
      commentCount.setText(DAFormatUtils.format(paramDeviationDescription.getStats().getComments()));
      favCount.setText(DAFormatUtils.format(paramDeviationDescription.getStats().getFavourites()));
    }
    if (paramDeviationDescription.getAuthor() != null)
    {
      username.setText(paramDeviationDescription.getAuthor().getUserName());
      ImageUtils.isEmpty(authorAvatar, Uri.parse(paramDeviationDescription.getAuthor().getUserIconURL()));
    }
  }
}
