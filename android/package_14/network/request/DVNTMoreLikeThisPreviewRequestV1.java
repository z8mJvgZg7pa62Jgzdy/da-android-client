package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.sdk.api.network.request.GroupableRequest;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.HashMap;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTMoreLikeThisPreviewRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTMoreLikeThisPreviewResults>
  implements GroupableRequest
{
  private final String deviationId;
  
  public DVNTMoreLikeThisPreviewRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTMoreLikeThisPreviewResults.class);
    deviationId = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTMoreLikeThisPreviewRequestV1 localDVNTMoreLikeThisPreviewRequestV1 = (DVNTMoreLikeThisPreviewRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(deviationId, deviationId).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "morelikethis" + deviationId;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 21600000L;
  }
  
  public HashMap getGETArguments()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("seed", deviationId);
    return localHashMap;
  }
  
  public String getGroupEndpoint()
  {
    return "/api/v1/oauth2/browse/morelikethis/preview";
  }
  
  public HashMap getPOSTArguments()
  {
    return null;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(deviationId).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTMoreLikeThisPreviewResults sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).moreLikeThisPreview(paramString, deviationId);
  }
}
