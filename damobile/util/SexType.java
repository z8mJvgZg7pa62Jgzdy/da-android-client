package com.deviantart.android.damobile.util;

public enum SexType
{
  private int descr;
  private String name;
  private String val;
  
  private SexType(String paramString1, String paramString2, int paramInt)
  {
    val = paramString1;
    name = paramString2;
    descr = paramInt;
  }
  
  public static SexType get(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    SexType[] arrayOfSexType = values();
    int j = arrayOfSexType.length;
    int i = 0;
    while (i < j)
    {
      SexType localSexType = arrayOfSexType[i];
      if (paramString.equalsIgnoreCase(name)) {
        return localSexType;
      }
      i += 1;
    }
    return null;
  }
  
  public int getTitle()
  {
    return descr;
  }
  
  public String getValue()
  {
    return val;
  }
}
