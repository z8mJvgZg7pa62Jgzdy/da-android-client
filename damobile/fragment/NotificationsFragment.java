package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import com.cocosw.undobar.UndoBarController.UndoBar;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.HomeActivity.BottomBarButton;
import com.deviantart.android.damobile.adapter.NotificationsPagerAdapter;
import com.deviantart.android.damobile.adapter.StatefulPagerAdapter;
import com.deviantart.android.damobile.util.BusStation.OnMCItemChangeEventBase;
import com.deviantart.android.damobile.util.BusStation.OnNotificationItemChangeEvent;
import com.deviantart.android.damobile.util.notifications.NotificationItemData;
import com.deviantart.android.damobile.util.notifications.NotificationItemType;
import com.deviantart.android.damobile.util.notifications.NotificationsPage;
import com.deviantart.android.damobile.util.opt.ItemDeleteHelperBase;
import com.deviantart.android.damobile.util.opt.ItemDeleteType;
import com.deviantart.android.damobile.util.opt.ItemDeleteUtils;
import com.deviantart.android.damobile.util.opt.NotificationItemDeleteHelper;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.util.tracking.pacaya.MobileLava;
import com.deviantart.android.damobile.util.tracking.pacaya.UxTopicEventCreator;
import com.deviantart.android.damobile.view.notifications.NotificationsListFrame;
import com.deviantart.pacaya.Pacaya;
import java.util.HashSet;
import java.util.Set;

public class NotificationsFragment
  extends MCBaseFragment
{
  public static HashSet<String> a = new HashSet();
  
  public NotificationsFragment() {}
  
  public static NotificationsFragment getNextTimePosition()
  {
    return new NotificationsFragment();
  }
  
  public void b(BusStation.OnNotificationItemChangeEvent paramOnNotificationItemChangeEvent)
  {
    NotificationItemData localNotificationItemData = (NotificationItemData)paramOnNotificationItemChangeEvent.c();
    boolean bool = paramOnNotificationItemChangeEvent.b();
    b(localNotificationItemData, NotificationsPage.i, bool);
    b(localNotificationItemData, localNotificationItemData.b().getString(), bool);
  }
  
  public void b(NotificationItemData paramNotificationItemData, NotificationsPage paramNotificationsPage, boolean paramBoolean)
  {
    NotificationsListFrame localNotificationsListFrame = (NotificationsListFrame)pager.findViewWithTag(paramNotificationsPage.getString());
    NotificationItemDeleteHelper localNotificationItemDeleteHelper = (NotificationItemDeleteHelper)ItemDeleteUtils.a(ItemDeleteType.b);
    if (localNotificationsListFrame == null)
    {
      if (paramBoolean)
      {
        localNotificationItemDeleteHelper.a(paramNotificationItemData, paramNotificationsPage);
        return;
      }
      localNotificationItemDeleteHelper.get(paramNotificationItemData, paramNotificationsPage);
      return;
    }
    localNotificationItemDeleteHelper.a(localNotificationsListFrame, paramNotificationItemData, paramBoolean);
  }
  
  protected HomeActivity.BottomBarButton c()
  {
    return HomeActivity.BottomBarButton.g;
  }
  
  protected void c(int paramInt)
  {
    if (DVNTContextUtils.isContextDead(getActivity())) {
      return;
    }
    String str = NotificationsPage.c[paramInt].get(getActivity()).toLowerCase();
    TrackerUtil.get(getActivity(), EventKeys.Category.i, "View", str);
    MobileLava.e().setText(new UxTopicEventCreator().get("notifications").getString(str).b());
  }
  
  Set get()
  {
    return a;
  }
  
  protected StatefulPagerAdapter getParentFile(int paramInt)
  {
    return new NotificationsPagerAdapter(paramInt);
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (DVNTAbstractAsyncAPI.isUserSession(getActivity())) {
      a(paramMenu, paramMenuInflater);
    }
  }
  
  public void onPause()
  {
    new UndoBarController.UndoBar(getActivity()).clear();
    super.onPause();
  }
  
  protected int setupButtons()
  {
    return 2130968712;
  }
}
