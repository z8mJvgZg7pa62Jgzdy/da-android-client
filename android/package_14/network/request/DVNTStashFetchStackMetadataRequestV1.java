package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTStashFetchStackMetadataRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTStashStackMetadata>
{
  private Long stackId;
  
  public DVNTStashFetchStackMetadataRequestV1(Long paramLong)
  {
    super(com.deviantart.android.sdk.api.model.DVNTStashStackMetadata.class);
    stackId = paramLong;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()) && (super.equals(paramObject)))
    {
      paramObject = (DVNTStashFetchStackMetadataRequestV1)paramObject;
      return Objects.append(stackId, stackId);
    }
    return false;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), stackId });
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTStashStackMetadata sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).stashFetchStackMetadata(paramString, stackId);
  }
}
