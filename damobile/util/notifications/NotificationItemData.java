package com.deviantart.android.damobile.util.notifications;

import com.deviantart.android.android.package_14.model.DVNTFeedbackMessage;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessageStack;
import com.deviantart.android.damobile.util.tracking.NotificationTrackingUtil;
import com.google.common.base.Objects;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NotificationItemData
{
  private String b;
  private NotificationItemType j;
  private DVNTFeedbackMessage l;
  private DVNTFeedbackMessageStack s;
  
  public NotificationItemData(NotificationItemType paramNotificationItemType, DVNTFeedbackMessage paramDVNTFeedbackMessage, DVNTFeedbackMessageStack paramDVNTFeedbackMessageStack)
  {
    j = paramNotificationItemType;
    l = paramDVNTFeedbackMessage;
    s = ((DVNTFeedbackMessageStack)paramDVNTFeedbackMessageStack);
    if (paramDVNTFeedbackMessageStack != null) {
      if (paramNotificationItemType != null) {
        break label43;
      }
    }
    label43:
    for (paramNotificationItemType = null;; paramNotificationItemType = NotificationTrackingUtil.getString(paramNotificationItemType, paramDVNTFeedbackMessageStack))
    {
      b = paramNotificationItemType;
      return;
      paramDVNTFeedbackMessageStack = paramDVNTFeedbackMessage;
      break;
    }
  }
  
  private static NotificationItemData a(DVNTFeedbackMessage paramDVNTFeedbackMessage)
  {
    return new NotificationItemData(NotificationItemType.findItem(paramDVNTFeedbackMessage), paramDVNTFeedbackMessage, null);
  }
  
  public static ArrayList a(List paramList)
  {
    ArrayList localArrayList = new ArrayList();
    if ((paramList == null) || (paramList.isEmpty())) {
      return localArrayList;
    }
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      localArrayList.add(getDescriptor((DVNTFeedbackMessageStack)paramList.next()));
    }
    return localArrayList;
  }
  
  private static NotificationItemData getDescriptor(DVNTFeedbackMessageStack paramDVNTFeedbackMessageStack)
  {
    return new NotificationItemData(NotificationItemType.a(paramDVNTFeedbackMessageStack), null, paramDVNTFeedbackMessageStack);
  }
  
  public static ArrayList write(List paramList)
  {
    ArrayList localArrayList = new ArrayList();
    if ((paramList == null) || (paramList.isEmpty())) {
      return localArrayList;
    }
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      localArrayList.add(a((DVNTFeedbackMessage)paramList.next()));
    }
    return localArrayList;
  }
  
  public DVNTFeedbackMessage a()
  {
    if (l != null) {
      return l;
    }
    return s;
  }
  
  public void a(NotificationItemType paramNotificationItemType)
  {
    j = paramNotificationItemType;
  }
  
  public NotificationItemType b()
  {
    return j;
  }
  
  public String c()
  {
    return b;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()))
    {
      paramObject = (NotificationItemData)paramObject;
      if (Objects.append(j, j))
      {
        if ((get()) && (getString().getStackId() != null) && (paramObject.get()) && (paramObject.getString().getStackId() != null)) {
          return Objects.append(getString().getStackId(), paramObject.getString().getStackId());
        }
        return Objects.append(a().getMessageId(), paramObject.a().getMessageId());
      }
    }
    return false;
  }
  
  public boolean get()
  {
    return s != null;
  }
  
  public DVNTFeedbackMessageStack getString()
  {
    if (!get()) {
      throw new IllegalStateException("Expecting stack");
    }
    return s;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { j, a().getMessageId() });
  }
}
