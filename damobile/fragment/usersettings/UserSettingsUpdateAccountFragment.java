package com.deviantart.android.damobile.fragment.usersettings;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTPushNotificationsSettings;
import com.deviantart.android.android.package_14.model.DVNTPushNotificationsSettings.DVNTSendPushNotificationsSettings;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.UserSettingsActivity;
import com.deviantart.android.damobile.util.LandingPageType;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.util.SharedPreferenceUtil;
import com.deviantart.android.damobile.util.UserUtils;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableBiMap.Builder;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.util.Map;

public class UserSettingsUpdateAccountFragment
  extends UserSettingsBaseFragment
{
  private static ImmutableBiMap<com.deviantart.android.sdk.api.model.DVNTPushNotificationsSettings.Favourite, String> forward;
  private static ImmutableBiMap<String, com.deviantart.android.sdk.api.model.DVNTPushNotificationsSettings.Favourite> fragmentManager;
  @Bind({2131689853})
  TextView landingScreenValue;
  DVNTPushNotificationsSettings mLastPath;
  private String[] path;
  @Bind({2131689857})
  SwitchCompat pnCommentsRepliesSwitch;
  @Bind({2131689863})
  TextView pnFavouriteValue;
  @Bind({2131689861})
  SwitchCompat pnMentionsSwitch;
  @Bind({2131689855})
  SwitchCompat pnNotesSwitch;
  @Bind({2131689859})
  SwitchCompat pnWatchersSwitch;
  
  public UserSettingsUpdateAccountFragment() {}
  
  private void UpdateList()
  {
    DVNTAsyncAPI.getPushNotificationsSettings().call(getActivity(), new UserSettingsUpdateAccountFragment.2(this));
  }
  
  private LandingPageType get()
  {
    String str = SharedPreferenceUtil.getString(getActivity(), "recent_username", null);
    if (str != null) {}
    for (int i = getActivity().getSharedPreferences(str, 0).getInt("landing_screen", -1);; i = -1)
    {
      int j = i;
      if (i == -1) {
        j = SharedPreferenceUtil.getKey(getActivity(), "landing_screen", 1);
      }
      return LandingPageType.get(j);
    }
  }
  
  private void goHome()
  {
    if ((mLastPath != null) && (mLastPath.getSendSettings() != null))
    {
      if (DVNTContextUtils.isContextDead(getActivity())) {
        return;
      }
      DVNTAsyncAPI.updatePushNotificationsSettings(mLastPath).call(getActivity(), new UserSettingsUpdateAccountFragment.1(this));
    }
  }
  
  private void onSelection(CharSequence[] paramArrayOfCharSequence, int paramInt)
  {
    if (DVNTContextUtils.isContextDead(getActivity())) {
      return;
    }
    landingScreenValue.setText(paramArrayOfCharSequence[paramInt]);
    LandingPageType localLandingPageType = LandingPageType.findByName(getActivity(), landingScreenValue.getText().toString());
    if (localLandingPageType != null)
    {
      String str = SharedPreferenceUtil.getString(getActivity(), "recent_username", null);
      paramArrayOfCharSequence = str;
      if (str == null)
      {
        paramArrayOfCharSequence = UserUtils.a;
        SharedPreferenceUtil.putString(getActivity(), "recent_username", paramArrayOfCharSequence);
      }
      getActivity().getSharedPreferences(paramArrayOfCharSequence, 0).edit().putInt("landing_screen", localLandingPageType.ordinal()).apply();
    }
  }
  
  private void showMessageDetails()
  {
    LandingPageType localLandingPageType = get();
    landingScreenValue.setText(getString(localLandingPageType.getTitle()));
  }
  
  private void updateEditText()
  {
    DVNTPushNotificationsSettings.DVNTSendPushNotificationsSettings localDVNTSendPushNotificationsSettings = mLastPath.getSendSettings();
    pnNotesSwitch.setChecked(localDVNTSendPushNotificationsSettings.getNote().booleanValue());
    pnMentionsSwitch.setChecked(localDVNTSendPushNotificationsSettings.getMention().booleanValue());
    pnCommentsRepliesSwitch.setChecked(localDVNTSendPushNotificationsSettings.getCommentReply().booleanValue());
    pnWatchersSwitch.setChecked(localDVNTSendPushNotificationsSettings.getWatcher().booleanValue());
    pnFavouriteValue.setText((CharSequence)forward.get(localDVNTSendPushNotificationsSettings.getFavourite()));
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setRetainInstance(true);
    paramBundle = getString(2131231302);
    String str1 = getString(2131231300);
    String str2 = getString(2131231301);
    forward = new ImmutableBiMap.Builder().get(com.deviantart.android.android.package_14.model.DVNTPushNotificationsSettings.Favourite.NONE, paramBundle).get(com.deviantart.android.android.package_14.model.DVNTPushNotificationsSettings.Favourite.EVERY, str1).get(com.deviantart.android.android.package_14.model.DVNTPushNotificationsSettings.Favourite.MILESTONES, str2).get();
    fragmentManager = forward.inverse();
    path = ((String[])forward.rowKeySet().toArray(new String[forward.size()]));
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = (ScrollView)paramLayoutInflater.inflate(2130968691, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    showMessageDetails();
    UpdateList();
    return paramLayoutInflater;
  }
  
  public void onHelpClick()
  {
    NavigationUtils.openBrowser(getActivity(), getString(2131231279));
  }
  
  public void onLandingScreenClick()
  {
    CharSequence[] arrayOfCharSequence = LandingPageType.getTitles(getActivity());
    int i = get().ordinal();
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
    localBuilder.setTitle(2131231282).setSingleChoiceItems(arrayOfCharSequence, i, UserSettingsUpdateAccountFragment..Lambda.1.doIt(this, arrayOfCharSequence)).setNegativeButton(2131230820, UserSettingsUpdateAccountFragment..Lambda.2.access$getNo());
    localBuilder.create().show();
  }
  
  public void onNoticesClick()
  {
    onPostExecute(new UserSettingsNoticesFragment(), "view_notices");
  }
  
  public void onPNCommentsRepliesSwitchClick(boolean paramBoolean)
  {
    if (mLastPath != null)
    {
      if (mLastPath.getSendSettings() == null) {
        return;
      }
      mLastPath.getSendSettings().setCommentReply(Boolean.valueOf(paramBoolean));
    }
  }
  
  public void onPNFavouritesClick()
  {
    if ((mLastPath != null) && (mLastPath.getSendSettings() != null))
    {
      if (mLastPath.getSendSettings().getFavourite() == null) {
        return;
      }
      DVNTPushNotificationsSettings.DVNTSendPushNotificationsSettings localDVNTSendPushNotificationsSettings = mLastPath.getSendSettings();
      ImmutableList localImmutableList = fragmentManager.rowKeySet().asList();
      int i = localImmutableList.indexOf(localDVNTSendPushNotificationsSettings.getFavourite());
      new AlertDialog.Builder(getActivity()).setSingleChoiceItems(path, i, UserSettingsUpdateAccountFragment..Lambda.3.calculate(this, localImmutableList, localDVNTSendPushNotificationsSettings)).setNegativeButton(2131230820, UserSettingsUpdateAccountFragment..Lambda.4.access$getNo()).show();
    }
  }
  
  public void onPNMentionsSwitchClick(boolean paramBoolean)
  {
    if (mLastPath != null)
    {
      if (mLastPath.getSendSettings() == null) {
        return;
      }
      mLastPath.getSendSettings().setMention(Boolean.valueOf(paramBoolean));
    }
  }
  
  public void onPNNotesSwitchClick(boolean paramBoolean)
  {
    if (mLastPath != null)
    {
      if (mLastPath.getSendSettings() == null) {
        return;
      }
      mLastPath.getSendSettings().setNote(Boolean.valueOf(paramBoolean));
    }
  }
  
  public void onPNWatchersSwitchClick(boolean paramBoolean)
  {
    if (mLastPath != null)
    {
      if (mLastPath.getSendSettings() == null) {
        return;
      }
      mLastPath.getSendSettings().setWatcher(Boolean.valueOf(paramBoolean));
    }
  }
  
  public void onPause()
  {
    super.onPause();
    goHome();
  }
  
  public void onPrivacyClick()
  {
    NavigationUtils.openBrowser(getActivity(), getString(2131231307));
  }
  
  public void onReportBugClick()
  {
    Uri localUri2 = Uri.parse(getString(2131231313));
    Uri localUri1 = localUri2;
    if (UserUtils.a != null) {
      localUri1 = localUri2.buildUpon().appendQueryParameter("realname", UserUtils.a).build();
    }
    NavigationUtils.navigate(getActivity(), localUri1);
  }
  
  public void onResume()
  {
    super.onResume();
    ((UserSettingsActivity)getActivity()).onProgressChanged(2131231256);
  }
  
  public void onTermsClick()
  {
    NavigationUtils.openBrowser(getActivity(), getString(2131231319));
  }
}
