package com.deviantart.android.damobile.view.image;

import android.view.MotionEvent;

public class MultiPointerGestureDetector
{
  private final float[] a = new float[2];
  private final float[] b = new float[2];
  private final float[] c = new float[2];
  private final float[] d = new float[2];
  private final int[] e = new int[2];
  private MultiPointerGestureDetector.Listener j = null;
  private boolean k;
  private int v;
  
  public MultiPointerGestureDetector()
  {
    b();
  }
  
  private int a(MotionEvent paramMotionEvent, int paramInt)
  {
    int i = paramMotionEvent.getPointerCount();
    int m = paramMotionEvent.getActionMasked();
    int n = paramMotionEvent.getActionIndex();
    if (((m == 1) || (m == 6)) && (paramInt >= n)) {
      paramInt += 1;
    }
    for (;;)
    {
      if (paramInt < i) {
        return paramInt;
      }
      return -1;
    }
  }
  
  private void i()
  {
    if (k)
    {
      k = false;
      if (j != null) {
        j.c(this);
      }
    }
  }
  
  public static MultiPointerGestureDetector scalb()
  {
    return new MultiPointerGestureDetector();
  }
  
  private void visitMaxs()
  {
    if (!k)
    {
      k = true;
      if (j != null) {
        j.b(this);
      }
    }
  }
  
  public void a()
  {
    if (!k) {
      return;
    }
    i();
    int i = 0;
    while (i < 2)
    {
      a[i] = d[i];
      b[i] = c[i];
      i += 1;
    }
    visitMaxs();
  }
  
  public boolean a(MotionEvent paramMotionEvent)
  {
    int m = 0;
    int i = 0;
    switch (paramMotionEvent.getActionMasked())
    {
    default: 
      break;
    }
    for (;;)
    {
      return true;
      while (i < 2)
      {
        m = paramMotionEvent.findPointerIndex(e[i]);
        if (m != -1)
        {
          d[i] = paramMotionEvent.getX(m);
          c[i] = paramMotionEvent.getY(m);
        }
        i += 1;
      }
      if ((!k) && (c())) {
        visitMaxs();
      }
      if ((k) && (j != null))
      {
        j.a(this);
        continue;
        boolean bool = k;
        i();
        b();
        i = m;
        for (;;)
        {
          if (i < 2)
          {
            m = a(paramMotionEvent, i);
            if (m != -1) {}
          }
          else
          {
            if ((!bool) || (v <= 0)) {
              break;
            }
            visitMaxs();
            break;
          }
          e[i] = paramMotionEvent.getPointerId(m);
          float[] arrayOfFloat1 = d;
          float[] arrayOfFloat2 = a;
          float f = paramMotionEvent.getX(m);
          arrayOfFloat2[i] = f;
          arrayOfFloat1[i] = f;
          arrayOfFloat1 = c;
          arrayOfFloat2 = b;
          f = paramMotionEvent.getY(m);
          arrayOfFloat2[i] = f;
          arrayOfFloat1[i] = f;
          v += 1;
          i += 1;
        }
        i();
        b();
      }
    }
  }
  
  public float[] add()
  {
    return a;
  }
  
  public void b()
  {
    int i = 0;
    k = false;
    v = 0;
    while (i < 2)
    {
      e[i] = -1;
      i += 1;
    }
  }
  
  public void b(MultiPointerGestureDetector.Listener paramListener)
  {
    j = paramListener;
  }
  
  protected boolean c()
  {
    return true;
  }
  
  public boolean d()
  {
    return k;
  }
  
  public int get()
  {
    return v;
  }
  
  public float[] getValue()
  {
    return b;
  }
  
  public float[] removeFirst()
  {
    return d;
  }
  
  public float[] size()
  {
    return c;
  }
}
