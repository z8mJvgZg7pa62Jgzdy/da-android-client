package com.deviantart.android.damobile.util.tracking;

import android.content.Context;
import com.deviantart.android.damobile.util.DeviceIdUtil;
import com.deviantart.datoolkit.pixeltracker.DVNTPixelTracker;
import com.deviantart.datoolkit.pixeltracker.DVNTPixelTracker.Builder;

public class InternalPixelTracker
{
  private static DVNTPixelTracker m;
  
  public InternalPixelTracker() {}
  
  public static DVNTPixelTracker b()
  {
    return m;
  }
  
  public static void b(Context paramContext)
  {
    m = new DVNTPixelTracker.Builder("daandroid", DeviceIdUtil.getDeviceId(paramContext)).a("m.gif").b("DeviantArt-Android/1.12.3").a();
  }
}
