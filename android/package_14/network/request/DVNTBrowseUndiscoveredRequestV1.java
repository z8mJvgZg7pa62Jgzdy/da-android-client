package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTBrowseUndiscoveredRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse>
{
  final String categoryPath;
  final Integer limit;
  final Integer offset;
  
  public DVNTBrowseUndiscoveredRequestV1(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse.class);
    categoryPath = paramString;
    offset = paramInteger1;
    limit = paramInteger2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTBrowseUndiscoveredRequestV1 localDVNTBrowseUndiscoveredRequestV1 = (DVNTBrowseUndiscoveredRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(categoryPath, categoryPath).append(offset, offset).append(limit, limit).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    StringBuilder localStringBuilder = new StringBuilder().append("browse_undiscovered");
    if (categoryPath != null) {}
    for (String str = categoryPath.replace("/", "");; str = "") {
      return str + offset + limit;
    }
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(categoryPath).append(offset).append(limit).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTPaginatedResultResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).browseUndiscovered(paramString, categoryPath, offset, limit);
  }
}
