package com.deviantart.android.damobile.util.opt;

import android.app.Activity;
import android.content.Context;
import com.cocosw.undobar.UndoBarController;
import com.cocosw.undobar.UndoBarController.AdvancedUndoListener;
import com.cocosw.undobar.UndoBarController.UndoBar;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.view.opt.MCListAdapterBase;
import com.deviantart.android.damobile.view.opt.MCListFrameBase;
import com.deviantart.android.damobile.view.opt.MCListRecyclerView;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class ItemDeleteHelperBase<ITEM_TYPE, PAGE_TYPE extends com.deviantart.android.damobile.util.mc.ItemPendable>
{
  private ITEM_TYPE a = null;
  private final ArrayList<ITEM_TYPE> c = new ArrayList();
  private final ArrayList<ITEM_TYPE> k = new ArrayList();
  private final HashMap<String, HashMap<ITEM_TYPE, Integer>> replacements = new HashMap();
  
  public ItemDeleteHelperBase() {}
  
  private UndoBarController.AdvancedUndoListener visitField(Activity paramActivity, Object paramObject)
  {
    return new ItemDeleteHelperBase.1(this, paramActivity, paramObject);
  }
  
  public void a(Context paramContext)
  {
    if (a != null)
    {
      if (DVNTContextUtils.isContextDead(paramContext)) {
        return;
      }
      c.add(a);
      f(paramContext, a);
      a = null;
    }
  }
  
  public void a(Context paramContext, Object paramObject)
  {
    if (!DVNTContextUtils.isContextDead(paramContext))
    {
      if (!(paramContext instanceof Activity)) {
        return;
      }
      Activity localActivity = (Activity)paramContext;
      int i = b(paramObject);
      f(localActivity, paramObject);
      new UndoBarController.UndoBar(localActivity).clear();
      new UndoBarController.UndoBar(localActivity).message(i).show(UndoBarController.UNDOSTYLE).duration(7000L).listener(visitField(localActivity, paramObject)).show();
      b(paramContext, paramObject);
    }
  }
  
  public void a(MCListFrameBase paramMCListFrameBase, Object paramObject, boolean paramBoolean)
  {
    MCListAdapterBase localMCListAdapterBase = paramMCListFrameBase.getRecyclerView().getAdapter();
    if (paramBoolean) {
      localMCListAdapterBase.b(paramObject);
    }
    while (localMCListAdapterBase.e() == 0)
    {
      paramMCListFrameBase.b();
      return;
      localMCListAdapterBase.a(paramObject);
    }
    if ((paramBoolean) && (localMCListAdapterBase.e() > 0)) {
      paramMCListFrameBase.c();
    }
  }
  
  public void a(Object paramObject, ItemPendable paramItemPendable)
  {
    Stream localStream = b(paramItemPendable);
    if (localStream == null) {
      return;
    }
    paramItemPendable = (HashMap)get().get(paramItemPendable.getString());
    if (paramItemPendable != null)
    {
      paramItemPendable = (Integer)paramItemPendable.remove(paramObject);
      if (paramItemPendable != null) {
        localStream.a(Integer.valueOf(Math.max(0, Math.min(localStream.size() - 1, paramItemPendable.intValue()))).intValue(), paramObject);
      }
    }
  }
  
  abstract int b(Object paramObject);
  
  public abstract Stream b(ItemPendable paramItemPendable);
  
  public void b()
  {
    if (a == null) {
      return;
    }
    f(a);
    a = null;
  }
  
  abstract void b(Activity paramActivity, Object paramObject);
  
  public void b(Context paramContext, Object paramObject)
  {
    a(paramContext);
    a = paramObject;
    deleteChannel(paramObject);
  }
  
  abstract void deleteChannel(Object paramObject);
  
  abstract void f(Activity paramActivity, Object paramObject);
  
  abstract void f(Context paramContext, Object paramObject);
  
  abstract void f(Object paramObject);
  
  public HashMap get()
  {
    return replacements;
  }
  
  public void get(Object paramObject, ItemPendable paramItemPendable)
  {
    Stream localStream = b(paramItemPendable);
    if (localStream == null) {
      return;
    }
    int i = localStream.get().indexOf(paramObject);
    if (i != -1)
    {
      paramItemPendable = (HashMap)get().get(paramItemPendable.getString());
      if (paramItemPendable != null)
      {
        paramItemPendable.put(paramObject, Integer.valueOf(i));
        localStream.close(i);
      }
    }
  }
  
  public ArrayList getName()
  {
    return c;
  }
  
  public Object getValue()
  {
    return a;
  }
  
  public ArrayList i()
  {
    return k;
  }
}
