package com.deviantart.android.damobile.drawable;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import com.deviantart.android.damobile.util.Graphics;

public class ProfileCardLeftBackground
  extends Drawable
{
  private Paint mFillPaint = new Paint();
  private Path path;
  
  private ProfileCardLeftBackground(int paramInt)
  {
    mFillPaint.setColor(paramInt);
    mFillPaint.setStyle(Paint.Style.FILL);
    mFillPaint.setAntiAlias(true);
  }
  
  public static ProfileCardLeftBackground a(Context paramContext)
  {
    return new ProfileCardLeftBackground(paramContext.getResources().getColor(2131558424));
  }
  
  public void draw(Canvas paramCanvas)
  {
    paramCanvas.drawPath(path, mFillPaint);
  }
  
  public int getOpacity()
  {
    return 1;
  }
  
  protected void onBoundsChange(Rect paramRect)
  {
    Point localPoint1 = new Point(0, 0);
    Point localPoint2 = new Point(0, paramRect.height());
    Point localPoint3 = new Point((int)(paramRect.width() * 0.45F), paramRect.height());
    path = Graphics.parsePath(new Point[] { localPoint1, localPoint2, localPoint3, new Point((int)(x + paramRect.height() / 1.8807265F), 0) });
  }
  
  public void setAlpha(int paramInt) {}
  
  public void setColorFilter(ColorFilter paramColorFilter) {}
}
