package com.deviantart.android.damobile.pushnotifications;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import com.deviantart.android.android.utils.DVNTContextUtils;

public class GcmBroadcastReceiver
  extends WakefulBroadcastReceiver
{
  public GcmBroadcastReceiver() {}
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    if (!DVNTContextUtils.isContextDead(paramContext))
    {
      if (paramIntent == null) {
        return;
      }
      Log.d("GcmReceiver", "received intent");
      WakefulBroadcastReceiver.startWakefulService(paramContext, paramIntent.setComponent(new ComponentName(paramContext.getPackageName(), GcmIntentService.class.getName())));
      setResultCode(-1);
    }
  }
}
