package com.deviantart.android.damobile.util.opt;

public abstract interface ItemPendable
{
  public abstract String getString();
}
