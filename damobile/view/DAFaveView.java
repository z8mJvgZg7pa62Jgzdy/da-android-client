package com.deviantart.android.damobile.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;

public class DAFaveView
  extends FrameLayout
{
  public DAFaveView(Context paramContext)
  {
    super(paramContext);
    inflate(paramContext);
  }
  
  public DAFaveView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    inflate(paramContext);
  }
  
  public DAFaveView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    inflate(paramContext);
  }
  
  public void inflate(Context paramContext)
  {
    addView(LayoutInflater.from(paramContext).inflate(2130968624, this, false), getChildCount());
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    findViewById(2131689641).bringToFront();
  }
  
  public void show(boolean paramBoolean1, boolean paramBoolean2)
  {
    View localView = findViewById(2131689642);
    if (paramBoolean1)
    {
      localView.setVisibility(0);
      if (paramBoolean2)
      {
        localView.setAlpha(0.0F);
        localView.animate().alpha(1.0F).setDuration(300L).setListener(new DAFaveView.1(this));
      }
    }
    else
    {
      if (paramBoolean2)
      {
        localView.setVisibility(0);
        localView.setAlpha(1.0F);
        localView.animate().alpha(0.0F).setDuration(300L).setListener(new DAFaveView.2(this, localView));
        return;
      }
      localView.setVisibility(8);
    }
  }
}
