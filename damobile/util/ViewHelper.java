package com.deviantart.android.damobile.util;

import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.TextView;

public class ViewHelper
{
  public ViewHelper() {}
  
  public static void a(View paramView, int paramInt)
  {
    paramView.setPadding(paramView.getLeft(), paramInt, paramView.getPaddingRight(), paramView.getPaddingBottom());
  }
  
  public static void a(View paramView, Drawable paramDrawable)
  {
    if (Build.VERSION.SDK_INT < 16)
    {
      paramView.setBackgroundDrawable(paramDrawable);
      return;
    }
    paramView.setBackground(paramDrawable);
  }
  
  public static void append(TextView paramTextView, CharSequence paramCharSequence)
  {
    setValue(paramTextView, paramCharSequence, 4, null);
  }
  
  public static void format(View paramView)
  {
    init(paramView, false);
  }
  
  public static void init(View paramView, boolean paramBoolean)
  {
    if ((Build.VERSION.SDK_INT < 21) || (paramBoolean)) {
      paramView.setLayerType(1, null);
    }
  }
  
  public static void removeOnGlobalLayoutListener(ViewTreeObserver paramViewTreeObserver, ViewTreeObserver.OnGlobalLayoutListener paramOnGlobalLayoutListener)
  {
    if (Build.VERSION.SDK_INT < 16)
    {
      paramViewTreeObserver.removeGlobalOnLayoutListener(paramOnGlobalLayoutListener);
      return;
    }
    paramViewTreeObserver.removeOnGlobalLayoutListener(paramOnGlobalLayoutListener);
  }
  
  public static void setText(TextView paramTextView, CharSequence paramCharSequence)
  {
    setValue(paramTextView, paramCharSequence, 8, null);
  }
  
  public static void setValue(TextView paramTextView, CharSequence paramCharSequence, int paramInt, Integer paramInteger)
  {
    if (TextUtils.isEmpty(paramCharSequence)) {
      paramTextView.setVisibility(paramInt);
    }
    for (;;)
    {
      paramTextView.setText(paramCharSequence);
      return;
      if (paramInteger != null) {
        paramTextView.setVisibility(paramInteger.intValue());
      }
    }
  }
  
  public static void show(View... paramVarArgs)
  {
    int j = paramVarArgs.length;
    int i = 0;
    while (i < j)
    {
      paramVarArgs[i].setSelected(false);
      i += 1;
    }
  }
}
