package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTBrowsePopularRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse>
{
  final String categoryPath;
  final Integer limit;
  final Integer offset;
  final Boolean prints;
  final String query;
  final String timeRange;
  
  public DVNTBrowsePopularRequestV1(String paramString1, String paramString2, String paramString3, Boolean paramBoolean, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse.class);
    categoryPath = paramString1;
    query = paramString2;
    timeRange = paramString3;
    prints = paramBoolean;
    offset = paramInteger1;
    limit = paramInteger2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTBrowsePopularRequestV1 localDVNTBrowsePopularRequestV1 = (DVNTBrowsePopularRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(categoryPath, categoryPath).append(query, query).append(timeRange, timeRange).append(prints, prints).append(offset, offset).append(limit, limit).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    StringBuilder localStringBuilder = new StringBuilder().append("browse_popular").append(query);
    if (categoryPath != null) {}
    for (String str = categoryPath.replace("/", "");; str = "") {
      return str + timeRange + prints + offset + limit;
    }
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(categoryPath).append(query).append(timeRange).append(prints).append(offset).append(limit).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTPaginatedResultResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).browsePopular(paramString, categoryPath, query, timeRange, prints, offset, limit);
  }
}
