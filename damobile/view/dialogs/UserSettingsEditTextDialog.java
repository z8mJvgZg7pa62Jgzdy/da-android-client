package com.deviantart.android.damobile.view.dialogs;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.os.BaseBundle;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;

public class UserSettingsEditTextDialog
  extends DABaseDialog
{
  @Bind({2131690111})
  EditText editText;
  
  public UserSettingsEditTextDialog() {}
  
  public static UserSettingsEditTextDialog a(String paramString1, String paramString2)
  {
    UserSettingsEditTextDialog localUserSettingsEditTextDialog = new UserSettingsEditTextDialog();
    Bundle localBundle = new Bundle();
    localBundle.putString("dialog_title", paramString1);
    localBundle.putString("dialog_hint", paramString2);
    localUserSettingsEditTextDialog.setArguments(localBundle);
    return localUserSettingsEditTextDialog;
  }
  
  public String getText()
  {
    return editText.getText().toString();
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new AlertDialog.Builder(getActivity());
    View localView = getActivity().getLayoutInflater().inflate(2130968794, null);
    ButterKnife.bind(this, localView);
    editText.setHint(getArguments().getString("dialog_hint"));
    paramBundle.setTitle(getArguments().getString("dialog_title")).setView(localView).setPositiveButton(2131231183, new UserSettingsEditTextDialog.2(this)).setNegativeButton(2131230820, new UserSettingsEditTextDialog.1(this));
    return paramBundle.create();
  }
}
