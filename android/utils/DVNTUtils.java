package com.deviantart.android.android.utils;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

public class DVNTUtils
{
  public DVNTUtils() {}
  
  /* Error */
  public static String generateSecureAndroidHash(android.content.Context paramContext)
  {
    // Byte code:
    //   0: new 15	java/lang/StringBuilder
    //   3: dup
    //   4: invokespecial 16	java/lang/StringBuilder:<init>	()V
    //   7: ldc 18
    //   9: invokevirtual 22	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   12: aload_0
    //   13: invokevirtual 28	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   16: ldc 30
    //   18: invokestatic 36	android/provider/Settings$Secure:getString	(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;
    //   21: invokevirtual 22	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   24: invokevirtual 40	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   27: invokestatic 44	com/deviantart/android/android/utils/DVNTUtils:sha1hex	(Ljava/lang/String;)Ljava/lang/String;
    //   30: astore_2
    //   31: aload_2
    //   32: astore_0
    //   33: aload_2
    //   34: invokevirtual 50	java/lang/String:length	()I
    //   37: istore_1
    //   38: iload_1
    //   39: bipush 32
    //   41: if_icmple +27 -> 68
    //   44: aload_2
    //   45: iconst_0
    //   46: bipush 32
    //   48: invokevirtual 54	java/lang/String:substring	(II)Ljava/lang/String;
    //   51: astore_2
    //   52: aload_2
    //   53: areturn
    //   54: astore_2
    //   55: ldc 18
    //   57: astore_0
    //   58: aload_2
    //   59: invokevirtual 57	java/lang/Exception:printStackTrace	()V
    //   62: aload_0
    //   63: areturn
    //   64: astore_2
    //   65: goto -7 -> 58
    //   68: aload_2
    //   69: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	70	0	paramContext	android.content.Context
    //   37	5	1	i	int
    //   30	23	2	str	String
    //   54	5	2	localException1	Exception
    //   64	5	2	localException2	Exception
    // Exception table:
    //   from	to	target	type
    //   0	31	54	java/lang/Exception
    //   33	38	64	java/lang/Exception
    //   44	52	64	java/lang/Exception
  }
  
  public static String sha1hex(String paramString)
  {
    return new String(Hex.encodeHex(DigestUtils.sha1(paramString)));
  }
}
