package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.CoordinatorLayout.LayoutParams;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AbsListView.LayoutParams;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTComment;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.HomeActivity;
import com.deviantart.android.damobile.activity.HomeActivity.BottomBarButton;
import com.deviantart.android.damobile.activity.UserSettingsActivity;
import com.deviantart.android.damobile.drawable.SlashBackgroundDrawable;
import com.deviantart.android.damobile.util.AppBarStateChangeListener;
import com.deviantart.android.damobile.util.AppBarStateChangeListener.AppBarStateChangeNotifier;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnCommentPostedEvent;
import com.deviantart.android.damobile.util.BusStation.OnLoadedFocusedComment;
import com.deviantart.android.damobile.util.BusStation.OnUserSettingChangeEvent;
import com.deviantart.android.damobile.util.CommentType;
import com.deviantart.android.damobile.util.CommentUtils.OnClickOpenCommentListener;
import com.deviantart.android.damobile.util.FloatingButtonDescription;
import com.deviantart.android.damobile.util.ProfileCardBehavior;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.ViewHelper;
import com.deviantart.android.damobile.util.deeplink.DeepLinkUtils;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.util.tracking.TrackerUtil.EventLabelBuilder;
import com.deviantart.android.damobile.util.tracking.pacaya.MobileLava;
import com.deviantart.android.damobile.util.tracking.pacaya.UxTopicEventCreator;
import com.deviantart.android.damobile.view.CommentsLayout;
import com.deviantart.android.damobile.view.VerificationBanner;
import com.deviantart.android.damobile.view.userprofile.UserProfileCardContainer;
import com.deviantart.android.damobile.view.viewpageindicator.CustomizableTabPageIndicator;
import com.deviantart.android.damobile.view.viewpageindicator.CustomizableTabPageIndicator.OnTabReselectedListener;
import com.deviantart.android.damobile.view.viewpageindicator.DATabIndicator;
import com.deviantart.android.damobile.view.viewpageindicator.DATabIndicator.OnTabClickListener;
import com.deviantart.android.damobile.view.viewpageindicator.UserProfileMenuIndicator;
import com.deviantart.datoolkit.logger.DVNTLog;
import com.deviantart.pacaya.Pacaya;
import com.google.android.com.appindexing.Action;
import com.google.android.com.appindexing.AppIndex;
import com.google.android.com.appindexing.AppIndexApi;
import com.squareup.otto.Bus;

public class UserProfileFragment
  extends HomeBaseFragment
  implements AppBarStateChangeListener.AppBarStateChangeNotifier, CustomizableTabPageIndicator.OnTabReselectedListener, DATabIndicator.OnTabClickListener
{
  private boolean a;
  private Boolean alive = null;
  @Bind({2131689750})
  AppBarLayout appBarLayout;
  private String b;
  private String c;
  private boolean d = false;
  @Bind({2131689848})
  UserProfileMenuIndicator indicator;
  private Action mReceiver;
  @Bind({2131689844})
  CoordinatorLayout mainPanel;
  @Bind({2131689849})
  ViewPager pager;
  View.OnClickListener q = new UserProfileFragment.1(this);
  @Bind({2131689850})
  View settingButton;
  @Bind({2131689847})
  TextView toolbarTitle;
  @Bind({2131689846})
  UserProfileCardContainer userProfileCard;
  @Bind({2131689609})
  VerificationBanner verificationBanner;
  
  public UserProfileFragment() {}
  
  private void b(int paramInt)
  {
    if (DVNTContextUtils.isContextDead(getActivity())) {
      return;
    }
    UserProfileFragment.UserProfileTab localUserProfileTab = UserProfileFragment.UserProfileTab.get(paramInt);
    String str = new TrackerUtil.EventLabelBuilder().get("tab", localUserProfileTab.getValue()).getValue();
    TrackerUtil.get(getActivity(), EventKeys.Category.c, "tab_viewed", str);
    MobileLava.e().setText(new UxTopicEventCreator().get("profile").getString(localUserProfileTab.getValue()).b());
  }
  
  private void e()
  {
    if (b == null) {
      b = "";
    }
    DVNTCommonAsyncAPI.userProfile(b, true, true).call(getActivity(), new UserProfileFragment.3(this));
  }
  
  private void from(boolean paramBoolean)
  {
    if (appBarLayout == null) {
      return;
    }
    Object localObject = (CoordinatorLayout.LayoutParams)appBarLayout.getLayoutParams();
    try
    {
      localObject = ((CoordinatorLayout.LayoutParams)localObject).getBehavior();
      localObject = (ProfileCardBehavior)localObject;
      if (paramBoolean)
      {
        localCoordinatorLayout = mainPanel;
        localAppBarLayout = appBarLayout;
        ((ProfileCardBehavior)localObject).scroll(localCoordinatorLayout, localAppBarLayout, -5000.0F);
        return;
      }
    }
    catch (Exception localException)
    {
      appBarLayout.setExpanded(paramBoolean);
      return;
    }
    CoordinatorLayout localCoordinatorLayout = mainPanel;
    AppBarLayout localAppBarLayout = appBarLayout;
    localException.scroll(localCoordinatorLayout, localAppBarLayout, 5000.0F);
  }
  
  private void goTo(boolean paramBoolean)
  {
    if (!DVNTContextUtils.isContextDead(getActivity()))
    {
      if (!isResumed()) {
        return;
      }
      userProfileCard.setEnabled(paramBoolean);
      d = false;
      if (a) {
        settingButton.setVisibility(0);
      }
      toolbarTitle.setVisibility(8);
      if (DVNTAbstractAsyncAPI.isUserSession(getActivity())) {
        ((HomeActivity)getActivity()).show(true);
      }
    }
  }
  
  public static View init(Context paramContext)
  {
    FrameLayout localFrameLayout = new FrameLayout(paramContext);
    localFrameLayout.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
    View localView = new View(paramContext);
    FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-1, -2);
    localLayoutParams.setMargins(0, 0, 0, paramContext.getResources().getDimensionPixelOffset(2131361936));
    localView.setLayoutParams(localLayoutParams);
    localFrameLayout.addView(localView);
    return localFrameLayout;
  }
  
  private void updateAccountList()
  {
    if ((UserUtils.e) && (verificationBanner != null)) {
      verificationBanner.setVisibility(8);
    }
  }
  
  public void a()
  {
    updateAccountList();
    if (TextUtils.isEmpty(b))
    {
      b = UserUtils.a;
      getArguments().putString("username", b);
      pager.setAdapter(null);
      pager.setAdapter(new UserProfileFragment.UserProfileBodyPagerAdapter(this, null));
      pager.getAdapter().notifyDataSetChanged();
    }
  }
  
  public void b(int paramInt, boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      if (d) {
        b(paramInt);
      }
    }
    else {
      from(d);
    }
  }
  
  public void b(BusStation.OnCommentPostedEvent paramOnCommentPostedEvent)
  {
    super.b(paramOnCommentPostedEvent);
    CommentsLayout localCommentsLayout = (CommentsLayout)pager.findViewWithTag("comments_profile_tab");
    paramOnCommentPostedEvent = (DVNTComment)paramOnCommentPostedEvent.getArguments().getSerializable("comment_posted");
    if (localCommentsLayout == null) {
      return;
    }
    localCommentsLayout.b(paramOnCommentPostedEvent);
  }
  
  public boolean b()
  {
    if (!isResumed()) {
      return false;
    }
    if (d)
    {
      from(d);
      return true;
    }
    return super.b();
  }
  
  protected HomeActivity.BottomBarButton c()
  {
    return HomeActivity.BottomBarButton.c;
  }
  
  public void goTo()
  {
    goTo(true);
  }
  
  protected boolean isAuthenticatedUser()
  {
    return true;
  }
  
  public void logPreferences()
  {
    goTo(false);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    b = getArguments().getString("username");
    DVNTLog.get(">>>>>username : " + b, new Object[0]);
    a = getArguments().getBoolean("own_profile", false);
    c = getArguments().getString("notification_comment_id");
    setRetainInstance(true);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = paramLayoutInflater.inflate(2130968690, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    ViewHelper.a(mainPanel, SlashBackgroundDrawable.getDrawable(getActivity()));
    e();
    if ((UserUtils.e) || (!a)) {
      verificationBanner.setVisibility(8);
    }
    pager.setOffscreenPageLimit(1);
    pager.setAdapter(new UserProfileFragment.UserProfileBodyPagerAdapter(this, null));
    indicator.setViewPager(pager);
    indicator.setOnTabClickListener(this);
    indicator.setOnTabReselectedListener(this);
    indicator.setOnPageChangeListener(new UserProfileFragment.2(this));
    if (getArguments().containsKey("initial_tab"))
    {
      paramViewGroup = (UserProfileFragment.UserProfileTab)getArguments().getSerializable("initial_tab");
      pager.setCurrentItem(paramViewGroup.ordinal());
    }
    appBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener(this));
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public void onLoadedFocusedComment(BusStation.OnLoadedFocusedComment paramOnLoadedFocusedComment)
  {
    t = new FloatingButtonDescription(getString(2131231242), new CommentUtils.OnClickOpenCommentListener(paramOnLoadedFocusedComment.getOrigin(), b, CommentType.g));
    init();
  }
  
  public void onPostExecute(BusStation.OnUserSettingChangeEvent paramOnUserSettingChangeEvent)
  {
    if (!isAdded()) {
      return;
    }
    switch (UserProfileFragment.4.d[paramOnUserSettingChangeEvent.getValue().ordinal()])
    {
    default: 
      
    case 1: 
      if (pager != null)
      {
        int i = pager.getCurrentItem();
        pager.setAdapter(new UserProfileFragment.UserProfileBodyPagerAdapter(this, null));
        pager.setCurrentItem(i);
        pager.getAdapter().notifyDataSetChanged();
        return;
      }
      break;
    case 2: 
      if (userProfileCard != null)
      {
        userProfileCard.onLoadFinished();
        return;
      }
      break;
    case 3: 
      if (userProfileCard != null)
      {
        userProfileCard.handleResult();
        return;
      }
      break;
    case 4: 
      if (userProfileCard != null) {
        e();
      }
      break;
    }
  }
  
  public void onResume()
  {
    super.onResume();
    if (d)
    {
      toolbarTitle.setVisibility(0);
      if ((a) && (!d)) {
        break label80;
      }
      settingButton.setVisibility(8);
    }
    for (;;)
    {
      updateAccountList();
      return;
      toolbarTitle.setVisibility(8);
      if (!DVNTAbstractAsyncAPI.isUserSession(getActivity())) {
        break;
      }
      ((HomeActivity)getActivity()).show(true);
      break;
      label80:
      settingButton.setVisibility(0);
    }
  }
  
  void onSettingsClick()
  {
    getActivity().startActivityForResult(UserSettingsActivity.startActivity(getActivity()), 105);
    getActivity().overridePendingTransition(2131034138, 2131034142);
  }
  
  public void onStart()
  {
    super.onStart();
    BusStation.get().register(this);
    mReceiver = Action.newAction("http://schema.org/ViewAction", getString(2131231403, new Object[] { b }), Uri.parse(getString(2131231402, new Object[] { b })), Uri.parse(getString(2131231401, new Object[] { DeepLinkUtils.getConfigFile(getActivity()), b })));
    AppIndex.AppIndexApi.start(getApplicationContext(), mReceiver);
  }
  
  public void onStop()
  {
    AppIndex.AppIndexApi.get(getApplicationContext(), mReceiver);
    BusStation.get().unregister(this);
    super.onStop();
  }
  
  public void setFrom(int paramInt)
  {
    indicator.d(paramInt);
  }
  
  public void validateInput()
  {
    if (!DVNTContextUtils.isContextDead(getActivity()))
    {
      if (!isResumed()) {
        return;
      }
      userProfileCard.setEnabled(false);
      d = true;
      settingButton.setVisibility(8);
      toolbarTitle.setVisibility(0);
      b(pager.getCurrentItem());
    }
  }
}
