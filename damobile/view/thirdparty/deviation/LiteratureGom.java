package com.deviantart.android.damobile.view.thirdparty.deviation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.view.thirdparty.GomType;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class LiteratureGom
  extends com.deviantart.android.damobile.view.gom.deviation.DeviationGom<com.deviantart.android.damobile.view.gom.deviation.viewholder.LiteratureViewHolder>
{
  public LiteratureGom() {}
  
  public GomType b()
  {
    return GomType.a;
  }
  
  public void c(Context paramContext, DeviationDescription paramDeviationDescription, com.deviantart.android.damobile.view.thirdparty.deviation.viewholder.LiteratureViewHolder paramLiteratureViewHolder, boolean paramBoolean)
  {
    DVNTDeviation localDVNTDeviation = paramDeviationDescription.a();
    catPath.setText(DAFormatUtils.getTitle(localDVNTDeviation.getCategoryPath()));
    title.setText(localDVNTDeviation.getTitle());
    preview.setText(Jsoup.get(localDVNTDeviation.getExcerpt()).get());
    if (paramBoolean) {
      DeviationUtils.b(faveView, localDVNTDeviation);
    }
    super.b(paramContext, paramDeviationDescription, paramLiteratureViewHolder, paramBoolean);
  }
  
  public com.deviantart.android.damobile.view.thirdparty.deviation.viewholder.LiteratureViewHolder inflate(Context paramContext, ViewGroup paramViewGroup)
  {
    return new com.deviantart.android.damobile.view.thirdparty.deviation.viewholder.LiteratureViewHolder(LayoutInflater.from(paramContext).inflate(2130968701, paramViewGroup, false));
  }
}
