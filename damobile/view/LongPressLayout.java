package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.deviantart.android.damobile.util.EventPayload;
import com.deviantart.android.damobile.util.Graphics;
import java.util.ArrayList;
import java.util.Iterator;

public class LongPressLayout
  extends FrameLayout
{
  private static int c = -1;
  private RelativeLayout a;
  private boolean b = true;
  int[] k = new int[2];
  private ArrayList<MenuButton> m = new ArrayList();
  GestureDetector mTapDetector;
  private boolean y;
  private boolean z = false;
  
  public LongPressLayout(Context paramContext)
  {
    super(paramContext);
    init((Activity)paramContext);
  }
  
  public LongPressLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init((Activity)paramContext);
  }
  
  private void a(MotionEvent paramMotionEvent)
  {
    requestFocus();
    Object localObject1 = getContext();
    Object localObject2 = ((Context)localObject1).getResources();
    if (c == -1) {
      c = Graphics.add((Context)localObject1, 50);
    }
    int n = (int)paramMotionEvent.getX();
    int i1 = (int)paramMotionEvent.getY();
    int i2 = ((Resources)localObject2).getDimensionPixelSize(2131361955);
    int j = getWidth();
    a = new RelativeLayout((Context)localObject1);
    a.setClipChildren(false);
    paramMotionEvent = new FrameLayout.LayoutParams(-1, -1);
    localObject1 = new ImageView((Context)localObject1);
    ((ImageView)localObject1).setImageResource(2130837821);
    localObject2 = new RelativeLayout.LayoutParams(c, c);
    leftMargin = (n - 60);
    topMargin = (i1 - 60);
    a.addView((View)localObject1, (ViewGroup.LayoutParams)localObject2);
    int i = 90;
    if (n < j / 2)
    {
      if (n > j / 4) {
        i = 100;
      }
      if (i1 < i2) {
        i = (int)Math.toDegrees(Math.asin(i1 / i2)) - 20;
      }
      for (;;)
      {
        localObject1 = m.iterator();
        while (((Iterator)localObject1).hasNext())
        {
          a((MenuButton)((Iterator)localObject1).next(), a, i2, i, n, i1);
          i -= 40;
        }
        if (i1 < i2 + 50) {
          i = 45;
        }
      }
    }
    if (n < j * 0.75D)
    {
      i = 80;
      if (i1 >= i2) {
        break label376;
      }
      i = 180 - (int)Math.toDegrees(Math.asin(i1 / i2)) + 20;
    }
    for (;;)
    {
      j = m.size() - 1;
      while (j >= 0)
      {
        a((MenuButton)m.get(j), a, i2, i, n, i1);
        i += 40;
        j -= 1;
      }
      i = 110;
      break;
      label376:
      if (i1 < i2 + 50) {
        i = 135;
      }
    }
    a.startAnimation(getShowAnimation(n, i1));
    addView(a, paramMotionEvent);
  }
  
  private void a(MenuButton paramMenuButton, RelativeLayout paramRelativeLayout, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(c, c);
    leftMargin = (paramInt3 - 60 + (int)(paramInt1 * Math.cos(Math.toRadians(paramInt2))));
    topMargin = (paramInt4 - 60 - (int)(paramInt1 * Math.sin(Math.toRadians(paramInt2))));
    ViewGroup localViewGroup = (ViewGroup)paramMenuButton.getParent();
    if (localViewGroup != null) {
      localViewGroup.removeView(paramMenuButton);
    }
    paramRelativeLayout.addView(paramMenuButton, localLayoutParams);
  }
  
  private Animation getShowAnimation(int paramInt1, int paramInt2)
  {
    AnimationSet localAnimationSet = new AnimationSet(true);
    localAnimationSet.setDuration(300L);
    localAnimationSet.setInterpolator(new AccelerateDecelerateInterpolator());
    localAnimationSet.addAnimation(new AlphaAnimation(0.0F, 1.0F));
    localAnimationSet.addAnimation(new ScaleAnimation(0.0F, 1.0F, 0.0F, 1.0F, paramInt1, paramInt2));
    return localAnimationSet;
  }
  
  private void hide()
  {
    Animation localAnimation = AnimationUtils.loadAnimation(getContext(), 2131034113);
    localAnimation.setDuration(300L);
    localAnimation.setAnimationListener(new LongPressLayout.2(this));
    a.startAnimation(localAnimation);
  }
  
  private boolean inChild(MenuButton paramMenuButton, int paramInt1, int paramInt2)
  {
    return (paramInt1 > paramMenuButton.getLeft()) && (paramInt1 < paramMenuButton.getRight()) && (paramInt2 > paramMenuButton.getTop()) && (paramInt2 < paramMenuButton.getBottom());
  }
  
  private void select(boolean paramBoolean)
  {
    y = paramBoolean;
    ViewParent localViewParent = getParent();
    if (localViewParent != null) {
      localViewParent.requestDisallowInterceptTouchEvent(paramBoolean);
    }
  }
  
  private void update()
  {
    Animation localAnimation = null;
    EventPayload.add();
    y = false;
    if (m != null)
    {
      if (m.isEmpty()) {
        return;
      }
      z = true;
      Iterator localIterator = m.iterator();
      boolean bool = false;
      Object localObject = null;
      while (localIterator.hasNext())
      {
        MenuButton localMenuButton = (MenuButton)localIterator.next();
        if (localMenuButton.isSelected())
        {
          bool = localMenuButton.b();
          localMenuButton.setSelected(false);
          localObject = localMenuButton;
        }
      }
      if (localObject != null) {
        localAnimation = localObject.getOnSelectedAnimation();
      }
      if ((localAnimation == null) || (!bool))
      {
        hide();
        return;
      }
      localObject.setOnAnimationFinishListener(new LongPressLayout.1(this));
      localObject.startAnimation(localAnimation);
    }
  }
  
  public void init(Activity paramActivity)
  {
    mTapDetector = new GestureDetector(paramActivity, new LongPressLayout.LongPressListener(this));
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    if (!b) {
      return false;
    }
    if (y) {
      return onTouchEvent(paramMotionEvent);
    }
    mTapDetector.onTouchEvent(paramMotionEvent);
    return false;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (paramMotionEvent.getAction() != 1)
    {
      int i = (int)paramMotionEvent.getX();
      int j = (int)paramMotionEvent.getY();
      if (m != null)
      {
        if (m.isEmpty()) {
          return false;
        }
        int n = getContext().getResources().getDimensionPixelSize(2131361955);
        paramMotionEvent = m.iterator();
        while (paramMotionEvent.hasNext())
        {
          MenuButton localMenuButton = (MenuButton)paramMotionEvent.next();
          int i1 = localMenuButton.getLeft() - i;
          int i2 = localMenuButton.getTop() - j;
          float f = Math.max(0.9F, Math.min(1.5F, (n - (float)Math.sqrt(new Double(i1 * i1 + i2 * i2).doubleValue())) / n * 0.5F + 1.0F));
          localMenuButton.setScaleX(f);
          localMenuButton.setScaleY(f);
          localMenuButton.setSelected(inChild(localMenuButton, i, j));
        }
        return true;
      }
    }
    else
    {
      update();
      select(false);
    }
    return false;
  }
  
  public void onWindowFocusChanged(boolean paramBoolean)
  {
    getLocationOnScreen(k);
    super.onWindowFocusChanged(paramBoolean);
  }
}
