package com.deviantart.android.damobile.util.deeplink;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.BaseBundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.damobile.activity.HomeActivity.HomeActivityPages;
import com.deviantart.android.damobile.fragment.DeviationFullViewFragment;
import com.deviantart.android.damobile.fragment.DeviationFullViewFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.HomeBaseFragment.HomeFragmentInstanceBuilder;
import com.deviantart.android.damobile.fragment.NotesDetailFragment;
import com.deviantart.android.damobile.fragment.StashOverlayFragment;
import com.deviantart.android.damobile.fragment.StatusFullViewFragment;
import com.deviantart.android.damobile.fragment.StatusFullViewFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.UserProfileFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.WatchFeedFragment;
import com.deviantart.android.damobile.fragment.WelcomeDialogFragment;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.SharedPreferenceUtil;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.deviation.DeviationPanelTab;
import com.deviantart.android.damobile.util.discovery.DiscoveryPageInfo;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.datoolkit.logger.DVNTLog;
import java.util.List;

public class DeepLinkController
{
  public DeepLinkController() {}
  
  private static void b(Activity paramActivity, Uri paramUri, boolean paramBoolean)
  {
    int i = 1;
    int j = paramUri.getPathSegments().size();
    Object localObject = DeepLinkController.Route.a(paramUri.getHost());
    if (localObject == null) {
      return;
    }
    switch (DeepLinkController.1.b[localObject.ordinal()])
    {
    default: 
      return;
    case 1: 
      if (j <= 0) {
        return;
      }
      paramUri = (String)paramUri.getPathSegments().get(0);
      if ("123456".equals(paramUri))
      {
        Toast.makeText(paramActivity, "Test is a success", 0).show();
        return;
      }
      ScreenFlowManager.get(paramActivity, NotesDetailFragment.a(paramUri), "notes_detail|" + paramUri, paramBoolean);
      return;
    case 2: 
      if (j <= 0) {
        return;
      }
      paramUri = (String)paramUri.getPathSegments().get(0);
      ScreenFlowManager.get(paramActivity, (StatusFullViewFragment)new StatusFullViewFragment.InstanceBuilder().b(paramUri).a(), "status_full_view" + paramUri, paramBoolean);
      return;
    case 3: 
      if (j <= 0) {
        return;
      }
      localObject = (String)paramUri.getPathSegments().get(0);
      if (j <= 1)
      {
        paramUri = DiscoveryPageInfo.getString((String)localObject);
        if (paramUri == null)
        {
          Log.e("DeepLink", "could not find browse mode : " + (String)localObject);
          Toast.makeText(paramActivity, "could not open the browse page.", 0).show();
          return;
        }
        NavigationUtils.c(paramActivity, paramUri);
        return;
      }
      if (!"morelikethis".equals(localObject)) {
        return;
      }
      NavigationUtils.c(paramActivity, (String)paramUri.getPathSegments().get(1), paramBoolean);
      return;
    case 4: 
      if (j <= 0) {
        break;
      }
    }
    for (paramUri = (String)paramUri.getPathSegments().get(0);; paramUri = null)
    {
      NavigationUtils.load(paramActivity, paramUri);
      return;
      if (!DVNTAbstractAsyncAPI.isUserSession(paramActivity))
      {
        Toast.makeText(paramActivity, paramActivity.getString(2131230974), 1).show();
        return;
      }
      DVNTAbstractAsyncAPI.cancelAllRequests();
      ScreenFlowManager.goHome(paramActivity);
      ScreenFlowManager.get(paramActivity, new WatchFeedFragment(), HomeActivity.HomeActivityPages.p.a(), false);
      return;
      if (j <= 1) {
        break;
      }
      NavigationUtils.c(paramActivity, (String)paramUri.getPathSegments().get(0), (String)paramUri.getPathSegments().get(1), paramBoolean, true);
      return;
      if (j <= 1) {
        break;
      }
      NavigationUtils.b(paramActivity, (String)paramUri.getPathSegments().get(0), (String)paramUri.getPathSegments().get(1), paramBoolean, true);
      return;
      if (j <= 0) {
        break;
      }
      NavigationUtils.c(paramActivity, (String)paramUri.getPathSegments().get(0), paramBoolean, true);
      return;
      DVNTLog.get("DPL - user profile", new Object[0]);
      if (j <= 0) {
        break;
      }
      localObject = (String)paramUri.getPathSegments().get(0);
      DVNTLog.get("DPL - username " + (String)localObject, new Object[0]);
      paramUri = UserUtils.a;
      if (paramUri == null) {
        paramUri = SharedPreferenceUtil.getString(paramActivity, "recent_username", null);
      }
      for (;;)
      {
        if ((paramUri != null) && (paramUri.equals(localObject))) {}
        for (boolean bool = true;; bool = false)
        {
          ScreenFlowManager.get(paramActivity, (Fragment)new UserProfileFragment.InstanceBuilder().a((String)localObject).a(bool).a(), HomeActivity.HomeActivityPages.b.a() + (String)localObject, paramBoolean);
          return;
        }
        if (j <= 0) {
          break;
        }
        localObject = (String)paramUri.getPathSegments().get(0);
        String str = (String)paramUri.getPathSegments().get(1);
        paramUri = (String)paramUri.getPathSegments().get(2);
        switch (((String)localObject).hashCode())
        {
        default: 
          label811:
          i = -1;
        }
        for (;;)
        {
          switch (i)
          {
          default: 
            Log.e("DeepLinkController", "Invalid comment type!");
            return;
            if (!((String)localObject).equals("deviation")) {
              break label811;
            }
            i = 0;
            continue;
            if (!((String)localObject).equals("status")) {
              break label811;
            }
            continue;
            if (!((String)localObject).equals("profile")) {
              break label811;
            }
            i = 2;
          }
        }
        ScreenFlowManager.get(paramActivity, (DeviationFullViewFragment)new DeviationFullViewFragment.InstanceBuilder().a(false).b(str).a(DeviationPanelTab.i).a(paramUri).a(), "deviation_fullview_single" + str, paramBoolean);
        return;
        ScreenFlowManager.get(paramActivity, (StatusFullViewFragment)new StatusFullViewFragment.InstanceBuilder().b(str).a(paramUri).a(), "status_full_view" + str, paramBoolean);
        return;
        UserUtils.c(paramActivity, str, paramUri, paramBoolean);
        return;
        if (j <= 0) {
          break;
        }
        localObject = (String)paramUri.getPathSegments().get(0);
        if ("embedded".equals(localObject))
        {
          decode(paramActivity, paramUri, j, paramBoolean);
          return;
        }
        ScreenFlowManager.get(paramActivity, (DeviationFullViewFragment)new DeviationFullViewFragment.InstanceBuilder().b((String)localObject).a(), "deviation_fullview_single" + (String)localObject, paramBoolean);
        return;
        if (j <= 0) {
          break;
        }
        paramUri = (String)paramUri.getPathSegments().get(0);
        localObject = Long.valueOf(paramUri);
        if (localObject == null) {
          break;
        }
        ScreenFlowManager.get(paramActivity, StashOverlayFragment.a(((Long)localObject).longValue()), "stash_overlay" + paramUri, paramBoolean);
        return;
        if ((j <= 0) || (TextUtils.isEmpty((CharSequence)paramUri.getPathSegments().get(0)))) {
          break;
        }
        promptForPassword(paramActivity, (String)paramUri.getPathSegments().get(0));
        return;
        if ((j <= 2) || (!"verify".equals(paramUri.getPathSegments().get(0))) || (!"mobile".equals(paramUri.getPathSegments().get(1))) || (TextUtils.isEmpty((CharSequence)paramUri.getPathSegments().get(2)))) {
          break;
        }
        promptForPassword(paramActivity, (String)paramUri.getPathSegments().get(2));
        return;
      }
    }
  }
  
  private static void decode(Activity paramActivity, Uri paramUri, int paramInt, boolean paramBoolean)
  {
    if (paramInt <= 1) {
      return;
    }
    String str = (String)paramUri.getPathSegments().get(1);
    if (paramInt > 2) {}
    for (paramUri = (String)paramUri.getPathSegments().get(2);; paramUri = null)
    {
      ScreenFlowManager.get(paramActivity, (DeviationFullViewFragment)new DeviationFullViewFragment.InstanceBuilder().putByte(str).b(paramUri).a(), "deviation_fullview_streamdeviationembeddedloader" + str + "|" + paramUri, paramBoolean);
      return;
    }
  }
  
  public static boolean handleIntent(Intent paramIntent)
  {
    if (paramIntent.getData() != null) {
      return true;
    }
    return (paramIntent.getExtras() != null) && (paramIntent.getExtras().getString("pn_uri") != null);
  }
  
  public static void loadData(Activity paramActivity, Intent paramIntent, boolean paramBoolean)
  {
    Object localObject;
    if (paramIntent.getData() != null)
    {
      localObject = paramIntent.getData();
      paramIntent.setData(null);
    }
    for (;;)
    {
      if (localObject == null)
      {
        return;
        if (paramIntent.getExtras() != null)
        {
          localObject = paramIntent.getExtras().getString("pn_uri");
          if (localObject != null)
          {
            localObject = Uri.parse((String)localObject);
            TrackerUtil.a(paramActivity, EventKeys.Category.D, "tap_pn_note");
          }
        }
      }
      else
      {
        DVNTLog.get("[deeplink] routing intent", new Object[] { paramIntent });
        b(paramActivity, (Uri)localObject, paramBoolean);
        return;
      }
      localObject = null;
    }
  }
  
  private static void promptForPassword(Activity paramActivity, String paramString)
  {
    WelcomeDialogFragment.access$000(paramString, DVNTAbstractAsyncAPI.isUserSession(paramActivity)).show(paramActivity.getFragmentManager(), "WelcomeDialogFragment");
  }
}
