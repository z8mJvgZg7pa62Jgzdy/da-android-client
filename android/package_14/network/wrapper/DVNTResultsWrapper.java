package com.deviantart.android.android.package_14.network.wrapper;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTResultsWrapper<T>
  implements Serializable
{
  @SerializedName("results")
  private T results;
  
  public DVNTResultsWrapper() {}
  
  public Object getResults()
  {
    return results;
  }
  
  public void setResults(Object paramObject)
  {
    results = paramObject;
  }
}
