package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.damobile.util.notifications.NotificationItemData;

public class APINotificationsStackLoader
  extends StreamLoader<NotificationItemData>
{
  private final String d;
  
  public APINotificationsStackLoader(String paramString)
  {
    d = paramString;
  }
  
  public String b()
  {
    return "notification_stack_" + d;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTAsyncAPI.feedbackMessagesForStack(d, Integer.valueOf(paramInt), Integer.valueOf(a), Integer.valueOf(2));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APINotificationsStackLoader.1(this, paramStreamLoadListener));
  }
}
