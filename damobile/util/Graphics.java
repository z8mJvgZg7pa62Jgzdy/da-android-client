package com.deviantart.android.damobile.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.graphics.Point;
import android.opengl.GLES10;
import android.util.DisplayMetrics;
import org.apache.commons.io.FilenameUtils;

public class Graphics
{
  static final int maxMemory = Math.max(arrayOfInt[0], 2048);
  
  static
  {
    int[] arrayOfInt = new int[1];
    GLES10.glGetIntegerv(3379, arrayOfInt, 0);
  }
  
  public Graphics() {}
  
  public static int add(Context paramContext, int paramInt)
  {
    return Math.round(paramInt * toString(paramContext) + 0.5F);
  }
  
  public static DisplayMetrics getDisplayMetrics(Context paramContext)
  {
    return paramContext.getResources().getDisplayMetrics();
  }
  
  public static int height(Context paramContext)
  {
    return getDisplayMetricswidthPixels;
  }
  
  public static Path parsePath(Point... paramVarArgs)
  {
    if (paramVarArgs.length < 3) {
      throw new IllegalArgumentException("Expecting 3 or more points");
    }
    Path localPath = new Path();
    localPath.setFillType(Path.FillType.EVEN_ODD);
    localPath.moveTo(0x, 0y);
    int i = 1;
    while (i < paramVarArgs.length)
    {
      Point localPoint = paramVarArgs[i];
      localPath.lineTo(x, y);
      i += 1;
    }
    localPath.close();
    return localPath;
  }
  
  public static boolean setIcon(String paramString)
  {
    return FilenameUtils.getExtension(paramString).contains("gif");
  }
  
  public static float toString(Context paramContext)
  {
    return getDisplayMetricsdensity;
  }
  
  public static int width(Context paramContext)
  {
    return getDisplayMetricsheightPixels;
  }
}
