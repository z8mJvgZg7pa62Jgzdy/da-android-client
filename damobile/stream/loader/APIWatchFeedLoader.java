package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTFeedItem;

public class APIWatchFeedLoader
  extends StreamLoader<DVNTFeedItem>
{
  String e = "";
  
  public APIWatchFeedLoader() {}
  
  public String b()
  {
    return "activityfeed";
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    if (paramBoolean) {
      e = "";
    }
    DVNTCommonAsyncAPI.feedHome(e).call(paramContext, new APIWatchFeedLoader.1(this, paramStreamLoadListener));
  }
}
