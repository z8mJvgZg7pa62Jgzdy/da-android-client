package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTCategory;

public class APISubmissionCategoryTreeLoader
  extends StreamLoader<DVNTCategory>
{
  private Boolean b;
  private String c;
  private String d;
  
  public APISubmissionCategoryTreeLoader(String paramString1, String paramString2, Boolean paramBoolean)
  {
    d = paramString1;
    c = paramString2;
    if ((paramString1 != null) && (paramString1.startsWith("frequent"))) {}
    for (boolean bool = true;; bool = paramBoolean.booleanValue())
    {
      b = Boolean.valueOf(bool);
      return;
    }
  }
  
  public String b()
  {
    return "categorytreeloader" + d + c;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTCommonAsyncAPI.submissionCategoryTree(d, c, b).noCache().call(paramContext, new APISubmissionCategoryTreeLoader.1(this, paramStreamLoadListener));
  }
}
