package com.deviantart.android.damobile.util.deviation;

public enum DeviationPanelTab
{
  private String F;
  private String l;
  
  static
  {
    b = new DeviationPanelTab("TAB_FAV", 2, "favourites", "DeviationPanelFavIcon");
  }
  
  private DeviationPanelTab(String paramString1, String paramString2)
  {
    l = paramString1;
    F = paramString2;
  }
  
  public static DeviationPanelTab get(int paramInt)
  {
    return values()[paramInt];
  }
  
  public String b()
  {
    return F;
  }
  
  public String getValue()
  {
    return l;
  }
}
