package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTFriend
  implements Serializable
{
  @SerializedName("groupname")
  String groupName;
  @SerializedName("lastvisit")
  String lastVisit;
  DVNTUser user;
  @SerializedName("watch")
  DVNTFriend.DVNTWatchInfo watchInfo;
  @SerializedName("watches_you")
  boolean watchesYou;
  @SerializedName("is_watching")
  boolean youreWatching;
  
  public DVNTFriend() {}
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      paramObject = (DVNTFriend)paramObject;
      return new EqualsBuilder().append(user, user).append(groupName, groupName).append(watchesYou, watchesYou).append(youreWatching, youreWatching).append(lastVisit, lastVisit).append(watchInfo, watchInfo).isEquals();
    }
    return false;
  }
  
  public String getGroupName()
  {
    return groupName;
  }
  
  public String getLastVisit()
  {
    return lastVisit;
  }
  
  public DVNTUser getUser()
  {
    return user;
  }
  
  public DVNTFriend.DVNTWatchInfo getWatchInfo()
  {
    return watchInfo;
  }
  
  public boolean getYoureWatching()
  {
    return youreWatching;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().append(user).append(groupName).append(watchesYou).append(youreWatching).append(lastVisit).append(watchInfo).toHashCode();
  }
  
  public boolean isWatchesYou()
  {
    return watchesYou;
  }
  
  public boolean isWatchingYou()
  {
    return watchesYou;
  }
  
  public boolean isYoureWatching()
  {
    return youreWatching;
  }
  
  public void setGroupName(String paramString)
  {
    groupName = paramString;
  }
  
  public void setLastVisit(String paramString)
  {
    lastVisit = paramString;
  }
  
  public void setUser(DVNTUser paramDVNTUser)
  {
    user = paramDVNTUser;
  }
  
  public void setWatchInfo(DVNTFriend.DVNTWatchInfo paramDVNTWatchInfo)
  {
    watchInfo = paramDVNTWatchInfo;
  }
  
  public void setWatchesYou(boolean paramBoolean)
  {
    watchesYou = paramBoolean;
  }
  
  public void setYoureWatching(boolean paramBoolean)
  {
    youreWatching = paramBoolean;
  }
}
