package com.deviantart.android.damobile.view.tooltip.tour;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.viewpagerindicator.CirclePageIndicator;

public class DATourSlideView
  extends FrameLayout
{
  DAToolTipTour b;
  @Bind({2131690083})
  View button;
  @Bind({2131690084})
  TextView buttonText;
  @Bind({2131690081})
  ImageView image;
  @Bind({2131690080})
  CirclePageIndicator indicator;
  int l;
  ViewPager pager;
  @Bind({2131690082})
  TextView text;
  @Bind({2131690079})
  TextView title;
  
  public DATourSlideView(Context paramContext, DAToolTipTour paramDAToolTipTour)
  {
    super(paramContext);
    b = paramDAToolTipTour;
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130968787, this, true);
    ButterKnife.bind(this);
    if (a.length > 1)
    {
      pager = new ViewPager(getContext());
      pager.setAdapter(new DATourSlideView.DummyPagerAdapter(this));
      indicator.setViewPager(pager);
      indicator.setOnTouchListener(new DATourSlideView.1(this));
      return;
    }
    indicator.setVisibility(8);
    image.setPadding(0, (int)getResources().getDimension(2131361993), 0, 0);
  }
  
  public void onClickNext()
  {
    b.a(l);
  }
  
  public void onCreateView(int paramInt)
  {
    l = paramInt;
    Object localObject = b.a[paramInt];
    title.setText(description);
    image.setImageResource(x);
    text.setText(c);
    if (b.b(paramInt))
    {
      button.setBackgroundResource(2130837596);
      buttonText.setTextColor(getResources().getColor(17170443));
      localObject = buttonText;
      if (b.a.length != 1) {
        break label125;
      }
    }
    label125:
    for (int i = 2131231396;; i = 2131231394)
    {
      ((TextView)localObject).setText(i);
      if (pager == null) {
        break;
      }
      pager.setCurrentItem(paramInt);
      return;
    }
  }
}
