package com.deviantart.android.damobile.view.notes;

import android.os.Handler;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.adapter.HeaderFooterRecyclerViewAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.notes.Mark;
import com.deviantart.android.damobile.util.notes.Mark.DataUpdater;
import com.deviantart.android.damobile.util.notes.NotesItemData;
import com.deviantart.android.damobile.util.notes.NotesItemViewHolder;
import com.deviantart.android.damobile.util.notes.NotesPage;
import com.deviantart.android.damobile.util.opt.ItemDeleteHelperBase;
import com.deviantart.android.damobile.util.opt.ItemDeleteType;
import com.deviantart.android.damobile.util.opt.ItemDeleteUtils;
import com.deviantart.android.damobile.util.opt.NoteItemDeleteHelper;
import java.util.HashMap;

public class NotesListAdapter
  extends com.deviantart.android.damobile.view.mc.MCListAdapterBase<NotesItemData, NotesPage>
{
  String c;
  
  public NotesListAdapter(Stream paramStream, String paramString)
  {
    c = paramStream;
    c = paramString;
    b();
  }
  
  private void a(NotesItemData paramNotesItemData, Mark paramMark)
  {
    if (paramMark.e() < 0) {}
    int j;
    for (int i = 1;; i = 0)
    {
      j = c.a(paramNotesItemData);
      if ((j != -1) || (i != 0)) {
        break;
      }
      c.a(0, paramNotesItemData);
      k += 1;
      try
      {
        close(0);
        return;
      }
      catch (Exception paramNotesItemData)
      {
        new Handler().post(NotesListAdapter..Lambda.2.c(this));
        return;
      }
    }
    if ((j != -1) && (i != 0)) {
      try
      {
        add(j);
        c.close(j);
        k -= 1;
        return;
      }
      catch (Exception paramNotesItemData)
      {
        for (;;)
        {
          new Handler().post(NotesListAdapter..Lambda.3.c(this));
        }
      }
    }
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    return new NotesItemViewHolder(LayoutInflater.from(paramViewGroup.getContext()).inflate(2130968742, paramViewGroup, false), c);
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    ((NotesItemViewHolder)paramViewHolder).b((NotesItemData)c.get(paramInt));
  }
  
  public void b(NotesItemData paramNotesItemData, Mark paramMark, boolean paramBoolean)
  {
    if (c == null) {
      return;
    }
    if (paramBoolean)
    {
      a(paramNotesItemData, paramMark);
      return;
    }
    int i = c.a(paramNotesItemData);
    if (i != -1)
    {
      paramNotesItemData = (NotesItemData)get(i);
      paramMark.b().b(paramNotesItemData);
      try
      {
        d(i);
        return;
      }
      catch (Exception paramNotesItemData)
      {
        new Handler().post(NotesListAdapter..Lambda.1.c(this));
      }
    }
  }
  
  protected HashMap get()
  {
    return (HashMap)getCount().get().get(c);
  }
  
  protected NoteItemDeleteHelper getCount()
  {
    return (NoteItemDeleteHelper)ItemDeleteUtils.a(ItemDeleteType.c);
  }
}
