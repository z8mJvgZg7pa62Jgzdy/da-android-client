package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.util.DAWebViewHelper;
import com.deviantart.android.damobile.util.DAWebViewHelper.WebViewParams;
import com.deviantart.android.damobile.util.DAWebViewHelper.WebViewParams.Builder;
import com.deviantart.android.damobile.util.MemberType;
import com.deviantart.android.damobile.util.UserDisplay;
import com.deviantart.android.damobile.util.ViewHelper;
import com.deviantart.android.damobile.view.userprofile.UserCardData;

public class UserBioOverlayFragment
  extends HomeFullViewFragment
{
  @Bind({2131689842})
  WebView description;
  UserCardData msg;
  @Bind({2131689841})
  TextView realName;
  @Bind({2131689840})
  TextView username;
  @Bind({2131689843})
  TextView website;
  
  public UserBioOverlayFragment() {}
  
  public static UserBioOverlayFragment doSearch(UserCardData paramUserCardData)
  {
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("user_card", paramUserCardData);
    paramUserCardData = new UserBioOverlayFragment();
    paramUserCardData.setArguments(localBundle);
    return paramUserCardData;
  }
  
  private void onCreateView()
  {
    Object localObject = new DVNTUser();
    ((DVNTUser)localObject).setUserName(msg.format());
    ((DVNTUser)localObject).setType(msg.a().code());
    username.setText(UserDisplay.a(getActivity(), (DVNTUser)localObject, true));
    ViewHelper.setText(realName, msg.getColor());
    localObject = msg.getId();
    if (TextUtils.isEmpty((CharSequence)localObject)) {
      description.setVisibility(8);
    }
    for (;;)
    {
      ViewHelper.setText(website, msg.getOverview());
      return;
      DAWebViewHelper.WebViewParams localWebViewParams = new DAWebViewHelper.WebViewParams.Builder().b("transparent").a("white").b(Integer.valueOf(20)).a(Boolean.valueOf(true)).a();
      DAWebViewHelper.onCreateView(description, (String)localObject, "", localWebViewParams);
    }
  }
  
  public void onClickClose()
  {
    getActivity().onBackPressed();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    msg = ((UserCardData)getArguments().getSerializable("user_card"));
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = paramLayoutInflater.inflate(2130968689, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    ViewHelper.format(paramLayoutInflater);
    onCreateView();
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
}
