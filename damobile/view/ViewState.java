package com.deviantart.android.damobile.view;

import android.os.Bundle;
import java.io.Serializable;
import java.util.HashMap;

public class ViewState
  implements Serializable
{
  private HashMap<ViewState.State, Bundle> c = new HashMap();
  private int d = 0;
  private ViewState.State j;
  private String l;
  
  public ViewState() {}
  
  public ViewState.State a()
  {
    return j;
  }
  
  public void a(int paramInt)
  {
    d = paramInt;
  }
  
  public void a(ViewState.State paramState)
  {
    j = paramState;
  }
  
  public void a(ViewState.State paramState, Bundle paramBundle)
  {
    c.put(paramState, paramBundle);
  }
  
  public void a(String paramString)
  {
    l = paramString;
  }
  
  public int b()
  {
    if (d == 0) {
      return 2131230883;
    }
    return d;
  }
  
  public String c()
  {
    return l;
  }
  
  public Bundle get(ViewState.State paramState)
  {
    if (c.containsKey(paramState)) {
      return (Bundle)c.get(paramState);
    }
    return null;
  }
}
