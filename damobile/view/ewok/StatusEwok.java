package com.deviantart.android.damobile.view.ewok;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTFeedItem;
import com.deviantart.android.android.package_14.model.DVNTRepostItem;
import com.deviantart.android.android.package_14.model.DVNTUserStatus;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.FeedItemType;
import com.deviantart.android.damobile.util.PlaceholderUtil;
import com.deviantart.android.damobile.util.StatusViewHelper;
import com.deviantart.android.damobile.util.markup.OrderedDAMLHelper;
import com.deviantart.android.damobile.view.MaxHeightScrollView;
import com.deviantart.android.damobile.view.ewok.decorator.WatchFeedItemEwok;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;

public class StatusEwok
  extends WatchFeedItemEwok
  implements Ewok, RepostEwok
{
  boolean b;
  boolean c = true;
  DVNTUserStatus d;
  DVNTFeedItem o;
  
  public StatusEwok(DVNTFeedItem paramDVNTFeedItem)
  {
    o = paramDVNTFeedItem;
    d = paramDVNTFeedItem.getStatus();
    b = false;
  }
  
  public StatusEwok(DVNTUserStatus paramDVNTUserStatus)
  {
    d = paramDVNTUserStatus;
    b = false;
  }
  
  public StatusEwok(DVNTUserStatus paramDVNTUserStatus, boolean paramBoolean)
  {
    d = paramDVNTUserStatus;
    b = paramBoolean;
  }
  
  private void b(Activity paramActivity, ViewGroup paramViewGroup)
  {
    if (o != null)
    {
      a(paramActivity, paramViewGroup, o);
      return;
    }
    if (d == null)
    {
      paramViewGroup.findViewById(2131689617).setVisibility(8);
      return;
    }
    SimpleDraweeView localSimpleDraweeView = (SimpleDraweeView)paramViewGroup.findViewById(2131689703);
    TextView localTextView = (TextView)paramViewGroup.findViewById(2131689705);
    StatusViewHelper.setText(paramActivity, d.getAuthor(), localSimpleDraweeView, localTextView);
    ((TextView)paramViewGroup.findViewById(2131689706)).setText(paramActivity.getString(FeedItemType.d.a()));
    paramViewGroup = (TextView)paramViewGroup.findViewById(2131689707);
    paramActivity = DAFormatUtils.format(paramActivity, d.getTime());
    if ((paramActivity == null) || (paramActivity.equals(d.getTime())))
    {
      paramViewGroup.setVisibility(8);
      return;
    }
    paramViewGroup.setVisibility(0);
    paramViewGroup.setText(paramActivity);
  }
  
  private void d(Activity paramActivity, ViewGroup paramViewGroup)
  {
    paramViewGroup.removeAllViews();
    paramViewGroup.setVisibility(8);
    if (!b)
    {
      if (!d.isShare().booleanValue()) {
        return;
      }
      Object localObject = d.getRepostItems();
      if ((localObject != null) && (!((ArrayList)localObject).isEmpty()))
      {
        localObject = EwokFactory.b((DVNTRepostItem)((ArrayList)localObject).get(0), b);
        if (localObject != null)
        {
          paramActivity = ((RepostEwok)localObject).onCreateView(paramActivity);
          if (paramActivity != null)
          {
            paramViewGroup.addView(paramActivity);
            paramViewGroup.setVisibility(0);
          }
        }
      }
    }
  }
  
  public View a(Activity paramActivity, ViewGroup paramViewGroup)
  {
    return onCreateView(paramActivity, paramViewGroup, 0, 0);
  }
  
  public void b(boolean paramBoolean)
  {
    c = paramBoolean;
  }
  
  public View init(Activity paramActivity)
  {
    LayoutInflater localLayoutInflater = (LayoutInflater)paramActivity.getSystemService("layout_inflater");
    LinearLayout localLinearLayout1 = (LinearLayout)localLayoutInflater.inflate(2130968766, null, false);
    LinearLayout localLinearLayout2 = (LinearLayout)ButterKnife.findById(localLinearLayout1, 2131690026);
    if (d.getBody() != null) {
      OrderedDAMLHelper.update(paramActivity, localLinearLayout2, d.getBody(), b);
    }
    for (;;)
    {
      d(paramActivity, (FrameLayout)ButterKnife.findById(localLinearLayout1, 2131690017));
      return localLinearLayout1;
      localLinearLayout2.addView(PlaceholderUtil.createView(localLayoutInflater, 2131231217), new ViewGroup.LayoutParams(-1, -2));
    }
  }
  
  public View onCreateView(Activity paramActivity)
  {
    if (d == null) {
      return null;
    }
    Object localObject1 = (LayoutInflater)paramActivity.getSystemService("layout_inflater");
    if (d.isDeleted().booleanValue()) {
      return PlaceholderUtil.createView((LayoutInflater)localObject1, 2131231217);
    }
    localObject1 = (LinearLayout)((LayoutInflater)localObject1).inflate(2130968769, null, false);
    Object localObject2 = (SimpleDraweeView)ButterKnife.findById((View)localObject1, 2131689729);
    Object localObject3 = (TextView)ButterKnife.findById((View)localObject1, 2131689731);
    StatusViewHelper.setText(paramActivity, d.getAuthor(), (SimpleDraweeView)localObject2, (TextView)localObject3);
    localObject2 = (TextView)ButterKnife.findById((View)localObject1, 2131689730);
    localObject3 = d.getTime();
    String str = DAFormatUtils.format(paramActivity, (String)localObject3);
    if ((str != null) && (!str.equals(localObject3))) {
      ((TextView)localObject2).setText(str);
    }
    ((FrameLayout)ButterKnife.findById((View)localObject1, 2131690032)).addView(init(paramActivity));
    return localObject1;
  }
  
  public View onCreateView(Activity paramActivity, ViewGroup paramViewGroup, int paramInt1, int paramInt2)
  {
    LinearLayout localLinearLayout = (LinearLayout)((LayoutInflater)paramActivity.getSystemService("layout_inflater")).inflate(2130968770, paramViewGroup, false);
    onCreateView(paramActivity, paramViewGroup, localLinearLayout, true);
    b(paramActivity, localLinearLayout);
    return localLinearLayout;
  }
  
  public void onCreateView(Activity paramActivity, ViewGroup paramViewGroup1, ViewGroup paramViewGroup2, boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      paramViewGroup2.removeAllViews();
      paramViewGroup2.addView((LinearLayout)((LayoutInflater)paramActivity.getSystemService("layout_inflater")).inflate(2130968770, paramViewGroup1, false));
    }
    b(paramActivity, paramViewGroup2);
    paramViewGroup1 = ButterKnife.findById(paramViewGroup2, 2131690035);
    Object localObject = StatusViewHelper.add(paramActivity, d);
    if (localObject == null)
    {
      paramViewGroup1.setVisibility(4);
      paramViewGroup1.setOnClickListener(null);
    }
    for (;;)
    {
      paramViewGroup1 = (TextView)ButterKnife.findById(paramViewGroup2, 2131690036);
      localObject = d.getCommentsCount();
      if (localObject != null) {
        paramViewGroup1.setText(String.valueOf(localObject));
      }
      paramViewGroup1.setOnClickListener(new StatusEwok.1(this, paramActivity));
      paramViewGroup1 = (MaxHeightScrollView)ButterKnife.findById(paramViewGroup2, 2131690033);
      paramViewGroup1.setMaxHeight((int)paramActivity.getResources().getDimension(2131361972));
      paramViewGroup1.removeAllViews();
      paramViewGroup1.addView(init(paramActivity));
      return;
      paramViewGroup1.setVisibility(0);
      paramViewGroup1.setOnClickListener((View.OnClickListener)localObject);
    }
  }
}
