package com.deviantart.android.android.package_14.network.wrapper;

import com.google.gson.annotations.SerializedName;

public class DVNTIncludeWrapper<T>
{
  @SerializedName("include")
  private T include;
  
  public DVNTIncludeWrapper() {}
  
  public Object getInclude()
  {
    return include;
  }
  
  public void setInclude(Object paramObject)
  {
    include = paramObject;
  }
}
