package com.deviantart.android.damobile.stream;

import java.util.ArrayList;

public enum StreamCacheStrategy
{
  private ArrayList<String> l;
  private int o;
  
  static
  {
    a = new StreamCacheStrategy("HORIZONTAL_TORPEDO", 2, 20);
    i = new StreamCacheStrategy("OTHERS", 3, 50);
  }
  
  private StreamCacheStrategy(int paramInt)
  {
    o = paramInt;
    l = new ArrayList();
  }
  
  public static void forget()
  {
    StreamCacheStrategy[] arrayOfStreamCacheStrategy = values();
    int k = arrayOfStreamCacheStrategy.length;
    int j = 0;
    while (j < k)
    {
      arrayOfStreamCacheStrategy[j].next();
      j += 1;
    }
  }
  
  public void b(String paramString)
  {
    l.remove(paramString);
  }
  
  public int d()
  {
    return o;
  }
  
  public ArrayList getValue()
  {
    return l;
  }
  
  public void next()
  {
    l.clear();
  }
}
