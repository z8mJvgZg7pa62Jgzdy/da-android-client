package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTFeedbackMessageStack
  extends DVNTFeedbackMessage
  implements Serializable
{
  @SerializedName("stack_count")
  Integer count;
  @SerializedName("stackid")
  String stackId;
  
  public DVNTFeedbackMessageStack() {}
  
  public Integer getCount()
  {
    return count;
  }
  
  public String getStackId()
  {
    return stackId;
  }
  
  public void setCount(Integer paramInteger)
  {
    count = paramInteger;
  }
  
  public void setStackId(String paramString)
  {
    stackId = paramString;
  }
  
  public void updateFeedbackMessage(DVNTFeedbackMessage paramDVNTFeedbackMessage)
  {
    setMessageId(paramDVNTFeedbackMessage.getMessageId());
    setOrphaned(paramDVNTFeedbackMessage.isOrphaned());
    setTimestamp(paramDVNTFeedbackMessage.getTimestamp());
    setOriginator(paramDVNTFeedbackMessage.getOriginator());
    setSubject(paramDVNTFeedbackMessage.getSubject());
    setProfile(paramDVNTFeedbackMessage.getProfile());
    setDeviation(paramDVNTFeedbackMessage.getDeviation());
    setStatus(paramDVNTFeedbackMessage.getStatus());
    setComment(paramDVNTFeedbackMessage.getComment());
    setCollection(paramDVNTFeedbackMessage.getCollection());
  }
}
