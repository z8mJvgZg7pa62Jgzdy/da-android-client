package com.deviantart.android.android.package_14.network;

import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.config.DVNTAPIConfig;
import retrofit.RequestInterceptor.RequestFacade;

public class DVNTRequestInterceptor
  extends DVNTAbstractRequestInterceptor
{
  public DVNTRequestInterceptor() {}
  
  public void intercept(RequestInterceptor.RequestFacade paramRequestFacade)
  {
    super.intercept(paramRequestFacade);
    Object localObject1 = DVNTAbstractAsyncAPI.getConfig();
    Object localObject2 = ((DVNTAPIConfig)localObject1).getTempBuildType();
    if (localObject2 != null) {
      paramRequestFacade.addHeader("dA-app-build", ((Enum)localObject2).toString().toLowerCase());
    }
    localObject2 = ((DVNTAPIConfig)localObject1).getUserAgent();
    if (localObject2 != null) {
      paramRequestFacade.addHeader("User-Agent", (String)localObject2);
    }
    localObject1 = ((DVNTAPIConfig)localObject1).getShowRCContent();
    if ((localObject1 != null) && (((Boolean)localObject1).booleanValue())) {
      paramRequestFacade.addQueryParam("rconly", "1");
    }
  }
}
