package com.deviantart.android.damobile.view.thirdparty.decorator;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.package_14.model.DVNTWatchRecommendationItem;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.MemberType;
import com.deviantart.android.damobile.util.UserDisplay;
import com.deviantart.android.damobile.util.WatchReco;
import com.deviantart.android.damobile.util.WatchRecoUpdateListener;
import com.deviantart.android.damobile.view.AbstractWatchButton;
import com.deviantart.android.damobile.view.gom.decorator.DecoratorGom;

public class WatchRecoDecoratorGom
  implements DecoratorGom<com.deviantart.android.damobile.view.gom.decorator.WatchRecoDecoratorViewHolder, WatchReco>
{
  public WatchRecoDecoratorGom() {}
  
  public Class getComponentType()
  {
    return WatchReco.class;
  }
  
  public void onCreate(Context paramContext, WatchRecoDecoratorViewHolder paramWatchRecoDecoratorViewHolder, WatchReco paramWatchReco)
  {
    int i = 0;
    DVNTWatchRecommendationItem localDVNTWatchRecommendationItem = paramWatchReco.getLogin();
    DVNTUser localDVNTUser = paramWatchReco.getLogin().getUser();
    String str1 = localDVNTUser.getUserName();
    MemberType localMemberType = MemberType.toString(localDVNTUser.getType());
    String str2 = localDVNTUser.getUserIconURL();
    WatchRecoUpdateListener localWatchRecoUpdateListener = paramWatchReco.getValue();
    int j = paramWatchReco.id();
    if (localDVNTUser.getIsWatching() == null) {}
    for (boolean bool = false;; bool = localDVNTUser.getIsWatching().booleanValue())
    {
      watchButton.a(str1, localMemberType, Boolean.valueOf(bool));
      watchButton.a(WatchRecoDecoratorGom..Lambda.1.d(localWatchRecoUpdateListener, bool, localDVNTWatchRecommendationItem));
      userName.setText(UserDisplay.add(paramContext, localDVNTUser));
      userName.setOnClickListener(WatchRecoDecoratorGom..Lambda.2.lambdaFactory$(paramContext, str1));
      ImageUtils.setImage(paramContext, avatar, Uri.parse(str2));
      avatar.setOnClickListener(WatchRecoDecoratorGom..Lambda.3.getCallback(paramWatchRecoDecoratorViewHolder));
      paramContext = dismiss;
      if (bool) {
        i = 8;
      }
      paramContext.setVisibility(i);
      dismiss.setOnClickListener(WatchRecoDecoratorGom..Lambda.4.setText(localWatchRecoUpdateListener, localDVNTWatchRecommendationItem, j));
      return;
    }
  }
  
  public WatchRecoDecoratorViewHolder onCreateView(Context paramContext, ViewGroup paramViewGroup, RecyclerView.ViewHolder paramViewHolder)
  {
    paramContext = LayoutInflater.from(paramContext).inflate(2130968803, paramViewGroup, false);
    ((FrameLayout)ButterKnife.findById(paramContext, 2131689650)).addView(itemView);
    return new WatchRecoDecoratorViewHolder(paramViewHolder, paramContext);
  }
}
