package com.deviantart.android.damobile.util.markup;

import com.deviantart.android.android.package_14.model.DVNTDAMLComponent;
import com.deviantart.android.android.package_14.model.DVNTDAMLComponent.List;
import java.util.ArrayList;
import java.util.Iterator;

public class DAMLHelper
{
  public DAMLHelper() {}
  
  public static String c(DVNTDAMLComponent.List paramList)
  {
    if ((paramList == null) || (paramList.isEmpty())) {
      return "";
    }
    StringBuilder localStringBuilder = new StringBuilder();
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      localStringBuilder.append(((DVNTDAMLComponent)paramList.next()).getFallbackHTML());
    }
    return localStringBuilder.toString();
  }
}
