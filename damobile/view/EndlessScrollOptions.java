package com.deviantart.android.damobile.view;

public class EndlessScrollOptions
{
  private int d;
  private EndlessScrollListener j;
  
  public EndlessScrollOptions(int paramInt, EndlessScrollListener paramEndlessScrollListener)
  {
    d = paramInt;
    j = paramEndlessScrollListener;
  }
  
  public EndlessScrollListener a()
  {
    return j;
  }
  
  public int b()
  {
    return d;
  }
}
