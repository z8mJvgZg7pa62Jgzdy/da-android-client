package com.deviantart.android.damobile.view;

import android.content.Context;
import android.support.design.widget.CollapsingToolbarLayout;
import android.util.AttributeSet;
import android.view.View;

public class DeviationFullViewCollapsingLayout
  extends CollapsingToolbarLayout
{
  private int paddingRight;
  
  public DeviationFullViewCollapsingLayout(Context paramContext)
  {
    super(paramContext);
  }
  
  public DeviationFullViewCollapsingLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public DeviationFullViewCollapsingLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    if (paddingRight != 0) {
      setMinimumHeight(paddingRight);
    }
  }
  
  public void setCollapsedViewHeight(int paramInt)
  {
    paddingRight = paramInt;
  }
}
