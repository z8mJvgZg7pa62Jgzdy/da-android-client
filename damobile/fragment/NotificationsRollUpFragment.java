package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.os.BaseBundle;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.cocosw.undobar.UndoBarController.UndoBar;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessageStack;
import com.deviantart.android.damobile.stream.FilteredStream;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.filter.NotificationsModelFilter;
import com.deviantart.android.damobile.stream.filter.StreamFilter;
import com.deviantart.android.damobile.stream.loader.APINotificationsStackLoader;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.BusStation.OnMCItemChangeEventBase;
import com.deviantart.android.damobile.util.BusStation.OnNotificationItemChangeEvent;
import com.deviantart.android.damobile.util.notifications.NotificationItemData;
import com.deviantart.android.damobile.util.notifications.NotificationItemType;
import com.deviantart.android.damobile.util.notifications.NotificationsPage;
import com.deviantart.android.damobile.util.opt.ItemDeleteHelperBase;
import com.deviantart.android.damobile.util.opt.ItemDeleteType;
import com.deviantart.android.damobile.util.opt.ItemDeleteUtils;
import com.deviantart.android.damobile.util.opt.NotificationItemDeleteHelper;
import com.deviantart.android.damobile.view.notifications.NotificationsListAdapter;
import com.deviantart.android.damobile.view.notifications.NotificationsListFrame;
import com.deviantart.android.damobile.view.opt.MCListAdapterBase;
import com.deviantart.android.damobile.view.opt.MCListFrameBase;
import com.deviantart.android.damobile.view.opt.MCListRecyclerView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class NotificationsRollUpFragment
  extends HomeFullViewFragment
{
  String c;
  NotificationItemType j;
  @Bind({2131689773})
  NotificationsListFrame notificationsListFrame;
  NotificationItemData w;
  int x = 0;
  
  public NotificationsRollUpFragment() {}
  
  private void b(NotificationsPage paramNotificationsPage, NotificationItemData paramNotificationItemData)
  {
    paramNotificationsPage = NotificationsPage.b(paramNotificationsPage);
    if (!StreamCacher.b(paramNotificationsPage)) {
      return;
    }
    paramNotificationsPage = new FilteredStream(StreamCacher.a(paramNotificationsPage), new StreamFilter[] { new NotificationsModelFilter() });
    int i = paramNotificationsPage.get().indexOf(paramNotificationItemData);
    if (i != -1)
    {
      paramNotificationItemData = (NotificationItemData)paramNotificationsPage.get(i);
      NotificationItemType localNotificationItemType = paramNotificationItemData.b();
      DVNTFeedbackMessageStack localDVNTFeedbackMessageStack = paramNotificationItemData.getString();
      int k = localDVNTFeedbackMessageStack.getCount().intValue();
      if (w != null) {
        localDVNTFeedbackMessageStack.updateFeedbackMessage(w.a());
      }
      k = Math.max(0, k - x);
      if (k == 0)
      {
        paramNotificationsPage.close(i);
        return;
      }
      if ((k == 1) && (localNotificationItemType != null) && (localNotificationItemType.e() != null)) {
        paramNotificationItemData.a(localNotificationItemType.e());
      }
      paramNotificationItemData.getString().setCount(Integer.valueOf(k));
    }
  }
  
  private NotificationItemDeleteHelper d()
  {
    return (NotificationItemDeleteHelper)ItemDeleteUtils.a(ItemDeleteType.b);
  }
  
  public static NotificationsRollUpFragment getItem(String paramString, NotificationItemType paramNotificationItemType)
  {
    NotificationsRollUpFragment localNotificationsRollUpFragment = new NotificationsRollUpFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("rollup_id", paramString);
    localBundle.putSerializable("notification_type", paramNotificationItemType);
    localNotificationsRollUpFragment.setArguments(localBundle);
    return localNotificationsRollUpFragment;
  }
  
  private void i()
  {
    if (x <= 0) {
      return;
    }
    Object localObject = new DVNTFeedbackMessageStack();
    ((DVNTFeedbackMessageStack)localObject).setStackId(c);
    localObject = new NotificationItemData(j, null, (DVNTFeedbackMessageStack)localObject);
    b(NotificationsPage.i, (NotificationItemData)localObject);
    b(j.getString(), (NotificationItemData)localObject);
  }
  
  public void b(BusStation.OnNotificationItemChangeEvent paramOnNotificationItemChangeEvent)
  {
    if (notificationsListFrame == null) {
      return;
    }
    d().a(notificationsListFrame, paramOnNotificationItemChangeEvent.c(), paramOnNotificationItemChangeEvent.b());
    if (paramOnNotificationItemChangeEvent.b()) {}
    for (x -= 1;; x += 1)
    {
      paramOnNotificationItemChangeEvent = notificationsListFrame.getRecyclerView();
      if (paramOnNotificationItemChangeEvent == null) {
        break;
      }
      paramOnNotificationItemChangeEvent = (NotificationsListAdapter)paramOnNotificationItemChangeEvent.getAdapter();
      if ((paramOnNotificationItemChangeEvent == null) || (paramOnNotificationItemChangeEvent.e() <= 0)) {
        break;
      }
      w = ((NotificationItemData)paramOnNotificationItemChangeEvent.get(0));
      return;
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = paramLayoutInflater.inflate(2130968675, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    c = getArguments().getString("rollup_id");
    j = ((NotificationItemType)getArguments().getSerializable("notification_type"));
    paramViewGroup = new APINotificationsStackLoader(c);
    if (!d().get().containsKey(c)) {
      d().get().put(c, new HashMap());
    }
    paramViewGroup.add(20);
    NotificationsFragment.a.add(paramViewGroup.b());
    paramViewGroup = new FilteredStream(StreamCacher.a(paramViewGroup), new StreamFilter[] { new NotificationsModelFilter() });
    notificationsListFrame.getRecyclerView().setAdapterSafe(new NotificationsListAdapter(paramViewGroup, c));
    if (paramViewGroup.size() <= 0) {
      notificationsListFrame.c(true);
    }
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    ButterKnife.unbind(this);
    super.onDestroyView();
  }
  
  public void onPause()
  {
    i();
    new UndoBarController.UndoBar(getActivity()).clear();
    super.onPause();
  }
}
