package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTFriendsResponse
  implements Serializable
{
  @SerializedName("results")
  private DVNTFriend.List friendsList;
  @SerializedName("has_more")
  private boolean hasMore;
  @SerializedName("next_offset")
  private Integer nextOffset;
  
  public DVNTFriendsResponse() {}
  
  public DVNTFriend.List getFriendsList()
  {
    return friendsList;
  }
  
  public boolean getHasMore()
  {
    return hasMore;
  }
  
  public Integer getNextOffset()
  {
    return nextOffset;
  }
  
  public boolean isHasMore()
  {
    return hasMore;
  }
  
  public void setFriendsList(DVNTFriend.List paramList)
  {
    friendsList = paramList;
  }
  
  public void setHasMore(boolean paramBoolean)
  {
    hasMore = paramBoolean;
  }
  
  public void setNextOffset(Integer paramInteger)
  {
    nextOffset = paramInteger;
  }
}
