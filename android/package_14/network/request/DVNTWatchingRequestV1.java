package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTWatchingRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTWatchingInfo>
{
  private String userName;
  
  public DVNTWatchingRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTWatchingInfo.class);
    userName = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTWatchingRequestV1 localDVNTWatchingRequestV1 = (DVNTWatchingRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(userName, userName).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(userName).toHashCode();
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTWatchingInfo sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).watching(paramString, userName);
  }
}
