package com.deviantart.android.damobile.util;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Environment;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.widget.Toast;
import com.deviantart.android.damobile.activity.SimpleImageActivity;
import com.deviantart.android.damobile.fragment.NoUIPhotoFragment;
import com.deviantart.android.damobile.fragment.NoUIPhotoFragment.InfoOnImageFileReadyListener;
import com.deviantart.android.damobile.fragment.PhotoFragment;
import com.facebook.drawee.view.SimpleDraweeView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.IOUtils;

public class PhotoUtils
{
  public static final Integer a = Integer.valueOf(500);
  public static final Integer b = Integer.valueOf(1500);
  public static final Integer type = Integer.valueOf(2000);
  
  public PhotoUtils() {}
  
  public static void a(Activity paramActivity, SimpleDraweeView paramSimpleDraweeView, boolean paramBoolean, String paramString, PhotoUtils.PictureType paramPictureType)
  {
    paramActivity = paramActivity.getFragmentManager();
    NoUIPhotoFragment localNoUIPhotoFragment = new NoUIPhotoFragment();
    paramActivity.beginTransaction().add(localNoUIPhotoFragment, "NO_UI_PHOTO_FRAGMENT").commitAllowingStateLoss();
    paramActivity.executePendingTransactions();
    localNoUIPhotoFragment.getClass();
    localNoUIPhotoFragment.b(paramBoolean, new NoUIPhotoFragment.InfoOnImageFileReadyListener(localNoUIPhotoFragment, paramSimpleDraweeView, paramString, paramPictureType));
  }
  
  public static void a(Context paramContext, PhotoUtils.PictureType paramPictureType, String paramString)
  {
    String str = null;
    switch (PhotoUtils.1.u[paramPictureType.ordinal()])
    {
    default: 
      paramPictureType = str;
      break;
    }
    while (paramString == null)
    {
      Log.e("launchSimpleImage", "User hasn't set the image.");
      return;
      str = paramContext.getString(2131231209);
      paramPictureType = str;
      if (UserUtils.a() != null)
      {
        paramContext.startActivity(SimpleImageActivity.a(paramContext, str, UserUtils.a()));
        return;
        str = paramContext.getString(2131231210);
        paramPictureType = str;
        if (UserUtils.d() != null)
        {
          paramContext.startActivity(SimpleImageActivity.a(paramContext, str, UserUtils.d()));
          return;
        }
      }
    }
    paramContext.startActivity(SimpleImageActivity.newIntent(paramContext, paramPictureType, paramString));
  }
  
  private static PermissionUtil.CheckResult checkSelfPermission(PhotoFragment paramPhotoFragment)
  {
    return PermissionUtil.checkPermission(paramPhotoFragment, "android.permission.WRITE_EXTERNAL_STORAGE", 0, 2131231199);
  }
  
  public static File doInBackground(File paramFile, ImageQualityType paramImageQualityType)
  {
    Object localObject2;
    int i;
    Object localObject1;
    int j;
    try
    {
      localObject2 = Uri.fromFile(paramFile);
      i = new ExifInterface(((Uri)localObject2).getPath()).getAttributeInt("Orientation", 1);
      localObject1 = PhotoUtils.1.a;
      j = paramImageQualityType.ordinal();
      switch (localObject1[j])
      {
      default: 
        return paramFile;
      }
    }
    catch (IOException paramImageQualityType)
    {
      Log.e("resizeImageFile", ((IOException)paramImageQualityType).toString());
      return paramFile;
    }
    if (i != 1)
    {
      paramImageQualityType = type;
      for (;;)
      {
        localObject1 = getBitmap(((Uri)localObject2).getPath(), paramImageQualityType.intValue(), paramImageQualityType.intValue());
        if (localObject1 != null) {
          break;
        }
        Log.e("resizeImageFile", "Got null bitmap");
        return paramFile;
        paramImageQualityType = b;
        continue;
        paramImageQualityType = a;
      }
      paramImageQualityType = (ImageQualityType)localObject1;
      switch (i)
      {
      default: 
        paramImageQualityType = (ImageQualityType)localObject1;
        break;
      }
      for (;;)
      {
        localObject3 = ((Uri)localObject2).getPathSegments();
        localObject1 = "";
        i = 0;
        for (;;)
        {
          j = ((List)localObject3).size();
          if (i >= j - 1) {
            break;
          }
          localObject1 = new StringBuilder().append((String)localObject1).append("/");
          Object localObject4 = ((List)localObject3).get(i);
          localObject4 = (String)localObject4;
          localObject1 = (String)localObject4;
          i += 1;
        }
        paramImageQualityType = rotateBitmap((Bitmap)localObject1, 90.0F);
        continue;
        paramImageQualityType = rotateBitmap((Bitmap)localObject1, 180.0F);
        continue;
        paramImageQualityType = rotateBitmap((Bitmap)localObject1, 270.0F);
      }
      localObject1 = (String)localObject1 + "/resized_" + ((Uri)localObject2).getLastPathSegment();
      localObject1 = new File((String)localObject1);
      localObject2 = new FileOutputStream((File)localObject1);
      Object localObject3 = Bitmap.CompressFormat.JPEG;
      paramImageQualityType.compress((Bitmap.CompressFormat)localObject3, 70, (OutputStream)localObject2);
      ((FileOutputStream)localObject2).flush();
      ((FileOutputStream)localObject2).close();
      paramImageQualityType.recycle();
      return localObject1;
    }
    return paramFile;
  }
  
  private static File download()
  {
    return downloadFile(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES));
  }
  
  private static File downloadFile(File paramFile)
  {
    String str = "" + new Date().getTime();
    return File.createTempFile("JPEG_" + str + "_", ".jpg", paramFile);
  }
  
  public static Bitmap getBitmap(String paramString, int paramInt1, int paramInt2)
  {
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    inJustDecodeBounds = true;
    BitmapFactory.decodeFile(paramString, localOptions);
    inSampleSize = getScale(localOptions, paramInt1, paramInt2);
    inJustDecodeBounds = false;
    return BitmapFactory.decodeFile(paramString, localOptions);
  }
  
  public static File getBitmap(ContentResolver paramContentResolver, Uri paramUri)
  {
    File localFile = download();
    IOUtils.copy(paramContentResolver.openInputStream(paramUri), new FileOutputStream(localFile));
    return localFile;
  }
  
  public static int getScale(BitmapFactory.Options paramOptions, int paramInt1, int paramInt2)
  {
    int k = outHeight;
    int j = outWidth;
    int i = 1;
    while ((k > paramInt2) || (j > paramInt1))
    {
      k /= 2;
      j /= 2;
      i *= 2;
    }
    return i;
  }
  
  private static boolean isEnabled(Context paramContext)
  {
    if (Build.VERSION.SDK_INT < 17) {
      return (paramContext.getPackageManager().hasSystemFeature("android.hardware.camera")) || (paramContext.getPackageManager().hasSystemFeature("android.hardware.camera.front"));
    }
    return paramContext.getPackageManager().hasSystemFeature("android.hardware.camera.any");
  }
  
  public static void onActivityResult(PhotoFragment paramPhotoFragment)
  {
    if (onRequestPermissionsResult(paramPhotoFragment) != PermissionUtil.CheckResult.mFileManager) {
      return;
    }
    paramPhotoFragment.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 103);
  }
  
  public static void onOptionsItemSelected(PhotoFragment paramPhotoFragment)
  {
    Activity localActivity = paramPhotoFragment.getActivity();
    Intent localIntent = new Intent("android.media.action.IMAGE_CAPTURE");
    File localFile = null;
    if (localIntent.resolveActivity(localActivity.getPackageManager()) != null) {
      if (checkSelfPermission(paramPhotoFragment) != PermissionUtil.CheckResult.mFileManager) {
        return;
      }
    }
    try
    {
      localFile = download();
      if (localFile != null)
      {
        localIntent.putExtra("output", Uri.fromFile(localFile));
        paramPhotoFragment.a(localFile);
        paramPhotoFragment.startActivityForResult(localIntent, 102);
        return;
      }
    }
    catch (IOException localIOException)
    {
      Toast.makeText(localActivity, paramPhotoFragment.getString(2131230944), 0).show();
    }
  }
  
  private static PermissionUtil.CheckResult onRequestPermissionsResult(PhotoFragment paramPhotoFragment)
  {
    if (Build.VERSION.SDK_INT < 16) {}
    for (String str = "android.permission.WRITE_EXTERNAL_STORAGE";; str = "android.permission.READ_EXTERNAL_STORAGE") {
      return PermissionUtil.checkPermission(paramPhotoFragment, str, 1, 2131231203);
    }
  }
  
  private static int openCamera(Context paramContext)
  {
    if (Build.VERSION.SDK_INT < 21) {
      return Camera.getNumberOfCameras();
    }
    return ((CameraManager)paramContext.getSystemService("camera")).getCameraIdList().length;
  }
  
  public static Bitmap rotateBitmap(Bitmap paramBitmap, float paramFloat)
  {
    Matrix localMatrix = new Matrix();
    localMatrix.postRotate(paramFloat);
    return Bitmap.createBitmap(paramBitmap, 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight(), localMatrix, true);
  }
}
