package com.deviantart.android.damobile.stream;

import android.content.Context;
import com.deviantart.android.damobile.stream.filter.StreamFilter;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class FilteredStream<T>
  extends Stream<T>
{
  final List<StreamFilter<T>> a;
  final Stream<T> b;
  int l = 0;
  
  public FilteredStream(Stream paramStream, StreamFilter... paramVarArgs)
  {
    a = new ArrayList(Arrays.asList(paramVarArgs));
    b = paramStream;
    b();
  }
  
  private void b(Collection paramCollection)
  {
    paramCollection = paramCollection.iterator();
    while (paramCollection.hasNext()) {
      f(paramCollection.next());
    }
  }
  
  private boolean b(Object paramObject)
  {
    Iterator localIterator = a.iterator();
    while (localIterator.hasNext()) {
      if (((StreamFilter)localIterator.next()).b(paramObject)) {
        return true;
      }
    }
    return false;
  }
  
  private void close(int paramInt, Object paramObject)
  {
    if (!b(paramObject))
    {
      if (a.contains(paramObject)) {
        return;
      }
      a.add(paramInt, paramObject);
    }
  }
  
  private void f(Object paramObject)
  {
    if (!b(paramObject))
    {
      if (a.contains(paramObject)) {
        return;
      }
      a.add(paramObject);
    }
  }
  
  public void a()
  {
    b.a();
    a.clear();
    l = 0;
  }
  
  public void a(int paramInt)
  {
    b.a(paramInt);
  }
  
  public void a(int paramInt, Object paramObject)
  {
    b.a(paramInt, paramObject);
    close(paramInt, paramObject);
  }
  
  public void a(Context paramContext, Stream.Notifiable paramNotifiable, boolean paramBoolean1, boolean paramBoolean2)
  {
    paramNotifiable = new FilteredStream.1(this, paramNotifiable);
    if (paramBoolean1) {
      a();
    }
    b.a(paramContext, paramNotifiable, paramBoolean1, paramBoolean2);
  }
  
  public void a(Collection paramCollection)
  {
    b.a(paramCollection);
    b(paramCollection);
  }
  
  public void a(boolean paramBoolean)
  {
    b.a(paramBoolean);
  }
  
  public void b()
  {
    ArrayList localArrayList = b.get();
    int i = localArrayList.size();
    if (i > 0)
    {
      if (l >= i) {
        return;
      }
      b(localArrayList.subList(l, i));
      l = i;
    }
  }
  
  public void c()
  {
    b.c();
  }
  
  public Object close(int paramInt)
  {
    Object localObject = super.close(paramInt);
    b.remove(localObject);
    return localObject;
  }
  
  public void close()
  {
    b.close();
  }
  
  public void close(Object paramObject)
  {
    b.close(paramObject);
    f(paramObject);
  }
  
  public void close(boolean paramBoolean)
  {
    b.close(paramBoolean);
  }
  
  public boolean d()
  {
    return b.d();
  }
  
  public int e()
  {
    return b.e();
  }
  
  public boolean equals()
  {
    return b.equals();
  }
  
  public long getValue()
  {
    return b.getValue();
  }
  
  public boolean iterator()
  {
    return b.iterator();
  }
  
  public StreamLoader length()
  {
    return b.length();
  }
  
  public void next()
  {
    b.next();
  }
  
  public boolean read()
  {
    return b.read();
  }
}
