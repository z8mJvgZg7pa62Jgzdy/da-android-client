package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.android.utils.DVNTExpandableArgumentBuilder;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTUserProfileRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTUserProfile>
{
  private String expandArguments;
  private String userName;
  
  public DVNTUserProfileRequestV1(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTUserProfile.class);
    userName = paramString;
    expandArguments = new DVNTExpandableArgumentBuilder().loadInfo(DVNTExpandUserArgument.EXPAND_USER_DETAILS, paramBoolean1).loadInfo(DVNTExpandUserArgument.EXPAND_USER_STATS, paramBoolean2).build();
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTUserProfileRequestV1 localDVNTUserProfileRequestV1 = (DVNTUserProfileRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(userName, userName).append(expandArguments, expandArguments).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "userprofile" + userName + "/" + expandArguments;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 300000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(userName).append(expandArguments).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return (userName == null) || (userName.length() <= 0);
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTUserProfile sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).userProfile(paramString, userName, expandArguments, Boolean.valueOf(true));
  }
}
