package com.deviantart.android.damobile.util;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation.DVNTDeviationBaseStats;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import java.util.ArrayList;

public class DeviationCompositeModel
{
  private DeviationCompositeModel.FaveListener d;
  private DVNTDeviation e;
  private DeviationCompositeModel.MetadataDeferred j;
  
  public DeviationCompositeModel(DVNTDeviation paramDVNTDeviation)
  {
    e = paramDVNTDeviation;
  }
  
  private void b(Context paramContext)
  {
    j = new DeviationCompositeModel.1(this);
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(e.getId());
    DVNTCommonAsyncAPI.deviationMetadata(localArrayList, Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(true), Boolean.valueOf(false)).call(paramContext, j);
  }
  
  public DeviationCompositeModel.MetadataDeferred a(Context paramContext)
  {
    if (j == null) {
      b(paramContext);
    }
    return j;
  }
  
  public void a()
  {
    e.getStats().setComments(Integer.valueOf(e.getStats().getComments().intValue() + 1));
  }
  
  public void b()
  {
    e.getStats().setFavourites(Integer.valueOf(e.getStats().getFavourites().intValue() - 1));
  }
  
  public void b(DeviationCompositeModel.FaveListener paramFaveListener)
  {
    d = paramFaveListener;
  }
  
  public void b(boolean paramBoolean)
  {
    e.setFavourited(Boolean.valueOf(paramBoolean));
    if (paramBoolean) {
      e();
    }
    while (d != null)
    {
      d.onMediaChanged();
      return;
      b();
    }
  }
  
  public void e()
  {
    e.getStats().setFavourites(Integer.valueOf(e.getStats().getFavourites().intValue() + 1));
  }
  
  public DVNTDeviation getItem()
  {
    return e;
  }
}
