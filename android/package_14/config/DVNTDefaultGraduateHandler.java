package com.deviantart.android.android.package_14.config;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthHelper;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthPreferencesManager;
import com.deviantart.android.sdk.api.config.DVNTDefaultGraduateHandler.DVNTDefaultOAuthActivity;

public class DVNTDefaultGraduateHandler
  extends DVNTGraduateHandler
{
  private static DVNTDefaultGraduateHandler handlerInstance;
  private final DVNTAPIConfig apiConfig;
  
  public DVNTDefaultGraduateHandler(DVNTAPIConfig paramDVNTAPIConfig)
  {
    handlerInstance = this;
    apiConfig = paramDVNTAPIConfig;
  }
  
  private Intent createOAuthActivityIntent(Context paramContext)
  {
    paramContext = new Intent(paramContext, DVNTDefaultGraduateHandler.DVNTDefaultOAuthActivity.class);
    paramContext.putExtra("authorizeURL", oAuthHelper.buildAuthorizationURL(apiConfig));
    return paramContext;
  }
  
  public void graduate(Context paramContext)
  {
    oAuthPreferencesManager.clearStoredTokenAndCookies(paramContext);
    paramContext.startActivity(createOAuthActivityIntent(paramContext));
  }
  
  public void onGraduateCancelled(Context paramContext) {}
  
  public void onGraduationFail(Context paramContext, String paramString)
  {
    Toast.makeText(paramContext, "could not log in, please try again later", 0).show();
  }
  
  public void onGraduationSuccess(Context paramContext) {}
}
