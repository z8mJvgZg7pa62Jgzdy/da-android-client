package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTResendVerificationEmailRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  String newEmail;
  String password;
  
  public DVNTResendVerificationEmailRequestV1(String paramString1, String paramString2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    newEmail = paramString1;
    password = paramString2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTResendVerificationEmailRequestV1 localDVNTResendVerificationEmailRequestV1 = (DVNTResendVerificationEmailRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(newEmail, newEmail).append(password, password).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(newEmail).append(password).toHashCode();
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).resendVerificationEmail(paramString, newEmail, password);
  }
}
