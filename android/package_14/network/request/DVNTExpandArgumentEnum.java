package com.deviantart.android.android.package_14.network.request;

public abstract interface DVNTExpandArgumentEnum
{
  public abstract String getExpandableKey();
}
