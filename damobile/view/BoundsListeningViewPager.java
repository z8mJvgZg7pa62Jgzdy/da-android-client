package com.deviantart.android.damobile.view;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

public abstract class BoundsListeningViewPager
  extends ViewPager
{
  private int items = 3;
  protected boolean loading = false;
  
  public BoundsListeningViewPager(Context paramContext)
  {
    super(paramContext);
  }
  
  public BoundsListeningViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public abstract boolean flagActionItems();
  
  public abstract boolean load();
  
  public void onPostExecute()
  {
    if (loading) {
      return;
    }
    if (getCurrentItem() < items)
    {
      loading = flagActionItems();
      return;
    }
    if (getCurrentItem() >= getAdapter().getCount() - 1 - items) {
      loading = load();
    }
  }
  
  public void setLoading(boolean paramBoolean)
  {
    loading = paramBoolean;
  }
  
  public void setThreshold(int paramInt)
  {
    items = paramInt;
  }
}
