package com.deviantart.android.damobile.util.submit.pages;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTCategory;
import com.deviantart.android.damobile.fragment.SubmitCategoryFragment;
import com.deviantart.android.damobile.fragment.SubmitMention;
import com.deviantart.android.damobile.fragment.SubmitTag;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.Keyboard;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.mainpager.DAMainPage;
import com.deviantart.android.damobile.util.submit.Submission;
import com.deviantart.android.damobile.util.submit.SubmitType;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class SubmitPage
  extends DAMainPage
{
  private Context c;
  @Bind({2131690037})
  protected View categoryContainer;
  @Bind({2131690038})
  TextView categoryTitle;
  @Bind({2131690039})
  TextView categoryView;
  @Bind({2131690040})
  protected View matureContentContainer;
  @Bind({2131690041})
  SwitchCompat matureContentSwitch;
  @Bind({2131690006})
  HorizontalScrollView mentionsScrollView;
  private String pageTitle;
  @Bind({2131690015})
  protected ImageView photoPlaceholder;
  @Bind({2131690007})
  LinearLayout scrollableMentions;
  @Bind({2131690009})
  LinearLayout scrollableTags;
  @Bind({2131690013})
  protected EditText submitDescription;
  @Bind({2131690012})
  protected TextView submitDescriptionLabel;
  @Bind({2131690045})
  protected EditText submitText;
  @Bind({2131690043})
  protected TextView submitTextLabel;
  @Bind({2131690060})
  protected EditText submitTitle;
  @Bind({2131690004})
  protected View submitTitleContainer;
  @Bind({2131690058})
  protected TextView submitTitleLabel;
  @Bind({2131690008})
  HorizontalScrollView tagsScrollView;
  @Bind({2131690042})
  protected View termsOfUse;
  
  public SubmitPage(String paramString)
  {
    pageTitle = paramString;
  }
  
  public void b()
  {
    ButterKnife.unbind(this);
    c = null;
  }
  
  protected void b(ViewGroup paramViewGroup)
  {
    c = paramViewGroup.getContext();
  }
  
  public abstract SubmitType c();
  
  public String formatBytes()
  {
    return getContext().getString(2131231355);
  }
  
  public boolean g()
  {
    return false;
  }
  
  protected Activity getContext()
  {
    return (Activity)c;
  }
  
  public CharSequence getPageTitle()
  {
    return pageTitle;
  }
  
  public abstract SimpleDraweeView getString();
  
  public boolean isDigit()
  {
    return true;
  }
  
  public boolean m()
  {
    return true;
  }
  
  public boolean mkdirs()
  {
    return true;
  }
  
  public void onBindViewHolder()
  {
    Submission localSubmission = Submission.getInstance();
    if ((submitTitle != null) && (localSubmission.getCategory() != null) && (!localSubmission.getCategory().isEmpty())) {
      submitTitle.setText(localSubmission.getCategory());
    }
    if ((submitDescription != null) && (localSubmission.getName() != null) && (!localSubmission.getName().isEmpty())) {
      submitDescription.setText(localSubmission.getName());
    }
    if ((submitText != null) && (localSubmission.getUrl() != null) && (!localSubmission.getUrl().isEmpty())) {
      submitText.setText(localSubmission.getUrl());
    }
    if (localSubmission.size() != null)
    {
      if (getString() != null)
      {
        getString().setVisibility(0);
        ImageUtils.isEmpty(getString(), Uri.fromFile(localSubmission.size()));
      }
      if (photoPlaceholder != null) {
        photoPlaceholder.setVisibility(8);
      }
      if (localSubmission.getMessage() == null) {
        break label332;
      }
      if (categoryTitle != null) {
        categoryTitle.setText(formatBytes());
      }
      if (categoryView != null)
      {
        categoryView.setText(localSubmission.getMessage().getCatPath());
        categoryView.setVisibility(0);
      }
    }
    label215:
    Iterator localIterator;
    View localView;
    for (;;)
    {
      if ((scrollableTags != null) && (tagsScrollView != null))
      {
        if ((localSubmission.getKey() == null) || (localSubmission.getKey().isEmpty())) {
          break label465;
        }
        scrollableTags.removeAllViews();
        localIterator = localSubmission.getKey().iterator();
        for (;;)
        {
          if (localIterator.hasNext())
          {
            localView = ((SubmitTag)localIterator.next()).getView(getContext());
            scrollableTags.addView(localView);
            continue;
            if (getString() != null) {
              getString().setVisibility(8);
            }
            if (photoPlaceholder == null) {
              break;
            }
            photoPlaceholder.setVisibility(0);
            break;
            label332:
            if (categoryTitle != null) {
              categoryTitle.setText(getContext().getString(2131231357));
            }
            if (categoryView == null) {
              break label215;
            }
            categoryView.setVisibility(8);
            break label215;
          }
        }
        tagsScrollView.setVisibility(0);
      }
    }
    while ((scrollableMentions != null) && (mentionsScrollView != null))
    {
      if ((localSubmission.get() == null) || (localSubmission.get().isEmpty())) {
        break label514;
      }
      scrollableMentions.removeAllViews();
      localIterator = localSubmission.get().iterator();
      for (;;)
      {
        if (localIterator.hasNext())
        {
          localView = ((SubmitMention)localIterator.next()).getView(getContext());
          scrollableMentions.addView(localView);
          continue;
          label465:
          tagsScrollView.setVisibility(8);
          break;
        }
      }
      mentionsScrollView.setVisibility(0);
    }
    while ((matureContentSwitch != null) && (localSubmission.isHidden() != null))
    {
      matureContentSwitch.setChecked(localSubmission.isHidden().booleanValue());
      return;
      label514:
      mentionsScrollView.setVisibility(8);
    }
  }
  
  public void onClickSelectCategory()
  {
    Submission localSubmission = Submission.getInstance();
    if ((!localSubmission.add()) && (!localSubmission.equals()))
    {
      Toast.makeText(getContext(), 2131231251, 0).show();
      return;
    }
    Keyboard.hideKeyboard(getContext());
    ScreenFlowManager.a(getContext(), SubmitCategoryFragment.showConfirmDialog(localSubmission.getString(), localSubmission.doInBackground()), "submit_category");
  }
  
  public void onClickTermsOfUse()
  {
    NavigationUtils.openBrowser(getContext(), getContext().getString(2131231369));
  }
  
  public void onDescriptionChanged(CharSequence paramCharSequence)
  {
    if (paramCharSequence.toString().isEmpty()) {
      submitDescriptionLabel.setVisibility(8);
    }
    for (;;)
    {
      Submission localSubmission = Submission.getInstance();
      localSubmission.setLanguage(paramCharSequence.toString());
      localSubmission.setTitle();
      return;
      submitDescriptionLabel.setVisibility(0);
    }
  }
  
  public void onMatureContentCheckedChanged(boolean paramBoolean)
  {
    Submission.getInstance().setHidden(Boolean.valueOf(paramBoolean));
  }
  
  public void onTextChanged(CharSequence paramCharSequence)
  {
    if (paramCharSequence.toString().isEmpty()) {
      submitTextLabel.setVisibility(8);
    }
    for (;;)
    {
      Submission localSubmission = Submission.getInstance();
      localSubmission.setUrl(paramCharSequence.toString());
      localSubmission.setTitle();
      return;
      if (submitText.getHint().equals(submitTextLabel.getText())) {
        submitTextLabel.setVisibility(0);
      }
    }
  }
  
  public void onTitleChanged(CharSequence paramCharSequence)
  {
    if (paramCharSequence.toString().isEmpty()) {
      submitTitleLabel.setVisibility(8);
    }
    for (;;)
    {
      if (!Submission.add(paramCharSequence.toString())) {
        submitTitle.setError(getContext().getString(2131230970));
      }
      Submission localSubmission = Submission.getInstance();
      localSubmission.setCategory(paramCharSequence.toString());
      localSubmission.setTitle();
      return;
      submitTitleLabel.setVisibility(0);
    }
  }
  
  public boolean setContentHeight()
  {
    return true;
  }
}
