package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.damobile.util.notes.NotesItemData;

public class APINotesFolderLoader
  extends StreamLoader<NotesItemData>
{
  private String d;
  
  public APINotesFolderLoader(String paramString)
  {
    d = paramString;
    add(20);
  }
  
  public String b()
  {
    return "notesfolder|" + d;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTAsyncAPI.notesInFolder(d, Integer.valueOf(paramInt), Integer.valueOf(a()));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APINotesFolderLoader.1(this, paramStreamLoadListener));
  }
}
