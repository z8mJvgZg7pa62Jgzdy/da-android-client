package com.deviantart.android.damobile.view.ewok;

import android.app.Activity;
import android.view.View;

public abstract interface RepostEwok
{
  public abstract View onCreateView(Activity paramActivity);
}
