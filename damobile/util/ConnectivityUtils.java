package com.deviantart.android.damobile.util;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.deviantart.android.android.utils.DVNTContextUtils;

public class ConnectivityUtils
{
  private static boolean b = false;
  private static boolean c = false;
  
  public ConnectivityUtils() {}
  
  public static NetworkInfo getNetworkInfo(Context paramContext)
  {
    return ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
  }
  
  public static boolean isConnected(Context paramContext)
  {
    paramContext = getNetworkInfo(paramContext);
    return (paramContext != null) && (paramContext.isConnected());
  }
  
  public static boolean isWifiConnected(Context paramContext)
  {
    paramContext = getNetworkInfo(paramContext);
    return (paramContext != null) && (paramContext.isConnected()) && (paramContext.getType() == 1);
  }
  
  public static void showErrorDialog(Context paramContext)
  {
    if ((!DVNTContextUtils.isContextDead(paramContext)) && (!isConnected(paramContext)) && (!b))
    {
      if (c) {
        return;
      }
      c = true;
      new AlertDialog.Builder(paramContext).setPositiveButton(2131230931, new ConnectivityUtils.3(paramContext)).setNegativeButton(2131230929, new ConnectivityUtils.2()).setOnCancelListener(new ConnectivityUtils.1()).setTitle(2131230932).setMessage(2131230930).setIcon(17301543).create().show();
    }
  }
}
