package com.deviantart.android.damobile.util;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import com.deviantart.android.android.utils.DVNTContextUtils;

public class ErrorUtils
{
  public ErrorUtils() {}
  
  public static void showMessage(Activity paramActivity, int paramInt1, int paramInt2)
  {
    if (DVNTContextUtils.isContextDead(paramActivity)) {
      return;
    }
    new AlertDialog.Builder(paramActivity).setTitle(paramInt1).setMessage(paramInt2).setCancelable(false).setPositiveButton(2131231183, new ErrorUtils.1(paramActivity)).show();
  }
}
