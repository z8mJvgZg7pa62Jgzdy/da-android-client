package com.deviantart.android.android.package_14.model;

import com.deviantart.android.sdk.api.model.DVNTCollection.List;
import com.deviantart.android.sdk.api.model.DVNTPaginatedResult;
import java.io.Serializable;

public class DVNTPaginatedCollectionFolders
  extends DVNTPaginatedResult<DVNTCollection.List>
  implements Serializable
{
  public DVNTPaginatedCollectionFolders() {}
}
