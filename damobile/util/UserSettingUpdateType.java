package com.deviantart.android.damobile.util;

public enum UserSettingUpdateType
{
  static
  {
    c = new UserSettingUpdateType("AVATAR_UPDATED", 1);
    g = new UserSettingUpdateType("PROFILE_PICTURE_UPDATED", 2);
    l = new UserSettingUpdateType("RC_OPTION_CHANGED", 3);
  }
}
