package com.deviantart.android.damobile.util;

import android.content.Context;
import android.graphics.Typeface;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

public enum DAFont
{
  private String levels;
  
  static
  {
    b = new DAFont("CALIBRE_SEMIBOLD", 4, "fonts/Calibre-Semibold.otf");
  }
  
  private DAFont(String paramString)
  {
    levels = paramString;
  }
  
  public Typeface get(Context paramContext)
  {
    return TypefaceUtils.load(paramContext.getAssets(), levels);
  }
}
