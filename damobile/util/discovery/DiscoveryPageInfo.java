package com.deviantart.android.damobile.util.discovery;

import android.content.Context;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;

public enum DiscoveryPageInfo
{
  private EventKeys.Category j;
  private int k;
  private String m;
  private String mString;
  private String preferencesName;
  
  static
  {
    a = new DiscoveryPageInfo("WHATS_HOT", 1, "whats_hot", 2131231440, null, "WhatsHot", "hot");
    b = new DiscoveryPageInfo("DAILY_DEVIATIONS", 2, "daily_deviations", 2131230846, null, "DailyDeviations", "dailydeviations");
    l = new DiscoveryPageInfo("CURATED_TAGS", 3, "explore", 2131230996, EventKeys.Category.u, "Explore");
    o = new DiscoveryPageInfo("BROWSE_CATEGORIES", 4, "browse", 2131230821, null, "Browse");
  }
  
  private DiscoveryPageInfo(String paramString1, int paramInt, EventKeys.Category paramCategory, String paramString2)
  {
    mString = paramString1;
    k = paramInt;
    j = paramCategory;
    m = paramString2;
  }
  
  private DiscoveryPageInfo(String paramString1, int paramInt, EventKeys.Category paramCategory, String paramString2, String paramString3)
  {
    this(paramString1, paramInt, paramCategory, paramString2);
    preferencesName = paramString3;
  }
  
  public static DiscoveryPageInfo b(int paramInt)
  {
    if (paramInt >= values().length) {
      return null;
    }
    return values()[paramInt];
  }
  
  public static DiscoveryPageInfo getString(String paramString)
  {
    DiscoveryPageInfo[] arrayOfDiscoveryPageInfo = values();
    int n = arrayOfDiscoveryPageInfo.length;
    int i = 0;
    while (i < n)
    {
      DiscoveryPageInfo localDiscoveryPageInfo = arrayOfDiscoveryPageInfo[i];
      if (localDiscoveryPageInfo.get() != null) {}
      for (String str = localDiscoveryPageInfo.get(); str.equals(paramString); str = localDiscoveryPageInfo.getString()) {
        return localDiscoveryPageInfo;
      }
      i += 1;
    }
    return null;
  }
  
  public EventKeys.Category a()
  {
    return j;
  }
  
  public String b()
  {
    return m;
  }
  
  public String get()
  {
    return preferencesName;
  }
  
  public String get(Context paramContext)
  {
    return paramContext.getString(k);
  }
  
  public String getString()
  {
    return mString;
  }
}
