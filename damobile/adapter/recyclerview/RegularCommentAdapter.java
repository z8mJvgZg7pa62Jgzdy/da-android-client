package com.deviantart.android.damobile.adapter.recyclerview;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.CommentType;
import com.deviantart.android.damobile.util.threaditem.CommentItem;
import com.deviantart.android.damobile.view.thirdparty.comment.viewholder.CommentViewHolderBase;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RegularCommentAdapter
  extends BaseCommentAdapter<com.deviantart.android.sdk.api.model.DVNTComment>
{
  private RegularCommentAdapter(StreamLoader paramStreamLoader, CommentType paramCommentType, String paramString1, String paramString2)
  {
    super(paramStreamLoader, paramCommentType, paramString1, paramString2);
  }
  
  public int a(List paramList)
  {
    paramList = paramList.iterator();
    int i = 0;
    while (paramList.hasNext())
    {
      Object localObject = (com.deviantart.android.android.package_14.model.DVNTComment)paramList.next();
      if (!b((com.deviantart.android.android.package_14.model.DVNTComment)localObject))
      {
        if (!get().equals()) {}
        for (boolean bool = true;; bool = false)
        {
          localObject = new CommentItem((com.deviantart.android.android.package_14.model.DVNTComment)localObject, bool);
          c.add(localObject);
          i += 1;
          break;
        }
      }
    }
    return i;
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    super.b(paramViewHolder, paramInt);
    paramViewHolder = (CommentViewHolderBase)paramViewHolder;
    loadMoreSiblingsTop.setVisibility(8);
    loadMoreSiblingsBottom.setVisibility(8);
  }
}
