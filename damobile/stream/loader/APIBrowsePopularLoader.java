package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTDeviation;

public class APIBrowsePopularLoader
  extends StreamLoader<DVNTDeviation>
{
  String b;
  Boolean c;
  String d;
  String e;
  
  public APIBrowsePopularLoader(String paramString1, String paramString2, String paramString3, Boolean paramBoolean)
  {
    d = paramString1;
    e = paramString2;
    c = paramBoolean;
    b = paramString3;
  }
  
  public String b()
  {
    return "browsepopularloader" + d + "|" + e + "|" + c + "|" + b;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.browsePopular(d, e, b, c, Integer.valueOf(paramInt), Integer.valueOf(a));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APIBrowsePopularLoader.1(this, paramStreamLoadListener));
  }
}
