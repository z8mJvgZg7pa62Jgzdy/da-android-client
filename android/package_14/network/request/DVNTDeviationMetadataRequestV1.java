package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.android.package_14.network.wrapper.DVNTMetadataWrapper;
import com.deviantart.android.sdk.api.network.request.GroupableRequest;
import com.google.common.reflect.TypeToken;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTDeviationMetadataRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTDeviationMetadata.List>
  implements GroupableRequest
{
  private final List<String> deviationIds;
  private final Boolean extCamera;
  private final Boolean extCollection;
  private final Boolean extStats;
  private final Boolean extSubmission;
  
  public DVNTDeviationMetadataRequestV1(List paramList, Boolean paramBoolean1, Boolean paramBoolean2, Boolean paramBoolean3, Boolean paramBoolean4)
  {
    super(com.deviantart.android.sdk.api.model.DVNTDeviationMetadata.List.class);
    deviationIds = paramList;
    extSubmission = paramBoolean1;
    extCamera = paramBoolean2;
    extStats = paramBoolean3;
    extCollection = paramBoolean4;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTDeviationMetadataRequestV1 localDVNTDeviationMetadataRequestV1 = (DVNTDeviationMetadataRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(deviationIds, deviationIds).append(extSubmission, extSubmission).append(extCamera, extCamera).append(extStats, extStats).append(extCollection, extCollection).isEquals();
    }
    return false;
  }
  
  public HashMap getGETArguments()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("deviationids", deviationIds);
    localHashMap.put("ext_submission", extSubmission);
    localHashMap.put("ext_camera", extCamera);
    localHashMap.put("ext_stats", extStats);
    localHashMap.put("ext_collection", extCollection);
    return localHashMap;
  }
  
  public String getGroupEndpoint()
  {
    return "/api/v1/oauth2/deviation/metadata";
  }
  
  public HashMap getPOSTArguments()
  {
    return null;
  }
  
  public TypeToken getWrapperTypeToken()
  {
    return new DVNTDeviationMetadataRequestV1.1(this);
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(deviationIds).append(extSubmission).append(extCamera).append(extStats).append(extCollection).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTDeviationMetadata.List sendRequest(String paramString)
  {
    return (com.deviantart.android.android.package_14.model.DVNTDeviationMetadata.List)((DVNTServiceV1)getService()).deviationMetadata(paramString, deviationIds, extSubmission, extCamera, extStats, extCollection, Boolean.valueOf(true)).getResults();
  }
}
