package com.deviantart.android.damobile.util;

import android.content.Context;
import android.content.res.Resources;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.package_14.model.DVNTUser.List;
import java.util.ArrayList;
import java.util.Iterator;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;

public class UserDisplay
{
  public UserDisplay() {}
  
  public static Spannable a(Context paramContext, DVNTUser paramDVNTUser, boolean paramBoolean)
  {
    SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(paramDVNTUser.getUserName());
    localSpannableStringBuilder.setSpan(new CalligraphyTypefaceSpan(DAFont.o.get(paramContext)), 0, localSpannableStringBuilder.length(), 33);
    paramDVNTUser = MemberType.toString(paramDVNTUser.getType());
    if (paramDVNTUser == null) {
      return localSpannableStringBuilder;
    }
    localSpannableStringBuilder.append(a(paramContext, paramDVNTUser, paramBoolean));
    if (paramDVNTUser == MemberType.l) {
      localSpannableStringBuilder.setSpan(new StrikethroughSpan(), 0, localSpannableStringBuilder.length(), 33);
    }
    return localSpannableStringBuilder;
  }
  
  public static Spannable a(Context paramContext, MemberType paramMemberType, boolean paramBoolean)
  {
    Resources localResources = paramContext.getResources();
    if ((paramBoolean) && (paramMemberType.a() != null)) {}
    SpannableString localSpannableString;
    for (paramMemberType = paramMemberType.a();; paramMemberType = paramMemberType.q())
    {
      localSpannableString = new SpannableString(paramContext.getResources().getText(paramMemberType.c()));
      localSpannableString.setSpan(new CalligraphyTypefaceSpan(DAFont.d.get(paramContext)), 0, localSpannableString.length(), 33);
      if ((paramMemberType.b() == null) || (paramMemberType.get() != null)) {
        break;
      }
      localSpannableString.setSpan(new ForegroundColorSpan(localResources.getColor(paramMemberType.b().intValue())), 0, localSpannableString.length(), 33);
      return localSpannableString;
    }
    if (paramMemberType.get() != null) {
      localSpannableString.setSpan(new TextOverlaySpan(localResources.getText(paramMemberType.get().intValue()), Integer.valueOf(localResources.getColor(paramMemberType.getValue().intValue())), Integer.valueOf(localResources.getColor(paramMemberType.b().intValue()))), 0, localSpannableString.length(), 33);
    }
    return localSpannableString;
  }
  
  public static Spannable add(Context paramContext, DVNTUser paramDVNTUser)
  {
    return a(paramContext, paramDVNTUser, false);
  }
  
  public static Spannable d(Context paramContext, MemberType paramMemberType)
  {
    return a(paramContext, paramMemberType, false);
  }
  
  public static Spannable getText(Context paramContext, DVNTUser.List paramList)
  {
    SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder("");
    paramList = paramList.iterator();
    int i = 0;
    while (paramList.hasNext())
    {
      DVNTUser localDVNTUser = (DVNTUser)paramList.next();
      if (i > 0) {
        localSpannableStringBuilder.append(", ");
      }
      localSpannableStringBuilder.append(add(paramContext, localDVNTUser));
      i += 1;
    }
    return localSpannableStringBuilder;
  }
}
