package com.deviantart.android.damobile.view.thirdparty.comment.viewholder;

import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;

public class HiddenCommentViewHolder
  extends CommentViewHolderBase
{
  @Bind({2131689622})
  public TextView commentHidden;
  
  public HiddenCommentViewHolder(View paramView)
  {
    super(paramView);
    ButterKnife.bind(this, paramView);
  }
}
