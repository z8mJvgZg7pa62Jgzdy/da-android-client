package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.sdk.api.network.request.GroupableRequest;
import com.google.common.reflect.TypeToken;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.HashMap;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTGalleryFoldersRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTPaginatedGalleryFolders>
  implements GroupableRequest
{
  private boolean calculateSize;
  private boolean getPreload;
  private Integer limit;
  private Integer offset;
  private String userName;
  
  public DVNTGalleryFoldersRequestV1(String paramString, boolean paramBoolean1, boolean paramBoolean2, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTPaginatedGalleryFolders.class);
    userName = paramString;
    calculateSize = paramBoolean1;
    getPreload = paramBoolean2;
    offset = paramInteger1;
    limit = paramInteger2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTGalleryFoldersRequestV1 localDVNTGalleryFoldersRequestV1 = (DVNTGalleryFoldersRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(userName, userName).append(calculateSize, calculateSize).append(getPreload, getPreload).append(offset, offset).append(limit, limit).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "gallery" + userName + "_calcsize_" + calculateSize + "_getpreload_" + getPreload + "_offset_" + offset + "_limit_" + limit;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 120000L;
  }
  
  public HashMap getGETArguments()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("username", userName);
    localHashMap.put("calculate_size", Boolean.valueOf(calculateSize));
    localHashMap.put("ext_preload", Boolean.valueOf(getPreload));
    localHashMap.put("offset", offset);
    localHashMap.put("limit", limit);
    return localHashMap;
  }
  
  public String getGroupEndpoint()
  {
    return "/api/v1/oauth2/gallery/folders";
  }
  
  public HashMap getPOSTArguments()
  {
    return null;
  }
  
  public TypeToken getWrapperTypeToken()
  {
    return new DVNTGalleryFoldersRequestV1.1(this);
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(userName).append(calculateSize).append(getPreload).append(offset).append(limit).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTPaginatedGalleryFolders sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).galleryFolders(paramString, userName, calculateSize, getPreload, offset, limit);
  }
}
