package com.deviantart.android.damobile.util.submit;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import android.widget.Toast;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTPublishOptions;
import com.deviantart.android.damobile.fragment.SubmitGallery;
import com.deviantart.android.damobile.fragment.SubmitMention;
import com.deviantart.android.damobile.fragment.SubmitTag;
import com.deviantart.android.damobile.util.ImageQualityType;
import com.deviantart.android.damobile.util.PhotoUtils;
import com.deviantart.android.damobile.util.Recent;
import com.deviantart.android.damobile.util.SharedPreferenceUtil;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

public class SubmissionSender
{
  private final Application context;
  private final Submission this$0;
  
  public SubmissionSender(Application paramApplication, Submission paramSubmission)
  {
    context = paramApplication;
    this$0 = paramSubmission;
  }
  
  private void attribute(String paramString1, String paramString2, String paramString3)
  {
    DVNTCommonAsyncAPI.postStatus(paramString1, paramString2, paramString3).call(context, new SubmissionSender.3(this, paramString2));
  }
  
  private File getName(File paramFile)
  {
    return PhotoUtils.doInBackground(paramFile, ImageQualityType.get(context));
  }
  
  private DVNTPublishOptions getView()
  {
    Object localObject2 = this$0.isHidden();
    Object localObject1 = localObject2;
    if (localObject2 == null) {
      localObject1 = Boolean.valueOf(false);
    }
    localObject2 = new DVNTPublishOptions();
    SubmitOptions localSubmitOptions = this$0.getList();
    ((DVNTPublishOptions)localObject2).setAllowComments(localSubmitOptions.isVisible());
    ((DVNTPublishOptions)localObject2).setAllowFreeDownloads(localSubmitOptions.getSize());
    ((DVNTPublishOptions)localObject2).setAgreeTOS(Boolean.valueOf(true));
    ((DVNTPublishOptions)localObject2).setAgreeSubmissionTerms(Boolean.valueOf(true));
    ((DVNTPublishOptions)localObject2).setWatermark(localSubmitOptions.isPinned());
    ((DVNTPublishOptions)localObject2).setIsMature((Boolean)localObject1);
    localObject1 = localSubmitOptions.getString();
    ((DVNTPublishOptions)localObject2).setIsLicenseCreativeCommons(Boolean.valueOf(((SubmitLicenseOptions)localObject1).equals()));
    ((DVNTPublishOptions)localObject2).setLicenseModification(((SubmitLicenseOptions)localObject1).equalsIgnoreCase());
    ((DVNTPublishOptions)localObject2).setIsLicenseOkforCommercialUse(Boolean.valueOf(((SubmitLicenseOptions)localObject1).a()));
    return localObject2;
  }
  
  private void parse(ArrayList paramArrayList)
  {
    Object localObject1;
    if (this$0.getParent() != null)
    {
      localObject1 = new StringWriter();
      localObject2 = this$0;
    }
    for (;;)
    {
      try
      {
        IOUtils.copy(new FileInputStream(((Submission)localObject2).getParent()), (Writer)localObject1, "UTF-8");
        localObject1 = new StringBuilder(((StringWriter)localObject1).toString());
        i = 0;
        if (i >= paramArrayList.size()) {
          break;
        }
        if (i != 0) {
          break label143;
        }
        ((StringBuilder)localObject1).append("<br />");
        ((StringBuilder)localObject1).append("#").append((String)paramArrayList.get(i));
        i += 1;
        continue;
        localObject1 = new StringBuilder("");
      }
      catch (IOException paramArrayList)
      {
        this$0.append(false);
        Toast.makeText(context, context.getString(2131230964), 0).show();
        return;
      }
      continue;
      label143:
      ((StringBuilder)localObject1).append(" ");
    }
    Object localObject2 = this$0.get().iterator();
    int i = 1;
    if (((Iterator)localObject2).hasNext())
    {
      SubmitMention localSubmitMention = (SubmitMention)((Iterator)localObject2).next();
      if (i != 0)
      {
        ((StringBuilder)localObject1).append("<br />");
        i = 0;
      }
      for (;;)
      {
        ((StringBuilder)localObject1).append("@").append(localSubmitMention.getValue());
        break;
        ((StringBuilder)localObject1).append(" ");
      }
    }
    if (this$0.size() != null)
    {
      DVNTCommonAsyncAPI.stashSubmitFile(getName(this$0.size()), this$0.getTags().equals(), "embedded_item" + new Date().getTime(), "", "", paramArrayList).call(context, new SubmissionSender.2(this, (StringBuilder)localObject1));
      return;
    }
    attribute(((StringBuilder)localObject1).toString(), this$0.getTitle(), null);
  }
  
  private void parse(ArrayList paramArrayList1, StringBuilder paramStringBuilder, ArrayList paramArrayList2)
  {
    FileWriter localFileWriter = new FileWriter(this$0.getParent(), true);
    Iterator localIterator = this$0.get().iterator();
    int i = 1;
    if (localIterator.hasNext())
    {
      Object localObject = (SubmitMention)localIterator.next();
      if (i != 0)
      {
        localFileWriter.append("<br />");
        i = 0;
      }
      for (;;)
      {
        localObject = ((SubmitMention)localObject).getValue();
        localFileWriter.append("@");
        localFileWriter.append((CharSequence)localObject);
        break;
        localFileWriter.append(" ");
      }
    }
    if (this$0.size() != null)
    {
      DVNTCommonAsyncAPI.stashSubmitFile(getName(this$0.size()), this$0.getTags().equals(), "embedded_item" + new Date().getTime(), "", "", paramArrayList1).call(context, new SubmissionSender.1(this, localFileWriter, paramArrayList1, paramStringBuilder, paramArrayList2));
      return;
    }
    localFileWriter.close();
    toString(paramArrayList1, paramStringBuilder, paramArrayList2);
  }
  
  public String doInBackground()
  {
    this$0.append(true);
    ArrayList localArrayList = new ArrayList();
    Object localObject1 = this$0.getKey().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = ((SubmitTag)((Iterator)localObject1).next()).getValue();
      localArrayList.add(localObject2);
      new Recent(context, "recent_tags").write((String)localObject2);
    }
    localObject1 = new StringBuilder();
    if (this$0.getName() != null) {
      ((StringBuilder)localObject1).append(this$0.getName());
    }
    Object localObject2 = this$0.get().iterator();
    int i = 1;
    Object localObject4;
    if (((Iterator)localObject2).hasNext())
    {
      localObject3 = (SubmitMention)((Iterator)localObject2).next();
      if (i != 0)
      {
        ((StringBuilder)localObject1).append("<br />");
        i = 0;
      }
      for (;;)
      {
        localObject4 = ((SubmitMention)localObject3).getValue();
        ((StringBuilder)localObject1).append("@" + (String)localObject4);
        new Recent(context, "recent_mentions").write((String)localObject4 + "&&" + ((SubmitMention)localObject3).e());
        break;
        ((StringBuilder)localObject1).append(" ");
      }
    }
    localObject2 = new ArrayList();
    Object localObject3 = this$0.remove().iterator();
    while (((Iterator)localObject3).hasNext())
    {
      localObject4 = (SubmitGallery)((Iterator)localObject3).next();
      new Recent(context, "recent_galleries").write(StringUtils.join(new String[] { ((SubmitGallery)localObject4).getValue(), "||", ((SubmitGallery)localObject4).s() }));
      ((ArrayList)localObject2).add(((SubmitGallery)localObject4).s());
    }
    localObject3 = SharedPreferenceUtil.get(context);
    if (this$0.getList().get().booleanValue())
    {
      localObject4 = new Gson();
      ((SharedPreferences)localObject3).edit().putString("submit_remembered_settings", ((Gson)localObject4).toJson(this$0.getList())).apply();
    }
    if (TextUtils.isEmpty(this$0.getCategory()))
    {
      if (this$0.getString() == SubmitType.i) {
        this$0.setCategory(context.getString(2131231360));
      }
    }
    else {
      switch (SubmissionSender.5.c[this$0.getString().ordinal()])
      {
      default: 
        break;
      }
    }
    for (;;)
    {
      toString(localArrayList, (StringBuilder)localObject1, (ArrayList)localObject2);
      for (;;)
      {
        return null;
        if (this$0.getString() == SubmitType.c) {
          break;
        }
        this$0.setCategory(context.getString(2131231359));
        break;
        try
        {
          parse(localArrayList, (StringBuilder)localObject1, (ArrayList)localObject2);
        }
        catch (IOException localIOException)
        {
          this$0.append(false);
          return context.getString(2131230959);
        }
        parse(localIOException);
      }
      this$0.download(null);
    }
  }
  
  public void toString(ArrayList paramArrayList1, StringBuilder paramStringBuilder, ArrayList paramArrayList2)
  {
    paramArrayList2 = new SubmissionSender.4(this, paramArrayList2);
    if ((this$0.getParent() == null) && (this$0.size() != null))
    {
      DVNTCommonAsyncAPI.stashSubmitFile(getName(this$0.size()), this$0.getTags().equals(), this$0.getCategory(), paramStringBuilder.toString(), "dA Mobile submissions", paramArrayList1).call(context, paramArrayList2);
      return;
    }
    if ((this$0.size() == null) && (this$0.getParent() != null))
    {
      DVNTCommonAsyncAPI.stashSubmitFile(this$0.getParent(), this$0.getMimeType().equals(), this$0.getCategory(), paramStringBuilder.toString(), "dA Mobile submissions", paramArrayList1).call(context, paramArrayList2);
      return;
    }
    DVNTCommonAsyncAPI.stashSubmitFile(getName(this$0.size()), this$0.getTags().equals(), this$0.getParent(), this$0.getMimeType().equals(), this$0.getCategory(), paramStringBuilder.toString(), "dA Mobile submissions", paramArrayList1).call(context, paramArrayList2);
  }
}
