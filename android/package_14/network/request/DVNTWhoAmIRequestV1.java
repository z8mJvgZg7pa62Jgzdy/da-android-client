package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTWhoAmIRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTWhoAmIResponse>
{
  public DVNTWhoAmIRequestV1()
  {
    super(com.deviantart.android.sdk.api.model.DVNTWhoAmIResponse.class);
  }
  
  public String getCacheKey()
  {
    return "whoami";
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTWhoAmIResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).whoAmI(paramString);
  }
}
