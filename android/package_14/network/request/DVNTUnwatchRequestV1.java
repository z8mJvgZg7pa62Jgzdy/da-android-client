package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTUnwatchRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  private String userName;
  
  public DVNTUnwatchRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    userName = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTUnwatchRequestV1 localDVNTUnwatchRequestV1 = (DVNTUnwatchRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(userName, userName).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "unwatch" + userName;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 10000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(userName).toHashCode();
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).unwatch(paramString, userName);
  }
}
