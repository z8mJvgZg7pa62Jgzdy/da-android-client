package com.deviantart.android.damobile.view.viewpageindicator;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout.LayoutParams;
import com.deviantart.android.damobile.util.DAFont;

public class UserProfileMenuIndicator
  extends DATabIndicator
{
  public UserProfileMenuIndicator(Context paramContext)
  {
    super(paramContext);
  }
  
  public UserProfileMenuIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  private Typeface getTypeface()
  {
    return DAFont.b.get(getContext());
  }
  
  protected void a(int paramInt1, CharSequence paramCharSequence1, int paramInt2, CharSequence paramCharSequence2)
  {
    UserProfileMenuIndicator.UserProfileTabView localUserProfileTabView = new UserProfileMenuIndicator.UserProfileTabView(this, getContext(), paramCharSequence1, paramInt2);
    z = paramInt1;
    localUserProfileTabView.setFocusable(true);
    localUserProfileTabView.setOnClickListener(a);
    localUserProfileTabView.setContentDescription(paramCharSequence2);
    mTabLayout.addView(localUserProfileTabView, b(paramInt1, paramCharSequence1, paramInt2));
  }
  
  protected LinearLayout.LayoutParams b(int paramInt1, CharSequence paramCharSequence, int paramInt2)
  {
    return new LinearLayout.LayoutParams(-2, -1, 1.0F);
  }
  
  protected ViewGroup.LayoutParams getTabContainerLayout()
  {
    return new ViewGroup.LayoutParams(-2, -1);
  }
  
  protected int measureHeight(int paramInt1, int paramInt2)
  {
    return View.MeasureSpec.getSize(paramInt2);
  }
}
