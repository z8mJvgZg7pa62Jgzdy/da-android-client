package com.deviantart.android.damobile.view.userprofile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.deviantart.android.damobile.adapter.recyclerview.UserStatusAdapter;
import com.deviantart.android.damobile.fragment.UserProfileFragment;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.view.DAStateRecyclerView;
import com.deviantart.android.damobile.view.ViewState.Helper;

public class UserProfileStatusLayout
  extends DAStateRecyclerView
{
  public UserProfileStatusLayout(String paramString, boolean paramBoolean, Context paramContext, StreamLoader paramStreamLoader)
  {
    super(paramContext);
    setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    setAdapter();
    onViewCreated();
    ViewState.Helper.a(b, paramString, getContext().getString(2131230892), paramString + " " + getContext().getString(2131230893));
    setAdapter(new UserStatusAdapter(StreamCacher.a(paramStreamLoader)));
    if (paramBoolean)
    {
      paramString = LayoutInflater.from(paramContext).inflate(2130968805, this, false);
      paramString.setOnClickListener(UserProfileStatusLayout..Lambda.1.lambdaFactory$(this));
      a(paramString);
    }
    b(UserProfileFragment.init(paramContext));
  }
}
