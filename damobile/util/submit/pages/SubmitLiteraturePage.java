package com.deviantart.android.damobile.util.submit.pages;

import com.deviantart.android.damobile.util.submit.SubmitType;

public class SubmitLiteraturePage
  extends SubmitTextPage
{
  public SubmitLiteraturePage(String paramString)
  {
    super(paramString);
  }
  
  public SubmitType c()
  {
    return SubmitType.b;
  }
  
  public boolean g()
  {
    return true;
  }
  
  public CharSequence getFormatted()
  {
    return "SubmitLiteratureTab";
  }
  
  protected int newHandle()
  {
    return 2130968753;
  }
  
  public boolean setContentHeight()
  {
    return false;
  }
}
