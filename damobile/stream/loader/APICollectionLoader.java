package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTDeviation;

public class APICollectionLoader
  extends FolderStreamLoader<DVNTDeviation>
{
  private final String a;
  private final String d;
  
  public APICollectionLoader(String paramString1, String paramString2)
  {
    d = paramString1;
    a = paramString2;
    a = 14;
  }
  
  public String b()
  {
    return "collectionloader" + d + "|" + a;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.getCollection(d, a, Integer.valueOf(paramInt), Integer.valueOf(a()));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APICollectionLoader.1(this, paramStreamLoadListener));
  }
}
