package com.deviantart.android.damobile.view.image;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import com.deviantart.android.android.utils.DVNTContextUtils;
import java.lang.ref.WeakReference;

public class DefaultZoomableController
  extends GestureDetector.SimpleOnGestureListener
  implements TransformGestureDetector.Listener, ZoomableController
{
  private float a = 1.0F;
  private TransformGestureDetector b;
  private final Matrix c = new Matrix();
  private boolean d = false;
  private boolean e = false;
  private boolean f = true;
  private boolean g = true;
  private ZoomableController.Listener h = null;
  private GestureDetector i;
  private float j = 10.0F;
  private float k = 0.75F * a;
  private boolean l = false;
  private final RectF left = new RectF();
  private final float[] m = new float[9];
  private final Matrix mMatrix = new Matrix();
  private final RectF mRect = new RectF();
  private final RectF mRegion = new RectF();
  private final RectF mViewportRect = new RectF();
  private DefaultZoomableController.Fling n;
  private final Matrix this$0 = new Matrix();
  private WeakReference<Context> w;
  private float x;
  private float y = 1.25F * j;
  
  public DefaultZoomableController(Context paramContext, TransformGestureDetector paramTransformGestureDetector)
  {
    b = paramTransformGestureDetector;
    b.b(this);
    i = new GestureDetector(paramContext, this);
    w = new WeakReference(paramContext);
  }
  
  private void a(float paramFloat1, float paramFloat2)
  {
    float f1 = d();
    float f2 = max(f1, k, y);
    if (f2 != f1)
    {
      f1 = f2 / f1;
      this$0.postScale(f1, f1, paramFloat1, paramFloat2);
    }
  }
  
  private void a(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2, int paramInt)
  {
    int i1 = 0;
    while (i1 < paramInt)
    {
      paramArrayOfFloat1[(i1 * 2 + 0)] = (paramArrayOfFloat2[(i1 * 2 + 0)] * left.width() + left.left);
      paramArrayOfFloat1[(i1 * 2 + 1)] = (paramArrayOfFloat2[(i1 * 2 + 1)] * left.height() + left.top);
      i1 += 1;
    }
  }
  
  private void draw(float[] paramArrayOfFloat1, float[] paramArrayOfFloat2, int paramInt)
  {
    int i1 = 0;
    while (i1 < paramInt)
    {
      paramArrayOfFloat1[(i1 * 2 + 0)] = ((paramArrayOfFloat2[(i1 * 2 + 0)] - left.left) / left.width());
      paramArrayOfFloat1[(i1 * 2 + 1)] = ((paramArrayOfFloat2[(i1 * 2 + 1)] - left.top) / left.height());
      i1 += 1;
    }
  }
  
  public static DefaultZoomableController getContentProvider(Context paramContext)
  {
    return new DefaultZoomableController(paramContext, TransformGestureDetector.scalb());
  }
  
  private float getOffset(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    paramFloat2 = paramFloat3 - paramFloat2;
    if (paramFloat2 > 0.0F) {
      return paramFloat2 / 2.0F;
    }
    return max(paramFloat1, paramFloat2, 0.0F);
  }
  
  private float max(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    return Math.min(Math.max(paramFloat2, paramFloat1), paramFloat3);
  }
  
  private boolean update()
  {
    RectF localRectF = mRect;
    localRectF.set(left);
    this$0.mapRect(localRectF);
    float f1 = getOffset(left, localRectF.width(), mRegion.width());
    float f2 = getOffset(top, localRectF.height(), mRegion.height());
    if ((f1 != left) || (f2 != top))
    {
      this$0.postTranslate(f1 - left, f2 - top);
      return true;
    }
    return false;
  }
  
  public void a(RectF paramRectF)
  {
    left.set(paramRectF);
  }
  
  public void a(boolean paramBoolean)
  {
    d = paramBoolean;
    if (!paramBoolean) {
      visitMethodInsn();
    }
  }
  
  public boolean a()
  {
    return d;
  }
  
  public void b()
  {
    float f1 = mRegion.width() / left.width();
    float f2 = mRegion.height() / left.height();
    if (f1 > f2)
    {
      x = f1;
      return;
    }
    x = f2;
  }
  
  public void b(float paramFloat)
  {
    j = paramFloat;
  }
  
  public void b(TransformGestureDetector paramTransformGestureDetector)
  {
    float f1 = d();
    float f2 = max(f1, a, j);
    if ((f2 != f1) && (!l))
    {
      paramTransformGestureDetector = new DefaultZoomableController.DoubleTapZoom(this, f1, f2, mRegion.centerX(), mRegion.centerY());
      if (h != null) {
        h.a(paramTransformGestureDetector);
      }
    }
    c.set(this$0);
  }
  
  public void b(ZoomableController.Listener paramListener)
  {
    h = paramListener;
  }
  
  public float c()
  {
    return a;
  }
  
  public float d()
  {
    this$0.getValues(m);
    return m[0];
  }
  
  public boolean d(MotionEvent paramMotionEvent)
  {
    if (d)
    {
      i.onTouchEvent(paramMotionEvent);
      return b.a(paramMotionEvent);
    }
    return false;
  }
  
  public PointF draw(PointF paramPointF)
  {
    float[] arrayOfFloat = m;
    arrayOfFloat[0] = x;
    arrayOfFloat[1] = y;
    this$0.invert(mMatrix);
    mMatrix.mapPoints(arrayOfFloat, 0, arrayOfFloat, 0, 1);
    draw(arrayOfFloat, arrayOfFloat, 1);
    return new PointF(arrayOfFloat[0], arrayOfFloat[1]);
  }
  
  public void draw(float paramFloat, PointF paramPointF)
  {
    if (b.d()) {
      b.a();
    }
    float[] arrayOfFloat = m;
    arrayOfFloat[0] = x;
    arrayOfFloat[1] = y;
    a(arrayOfFloat, arrayOfFloat, 1);
    this$0.postScale(paramFloat, paramFloat, arrayOfFloat[0], arrayOfFloat[1]);
    update();
    if (h != null) {
      h.a(this$0);
    }
  }
  
  public void draw(TransformGestureDetector paramTransformGestureDetector)
  {
    this$0.set(c);
    float f1;
    if (e)
    {
      f1 = paramTransformGestureDetector.getAngle();
      this$0.postRotate(f1 * 57.29578F, paramTransformGestureDetector.f(), paramTransformGestureDetector.component2());
    }
    if (f)
    {
      f1 = paramTransformGestureDetector.getScale();
      this$0.postScale(f1, f1, paramTransformGestureDetector.f(), paramTransformGestureDetector.component2());
    }
    a(paramTransformGestureDetector.f(), paramTransformGestureDetector.component2());
    if (g) {
      this$0.postTranslate(paramTransformGestureDetector.remove(), paramTransformGestureDetector.getCount());
    }
    if (update())
    {
      l = true;
      b.b();
    }
    if (h != null) {
      h.a(this$0);
    }
  }
  
  public void f(TransformGestureDetector paramTransformGestureDetector)
  {
    c.set(this$0);
    l = false;
  }
  
  public Matrix get()
  {
    return this$0;
  }
  
  public void inflate(RectF paramRectF)
  {
    mRegion.set(paramRectF);
  }
  
  public boolean onDoubleTap(MotionEvent paramMotionEvent)
  {
    if (d() == a) {}
    for (float f1 = x;; f1 = a)
    {
      if (h != null)
      {
        paramMotionEvent = new DefaultZoomableController.DoubleTapZoom(this, d(), f1, paramMotionEvent.getX(), paramMotionEvent.getY());
        h.a(paramMotionEvent);
      }
      return true;
    }
  }
  
  public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    if (n != null) {
      n.cancelFling();
    }
    if (DVNTContextUtils.isContextDead((Context)w.get())) {
      return false;
    }
    n = new DefaultZoomableController.Fling(this, (Context)w.get(), (int)paramFloat1, (int)paramFloat2);
    if (h != null) {
      h.a(n);
    }
    return super.onFling(paramMotionEvent1, paramMotionEvent2, paramFloat1, paramFloat2);
  }
  
  public void visitMethodInsn()
  {
    b.a();
    c.reset();
    this$0.reset();
  }
}
