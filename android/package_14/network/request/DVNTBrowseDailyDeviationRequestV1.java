package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.android.package_14.network.wrapper.DVNTResultsWrapper;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTBrowseDailyDeviationRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTAbstractDeviation.List>
{
  final String date;
  final Integer limit;
  final Integer offset;
  
  public DVNTBrowseDailyDeviationRequestV1(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTAbstractDeviation.List.class);
    date = paramString;
    offset = paramInteger1;
    limit = paramInteger2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTBrowseDailyDeviationRequestV1 localDVNTBrowseDailyDeviationRequestV1 = (DVNTBrowseDailyDeviationRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(date, date).append(offset, offset).append(limit, limit).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "dailydev" + date + offset + limit;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 3600000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(date).append(offset).append(limit).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTAbstractDeviation.List sendRequest(String paramString)
  {
    return (com.deviantart.android.android.package_14.model.DVNTAbstractDeviation.List)((DVNTServiceV1)getService()).browseDailyDeviations(paramString, date, offset, limit).getResults();
  }
}
