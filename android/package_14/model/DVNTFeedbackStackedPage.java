package com.deviantart.android.android.package_14.model;

import com.deviantart.android.sdk.api.model.DVNTFeedbackMessageStack.ListO;
import com.deviantart.android.sdk.api.model.DVNTPaginatedResult;

public class DVNTFeedbackStackedPage
  extends DVNTPaginatedResult<DVNTFeedbackMessageStack.ListO>
{
  public DVNTFeedbackStackedPage() {}
}
