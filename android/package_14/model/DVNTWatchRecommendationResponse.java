package com.deviantart.android.android.package_14.model;

import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTWatchRecommendationResponse
  implements Serializable
{
  @SerializedName("cursor")
  String cursor;
  @SerializedName("recommendations")
  DVNTWatchRecommendationItem.List recommendations;
  
  public DVNTWatchRecommendationResponse() {}
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    paramObject = (DVNTWatchRecommendationResponse)paramObject;
    return (Objects.append(cursor, cursor)) && (Objects.append(recommendations, recommendations));
  }
  
  public String getCursor()
  {
    return cursor;
  }
  
  public DVNTWatchRecommendationItem.List getRecommendations()
  {
    return recommendations;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { cursor, recommendations });
  }
  
  public void setCursor(String paramString)
  {
    cursor = paramString;
  }
  
  public void setRecommendations(DVNTWatchRecommendationItem.List paramList)
  {
    recommendations = paramList;
  }
}
