package com.deviantart.android.damobile.drawable;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.deviantart.android.damobile.util.Graphics;

public class SlashBackgroundDrawable
  extends Drawable
{
  private Paint fillPaint = new Paint();
  private Path path;
  private Paint strokePaint;
  
  private SlashBackgroundDrawable(int paramInt1, int paramInt2)
  {
    fillPaint.setColor(paramInt2);
    strokePaint = new Paint();
    strokePaint.setColor(paramInt1);
    strokePaint.setStyle(Paint.Style.FILL);
    strokePaint.setAntiAlias(true);
  }
  
  public static SlashBackgroundDrawable getDrawable(Context paramContext)
  {
    return new SlashBackgroundDrawable(paramContext.getResources().getColor(2131558548), paramContext.getResources().getColor(2131558549));
  }
  
  public void draw(Canvas paramCanvas)
  {
    if ((fillPaint == null) || (path == null) || (strokePaint == null))
    {
      Log.e("SlashBG", "Unexpected null drawable");
      return;
    }
    paramCanvas.drawPaint(fillPaint);
    paramCanvas.drawPath(path, strokePaint);
  }
  
  public int getOpacity()
  {
    return -1;
  }
  
  protected void onBoundsChange(Rect paramRect)
  {
    int i = (int)(paramRect.width() * 0.7F);
    int j = (int)(1.8807265F * i);
    path = Graphics.parsePath(new Point[] { new Point(0, 0), new Point(i, 0), new Point(0, j) });
  }
  
  public void setAlpha(int paramInt) {}
  
  public void setColorFilter(ColorFilter paramColorFilter) {}
}
