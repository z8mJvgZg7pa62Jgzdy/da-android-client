package com.deviantart.android.damobile.view.thirdparty.decorator;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.view.DeviationBottomBarMenu;
import com.deviantart.android.damobile.view.gom.decorator.DecoratorGom;

public class DeviationBottomBarDecoratorGom
  implements DecoratorGom<com.deviantart.android.damobile.view.gom.decorator.DeviationBottomBarViewHolder, DeviationDescription>
{
  public DeviationBottomBarDecoratorGom() {}
  
  public void a(Context paramContext, DeviationBottomBarViewHolder paramDeviationBottomBarViewHolder, DeviationDescription paramDeviationDescription)
  {
    DVNTDeviation localDVNTDeviation = paramDeviationDescription.a();
    bottomBarMenu.onCreateView(paramDeviationDescription);
    paramDeviationDescription = citemView;
    switch (DeviationBottomBarDecoratorGom.1.s[com.deviantart.android.damobile.util.DeviationUtils.a(localDVNTDeviation).ordinal()])
    {
    default: 
      title.setVisibility(0);
      title.setText(localDVNTDeviation.getTitle());
      paramDeviationDescription.setPadding(paramDeviationDescription.getPaddingLeft(), paramDeviationDescription.getPaddingTop(), paramDeviationDescription.getPaddingRight(), 0);
      return;
    }
    title.setVisibility(8);
    paramDeviationDescription.setPadding(paramDeviationDescription.getPaddingLeft(), paramDeviationDescription.getPaddingTop(), paramDeviationDescription.getPaddingRight(), (int)paramContext.getResources().getDimension(2131361951));
  }
  
  public Class getComponentType()
  {
    return DeviationDescription.class;
  }
  
  public DeviationBottomBarViewHolder onCreateView(Context paramContext, ViewGroup paramViewGroup, RecyclerView.ViewHolder paramViewHolder)
  {
    paramContext = LayoutInflater.from(paramContext).inflate(2130968639, paramViewGroup, false);
    ((FrameLayout)ButterKnife.findById(paramContext, 2131689650)).addView(itemView, 0);
    return new DeviationBottomBarViewHolder(paramViewHolder, paramContext);
  }
}
