package com.deviantart.android.damobile.util;

import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.content.Context;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.Toast;
import com.deviantart.datoolkit.logger.DVNTLog;

public class PermissionUtil
{
  public PermissionUtil() {}
  
  public static void alert(Context paramContext, CharSequence paramCharSequence)
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
    localBuilder.setTitle(2131231194).setMessage(paramCharSequence).setPositiveButton(2131231193, new PermissionUtil.4(paramContext)).setNeutralButton(2131231195, new PermissionUtil.3());
    localBuilder.show();
  }
  
  public static PermissionUtil.CheckResult checkPermission(Fragment paramFragment, String paramString, int paramInt1, int paramInt2)
  {
    PermissionUtil.CheckResult localCheckResult = onCreate(paramFragment, paramString, paramInt1);
    if (localCheckResult == PermissionUtil.CheckResult.First) {
      show(paramFragment, paramString, paramInt1, paramInt2);
    }
    return localCheckResult;
  }
  
  public static boolean execute(Fragment paramFragment, String[] paramArrayOfString, int[] paramArrayOfInt, int paramInt1, int paramInt2)
  {
    if (isPermitted(paramArrayOfInt)) {
      return true;
    }
    if (paramArrayOfString.length == 0)
    {
      DVNTLog.write("Got empty permission array. Permission request cancelled?", new Object[0]);
      return false;
    }
    if (!FragmentCompat.shouldShowRequestPermissionRationale(paramFragment, paramArrayOfString[0])) {}
    for (int i = 1;; i = 0)
    {
      paramFragment = paramFragment.getActivity();
      if (i == 0) {
        break;
      }
      alert(paramFragment, TextUtils.concat((CharSequence[])new CharSequence[] { paramFragment.getText(paramInt1), "\n\n", paramFragment.getText(paramInt2) }));
      return false;
    }
    Toast.makeText(paramFragment, paramInt1, 1).show();
    return false;
  }
  
  public static boolean isPermitted(int[] paramArrayOfInt)
  {
    return (paramArrayOfInt.length > 0) && (paramArrayOfInt[0] == 0);
  }
  
  public static PermissionUtil.CheckResult onCreate(Fragment paramFragment, String paramString, int paramInt)
  {
    if (ContextCompat.checkSelfPermission(paramFragment.getActivity(), paramString) == 0) {
      return PermissionUtil.CheckResult.mFileManager;
    }
    if (FragmentCompat.shouldShowRequestPermissionRationale(paramFragment, paramString)) {
      return PermissionUtil.CheckResult.First;
    }
    FragmentCompat.requestPermissions(paramFragment, new String[] { paramString }, paramInt);
    return PermissionUtil.CheckResult.b;
  }
  
  public static void show(Fragment paramFragment, String paramString, int paramInt1, int paramInt2)
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramFragment.getActivity());
    localBuilder.setTitle(2131231202).setMessage(paramInt2).setPositiveButton(2131231201, new PermissionUtil.2(paramFragment, paramString, paramInt1)).setNegativeButton(2131230820, new PermissionUtil.1());
    localBuilder.show();
  }
}
