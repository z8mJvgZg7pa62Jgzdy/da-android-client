package com.deviantart.android.damobile.util;

import android.content.Context;
import com.deviantart.android.android.package_14.model.DVNTTag;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APIBrowseTagLoader;
import com.deviantart.android.damobile.util.tracking.TrackerUtil.EventLabelBuilder;
import java.util.ArrayList;

public class CuratedTagPreviewItem
  extends DefaultDeviationTorpedoPreviewItem
{
  private final DVNTTag e;
  
  public CuratedTagPreviewItem(DVNTTag paramDVNTTag)
  {
    e = paramDVNTTag;
  }
  
  public String a(Context paramContext)
  {
    Boolean localBoolean = e.getSponsored();
    if ((localBoolean == null) || (!localBoolean.booleanValue())) {
      return null;
    }
    return paramContext.getString(2131231344) + ": " + e.getSponsorName();
  }
  
  public String b()
  {
    return "#" + e.getTagName();
  }
  
  public Stream c()
  {
    Stream localStream = StreamCacher.a(new APIBrowseTagLoader(e.getTagName()), StreamCacheStrategy.d);
    if ((e.getDeviations() != null) && (!e.getDeviations().isEmpty())) {
      localStream.a(e.getDeviations());
    }
    return localStream;
  }
  
  public String getPropertyName()
  {
    return "tag";
  }
  
  public String getRawValue()
  {
    return new TrackerUtil.EventLabelBuilder().get("tagname", e.getTagName()).getValue();
  }
}
