package com.deviantart.android.damobile.view.animation;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;

public abstract class SimpleAnimatorListener
  implements Animator.AnimatorListener
{
  public SimpleAnimatorListener() {}
  
  public void onAnimationCancel(Animator paramAnimator) {}
  
  public void onAnimationRepeat(Animator paramAnimator) {}
  
  public void onAnimationStart(Animator paramAnimator) {}
}
