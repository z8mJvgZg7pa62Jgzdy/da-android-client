package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.content.res.Resources;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTCategory;
import com.deviantart.android.damobile.activity.SubmitActivity;
import com.deviantart.android.damobile.adapter.recyclerview.CategoryAdapter.Builder;
import com.deviantart.android.damobile.adapter.recyclerview.CategoryAdapter.CategorySelectedListener;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.submit.Submission;
import com.deviantart.android.damobile.util.submit.SubmitType;
import com.deviantart.android.damobile.view.DAStateRecyclerView;

public class SubmitCategoryFragment
  extends DABaseFragment
  implements CategoryAdapter.CategorySelectedListener
{
  public SubmitCategoryFragment() {}
  
  private static String getPath(SubmitType paramSubmitType)
  {
    if (paramSubmitType == null) {
      return "/";
    }
    switch (SubmitCategoryFragment.1.d[paramSubmitType.ordinal()])
    {
    default: 
      return "/";
    case 1: 
      return "/journals";
    case 2: 
      return "/";
    }
    return "/literature";
  }
  
  public static SubmitCategoryFragment showConfirmDialog(SubmitType paramSubmitType, String paramString)
  {
    SubmitCategoryFragment localSubmitCategoryFragment = new SubmitCategoryFragment();
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("type", paramSubmitType);
    localBundle.putString("filetype", paramString);
    localSubmitCategoryFragment.setArguments(localBundle);
    return localSubmitCategoryFragment;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    ((SubmitActivity)getActivity()).getSupportActionBar().setTitle(2131231357);
    paramLayoutInflater = new DAStateRecyclerView(getActivity());
    paramLayoutInflater.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    paramLayoutInflater.setBackgroundColor(getActivity().getResources().getColor(2131558441));
    paramViewGroup = getPath((SubmitType)getArguments().getSerializable("type"));
    paramBundle = getArguments().getString("filetype");
    paramViewGroup = new CategoryAdapter.Builder(getActivity(), paramViewGroup).b(paramBundle).a(this).b(true).a();
    paramLayoutInflater.b(false);
    paramLayoutInflater.setAdapter(paramViewGroup);
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public void onItemClick(DVNTCategory paramDVNTCategory)
  {
    Submission.getInstance().setTitle(paramDVNTCategory);
    ScreenFlowManager.hideKeyboard(getActivity());
  }
}
