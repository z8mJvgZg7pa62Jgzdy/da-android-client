package com.deviantart.android.damobile.util;

import android.util.Log;

public enum MemberType
{
  private String code;
  private boolean h;
  private boolean k;
  private int m;
  private MemberType.SymbolTextInfo t;
  private MemberType.SymbolTextInfo y;
  
  static
  {
    E = new MemberType("ADMIN", 1, "admin", false, false, 2131230794, new MemberType.SymbolTextInfo(2131230793));
    b = new MemberType("SENIOR", 2, "senior", false, false, 2131231254, new MemberType.SymbolTextInfo(2131231253));
    d = new MemberType("VOLUNTEER", 3, "volunteer", false, false, 2131231423, new MemberType.SymbolTextInfo(2131231422));
    id = new MemberType("CORE", 4, "premium", false, true, 2131230838, new MemberType.SymbolTextInfo(2131230837, Integer.valueOf(2131558427), Integer.valueOf(2131231345), Integer.valueOf(17170443)), MemberType.SymbolTextInfo.init());
    end = new MemberType("CORE_BETA", 5, "beta", false, true, 2131230815, new MemberType.SymbolTextInfo(2131230814, Integer.valueOf(2131558427), Integer.valueOf(2131231345), Integer.valueOf(17170443)));
    o = new MemberType("CORE_HELL", 6, "hell", false, true, 2131230834, new MemberType.SymbolTextInfo(2131230833, Integer.valueOf(2131558427)));
    u = new MemberType("CORE_HELL_BETA", 7, "hell-beta", false, true, 2131230813, new MemberType.SymbolTextInfo(2131230812, Integer.valueOf(2131558427)));
    l = new MemberType("BANNED", 8, "banned", false, false, 2131230811, new MemberType.SymbolTextInfo(2131231231));
    s = new MemberType("GROUP", 9, "group", true, false, 2131231011, new MemberType.SymbolTextInfo(2131231010));
  }
  
  private MemberType(String paramString, boolean paramBoolean1, boolean paramBoolean2, int paramInt, MemberType.SymbolTextInfo paramSymbolTextInfo)
  {
    this(paramString, paramBoolean1, paramBoolean2, paramInt, paramSymbolTextInfo, null);
  }
  
  private MemberType(String paramString, boolean paramBoolean1, boolean paramBoolean2, int paramInt, MemberType.SymbolTextInfo paramSymbolTextInfo1, MemberType.SymbolTextInfo paramSymbolTextInfo2)
  {
    code = paramString;
    h = paramBoolean1;
    k = paramBoolean2;
    m = paramInt;
    t = paramSymbolTextInfo1;
    y = paramSymbolTextInfo2;
  }
  
  public static MemberType toString(String paramString)
  {
    if (paramString != null)
    {
      MemberType[] arrayOfMemberType = values();
      int j = arrayOfMemberType.length;
      int i = 0;
      while (i < j)
      {
        MemberType localMemberType = arrayOfMemberType[i];
        if (paramString.equalsIgnoreCase(code)) {
          return localMemberType;
        }
        i += 1;
      }
      Log.e("MemberType", "Encountered unhandled user class: " + paramString);
    }
    return null;
  }
  
  public MemberType.SymbolTextInfo a()
  {
    return y;
  }
  
  public boolean b()
  {
    return h;
  }
  
  public int c()
  {
    return m;
  }
  
  public String code()
  {
    return code;
  }
  
  public boolean d()
  {
    return k;
  }
  
  public MemberType.SymbolTextInfo q()
  {
    return t;
  }
}
