package com.deviantart.android.damobile.activity;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTCategory;
import com.deviantart.android.android.package_14.model.DVNTNote;
import com.deviantart.android.android.package_14.model.DVNTNotesFolder;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.fragment.DiscoveryFragment;
import com.deviantart.android.damobile.fragment.DiscoverySearchFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.HomeBaseFragment.HomeFragmentInstanceBuilder;
import com.deviantart.android.damobile.fragment.NotesFragment;
import com.deviantart.android.damobile.fragment.NotificationsFragment;
import com.deviantart.android.damobile.fragment.SearchFragment.SearchType;
import com.deviantart.android.damobile.fragment.UserProfileFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.WatchFeedFragment;
import com.deviantart.android.damobile.fragment.WatchLoadingFragment;
import com.deviantart.android.damobile.fragment.WatchRecoFragment;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnGetUserInfoEvent;
import com.deviantart.android.damobile.util.BusStation.OnMCItemChangeEventBase;
import com.deviantart.android.damobile.util.BusStation.OnNoteItemChangeEvent;
import com.deviantart.android.damobile.util.BusStation.OnNotesItemMarkUpdated;
import com.deviantart.android.damobile.util.BusStation.OnNotesUnreadCountLoaded;
import com.deviantart.android.damobile.util.BusStation.OnSignificantScrollEvent;
import com.deviantart.android.damobile.util.BusStation.OnUserSettingChangeEvent;
import com.deviantart.android.damobile.util.DAAnimationUtils;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.util.Recent;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.UserSettingUpdateType;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.ViewHelper;
import com.deviantart.android.damobile.util.WatchFeedContentsSetting;
import com.deviantart.android.damobile.util.deeplink.DeepLinkController;
import com.deviantart.android.damobile.util.notes.NotesDefaultPage;
import com.deviantart.android.damobile.util.notes.NotesFolders;
import com.deviantart.android.damobile.util.notes.NotesFoldersLoader;
import com.deviantart.android.damobile.util.notes.NotesItemData;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.view.LongPressLayout;
import com.deviantart.android.damobile.view.UserAvatar;
import com.deviantart.android.damobile.view.tooltip.DAToolTipRelativeLayout;
import com.squareup.otto.Bus;
import java.util.HashMap;
import samples.grantland.widget.AutofitHelper;

public class HomeActivity
  extends BaseActivity
{
  private static boolean h = false;
  private static int s = 0;
  private boolean E;
  public Integer a;
  private DVNTCategory b;
  @Bind({2131689928})
  ImageView discoveryButton;
  public Intent f;
  @Bind({2131689595})
  FrameLayout floatingBottomButton;
  @Bind({2131689594})
  LinearLayout homeBottomBar;
  private boolean isShown = false;
  @Bind({2131689593})
  LongPressLayout longPressLayout;
  private HomeActivity.BottomBarState m = new HomeActivity.BottomBarState(HomeActivity.BottomBarButton.c);
  @Bind({2131689933})
  FrameLayout notesButton;
  @Bind({2131689934})
  TextView notesUnreadCountView;
  @Bind({2131689932})
  ImageView notificationsButton;
  @Bind({2131689930})
  LinearLayout submitButton;
  @Bind({2131689596})
  DAToolTipRelativeLayout toolTipLayout;
  @Bind({2131689929})
  ImageView watchlistButton;
  @Bind({2131690145})
  UserAvatar wrappedButtonAvatar;
  
  public HomeActivity() {}
  
  private void a(int paramInt)
  {
    if ((notesUnreadCountView != null) && (E))
    {
      if (DVNTContextUtils.isContextDead(this)) {
        return;
      }
      String str;
      if (paramInt > 99)
      {
        str = 99 + getString(2131231115);
        notesUnreadCountView.setVisibility(0);
      }
      for (;;)
      {
        s = paramInt;
        notesUnreadCountView.setText(str);
        return;
        if (paramInt > 0)
        {
          str = Integer.toString(paramInt);
          notesUnreadCountView.setVisibility(0);
        }
        else
        {
          str = "";
          notesUnreadCountView.setVisibility(8);
        }
      }
    }
  }
  
  private void b(WatchFeedContentsSetting paramWatchFeedContentsSetting)
  {
    TrackerUtil.get(this, paramWatchFeedContentsSetting.getLogs(), "change_setting", paramWatchFeedContentsSetting.d());
    DVNTCommonAsyncAPI.feedSettingsUpdate(paramWatchFeedContentsSetting.c(), paramWatchFeedContentsSetting.a(), paramWatchFeedContentsSetting.getValue(), paramWatchFeedContentsSetting.f(), paramWatchFeedContentsSetting.q()).call(this, new HomeActivity.1(this, paramWatchFeedContentsSetting));
  }
  
  private View onCreateView(HomeActivity.BottomBarButton paramBottomBarButton)
  {
    switch (HomeActivity.3.b[paramBottomBarButton.ordinal()])
    {
    default: 
      Log.e("HomeActivity", "Unexpected button value given");
      return null;
    case 1: 
      return discoveryButton;
    case 2: 
    case 3: 
      return watchlistButton;
    case 4: 
      return submitButton;
    case 5: 
      return notificationsButton;
    }
    return notesButton;
  }
  
  private void setContentView()
  {
    DVNTCommonAsyncAPI.feedSettings().call(this, new HomeActivity.2(this));
  }
  
  public void a()
  {
    super.a();
    if (f != null)
    {
      if (a == null) {
        break label55;
      }
      startActivityForResult(f, a.intValue());
    }
    for (;;)
    {
      f = null;
      a = null;
      setContentView();
      if (homeBottomBar != null) {
        break;
      }
      return;
      label55:
      startActivity(f);
    }
    if (E) {
      homeBottomBar.setVisibility(0);
    }
  }
  
  public void a(HomeActivity.BottomBarButton paramBottomBarButton)
  {
    if (paramBottomBarButton == HomeActivity.BottomBarButton.o) {
      return;
    }
    m.b = paramBottomBarButton;
    ViewHelper.show(new View[] { discoveryButton, watchlistButton, submitButton, notificationsButton, notesButton });
    if (paramBottomBarButton != HomeActivity.BottomBarButton.c) {
      onCreateView(paramBottomBarButton).setSelected(true);
    }
  }
  
  public void b()
  {
    super.b();
    if (homeBottomBar == null) {
      return;
    }
    homeBottomBar.setVisibility(8);
    NotesFolders.b();
  }
  
  public void b(DVNTCategory paramDVNTCategory)
  {
    DVNTAbstractAsyncAPI.cancelAllRequests();
    b = paramDVNTCategory;
    ScreenFlowManager.a(this, new DiscoverySearchFragment.InstanceBuilder().p(paramDVNTCategory.getCatPath()).a(paramDVNTCategory.getTitle()).c("browse").a(), HomeActivity.HomeActivityPages.l.a());
  }
  
  public void b(String paramString)
  {
    DVNTAbstractAsyncAPI.cancelAllRequests();
    new Recent(this, "recent_searches").write(paramString);
    Object localObject = SearchFragment.SearchType.a(paramString);
    String str = ((SearchFragment.SearchType)localObject).b(paramString);
    switch (HomeActivity.3.c[localObject.ordinal()])
    {
    default: 
      return;
    case 1: 
      NavigationUtils.b(this, str);
      return;
    }
    DiscoverySearchFragment.InstanceBuilder localInstanceBuilder = new DiscoverySearchFragment.InstanceBuilder().b(str).c("search");
    localObject = HomeActivity.HomeActivityPages.u.a() + str;
    paramString = (String)localObject;
    if (b != null)
    {
      localInstanceBuilder.p(b.getCatPath()).a(b.getTitle()).b(str).putShort();
      paramString = (String)localObject + b.getCatPath();
      b = null;
    }
    ScreenFlowManager.a(this, localInstanceBuilder.a(), paramString);
  }
  
  public void clickDiscoveryButton()
  {
    if (m.b.equals(HomeActivity.BottomBarButton.i)) {
      return;
    }
    DVNTAbstractAsyncAPI.cancelAllRequests();
    ScreenFlowManager.goHome(this);
    ScreenFlowManager.get(this, new DiscoveryFragment(), HomeActivity.HomeActivityPages.g.a(), false);
  }
  
  public void clickNotesButton()
  {
    if (UserUtils.h)
    {
      Toast.makeText(this, 2131231120, 0).show();
      return;
    }
    if (!UserUtils.e)
    {
      UserUtils.showInfoDialog(this);
      return;
    }
    if (!m.b.equals(HomeActivity.BottomBarButton.q))
    {
      DVNTAbstractAsyncAPI.cancelAllRequests();
      if (notesUnreadCountView.getVisibility() == 0) {}
      for (String str = "dot_is_shown";; str = "dot_is_hidden")
      {
        TrackerUtil.get(this, EventKeys.Category.d, "open_notes_selection", str);
        ScreenFlowManager.goHome(this);
        ScreenFlowManager.get(this, NotesFragment.getNextTimePosition(), HomeActivity.HomeActivityPages.B.a(), false);
        return;
      }
    }
  }
  
  public void clickNotificationsButton()
  {
    if (m.b.equals(HomeActivity.BottomBarButton.g)) {
      return;
    }
    DVNTAbstractAsyncAPI.cancelAllRequests();
    ScreenFlowManager.goHome(this);
    ScreenFlowManager.get(this, NotificationsFragment.getNextTimePosition(), HomeActivity.HomeActivityPages.q.a(), false);
  }
  
  public void clickSubmitButton()
  {
    DVNTAbstractAsyncAPI.cancelAllRequests();
    startActivityForResult(new SubmitActivity.IntentBuilder().a(this), 101);
    overridePendingTransition(2131034146, 2131034132);
  }
  
  public void clickWatchlistButton()
  {
    if (m.b.equals(HomeActivity.BottomBarButton.e)) {
      return;
    }
    DVNTAbstractAsyncAPI.cancelAllRequests();
    ScreenFlowManager.goHome(this);
    if (UserUtils.b == -1)
    {
      ScreenFlowManager.get(this, new WatchLoadingFragment(), "watch_feed_loading", false);
      return;
    }
    ScreenFlowManager.get(this, new WatchFeedFragment(), HomeActivity.HomeActivityPages.p.a(), false);
  }
  
  public FrameLayout createFragment()
  {
    return floatingBottomButton;
  }
  
  public DAToolTipRelativeLayout getPagerAdapter()
  {
    return toolTipLayout;
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if ((paramInt1 == 104) && ((paramInt2 == -1) || (paramInt2 == 1)) && (paramInt2 == -1)) {
      finish();
    }
    if ((paramInt1 == 105) && (paramInt2 == -1))
    {
      if (paramIntent == null)
      {
        clickDiscoveryButton();
        return;
      }
      boolean bool1 = paramIntent.getBooleanExtra("avatar_updated", false);
      boolean bool2 = paramIntent.getBooleanExtra("profile_picture_updated", false);
      boolean bool3 = paramIntent.getBooleanExtra("profile_updated", false);
      if (bool1) {
        BusStation.get().post(new BusStation.OnUserSettingChangeEvent(UserSettingUpdateType.c));
      }
      if (bool2) {
        BusStation.get().post(new BusStation.OnUserSettingChangeEvent(UserSettingUpdateType.g));
      }
      if (bool3) {
        BusStation.get().post(new BusStation.OnUserSettingChangeEvent(UserSettingUpdateType.b));
      }
      if (paramIntent.hasExtra("updated_watch_feed_setting")) {
        b((WatchFeedContentsSetting)paramIntent.getSerializableExtra("updated_watch_feed_setting"));
      }
    }
  }
  
  public void onCreate()
  {
    if (longPressLayout == null) {
      return;
    }
    longPressLayout.init(this);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    BusStation.get().register(this);
    setContentView(2130968605);
    ButterKnife.bind(this);
    if (paramBundle != null) {
      return;
    }
    onRefresh();
    AutofitHelper.doInit(notesUnreadCountView);
    if ((!h) || (!DeepLinkController.handleIntent(getIntent()))) {}
    for (int i = 1;; i = 0)
    {
      h = true;
      if (i != 0) {
        break;
      }
      DeepLinkController.loadData(this, getIntent(), false);
      return;
    }
    paramBundle = (HomeActivity.HomeActivityPages)getIntent().getSerializableExtra("home_start_page");
    if (paramBundle != null) {
      switch (HomeActivity.3.d[paramBundle.ordinal()])
      {
      default: 
        break;
      }
    }
    for (;;)
    {
      DeepLinkController.loadData(this, getIntent(), true);
      return;
      boolean bool1 = getIntent().getBooleanExtra("home_profile_add_discovery", false);
      if (bool1) {
        ScreenFlowManager.get(this, new DiscoveryFragment(), HomeActivity.HomeActivityPages.g.a(), false);
      }
      String str = getIntent().getStringExtra("home_profile_username");
      boolean bool2 = getIntent().getBooleanExtra("home_profile_is_owner", false);
      ScreenFlowManager.get(this, (Fragment)new UserProfileFragment.InstanceBuilder().a(str).a(bool2).a(), paramBundle.a(), bool1);
      continue;
      ScreenFlowManager.get(this, new WatchRecoFragment(), HomeActivity.HomeActivityPages.d.a(), false);
      continue;
      ScreenFlowManager.get(this, new DiscoveryFragment(), HomeActivity.HomeActivityPages.g.a(), false);
    }
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    NotesFolders.b();
    BusStation.get().unregister(this);
  }
  
  public void onGetUserInfo(BusStation.OnGetUserInfoEvent paramOnGetUserInfoEvent)
  {
    if (wrappedButtonAvatar != null) {
      wrappedButtonAvatar.b(this);
    }
  }
  
  public void onNoteItemChange(BusStation.OnNoteItemChangeEvent paramOnNoteItemChangeEvent)
  {
    if (!((NotesItemData)paramOnNoteItemChangeEvent.c()).getContext().getIsUnread().booleanValue()) {
      return;
    }
    if (paramOnNoteItemChangeEvent.b()) {}
    for (int i = 1; NotesFolders.get().containsKey("2E23AB5F-02FC-0EAD-F27A-BD5A251529FE"); i = -1)
    {
      a(((DVNTNotesFolder)NotesFolders.get().get("2E23AB5F-02FC-0EAD-F27A-BD5A251529FE")).getCount().intValue() + i);
      return;
    }
  }
  
  public void onNotesItemMarkUpdated(BusStation.OnNotesItemMarkUpdated paramOnNotesItemMarkUpdated)
  {
    paramOnNotesItemMarkUpdated = NotesFolders.a().update(this);
    if (paramOnNotesItemMarkUpdated == null) {
      return;
    }
    a(paramOnNotesItemMarkUpdated.intValue());
  }
  
  public void onNotesUnreadCountLoaded(BusStation.OnNotesUnreadCountLoaded paramOnNotesUnreadCountLoaded)
  {
    int i = paramOnNotesUnreadCountLoaded.c();
    if (i > s)
    {
      StreamCacher.d(NotesDefaultPage.o.a());
      StreamCacher.d(NotesDefaultPage.l.a());
    }
    a(i);
  }
  
  protected void onPause()
  {
    super.onPause();
  }
  
  public void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    m = ((HomeActivity.BottomBarState)paramBundle.getParcelable("selected_bottom_bar_state"));
    if (m.b != HomeActivity.BottomBarButton.c) {
      onCreateView(m.b).setSelected(true);
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putParcelable("selected_bottom_bar_state", m);
    super.onSaveInstanceState(paramBundle);
  }
  
  public void onSlideBottomBar(BusStation.OnSignificantScrollEvent paramOnSignificantScrollEvent)
  {
    if (UserUtils.a != null)
    {
      if (!E) {
        return;
      }
      show(c);
    }
  }
  
  public void selectItem()
  {
    super.selectItem();
    f = null;
    a = null;
  }
  
  public void show(boolean paramBoolean)
  {
    if ((homeBottomBar.getVisibility() != 0) || (!paramBoolean))
    {
      if ((homeBottomBar.getVisibility() == 8) && (!paramBoolean)) {
        return;
      }
      if (isShown) {
        homeBottomBar.getAnimation().cancel();
      }
      isShown = true;
      if (paramBoolean) {}
      for (int i = 2131034122;; i = 2131034123)
      {
        DAAnimationUtils.show(homeBottomBar, i, paramBoolean, HomeActivity..Lambda.1.getAppVersionCode(this));
        return;
      }
    }
  }
  
  public void showTabs(boolean paramBoolean)
  {
    E = paramBoolean;
    if ((paramBoolean) && (DVNTAbstractAsyncAPI.isUserSession(this)))
    {
      homeBottomBar.setVisibility(0);
      return;
    }
    homeBottomBar.setVisibility(8);
  }
}
