package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTCommentContextResponse
  implements Serializable
{
  @SerializedName("context")
  private DVNTCommentContextResponse.DVNTCommentContext context;
  @SerializedName("has_less")
  private boolean hasLess;
  @SerializedName("has_more")
  private boolean hasMore;
  @SerializedName("next_offset")
  private Integer nextOffset;
  @SerializedName("prev_offset")
  private Integer previousOffset;
  @SerializedName("thread")
  private DVNTComment.List thread;
  
  public DVNTCommentContextResponse() {}
  
  public DVNTCommentContextResponse.DVNTCommentContext getContext()
  {
    return context;
  }
  
  public boolean getHasLess()
  {
    return hasLess;
  }
  
  public boolean getHasMore()
  {
    return hasMore;
  }
  
  public Integer getNextOffset()
  {
    return nextOffset;
  }
  
  public Integer getPreviousOffset()
  {
    return previousOffset;
  }
  
  public DVNTComment.List getThread()
  {
    return thread;
  }
  
  public boolean isHasLess()
  {
    return hasLess;
  }
  
  public boolean isHasMore()
  {
    return hasMore;
  }
  
  public void setContext(DVNTCommentContextResponse.DVNTCommentContext paramDVNTCommentContext)
  {
    context = paramDVNTCommentContext;
  }
  
  public void setHasLess(boolean paramBoolean)
  {
    hasLess = paramBoolean;
  }
  
  public void setHasMore(boolean paramBoolean)
  {
    hasMore = paramBoolean;
  }
  
  public void setNextOffset(Integer paramInteger)
  {
    nextOffset = paramInteger;
  }
  
  public void setPreviousOffset(Integer paramInteger)
  {
    previousOffset = paramInteger;
  }
  
  public void setThread(DVNTComment.List paramList)
  {
    thread = paramList;
  }
}
