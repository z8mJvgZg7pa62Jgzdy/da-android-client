package com.deviantart.android.android.package_14.network;

import com.deviantart.android.android.BuildConfig;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig;
import retrofit.RequestInterceptor;
import retrofit.RequestInterceptor.RequestFacade;

public abstract class DVNTAbstractRequestInterceptor
  implements RequestInterceptor
{
  public DVNTAbstractRequestInterceptor() {}
  
  public void intercept(RequestInterceptor.RequestFacade paramRequestFacade)
  {
    Object localObject = DVNTAbstractAsyncAPI.getConfig();
    paramRequestFacade.addHeader("dA-session-id", ((DVNTAbstractAPIConfig)localObject).getSessionId());
    paramRequestFacade.addHeader("dA-minor-version", BuildConfig.API_VERSION.toString());
    if (((DVNTAbstractAPIConfig)localObject).getShowMatureContent() != null)
    {
      if (((DVNTAbstractAPIConfig)localObject).getShowMatureContent().booleanValue()) {}
      for (localObject = "1";; localObject = "0")
      {
        paramRequestFacade.addQueryParam("mature_content", (String)localObject);
        return;
      }
    }
  }
}
