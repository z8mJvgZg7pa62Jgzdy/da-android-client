package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.os.BaseBundle;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.android.package_14.model.DVNTStashItem;
import com.deviantart.android.damobile.util.DAWebViewHelper;
import com.deviantart.android.damobile.util.DAWebViewHelper.WebViewParams;
import com.deviantart.android.damobile.util.DAWebViewHelper.WebViewParams.Builder;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.markup.FallbackDAMLContent;
import com.deviantart.android.damobile.util.markup.FallbackDAMLContent.Builder;
import com.deviantart.android.damobile.view.ImageFullView;
import com.deviantart.datoolkit.logger.DVNTLog;
import java.util.ArrayList;
import java.util.List;

public class StashOverlayFragment
  extends HomeFullViewFragment
{
  @Bind({2131689811})
  View closeButton;
  @Bind({2131689719})
  FrameLayout container;
  @Bind({2131689748})
  View loading;
  DVNTStashItem mContext;
  
  public StashOverlayFragment() {}
  
  public static StashOverlayFragment a(long paramLong)
  {
    StashOverlayFragment localStashOverlayFragment = new StashOverlayFragment();
    Bundle localBundle = new Bundle();
    localBundle.putLong("stash_id", paramLong);
    localStashOverlayFragment.setArguments(localBundle);
    return localStashOverlayFragment;
  }
  
  private void createViews(DVNTImage paramDVNTImage1, DVNTImage paramDVNTImage2)
  {
    ImageFullView localImageFullView = new ImageFullView(getActivity());
    localImageFullView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
    localImageFullView.a(paramDVNTImage1, paramDVNTImage2, false, true);
    container.setBackgroundColor(0);
    container.addView(localImageFullView);
  }
  
  private void onCreateView()
  {
    DVNTImage localDVNTImage = null;
    if (mContext == null) {
      return;
    }
    container.removeAllViews();
    Object localObject1 = mContext.getThumbs();
    Object localObject2;
    if ((localObject1 != null) && (!((ArrayList)localObject1).isEmpty()))
    {
      localObject2 = ImageUtils.draw(getActivity(), (List)localObject1, (DVNTImage)((ArrayList)localObject1).get(0), Graphics.height(getActivity()), Graphics.width(getActivity()), true);
      localObject1 = localObject2;
      localDVNTImage = DeviationUtils.a(mContext);
      if ((localObject2 != null) && (((DVNTImage)localObject2).getSrc() != null) && (((DVNTImage)localObject2).getSrc().equals(localDVNTImage.getSrc()))) {
        localObject1 = null;
      }
    }
    for (;;)
    {
      localObject2 = new FallbackDAMLContent.Builder().c(mContext.getHtml()).a().c();
      if ((localDVNTImage == null) && (localObject2 == null))
      {
        Log.e("StashOverlay", "no valid data from stash media to show");
        getActivity().onBackPressed();
      }
      if (localDVNTImage != null)
      {
        createViews(localDVNTImage, (DVNTImage)localObject1);
        return;
      }
      if (localObject2 == null) {
        break;
      }
      onCreateView((String)localObject2);
      return;
      continue;
      localObject1 = null;
    }
  }
  
  private void onCreateView(String paramString)
  {
    WebView localWebView = new WebView(getActivity());
    DAWebViewHelper.WebViewParams localWebViewParams = new DAWebViewHelper.WebViewParams.Builder().b("transparent").a("#" + Integer.toHexString(getActivity().getResources().getColor(2131558505))).b(Integer.valueOf(20)).a(mContext.getCssFonts()).a(Integer.valueOf(getActivity().getResources().getDimensionPixelSize(2131361995))).c(Integer.valueOf(getActivity().getResources().getDimensionPixelSize(2131361994))).a(Boolean.valueOf(true)).a();
    localWebView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
    localWebView.setVerticalScrollBarEnabled(false);
    localWebView.setHorizontalScrollBarEnabled(false);
    DAWebViewHelper.onCreateView(localWebView, paramString, mContext.getCss(), localWebViewParams);
    container.setBackgroundColor(getActivity().getResources().getColor(2131558504));
    container.addView(localWebView);
  }
  
  private void update(long paramLong)
  {
    container.setVisibility(8);
    closeButton.setVisibility(8);
    loading.setVisibility(0);
    DVNTCommonAsyncAPI.stashFetchItem(Long.valueOf(paramLong)).call(getActivity(), new StashOverlayFragment.1(this));
  }
  
  public void onClickClose()
  {
    getActivity().onBackPressed();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (getArguments().containsKey("stash_media")) {
      mContext = ((DVNTStashItem)getArguments().getSerializable("stash_media"));
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = paramLayoutInflater.inflate(2130968681, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    long l = getArguments().getLong("stash_id", -1L);
    if ((mContext == null) && (l == -1L))
    {
      DVNTLog.append("No stash item to show", new Object[0]);
      getActivity().onBackPressed();
    }
    if (mContext == null)
    {
      update(l);
      return paramLayoutInflater;
    }
    onCreateView();
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
}
