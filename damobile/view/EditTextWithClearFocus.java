package com.deviantart.android.damobile.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

public class EditTextWithClearFocus
  extends EditText
{
  public EditTextWithClearFocus(Context paramContext)
  {
    super(paramContext);
  }
  
  public EditTextWithClearFocus(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((paramInt == 4) && (paramKeyEvent.getAction() == 1)) {
      clearFocus();
    }
    return false;
  }
}
