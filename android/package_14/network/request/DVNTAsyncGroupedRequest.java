package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.wrapper.DVNTGroupedRequestResponse;
import com.deviantart.android.android.package_14.network.wrapper.DVNTResultsWrapper;
import com.google.common.base.Objects;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class DVNTAsyncGroupedRequest
  extends com.deviantart.android.sdk.api.network.request.DVNTBaseAsyncRequest<HashMap, com.deviantart.android.sdk.api.network.service.DVNTServiceV1Flavored>
{
  public static final int GROUP_MAX_SIZE = 10;
  private HashMap<String, com.deviantart.android.sdk.api.network.request.GroupableRequest> requests;
  
  public DVNTAsyncGroupedRequest(HashMap paramHashMap)
  {
    super(HashMap.class, com.deviantart.android.sdk.api.network.service.DVNTServiceV1Flavored.class);
    requests = paramHashMap;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()) && (super.equals(paramObject)))
    {
      paramObject = (DVNTAsyncGroupedRequest)paramObject;
      return Objects.append(requests, requests);
    }
    return false;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), requests });
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected HashMap sendRequest(String paramString)
  {
    HashMap localHashMap1 = new HashMap();
    HashMap localHashMap2 = new HashMap();
    Object localObject1 = new ArrayList();
    Gson localGson = new Gson();
    Object localObject2 = requests.entrySet().iterator();
    int i = 10;
    Object localObject3;
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = (Map.Entry)((Iterator)localObject2).next();
      if (i < 0) {
        throw new RuntimeException("You can't group more than 10 requests at once");
      }
      GroupableRequest localGroupableRequest = (GroupableRequest)((Map.Entry)localObject3).getValue();
      ((ArrayList)localObject1).add(new DVNTAsyncGroupedRequest.DVNTAggregateRequest(this, (String)((Map.Entry)localObject3).getKey(), localGroupableRequest.getGroupEndpoint(), localGroupableRequest.getGETArguments(), localGroupableRequest.getPOSTArguments()));
      localHashMap1.put(((Map.Entry)localObject3).getKey(), localGroupableRequest.getResultType());
      localHashMap2.put(((Map.Entry)localObject3).getKey(), localGroupableRequest.getWrapperTypeToken());
      i -= 1;
    }
    localObject1 = localGson.toJson(localObject1);
    localObject1 = (ArrayList)((com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored)getService()).aggregate(paramString, (String)localObject1, Boolean.valueOf(true)).getResults();
    paramString = new HashMap();
    localObject1 = ((ArrayList)localObject1).iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (DVNTGroupedRequestResponse)((Iterator)localObject1).next();
      localObject3 = localGson.toJson(((DVNTGroupedRequestResponse)localObject2).getResult());
      if ((TypeToken)localHashMap2.get(((DVNTGroupedRequestResponse)localObject2).getKey()) != null) {
        paramString.put(((DVNTGroupedRequestResponse)localObject2).getKey(), localGson.fromJson((String)localObject3, ((TypeToken)localHashMap2.get(((DVNTGroupedRequestResponse)localObject2).getKey())).getType()));
      } else {
        paramString.put(((DVNTGroupedRequestResponse)localObject2).getKey(), localGson.fromJson((String)localObject3, (Class)localHashMap1.get(((DVNTGroupedRequestResponse)localObject2).getKey())));
      }
    }
    return paramString;
  }
}
