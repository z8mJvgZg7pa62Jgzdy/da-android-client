package com.deviantart.android.damobile.adapter.recyclerview;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.adapter.HeaderFooterRecyclerViewAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.WatchReco;
import com.deviantart.android.damobile.util.WatchRecoUpdateListener;
import com.deviantart.android.damobile.view.thirdparty.ByteVector;
import com.deviantart.android.damobile.view.thirdparty.GomFactory;
import com.deviantart.android.damobile.view.thirdparty.GomType;
import com.deviantart.android.damobile.view.thirdparty.decorator.DecoratorGomHelper;
import com.deviantart.android.damobile.view.thirdparty.decorator.DecoratorType;
import com.deviantart.android.damobile.view.thirdparty.decorator.DecoratorViewHolder;
import java.util.ArrayList;

public class WatchRecoListAdapter
  extends DAStateRecyclerViewAdapter<com.deviantart.android.sdk.api.model.DVNTWatchRecommendationItem>
  implements WatchRecoUpdateListener
{
  private DecoratorGomHelper o = new DecoratorGomHelper(new DecoratorType[] { DecoratorType.b });
  
  public WatchRecoListAdapter(Stream paramStream)
  {
    super(paramStream);
  }
  
  public void a(boolean paramBoolean, com.deviantart.android.android.package_14.model.DVNTWatchRecommendationItem paramDVNTWatchRecommendationItem)
  {
    com.deviantart.android.damobile.fragment.WatchFeedFragment.p = true;
    paramDVNTWatchRecommendationItem.getUser().setIsWatching(Boolean.valueOf(paramBoolean));
    try
    {
      c();
      return;
    }
    catch (Exception paramDVNTWatchRecommendationItem)
    {
      new Handler().post(WatchRecoListAdapter..Lambda.1.c(this));
    }
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    RecyclerView.ViewHolder localViewHolder = GomFactory.a(GomType.b).b(paramViewGroup.getContext(), paramViewGroup);
    return o.a(paramViewGroup.getContext(), paramViewGroup, localViewHolder);
  }
  
  public void b(Context paramContext, com.deviantart.android.android.package_14.model.DVNTWatchRecommendationItem paramDVNTWatchRecommendationItem, int paramInt)
  {
    if (paramDVNTWatchRecommendationItem.equals(get().get(paramInt))) {}
    for (;;)
    {
      int i = paramInt;
      if (paramInt == -1) {
        i = get().get().indexOf(paramDVNTWatchRecommendationItem);
      }
      if (i == -1) {
        return;
      }
      try
      {
        add(i);
        get().close(i);
        e -= 1;
        if (DVNTContextUtils.isContextDead(paramContext)) {
          break;
        }
        paramDVNTWatchRecommendationItem = paramDVNTWatchRecommendationItem.getUser().getUserUUID();
        DVNTAsyncAPI.unsuggestArtist(paramDVNTWatchRecommendationItem).call(paramContext, new WatchRecoListAdapter.1(this, paramDVNTWatchRecommendationItem));
        return;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          new Handler().post(WatchRecoListAdapter..Lambda.2.c(this));
        }
      }
      paramInt = -1;
    }
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    if (!(paramViewHolder instanceof DecoratorViewHolder)) {
      return;
    }
    DecoratorViewHolder localDecoratorViewHolder = (DecoratorViewHolder)paramViewHolder;
    WatchReco localWatchReco = new WatchReco((com.deviantart.android.android.package_14.model.DVNTWatchRecommendationItem)get().get(paramInt), this, paramInt);
    o.a(itemView.getContext(), localDecoratorViewHolder, localWatchReco);
    paramViewHolder = DecoratorGomHelper.b(localDecoratorViewHolder);
    GomFactory.a(GomType.b).b(itemView.getContext(), localWatchReco, paramViewHolder);
  }
}
