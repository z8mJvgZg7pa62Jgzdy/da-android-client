package com.deviantart.android.android.package_14.model;

import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTWatchRecommendationItem
  implements Serializable
{
  @SerializedName("deviations")
  DVNTAbstractDeviation.List deviations;
  @SerializedName("user")
  DVNTUser user;
  
  public DVNTWatchRecommendationItem() {}
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    paramObject = (DVNTWatchRecommendationItem)paramObject;
    return (Objects.append(user, user)) && (Objects.append(deviations, deviations));
  }
  
  public DVNTAbstractDeviation.List getDeviations()
  {
    return deviations;
  }
  
  public DVNTUser getUser()
  {
    return user;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { user, deviations });
  }
  
  public void setDeviations(DVNTAbstractDeviation.List paramList)
  {
    deviations = paramList;
  }
  
  public void setUser(DVNTUser paramDVNTUser)
  {
    user = paramDVNTUser;
  }
}
