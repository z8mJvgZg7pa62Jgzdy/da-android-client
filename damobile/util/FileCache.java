package com.deviantart.android.damobile.util;

import android.content.Context;
import java.io.File;

public class FileCache
{
  private static File file;
  private static File folder;
  
  public FileCache() {}
  
  public static File create(Context paramContext)
  {
    if (folder == null)
    {
      folder = new File(paramContext.getFilesDir(), "image_cache");
      folder.mkdirs();
    }
    return folder;
  }
  
  private static void delete(File paramFile)
  {
    paramFile = paramFile.listFiles();
    if (paramFile == null) {
      return;
    }
    int j = paramFile.length;
    int i = 0;
    while (i < j)
    {
      paramFile[i].delete();
      i += 1;
    }
  }
  
  public static File getFile(Context paramContext)
  {
    if (file == null)
    {
      file = new File(paramContext.getFilesDir(), "resolved_content_cache");
      file.mkdirs();
    }
    return file;
  }
  
  public static void initialize(Context paramContext)
  {
    delete(create(paramContext));
    delete(getFile(paramContext));
  }
}
