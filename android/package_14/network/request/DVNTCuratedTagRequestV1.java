package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.android.package_14.network.wrapper.DVNTResultsWrapper;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTCuratedTagRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTTag.List>
{
  private boolean getPreload;
  
  public DVNTCuratedTagRequestV1(boolean paramBoolean)
  {
    super(com.deviantart.android.sdk.api.model.DVNTTag.List.class);
    getPreload = paramBoolean;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTCuratedTagRequestV1 localDVNTCuratedTagRequestV1 = (DVNTCuratedTagRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(getPreload, getPreload).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "curatedtags";
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 120000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(getPreload).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTTag.List sendRequest(String paramString)
  {
    return (com.deviantart.android.android.package_14.model.DVNTTag.List)((DVNTServiceV1)getService()).curatedTag(paramString, getPreload).getResults();
  }
}
