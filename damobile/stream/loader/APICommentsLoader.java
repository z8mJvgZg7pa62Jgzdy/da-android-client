package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.damobile.util.CommentType;
import com.deviantart.android.sdk.api.model.DVNTComment;

public class APICommentsLoader
  extends StreamLoader<DVNTComment>
{
  private CommentType b;
  private String d;
  private String e;
  
  public APICommentsLoader(CommentType paramCommentType, String paramString1, String paramString2)
  {
    d = paramString1;
    e = paramString2;
    b = paramCommentType;
  }
  
  public String b()
  {
    return "commentsloader" + d + "|" + e + "|" + b;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    APICommentsLoader.1 local1 = new APICommentsLoader.1(this, paramStreamLoadListener);
    paramStreamLoadListener = null;
    switch (APICommentsLoader.2.d[b.ordinal()])
    {
    default: 
      break;
    }
    for (;;)
    {
      if (paramBoolean) {
        paramStreamLoadListener.noCache();
      }
      paramStreamLoadListener.call(paramContext, local1);
      return;
      paramStreamLoadListener = DVNTCommonAsyncAPI.commentsForDeviation(d, e, Integer.valueOf(paramInt), Integer.valueOf(a));
      continue;
      paramStreamLoadListener = DVNTCommonAsyncAPI.commentsForUser(d, e, Integer.valueOf(paramInt), Integer.valueOf(a));
      continue;
      paramStreamLoadListener = DVNTCommonAsyncAPI.commentsForStatus(d, e, Integer.valueOf(paramInt), Integer.valueOf(a));
    }
  }
}
