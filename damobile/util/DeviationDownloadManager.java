package com.deviantart.android.damobile.util;

import android.content.Context;
import android.content.IntentFilter;
import java.util.ArrayList;

public class DeviationDownloadManager
{
  private static DeviationDownloadReceiver broadcastReceiver;
  public static ArrayList<Long> this$0 = new ArrayList();
  
  public DeviationDownloadManager() {}
  
  public static void registerReceiver(Context paramContext)
  {
    if (broadcastReceiver == null) {
      broadcastReceiver = new DeviationDownloadReceiver();
    }
    paramContext.registerReceiver(broadcastReceiver, new IntentFilter("android.intent.action.DOWNLOAD_COMPLETE"));
  }
  
  public static void unregisterReceiver(Context paramContext)
  {
    if (broadcastReceiver == null) {
      return;
    }
    DeviationDownloadReceiver localDeviationDownloadReceiver = broadcastReceiver;
    try
    {
      paramContext.unregisterReceiver(localDeviationDownloadReceiver);
      return;
    }
    catch (IllegalArgumentException paramContext) {}
  }
}
