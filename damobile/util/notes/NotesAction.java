package com.deviantart.android.damobile.util.notes;

import android.content.Context;
import android.util.Log;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTNote;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnNotesItemMarkUpdated;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.List;

public class NotesAction
{
  public NotesAction() {}
  
  public static void a(Context paramContext, NotesItemData.Observable paramObservable, Mark paramMark)
  {
    b(paramContext, paramObservable, paramMark, false);
  }
  
  public static void b(Context paramContext, NotesItemData.Observable paramObservable, Mark paramMark, boolean paramBoolean)
  {
    paramMark.b().b(paramObservable.a());
    paramObservable.b();
    paramMark.f();
    try
    {
      localObject = BusStation.get();
      ((Bus)localObject).post(new BusStation.OnNotesItemMarkUpdated(paramObservable.a(), paramMark));
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;)
      {
        Object localObject;
        Log.e("NotesAction", "could not post bus for markUpdated");
      }
    }
    localObject = new ArrayList();
    ((ArrayList)localObject).add(paramObservable.a().getContext().getNoteId());
    DVNTAsyncAPI.markNotes((List)localObject, paramMark.i()).call(paramContext, new NotesAction.1(paramContext, paramMark, paramObservable, paramBoolean));
  }
}
