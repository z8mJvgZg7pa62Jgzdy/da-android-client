package com.deviantart.android.damobile.util.scrolling;

import com.deviantart.android.damobile.view.EndlessScrollListener;
import com.deviantart.android.damobile.view.EndlessScrollOptions;

public class RecyclerViewEndlessScrollListener
  extends RecyclerViewScrollListenerBase
{
  EndlessScrollOptions o;
  
  public RecyclerViewEndlessScrollListener(EndlessScrollOptions paramEndlessScrollOptions)
  {
    o = paramEndlessScrollOptions;
  }
  
  public void b(int paramInt1, int paramInt2, int paramInt3)
  {
    if (o == null) {
      return;
    }
    if (paramInt2 - (paramInt3 + paramInt1) <= o.b()) {
      o.a().a();
    }
  }
}
