package com.deviantart.android.android.package_14.session;

import com.deviantart.android.android.package_14.network.request.DVNTBaseAsyncRequest;
import com.deviantart.datoolkit.logger.DVNTLog;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.listener.RequestListener;

public class DVNTSessionManager
{
  private String format = "DVNTSessionManager - {}";
  protected final SpiceManager spiceManager;
  
  public DVNTSessionManager(SpiceManager paramSpiceManager)
  {
    spiceManager = paramSpiceManager;
  }
  
  public void asyncExecute(DVNTBaseAsyncRequest paramDVNTBaseAsyncRequest, RequestListener paramRequestListener)
  {
    String str = paramDVNTBaseAsyncRequest.getCacheUniqueId();
    long l = paramDVNTBaseAsyncRequest.getCacheLifeTimeInMillis();
    if (paramDVNTBaseAsyncRequest.shouldWipeCacheFirst()) {
      spiceManager.removeDataFromCache(paramDVNTBaseAsyncRequest.getResponseClass());
    }
    spiceManager.execute(paramDVNTBaseAsyncRequest, str, l, paramRequestListener);
  }
  
  public boolean isStarted()
  {
    return spiceManager.isStarted();
  }
  
  public void shouldStop()
  {
    try
    {
      DVNTLog.get(format, new Object[] { " - stopped - requests left to run :", Integer.valueOf(spiceManager.getRequestToLaunchCount()) });
      spiceManager.shouldStop();
      return;
    }
    catch (Throwable localThrowable)
    {
      throw localThrowable;
    }
  }
  
  /* Error */
  public void start(android.content.Context paramContext)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 19	com/deviantart/android/android/package_14/session/DVNTSessionManager:spiceManager	Lcom/octo/android/robospice/SpiceManager;
    //   6: invokevirtual 53	com/octo/android/robospice/SpiceManager:isStarted	()Z
    //   9: ifne +7 -> 16
    //   12: aload_1
    //   13: ifnonnull +22 -> 35
    //   16: aload_0
    //   17: getfield 17	com/deviantart/android/android/package_14/session/DVNTSessionManager:format	Ljava/lang/String;
    //   20: iconst_1
    //   21: anewarray 4	java/lang/Object
    //   24: dup
    //   25: iconst_0
    //   26: ldc 80
    //   28: aastore
    //   29: invokestatic 74	com/deviantart/datoolkit/logger/DVNTLog:get	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   32: aload_0
    //   33: monitorexit
    //   34: return
    //   35: aload_0
    //   36: getfield 19	com/deviantart/android/android/package_14/session/DVNTSessionManager:spiceManager	Lcom/octo/android/robospice/SpiceManager;
    //   39: aload_1
    //   40: invokevirtual 82	com/octo/android/robospice/SpiceManager:start	(Landroid/content/Context;)V
    //   43: goto -11 -> 32
    //   46: astore_1
    //   47: aload_0
    //   48: monitorexit
    //   49: aload_1
    //   50: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	51	0	this	DVNTSessionManager
    //   0	51	1	paramContext	android.content.Context
    // Exception table:
    //   from	to	target	type
    //   2	12	46	java/lang/Throwable
    //   16	32	46	java/lang/Throwable
    //   35	43	46	java/lang/Throwable
  }
  
  public void stop()
  {
    if (!spiceManager.isStarted())
    {
      DVNTLog.get(format, new Object[] { "can't stop manager because it's not started" });
      return;
    }
    shouldStop();
  }
}
