package com.deviantart.android.damobile.view.dialogs;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;

public class AlertDialogsBuilders
{
  public AlertDialogsBuilders() {}
  
  public static AlertDialog.Builder createDialog(Activity paramActivity)
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramActivity);
    localBuilder.setTitle(paramActivity.getString(2131231082)).setMessage(paramActivity.getString(2131231084)).setPositiveButton(2131231009, new AlertDialogsBuilders.5(paramActivity)).setNegativeButton(2131231183, new AlertDialogsBuilders.4());
    return localBuilder;
  }
  
  public static AlertDialog.Builder createDialog(Context paramContext)
  {
    return new AlertDialog.Builder(paramContext).setTitle(2131230946).setMessage(2131230949).setCancelable(false).setPositiveButton(2131231183, new AlertDialogsBuilders.6());
  }
  
  public static AlertDialog.Builder deleteAlarm(Context paramContext)
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
    localBuilder.setTitle(paramContext.getString(2131231082)).setMessage(paramContext.getString(2131231083)).setPositiveButton(2131231054, new AlertDialogsBuilders.3(paramContext)).setNegativeButton(2131231183, new AlertDialogsBuilders.2());
    return localBuilder;
  }
  
  public static AlertDialog.Builder showConfirmationDialog(Context paramContext)
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
    localBuilder.setTitle(paramContext.getString(2131231082)).setMessage(paramContext.getString(2131231081)).setPositiveButton(2131231183, new AlertDialogsBuilders.1());
    return localBuilder;
  }
}
