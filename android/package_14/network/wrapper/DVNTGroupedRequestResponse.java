package com.deviantart.android.android.package_14.network.wrapper;

public class DVNTGroupedRequestResponse
{
  private String range;
  private Object result;
  
  public DVNTGroupedRequestResponse() {}
  
  public String getKey()
  {
    return range;
  }
  
  public Object getResult()
  {
    return result;
  }
}
