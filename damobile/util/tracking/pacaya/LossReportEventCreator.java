package com.deviantart.android.damobile.util.tracking.pacaya;

import com.deviantart.upgrade.labs.DVNTTopicEvent;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class LossReportEventCreator
  extends DebugTopicEventCreator
{
  public LossReportEventCreator(List paramList, String paramString)
  {
    a("lost_events");
    int j = paramList.size();
    HashMap localHashMap = new HashMap();
    Object localObject = paramList.iterator();
    if (((Iterator)localObject).hasNext())
    {
      DVNTTopicEvent localDVNTTopicEvent = (DVNTTopicEvent)((Iterator)localObject).next();
      String str = c;
      if (localHashMap.containsKey(c)) {}
      for (int i = ((Integer)localHashMap.get(c)).intValue() + 1;; i = 1)
      {
        localHashMap.put(str, Integer.valueOf(i));
        break;
      }
    }
    localObject = get0f;
    paramList = get1f;
    add("why", paramString);
    add("lost_count", localHashMap);
    add("lost_event_range", new String[] { localObject, paramList });
  }
}
