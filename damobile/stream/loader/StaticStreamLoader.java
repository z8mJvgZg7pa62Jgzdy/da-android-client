package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import java.util.ArrayList;
import java.util.List;

public class StaticStreamLoader<T>
  extends StreamLoader<T>
{
  public static String H = "staticstreamloader-deviations-simpledaml";
  public static String K = "staticstreamloader-deviations-simpledaml";
  private final ArrayList<T> c = new ArrayList();
  private final String d;
  
  public StaticStreamLoader(Object paramObject, String paramString)
  {
    a(paramObject);
    d = paramString;
  }
  
  public StaticStreamLoader(String paramString)
  {
    d = paramString;
  }
  
  public void a(Object paramObject)
  {
    c.add(paramObject);
  }
  
  public void a(List paramList)
  {
    c.addAll(paramList);
  }
  
  public String b()
  {
    return d;
  }
  
  public int c()
  {
    return c.size();
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    if (c != null)
    {
      if (c.size() <= paramInt) {
        return;
      }
      int i;
      label72:
      ArrayList localArrayList;
      if (c.size() < a + paramInt)
      {
        i = c.size() - paramInt;
        paramContext = c.subList(paramInt, i);
        if (paramContext.size() < a) {
          break label115;
        }
        paramBoolean = true;
        localArrayList = new ArrayList(paramContext);
        if (!paramBoolean) {
          break label120;
        }
      }
      label115:
      label120:
      for (paramContext = Integer.valueOf(a + paramInt);; paramContext = null)
      {
        paramStreamLoadListener.a(localArrayList, paramBoolean, paramContext);
        return;
        i = a;
        break;
        paramBoolean = false;
        break label72;
      }
    }
  }
}
