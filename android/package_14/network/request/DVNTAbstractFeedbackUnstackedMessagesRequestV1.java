package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.model.DVNTFeedbackType;
import com.deviantart.android.sdk.api.model.DVNTFeedbackMessages;
import com.google.common.base.Objects;

public abstract class DVNTAbstractFeedbackUnstackedMessagesRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<DVNTFeedbackMessages>
{
  protected Integer limit;
  protected String messageFolderId;
  protected Integer offset;
  protected DVNTFeedbackType type;
  
  public DVNTAbstractFeedbackUnstackedMessagesRequestV1(DVNTFeedbackType paramDVNTFeedbackType, String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    super(DVNTFeedbackMessages.class);
    type = paramDVNTFeedbackType;
    messageFolderId = paramString;
    offset = paramInteger1;
    limit = paramInteger2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTAbstractFeedbackUnstackedMessagesRequestV1)paramObject;
    return (Objects.append(type, type)) && (Objects.append(messageFolderId, messageFolderId)) && (Objects.append(offset, offset)) && (Objects.append(limit, limit));
  }
  
  public String getCacheKey()
  {
    return "feedbackunstackedmessages" + type + messageFolderId + offset + limit;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), type, messageFolderId, offset, limit });
  }
}
