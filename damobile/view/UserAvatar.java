package com.deviantart.android.damobile.view;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.damobile.util.UserUtils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.AbstractDraweeControllerBuilder;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.DraweeView;
import com.facebook.drawee.view.SimpleDraweeView;
import java.io.File;

public class UserAvatar
  extends SimpleDraweeView
{
  protected File f;
  protected String p;
  
  public UserAvatar(Context paramContext)
  {
    super(paramContext);
    b(paramContext);
  }
  
  public UserAvatar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    b(paramContext);
  }
  
  public UserAvatar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    b(paramContext);
  }
  
  public void b(Context paramContext)
  {
    Uri localUri;
    if ((DVNTAbstractAsyncAPI.isUserSession(paramContext)) && (UserUtils.a() != null))
    {
      setVisibility(0);
      if ((f != null) && (UserUtils.a().equals(f))) {
        return;
      }
      localUri = Uri.fromFile(UserUtils.a());
    }
    for (Object localObject = new UserAvatar.1(this);; localObject = new UserAvatar.2(this))
    {
      setHierarchy(new GenericDraweeHierarchyBuilder(paramContext.getResources()).a(100).a(paramContext.getResources().getDrawable(2130837714)).c());
      setController(((PipelineDraweeControllerBuilder)((PipelineDraweeControllerBuilder)((PipelineDraweeControllerBuilder)Fresco.b().a(getController())).a(true)).write(localUri).b((ControllerListener)localObject)).visitAnnotation());
      return;
      if ((!DVNTAbstractAsyncAPI.isUserSession(paramContext)) || (UserUtils.getP() == null)) {
        break;
      }
      setVisibility(0);
      if ((p != null) && (UserUtils.getP().equals(p))) {
        return;
      }
      localUri = Uri.parse(UserUtils.getP());
    }
    setVisibility(8);
  }
}
