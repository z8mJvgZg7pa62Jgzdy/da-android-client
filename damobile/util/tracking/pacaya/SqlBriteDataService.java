package com.deviantart.android.damobile.util.tracking.pacaya;

import android.content.Context;
import android.database.Cursor;
import com.deviantart.pacaya.LocalStorageService;
import com.deviantart.sdk.log.DVNTTopicEvent;
import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.functions.Func1;

public class SqlBriteDataService
  implements LocalStorageService<DVNTTopicEvent>
{
  private final Func1<Cursor, DVNTTopicEvent> distance;
  private Logger log;
  private final BriteDatabase path;
  
  public SqlBriteDataService(Context paramContext)
  {
    path = SqlBrite.b().persist(new SqlBriteDataService.SqlBriteOpenHelper(paramContext));
    distance = new SqlBriteTopicEvent.SqlBriteRowMapper();
    log = LoggerFactory.getLogger("SqlBrite");
  }
  
  private Observable register(String paramString)
  {
    try
    {
      log.debug("listInternal - start");
      paramString = Observable.create(new SqlBriteDataService.1(this, paramString)).toList();
      return paramString;
    }
    catch (Throwable paramString)
    {
      throw paramString;
    }
  }
  
  public Observable call()
  {
    Cursor localCursor = path.query("SELECT COUNT(*) as total FROM events", new String[0]);
    localCursor.moveToFirst();
    return Observable.get(Integer.valueOf(localCursor.getInt(0)));
  }
  
  public Observable call(String paramString)
  {
    int i = path.delete("events", "_id <= ?", new String[] { Long.toString(Long.parseLong(paramString, 16)) });
    log.info("deleted {} rows", Integer.valueOf(i));
    return Observable.get(Integer.valueOf(i));
  }
  
  /* Error */
  public Observable call(java.util.List paramList)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 35	com/deviantart/android/damobile/util/tracking/pacaya/SqlBriteDataService:path	Lcom/squareup/sqlbrite/BriteDatabase;
    //   6: invokevirtual 143	com/squareup/sqlbrite/BriteDatabase:get	()Lcom/squareup/sqlbrite/BriteDatabase$Transaction;
    //   9: astore_2
    //   10: aload_1
    //   11: invokeinterface 149 1 0
    //   16: astore_3
    //   17: aload_3
    //   18: invokeinterface 154 1 0
    //   23: ifeq +63 -> 86
    //   26: aload_3
    //   27: invokeinterface 158 1 0
    //   32: checkcast 160	com/deviantart/upgrade/labs/DVNTTopicEvent
    //   35: astore 4
    //   37: aload_0
    //   38: getfield 35	com/deviantart/android/damobile/util/tracking/pacaya/SqlBriteDataService:path	Lcom/squareup/sqlbrite/BriteDatabase;
    //   41: ldc 117
    //   43: aload 4
    //   45: invokestatic 166	com/deviantart/android/damobile/util/tracking/pacaya/SqlBriteTopicEvent:getContentValues	(Lcom/deviantart/upgrade/labs/DVNTTopicEvent;)Landroid/content/ContentValues;
    //   48: invokevirtual 170	com/squareup/sqlbrite/BriteDatabase:add	(Ljava/lang/String;Landroid/content/ContentValues;)J
    //   51: ldc2_w 171
    //   54: lcmp
    //   55: ifne -38 -> 17
    //   58: aload_0
    //   59: getfield 50	com/deviantart/android/damobile/util/tracking/pacaya/SqlBriteDataService:log	Lorg/slf4j/Logger;
    //   62: ldc -82
    //   64: invokeinterface 177 2 0
    //   69: goto -52 -> 17
    //   72: astore_1
    //   73: aload_2
    //   74: invokeinterface 181 1 0
    //   79: aload_1
    //   80: athrow
    //   81: astore_1
    //   82: aload_0
    //   83: monitorexit
    //   84: aload_1
    //   85: athrow
    //   86: aload_2
    //   87: invokeinterface 184 1 0
    //   92: aload_0
    //   93: getfield 50	com/deviantart/android/damobile/util/tracking/pacaya/SqlBriteDataService:log	Lorg/slf4j/Logger;
    //   96: ldc -70
    //   98: aload_1
    //   99: invokeinterface 190 1 0
    //   104: invokestatic 111	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   107: invokeinterface 139 3 0
    //   112: aload_2
    //   113: invokeinterface 181 1 0
    //   118: aload_1
    //   119: aload_1
    //   120: invokeinterface 190 1 0
    //   125: iconst_1
    //   126: isub
    //   127: invokeinterface 193 2 0
    //   132: invokestatic 115	rx/Observable:get	(Ljava/lang/Object;)Lrx/Observable;
    //   135: astore_1
    //   136: aload_0
    //   137: monitorexit
    //   138: aload_1
    //   139: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	140	0	this	SqlBriteDataService
    //   0	140	1	paramList	java.util.List
    //   9	104	2	localTransaction	com.squareup.sqlbrite.BriteDatabase.Transaction
    //   16	11	3	localIterator	java.util.Iterator
    //   35	9	4	localDVNTTopicEvent	com.deviantart.upgrade.labs.DVNTTopicEvent
    // Exception table:
    //   from	to	target	type
    //   10	17	72	java/lang/Throwable
    //   17	69	72	java/lang/Throwable
    //   86	112	72	java/lang/Throwable
    //   2	10	81	java/lang/Throwable
    //   73	81	81	java/lang/Throwable
    //   112	136	81	java/lang/Throwable
  }
  
  public Observable create(int paramInt)
  {
    log.info("call list() with limit {}", Integer.valueOf(paramInt));
    return register("LIMIT " + paramInt);
  }
  
  public Observable toString(Integer paramInteger)
  {
    return Observable.get(Integer.valueOf(path.delete("events", "_id < (SELECT _id FROM events LIMIT " + paramInteger + ", 1)", new String[0])));
  }
}
