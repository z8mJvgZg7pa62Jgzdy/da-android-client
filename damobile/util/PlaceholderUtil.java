package com.deviantart.android.damobile.util;

import android.view.LayoutInflater;
import android.widget.TextView;

public class PlaceholderUtil
{
  public PlaceholderUtil() {}
  
  public static TextView createView(LayoutInflater paramLayoutInflater, int paramInt)
  {
    paramLayoutInflater = (TextView)paramLayoutInflater.inflate(2130968757, null);
    paramLayoutInflater.setText(paramInt);
    return paramLayoutInflater;
  }
}
