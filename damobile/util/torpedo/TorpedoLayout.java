package com.deviantart.android.damobile.util.torpedo;

import android.content.Context;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.R.styleable;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.view.DASwipeRefreshLayout;
import com.deviantart.android.damobile.view.RefreshListener;

public class TorpedoLayout
  extends FrameLayout
{
  private boolean b;
  private RefreshListener onClickListener;
  @Bind({2131690078})
  TorpedoRecyclerView recyclerView;
  @Bind({2131690077})
  DASwipeRefreshLayout swipeRefreshLayout;
  
  public TorpedoLayout(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public TorpedoLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initAttributes(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public TorpedoLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    initAttributes(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  private boolean handleMove(boolean paramBoolean)
  {
    if (paramBoolean) {
      getParent().requestDisallowInterceptTouchEvent(true);
    }
    return paramBoolean;
  }
  
  private void init(Context paramContext)
  {
    View.inflate(paramContext, 2130968786, this);
    ButterKnife.bind(this);
    swipeRefreshLayout.setEnabled(b);
    swipeRefreshLayout.setOnRefreshListener(new TorpedoLayout.1(this));
  }
  
  private void initAttributes(Context paramContext, AttributeSet paramAttributeSet)
  {
    paramContext = paramContext.getTheme().obtainStyledAttributes(paramAttributeSet, R.styleable.TorpedoGrid, 0, 0);
    try
    {
      b = paramContext.getBoolean(0, false);
      paramContext.recycle();
      return;
    }
    catch (Throwable paramAttributeSet)
    {
      paramContext.recycle();
      throw paramAttributeSet;
    }
  }
  
  public TorpedoAdapter getAdapter()
  {
    return recyclerView.getAdapter();
  }
  
  public Stream getDeviationStream()
  {
    return recyclerView.getDeviationStream();
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    if (b) {
      return super.onInterceptTouchEvent(paramMotionEvent);
    }
    return handleMove(recyclerView.onInterceptTouchEvent(paramMotionEvent));
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (b) {
      return super.onTouchEvent(paramMotionEvent);
    }
    return handleMove(recyclerView.onTouchEvent(paramMotionEvent));
  }
  
  public void setIsFromDeepLink(boolean paramBoolean)
  {
    if (recyclerView.getAdapter() == null) {
      return;
    }
    recyclerView.getAdapter().setDisplayMode(paramBoolean);
  }
  
  public void setIsRefreshing(boolean paramBoolean)
  {
    if (swipeRefreshLayout != null) {
      swipeRefreshLayout.setRefreshing(paramBoolean);
    }
  }
  
  public void setProgressViewOffset(int paramInt)
  {
    if (swipeRefreshLayout != null) {
      swipeRefreshLayout.setProgressViewOffset(paramInt);
    }
  }
  
  public void setRefreshListener(RefreshListener paramRefreshListener)
  {
    onClickListener = paramRefreshListener;
  }
  
  public void setRefreshable(boolean paramBoolean)
  {
    b = paramBoolean;
  }
  
  public void updateListView()
  {
    if (swipeRefreshLayout == null) {
      return;
    }
    swipeRefreshLayout.setRefreshing(false);
  }
}
