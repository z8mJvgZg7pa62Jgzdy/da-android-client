package com.deviantart.android.damobile.view.tooltip;

import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewManager;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTip.AnimationType;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.view.ViewHelper;
import java.util.ArrayList;
import java.util.Collection;

public class DAToolTipView
  extends LinearLayout
  implements View.OnClickListener, ViewTreeObserver.OnPreDrawListener
{
  private View a;
  private View button;
  private ViewGroup content;
  private TextView header;
  private ImageView imageView;
  private DAToolTipView.OnToolTipViewClickedListener o;
  private View progressBar;
  private boolean text;
  private ImageView textView;
  private DAToolTip this$0;
  private int type;
  private View view;
  private int width;
  private int x;
  
  public DAToolTipView(Context paramContext)
  {
    super(paramContext);
    init();
  }
  
  private void init()
  {
    setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
    setOrientation(1);
    LayoutInflater.from(getContext()).inflate(2130968783, this, true);
    textView = ((ImageView)findViewById(2131690066));
    button = findViewById(2131690065);
    content = ((ViewGroup)findViewById(2131690067));
    header = ((TextView)findViewById(2131690068));
    a = findViewById(2131690070);
    imageView = ((ImageView)findViewById(2131690071));
    progressBar = findViewById(2131690069);
    setOnClickListener(this);
    getViewTreeObserver().addOnPreDrawListener(this);
  }
  
  private void setContentView(View paramView)
  {
    content.removeAllViews();
    content.addView(paramView);
  }
  
  private void show()
  {
    int n = 8;
    Object localObject1 = new int[2];
    view.getLocationOnScreen((int[])localObject1);
    Object localObject2 = new Rect();
    view.getWindowVisibleDisplayFrame((Rect)localObject2);
    int[] arrayOfInt = new int[2];
    if (getParent() != null) {
      ((View)getParent()).getLocationOnScreen(arrayOfInt);
    }
    int j = view.getWidth();
    int i = view.getHeight();
    type = (localObject1[0] - arrayOfInt[0]);
    x = (localObject1[1] - arrayOfInt[1]);
    int k = type;
    int i2 = j / 2;
    int m = x - getHeight();
    int i1 = Math.max(0, x + i);
    j = (right - width) / 2;
    i = j;
    if (width + j > right) {
      i = right - width;
    }
    setX(i);
    setPointerCenterX(k + i2);
    if (m < 0)
    {
      j = 1;
      if (this$0.getValue()) {
        break label264;
      }
      textView.setVisibility(8);
      imageView.setVisibility(8);
      if (j == 0) {
        break label378;
      }
    }
    label264:
    label284:
    label316:
    label334:
    label372:
    label378:
    for (j = i1;; j = m)
    {
      if (this$0.getText() != ToolTip.AnimationType.c) {
        break label384;
      }
      ViewHelper.setTranslationY(this, j);
      ViewHelper.setScaleX(this, i);
      return;
      j = 0;
      break;
      if (Build.VERSION.SDK_INT < 11)
      {
        localObject1 = textView;
        if (j != 0)
        {
          f = 1.0F;
          ViewHelper.setAlpha((View)localObject1, f);
          localObject1 = imageView;
          if (j == 0) {
            break label316;
          }
        }
        for (float f = 0.0F;; f = 1.0F)
        {
          ViewHelper.setAlpha((View)localObject1, f);
          break;
          f = 0.0F;
          break label284;
        }
      }
      localObject1 = textView;
      if (j != 0)
      {
        k = 0;
        ((ImageView)localObject1).setVisibility(k);
        localObject1 = imageView;
        if (j == 0) {
          break label372;
        }
      }
      for (k = n;; k = 0)
      {
        ((ImageView)localObject1).setVisibility(k);
        break;
        k = 8;
        break label334;
      }
    }
    label384:
    localObject1 = new ArrayList(5);
    if (this$0.getText() == ToolTip.AnimationType.i)
    {
      ((Collection)localObject1).add(ObjectAnimator.ofFloat(this, "translationY", new int[] { x + view.getHeight() / 2 - getHeight() / 2, j }));
      ((Collection)localObject1).add(ObjectAnimator.ofFloat(this, "translationX", new int[] { type + view.getWidth() / 2 - width / 2, i }));
    }
    for (;;)
    {
      ((Collection)localObject1).add(ObjectAnimator.ofFloat(this, "scaleX", new float[] { 0.0F, 1.0F }));
      ((Collection)localObject1).add(ObjectAnimator.ofFloat(this, "scaleY", new float[] { 0.0F, 1.0F }));
      ((Collection)localObject1).add(ObjectAnimator.ofFloat(this, "alpha", new float[] { 0.0F, 1.0F }));
      localObject2 = new AnimatorSet();
      ((AnimatorSet)localObject2).playTogether((Collection)localObject1);
      if (Build.VERSION.SDK_INT < 11) {
        ((Animator)localObject2).addListener(new DAToolTipView.AppearanceAnimatorListener(this, i, j));
      }
      ((AnimatorSet)localObject2).start();
      return;
      if (this$0.getText() == ToolTip.AnimationType.d) {
        ((Collection)localObject1).add(ObjectAnimator.ofFloat(this, "translationY", new float[] { 0.0F, j }));
      }
    }
  }
  
  public float getX()
  {
    if (Build.VERSION.SDK_INT >= 11) {
      return super.getX();
    }
    return ViewHelper.getX(this);
  }
  
  public float getY()
  {
    if (Build.VERSION.SDK_INT >= 11) {
      return super.getY();
    }
    return ViewHelper.getY(this);
  }
  
  public void init(DAToolTip paramDAToolTip, View paramView)
  {
    this$0 = paramDAToolTip;
    view = paramView;
    if (this$0.get() != null) {
      header.setText(this$0.get());
    }
    for (;;)
    {
      if (this$0.getTypeface() != null) {
        header.setTypeface(this$0.getTypeface());
      }
      if (this$0.set() != 0) {
        header.setTextColor(this$0.set());
      }
      if (this$0.getTextColor() != 0) {
        setColor(this$0.getTextColor());
      }
      if (this$0.getContentView() != null) {
        setContentView(this$0.getContentView());
      }
      if (!this$0.getNumColumns()) {
        progressBar.setVisibility(8);
      }
      if (!text) {
        break;
      }
      show();
      return;
      if (this$0.getColor() != 0) {
        header.setText(this$0.getColor());
      }
    }
  }
  
  public void onClick(View paramView)
  {
    remove();
    if (o != null) {
      o.b(this);
    }
  }
  
  public boolean onPreDraw()
  {
    getViewTreeObserver().removeOnPreDrawListener(this);
    text = true;
    width = content.getWidth();
    RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)getLayoutParams();
    width = width;
    setLayoutParams(localLayoutParams);
    if (getParent() == null) {
      return false;
    }
    if (this$0 != null) {
      show();
    }
    return true;
  }
  
  public void remove()
  {
    Object localObject;
    if (Build.VERSION.SDK_INT < 11)
    {
      localObject = (RelativeLayout.LayoutParams)getLayoutParams();
      setX(leftMargin);
      setY(topMargin);
      leftMargin = 0;
      topMargin = 0;
      setLayoutParams((ViewGroup.LayoutParams)localObject);
    }
    if (this$0.getText() == ToolTip.AnimationType.c)
    {
      if (getParent() != null) {
        ((ViewManager)getParent()).removeView(this);
      }
    }
    else
    {
      localObject = new ArrayList(5);
      if (this$0.getText() == ToolTip.AnimationType.i)
      {
        ((Collection)localObject).add(ObjectAnimator.ofFloat(this, "translationY", new int[] { (int)getY(), x + view.getHeight() / 2 - getHeight() / 2 }));
        ((Collection)localObject).add(ObjectAnimator.ofFloat(this, "translationX", new int[] { (int)getX(), type + view.getWidth() / 2 - width / 2 }));
      }
      for (;;)
      {
        ((Collection)localObject).add(ObjectAnimator.ofFloat(this, "scaleX", new float[] { 1.0F, 0.0F }));
        ((Collection)localObject).add(ObjectAnimator.ofFloat(this, "scaleY", new float[] { 1.0F, 0.0F }));
        ((Collection)localObject).add(ObjectAnimator.ofFloat(this, "alpha", new float[] { 1.0F, 0.0F }));
        AnimatorSet localAnimatorSet = new AnimatorSet();
        localAnimatorSet.playTogether((Collection)localObject);
        localAnimatorSet.addListener(new DAToolTipView.DisappearanceAnimatorListener(this, null));
        localAnimatorSet.start();
        return;
        ((Collection)localObject).add(ObjectAnimator.ofFloat(this, "translationY", new float[] { getY(), 0.0F }));
      }
    }
  }
  
  public void setColor(int paramInt)
  {
    textView.setColorFilter(paramInt, PorterDuff.Mode.MULTIPLY);
    button.getBackground().setColorFilter(paramInt, PorterDuff.Mode.MULTIPLY);
    imageView.setColorFilter(paramInt, PorterDuff.Mode.MULTIPLY);
    a.getBackground().setColorFilter(paramInt, PorterDuff.Mode.MULTIPLY);
    content.setBackgroundColor(paramInt);
  }
  
  public void setOnToolTipViewClickedListener(DAToolTipView.OnToolTipViewClickedListener paramOnToolTipViewClickedListener)
  {
    o = paramOnToolTipViewClickedListener;
  }
  
  public void setPointerCenterX(int paramInt)
  {
    int i = Math.max(textView.getMeasuredWidth(), imageView.getMeasuredWidth());
    ViewHelper.setX(textView, paramInt - i / 2 - (int)getX());
    ViewHelper.setX(imageView, paramInt - i / 2 - (int)getX());
  }
  
  public void setX(float paramFloat)
  {
    if (Build.VERSION.SDK_INT >= 11)
    {
      super.setX(paramFloat);
      return;
    }
    ViewHelper.setX(this, paramFloat);
  }
  
  public void setY(float paramFloat)
  {
    if (Build.VERSION.SDK_INT >= 11)
    {
      super.setY(paramFloat);
      return;
    }
    ViewHelper.setY(this, paramFloat);
  }
}
