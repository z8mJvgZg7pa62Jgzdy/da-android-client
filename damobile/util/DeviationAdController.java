package com.deviantart.android.damobile.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import com.amazon.device.package_13.AdRegistration;
import com.amazon.device.package_13.InterstitialAd;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.utils.DVNTContextUtils;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DeviationAdController
{
  private static int h = 10;
  private Context c;
  private boolean d;
  private boolean l = false;
  private DVNTDeviation p;
  private InterstitialAd s;
  private DVNTDeviation u;
  private DVNTDeviation x;
  
  public DeviationAdController(Context paramContext)
  {
    c = paramContext;
    if (!"debug".equals("store")) {}
    for (;;)
    {
      b();
      return;
      AdRegistration.enableLogging(true);
      AdRegistration.enableTesting(true);
      h = 3;
    }
  }
  
  private void b()
  {
    if (c()) {
      return;
    }
    Log.d("AmazonAds", "-- Set regular ad mode -- ");
    AdRegistration.setAppKey("c3b9ed355e0c43debcb33f422fef6554");
    s = new InterstitialAd(c);
    s.setListener(new DeviationAdController.RegularAdListener(this, null));
  }
  
  private boolean c()
  {
    return DVNTContextUtils.isContextDead(c);
  }
  
  private void d()
  {
    if (c()) {
      return;
    }
    Log.d("AmazonAds", "-- Set merch ad mode -- ");
    AdRegistration.setAppKey("9bdfada9fb294cafa45e3ee2f2d8cf0b");
    s = new InterstitialAd(c);
    s.setListener(new DeviationAdController.MerchAdListener(this, null));
  }
  
  private static String getKey()
  {
    return DAFormatUtils.DateFormats.OUTPUT_DATE.format(new Date());
  }
  
  private static void put(Context paramContext)
  {
    if (DVNTContextUtils.isContextDead(paramContext)) {
      return;
    }
    SharedPreferenceUtil.get(paramContext).edit().putInt("devads_view_counter", 0).putString("devads_last_shown_date", getKey()).apply();
    Log.d("AmazonAds", "-- Reset deviation view counter --");
    Log.d("AmazonAds", "-- Updated last shown date pref --");
  }
  
  public void a()
  {
    c = null;
    s = null;
    u = null;
    p = null;
    x = null;
  }
  
  public void b(DVNTDeviation paramDVNTDeviation1, DVNTDeviation paramDVNTDeviation2, DVNTDeviation paramDVNTDeviation3)
  {
    int i = 0;
    if (c()) {
      return;
    }
    u = paramDVNTDeviation1;
    x = paramDVNTDeviation2;
    p = paramDVNTDeviation3;
    Log.d("AmazonAds", "Deviation view event");
    if ((UserUtils.g != null) && (UserUtils.g.d()))
    {
      Log.d("AmazonAds", "Skipping count - user is Core");
      return;
    }
    String str = getKey();
    SharedPreferences localSharedPreferences = SharedPreferenceUtil.get(c);
    int j = localSharedPreferences.getInt("devads_view_counter", 0);
    if (!str.equals(localSharedPreferences.getString("devads_view_counter_date", null)))
    {
      Log.d("AmazonAds", "-- Reset view counter for new day --");
      localSharedPreferences.edit().putString("devads_view_counter_date", str).apply();
    }
    for (;;)
    {
      i += 1;
      localSharedPreferences.edit().putInt("devads_view_counter", i).apply();
      Log.d("AmazonAds", "Incremented deviation view counter: " + i);
      if ((i <= h) || (!d) || (paramDVNTDeviation1.isMature().booleanValue()) || ((paramDVNTDeviation2 != null) && (paramDVNTDeviation2.isMature().booleanValue())) || ((paramDVNTDeviation3 != null) && (paramDVNTDeviation3.isMature().booleanValue())))
      {
        Log.d("AmazonAds", "Skipping ad load");
        return;
      }
      if (UserUtils.c(c))
      {
        Log.d("AmazonAds", "Skipping ad load - user info uninitialized");
        return;
      }
      if (str.equals(localSharedPreferences.getString("devads_last_shown_date", null)))
      {
        Log.d("AmazonAds", "Skipping ad load - ad was already shown today");
        return;
      }
      Log.d("AmazonAds", "Attempting to load ad...");
      s.loadAd();
      return;
      i = j;
    }
  }
  
  public void b(boolean paramBoolean)
  {
    d = paramBoolean;
  }
}
