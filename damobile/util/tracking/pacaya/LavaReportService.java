package com.deviantart.android.damobile.util.tracking.pacaya;

import com.deviantart.pacaya.ReportService;
import java.util.List;

public class LavaReportService
  implements ReportService<com.deviantart.sdk.log.DVNTTopicEvent>
{
  public LavaReportService() {}
  
  public com.deviantart.upgrade.labs.DVNTTopicEvent a(List paramList, String paramString)
  {
    return new LossReportEventCreator(paramList, paramString).b();
  }
  
  public String a(List paramList)
  {
    if ((paramList == null) || (paramList.isEmpty())) {
      return null;
    }
    return getsize1f;
  }
}
