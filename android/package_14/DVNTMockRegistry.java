package com.deviantart.android.android.package_14;

import com.deviantart.datoolkit.logger.DVNTLog;
import java.util.ArrayList;
import java.util.HashMap;

public class DVNTMockRegistry
{
  private static HashMap<com.deviantart.android.sdk.api.network.request.DVNTBaseAsyncRequest, ArrayList<com.deviantart.android.sdk.api.DVNTMockRegistry.DVNTMock>> mockBuckets = new HashMap();
  private final String FORMAT = "DVNTMockRegistry {}";
  
  public DVNTMockRegistry() {}
  
  protected DVNTMockRegistry.DVNTMockBuilder buildMocks(com.deviantart.android.android.package_14.network.request.DVNTBaseAsyncRequest paramDVNTBaseAsyncRequest, Class paramClass)
  {
    return new DVNTMockRegistry.DVNTMockBuilder(this, paramDVNTBaseAsyncRequest, null);
  }
  
  public DVNTMockRegistry.DVNTMock findNextMock(com.deviantart.android.android.package_14.network.request.DVNTBaseAsyncRequest paramDVNTBaseAsyncRequest)
  {
    paramDVNTBaseAsyncRequest = (ArrayList)mockBuckets.get(paramDVNTBaseAsyncRequest);
    if ((paramDVNTBaseAsyncRequest == null) || (paramDVNTBaseAsyncRequest.isEmpty())) {
      return null;
    }
    DVNTMockRegistry.DVNTMock localDVNTMock = (DVNTMockRegistry.DVNTMock)paramDVNTBaseAsyncRequest.get(0);
    Integer localInteger = localDVNTMock.getTimes();
    if (localInteger != null)
    {
      int i = localInteger.intValue() - 1;
      localDVNTMock.times(i);
      if (i < 0)
      {
        DVNTLog.append("DVNTMockRegistry {}", new Object[] { "Mock had timesLeft < 0" });
        return null;
      }
      if (i == 0) {
        paramDVNTBaseAsyncRequest.remove(0);
      }
    }
    return localDVNTMock;
  }
  
  public boolean hasMocksFor(com.deviantart.android.android.package_14.network.request.DVNTBaseAsyncRequest paramDVNTBaseAsyncRequest)
  {
    return (mockBuckets.containsKey(paramDVNTBaseAsyncRequest)) && (!((ArrayList)mockBuckets.get(paramDVNTBaseAsyncRequest)).isEmpty());
  }
}
