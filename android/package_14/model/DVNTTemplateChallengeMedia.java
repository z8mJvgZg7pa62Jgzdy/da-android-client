package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;

public class DVNTTemplateChallengeMedia
  extends DVNTChallengeMedia
{
  @SerializedName("transparency")
  private Boolean isTransparent;
  
  public DVNTTemplateChallengeMedia() {}
  
  public Boolean isTransparent()
  {
    return isTransparent;
  }
  
  public void setIsTransparent(Boolean paramBoolean)
  {
    isTransparent = paramBoolean;
  }
}
