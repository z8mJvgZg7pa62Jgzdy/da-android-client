package com.deviantart.android.damobile.util.mainpager;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.view.viewpageindicator.DAPagerAdapter;
import java.util.List;

public abstract class DAMainPagerAdapter<PAGE_TYPE extends DAMainPage>
  extends DAPagerAdapter
{
  protected List<PAGE_TYPE> a;
  
  public DAMainPagerAdapter(List paramList)
  {
    a = paramList;
  }
  
  public Object a(ViewGroup paramViewGroup, int paramInt)
  {
    View localView = ((DAMainPage)a.get(paramInt)).a(paramViewGroup);
    if (localView.getParent() != null)
    {
      Log.e("DAMainPagerAdapter", "main page view already has a parent");
      ((ViewGroup)localView.getParent()).removeView(localView);
    }
    paramViewGroup.addView(localView);
    return localView;
  }
  
  public CharSequence b(int paramInt)
  {
    return ((DAMainPage)a.get(paramInt)).getFormatted();
  }
  
  public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    paramViewGroup.removeView((View)paramObject);
    ((DAMainPage)a.get(paramInt)).b();
  }
  
  public List get()
  {
    return a;
  }
  
  public int getCount()
  {
    return a.size();
  }
  
  public int getIcon(int paramInt)
  {
    return ((DAMainPage)a.get(paramInt)).intValue();
  }
  
  public CharSequence getPageTitle(int paramInt)
  {
    return ((DAMainPage)a.get(paramInt)).getPageTitle();
  }
  
  public boolean isViewFromObject(View paramView, Object paramObject)
  {
    return paramView == paramObject;
  }
}
