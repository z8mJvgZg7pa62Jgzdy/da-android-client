package com.deviantart.android.android.package_14;

import android.content.Context;
import com.deviantart.android.android.package_14.listener.DVNTAsyncRequestListener;
import com.deviantart.android.android.package_14.listener.DVNTSpiceAsyncRequestListener;
import com.deviantart.android.android.package_14.network.DVNTRequestManager;
import com.deviantart.android.android.package_14.network.request.DVNTBaseAsyncRequest;
import com.deviantart.android.android.package_14.session.DVNTClientSessionManager;
import com.deviantart.android.android.package_14.session.DVNTSessionManager;
import com.deviantart.android.android.package_14.session.DVNTTokenSessionManager;
import com.deviantart.android.android.package_14.session.DVNTUserSessionManager;
import com.deviantart.datoolkit.logger.DVNTLog;
import java.util.UUID;

public class DVNTAPIClient
{
  private static final String FORMAT = "APIClient {}";
  private final DVNTSessionManager anonymousSessionManager;
  final DVNTClientSessionManager clientSessionManager;
  final DVNTUserSessionManager userSessionManager;
  
  public DVNTAPIClient(DVNTSessionManager paramDVNTSessionManager, DVNTUserSessionManager paramDVNTUserSessionManager, DVNTClientSessionManager paramDVNTClientSessionManager)
  {
    anonymousSessionManager = paramDVNTSessionManager;
    userSessionManager = paramDVNTUserSessionManager;
    clientSessionManager = paramDVNTClientSessionManager;
  }
  
  private UUID callAsync(Context paramContext, DVNTBaseAsyncRequest paramDVNTBaseAsyncRequest, DVNTAsyncRequestListener paramDVNTAsyncRequestListener, DVNTSessionManager paramDVNTSessionManager)
  {
    if (paramContext == null)
    {
      DVNTLog.get("APIClient {}", new Object[] { "no context, aborting" });
      return null;
    }
    if (paramDVNTAsyncRequestListener.shouldCancel())
    {
      DVNTLog.get("APIClient {}", new Object[] { "request has been cancelled before execution" });
      return null;
    }
    UUID localUUID = DVNTRequestManager.create(paramDVNTBaseAsyncRequest);
    paramDVNTSessionManager.asyncExecute(paramDVNTBaseAsyncRequest, new DVNTSpiceAsyncRequestListener(paramContext, paramDVNTAsyncRequestListener, localUUID));
    return localUUID;
  }
  
  public UUID anonymousCallAsync(Context paramContext, DVNTBaseAsyncRequest paramDVNTBaseAsyncRequest, DVNTAsyncRequestListener paramDVNTAsyncRequestListener)
  {
    if (!anonymousSessionManager.isStarted()) {
      anonymousSessionManager.start(paramContext);
    }
    DVNTLog.get("APIClient {}", new Object[] { "clientCall request " + paramDVNTBaseAsyncRequest.getCacheKey() });
    return callAsync(paramContext, paramDVNTBaseAsyncRequest, paramDVNTAsyncRequestListener, anonymousSessionManager);
  }
  
  public void cancelAllRequests()
  {
    userSessionManager.cancelAllRequests();
  }
  
  public void cancelRequest(UUID paramUUID)
  {
    userSessionManager.cancelRequest(paramUUID);
  }
  
  public void clearCache()
  {
    userSessionManager.clearCache();
    clientSessionManager.clearCache();
  }
  
  public void clearCache(UUID paramUUID)
  {
    clientSessionManager.clearCache(paramUUID);
    userSessionManager.clearCache(paramUUID);
  }
  
  public UUID clientCallAsync(Context paramContext, DVNTBaseAsyncRequest paramDVNTBaseAsyncRequest, DVNTAsyncRequestListener paramDVNTAsyncRequestListener)
  {
    DVNTLog.get("APIClient {}", new Object[] { "clientCall request " + paramDVNTBaseAsyncRequest.getCacheKey() });
    return callAsync(paramContext, paramDVNTBaseAsyncRequest, paramDVNTAsyncRequestListener, clientSessionManager);
  }
  
  public void closeUserSession(Context paramContext)
  {
    userSessionManager.close(paramContext);
    userSessionManager.reset();
  }
  
  public void getSession(Context paramContext, boolean paramBoolean)
  {
    for (;;)
    {
      try
      {
        DVNTLog.get("APIClient {}", new Object[] { "let's try to open user session" });
        boolean bool = userSessionManager.openSession(paramContext, paramBoolean);
        if (bool) {
          return;
        }
        DVNTLog.get("APIClient {}", new Object[] { "could not open user session" });
        if (paramBoolean)
        {
          DVNTLog.get("APIClient {}", new Object[] { "we need graduation - starting graduation process" });
          userSessionManager.startGraduationProcess(paramContext);
          continue;
        }
      }
      catch (Throwable paramContext)
      {
        throw paramContext;
      }
      tmp87_84[0] = "we don't need graduation here, let's try to open client session";
      DVNTLog.get("APIClient {}", tmp87_84);
      clientSessionManager.openSession(paramContext);
    }
  }
  
  public boolean hasGraduated(Context paramContext, boolean paramBoolean)
  {
    DVNTLog.get("APIClient {}", new Object[] { "check if hasGraduated" });
    return userSessionManager.openSession(paramContext, paramBoolean);
  }
  
  public void resetUserSession()
  {
    userSessionManager.reset();
  }
  
  public void stopListening()
  {
    userSessionManager.stop();
    clientSessionManager.stop();
  }
  
  public UUID userCallAsync(Context paramContext, DVNTBaseAsyncRequest paramDVNTBaseAsyncRequest, DVNTAsyncRequestListener paramDVNTAsyncRequestListener)
  {
    DVNTLog.get("APIClient {}", new Object[] { "userCall request " + paramDVNTBaseAsyncRequest.getCacheKey() });
    return callAsync(paramContext, paramDVNTBaseAsyncRequest, paramDVNTAsyncRequestListener, userSessionManager);
  }
}
