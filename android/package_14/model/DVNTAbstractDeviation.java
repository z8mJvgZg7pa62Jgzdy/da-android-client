package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public abstract class DVNTAbstractDeviation
  implements DVNTThumbable, Serializable
{
  @SerializedName("deviationid")
  private String Id;
  @SerializedName("allows_comments")
  Boolean allowsComments;
  @SerializedName("author")
  private DVNTUser author;
  @SerializedName("category")
  private String category;
  @SerializedName("category_path")
  private String categoryPath;
  @SerializedName("challenge")
  private DVNTChallenge challenge;
  @SerializedName("challenge_entry")
  private DVNTChallengeEntry challengeEntry;
  @SerializedName("content")
  private DVNTImage content;
  @SerializedName("daily_deviation")
  private DVNTAbstractDeviation.DVNTDailyDeviation dailyDeviation;
  @SerializedName("download_filesize")
  Integer downloadFileSize;
  @SerializedName("excerpt")
  private String excerpt;
  @SerializedName("is_favourited")
  Boolean favourited;
  @SerializedName("flash")
  private DVNTImage flash;
  @SerializedName("is_deleted")
  Boolean isDeleted;
  @SerializedName("is_downloadable")
  Boolean isDownloadable;
  @SerializedName("is_mature")
  Boolean isMature;
  @SerializedName("preview")
  private DVNTImage preview;
  @SerializedName("printid")
  private String printId;
  @SerializedName("stats")
  private DVNTAbstractDeviation.DVNTDeviationBaseStats stats;
  @SerializedName("thumbs")
  private DVNTImage.List thumbs;
  @SerializedName("title")
  private String title;
  @SerializedName("url")
  private String urlString;
  @SerializedName("videos")
  private DVNTVideo.List videos;
  
  public DVNTAbstractDeviation() {}
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != com.deviantart.android.sdk.api.model.DVNTDeviation.class)) {
      return false;
    }
    DVNTDeviation localDVNTDeviation = (DVNTDeviation)paramObject;
    if ((Id != null) && (Id.equals(localDVNTDeviation.getId()))) {
      return true;
    }
    return super.equals(paramObject);
  }
  
  public Boolean getAllowsComments()
  {
    return allowsComments;
  }
  
  public DVNTUser getAuthor()
  {
    return author;
  }
  
  public String getCategory()
  {
    return category;
  }
  
  public String getCategoryPath()
  {
    return categoryPath;
  }
  
  public DVNTChallenge getChallenge()
  {
    return challenge;
  }
  
  public DVNTChallengeEntry getChallengeEntry()
  {
    return challengeEntry;
  }
  
  public DVNTImage getContent()
  {
    return content;
  }
  
  public DVNTAbstractDeviation.DVNTDailyDeviation getDailyDeviation()
  {
    return dailyDeviation;
  }
  
  public Integer getDownloadFileSize()
  {
    return downloadFileSize;
  }
  
  public String getExcerpt()
  {
    return excerpt;
  }
  
  public Boolean getFavourited()
  {
    return favourited;
  }
  
  public DVNTImage getFlash()
  {
    return flash;
  }
  
  public String getId()
  {
    return Id;
  }
  
  public Boolean getIsDeleted()
  {
    return isDeleted;
  }
  
  public Boolean getIsDownloadable()
  {
    return isDownloadable;
  }
  
  public Boolean getIsMature()
  {
    return isMature;
  }
  
  public DVNTImage getPreview()
  {
    return preview;
  }
  
  public String getPrintId()
  {
    return printId;
  }
  
  public DVNTAbstractDeviation.DVNTDeviationBaseStats getStats()
  {
    return stats;
  }
  
  public DVNTImage.List getThumbs()
  {
    return thumbs;
  }
  
  public String getTitle()
  {
    return title;
  }
  
  public String getUrl()
  {
    return urlString;
  }
  
  public DVNTVideo.List getVideos()
  {
    return videos;
  }
  
  public Boolean isDeleted()
  {
    return isDeleted;
  }
  
  public Boolean isFavourited()
  {
    return favourited;
  }
  
  public Boolean isMature()
  {
    return isMature;
  }
  
  public void setAllowsComments(Boolean paramBoolean)
  {
    allowsComments = paramBoolean;
  }
  
  public void setAuthor(DVNTUser paramDVNTUser)
  {
    author = paramDVNTUser;
  }
  
  public void setCategory(String paramString)
  {
    category = paramString;
  }
  
  public void setCategoryPath(String paramString)
  {
    categoryPath = paramString;
  }
  
  public void setChallenge(DVNTChallenge paramDVNTChallenge)
  {
    challenge = paramDVNTChallenge;
  }
  
  public void setChallengeEntry(DVNTChallengeEntry paramDVNTChallengeEntry)
  {
    challengeEntry = paramDVNTChallengeEntry;
  }
  
  public void setContent(DVNTImage paramDVNTImage)
  {
    content = paramDVNTImage;
  }
  
  public void setDailyDeviation(DVNTAbstractDeviation.DVNTDailyDeviation paramDVNTDailyDeviation)
  {
    dailyDeviation = paramDVNTDailyDeviation;
  }
  
  public void setDownloadFileSize(Integer paramInteger)
  {
    downloadFileSize = paramInteger;
  }
  
  public void setExcerpt(String paramString)
  {
    excerpt = paramString;
  }
  
  public void setFavourited(Boolean paramBoolean)
  {
    favourited = paramBoolean;
  }
  
  public void setFlash(DVNTImage paramDVNTImage)
  {
    flash = paramDVNTImage;
  }
  
  public void setId(String paramString)
  {
    Id = paramString;
  }
  
  public void setIsDeleted(Boolean paramBoolean)
  {
    isDeleted = paramBoolean;
  }
  
  public void setIsDownloadable(Boolean paramBoolean)
  {
    isDownloadable = paramBoolean;
  }
  
  public void setIsMature(Boolean paramBoolean)
  {
    isMature = paramBoolean;
  }
  
  public void setPreview(DVNTImage paramDVNTImage)
  {
    preview = paramDVNTImage;
  }
  
  public void setPrintId(String paramString)
  {
    printId = paramString;
  }
  
  public void setStats(DVNTAbstractDeviation.DVNTDeviationBaseStats paramDVNTDeviationBaseStats)
  {
    stats = paramDVNTDeviationBaseStats;
  }
  
  public void setThumbs(DVNTImage.List paramList)
  {
    thumbs = paramList;
  }
  
  public void setTitle(String paramString)
  {
    title = paramString;
  }
  
  public void setUrl(String paramString)
  {
    urlString = paramString;
  }
  
  public void setVideos(DVNTVideo.List paramList)
  {
    videos = paramList;
  }
}
