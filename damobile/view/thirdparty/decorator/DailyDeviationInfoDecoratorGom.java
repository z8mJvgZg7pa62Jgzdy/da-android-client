package com.deviantart.android.damobile.view.thirdparty.decorator;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation.DVNTDailyDeviation;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.util.TodayDailyDeviationItem;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.view.gom.decorator.DecoratorGom;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class DailyDeviationInfoDecoratorGom
  implements DecoratorGom<com.deviantart.android.damobile.view.gom.decorator.DailyDeviationInfoViewHolder, TodayDailyDeviationItem>
{
  public DailyDeviationInfoDecoratorGom() {}
  
  private void setText(TextView paramTextView, String paramString1, String paramString2)
  {
    paramString2 = new SpannableStringBuilder(paramString1 + " " + paramString2);
    StyleSpan localStyleSpan1 = new StyleSpan(0);
    StyleSpan localStyleSpan2 = new StyleSpan(1);
    paramString2.setSpan(localStyleSpan1, 0, paramString1.length(), 18);
    paramString2.setSpan(localStyleSpan2, paramString1.length() + 1, paramString2.length(), 18);
    paramTextView.setText(paramString2);
  }
  
  public void c(Context paramContext, DailyDeviationInfoViewHolder paramDailyDeviationInfoViewHolder, TodayDailyDeviationItem paramTodayDailyDeviationItem)
  {
    DVNTAbstractDeviation.DVNTDailyDeviation localDVNTDailyDeviation = paramTodayDailyDeviationItem.a().getDailyDeviation();
    if (paramTodayDailyDeviationItem.h())
    {
      ddHeader.setVisibility(0);
      ddHeader.setText(paramTodayDailyDeviationItem.b());
    }
    while (localDVNTDailyDeviation == null)
    {
      ddCommentContainer.setVisibility(8);
      ddUserInfoContainer.setVisibility(8);
      return;
      ddHeader.setVisibility(8);
    }
    commentView.setText(Jsoup.get(localDVNTDailyDeviation.getBody()).get());
    if (localDVNTDailyDeviation.getGiver() != null) {
      setText(featurerView, paramContext.getString(2131231002), localDVNTDailyDeviation.getGiver().getUserName());
    }
    if (localDVNTDailyDeviation.getSuggester() != null)
    {
      setText(suggesterView, paramContext.getString(2131231384), localDVNTDailyDeviation.getSuggester().getUserName());
      return;
    }
    suggesterView.setVisibility(8);
  }
  
  public Class getComponentType()
  {
    return TodayDailyDeviationItem.class;
  }
  
  public DailyDeviationInfoViewHolder onCreateView(Context paramContext, ViewGroup paramViewGroup, RecyclerView.ViewHolder paramViewHolder)
  {
    paramContext = LayoutInflater.from(paramContext).inflate(2130968626, paramViewGroup, false);
    ((FrameLayout)ButterKnife.findById(paramContext, 2131689650)).addView(itemView);
    return new DailyDeviationInfoViewHolder(paramViewHolder, paramContext);
  }
}
