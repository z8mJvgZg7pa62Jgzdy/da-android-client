package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.activity.HomeActivity.HomeActivityPages;
import com.deviantart.android.damobile.fragment.WatchRecoFragment;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.util.OnWatchRecoClickListener;
import com.deviantart.android.damobile.util.ScreenFlowManager;

public class WatchPageEmptyStateView
  extends FrameLayout
{
  @Bind({2131690133})
  TextView emptyMessage;
  @Bind({2131690132})
  TextView emptyTitle;
  private OnWatchRecoClickListener timeFormat;
  
  public WatchPageEmptyStateView(Context paramContext, WatchPageEmptyStateView.State paramState)
  {
    super(paramContext);
    setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
    LayoutInflater.from(paramContext).inflate(2130968801, this, true);
    ButterKnife.bind(this);
    Resources localResources = paramContext.getResources();
    int i = 0;
    switch (WatchPageEmptyStateView.1.u[paramState.ordinal()])
    {
    default: 
      break;
    }
    for (;;)
    {
      setPadding(getPaddingLeft(), i, getPaddingRight(), getPaddingBottom());
      return;
      emptyMessage.setTextSize(2, 24.0F);
      emptyTitle.setVisibility(8);
      emptyMessage.setText(localResources.getString(2131231427));
      continue;
      emptyTitle.setTextSize(2, 32.0F);
      emptyMessage.setTextSize(2, 24.0F);
      emptyTitle.setText(localResources.getString(2131231429));
      emptyMessage.setText(localResources.getString(2131231428));
      continue;
      i = Graphics.add(paramContext, 40);
      emptyTitle.setTextSize(2, 32.0F);
      emptyMessage.setTextSize(2, 24.0F);
      emptyTitle.setText(localResources.getString(2131231432));
      emptyMessage.setText(localResources.getString(2131231431));
    }
  }
  
  public void onWatchRecoClick()
  {
    if (timeFormat != null)
    {
      timeFormat.showFavorites();
      return;
    }
    ScreenFlowManager.a((Activity)getContext(), new WatchRecoFragment(), HomeActivity.HomeActivityPages.d.a());
  }
  
  public void setOnWatchRecoClickListener(OnWatchRecoClickListener paramOnWatchRecoClickListener)
  {
    timeFormat = paramOnWatchRecoClickListener;
  }
}
