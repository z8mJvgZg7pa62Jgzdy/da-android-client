package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.net.Uri;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTNote;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.package_14.model.DVNTUser.List;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.activity.CreateNoteActivity.IntentBuilder;
import com.deviantart.android.damobile.activity.HomeActivity.BottomBarButton;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.ProcessMenuListener;
import com.deviantart.android.damobile.util.ProcessMenuType;
import com.deviantart.android.damobile.util.UserDisplay;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.markup.OrderedDAMLHelper;
import com.deviantart.android.damobile.util.notes.Mark;
import com.deviantart.android.damobile.util.notes.NotesAction;
import com.deviantart.android.damobile.util.notes.NotesDefaultPage;
import com.deviantart.android.damobile.util.notes.NotesItemData;
import com.deviantart.android.damobile.util.notes.NotesItemData.Observable;
import com.deviantart.android.damobile.util.notes.NotesItemData.Observer;
import com.deviantart.android.damobile.util.notes.NotesMarkUtils;
import com.deviantart.android.damobile.util.opt.ItemDeleteHelperBase;
import com.deviantart.android.damobile.util.opt.ItemDeleteType;
import com.deviantart.android.damobile.util.opt.ItemDeleteUtils;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.util.tracking.pacaya.MobileLava;
import com.deviantart.android.damobile.util.tracking.pacaya.UxTopicEventCreator;
import com.deviantart.pacaya.Pacaya;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NotesDetailFragment
  extends HomeBaseFragment
  implements ProcessMenuListener, NotesItemData.Observer
{
  private NotesItemData a;
  @Bind({2131689770})
  TextView authorView;
  @Bind({2131689769})
  SimpleDraweeView avatarView;
  private NotesItemData.Observable c;
  @Bind({2131689772})
  TextView dateView;
  @Bind({2131689748})
  View loading;
  @Bind({2131689765})
  View mainNoteView;
  @Bind({2131689767})
  LinearLayout messageLayout;
  @Bind({2131689766})
  TextView subjectView;
  @Bind({2131689771})
  TextView toView;
  
  public NotesDetailFragment() {}
  
  public static NotesDetailFragment a(String paramString)
  {
    NotesDetailFragment localNotesDetailFragment = new NotesDetailFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("noteid", paramString);
    localNotesDetailFragment.setArguments(localBundle);
    return localNotesDetailFragment;
  }
  
  private void b(Mark paramMark)
  {
    NotesMarkUtils.a(NotesDefaultPage.o, a, paramMark, false);
    switch (NotesDetailFragment.4.d[paramMark.ordinal()])
    {
    default: 
      return;
    case 1: 
    case 2: 
      NotesMarkUtils.a(NotesDefaultPage.d, a, paramMark, false);
      NotesMarkUtils.a(NotesDefaultPage.l, a, paramMark, false);
      NotesMarkUtils.a(NotesDefaultPage.b, a, paramMark, true);
      return;
    }
    NotesMarkUtils.a(NotesDefaultPage.l, a, paramMark, true);
    NotesMarkUtils.a(NotesDefaultPage.b, a, paramMark, false);
  }
  
  private Spannable doInBackground()
  {
    DVNTUser.List localList = a.getContext().getRecipients();
    SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(getString(2131231130) + " ");
    localSpannableStringBuilder.append(UserDisplay.getText(getActivity(), localList));
    return localSpannableStringBuilder;
  }
  
  private List get(DVNTNote paramDVNTNote, boolean paramBoolean)
  {
    ArrayList localArrayList = new ArrayList();
    DVNTUser localDVNTUser = paramDVNTNote.getUser();
    if (!localDVNTUser.getUserName().equals(UserUtils.a)) {
      localArrayList.add(localDVNTUser);
    }
    if (!paramBoolean)
    {
      if (localArrayList.isEmpty()) {
        localArrayList.add(paramDVNTNote.getRecipients().get(0));
      }
      return localArrayList;
    }
    paramDVNTNote = paramDVNTNote.getRecipients().iterator();
    while (paramDVNTNote.hasNext())
    {
      localDVNTUser = (DVNTUser)paramDVNTNote.next();
      if ((!open(localArrayList, localDVNTUser.getUserName())) && (!localDVNTUser.getUserName().equals(UserUtils.a))) {
        localArrayList.add(localDVNTUser);
      }
    }
    return localArrayList;
  }
  
  public static NotesDetailFragment getItem(NotesItemData.Observable paramObservable)
  {
    NotesDetailFragment localNotesDetailFragment = new NotesDetailFragment();
    localNotesDetailFragment.b(paramObservable);
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("note_data", paramObservable.a());
    localNotesDetailFragment.setArguments(localBundle);
    return localNotesDetailFragment;
  }
  
  private void load(String paramString)
  {
    loading.setVisibility(0);
    mainNoteView.setVisibility(8);
    Activity localActivity = getActivity();
    DVNTAsyncAPI.getNote(paramString).call(localActivity, new NotesDetailFragment.1(this, localActivity));
  }
  
  private void onCreate()
  {
    DVNTNote localDVNTNote = a.getContext();
    DVNTUser localDVNTUser = localDVNTNote.getUser();
    ImageUtils.isEmpty(avatarView, Uri.parse(localDVNTUser.getUserIconURL()));
    authorView.setText(UserDisplay.add(getActivity(), localDVNTNote.getUser()));
    toView.setText(doInBackground());
    dateView.setText(DAFormatUtils.get(getActivity(), localDVNTNote.getTimeStamp()));
    subjectView.setText(localDVNTNote.getSubject());
    OrderedDAMLHelper.update(getActivity(), messageLayout, a.getContext().getBody(), this);
  }
  
  private void onCreateDialog(DVNTNote paramDVNTNote)
  {
    String str = "?";
    Object localObject = get(paramDVNTNote, false);
    if (!((List)localObject).isEmpty()) {
      str = ((DVNTUser)((List)localObject).get(0)).getUserName();
    }
    str = String.format(getString(2131231122), new Object[] { str });
    localObject = getString(2131231121);
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
    localBuilder.setTitle(2131231123);
    paramDVNTNote = new NotesDetailFragment.2(this, paramDVNTNote);
    localBuilder.setItems((CharSequence[])new CharSequence[] { str, localObject }, paramDVNTNote);
    localBuilder.setNegativeButton(2131230820, new NotesDetailFragment.3(this));
    localBuilder.create().show();
  }
  
  private boolean open(List paramList, String paramString)
  {
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      if (((DVNTUser)paramList.next()).getUserName().equals(paramString)) {
        return true;
      }
    }
    return false;
  }
  
  private void showDialog(DVNTNote paramDVNTNote, boolean paramBoolean)
  {
    Activity localActivity = getActivity();
    if (DVNTContextUtils.isContextDead(localActivity)) {
      return;
    }
    localActivity.startActivityForResult(new CreateNoteActivity.IntentBuilder().a(get(paramDVNTNote, paramBoolean)).b(paramDVNTNote).a(localActivity), 110);
    localActivity.overridePendingTransition(2131034146, 2131034113);
  }
  
  public void a(ProcessMenuType paramProcessMenuType, String paramString) {}
  
  public void b(NotesItemData.Observable paramObservable)
  {
    c = paramObservable;
  }
  
  protected HomeActivity.BottomBarButton c()
  {
    return HomeActivity.BottomBarButton.a;
  }
  
  public void d()
  {
    if (a != null)
    {
      if (DVNTContextUtils.isContextDead(getActivity())) {
        return;
      }
      Activity localActivity = getActivity();
      TrackerUtil.get(localActivity, EventKeys.Category.d, "mark_note_unread", "full_note_view");
      NotesAction.b(localActivity, c, Mark.a, true);
      b(Mark.a);
      localActivity.onBackPressed();
    }
  }
  
  protected boolean delete4()
  {
    return false;
  }
  
  public void doRefresh()
  {
    messageLayout.removeAllViews();
    getActivity().invalidateOptionsMenu();
    onCreate();
  }
  
  public void e()
  {
    if (a != null)
    {
      if (DVNTContextUtils.isContextDead(getActivity())) {
        return;
      }
      if (a.getContext().getIsStarred().booleanValue()) {}
      for (Mark localMark = Mark.d;; localMark = Mark.b)
      {
        if (Mark.b.equals(localMark)) {
          TrackerUtil.get(getActivity(), EventKeys.Category.d, "star_a_note", "full_note_view");
        }
        NotesAction.a(getActivity(), c, localMark);
        b(localMark);
        return;
      }
    }
  }
  
  public void onClickAvatar(View paramView)
  {
    UserUtils.c((Activity)paramView.getContext(), a.getContext().getUser().getUserName());
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if ((!DVNTContextUtils.isContextDead(getActivity())) && (DVNTAbstractAsyncAPI.isUserSession(getActivity())))
    {
      if (a == null) {
        return;
      }
      paramMenuInflater.inflate(2131820552, paramMenu);
      paramMenuInflater = a.getContext();
      if (paramMenuInflater.getIsStarred().booleanValue()) {
        paramMenu.findItem(2131690160).setIcon(2130837743);
      }
      while (paramMenuInflater.getIsSent().booleanValue())
      {
        paramMenu.removeItem(2131690162);
        return;
        paramMenu.findItem(2131690160).setIcon(2130837745);
      }
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = paramLayoutInflater.inflate(2130968673, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    MobileLava.e().setText(new UxTopicEventCreator().get("note").b());
    paramViewGroup = getArguments().getString("noteid", null);
    if (paramViewGroup != null) {
      load(paramViewGroup);
    }
    for (;;)
    {
      initUI();
      ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
      ((BaseActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      return paramLayoutInflater;
      a = ((NotesItemData)getArguments().getSerializable("note_data"));
      if (c == null) {
        c = new NotesItemData.Observable(a);
      }
      c.a(this);
      onCreate();
    }
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    if (c != null) {
      c.setOnDismissListener();
    }
    ButterKnife.unbind(this);
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      break;
    }
    for (;;)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      getActivity().onBackPressed();
      continue;
      visitLabel();
      continue;
      e();
      continue;
      showDialog();
      continue;
      d();
    }
  }
  
  public boolean refresh()
  {
    return true;
  }
  
  public void showDialog()
  {
    if (a != null)
    {
      if (DVNTContextUtils.isContextDead(getActivity())) {
        return;
      }
      DVNTNote localDVNTNote = a.getContext();
      int i = localDVNTNote.getRecipients().size();
      if (i >= 1)
      {
        TrackerUtil.get(getActivity(), EventKeys.Category.d, "reply_to_a_note", "full_note_view");
        if (i == 1)
        {
          showDialog(localDVNTNote, false);
          return;
        }
        onCreateDialog(localDVNTNote);
      }
    }
  }
  
  public void visitLabel()
  {
    if (a != null)
    {
      if (DVNTContextUtils.isContextDead(getActivity())) {
        return;
      }
      TrackerUtil.get(getActivity(), EventKeys.Category.d, "delete_a_note", "full_note_view");
      ItemDeleteUtils.a(ItemDeleteType.c).a(getActivity(), a);
      getActivity().onBackPressed();
    }
  }
}
