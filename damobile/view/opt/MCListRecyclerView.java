package com.deviantart.android.damobile.view.opt;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.stream.Stream.Notifiable;
import com.deviantart.android.damobile.util.scrolling.RecyclerViewEndlessScrollListener;
import com.deviantart.android.damobile.util.scrolling.RecyclerViewSignificantScrollListener;
import com.deviantart.android.damobile.view.EndlessScrollListener;
import com.deviantart.android.damobile.view.EndlessScrollOptions;

public class MCListRecyclerView<ADAPTER_TYPE extends com.deviantart.android.damobile.view.mc.MCListAdapterBase>
  extends RecyclerView
  implements EndlessScrollListener
{
  private Stream.Notifiable b;
  
  public MCListRecyclerView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public void a()
  {
    if (b != null)
    {
      if (getAdapter() == null) {
        return;
      }
      getAdapter().write(getContext(), b, false);
    }
  }
  
  public void d()
  {
    if ((b != null) && (getAdapter() != null))
    {
      if (DVNTContextUtils.isContextDead(getContext())) {
        return;
      }
      getAdapter().a();
      getAdapter().write(getContext(), b, true);
    }
  }
  
  public MCListAdapterBase getAdapter()
  {
    return (MCListAdapterBase)super.getAdapter();
  }
  
  public void init(Context paramContext)
  {
    setLayoutManager(new LinearLayoutManager(paramContext));
    addOnScrollListener(new RecyclerViewSignificantScrollListener());
    addOnScrollListener(new RecyclerViewEndlessScrollListener(new EndlessScrollOptions(10, this)));
  }
  
  public void setAdapter(RecyclerView.Adapter paramAdapter)
  {
    throw new UnsupportedOperationException("Use setAdapterSafe()");
  }
  
  public void setAdapterSafe(MCListAdapterBase paramMCListAdapterBase)
  {
    super.setAdapter(paramMCListAdapterBase);
  }
  
  public void setLoading(boolean paramBoolean)
  {
    getAdapter().clear(paramBoolean);
  }
  
  public void setNotifiable(Stream.Notifiable paramNotifiable)
  {
    b = paramNotifiable;
  }
}
