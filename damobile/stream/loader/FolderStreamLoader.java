package com.deviantart.android.damobile.stream.loader;

import com.deviantart.android.damobile.util.FolderTitleListener;

public abstract class FolderStreamLoader<T>
  extends StreamLoader<T>
{
  protected transient FolderTitleListener d;
  
  public FolderStreamLoader() {}
  
  public void d(FolderTitleListener paramFolderTitleListener)
  {
    d = paramFolderTitleListener;
  }
}
