package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTMoreLikeThisPreviewResults
  implements Serializable
{
  private DVNTUser author;
  @SerializedName("more_from_artist")
  private DVNTAbstractDeviation.List moreFromArtist;
  @SerializedName("more_from_da")
  private DVNTAbstractDeviation.List moreLikeThis;
  @SerializedName("seed")
  private String seedDeviationId;
  
  public DVNTMoreLikeThisPreviewResults() {}
  
  public DVNTUser getAuthor()
  {
    return author;
  }
  
  public DVNTAbstractDeviation.List getMoreFromArtist()
  {
    return moreFromArtist;
  }
  
  public DVNTAbstractDeviation.List getMoreLikeThis()
  {
    return moreLikeThis;
  }
  
  public String getSeedDeviationId()
  {
    return seedDeviationId;
  }
  
  public void setAuthor(DVNTUser paramDVNTUser)
  {
    author = paramDVNTUser;
  }
  
  public void setMoreFromArtist(DVNTAbstractDeviation.List paramList)
  {
    moreFromArtist = paramList;
  }
  
  public void setMoreLikeThis(DVNTAbstractDeviation.List paramList)
  {
    moreLikeThis = paramList;
  }
  
  public void setSeedDeviationId(String paramString)
  {
    seedDeviationId = paramString;
  }
}
