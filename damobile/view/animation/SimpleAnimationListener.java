package com.deviantart.android.damobile.view.animation;

import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public class SimpleAnimationListener
  implements Animation.AnimationListener
{
  public SimpleAnimationListener() {}
  
  public void onAnimationEnd(Animation paramAnimation) {}
  
  public void onAnimationRepeat(Animation paramAnimation) {}
  
  public void onAnimationStart(Animation paramAnimation) {}
}
