package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTCommentsUserProfileRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTDeviationCommentsThread>
{
  private final String commentId;
  private final Integer limit;
  private final Integer offset;
  private final String userName;
  
  public DVNTCommentsUserProfileRequestV1(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTDeviationCommentsThread.class);
    offset = paramInteger1;
    limit = paramInteger2;
    commentId = paramString2;
    userName = paramString1;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTCommentsUserProfileRequestV1 localDVNTCommentsUserProfileRequestV1 = (DVNTCommentsUserProfileRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(userName, userName).append(commentId, commentId).append(offset, offset).append(limit, limit).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "comment_deviation" + userName + commentId + offset + limit;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(userName).append(commentId).append(offset).append(limit).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTDeviationCommentsThread sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).commentsUserProfile(paramString, userName, commentId, offset, limit, Boolean.valueOf(true));
  }
}
