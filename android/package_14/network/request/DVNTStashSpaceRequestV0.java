package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV0;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTStashSpaceRequestV0
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV0<com.deviantart.android.sdk.api.model.DVNTStashSpace>
{
  public DVNTStashSpaceRequestV0()
  {
    super(com.deviantart.android.sdk.api.model.DVNTStashSpace.class);
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTStashSpace sendRequest(String paramString)
  {
    return ((DVNTServiceV0)getService()).stashSpace(paramString);
  }
}
