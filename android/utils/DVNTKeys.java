package com.deviantart.android.android.utils;

public class DVNTKeys
{
  public static final String CALCULATE_SIZE = "calculate_size";
  public static final String DAML = "daml";
  public static final String DEVIATIONIDS = "deviationids";
  public static final String EMAIL = "email";
  public static final String EXT_CAMERA = "ext_camera";
  public static final String EXT_COLLECTION = "ext_collection";
  public static final String EXT_PRELOAD = "ext_preload";
  public static final String EXT_STATS = "ext_stats";
  public static final String EXT_SUBMISSION = "ext_submission";
  public static final String LIMIT = "limit";
  public static final String OFFSET = "offset";
  public static final String PASSWORD = "password";
  public static final String QUERY = "query";
  public static final String SEED = "seed";
  public static final String USERNAME = "username";
  public static final String USERNAMES = "usernames";
  
  public DVNTKeys() {}
}
