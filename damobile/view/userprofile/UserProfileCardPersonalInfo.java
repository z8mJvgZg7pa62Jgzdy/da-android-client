package com.deviantart.android.damobile.view.userprofile;

import android.content.Context;
import android.content.res.Resources;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.DAFormatUtils.DateFormats;
import com.deviantart.android.damobile.util.MemberType;
import com.deviantart.android.damobile.util.SexType;
import com.deviantart.android.damobile.util.UserDisplay;
import com.deviantart.android.damobile.util.ViewHelper;
import java.text.SimpleDateFormat;

public class UserProfileCardPersonalInfo
  extends FrameLayout
  implements UserProfileCardBase
{
  @Bind({2131690100})
  TextView ageText;
  @Bind({2131690101})
  TextView country;
  @Bind({2131690103})
  TextView joinedAgo;
  @Bind({2131690104})
  TextView joinedDate;
  @Bind({2131690102})
  TextView memberTitle;
  @Bind({2131689841})
  TextView realName;
  
  public UserProfileCardPersonalInfo(Context paramContext)
  {
    super(paramContext);
    LayoutInflater.from(paramContext).inflate(2130968792, this, true);
    ButterKnife.bind(this);
    realName.setSelected(true);
    country.setSelected(true);
  }
  
  public void onCreateView(UserCardData paramUserCardData)
  {
    ViewHelper.append(realName, paramUserCardData.getColor());
    Integer localInteger = paramUserCardData.getCanonicalPath();
    Object localObject2 = SexType.get(paramUserCardData.d());
    Object localObject1 = "";
    if (localInteger != null)
    {
      localObject1 = localInteger.toString();
      if ((localObject2 == null) || (localObject2 == SexType.l)) {
        break label239;
      }
      ageText.setText(DAFormatUtils.a(getContext(), ((SexType)localObject2).getTitle(), (String)localObject1));
      label75:
      localObject1 = paramUserCardData.getParent();
      if (TextUtils.isEmpty((CharSequence)localObject1)) {
        break label250;
      }
      country.setText(DAFormatUtils.a(getContext(), 2131231050, (String)localObject1));
      label106:
      localObject1 = paramUserCardData.a();
      if (localObject1 != null)
      {
        if (localObject1 != MemberType.Eb) {
          break label262;
        }
        memberTitle.setText(((MemberType)localObject1).c());
      }
      label135:
      localObject2 = DAFormatUtils.getDate(getContext(), paramUserCardData.get());
      localObject1 = localObject2;
      if (localObject2 != null) {
        break label313;
      }
      joinedAgo.setVisibility(4);
    }
    for (;;)
    {
      if (localObject1 != null) {
        joinedAgo.setText(DAFormatUtils.a(getContext(), 2131230826, (String)localObject1));
      }
      paramUserCardData = DAFormatUtils.parse(paramUserCardData.get());
      if ((paramUserCardData != null) && ((localObject1 == null) || (!((String)localObject1).equals(getContext().getString(2131231036))))) {
        break label364;
      }
      joinedDate.setVisibility(4);
      return;
      if (localObject2 == null) {
        break;
      }
      localObject1 = ((SexType)localObject2).getValue();
      break;
      label239:
      ageText.setText((CharSequence)localObject1);
      break label75;
      label250:
      country.setVisibility(8);
      break label106;
      label262:
      localObject2 = new SpannableStringBuilder();
      ((SpannableStringBuilder)localObject2).append(UserDisplay.d(getContext(), (MemberType)localObject1)).append(" ").append(getContext().getString(((MemberType)localObject1).c()));
      memberTitle.setText((CharSequence)localObject2);
      break label135;
      label313:
      if (!((String)localObject2).equals(getContext().getString(2131231036))) {
        localObject1 = getResources().getString(2131230856) + " " + (String)localObject2;
      }
    }
    label364:
    joinedDate.setText(DAFormatUtils.a(getContext(), 2131230819, getResources().getString(2131231035) + " " + DAFormatUtils.DateFormats.dateFmt.format(paramUserCardData)));
  }
}
