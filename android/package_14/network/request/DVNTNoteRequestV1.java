package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTNoteRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTNote>
{
  private final String noteId;
  
  public DVNTNoteRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTNote.class);
    noteId = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()) && (super.equals(paramObject)))
    {
      paramObject = (DVNTNoteRequestV1)paramObject;
      return Objects.append(noteId, noteId);
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "getnote" + noteId;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 3600000L;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), noteId });
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTNote sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).getNote(paramString, noteId, Boolean.valueOf(true));
  }
}
