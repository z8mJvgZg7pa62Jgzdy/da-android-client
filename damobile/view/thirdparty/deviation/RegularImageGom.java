package com.deviantart.android.damobile.view.thirdparty.deviation;

import android.app.Activity;
import android.content.Context;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.view.thirdparty.GomType;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.AbstractDraweeControllerBuilder;
import com.facebook.drawee.drawable.ScalingUtils.ScaleType;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.DraweeView;
import com.facebook.drawee.view.GenericDraweeView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

public class RegularImageGom
  extends com.deviantart.android.damobile.view.gom.deviation.DeviationGom<com.deviantart.android.damobile.view.gom.deviation.viewholder.ImageViewHolder>
{
  public RegularImageGom() {}
  
  protected DVNTImage a(Context paramContext, DVNTDeviation paramDVNTDeviation)
  {
    DVNTImage localDVNTImage1;
    int i;
    if ((c == 0) || (b == 0))
    {
      DVNTImage localDVNTImage2 = DeviationUtils.b(paramDVNTDeviation, false);
      localDVNTImage1 = localDVNTImage2;
      if (localDVNTImage2 == null) {
        localDVNTImage1 = DeviationUtils.b(paramDVNTDeviation);
      }
      if (c == 0) {
        break label85;
      }
      i = c;
      label48:
      if (b == 0) {
        break label94;
      }
    }
    label85:
    label94:
    for (int j = b;; j = localDVNTImage1.getHeight())
    {
      return ImageUtils.a((Activity)paramContext, paramDVNTDeviation, localDVNTImage1, i, j, true);
      localDVNTImage1 = DeviationUtils.b(paramDVNTDeviation);
      break;
      i = localDVNTImage1.getWidth();
      break label48;
    }
  }
  
  public void a(Context paramContext, DeviationDescription paramDeviationDescription, com.deviantart.android.damobile.view.thirdparty.deviation.viewholder.ImageViewHolder paramImageViewHolder, boolean paramBoolean)
  {
    SimpleDraweeView localSimpleDraweeView = draweeView;
    DVNTDeviation localDVNTDeviation = paramDeviationDescription.a();
    if (a(paramContext, localDVNTDeviation, localSimpleDraweeView)) {
      return;
    }
    DVNTImage localDVNTImage1 = a(paramContext, localDVNTDeviation);
    DVNTImage localDVNTImage2 = DeviationUtils.b(localDVNTDeviation);
    Object localObject = localSimpleDraweeView.getLayoutParams();
    localSimpleDraweeView.setAspectRatio(localDVNTImage1.getWidth() / localDVNTImage1.getHeight());
    if ((c != 0) && (b != 0)) {}
    for (int i = -1;; i = -2)
    {
      height = i;
      localSimpleDraweeView.setHierarchy(new GenericDraweeHierarchyBuilder(paramContext.getResources()).a(new ColorDrawable(ContextCompat.getColor(paramContext, 2131558499))).a(ScalingUtils.ScaleType.I).a(new PointF(0.5F, 0.0F)).c());
      localObject = (PipelineDraweeControllerBuilder)((PipelineDraweeControllerBuilder)Fresco.b().a(true)).a(ImageRequestBuilder.write(Uri.parse(localDVNTImage1.getSrc())).c(true).a());
      if ((localDVNTImage2 != null) && (!localDVNTImage2.equals(localDVNTImage1))) {
        ((AbstractDraweeControllerBuilder)localObject).b(ImageRequest.fromUri(localDVNTImage2.getSrc()));
      }
      localSimpleDraweeView.setController(((AbstractDraweeControllerBuilder)localObject).visitAnnotation());
      if (paramBoolean) {
        DeviationUtils.b(faveView, localDVNTDeviation);
      }
      super.b(paramContext, paramDeviationDescription, paramImageViewHolder, paramBoolean);
      return;
    }
  }
  
  protected boolean a(Context paramContext, DVNTDeviation paramDVNTDeviation, SimpleDraweeView paramSimpleDraweeView)
  {
    if (paramDVNTDeviation.isMature() == null) {}
    for (boolean bool = false; (bool) && (!UserUtils.get(paramContext)); bool = paramDVNTDeviation.isMature().booleanValue())
    {
      paramSimpleDraweeView.setAspectRatio(1.0F);
      ImageUtils.a(paramContext, paramSimpleDraweeView, 2130837731);
      return true;
    }
    return false;
  }
  
  public GomType b()
  {
    return GomType.c;
  }
  
  public com.deviantart.android.damobile.view.thirdparty.deviation.viewholder.ImageViewHolder inflate(Context paramContext, ViewGroup paramViewGroup)
  {
    return new com.deviantart.android.damobile.view.thirdparty.deviation.viewholder.ImageViewHolder(LayoutInflater.from(paramContext).inflate(2130968699, paramViewGroup, false));
  }
}
