package com.deviantart.android.damobile.view;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.fragment.LoginDialogFragment.OnCancelListener;
import com.deviantart.android.damobile.util.Keyboard;
import com.deviantart.android.damobile.util.SignUpHelper;

public class LoginRecoverUserNameLayout
  extends LinearLayout
{
  @Bind({2131689980})
  ProgressBar loading;
  LoginDialogFragment.OnCancelListener log;
  @Bind({2131689981})
  TextView message;
  @Bind({2131689979})
  TextView recoverButtonText;
  @Bind({2131689975})
  EditTextWithClearFocus recoverText;
  @Bind({2131689973})
  TextView recoverTitle;
  @Bind({2131689974})
  TextView validator;
  
  public LoginRecoverUserNameLayout(Context paramContext, LoginDialogFragment.OnCancelListener paramOnCancelListener)
  {
    super(paramContext);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130968738, this, true);
    ButterKnife.bind(this);
    recoverTitle.setText(paramContext.getString(2131231069));
    validator.setText(paramContext.getString(2131231058));
    recoverText.setHint(paramContext.getString(2131231058));
    message.setText(paramContext.getString(2131231070));
    log = paramOnCancelListener;
  }
  
  public void focusChangeEditText(boolean paramBoolean)
  {
    SignUpHelper.onItemSelected(paramBoolean, recoverText, validator, 2131231058);
  }
  
  public void onCancel()
  {
    log.i();
  }
  
  public void onRecover()
  {
    String str = recoverText.getText().toString();
    if (str.isEmpty())
    {
      recoverText.setError(getResources().getString(2131231059));
      return;
    }
    Keyboard.hideKeyboard(getContext(), recoverText);
    loading.setVisibility(0);
    recoverButtonText.setVisibility(8);
    DVNTAsyncAPI.recoverAccountFromEmail(str).call(getContext(), new LoginRecoverUserNameLayout.1(this));
  }
}
