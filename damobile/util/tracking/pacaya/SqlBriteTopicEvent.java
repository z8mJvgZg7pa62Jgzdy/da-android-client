package com.deviantart.android.damobile.util.tracking.pacaya;

import android.content.ContentValues;
import android.provider.BaseColumns;
import com.deviantart.upgrade.labs.DVNTTopicEvent;
import com.google.gson.Gson;

public class SqlBriteTopicEvent
  implements BaseColumns
{
  public SqlBriteTopicEvent() {}
  
  public static ContentValues getContentValues(DVNTTopicEvent paramDVNTTopicEvent)
  {
    ContentValues localContentValues = new ContentValues();
    localContentValues.put("_id", Long.valueOf(Long.parseLong(f, 16)));
    localContentValues.put("topic", c);
    localContentValues.put("payload", new Gson().toJson(a));
    return localContentValues;
  }
}
