package com.deviantart.android.android.package_14.config;

public class DVNTAbstractAPIConfig
{
  private String clientId;
  private String clientSecret;
  private DVNTGraduateHandler graduateHandler;
  private String redirectURI;
  private String scope;
  private String sessionId;
  private Boolean showMatureContent;
  
  public DVNTAbstractAPIConfig() {}
  
  public String buildRequestVersion()
  {
    return getUserAgent() + "-" + getShowMatureContent();
  }
  
  public String getClientId()
  {
    return clientId;
  }
  
  public String getClientSecret()
  {
    return clientSecret;
  }
  
  public DVNTGraduateHandler getGraduateHandler()
  {
    return graduateHandler;
  }
  
  public String getRedirectURI()
  {
    return redirectURI;
  }
  
  public String getScope()
  {
    return scope;
  }
  
  public String getSessionId()
  {
    return sessionId;
  }
  
  public Boolean getShowMatureContent()
  {
    return showMatureContent;
  }
  
  public String getUserAgent()
  {
    return "AndroidSDK-based-" + getClientId();
  }
  
  public void setClientId(String paramString)
  {
    clientId = paramString;
  }
  
  public void setClientSecret(String paramString)
  {
    clientSecret = paramString;
  }
  
  public void setGraduateHandler(DVNTGraduateHandler paramDVNTGraduateHandler)
  {
    graduateHandler = paramDVNTGraduateHandler;
  }
  
  public DVNTAbstractAPIConfig setRedirectURI(String paramString)
  {
    redirectURI = paramString;
    return this;
  }
  
  public void setScope(String paramString)
  {
    scope = paramString;
  }
  
  public void setSessionId(String paramString)
  {
    sessionId = paramString;
  }
  
  public void setShowMatureContent(Boolean paramBoolean)
  {
    showMatureContent = paramBoolean;
  }
}
