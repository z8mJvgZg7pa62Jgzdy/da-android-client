package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTFriend;

public class APIWatchersLoader
  extends StreamLoader<DVNTFriend>
{
  private String d;
  
  public APIWatchersLoader(String paramString)
  {
    d = paramString;
    a = 20;
  }
  
  public String b()
  {
    return "watchersloader" + d;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.getWatchers(d, Integer.valueOf(paramInt), Integer.valueOf(a), true, false, false, true);
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APIWatchersLoader.1(this, paramStreamLoadListener));
  }
}
