package com.deviantart.android.damobile.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.damobile.drawable.SlashBackgroundDrawable;
import com.deviantart.android.damobile.fragment.DABaseFragment;
import com.deviantart.android.damobile.fragment.DiscoveryFragment;
import com.deviantart.android.damobile.fragment.SignUpRegistrationFragment;

public class VerifiedLinkLauncherActivity
  extends Activity
{
  public VerifiedLinkLauncherActivity() {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968611);
    getWindow().setBackgroundDrawable(SlashBackgroundDrawable.getDrawable(this));
    Intent localIntent = getIntent();
    boolean bool = DVNTAbstractAsyncAPI.isUserSession(this);
    DVNTAbstractAsyncAPI.clearCache();
    if (bool) {
      com.deviantart.android.damobile.util.UserUtils.h = true;
    }
    if ((!bool) || (SignUpRegistrationFragment.class.equals(DABaseFragment.type)))
    {
      localIntent.setComponent(new ComponentName(this, HomeActivity.class));
      if (DiscoveryFragment.class.equals(DABaseFragment.type)) {
        localIntent.setFlags(536870912);
      }
      finish();
      startActivity(localIntent);
      return;
    }
    if (BaseActivity.handler == null) {}
    for (paramBundle = HomeActivity.class;; paramBundle = BaseActivity.handler)
    {
      localIntent.setComponent(new ComponentName(this, paramBundle));
      localIntent.setFlags(536870912);
      break;
    }
  }
}
