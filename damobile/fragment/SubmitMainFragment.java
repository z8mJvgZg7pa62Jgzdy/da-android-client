package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Application;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.BaseBundle;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.listener.DVNTAsyncRequestListener;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTUserStatus;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.SubmitActivity;
import com.deviantart.android.damobile.util.Keyboard;
import com.deviantart.android.damobile.util.PermissionUtil;
import com.deviantart.android.damobile.util.Recent;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.ShareUtils;
import com.deviantart.android.damobile.util.SharedPreferenceUtil;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.submit.MimeType;
import com.deviantart.android.damobile.util.submit.SimpleTextStatus;
import com.deviantart.android.damobile.util.submit.Submission;
import com.deviantart.android.damobile.util.submit.Submission.CompletionChangeListener;
import com.deviantart.android.damobile.util.submit.SubmissionSender;
import com.deviantart.android.damobile.util.submit.SubmitOptions;
import com.deviantart.android.damobile.util.submit.SubmitType;
import com.deviantart.android.damobile.util.submit.pages.SubmitJournalPage;
import com.deviantart.android.damobile.util.submit.pages.SubmitLiteraturePage;
import com.deviantart.android.damobile.util.submit.pages.SubmitPage;
import com.deviantart.android.damobile.util.submit.pages.SubmitPhotoPage;
import com.deviantart.android.damobile.util.submit.pages.SubmitStatusPage;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.util.tracking.TrackerUtil.EventLabelBuilder;
import com.deviantart.android.damobile.view.RepostView;
import com.deviantart.android.damobile.view.viewpageindicator.CustomizableTabPageIndicator;
import com.deviantart.android.damobile.view.viewpageindicator.DATextTabIndicator;
import com.deviantart.android.damobile.view.viewpageindicator.SubmitPageIndicator;
import com.deviantart.datoolkit.logger.DVNTLog;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.commons.lang3.StringUtils;

public class SubmitMainFragment
  extends PhotoFragment
  implements Submission.CompletionChangeListener
{
  private int a = 0;
  private RepostView b = null;
  private ArrayList<SubmitPage> c;
  private boolean h = false;
  private boolean i = false;
  @Bind({2131689756})
  SubmitPageIndicator indicator;
  private Menu mOptionsMenu;
  @Bind({2131689755})
  ViewPager pager;
  @Bind({2131690048})
  ImageView submitAttachedPhotoIcon;
  @Bind({2131690047})
  ImageView submitGalleriesIcon;
  @Bind({2131690049})
  ImageView submitMentionsIcon;
  @Bind({2131690050})
  ImageView submitOptionsIcon;
  @Bind({2131690046})
  ImageView submitTagsIcon;
  
  public SubmitMainFragment() {}
  
  private DVNTAsyncRequestListener a(ArrayList paramArrayList, HashMap paramHashMap, SubmitSecondaryFragment paramSubmitSecondaryFragment)
  {
    return new SubmitMainFragment.5(this, paramArrayList, paramHashMap, paramSubmitSecondaryFragment);
  }
  
  private void a(SubmitType paramSubmitType)
  {
    ActionBar localActionBar = ((SubmitActivity)getActivity()).getSupportActionBar();
    switch (SubmitMainFragment.6.c[paramSubmitType.ordinal()])
    {
    default: 
      localActionBar.setTitle(2131231351);
      return;
    }
    localActionBar.setTitle(2131231354);
  }
  
  private void checkPermission(String paramString)
  {
    PermissionUtil.show(this, paramString, 3, 2131231204);
  }
  
  private static void create(Activity paramActivity, Submission paramSubmission)
  {
    TrackerUtil.EventLabelBuilder localEventLabelBuilder = new TrackerUtil.EventLabelBuilder();
    if (paramSubmission.size() != null)
    {
      localEventLabelBuilder.get("insertedimage", "yes");
      if (paramSubmission.getKey().isEmpty()) {
        break label146;
      }
      localEventLabelBuilder.get("insertedtag", "yes");
      label43:
      if (paramSubmission.get().isEmpty()) {
        break label158;
      }
      localEventLabelBuilder.get("insertedmention", "yes");
      label62:
      switch (SubmitMainFragment.6.c[paramSubmission.getString().ordinal()])
      {
      default: 
        break;
      }
    }
    for (;;)
    {
      TrackerUtil.read(paramActivity, EventKeys.Category.x, "submit_deviation", localEventLabelBuilder.getValue(), null);
      return;
      localEventLabelBuilder.get("insertedimage", "no");
      break;
      label146:
      localEventLabelBuilder.get("insertedtag", "no");
      break label43;
      label158:
      localEventLabelBuilder.get("insertedmention", "no");
      break label62;
      TrackerUtil.read(paramActivity, EventKeys.Category.x, "submit_status", localEventLabelBuilder.getValue(), null);
      return;
      if (!paramSubmission.remove().isEmpty()) {
        localEventLabelBuilder.get("infolder", "yes");
      } else {
        localEventLabelBuilder.get("infolder", "no");
      }
    }
  }
  
  private void onOptionsItemSelected()
  {
    Submission localSubmission = Submission.getInstance();
    if (localSubmission != null)
    {
      if (localSubmission.getString() == null) {
        return;
      }
      if (!localSubmission.create())
      {
        onPostExecute();
        return;
      }
      if (UserUtils.a == null)
      {
        Toast.makeText(getActivity(), getString(2131230960), 0).show();
        return;
      }
      if (!UserUtils.e)
      {
        UserUtils.showInfoDialog(getActivity());
        return;
      }
      Context localContext = getActivity().getApplicationContext();
      String str = new SubmissionSender((Application)localContext, localSubmission).doInBackground();
      if (str != null)
      {
        Toast.makeText(getActivity(), str, 0).show();
        return;
      }
      create(getActivity(), localSubmission);
      Toast.makeText(localContext, localContext.getString(2131231365), 0).show();
      getActivity().finish();
    }
  }
  
  private void onPostExecute()
  {
    ArrayList localArrayList = new ArrayList();
    Object localObject = Submission.getInstance();
    if ((((Submission)localObject).getString() != SubmitType.c) && (!((Submission)localObject).load())) {
      localArrayList.add(getString(2131231379));
    }
    switch (SubmitMainFragment.6.c[localObject.getString().ordinal()])
    {
    default: 
      break;
    }
    for (;;)
    {
      if ((((Submission)localObject).getString() != SubmitType.c) && (!((Submission)localObject).apply())) {
        localArrayList.add(getString(2131231356));
      }
      int j = 0;
      while (j < localArrayList.size())
      {
        localArrayList.set(j, "? " + (String)localArrayList.get(j));
        j += 1;
      }
      if (!((Submission)localObject).add())
      {
        localArrayList.add(getString(2131231362));
        continue;
        if (!((Submission)localObject).equals())
        {
          localArrayList.add(getString(2131231366));
          continue;
          if (!((Submission)localObject).equals())
          {
            localArrayList.add(getString(2131231364));
            continue;
            if (!((Submission)localObject).parse()) {
              localArrayList.add(getString(2131231373));
            }
          }
        }
      }
    }
    localObject = new AlertDialog.Builder(getActivity());
    ((AlertDialog.Builder)localObject).setTitle(2131231363).setMessage(TextUtils.join("\n", localArrayList)).setIcon(17301543).setNeutralButton(2131231183, new SubmitMainFragment.1(this));
    ((AlertDialog.Builder)localObject).create().show();
  }
  
  private void onRequestPermissionsResult()
  {
    if (Build.VERSION.SDK_INT < 16) {}
    for (String str = "android.permission.WRITE_EXTERNAL_STORAGE"; ContextCompat.checkSelfPermission(getActivity(), str) == 0; str = "android.permission.READ_EXTERNAL_STORAGE")
    {
      Toast.makeText(getActivity(), 2131230956, 0).show();
      return;
    }
    checkPermission(str);
  }
  
  public void a(Submission paramSubmission)
  {
    if (mOptionsMenu == null)
    {
      Log.d("Submit", "Menu not ready");
      return;
    }
    MenuItem localMenuItem = mOptionsMenu.findItem(2131690164);
    if (localMenuItem == null)
    {
      Log.d("Submit", "Menu item not ready");
      return;
    }
    if (paramSubmission.create()) {}
    for (int j = 2130837781;; j = 2130837780)
    {
      localMenuItem.setIcon(j);
      return;
    }
  }
  
  public void b(SimpleDraweeView paramSimpleDraweeView)
  {
    DVNTLog.get("submit - showAttachedImageIfAny", new Object[0]);
    if ((Submission.getInstance().size() == null) || (paramSimpleDraweeView == null))
    {
      DVNTLog.get("submit - no image file to show or previewphoto is null", new Object[0]);
      return;
    }
    paramSimpleDraweeView.setVisibility(0);
  }
  
  public void onClickSubmitGalleriesIcon()
  {
    Object localObject2 = new Recent(getActivity(), "recent_galleries");
    Object localObject1 = new ArrayList();
    localObject2 = ((Recent)localObject2).read().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      localObject3 = StringUtils.split((String)((Iterator)localObject2).next(), "||");
      if (localObject3.length >= 2) {
        ((ArrayList)localObject1).add(new SubmitGallery(localObject3[0], localObject3[1]));
      }
    }
    localObject2 = new HashMap();
    ((HashMap)localObject2).put(getActivity().getString(2131231228), localObject1);
    localObject1 = SubmitSecondaryFragment.a(getActivity().getString(2131230790), 1, (HashMap)localObject2, Submission.getInstance().remove());
    ((SubmitSecondaryFragment)localObject1).b(new SubmitMainFragment.4(this, (SubmitSecondaryFragment)localObject1, (HashMap)localObject2));
    Object localObject3 = new ArrayList();
    DVNTCommonAsyncAPI.getGalleryFolders(null, false, false, Integer.valueOf(0), Integer.valueOf(50)).call(getActivity(), a((ArrayList)localObject3, (HashMap)localObject2, (SubmitSecondaryFragment)localObject1));
  }
  
  public void onClickSubmitMentionsIcon()
  {
    Object localObject2 = new Recent(getActivity(), "recent_mentions");
    Object localObject1 = new ArrayList();
    localObject2 = ((Recent)localObject2).read().iterator();
    while (((Iterator)localObject2).hasNext())
    {
      String[] arrayOfString = ((String)((Iterator)localObject2).next()).split("&&");
      if (arrayOfString.length == 2) {
        ((ArrayList)localObject1).add(new SubmitMention(arrayOfString[0], arrayOfString[1]));
      }
    }
    localObject2 = new HashMap();
    ((HashMap)localObject2).put(getActivity().getString(2131231229), localObject1);
    localObject1 = SubmitSecondaryFragment.a(getString(2131230788), 1, (HashMap)localObject2, Submission.getInstance().get());
    ((SubmitSecondaryFragment)localObject1).b(new SubmitMainFragment.3(this, (SubmitSecondaryFragment)localObject1));
    Keyboard.hideKeyboard(getActivity());
    ScreenFlowManager.a(getActivity(), (Fragment)localObject1, "submit_mentions");
  }
  
  public void onClickSubmitOptionsIcon()
  {
    Keyboard.hideKeyboard(getActivity());
    ScreenFlowManager.a(getActivity(), new SubmitOptionsFragment(), "submit_options");
  }
  
  public void onClickSubmitTagsIcon()
  {
    Object localObject2 = new Recent(getActivity(), "recent_tags");
    Object localObject1 = new ArrayList();
    localObject2 = ((Recent)localObject2).read().iterator();
    while (((Iterator)localObject2).hasNext()) {
      ((ArrayList)localObject1).add(new SubmitTag((String)((Iterator)localObject2).next()));
    }
    localObject2 = new HashMap();
    ((HashMap)localObject2).put(getActivity().getString(2131231230), localObject1);
    localObject1 = SubmitSecondaryFragment.a(getString(2131230789), 3, (HashMap)localObject2, Submission.getInstance().getKey());
    ((SubmitSecondaryFragment)localObject1).b(new SubmitMainFragment.2(this, (SubmitSecondaryFragment)localObject1));
    Keyboard.hideKeyboard(getActivity());
    ScreenFlowManager.a(getActivity(), (Fragment)localObject1, "submit_tags");
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setHasOptionsMenu(true);
    Submission.getInstance().setTitle(this);
    if (UserUtils.a == null) {
      load();
    }
    if (paramBundle != null) {
      h = paramBundle.getBoolean("did_extfile_check", false);
    }
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    mOptionsMenu = paramMenu;
    paramMenuInflater.inflate(2131820554, paramMenu);
    a(Submission.getInstance());
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    View localView = paramLayoutInflater.inflate(2130968684, paramViewGroup, false);
    localBundle = getArguments();
    bool = localBundle.containsKey("mode");
    localSubmission = Submission.getInstance();
    i = false;
    if (!bool)
    {
      localSubmission.setTitle(SubmitType.a);
      if (localBundle.getSerializable("repost_deviation_info") != null)
      {
        b = new RepostView(getActivity());
        paramLayoutInflater = (DVNTDeviation)localBundle.getSerializable("repost_deviation_info");
        localSubmission.setTitle(paramLayoutInflater.getId());
        b.onCreateView(paramLayoutInflater);
      }
    }
    for (;;)
    {
      ButterKnife.bind(this, localView);
      paramLayoutInflater = SharedPreferenceUtil.get(getActivity()).getString("submit_remembered_settings", null);
      if ((!localSubmission.getValue()) && (paramLayoutInflater != null)) {}
      try
      {
        paramLayoutInflater = new Gson().fromJson(paramLayoutInflater, SubmitOptions.class);
        paramLayoutInflater = (SubmitOptions)paramLayoutInflater;
        localSubmission.setTitle(paramLayoutInflater);
        localSubmission.setTitle(true);
      }
      catch (Exception paramLayoutInflater)
      {
        for (;;)
        {
          SimpleTextStatus localSimpleTextStatus;
          String str;
          Object localObject;
          DVNTLog.append("submit - saved setting JSON failed to decode", new Object[0]);
          continue;
          c.add(new SubmitPhotoPage(getString(2131230807)));
          c.add(new SubmitJournalPage(getString(2131231037)));
          c.add(new SubmitLiteraturePage(getString(2131231048)));
          c.add(new SubmitStatusPage(getString(2131231347)));
          continue;
          if (bool)
          {
            paramViewGroup = SubmitType.findByName(localBundle.getString("mode"));
            if (paramViewGroup != null)
            {
              a = paramViewGroup.ordinal();
              localSubmission.setTitle(paramViewGroup);
            }
          }
        }
      }
      c = new ArrayList();
      if (b == null) {
        break;
      }
      localSubmission.setTitle(SubmitType.c);
      c.add(new SubmitStatusPage(getString(2131231239)));
      pager.setAdapter(new SubmitMainFragment.SubmitPagerAdapter(this, c));
      indicator.setViewPager(pager);
      paramLayoutInflater = new SubmitMainFragment.SubmitOnPageChangeListener(this, null);
      pager.setOnPageChangeListener(paramLayoutInflater);
      if (paramBundle == null) {
        break label767;
      }
      a = paramBundle.getInt("current_page");
      paramLayoutInflater.b(a);
      indicator.setCurrentItem(a);
      localSubmission.setTitle();
      return localView;
      if (localBundle.getSerializable("repost_status_info") != null)
      {
        b = new RepostView(getActivity());
        paramLayoutInflater = (DVNTUserStatus)localBundle.getSerializable("repost_status_info");
        localSubmission.setTitle(paramLayoutInflater.getStatusId());
        b.b(paramLayoutInflater);
      }
      else
      {
        Log.e(getClass().getName(), "Received unexpected bundle argument.");
        return localView;
        if (localBundle.getSerializable("prefilled_status") != null)
        {
          localSimpleTextStatus = (SimpleTextStatus)localBundle.getSerializable("prefilled_status");
          paramViewGroup = "";
          str = "";
          localObject = str;
          paramLayoutInflater = paramViewGroup;
          if (localSimpleTextStatus != null)
          {
            paramLayoutInflater = paramViewGroup;
            if (localSimpleTextStatus.c() != null)
            {
              paramLayoutInflater = paramViewGroup;
              if (!localSimpleTextStatus.c().isEmpty()) {
                paramLayoutInflater = localSimpleTextStatus.c() + "\n\n";
              }
            }
            paramViewGroup = str;
            if (localSimpleTextStatus.b() != null)
            {
              paramViewGroup = str;
              if (!localSimpleTextStatus.b().isEmpty()) {
                paramViewGroup = localSimpleTextStatus.b();
              }
            }
            DVNTLog.get("submit - received prefilled status with text : {}", new Object[] { localSimpleTextStatus.toString() });
            localObject = paramViewGroup;
          }
          localSubmission.setUrl(paramLayoutInflater + (String)localObject);
        }
        else if (localBundle.getParcelable("prefilled_file_uri") != null)
        {
          DVNTLog.get("submit - received prefilled file URI - {}", new Object[] { localBundle.getParcelable("prefilled_file_uri") });
          paramLayoutInflater = (Uri)localBundle.getParcelable("prefilled_file_uri");
          try
          {
            localSubmission.setTitle(ShareUtils.get(getActivity(), paramLayoutInflater));
            paramLayoutInflater = MimeType.d;
            localSubmission.setTitle(paramLayoutInflater);
          }
          catch (Exception paramLayoutInflater)
          {
            DVNTLog.get("submit - error with prefilled file URI - {}", new Object[] { paramLayoutInflater.getMessage() });
            i = true;
          }
        }
      }
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    Submission.getInstance().setTitle(null);
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
    b = null;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    }
    onOptionsItemSelected();
    return true;
  }
  
  public void onOptionsMenuClosed(Menu paramMenu)
  {
    mOptionsMenu = null;
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    Activity localActivity = getActivity();
    if (DVNTContextUtils.isContextDead(localActivity)) {
      return;
    }
    if (paramArrayOfString.length == 0)
    {
      DVNTLog.get("submit - cancelled permission request?", new Object[0]);
      return;
    }
    super.onRequestPermissionsResult(paramInt, paramArrayOfString, paramArrayOfInt);
    switch (paramInt)
    {
    default: 
      return;
    }
    if (PermissionUtil.execute(this, paramArrayOfString, paramArrayOfInt, 2131231191, 2131231198))
    {
      h = false;
      localActivity.recreate();
    }
  }
  
  public void onResume()
  {
    super.onResume();
    if (c == null) {
      return;
    }
    SubmitPage localSubmitPage = (SubmitPage)c.get(a);
    localSubmitPage.onBindViewHolder();
    a(localSubmitPage.c());
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putInt("current_page", a);
    paramBundle.putBoolean("did_extfile_check", h);
    super.onSaveInstanceState(paramBundle);
  }
  
  public void onViewCreated(View paramView, Bundle paramBundle)
  {
    super.onViewCreated(paramView, paramBundle);
    if ((i) && (!h))
    {
      onRequestPermissionsResult();
      h = true;
    }
  }
}
