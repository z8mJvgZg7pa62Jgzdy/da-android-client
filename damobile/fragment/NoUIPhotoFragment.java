package com.deviantart.android.damobile.fragment;

import android.content.Intent;

public class NoUIPhotoFragment
  extends PhotoFragment
{
  public NoUIPhotoFragment() {}
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
}
