package com.deviantart.android.damobile.util.discovery.pages;

import com.deviantart.android.android.package_14.model.DVNTCategory;

public class DiscoveryCategorySelectorPage
  extends DiscoveryCategoryBrowsePage
{
  protected DiscoveryCategorySelectorPage.OnSelectCategoryListener G;
  
  public DiscoveryCategorySelectorPage(String paramString1, String paramString2, String paramString3)
  {
    super(paramString1, paramString2, paramString3);
  }
  
  public void b(DiscoveryCategorySelectorPage.OnSelectCategoryListener paramOnSelectCategoryListener)
  {
    G = paramOnSelectCategoryListener;
  }
  
  protected boolean f()
  {
    return false;
  }
  
  protected boolean n()
  {
    return false;
  }
  
  public void onItemClick(DVNTCategory paramDVNTCategory)
  {
    G.onClick(paramDVNTCategory);
  }
}
