package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.HomeActivity.HomeActivityPages;
import com.deviantart.android.damobile.util.ScreenFlowManager;

public class WatchLoadingFragment
  extends LoadingFragment
{
  public WatchLoadingFragment() {}
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    load();
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public void sendComment()
  {
    if (DVNTContextUtils.isContextDead(getActivity())) {
      return;
    }
    ScreenFlowManager.goHome(getActivity());
    ScreenFlowManager.get(getActivity(), new WatchFeedFragment(), HomeActivity.HomeActivityPages.p.a(), false);
  }
}
