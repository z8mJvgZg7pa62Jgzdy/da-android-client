package com.deviantart.android.damobile.util;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.OnOffsetChangedListener;

public class AppBarStateChangeListener
  implements AppBarLayout.OnOffsetChangedListener
{
  private AppBarStateChangeListener.State mPopupMenu = AppBarStateChangeListener.State.mMenu;
  private final AppBarStateChangeListener.AppBarStateChangeNotifier this$0;
  
  public AppBarStateChangeListener(AppBarStateChangeListener.AppBarStateChangeNotifier paramAppBarStateChangeNotifier)
  {
    this$0 = paramAppBarStateChangeNotifier;
  }
  
  public final void onOffsetChanged(AppBarLayout paramAppBarLayout, int paramInt)
  {
    if (paramInt == 0)
    {
      if (mPopupMenu != AppBarStateChangeListener.State.mPopupMenu) {
        this$0.goTo();
      }
      mPopupMenu = AppBarStateChangeListener.State.mPopupMenu;
      return;
    }
    if (Math.abs(paramInt) >= paramAppBarLayout.getTotalScrollRange())
    {
      if (mPopupMenu != AppBarStateChangeListener.State.this$0) {
        this$0.validateInput();
      }
      mPopupMenu = AppBarStateChangeListener.State.this$0;
      return;
    }
    if (mPopupMenu != AppBarStateChangeListener.State.mMenu) {
      this$0.logPreferences();
    }
    mPopupMenu = AppBarStateChangeListener.State.mMenu;
  }
}
