package com.deviantart.android.damobile.util;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.deviantart.android.damobile.view.viewpageindicator.DeviationTabIndicator;

public class DeviationFullViewBehavior
  extends ProfileCardBehavior
{
  public DeviationFullViewBehavior() {}
  
  public DeviationFullViewBehavior(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public boolean onInterceptTouchEvent(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, MotionEvent paramMotionEvent)
  {
    DeviationTabIndicator localDeviationTabIndicator = (DeviationTabIndicator)paramAppBarLayout.findViewById(2131689665);
    View localView = paramAppBarLayout.findViewById(2131689753);
    if ((localDeviationTabIndicator == null) || (localView == null)) {
      return super.onInterceptTouchEvent(paramCoordinatorLayout, paramAppBarLayout, paramMotionEvent);
    }
    if (localDeviationTabIndicator.d()) {
      return false;
    }
    if (paramCoordinatorLayout.isPointInChildBounds(localDeviationTabIndicator, (int)paramMotionEvent.getX(), (int)paramMotionEvent.getY())) {
      return super.onInterceptTouchEvent(paramCoordinatorLayout, paramAppBarLayout, paramMotionEvent);
    }
    if (paramCoordinatorLayout.isPointInChildBounds(localView, (int)paramMotionEvent.getX(), (int)paramMotionEvent.getY())) {
      return false;
    }
    return super.onInterceptTouchEvent(paramCoordinatorLayout, paramAppBarLayout, paramMotionEvent);
  }
}
