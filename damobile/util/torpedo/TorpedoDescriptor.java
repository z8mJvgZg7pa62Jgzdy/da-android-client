package com.deviantart.android.damobile.util.torpedo;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class TorpedoDescriptor<T>
{
  public Torpedo.TorpedoLane a;
  private int b = 0;
  private boolean c = true;
  private int d;
  private boolean f = false;
  private int h;
  private final int j;
  private boolean k = false;
  private Torpedo l;
  protected ArrayList<T> m = new ArrayList();
  private int n;
  private int s;
  private int x;
  private int y;
  
  public TorpedoDescriptor(int paramInt1, int paramInt2, int paramInt3)
  {
    s = paramInt1;
    x = paramInt2;
    d = paramInt3;
    j = Double.valueOf(0.5D * paramInt2).intValue();
    a();
  }
  
  private boolean a(Object paramObject, boolean paramBoolean)
  {
    int i1 = 0;
    paramBoolean = f(paramObject);
    if ((a.c() >= d) || (paramBoolean)) {
      write();
    }
    Log.d("twoview", "add item d1/d2" + d(paramObject) + "/" + c(paramObject));
    Torpedo.TorpedoThumb localTorpedoThumb = new Torpedo.TorpedoThumb();
    int i2 = Math.max(Float.valueOf(y / c(paramObject) * d(paramObject)).intValue(), n);
    x = i2;
    y = y;
    if (i2 < s / 20) {}
    for (int i = 1;; i = 0)
    {
      if (i2 > (s - a.getValue()) * 2) {
        i1 = 1;
      }
      if (i1 != 0) {
        write();
      }
      a.a(localTorpedoThumb);
      if ((i == 0) && (!paramBoolean)) {
        break;
      }
      write();
      return true;
    }
    return true;
  }
  
  private void calculate()
  {
    float f1 = s / a.getValue();
    int i = y;
    int i1 = Math.max(Math.min(Float.valueOf(f1 * y).intValue(), i * 2), y);
    f1 = i1 / y;
    Object localObject = a.get().iterator();
    i = 0;
    while (((Iterator)localObject).hasNext())
    {
      Torpedo.TorpedoThumb localTorpedoThumb = (Torpedo.TorpedoThumb)((Iterator)localObject).next();
      x = Float.valueOf(x * f1).intValue();
      i += x;
      y = i1;
    }
    a.b(i);
    i = s - a.getValue();
    if (i != 0)
    {
      localObject = (Torpedo.TorpedoThumb)a.get().get(a.c() - 1);
      x = (i + x);
    }
    a.b(s);
    a.set(i1);
  }
  
  private void draw(float paramFloat)
  {
    int i = x;
    Iterator localIterator = l.get().iterator();
    if (localIterator.hasNext())
    {
      Object localObject = (Torpedo.TorpedoLane)localIterator.next();
      int i2 = Math.min(Float.valueOf(((Torpedo.TorpedoLane)localObject).e() * paramFloat).intValue(), i);
      ((Torpedo.TorpedoLane)localObject).set(i2);
      int i1 = i - i2;
      localObject = ((Torpedo.TorpedoLane)localObject).get().iterator();
      for (;;)
      {
        i = i1;
        if (!((Iterator)localObject).hasNext()) {
          break;
        }
        nexty = i2;
      }
    }
    l.d(x);
    f = true;
  }
  
  private void g()
  {
    int i = l.b() - x;
    float f1 = x / l.b();
    if (i > 0)
    {
      if (i <= j) {
        draw(f1);
      }
    }
    else {
      draw(f1);
    }
  }
  
  private void write()
  {
    Log.d("twoview", "seal lane");
    if ((a != null) && (!a.isEmpty()))
    {
      calculate();
      l.a(a);
      Iterator localIterator = a.get().iterator();
      while (localIterator.hasNext())
      {
        Torpedo.TorpedoThumb localTorpedoThumb = (Torpedo.TorpedoThumb)localIterator.next();
        Log.d("twoview", "with lane dim1/dim2 " + x + "/" + y);
      }
    }
    a = new Torpedo.TorpedoLane();
  }
  
  public int a(List paramList)
  {
    int i = 0;
    paramList = paramList.iterator();
    while (paramList.hasNext()) {
      if (a(paramList.next())) {
        i += 1;
      }
    }
    return i;
  }
  
  public void a()
  {
    if (s > x * 1.5D)
    {
      h = 2;
      d += 1;
      return;
    }
    h = 5;
  }
  
  public void a(int paramInt)
  {
    h = paramInt;
  }
  
  public boolean a(Object paramObject)
  {
    if (!b(paramObject)) {
      return false;
    }
    m.add(paramObject);
    return true;
  }
  
  public Torpedo b()
  {
    y = (x / h);
    n = (s / 4);
    if (l == null)
    {
      l = new Torpedo(x + j);
      if (b > 0) {
        l.b(b);
      }
      write();
    }
    Iterator localIterator = m.subList(l.a() + a.c(), m.size()).iterator();
    while (localIterator.hasNext()) {
      a(localIterator.next(), false);
    }
    return l;
  }
  
  public void b(int paramInt)
  {
    x = paramInt;
  }
  
  public abstract boolean b(Object paramObject);
  
  public abstract int c(Object paramObject);
  
  public boolean c()
  {
    return f;
  }
  
  public abstract int d(Object paramObject);
  
  public Torpedo d()
  {
    if (l == null) {
      b();
    }
    if (k) {
      write();
    }
    if (l.a() == 1)
    {
      l.a(0).y = x;
      f = true;
      return l;
    }
    if ((k) && (c)) {
      g();
    }
    return l;
  }
  
  public void d(int paramInt)
  {
    b = paramInt;
  }
  
  public void d(boolean paramBoolean)
  {
    c = paramBoolean;
  }
  
  public void e()
  {
    k = true;
  }
  
  public abstract boolean f(Object paramObject);
  
  public void g(int paramInt)
  {
    s = paramInt;
  }
  
  public ArrayList m()
  {
    return m;
  }
  
  public void set(int paramInt)
  {
    d = paramInt;
  }
}
