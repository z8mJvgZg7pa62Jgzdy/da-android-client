package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.BaseBundle;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTNote;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnBackPressedEvent;
import com.deviantart.android.damobile.util.DelayedUserInputTask;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.Keyboard;
import com.deviantart.android.damobile.util.Recent;
import com.deviantart.android.damobile.util.Recipient;
import com.deviantart.android.damobile.util.Recipient.RecipientStatus;
import com.deviantart.android.damobile.util.RecipientEditText;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.notes.NoteToSend;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.util.tracking.pacaya.MobileLava;
import com.deviantart.android.damobile.util.tracking.pacaya.UxTopicEventCreator;
import com.deviantart.android.damobile.view.RepostView;
import com.deviantart.pacaya.Pacaya;
import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.apmem.tools.layouts.FlowLayout;

public class CreateNoteFragment
  extends PhotoFragment
{
  private NoteToSend a;
  private Menu c;
  @Bind({2131689747})
  LinearLayout contentArea;
  @Bind({2131689745})
  EditText contentEditText;
  private DelayedUserInputTask currentTask;
  @Bind({2131689741})
  RecipientEditText recipientEditText;
  @Bind({2131689737})
  RelativeLayout recipientsContainer;
  @Bind({2131689739})
  FlowLayout recipientsFlowLayout;
  @Bind({2131689744})
  LinearLayout recipientsSuggestions;
  @Bind({2131689746})
  ProgressBar replyLoading;
  @Bind({2131689742})
  EditText subjectEditText;
  @Bind({2131689740})
  ImageView suggestionButton;
  @Bind({2131689743})
  ProgressBar suggestionsLoading;
  
  public CreateNoteFragment() {}
  
  private void a(Recipient paramRecipient)
  {
    Recipient.RecipientStatus localRecipientStatus = paramRecipient.get();
    DVNTUser localDVNTUser = paramRecipient.values();
    String str = paramRecipient.getValue();
    LinearLayout localLinearLayout = UserUtils.onCreateView(getActivity(), str, paramRecipient.getAttributeValue(), true);
    localLinearLayout.setOnClickListener(new CreateNoteFragment.6(this, str));
    recipientsFlowLayout.addView(localLinearLayout, recipientsFlowLayout.getChildCount() - 1);
    a.getValue().add(paramRecipient);
    recipientEditText.listFiles();
    init(localLinearLayout, localRecipientStatus, localDVNTUser);
    if (!Recipient.RecipientStatus.b.equals(localRecipientStatus)) {
      return;
    }
    DVNTCommonAsyncAPI.whoIs(Arrays.asList(new String[] { localDVNTUser.getUserName() })).call(getActivity(), new CreateNoteFragment.7(this, localLinearLayout, localDVNTUser));
  }
  
  private void b(String paramString)
  {
    if (paramString == null) {
      return;
    }
    paramString = paramString.trim();
    if (!paramString.isEmpty())
    {
      if (a.open(paramString) == null)
      {
        DVNTUser localDVNTUser = new DVNTUser();
        localDVNTUser.setUserName(paramString);
        a(new Recipient(localDVNTUser, Recipient.RecipientStatus.b));
      }
      recipientEditText.listFiles();
    }
  }
  
  private void b(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      suggestionsLoading.setVisibility(0);
      suggestionButton.setVisibility(8);
      return;
    }
    suggestionsLoading.setVisibility(8);
    if (recipientsSuggestions.getChildCount() > 0)
    {
      suggestionButton.setVisibility(0);
      return;
    }
    suggestionButton.setVisibility(8);
  }
  
  private void fadeIn(boolean paramBoolean)
  {
    int i = 0;
    if (recipientsSuggestions == null) {
      return;
    }
    boolean bool;
    Object localObject;
    if (recipientsSuggestions.getVisibility() == 0)
    {
      bool = true;
      if (paramBoolean == bool) {
        return;
      }
      localObject = recipientsSuggestions;
      if (bool) {
        i = 8;
      }
      ((View)localObject).setVisibility(i);
      localObject = suggestionButton.animate().setDuration(100L).setInterpolator(new AccelerateInterpolator());
      if (!bool) {
        break label95;
      }
      ((ViewPropertyAnimator)localObject).rotation(180.0F);
    }
    for (;;)
    {
      ((ViewPropertyAnimator)localObject).start();
      return;
      bool = false;
      break;
      label95:
      ((ViewPropertyAnimator)localObject).rotation(0.0F);
    }
  }
  
  private void init(LinearLayout paramLinearLayout, Recipient.RecipientStatus paramRecipientStatus, DVNTUser paramDVNTUser)
  {
    Recipient localRecipient = a.open(paramDVNTUser.getUserName());
    if ((!DVNTContextUtils.isContextDead(getActivity())) && (localRecipient != null))
    {
      if (paramLinearLayout == null) {
        return;
      }
      SimpleDraweeView localSimpleDraweeView = (SimpleDraweeView)ButterKnife.findById(paramLinearLayout, 2131690019);
      ProgressBar localProgressBar = (ProgressBar)ButterKnife.findById(paramLinearLayout, 2131690018);
      TextView localTextView = (TextView)ButterKnife.findById(paramLinearLayout, 2131690021);
      ImageView localImageView = (ImageView)ButterKnife.findById(paramLinearLayout, 2131690020);
      localRecipient.copy(paramRecipientStatus);
      switch (CreateNoteFragment.12.d[paramRecipientStatus.ordinal()])
      {
      default: 
        break;
      }
      for (;;)
      {
        updateActionBar();
        return;
        localProgressBar.setVisibility(8);
        localSimpleDraweeView.setVisibility(0);
        localImageView.setVisibility(8);
        if (paramDVNTUser.getUserIconURL() != null) {
          ImageUtils.isEmpty(localSimpleDraweeView, Uri.parse(paramDVNTUser.getUserIconURL()));
        }
        paramLinearLayout.setBackgroundColor(getResources().getColor(2131558426));
        localTextView.setTextColor(-1);
        continue;
        localProgressBar.setVisibility(8);
        localSimpleDraweeView.setVisibility(8);
        localImageView.setVisibility(0);
        paramLinearLayout.setBackgroundColor(getResources().getColor(2131558409));
        localTextView.setTextColor(-1);
        continue;
        localProgressBar.setVisibility(0);
        localSimpleDraweeView.setVisibility(8);
        localImageView.setVisibility(8);
        paramLinearLayout.setBackgroundColor(getResources().getColor(2131558457));
        localTextView.setTextColor(getResources().getColor(2131558522));
      }
    }
  }
  
  private void onCreateView(List paramList, boolean paramBoolean)
  {
    if (recipientsSuggestions != null)
    {
      if (DVNTContextUtils.isContextDead(getActivity())) {
        return;
      }
      recipientsSuggestions.removeAllViews();
      if ((paramList == null) || (paramList.isEmpty()))
      {
        if (paramBoolean) {
          recipientEditText.setTextColor(getResources().getColor(2131558547));
        }
        b(false);
        return;
      }
      suggestionButton.setVisibility(0);
      ArrayList localArrayList = new ArrayList();
      paramList = paramList.subList(0, Math.min(paramList.size(), 3)).iterator();
      while (paramList.hasNext())
      {
        Recipient localRecipient = (Recipient)paramList.next();
        String str = localRecipient.getValue();
        if (!localArrayList.contains(str))
        {
          RelativeLayout localRelativeLayout = (RelativeLayout)LayoutInflater.from(getActivity()).inflate(2130968780, recipientsSuggestions, false);
          SimpleDraweeView localSimpleDraweeView = (SimpleDraweeView)ButterKnife.findById(localRelativeLayout, 2131690061);
          TextView localTextView = (TextView)ButterKnife.findById(localRelativeLayout, 2131690062);
          View localView = ButterKnife.findById(localRelativeLayout, 2131690063);
          if (localRecipient.getAttributeValue() != null) {
            ImageUtils.isEmpty(localSimpleDraweeView, Uri.parse(localRecipient.getAttributeValue()));
          }
          if (localRecipient.getSocket()) {
            localView.setVisibility(0);
          }
          localTextView.setText(str);
          localRelativeLayout.setOnClickListener(new CreateNoteFragment.5(this, localRecipient, paramBoolean));
          recipientsSuggestions.addView(localRelativeLayout);
          localArrayList.add(str);
        }
      }
      b(false);
    }
  }
  
  private void onPostExecute()
  {
    ArrayList localArrayList = new ArrayList();
    if (!a.b()) {
      localArrayList.add(getString(2131231101));
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
    localBuilder.setTitle(2131231097).setMessage(TextUtils.join("\n", localArrayList)).setIcon(17301543).setNeutralButton(2131231183, new CreateNoteFragment.11(this));
    localBuilder.create().show();
  }
  
  private void setValue(DVNTNote paramDVNTNote, String paramString)
  {
    String str2 = getString(2131231106);
    String str1 = paramDVNTNote.getSubject();
    if (!paramDVNTNote.getSubject().startsWith(str2)) {
      str1 = str2 + paramDVNTNote.getSubject();
    }
    subjectEditText.setText(str1);
    str1 = getString(2131231104).replace("{sender_name}", paramDVNTNote.getUser().getUserName());
    contentEditText.setText(str1 + paramString);
    a.append(paramDVNTNote.getNoteId());
    contentArea.setVisibility(0);
    replyLoading.setVisibility(8);
    contentEditText.requestFocus();
    contentEditText.setSelection(0);
  }
  
  private void setVideo(DVNTNote paramDVNTNote)
  {
    contentArea.setVisibility(8);
    replyLoading.setVisibility(0);
    DVNTAsyncAPI.getNote(paramDVNTNote.getNoteId()).call(getActivity(), new CreateNoteFragment.4(this, paramDVNTNote));
  }
  
  private void updateActionBar()
  {
    if (c == null)
    {
      Log.d("CreateNote", "Menu not ready");
      return;
    }
    MenuItem localMenuItem = c.findItem(2131690163);
    if (localMenuItem == null)
    {
      Log.d("CreateNote", "Menu item not ready");
      return;
    }
    if (a.d()) {}
    for (int i = 2130837781;; i = 2130837780)
    {
      localMenuItem.setIcon(i);
      return;
    }
  }
  
  public void a()
  {
    if (a != null)
    {
      if (replyLoading.getVisibility() == 0) {
        return;
      }
      if (!a.d())
      {
        onPostExecute();
        return;
      }
      if (a.c())
      {
        Toast.makeText(getActivity(), 2131231102, 0).show();
        return;
      }
      if (a.m()) {}
      ArrayList localArrayList;
      for (Object localObject1 = "contains_deviation_thumb-yes";; localObject1 = "contains_deviation_thumb-no")
      {
        TrackerUtil.get(getActivity(), EventKeys.Category.d, "send_note", (String)localObject1);
        Object localObject2 = a.getValue();
        localObject1 = new Recent(getActivity(), "recent_mentions");
        localArrayList = new ArrayList();
        localObject2 = ((List)localObject2).iterator();
        while (((Iterator)localObject2).hasNext())
        {
          Recipient localRecipient = (Recipient)((Iterator)localObject2).next();
          if (localRecipient.get().equals(Recipient.RecipientStatus.e))
          {
            ((Recent)localObject1).write(localRecipient.getValue() + "&&" + localRecipient.getAttributeValue());
            localArrayList.add(localRecipient.getValue());
          }
        }
      }
      DVNTAsyncAPI.sendNote(localArrayList, a.read(), a.e(), a.f()).call(getActivity(), new CreateNoteFragment.10(this));
      getActivity().finish();
    }
  }
  
  public void b()
  {
    if ((a == null) || (a.a()))
    {
      getActivity().finish();
      return;
    }
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
    localBuilder.setMessage(2131231099).setPositiveButton(2131231098, new CreateNoteFragment.3(this)).setNegativeButton(2131230820, new CreateNoteFragment.2(this));
    localBuilder.create().show();
  }
  
  public void backButtonPressed(BusStation.OnBackPressedEvent paramOnBackPressedEvent)
  {
    paramOnBackPressedEvent.inc();
    b();
  }
  
  public void onClickSuggestionsVisibilityButton()
  {
    if (recipientsSuggestions.getVisibility() != 0) {}
    for (boolean bool = true;; bool = false)
    {
      fadeIn(bool);
      return;
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setHasOptionsMenu(true);
    if (UserUtils.a == null) {
      load();
    }
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    c = paramMenu;
    paramMenuInflater.inflate(2131820553, paramMenu);
    updateActionBar();
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    BusStation.get().register(this);
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    if (paramBundle == null) {}
    Object localObject;
    for (a = new NoteToSend();; a = ((NoteToSend)paramBundle.getSerializable("note_to_send")))
    {
      paramLayoutInflater = paramLayoutInflater.inflate(2130968667, paramViewGroup, false);
      ButterKnife.bind(this, paramLayoutInflater);
      MobileLava.e().setText(new UxTopicEventCreator().get("notes").getString("create").b());
      recipientsContainer.setOnClickListener(CreateNoteFragment..Lambda.1.finish(this));
      recipientEditText.addTextChangedListener(new CreateNoteFragment.1(this));
      paramViewGroup = getArguments();
      contentEditText.setMinHeight(Graphics.add(getActivity(), 150));
      if (paramViewGroup.containsKey("deviation"))
      {
        paramBundle = new RepostView(getActivity());
        localObject = (DVNTDeviation)paramViewGroup.getSerializable("deviation");
        a.a((DVNTDeviation)localObject);
        paramBundle.onCreateView((DVNTDeviation)localObject);
        contentArea.addView(paramBundle);
        contentEditText.setMinHeight(Graphics.add(getActivity(), 50));
        paramBundle.b(CreateNoteFragment..Lambda.2.b(this, paramBundle));
      }
      if (!paramViewGroup.containsKey("username")) {
        break label290;
      }
      paramBundle = ((List)paramViewGroup.getSerializable("username")).iterator();
      while (paramBundle.hasNext()) {
        a(new Recipient((DVNTUser)paramBundle.next(), Recipient.RecipientStatus.e));
      }
    }
    subjectEditText.requestFocus();
    label290:
    if (paramViewGroup.containsKey("subject")) {
      subjectEditText.setText(paramViewGroup.getString("subject"));
    }
    if (paramViewGroup.containsKey("note_data")) {
      setVideo((DVNTNote)paramViewGroup.getSerializable("note_data"));
    }
    paramViewGroup = new ArrayList();
    paramBundle = new Recent(getActivity(), "recent_mentions").read().iterator();
    while (paramBundle.hasNext())
    {
      localObject = ((String)paramBundle.next()).split("&&");
      if (localObject.length == 2)
      {
        DVNTUser localDVNTUser = new DVNTUser();
        localDVNTUser.setUserName(localObject[0]);
        localDVNTUser.setUserIconURL(localObject[1]);
        paramViewGroup.add(new Recipient(localDVNTUser, Recipient.RecipientStatus.e));
      }
    }
    if (paramViewGroup.isEmpty()) {
      suggestionButton.setVisibility(8);
    }
    onCreateView(paramViewGroup, false);
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    if (currentTask != null)
    {
      currentTask.cancel(true);
      currentTask = null;
    }
    BusStation.get().unregister(this);
    ButterKnife.unbind(this);
  }
  
  public void onLayoutRecipientsFocus(boolean paramBoolean)
  {
    if (recipientEditText == null) {
      return;
    }
    fadeIn(paramBoolean);
    if (paramBoolean)
    {
      Keyboard.showKeyboard(getActivity(), recipientEditText);
      return;
    }
    b(recipientEditText.getText().toString());
  }
  
  public void onNoteContentChanged(CharSequence paramCharSequence)
  {
    a.a(paramCharSequence.toString());
    updateActionBar();
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    case 16908332: 
      b();
      return true;
    }
    a();
    return true;
  }
  
  public void onOptionsMenuClosed(Menu paramMenu)
  {
    c = null;
  }
  
  public void onRecipientsEditTextChanged(CharSequence paramCharSequence)
  {
    recipientEditText.setTextColor(getResources().getColor(2131558419));
    paramCharSequence = paramCharSequence.toString().trim();
    if (currentTask != null) {
      currentTask.cancel(true);
    }
    if (paramCharSequence.isEmpty())
    {
      b(false);
      return;
    }
    b(true);
    currentTask = new CreateNoteFragment.9(this, new CreateNoteFragment.8(this));
    currentTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[] { paramCharSequence });
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("note_to_send", a);
  }
  
  public void onSubjectChanged(CharSequence paramCharSequence)
  {
    a.trim(paramCharSequence.toString());
    updateActionBar();
  }
}
