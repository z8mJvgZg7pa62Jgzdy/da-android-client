package com.deviantart.android.damobile.util;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import com.deviantart.android.android.utils.DVNTContextUtils;

public class ScreenFlowManager
{
  private ScreenFlowManager() {}
  
  public static void a(Activity paramActivity, Fragment paramFragment, String paramString)
  {
    create(paramActivity, paramFragment, paramString, ScreenFlowManager.FragmentAction.c, true);
  }
  
  public static void add(Activity paramActivity, Fragment paramFragment, String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    onPostExecute(paramActivity, paramFragment, paramString, ScreenFlowManager.FragmentAction.c, paramBoolean1, paramBoolean2);
  }
  
  private static void create(Activity paramActivity, Fragment paramFragment, String paramString, ScreenFlowManager.FragmentAction paramFragmentAction, boolean paramBoolean)
  {
    onPostExecute(paramActivity, paramFragment, paramString, paramFragmentAction, paramBoolean, false);
  }
  
  public static void get(Activity paramActivity, Fragment paramFragment, String paramString, boolean paramBoolean)
  {
    create(paramActivity, paramFragment, paramString, ScreenFlowManager.FragmentAction.c, paramBoolean);
  }
  
  public static void goHome(Activity paramActivity)
  {
    if (DVNTContextUtils.isContextDead(paramActivity)) {
      return;
    }
    paramActivity = paramActivity.getFragmentManager();
    if (paramActivity != null)
    {
      paramActivity.executePendingTransactions();
      while (paramActivity.getBackStackEntryCount() != 0) {
        paramActivity.popBackStackImmediate();
      }
    }
  }
  
  public static void hideKeyboard(Activity paramActivity)
  {
    if (DVNTContextUtils.isContextDead(paramActivity)) {
      return;
    }
    paramActivity = paramActivity.getFragmentManager();
    if (paramActivity != null)
    {
      paramActivity.executePendingTransactions();
      paramActivity.popBackStack();
    }
  }
  
  private static void onPostExecute(Activity paramActivity, Fragment paramFragment, String paramString, ScreenFlowManager.FragmentAction paramFragmentAction, boolean paramBoolean1, boolean paramBoolean2)
  {
    if ((!DVNTContextUtils.isContextDead(paramActivity)) && (paramFragment != null) && (!paramFragment.isAdded()))
    {
      if (paramFragment.isResumed()) {
        return;
      }
      FragmentManager localFragmentManager = paramActivity.getFragmentManager();
      if (localFragmentManager != null)
      {
        localFragmentManager.executePendingTransactions();
        Fragment localFragment = localFragmentManager.findFragmentByTag(paramString);
        EventPayload.add();
        paramActivity = paramFragment;
        if (paramString != null)
        {
          paramActivity = paramFragment;
          if (!paramString.isEmpty())
          {
            paramActivity = paramFragment;
            if (localFragment != null) {
              paramActivity = localFragment;
            }
          }
        }
        if ((!paramActivity.isAdded()) && (!paramActivity.isResumed()))
        {
          paramFragment = localFragmentManager.beginTransaction();
          if (paramBoolean1) {
            paramFragment.addToBackStack(paramString);
          }
          if (paramBoolean2) {
            paramFragment.setCustomAnimations(2131034139, 2131034143, 2131034141, 2131034145);
          }
          switch (ScreenFlowManager.1.flags[paramFragmentAction.ordinal()])
          {
          default: 
            break;
          }
          for (;;)
          {
            paramFragment.commitAllowingStateLoss();
            return;
            paramFragment.add(2131689591, paramActivity, paramString);
            continue;
            paramFragment.replace(2131689591, paramActivity, paramString);
          }
        }
      }
    }
  }
}
