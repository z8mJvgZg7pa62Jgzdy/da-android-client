package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTStashUpdateStackRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  private String newDescription;
  private String newTitle;
  private Long stackId;
  
  public DVNTStashUpdateStackRequestV1(Long paramLong, String paramString1, String paramString2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    stackId = paramLong;
    newTitle = paramString1;
    newDescription = paramString2;
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).stashUpdateStack(paramString, stackId, newTitle, newDescription);
  }
}
