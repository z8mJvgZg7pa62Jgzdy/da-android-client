package com.deviantart.android.damobile.adapter;

import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;
import com.deviantart.android.damobile.view.viewpageindicator.DAPagerAdapter;
import java.util.HashMap;

public abstract class StatefulPagerAdapter
  extends DAPagerAdapter
{
  private HashMap<String, Parcelable> a = null;
  
  public StatefulPagerAdapter() {}
  
  public static HashMap saveData(ViewPager paramViewPager)
  {
    HashMap localHashMap = new HashMap();
    int j = paramViewPager.getChildCount();
    int i = 0;
    while (i < j)
    {
      StatefulPagerAdapter.PageView localPageView = (StatefulPagerAdapter.PageView)paramViewPager.getChildAt(i);
      localHashMap.put(localPageView.getPageTag(), localPageView.getPageState());
      i += 1;
    }
    return localHashMap;
  }
  
  protected void b(StatefulPagerAdapter.PageView paramPageView, String paramString)
  {
    if ((a != null) && (a.containsKey(paramString)))
    {
      paramPageView.onSaveInstanceState((Parcelable)a.get(paramString));
      a.remove(paramString);
      if (a.isEmpty()) {
        a = null;
      }
    }
  }
  
  public void setOnValueChangedListener(HashMap paramHashMap)
  {
    a = paramHashMap;
  }
}
