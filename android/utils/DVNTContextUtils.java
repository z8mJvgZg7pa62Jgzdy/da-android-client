package com.deviantart.android.android.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;

public class DVNTContextUtils
{
  public DVNTContextUtils() {}
  
  public static boolean isContextDead(Context paramContext)
  {
    if (paramContext == null) {
      return true;
    }
    if (!(paramContext instanceof Activity)) {
      return false;
    }
    paramContext = (Activity)paramContext;
    return (paramContext.isFinishing()) || ((Build.VERSION.SDK_INT >= 17) && (paramContext.isDestroyed()));
  }
}
