package com.deviantart.android.android.package_14.model;

import java.io.Serializable;

public class DVNTStatus
  implements Serializable
{
  public String error;
  public String errorDescription;
  public String status;
  
  public DVNTStatus() {}
  
  public String getError()
  {
    return error;
  }
  
  public String getErrorDescription()
  {
    return errorDescription;
  }
  
  public String getStatus()
  {
    return status;
  }
  
  public void setError(String paramString)
  {
    error = paramString;
  }
  
  public void setErrorDescription(String paramString)
  {
    errorDescription = paramString;
  }
  
  public void setStatus(String paramString)
  {
    status = paramString;
  }
}
