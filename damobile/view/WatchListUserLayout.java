package com.deviantart.android.damobile.view;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTFriend;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.package_14.model.DVNTUserProfile;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.MemberType;
import com.deviantart.android.damobile.util.UserDisplay;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.UserWatchStateChangeListener;
import com.deviantart.android.damobile.util.ViewHelper;
import com.facebook.drawee.view.SimpleDraweeView;

public class WatchListUserLayout
  extends RelativeLayout
{
  private boolean b = false;
  UserWatchStateChangeListener d;
  @Bind({2131690143})
  SimpleDraweeView groupAvatar;
  private MemberType j;
  @Bind({2131690141})
  RelativeLayout mainContainer;
  @Bind({2131689841})
  TextView realNameView;
  @Bind({2131690142})
  SimpleDraweeView userAvatar;
  @Bind({2131689840})
  TextView usernameView;
  @Bind({2131690098})
  SmallWatchButton watchButton;
  
  public WatchListUserLayout(Context paramContext, UserWatchStateChangeListener paramUserWatchStateChangeListener)
  {
    super(paramContext);
    LayoutInflater.from(paramContext).inflate(2130968804, this, true);
    d = paramUserWatchStateChangeListener;
    ButterKnife.bind(this);
  }
  
  private void a(DVNTUser paramDVNTUser)
  {
    j = MemberType.toString(paramDVNTUser.getType());
    if (j == null)
    {
      mainContainer.setVisibility(8);
      return;
    }
    mainContainer.setVisibility(0);
    Uri localUri = Uri.parse(paramDVNTUser.getUserIconURL());
    if (j.b())
    {
      ImageUtils.isEmpty(groupAvatar, localUri);
      groupAvatar.setVisibility(0);
      userAvatar.setVisibility(8);
      usernameView.setText(UserDisplay.add(getContext(), paramDVNTUser));
      realNameView.setVisibility(0);
      if (!j.b()) {
        break label163;
      }
      realNameView.setVisibility(8);
    }
    for (;;)
    {
      setOnClickListener(new WatchListUserLayout.1(this, paramDVNTUser));
      return;
      ImageUtils.isEmpty(userAvatar, localUri);
      userAvatar.setVisibility(0);
      groupAvatar.setVisibility(8);
      break;
      label163:
      ViewHelper.setValue(realNameView, paramDVNTUser.getProfile().getRealName(), 8, Integer.valueOf(0));
    }
  }
  
  public void b(DVNTFriend paramDVNTFriend)
  {
    if (DVNTContextUtils.isContextDead(getContext())) {
      return;
    }
    a(paramDVNTFriend.getUser());
    String str = paramDVNTFriend.getUser().getUserName();
    MemberType localMemberType = MemberType.toString(paramDVNTFriend.getUser().getType());
    if ((UserUtils.a == null) || (str.equals(UserUtils.a))) {
      watchButton.setVisibility(4);
    }
    for (;;)
    {
      watchButton.a(WatchListUserLayout..Lambda.1.d(this));
      return;
      watchButton.setVisibility(0);
      watchButton.a(str, localMemberType, Boolean.valueOf(paramDVNTFriend.getYoureWatching()));
    }
  }
  
  public void setEventSource(String paramString)
  {
    if (watchButton == null) {
      return;
    }
    watchButton.setEventSource(paramString);
  }
}
