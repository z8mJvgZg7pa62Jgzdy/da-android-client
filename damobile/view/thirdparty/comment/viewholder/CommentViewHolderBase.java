package com.deviantart.android.damobile.view.thirdparty.comment.viewholder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.view.LoadMoreButton;
import com.deviantart.android.damobile.view.LoadMoreSiblingsButton;

public class CommentViewHolderBase
  extends RecyclerView.ViewHolder
{
  private String actionDescription;
  @Bind({2131689626})
  public FrameLayout commentCollapseTag;
  @Bind({2131689627})
  public LoadMoreButton commentLoadMoreButton;
  @Bind({2131689625})
  public TextView commentRepliesTag;
  @Bind({2131689623})
  public LinearLayout commentRightCol;
  @Bind({2131689628})
  public LoadMoreSiblingsButton loadMoreSiblingsBottom;
  @Bind({2131689621})
  public LoadMoreSiblingsButton loadMoreSiblingsTop;
  @Bind({2131689624})
  public ProgressBar loader;
  
  public CommentViewHolderBase(View paramView)
  {
    super(paramView);
    ButterKnife.bind(this, paramView);
  }
  
  public String buildKey()
  {
    return actionDescription;
  }
  
  public void from(String paramString)
  {
    actionDescription = paramString;
  }
}
