package com.deviantart.android.damobile.util;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation.DVNTDailyDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.datoolkit.logger.DVNTLog;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;

public class DAFormatUtils
{
  public DAFormatUtils() {}
  
  public static int a(CharSequence paramCharSequence)
  {
    try
    {
      paramCharSequence = paramCharSequence.toString().getBytes("UTF-8");
      return paramCharSequence.length;
    }
    catch (UnsupportedEncodingException paramCharSequence) {}
    return -1;
  }
  
  public static Spannable a(Context paramContext, int paramInt, String paramString)
  {
    SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(paramContext.getString(paramInt));
    paramInt = localSpannableStringBuilder.length();
    localSpannableStringBuilder.setSpan(new CalligraphyTypefaceSpan(DAFont.g.get(paramContext)), 0, paramInt, 33);
    localSpannableStringBuilder.setSpan(new RelativeSizeSpan(0.75F), 0, paramInt, 33);
    localSpannableStringBuilder.append(" ").append(paramString);
    localSpannableStringBuilder.setSpan(new CalligraphyTypefaceSpan(DAFont.l.get(paramContext)), paramInt, paramString.length(), 33);
    return localSpannableStringBuilder;
  }
  
  private static String format(double paramDouble)
  {
    if (paramDouble == paramDouble) {
      return String.format("%d", new Object[] { Long.valueOf(paramDouble) });
    }
    return String.format("%s", new Object[] { Double.valueOf(paramDouble) });
  }
  
  public static String format(Context paramContext, String paramString)
  {
    return format(paramContext, paramString, true);
  }
  
  private static String format(Context paramContext, String paramString, boolean paramBoolean)
  {
    if (paramString == null) {
      return null;
    }
    Object localObject = parse(paramString);
    if (localObject != null)
    {
      int i = 20;
      if (paramBoolean) {
        i = 65556;
      }
      localObject = DateUtils.getRelativeTimeSpanString(((Date)localObject).getTime(), new Date().getTime(), 60000L, i);
      paramString = (String)localObject;
      if (localObject.equals("0 minutes ago")) {
        paramString = paramContext.getString(2131231042);
      }
      localObject = paramString;
      if (((CharSequence)paramString).toString().startsWith("in ")) {
        localObject = paramContext.getString(2131231042);
      }
      paramContext = (Context)localObject;
      if (paramBoolean) {
        paramContext = ((CharSequence)localObject).toString().replace("minute", "min");
      }
      paramString = ((CharSequence)paramContext).toString();
    }
    return paramString;
  }
  
  public static String format(Integer paramInteger)
  {
    if (paramInteger == null) {
      return null;
    }
    if (paramInteger.intValue() < 10000) {
      return String.format("%,d", new Object[] { paramInteger });
    }
    int i = (int)(Math.log(paramInteger.intValue()) / Math.log(1000.0D));
    double d = paramInteger.intValue() / Math.pow(1000.0D, i);
    return format(BigDecimal.valueOf(d).round(new MathContext(3)).doubleValue()) + "KMB".charAt(i - 1);
  }
  
  public static String format(String paramString)
  {
    paramString = paramString.split("[_\\s]+");
    int i = 0;
    while (i < paramString.length)
    {
      paramString[i] = (paramString[i].substring(0, 1).toUpperCase() + paramString[i].substring(1));
      i += 1;
    }
    return TextUtils.join(" ", paramString);
  }
  
  public static String get(Context paramContext, String paramString)
  {
    return format(paramContext, paramString, false);
  }
  
  public static Date get(Stream paramStream)
  {
    paramStream = (DVNTDeviation)paramStream.get(0);
    if (paramStream == null)
    {
      DVNTLog.append("[DD item] Expecting non-empty deviation stream for DD date", new Object[0]);
      return new Date(0L);
    }
    return set(paramStream);
  }
  
  public static String getDate(Context paramContext, String paramString)
  {
    paramString = parse(paramString);
    if (paramString == null) {
      return null;
    }
    long l = (new Date().getTime() - paramString.getTime()) / 86400000L;
    if (l < 1L) {
      return paramContext.getString(2131231036);
    }
    if (l < 2L) {
      return "1 day";
    }
    if (l < 30L) {
      return (int)l + " Days";
    }
    int i = (int)l / 30;
    if (i == 1) {
      return "1 Month";
    }
    if (i < 12) {
      return i + " Months";
    }
    i = (int)l / 365;
    if (i == 0) {
      return "11 Months";
    }
    if (i == 1) {
      return "1 Year";
    }
    return i + " Years";
  }
  
  public static String getTitle(String paramString)
  {
    int i = 1;
    String[] arrayOfString = paramString.split("/");
    paramString = "/";
    if (arrayOfString.length > 1) {
      while (i < arrayOfString.length)
      {
        paramString = paramString + " " + arrayOfString[i] + " /";
        i += 1;
      }
    }
    return "/";
    return paramString;
  }
  
  public static Date parse(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    Object localObject = DAFormatUtils.DateFormats.c;
    try
    {
      localObject = ((SimpleDateFormat)localObject).parse(paramString);
      return localObject;
    }
    catch (ParseException localParseException)
    {
      Log.e(DAFormatUtils.class.getSimpleName(), "Failure parsing date string: " + paramString);
    }
    return null;
  }
  
  public static Date set(DVNTDeviation paramDVNTDeviation)
  {
    if (paramDVNTDeviation.getDailyDeviation() == null)
    {
      DVNTLog.append("[DD item] Expecting daily deviation object", new Object[0]);
      return new Date(0L);
    }
    SimpleDateFormat localSimpleDateFormat = DAFormatUtils.DateFormats.c;
    try
    {
      paramDVNTDeviation = localSimpleDateFormat.parse(paramDVNTDeviation.getDailyDeviation().getTime());
      return paramDVNTDeviation;
    }
    catch (ParseException paramDVNTDeviation)
    {
      paramDVNTDeviation.printStackTrace();
      DVNTLog.append("[DD item] Unexpected date format DD object", new Object[0]);
    }
    return new Date(0L);
  }
}
