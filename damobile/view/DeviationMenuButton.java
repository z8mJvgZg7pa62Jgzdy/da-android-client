package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.content.res.Resources;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;

public abstract class DeviationMenuButton
  extends MenuButton<com.deviantart.android.sdk.api.model.DVNTDeviation>
{
  protected com.deviantart.android.android.package_14.model.DVNTDeviation i;
  protected String t;
  
  public DeviationMenuButton(Activity paramActivity, com.deviantart.android.android.package_14.model.DVNTDeviation paramDVNTDeviation)
  {
    super(paramActivity);
    int j = paramActivity.getResources().getDimensionPixelSize(2131361954);
    setLayoutParams(new ViewGroup.LayoutParams(j, j));
    i = paramDVNTDeviation;
    t = paramDVNTDeviation.getId();
  }
  
  public DeviationMenuButton(Activity paramActivity, String paramString)
  {
    super(paramActivity);
    int j = paramActivity.getResources().getDimensionPixelSize(2131361954);
    setLayoutParams(new ViewGroup.LayoutParams(j, j));
    t = paramString;
  }
}
