package com.deviantart.android.damobile.view.notifications;

import android.content.Context;

public class NotificationsListRecyclerView
  extends com.deviantart.android.damobile.view.mc.MCListRecyclerView<NotificationsListAdapter>
{
  public NotificationsListRecyclerView(Context paramContext)
  {
    super(paramContext);
  }
}
