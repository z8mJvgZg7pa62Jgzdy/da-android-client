package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.CoordinatorLayout.LayoutParams;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTComment;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.activity.CreateNoteActivity.IntentBuilder;
import com.deviantart.android.damobile.activity.SubmitActivity.IntentBuilder;
import com.deviantart.android.damobile.adapter.DeviationFullViewPagerAdapter;
import com.deviantart.android.damobile.adapter.DeviationPanelTabsPagerAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.Stream.Notifiable;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APIDeviationEmbeddedLoader;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.AppBarStateChangeListener;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnChangedCommentContext;
import com.deviantart.android.damobile.util.BusStation.OnCommentPostedEvent;
import com.deviantart.android.damobile.util.BusStation.OnLoadedFocusedComment;
import com.deviantart.android.damobile.util.BusStation.OnUserSettingChangeEvent;
import com.deviantart.android.damobile.util.CommentType;
import com.deviantart.android.damobile.util.CommentUtils.OnClickOpenCommentListener;
import com.deviantart.android.damobile.util.DAAnimationUtils;
import com.deviantart.android.damobile.util.DeviationAdController;
import com.deviantart.android.damobile.util.DeviationCompositeModel;
import com.deviantart.android.damobile.util.DeviationCompositeModel.FaveListener;
import com.deviantart.android.damobile.util.DeviationDownloadUtils;
import com.deviantart.android.damobile.util.DeviationFullViewBehavior;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.FloatingButtonDescription;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.util.PermissionUtil;
import com.deviantart.android.damobile.util.PermissionUtil.CheckResult;
import com.deviantart.android.damobile.util.ProfileCardBehavior;
import com.deviantart.android.damobile.util.ShareUtils;
import com.deviantart.android.damobile.util.UserSettingUpdateType;
import com.deviantart.android.damobile.util.deeplink.DeepLinkUtils;
import com.deviantart.android.damobile.util.deviation.DeviationPanelTab;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.util.tracking.TrackerUtil.EventLabelBuilder;
import com.deviantart.android.damobile.util.tracking.pacaya.MobileLava;
import com.deviantart.android.damobile.util.tracking.pacaya.UxTopicEventCreator;
import com.deviantart.android.damobile.view.CommentsLayout;
import com.deviantart.android.damobile.view.CommentsLayout.CommentContextState;
import com.deviantart.android.damobile.view.DeviationFullViewCollapsingLayout;
import com.deviantart.android.damobile.view.DeviationFullViewPager;
import com.deviantart.android.damobile.view.DeviationFullViewPager.DeviationFullViewPagerListener;
import com.deviantart.android.damobile.view.deviationtab.FavouritesLoggedInLayout;
import com.deviantart.android.damobile.view.viewpageindicator.CustomizableTabPageIndicator;
import com.deviantart.android.damobile.view.viewpageindicator.DATabIndicator;
import com.deviantart.android.damobile.view.viewpageindicator.DATabIndicator.OnTabClickListener;
import com.deviantart.android.damobile.view.viewpageindicator.DATabIndicator.OnTabIconClickListener;
import com.deviantart.android.damobile.view.viewpageindicator.DeviationTabIndicator;
import com.deviantart.pacaya.Pacaya;
import com.google.android.com.appindexing.Action;
import com.google.android.com.appindexing.AppIndex;
import com.google.android.com.appindexing.AppIndexApi;
import com.squareup.otto.Bus;
import java.util.Arrays;
import java.util.List;

public class DeviationFullViewFragment
  extends HomeFullViewFragment
  implements DeviationCompositeModel.FaveListener, DeviationFullViewPager.DeviationFullViewPagerListener, DATabIndicator.OnTabClickListener, DATabIndicator.OnTabIconClickListener
{
  private Action D;
  private boolean a = false;
  @Bind({2131689922})
  TextView appBarDeviationAuthor;
  @Bind({2131689660})
  TextView appBarDeviationTitle;
  @Bind({2131689750})
  AppBarLayout appBarLayout;
  private boolean b;
  private int c = -1;
  @Bind({2131689751})
  DeviationFullViewCollapsingLayout collapsingLayout;
  @Bind({2131689665})
  DeviationTabIndicator deviationPanelIndicator;
  @Bind({2131689666})
  ViewPager deviationPanelPager;
  @Bind({2131689753})
  DeviationFullViewPager deviationViewPager;
  private boolean e = false;
  private int f = 10;
  private DeviationPanelTabsPagerAdapter g;
  private DeviationFullViewFragment.BarState h;
  private int index = -1;
  private int j = -1;
  private boolean l = false;
  @Bind({2131689748})
  View loading;
  @Bind({2131689749})
  CoordinatorLayout mainContainer;
  private DeviationCompositeModel r;
  private boolean s = false;
  private boolean v = false;
  
  public DeviationFullViewFragment() {}
  
  private int a(int paramInt1, int paramInt2)
  {
    paramInt1 = paramInt2 - paramInt1 + 5;
    if (paramInt1 < f) {
      return f;
    }
    if (paramInt1 >= 120) {
      return Math.min(paramInt1 / 2, 120);
    }
    return paramInt1;
  }
  
  private void a(Stream paramStream, Bundle paramBundle, boolean paramBoolean)
  {
    deviationViewPager.setAdapter(new DeviationFullViewPagerAdapter(paramStream));
    int i;
    if ((paramBundle != null) && (paramBundle.containsKey("stream_index"))) {
      i = paramBundle.getInt("stream_index");
    }
    while (paramStream.size() <= i)
    {
      mainContainer.setVisibility(8);
      loading.setVisibility(0);
      f = paramStream.length().a();
      paramStream.length().add(a(paramStream.size() - 1, i));
      paramStream.close(getActivity(), new DeviationFullViewFragment.3(this, i, paramBundle, paramBoolean), i);
      return;
      if (c != -1)
      {
        i = c;
        c = -1;
      }
      else
      {
        i = getArguments().getInt("stream_index");
      }
    }
    paramStream = deviationViewPager.getAdapter().get(i);
    onCreateView(paramBundle);
    a(i, paramStream);
    deviationViewPager.setCurrentItem(i);
    if (!paramBoolean) {
      deviationViewPager.a(i, paramStream);
    }
  }
  
  private void a(boolean paramBoolean)
  {
    if (appBarLayout != null)
    {
      if (mainContainer == null) {
        return;
      }
      Object localObject = (CoordinatorLayout.LayoutParams)appBarLayout.getLayoutParams();
      if (j == -1) {
        j = (Graphics.width(getActivity()) * 4);
      }
      try
      {
        localObject = ((CoordinatorLayout.LayoutParams)localObject).getBehavior();
        localObject = (DeviationFullViewBehavior)localObject;
        if (paramBoolean)
        {
          localCoordinatorLayout = mainContainer;
          localAppBarLayout2 = appBarLayout;
          f1 = j;
          ((ProfileCardBehavior)localObject).scroll(localCoordinatorLayout, localAppBarLayout2, f1);
          return;
        }
      }
      catch (Exception localException)
      {
        CoordinatorLayout localCoordinatorLayout;
        AppBarLayout localAppBarLayout2;
        float f1;
        AppBarLayout localAppBarLayout1 = appBarLayout;
        if (!paramBoolean) {}
        for (paramBoolean = true;; paramBoolean = false)
        {
          localAppBarLayout1.setExpanded(paramBoolean);
          return;
          localCoordinatorLayout = mainContainer;
          localAppBarLayout2 = appBarLayout;
          f1 = -j;
          localAppBarLayout1.scroll(localCoordinatorLayout, localAppBarLayout2, f1);
          return;
        }
      }
    }
  }
  
  private void c(String paramString1, String paramString2, Bundle paramBundle, boolean paramBoolean)
  {
    loading.setVisibility(0);
    mainContainer.setVisibility(8);
    paramString1 = StreamCacher.a(new APIDeviationEmbeddedLoader(paramString1, paramString2));
    paramString2 = new DeviationFullViewFragment.1(this, paramString2, paramString1, paramBundle, paramBoolean);
    if (paramString1.equals())
    {
      if (paramString1.size() == 0)
      {
        paramString2.b();
        return;
      }
      paramString2.c();
      return;
    }
    paramString1.add(getActivity(), paramString2, true);
  }
  
  private void clear()
  {
    if (deviationPanelPager == null) {
      return;
    }
    index = deviationPanelPager.getCurrentItem();
    DeviationPanelTab localDeviationPanelTab = DeviationPanelTab.get(deviationPanelPager.getCurrentItem());
    String str = new TrackerUtil.EventLabelBuilder().get("tab", localDeviationPanelTab.getValue()).getValue();
    TrackerUtil.get(getActivity(), EventKeys.Category.a, "tab_viewed", str);
    MobileLava.e().setText(new UxTopicEventCreator().get("deviation").getString(localDeviationPanelTab.getValue()).b());
  }
  
  private void d(DeviationFullViewFragment.BarState paramBarState)
  {
    if (paramBarState != h)
    {
      if (deviationPanelPager == null) {
        return;
      }
      h = paramBarState;
      switch (DeviationFullViewFragment.5.o[paramBarState.ordinal()])
      {
      default: 
        return;
      case 1: 
        updateSpinner();
        deviationPanelPager.invalidate();
        deviationPanelPager.requestLayout();
        return;
      }
      onActivityResult();
    }
  }
  
  private boolean equals()
  {
    return DeviationFullViewFragment.BarState.b.equals(h);
  }
  
  private void init(String paramString, Bundle paramBundle, boolean paramBoolean)
  {
    loading.setVisibility(0);
    mainContainer.setVisibility(8);
    DVNTCommonAsyncAPI.deviationInfo(paramString).call(getActivity(), new DeviationFullViewFragment.2(this, paramBundle, paramBoolean));
  }
  
  private void onActivityResult()
  {
    BusStation.get().post(new BusStation.OnChangedCommentContext(CommentsLayout.CommentContextState.c));
    if (b) {
      return;
    }
    if (a)
    {
      a = false;
      return;
    }
    if (deviationPanelPager.getCurrentItem() != 0)
    {
      e = true;
      deviationPanelPager.setCurrentItem(0);
    }
    showImage();
  }
  
  private void onCreateView()
  {
    if (!b)
    {
      if (!h.d()) {
        return;
      }
      h = DeviationFullViewFragment.BarState.h;
      deviationPanelIndicator.setIsAnimated(true);
      DAAnimationUtils.show(toolbar, 2131034149, true, DeviationFullViewFragment..Lambda.3.findById(this));
      deviationPanelIndicator.animate().translationYBy(-getResources().getDimensionPixelSize(2131361922)).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(300L).start();
      deviationPanelIndicator.notifyDataSetChanged();
      g.notifyDataSetChanged();
    }
  }
  
  private void onCreateView(Bundle paramBundle)
  {
    toolbar.setVisibility(0);
    deviationPanelIndicator.setIsAnimated(false);
    DeviationTabIndicator localDeviationTabIndicator1 = deviationPanelIndicator;
    DeviationTabIndicator localDeviationTabIndicator2 = deviationPanelIndicator;
    localDeviationTabIndicator2.getClass();
    localDeviationTabIndicator1.setOnTabReselectedListener(DeviationFullViewFragment..Lambda.1.setLyrics(localDeviationTabIndicator2));
    deviationPanelIndicator.setOnTabClickListener(this);
    deviationPanelIndicator.setOnTabIconClickListener(this);
    deviationPanelIndicator.setOnPageChangeListener(new DeviationFullViewFragment.4(this));
    deviationViewPager.setGestureDetector(new GestureDetector(getActivity(), new DeviationFullViewFragment.SimpleTapListener(this, null)));
    if (paramBundle == null) {}
    for (paramBundle = (DeviationPanelTab)getArguments().getSerializable("initial_tab");; paramBundle = (DeviationPanelTab)paramBundle.getSerializable("initial_tab"))
    {
      if (index != -1) {
        paramBundle = DeviationPanelTab.get(index);
      }
      if (paramBundle != null)
      {
        index = Arrays.asList(DeviationPanelTab.values()).indexOf(paramBundle);
        deviationPanelPager.setCurrentItem(index);
        a = true;
        appBarLayout.post(DeviationFullViewFragment..Lambda.2.c(this));
      }
      if ((getResourcesgetConfigurationorientation == 2) && (paramBundle == null) && (!equals())) {
        show();
      }
      if ((paramBundle != null) || (equals()) || (index != -1)) {
        break;
      }
      showImage();
      return;
    }
  }
  
  private void show()
  {
    if (!b)
    {
      if (!h.d()) {
        return;
      }
      h = DeviationFullViewFragment.BarState.h;
      deviationPanelIndicator.setIsAnimated(true);
      DAAnimationUtils.show(toolbar, 2131034150, false, DeviationFullViewFragment..Lambda.4.retrieve(this));
      deviationPanelIndicator.animate().translationYBy(getResources().getDimensionPixelSize(2131361922)).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(300L).start();
    }
  }
  
  private void showImage()
  {
    deviationPanelIndicator.setCurrentItem(-1);
    index = -1;
  }
  
  private void update()
  {
    if (deviationPanelPager == null) {
      return;
    }
    g.setColor(r);
    g.notifyDataSetChanged();
    if (!DeviationFullViewFragment.BarState.i.equals(h)) {
      deviationPanelIndicator.notifyDataSetChanged();
    }
  }
  
  private void updateSpinner()
  {
    if (!equals()) {
      return;
    }
    clear();
    if (deviationPanelPager != null) {
      deviationPanelIndicator.setCurrentItem(Math.max(0, deviationPanelPager.getCurrentItem()));
    }
  }
  
  public void a(int paramInt, DVNTDeviation paramDVNTDeviation)
  {
    if ((c != paramInt) || (v))
    {
      if (DVNTContextUtils.isContextDead(getActivity())) {
        return;
      }
      v = false;
      if (DeviationFullViewFragment.BarState.i.equals(h)) {
        onCreateView();
      }
      c = paramInt;
      r = new DeviationCompositeModel(paramDVNTDeviation);
      r.b(this);
      if (!b)
      {
        String str = paramDVNTDeviation.getTitle();
        Object localObject = paramDVNTDeviation.getAuthor();
        if (localObject != null)
        {
          localObject = ((DVNTUser)localObject).getUserName();
          appBarDeviationTitle.setText(str);
          appBarDeviationAuthor.setText((CharSequence)localObject);
          getActivity().invalidateOptionsMenu();
          if (D != null)
          {
            Log.d("AppIndex", "end-pager");
            AppIndex.AppIndexApi.get(getApplicationContext(), D);
          }
          D = Action.newAction("http://schema.org/ViewAction", getString(2131230868, new Object[] { str, localObject }), Uri.parse(paramDVNTDeviation.getUrl()), Uri.parse(getString(2131230863, new Object[] { DeepLinkUtils.getConfigFile(getActivity()), paramDVNTDeviation.getId() })));
          Log.d("AppIndex ", "start for " + str);
          AppIndex.AppIndexApi.start(getApplicationContext(), D);
          update();
        }
      }
    }
  }
  
  public void b(int paramInt, boolean paramBoolean)
  {
    if (e)
    {
      e = false;
      return;
    }
    if (h.a())
    {
      if (!equals())
      {
        a(true);
        return;
      }
      if (paramBoolean)
      {
        a(false);
        return;
      }
      clear();
    }
  }
  
  public void b(BusStation.OnCommentPostedEvent paramOnCommentPostedEvent)
  {
    if ((isAdded()) && (deviationPanelIndicator != null) && (deviationPanelPager != null))
    {
      if (r == null) {
        return;
      }
      super.b(paramOnCommentPostedEvent);
      paramOnCommentPostedEvent = (DVNTComment)paramOnCommentPostedEvent.getArguments().getSerializable("comment_posted");
      CommentsLayout localCommentsLayout = (CommentsLayout)deviationPanelPager.findViewWithTag("comments_tab");
      if (localCommentsLayout != null) {
        localCommentsLayout.b(paramOnCommentPostedEvent);
      }
      r.a();
      deviationPanelIndicator.onDraw();
    }
  }
  
  public boolean b()
  {
    if (!isResumed()) {
      return false;
    }
    if (equals())
    {
      a(false);
      return true;
    }
    return false;
  }
  
  public void c(int paramInt, boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      if (paramInt != DeviationPanelTab.b.ordinal()) {
        return;
      }
      FavouritesLoggedInLayout localFavouritesLoggedInLayout = (FavouritesLoggedInLayout)deviationPanelPager.findViewWithTag("logged_in_fave_layout");
      if (localFavouritesLoggedInLayout != null)
      {
        localFavouritesLoggedInLayout.doInBackground();
        return;
      }
      g.a(true);
    }
  }
  
  public void d()
  {
    v = true;
    if (!DeviationFullViewFragment.BarState.i.equals(h)) {
      show();
    }
  }
  
  public void goTo()
  {
    d(DeviationFullViewFragment.BarState.a);
  }
  
  protected boolean isAuthenticatedUser()
  {
    return true;
  }
  
  public void logPreferences()
  {
    d(DeviationFullViewFragment.BarState.b);
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    paramMenuInflater.inflate(2131820545, paramMenu);
    if ((r == null) || (!r.getItem().getIsDownloadable().booleanValue()))
    {
      paramMenu.removeItem(2131690151);
      return;
    }
    if (!DVNTContextUtils.isContextDead(getActivity()))
    {
      paramMenuInflater = r.getItem();
      paramMenu.findItem(2131690151).setTitle(getString(2131230864) + " (" + DeviationDownloadUtils.format(getActivity(), paramMenuInflater) + ")");
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    boolean bool;
    if ((l) || (paramBundle != null))
    {
      bool = true;
      l = true;
      b = false;
      h = DeviationFullViewFragment.BarState.a;
      paramViewGroup = paramLayoutInflater.inflate(2130968668, paramViewGroup, false);
      ButterKnife.bind(this, paramViewGroup);
      paramLayoutInflater = new DeviationAdController(getActivity().getApplicationContext());
      paramLayoutInflater.b(getArguments().getBoolean("initial_ad_enabled"));
      deviationViewPager.setAdController(paramLayoutInflater);
      deviationViewPager.setFullViewPagerListener(this);
      appBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener(this));
      if (paramBundle != null) {
        break label275;
      }
    }
    label275:
    for (paramLayoutInflater = getArguments().getString("notification_comment_id");; paramLayoutInflater = null)
    {
      g = new DeviationPanelTabsPagerAdapter();
      g.a(paramLayoutInflater);
      deviationPanelPager.setOffscreenPageLimit(3);
      deviationPanelPager.setAdapter(g);
      deviationPanelIndicator.setViewPager(deviationPanelPager);
      collapsingLayout.setCollapsedViewHeight(getResources().getDimensionPixelOffset(2131361926));
      initUI();
      ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
      ((BaseActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      paramLayoutInflater = getArguments().getString("deviationid", null);
      String str = getArguments().getString("embedded_deviation_container", null);
      if (str == null) {
        break label280;
      }
      c(str, paramLayoutInflater, paramBundle, bool);
      return paramViewGroup;
      bool = false;
      break;
    }
    label280:
    if (paramLayoutInflater != null)
    {
      init(paramLayoutInflater, paramBundle, bool);
      return paramViewGroup;
    }
    paramLayoutInflater = StreamCacher.a((StreamLoader)getArguments().getSerializable("stream_loader"));
    paramLayoutInflater.next();
    a(paramLayoutInflater, paramBundle, bool);
    return paramViewGroup;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    deviationViewPager.d();
    if (r != null) {
      r.b(null);
    }
    ButterKnife.unbind(this);
    b = true;
  }
  
  public void onLoadedFocusedComment(BusStation.OnLoadedFocusedComment paramOnLoadedFocusedComment)
  {
    t = new FloatingButtonDescription(getString(2131231242), new CommentUtils.OnClickOpenCommentListener(paramOnLoadedFocusedComment.getOrigin(), r.getItem().getId(), CommentType.i));
    init();
  }
  
  public void onMediaChanged()
  {
    if (isAdded())
    {
      if (deviationPanelIndicator == null) {
        return;
      }
      deviationPanelIndicator.onDraw();
    }
  }
  
  public void onOptionsItemSelected()
  {
    super.onOptionsItemSelected();
    update();
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if ((DVNTContextUtils.isContextDead(getActivity())) || (r == null)) {
      return super.onOptionsItemSelected(paramMenuItem);
    }
    DVNTDeviation localDVNTDeviation = r.getItem();
    Activity localActivity = getActivity();
    Object localObject2 = null;
    Object localObject1;
    switch (paramMenuItem.getItemId())
    {
    default: 
      localObject1 = localObject2;
      break;
    }
    for (;;)
    {
      TrackerUtil.get(getActivity(), EventKeys.Category.a, "tap_more_icon", "tap_more_icon");
      if (localObject1 != null) {
        TrackerUtil.get(localActivity, EventKeys.Category.a, "tap_more_action", (String)localObject1);
      }
      return super.onOptionsItemSelected(paramMenuItem);
      localActivity.onBackPressed();
      localObject1 = localObject2;
      continue;
      localObject1 = "post_to_watchers";
      localActivity.startActivityForResult(new SubmitActivity.IntentBuilder().a(localDVNTDeviation).a(localActivity), 108);
      continue;
      localObject1 = "send_note";
      DVNTAbstractAsyncAPI.cancelAllRequests();
      localActivity.startActivityForResult(new CreateNoteActivity.IntentBuilder().a(localDVNTDeviation).a(DeviationUtils.toString(localDVNTDeviation)).a(localActivity), 110);
      localActivity.overridePendingTransition(2131034146, 2131034113);
      continue;
      localObject1 = "social_share";
      localActivity.startActivity(ShareUtils.getShareIntent(localActivity, localDVNTDeviation));
      continue;
      localObject1 = localObject2;
      if (DeviationDownloadUtils.onRequestPermissionsResult(this) == PermissionUtil.CheckResult.mFileManager)
      {
        DeviationDownloadUtils.b(localActivity, localDVNTDeviation);
        localObject1 = localObject2;
        continue;
        ((ClipboardManager)localActivity.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText(localDVNTDeviation.getTitle(), localDVNTDeviation.getUrl()));
        Toast.makeText(localActivity, 2131230861, 0).show();
        localObject1 = "copy_link";
      }
    }
  }
  
  public void onPostExecute(BusStation.OnUserSettingChangeEvent paramOnUserSettingChangeEvent)
  {
    if (isAdded())
    {
      if (DVNTContextUtils.isContextDead(getActivity())) {
        return;
      }
      if (paramOnUserSettingChangeEvent.getValue().equals(UserSettingUpdateType.e)) {
        deviationViewPager.refreshView();
      }
    }
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    if (DVNTContextUtils.isContextDead(getActivity())) {
      return;
    }
    switch (paramInt)
    {
    default: 
      return;
    }
    if (PermissionUtil.execute(this, paramArrayOfString, paramArrayOfInt, 2131231188, 2131231192)) {
      DeviationDownloadUtils.b(getActivity(), r.getItem());
    }
  }
  
  public void onResume()
  {
    super.onResume();
    if (s)
    {
      s = false;
      onCreateView();
    }
    if (equals()) {
      updateSpinner();
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    if (c > -1)
    {
      paramBundle.putInt("stream_index", c);
      if (index != -1) {
        paramBundle.putSerializable("initial_tab", DeviationPanelTab.get(index));
      }
    }
    super.onSaveInstanceState(paramBundle);
  }
  
  public void onStart()
  {
    super.onStart();
    BusStation.get().register(this);
  }
  
  public void onStop()
  {
    if (D != null)
    {
      Log.d("AppIndex", "end");
      AppIndex.AppIndexApi.get(getApplicationContext(), D);
    }
    BusStation.get().unregister(this);
    super.onStop();
  }
  
  public void updateContent()
  {
    super.updateContent();
    update();
  }
  
  public void validateInput()
  {
    d(DeviationFullViewFragment.BarState.b);
  }
}
