package com.deviantart.android.damobile.view.thirdparty.decorator;

public enum DecoratorType
{
  static
  {
    b = new DecoratorType("WATCH_RECOMMENDATION", 1);
    i = new DecoratorType("DAILY_DEVIATION_INFO", 2);
  }
}
