package com.deviantart.android.android.package_14;

import com.deviantart.android.android.package_14.model.DVNTPublishOptions;
import com.deviantart.android.android.package_14.network.request.DVNTBrowseDailyDeviationRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTBrowseHotRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTBrowseNewestRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTBrowsePopularRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTBrowseSearchTagsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTBrowseTagsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTBrowseUndiscoveredRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTBrowseUserJournalsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTChallengeContentRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTChallengeEntriesRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTChallengeNewestRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTChallengeProgressionalRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTCollectionFoldersRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTCollectionRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTCommentContextRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTCommentsDeviationPostRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTCommentsDeviationRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTCommentsStatusPostRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTCommentsStatusRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTCommentsUserProfilePostRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTCommentsUserProfileRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTCuratedTagRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTDamnTokenRequestV0;
import com.deviantart.android.android.package_14.network.request.DVNTDeviationContentRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTDeviationDownloadInfoRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTDeviationEmbeddedRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTDeviationInfoRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTDeviationMetadataRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFaveRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFeedBucketRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFeedHomeRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFeedNotificationsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFeedProfileRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFeedSettingsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFeedSettingsUpdateRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFeedbackDeleteMessageRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTFriendsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTGalleryAllRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTGalleryFoldersRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTGalleryRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTGetAvatarUploadStatusRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTGetStatusRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTGetUserStatusesRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTMoreLikeThisPreviewRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTMoreLikeThisRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTPlaceboRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTPostStatusRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTPublishStashItemRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTSearchFriendsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTStashChangeStackPositionRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTStashDeleteItemRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTStashDeltaRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTStashFetchItemRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTStashFetchStackContentsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTStashFetchStackMetadataRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTStashMoveStackRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTStashSpaceRequestV0;
import com.deviantart.android.android.package_14.network.request.DVNTStashSubmitRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTStashUpdateStackRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTSubmissionCategoryTreeRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTSuggestFavouriteToGroupRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTUnfaveRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTUnwatchRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTUpdateAvatarRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTUpdateProfilePictureRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTUpdateUserProfileRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTUserFeaturesAndAgreementsRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTUserProfileRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTWatchRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTWatchersRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTWatchingRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTWhoAmIRequestV1;
import com.deviantart.android.android.package_14.network.request.DVNTWhoIsRequestV1;
import com.deviantart.android.sdk.api.model.DVNTAbstractDeviation.List;
import com.deviantart.android.sdk.api.model.DVNTAvatarResponse;
import com.deviantart.android.sdk.api.model.DVNTCategory.List;
import com.deviantart.android.sdk.api.model.DVNTComment;
import com.deviantart.android.sdk.api.model.DVNTCommentContextResponse;
import com.deviantart.android.sdk.api.model.DVNTDamnToken;
import com.deviantart.android.sdk.api.model.DVNTDeviation;
import com.deviantart.android.sdk.api.model.DVNTDeviationCommentsThread;
import com.deviantart.android.sdk.api.model.DVNTDeviationContent;
import com.deviantart.android.sdk.api.model.DVNTDeviationDownloadInfo;
import com.deviantart.android.sdk.api.model.DVNTDeviationMetadata.List;
import com.deviantart.android.sdk.api.model.DVNTFeaturesAndAgreements;
import com.deviantart.android.sdk.api.model.DVNTFeedBucketResponse;
import com.deviantart.android.sdk.api.model.DVNTFeedResponse;
import com.deviantart.android.sdk.api.model.DVNTFeedSettingsResponse;
import com.deviantart.android.sdk.api.model.DVNTFriendsResponse;
import com.deviantart.android.sdk.api.model.DVNTMoreLikeThisPreviewResults;
import com.deviantart.android.sdk.api.model.DVNTPaginatedCollectionFolders;
import com.deviantart.android.sdk.api.model.DVNTPaginatedGalleryFolders;
import com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse;
import com.deviantart.android.sdk.api.model.DVNTPostStatusResponse;
import com.deviantart.android.sdk.api.model.DVNTPublishStashItemResponse;
import com.deviantart.android.sdk.api.model.DVNTSearchedTag.List;
import com.deviantart.android.sdk.api.model.DVNTStashDelta;
import com.deviantart.android.sdk.api.model.DVNTStashItem;
import com.deviantart.android.sdk.api.model.DVNTStashMoveStackResponse;
import com.deviantart.android.sdk.api.model.DVNTStashSpace;
import com.deviantart.android.sdk.api.model.DVNTStashStackContents;
import com.deviantart.android.sdk.api.model.DVNTStashStackMetadata;
import com.deviantart.android.sdk.api.model.DVNTStashSubmitResponse;
import com.deviantart.android.sdk.api.model.DVNTStatus;
import com.deviantart.android.sdk.api.model.DVNTSuccess;
import com.deviantart.android.sdk.api.model.DVNTTag.List;
import com.deviantart.android.sdk.api.model.DVNTUser.List;
import com.deviantart.android.sdk.api.model.DVNTUserProfile;
import com.deviantart.android.sdk.api.model.DVNTUserStatus;
import com.deviantart.android.sdk.api.model.DVNTUserStatusResponse;
import com.deviantart.android.sdk.api.model.DVNTWatchingInfo;
import com.deviantart.android.sdk.api.model.DVNTWhoAmIResponse;
import com.deviantart.android.sdk.api.model.DVNTWhoIsResponses;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DVNTCommonAsyncAPI
  extends DVNTAbstractAsyncAPI
{
  public DVNTCommonAsyncAPI() {}
  
  public static DVNTRequestExecutor addToFavourites(String paramString1, String paramString2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFaveRequestV1(paramString1, paramString2), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor browseDailyDeviations(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    paramString = new DVNTBrowseDailyDeviationRequestV1(paramString, paramInteger1, paramInteger2);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString, DVNTAbstractDeviation.List.class);
  }
  
  public static DVNTRequestExecutor browseHot(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    paramString = new DVNTBrowseHotRequestV1(paramString, paramInteger1, paramInteger2);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString, DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor browseNewest(String paramString1, String paramString2, Boolean paramBoolean, Integer paramInteger1, Integer paramInteger2)
  {
    paramString1 = new DVNTBrowseNewestRequestV1(paramString1, paramString2, paramBoolean, paramInteger1, paramInteger2);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString1, DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor browsePopular(String paramString1, String paramString2, String paramString3, Boolean paramBoolean, Integer paramInteger1, Integer paramInteger2)
  {
    paramString1 = new DVNTBrowsePopularRequestV1(paramString1, paramString2, paramString3, paramBoolean, paramInteger1, paramInteger2);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString1, DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor browseSearchTags(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTBrowseSearchTagsRequestV1(paramString), DVNTSearchedTag.List.class);
  }
  
  public static DVNTRequestExecutor browseTag(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    paramString = new DVNTBrowseTagsRequestV1(paramString, paramInteger1, paramInteger2, Boolean.valueOf(false));
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString, DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor browseTagChallenges(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    paramString = new DVNTBrowseTagsRequestV1(paramString, paramInteger1, paramInteger2, Boolean.valueOf(true));
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString, DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor browseUndiscovered(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    paramString = new DVNTBrowseUndiscoveredRequestV1(paramString, paramInteger1, paramInteger2);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString, DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor browseUserJournals(String paramString, Boolean paramBoolean, Integer paramInteger1, Integer paramInteger2)
  {
    paramString = new DVNTBrowseUserJournalsRequestV1(paramString, paramBoolean, paramInteger1, paramInteger2);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString, DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor challengeContent(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTChallengeContentRequestV1(paramString), DVNTDeviationContent.class);
  }
  
  public static DVNTRequestExecutor challengeEntries(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    paramString = new DVNTChallengeEntriesRequestV1(paramString, paramInteger1, paramInteger2);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString, DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor challengeNewest(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    paramString = new DVNTChallengeNewestRequestV1(paramString, paramInteger1, paramInteger2);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString, DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor challengeProgressional(String paramString)
  {
    paramString = new DVNTChallengeProgressionalRequestV1(paramString);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString, DVNTAbstractDeviation.List.class);
  }
  
  public static DVNTRequestExecutor commentPostToDeviation(String paramString1, String paramString2, String paramString3)
  {
    paramString1 = new DVNTCommentsDeviationPostRequestV1(paramString1, paramString2, paramString3);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString1, DVNTComment.class);
  }
  
  public static DVNTRequestExecutor commentPostToStatus(String paramString1, String paramString2, String paramString3)
  {
    paramString1 = new DVNTCommentsStatusPostRequestV1(paramString1, paramString2, paramString3);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString1, DVNTComment.class);
  }
  
  public static DVNTRequestExecutor commentPostToUserProfile(String paramString1, String paramString2, String paramString3)
  {
    paramString1 = new DVNTCommentsUserProfilePostRequestV1(paramString1, paramString2, paramString3);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString1, DVNTComment.class);
  }
  
  public static DVNTRequestExecutor commentsForDeviation(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    paramString1 = new DVNTCommentsDeviationRequestV1(paramString1, paramString2, paramInteger1, paramInteger2);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString1, DVNTDeviationCommentsThread.class);
  }
  
  public static DVNTRequestExecutor commentsForStatus(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTCommentsStatusRequestV1(paramString1, paramString2, paramInteger1, paramInteger2), DVNTDeviationCommentsThread.class);
  }
  
  public static DVNTRequestExecutor commentsForUser(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTCommentsUserProfileRequestV1(paramString1, paramString2, paramInteger1, paramInteger2), DVNTDeviationCommentsThread.class);
  }
  
  public static DVNTRequestExecutor contextForComment(String paramString, boolean paramBoolean, Integer paramInteger1, Integer paramInteger2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTCommentContextRequestV1(paramString, paramBoolean, paramInteger1, paramInteger2), DVNTCommentContextResponse.class);
  }
  
  public static DVNTRequestExecutor curatedTags(boolean paramBoolean)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTCuratedTagRequestV1(paramBoolean), DVNTTag.List.class);
  }
  
  public static DVNTRequestExecutor damnToken()
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTDamnTokenRequestV0(), DVNTDamnToken.class);
  }
  
  public static DVNTRequestExecutor deleteSingleMessage(String paramString1, String paramString2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedbackDeleteMessageRequestV1(paramString1, paramString2, null), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor deleteStackedMessage(String paramString1, String paramString2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedbackDeleteMessageRequestV1(paramString1, null, paramString2), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor deviationContent(String paramString)
  {
    paramString = new DVNTDeviationContentRequestV1(paramString);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString, DVNTDeviationContent.class);
  }
  
  public static DVNTRequestExecutor deviationEmbedded(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    paramString1 = new DVNTDeviationEmbeddedRequestV1(paramString1, paramString2, paramInteger1, paramInteger2);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString1, DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor deviationInfo(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTDeviationInfoRequestV1(paramString), DVNTDeviation.class);
  }
  
  public static DVNTRequestExecutor deviationMetadata(List paramList, Boolean paramBoolean1, Boolean paramBoolean2, Boolean paramBoolean3, Boolean paramBoolean4)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTDeviationMetadataRequestV1(paramList, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4), DVNTDeviationMetadata.List.class);
  }
  
  public static DVNTRequestExecutor feedBucket(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedBucketRequestV1(paramString, paramInteger1, paramInteger2), DVNTFeedBucketResponse.class);
  }
  
  public static DVNTRequestExecutor feedHome(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedHomeRequestV1(paramString), DVNTFeedResponse.class);
  }
  
  public static DVNTRequestExecutor feedNotifications(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedNotificationsRequestV1(paramString), DVNTFeedResponse.class);
  }
  
  public static DVNTRequestExecutor feedProfile(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedProfileRequestV1(paramString), DVNTFeedResponse.class);
  }
  
  public static DVNTRequestExecutor feedSettings()
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedSettingsRequestV1(), DVNTFeedSettingsResponse.class);
  }
  
  public static DVNTRequestExecutor feedSettingsUpdate(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFeedSettingsUpdateRequestV1(paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4, paramBoolean5), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor getAllGallery(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTGalleryAllRequestV1(paramString, paramInteger1, paramInteger2), DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor getAvatarUploadStatus(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTGetAvatarUploadStatusRequestV1(paramString), DVNTAvatarResponse.class);
  }
  
  public static DVNTRequestExecutor getCollection(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTCollectionRequestV1(paramString1, paramString2, paramInteger1, paramInteger2), DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor getCollectionFolders(String paramString, boolean paramBoolean1, boolean paramBoolean2, Integer paramInteger1, Integer paramInteger2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTCollectionFoldersRequestV1(paramString, paramBoolean1, paramBoolean2, paramInteger1, paramInteger2), DVNTPaginatedCollectionFolders.class);
  }
  
  public static DVNTRequestExecutor getDeviationDownloadInfo(String paramString)
  {
    paramString = new DVNTDeviationDownloadInfoRequestV1(paramString);
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, paramString, DVNTDeviationDownloadInfo.class);
  }
  
  public static DVNTRequestExecutor getFriends(String paramString, Integer paramInteger1, Integer paramInteger2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTFriendsRequestV1(paramString, paramInteger1, paramInteger2, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4), DVNTFriendsResponse.class);
  }
  
  public static DVNTRequestExecutor getGallery(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTGalleryRequestV1(paramString1, paramString2, paramInteger1, paramInteger2), DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor getGalleryFolders(String paramString, boolean paramBoolean1, boolean paramBoolean2, Integer paramInteger1, Integer paramInteger2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTGalleryFoldersRequestV1(paramString, paramBoolean1, paramBoolean2, paramInteger1, paramInteger2), DVNTPaginatedGalleryFolders.class);
  }
  
  public static DVNTRequestExecutor getStatus(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTGetStatusRequestV1(paramString), DVNTUserStatus.class);
  }
  
  public static DVNTRequestExecutor getUserStatuses(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTGetUserStatusesRequestV1(paramString, paramInteger1, paramInteger2), DVNTUserStatusResponse.class);
  }
  
  public static DVNTRequestExecutor getWatchers(String paramString, Integer paramInteger1, Integer paramInteger2, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTWatchersRequestV1(paramString, paramInteger1, paramInteger2, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4), DVNTFriendsResponse.class);
  }
  
  public static DVNTRequestExecutor moreLikeThis(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTMoreLikeThisRequestV1(paramString1, paramString2, paramInteger1, paramInteger2), DVNTPaginatedResultResponse.class);
  }
  
  public static DVNTRequestExecutor moreLikeThisPreview(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTMoreLikeThisPreviewRequestV1(paramString), DVNTMoreLikeThisPreviewResults.class);
  }
  
  public static DVNTRequestExecutor placebo()
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTPlaceboRequestV1(), DVNTStatus.class);
  }
  
  public static DVNTRequestExecutor postStatus(String paramString1, String paramString2, String paramString3)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTPostStatusRequestV1(paramString1, paramString2, paramString3), DVNTPostStatusResponse.class);
  }
  
  public static DVNTRequestExecutor publishStashItem(Long paramLong, String paramString, ArrayList paramArrayList, DVNTPublishOptions paramDVNTPublishOptions)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTPublishStashItemRequestV1(paramLong, paramString, paramArrayList, paramDVNTPublishOptions), DVNTPublishStashItemResponse.class);
  }
  
  public static DVNTRequestExecutor removeFromFavourites(String paramString1, String paramString2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTUnfaveRequestV1(paramString1, paramString2), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor searchFriends(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTSearchFriendsRequestV1(paramString), DVNTUser.List.class);
  }
  
  public static DVNTRequestExecutor stashChangeStackPosition(Long paramLong, Integer paramInteger)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTStashChangeStackPositionRequestV1(paramLong, paramInteger), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor stashDeleteItem(Long paramLong)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTStashDeleteItemRequestV1(paramLong), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor stashDelta(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    return stashDelta(paramString, paramInteger1, paramInteger2, Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false));
  }
  
  public static DVNTRequestExecutor stashDelta(String paramString, Integer paramInteger1, Integer paramInteger2, Boolean paramBoolean1, Boolean paramBoolean2, Boolean paramBoolean3)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTStashDeltaRequestV1(paramString, paramInteger1, paramInteger2, paramBoolean1, paramBoolean2, paramBoolean3), DVNTStashDelta.class);
  }
  
  public static DVNTRequestExecutor stashFetchItem(Long paramLong)
  {
    return stashFetchItem(paramLong, Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false));
  }
  
  public static DVNTRequestExecutor stashFetchItem(Long paramLong, Boolean paramBoolean1, Boolean paramBoolean2, Boolean paramBoolean3)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTStashFetchItemRequestV1(paramLong, paramBoolean1, paramBoolean2, paramBoolean3), DVNTStashItem.class);
  }
  
  public static DVNTRequestExecutor stashFetchStackContents(Long paramLong)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTStashFetchStackContentsRequestV1(paramLong), DVNTStashStackContents.class);
  }
  
  public static DVNTRequestExecutor stashFetchStackMetadata(Long paramLong)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTStashFetchStackMetadataRequestV1(paramLong), DVNTStashStackMetadata.class);
  }
  
  public static DVNTRequestExecutor stashMoveStack(Long paramLong1, Long paramLong2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTStashMoveStackRequestV1(paramLong1, paramLong2), DVNTStashMoveStackResponse.class);
  }
  
  public static DVNTRequestExecutor stashSpace()
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTStashSpaceRequestV0(), DVNTStashSpace.class);
  }
  
  public static DVNTRequestExecutor stashSubmitFile(File paramFile1, String paramString1, File paramFile2, String paramString2, String paramString3, String paramString4, String paramString5, ArrayList paramArrayList)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTStashSubmitRequestV1(paramString3, paramString4, paramString5, paramArrayList, paramFile1, paramString1, paramFile2, paramString2), DVNTStashSubmitResponse.class);
  }
  
  public static DVNTRequestExecutor stashSubmitFile(File paramFile, String paramString1, String paramString2, String paramString3, String paramString4, ArrayList paramArrayList)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTStashSubmitRequestV1(paramString2, paramString3, paramString4, paramArrayList, paramFile, paramString1), DVNTStashSubmitResponse.class);
  }
  
  public static DVNTRequestExecutor stashUpdateStack(Long paramLong, String paramString1, String paramString2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTStashUpdateStackRequestV1(paramLong, paramString1, paramString2), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor submissionCategoryTree(String paramString1, String paramString2, Boolean paramBoolean)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTSubmissionCategoryTreeRequestV1(paramString1, paramString2, paramBoolean), DVNTCategory.List.class);
  }
  
  public static DVNTRequestExecutor suggestFavToGroup(String paramString1, String paramString2, Integer paramInteger)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTSuggestFavouriteToGroupRequestV1(paramString1, paramString2, paramInteger), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor unwatch(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTUnwatchRequestV1(paramString), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor updateAvatar(String paramString, File paramFile)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTUpdateAvatarRequestV1(paramString, paramFile), DVNTAvatarResponse.class);
  }
  
  public static DVNTRequestExecutor updateProfilePicture(String paramString, File paramFile)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTUpdateProfilePictureRequestV1(paramString, paramFile), DVNTAvatarResponse.class);
  }
  
  public static DVNTRequestExecutor updateUserProfile(boolean paramBoolean, Integer paramInteger1, Integer paramInteger2, String paramString1, String paramString2, Integer paramInteger3, String paramString3, String paramString4)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTUpdateUserProfileRequestV1(Boolean.valueOf(paramBoolean), paramInteger1, paramInteger2, paramString1, paramString2, paramInteger3, paramString3, paramString4), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor userFeaturesAndAgreements()
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTUserFeaturesAndAgreementsRequestV1(), DVNTFeaturesAndAgreements.class);
  }
  
  public static DVNTRequestExecutor userProfile(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTUserProfileRequestV1(paramString, paramBoolean1, paramBoolean2), DVNTUserProfile.class);
  }
  
  public static DVNTRequestExecutor watch(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTWatchRequestV1(paramString, true, true, true, true, true, false, true, true), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor watch(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTWatchRequestV1(paramString, paramBoolean1, paramBoolean2, paramBoolean3, paramBoolean4, paramBoolean5, paramBoolean6, paramBoolean7, paramBoolean8), DVNTSuccess.class);
  }
  
  public static DVNTRequestExecutor watching(String paramString)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTWatchingRequestV1(paramString), DVNTWatchingInfo.class);
  }
  
  public static DVNTRequestExecutor whoAmI()
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTWhoAmIRequestV1(), DVNTWhoAmIResponse.class);
  }
  
  public static DVNTRequestExecutor whoIs(List paramList)
  {
    return new DVNTRequestExecutor(DVNTAbstractAsyncAPI.asyncAPIInstance, new DVNTWhoIsRequestV1(paramList), DVNTWhoIsResponses.class);
  }
}
