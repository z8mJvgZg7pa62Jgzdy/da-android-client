package com.deviantart.android.damobile.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.deviantart.android.android.utils.DVNTContextUtils;

public class Keyboard
{
  public Keyboard() {}
  
  public static void hideKeyboard(Activity paramActivity)
  {
    if (!DVNTContextUtils.isContextDead(paramActivity))
    {
      if (paramActivity.getCurrentFocus() == null) {
        return;
      }
      hideKeyboard(paramActivity, paramActivity.getCurrentFocus());
    }
  }
  
  public static void hideKeyboard(Context paramContext, View paramView)
  {
    if (!DVNTContextUtils.isContextDead(paramContext))
    {
      if (paramView.getWindowToken() == null) {
        return;
      }
      ((InputMethodManager)paramContext.getSystemService("input_method")).hideSoftInputFromWindow(paramView.getWindowToken(), 2);
    }
  }
  
  public static boolean processRequest(View paramView)
  {
    Rect localRect = new Rect();
    paramView.getWindowVisibleDisplayFrame(localRect);
    return paramView.getRootView().getHeight() - (bottom - top) > paramView.getRootView().getHeight() / 4;
  }
  
  public static void showKeyboard(Activity paramActivity, EditText paramEditText)
  {
    paramEditText.requestFocus();
    ((InputMethodManager)paramActivity.getSystemService("input_method")).showSoftInput(paramEditText, 1);
  }
}
