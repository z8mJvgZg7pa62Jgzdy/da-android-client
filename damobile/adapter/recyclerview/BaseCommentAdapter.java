package com.deviantart.android.damobile.adapter.recyclerview;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.DAMobileApplication;
import com.deviantart.android.damobile.adapter.HeaderFooterRecyclerViewAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APICommentsLoader;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.stream.loader.StreamLoader.ErrorType;
import com.deviantart.android.damobile.util.CommentType;
import com.deviantart.android.damobile.util.CommentUtils;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.threaditem.ChildStreamNotifiable;
import com.deviantart.android.damobile.util.threaditem.CommentItem;
import com.deviantart.android.damobile.view.thirdparty.ByteVector;
import com.deviantart.android.damobile.view.thirdparty.GomFactory;
import com.deviantart.android.damobile.view.thirdparty.GomType;
import com.deviantart.android.damobile.view.thirdparty.comment.CommentGom;
import com.deviantart.android.damobile.view.thirdparty.comment.viewholder.CommentViewHolderBase;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public abstract class BaseCommentAdapter<STREAM_ITEM>
  extends ThreadedItemsAdapter<STREAM_ITEM, com.deviantart.android.sdk.api.model.DVNTComment, CommentItem>
{
  private String a;
  protected final HashMap<String, Integer> b = new HashMap();
  private String d;
  private boolean e = false;
  private CommentType o;
  
  public BaseCommentAdapter(StreamLoader paramStreamLoader, CommentType paramCommentType, String paramString1, String paramString2)
  {
    super(StreamCacher.a(paramStreamLoader));
    a = paramString2;
    o = paramCommentType;
    d = paramString1;
    b.clear();
  }
  
  private void a(Stream paramStream, List paramList, CommentItem paramCommentItem, int paramInt)
  {
    paramList = paramList.iterator();
    if (paramList.hasNext())
    {
      Object localObject = (com.deviantart.android.android.package_14.model.DVNTComment)paramList.next();
      boolean bool;
      if (!paramStream.equals())
      {
        bool = true;
        label37:
        localObject = new CommentItem((com.deviantart.android.android.package_14.model.DVNTComment)localObject, bool);
        if (paramCommentItem.b() != null) {
          break label118;
        }
        ((CommentItem)localObject).b(paramCommentItem.c());
        label66:
        if (!e) {
          break label130;
        }
        ((CommentItem)localObject).add(paramCommentItem.get());
      }
      for (;;)
      {
        paramInt += 1;
        d += 1;
        c.add(paramInt, localObject);
        break;
        bool = false;
        break label37;
        label118:
        ((CommentItem)localObject).b(paramCommentItem.a());
        break label66;
        label130:
        ((CommentItem)localObject).add(Integer.valueOf(paramCommentItem.get().intValue() + 1));
      }
    }
    paramStream = (CommentItem)c.get(paramInt);
    if (paramStream.isUnread().booleanValue()) {
      paramStream.b(true);
    }
  }
  
  public void a()
  {
    super.a();
    b.clear();
  }
  
  public void a(Context paramContext, com.deviantart.android.android.package_14.model.DVNTComment paramDVNTComment)
  {
    b(null);
    if (paramDVNTComment.getParentId() == null)
    {
      paramContext = new CommentItem(paramDVNTComment, false);
      paramContext.b(paramDVNTComment.getCommentId());
      paramContext.add(Integer.valueOf(0));
      c.add(0, paramContext);
      d += 1;
      try
      {
        close(0);
        return;
      }
      catch (Exception paramContext)
      {
        new Handler().post(BaseCommentAdapter..Lambda.4.c(this));
        return;
      }
    }
    int i = 0;
    CommentItem localCommentItem1;
    while (i < e())
    {
      localCommentItem1 = (CommentItem)c.get(i);
      if ((localCommentItem1 == null) || (localCommentItem1.c() == null) || (!localCommentItem1.c().equals(paramDVNTComment.getParentId())))
      {
        i += 1;
      }
      else
      {
        if (localCommentItem1.visitAnnotation().booleanValue()) {
          break label201;
        }
        localCommentItem1.intValue().setRepliesCount(Integer.valueOf(localCommentItem1.next().intValue() + 1));
        a(paramContext, localCommentItem1, i, true);
      }
    }
    for (;;)
    {
      try
      {
        c();
        return;
      }
      catch (Exception paramContext)
      {
        new Handler().post(BaseCommentAdapter..Lambda.5.c(this));
        return;
      }
      label201:
      paramDVNTComment = new ArrayList();
      a(paramDVNTComment, localCommentItem1.c());
      paramDVNTComment = paramDVNTComment.iterator();
      while (paramDVNTComment.hasNext())
      {
        CommentItem localCommentItem2 = (CommentItem)paramDVNTComment.next();
        a.remove(localCommentItem2.f());
        c.remove(localCommentItem2);
      }
      localCommentItem1.d(Boolean.valueOf(false));
      a(paramContext, localCommentItem1, i, true);
    }
  }
  
  public void a(Context paramContext, CommentItem paramCommentItem, int paramInt)
  {
    e = true;
    String str = paramCommentItem.d();
    Stream localStream = (Stream)a.get(str);
    if (localStream != null) {
      localStream.add(paramContext, new ChildStreamNotifiable(this, str, paramCommentItem, paramInt), false);
    }
  }
  
  public void a(Context paramContext, CommentItem paramCommentItem, int paramInt, boolean paramBoolean)
  {
    if (DVNTContextUtils.isContextDead(paramContext)) {
      return;
    }
    Stream localStream = StreamCacher.a(new APICommentsLoader(o, d, paramCommentItem.c()));
    a.put(paramCommentItem.f(), localStream);
    localStream.add(paramContext, new ChildStreamNotifiable(this, paramCommentItem.f(), paramCommentItem, paramInt), paramBoolean);
  }
  
  public void a(CommentItem paramCommentItem, int paramInt, StreamLoader.ErrorType paramErrorType, String paramString)
  {
    paramCommentItem = DAMobileApplication.getContext();
    if (DVNTContextUtils.isContextDead(paramCommentItem)) {
      return;
    }
    Toast.makeText(paramCommentItem, 2131230906, 0).show();
    e = false;
    try
    {
      c();
      return;
    }
    catch (Exception paramCommentItem)
    {
      new Handler().post(BaseCommentAdapter..Lambda.3.c(this));
    }
  }
  
  public void a(String paramString, CommentItem paramCommentItem, int paramInt)
  {
    if (a != null)
    {
      if (a.isEmpty()) {
        return;
      }
      Stream localStream = (Stream)a.get(paramString);
      if (localStream != null)
      {
        if (b.containsKey(paramString)) {}
        for (int i = ((Integer)b.get(paramString)).intValue();; i = 0)
        {
          int j = localStream.size();
          b.put(paramString, Integer.valueOf(j));
          paramString = localStream.get().subList(i, j);
          if (paramString.isEmpty()) {
            break;
          }
          paramInt = b(paramCommentItem, paramInt);
          b(paramCommentItem.b());
          a(localStream, paramString, paramCommentItem, paramInt);
          paramCommentItem.d(Boolean.valueOf(true));
          try
          {
            c();
            e = false;
            return;
          }
          catch (Exception paramString)
          {
            for (;;)
            {
              new Handler().post(BaseCommentAdapter..Lambda.1.c(this));
            }
          }
        }
      }
    }
  }
  
  protected int b(int paramInt)
  {
    return GomFactory.a(((CommentItem)c.get(paramInt)).intValue()).b().ordinal();
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    return GomType.b(paramInt).b(paramViewGroup.getContext(), paramViewGroup);
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    Context localContext = itemView.getContext();
    if (DVNTContextUtils.isContextDead(localContext)) {
      return;
    }
    paramViewHolder = (CommentViewHolderBase)paramViewHolder;
    CommentItem localCommentItem = (CommentItem)c.get(paramInt);
    if ((paramViewHolder.buildKey() != null) && (a.get(paramViewHolder.buildKey()) != null) && (((Stream)a.get(paramViewHolder.buildKey())).equals())) {
      a.remove(paramViewHolder.buildKey());
    }
    paramViewHolder.from(localCommentItem.f());
    CommentGom localCommentGom = GomFactory.a(localCommentItem.intValue());
    localCommentGom.b(a);
    int i = 0;
    if (b.containsKey(localCommentItem.a())) {
      i = ((Integer)b.get(localCommentItem.a())).intValue();
    }
    localCommentGom.a(i);
    b(localCommentItem);
    localCommentGom.a(localContext, localCommentItem, paramViewHolder);
    if ((localCommentItem.intValue() != null) && (localCommentItem.intValue().getHidden() == null)) {
      itemView.setOnClickListener(new BaseCommentAdapter.1(this, localCommentItem));
    }
    commentRightCol.setOnClickListener(new BaseCommentAdapter.2(this, localCommentItem, paramInt));
    commentLoadMoreButton.setOnClickListener(new BaseCommentAdapter.3(this, localCommentItem, paramInt, paramViewHolder));
  }
  
  protected void b(CommentItem paramCommentItem) {}
  
  public void b(String paramString)
  {
    Iterator localIterator = c.iterator();
    while (localIterator.hasNext())
    {
      CommentItem localCommentItem = (CommentItem)localIterator.next();
      if ((localCommentItem.b() != null) && (localCommentItem.b().equals(paramString))) {
        localCommentItem.b(false);
      }
    }
  }
  
  protected boolean b(com.deviantart.android.android.package_14.model.DVNTComment paramDVNTComment)
  {
    return (paramDVNTComment.getHidden() != null) && ((UserUtils.a == null) || (!CommentUtils.b(paramDVNTComment, a))) && (paramDVNTComment.getRepliesCount().intValue() == 0);
  }
  
  public void d(CommentItem paramCommentItem, int paramInt)
  {
    paramCommentItem = DAMobileApplication.getContext();
    if (DVNTContextUtils.isContextDead(paramCommentItem)) {
      return;
    }
    Toast.makeText(paramCommentItem, 2131230880, 0).show();
    e = false;
    try
    {
      c();
      return;
    }
    catch (Exception paramCommentItem)
    {
      new Handler().post(BaseCommentAdapter..Lambda.2.c(this));
    }
  }
}
