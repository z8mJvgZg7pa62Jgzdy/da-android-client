package com.deviantart.android.damobile.view.userprofile;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.util.DAFormatUtils;

public class UserProfileCardStats
  extends FrameLayout
  implements UserProfileCardBase
{
  @Bind({2131690107})
  TextView numComments;
  @Bind({2131690106})
  TextView numDeviations;
  @Bind({2131690109})
  TextView numFavs;
  @Bind({2131690108})
  TextView numPageviews;
  @Bind({2131690110})
  TextView numWatchers;
  
  public UserProfileCardStats(Context paramContext)
  {
    super(paramContext);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130968793, this, true);
    ButterKnife.bind(this);
  }
  
  private String a(Integer paramInteger)
  {
    if (paramInteger == null) {
      return "0";
    }
    return DAFormatUtils.format(paramInteger);
  }
  
  public void onCreateView(UserCardData paramUserCardData)
  {
    numDeviations.setText(a(paramUserCardData.getStringArray()));
    numComments.setText(a(paramUserCardData.visitAnnotationDefault()));
    numPageviews.setText(a(paramUserCardData.getApplicationContext()));
    numFavs.setText(a(paramUserCardData.visitParameterAnnotation()));
    numWatchers.setText(a(paramUserCardData.update()));
  }
}
