package com.deviantart.android.android.package_14.jsonadapter;

import com.deviantart.android.android.package_14.model.DVNTChallengeMediaType;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;

public class DVNTChallengeMediaJSONAdapter
  implements JsonDeserializer<com.deviantart.android.sdk.api.model.DVNTChallengeMedia>, JsonSerializer<com.deviantart.android.sdk.api.model.DVNTChallengeMedia>
{
  public DVNTChallengeMediaJSONAdapter() {}
  
  public com.deviantart.android.android.package_14.model.DVNTChallengeMedia deserialize(JsonElement paramJsonElement, Type paramType, JsonDeserializationContext paramJsonDeserializationContext)
  {
    return (com.deviantart.android.android.package_14.model.DVNTChallengeMedia)paramJsonDeserializationContext.deserialize(paramJsonElement, valueOfgetAsJsonObjectget"type"getAsStringtoUpperCasetype);
  }
  
  public JsonElement serialize(com.deviantart.android.android.package_14.model.DVNTChallengeMedia paramDVNTChallengeMedia, Type paramType, JsonSerializationContext paramJsonSerializationContext)
  {
    return paramJsonSerializationContext.serialize(paramDVNTChallengeMedia);
  }
}
