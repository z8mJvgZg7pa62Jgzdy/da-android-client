package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.activity.UserSettingsActivity;

public class VerifyEmailDialogFragment
  extends DialogFragment
{
  @Bind({2131689692})
  TextView changeEmailAddress;
  @Bind({2131689690})
  TextView messageLine2;
  @Bind({2131689694})
  TextView resendEmail;
  
  public VerifyEmailDialogFragment() {}
  
  private void populateView()
  {
    changeEmailAddress.setPaintFlags(changeEmailAddress.getPaintFlags() | 0x8);
    resendEmail.setPaintFlags(resendEmail.getPaintFlags() | 0x8);
  }
  
  private static Dialog show(Context paramContext, View paramView)
  {
    paramContext = new AlertDialog.Builder(paramContext);
    paramContext.setView(paramView).setTitle(2131231412).setPositiveButton(2131231183, new VerifyEmailDialogFragment.1());
    paramContext = paramContext.create();
    paramView = new WindowManager.LayoutParams();
    paramView.copyFrom(paramContext.getWindow().getAttributes());
    width = -1;
    height = -2;
    paramContext.show();
    paramContext.getWindow().setAttributes(paramView);
    return paramContext;
  }
  
  public void goToSettings()
  {
    dismiss();
    getActivity().startActivityForResult(UserSettingsActivity.startActivity(getActivity()), 105);
    getActivity().overridePendingTransition(2131034138, 2131034142);
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = getActivity().getLayoutInflater().inflate(2130968649, null);
    ButterKnife.bind(this, paramBundle);
    messageLine2.setText(EmailSentDialogFragment.getTitle(getActivity()));
    populateView();
    return show(getActivity(), paramBundle);
  }
  
  public void resendEmail()
  {
    DVNTAsyncAPI.resendVerificationEmail().call(getActivity(), new VerifyEmailDialogFragment.2(this));
  }
}
