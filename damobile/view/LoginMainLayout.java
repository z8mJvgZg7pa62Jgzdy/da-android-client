package com.deviantart.android.damobile.view;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.fragment.LoginDialogFragment.LoginDialogListener;
import com.deviantart.android.damobile.util.SignUpHelper;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginMainLayout
  extends LinearLayout
{
  @Bind({2131689969})
  TextView forgotText;
  @Bind({2131689965})
  TextView formErrorText;
  @Bind({2131689966})
  FrameLayout loginButton;
  @Bind({2131689968})
  ProgressBar loginLoading;
  @Bind({2131689967})
  TextView loginText;
  @Bind({2131689964})
  EditTextWithClearFocus password;
  @Bind({2131689786})
  TextView passwordValidator;
  LoginDialogFragment.LoginDialogListener this$0;
  @Bind({2131689782})
  TextView userNameValidator;
  @Bind({2131689963})
  EditTextWithClearFocus username;
  
  public LoginMainLayout(Context paramContext, LoginDialogFragment.LoginDialogListener paramLoginDialogListener)
  {
    super(paramContext);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130968734, this, true);
    ButterKnife.bind(this);
    this$0 = paramLoginDialogListener;
    init();
  }
  
  public void doLoginButton()
  {
    if (username.getText().toString().trim().length() == 0)
    {
      username.setError(getContext().getString(2131231078));
      return;
    }
    if (password.getText().toString().trim().length() == 0)
    {
      password.setError(getContext().getString(2131231062));
      return;
    }
    if ((loginButton.isEnabled()) && (this$0 != null))
    {
      loginButton.setEnabled(false);
      this$0.hyphenate(username.getText().toString().trim(), password.getText().toString());
    }
  }
  
  public void focusChangePasswordEditText(boolean paramBoolean)
  {
    SignUpHelper.onItemSelected(paramBoolean, password, passwordValidator, 2131231186);
  }
  
  public void focusChangeUserNameEditText(boolean paramBoolean)
  {
    SignUpHelper.onItemSelected(paramBoolean, username, userNameValidator, 2131231404);
  }
  
  protected void init()
  {
    Matcher localMatcher = Pattern.compile("Username|Password").matcher(getContext().getString(2131231052));
    SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(getContext().getString(2131231052));
    while (localMatcher.find()) {
      localSpannableStringBuilder.setSpan(new LoginMainLayout.RecoveryClickableSpan(this, localMatcher.group()), localMatcher.start(), localMatcher.end(), 33);
    }
    forgotText.setText(localSpannableStringBuilder);
    forgotText.setMovementMethod(LinkMovementMethod.getInstance());
  }
  
  public void setFrom(String paramString)
  {
    if (loginButton != null)
    {
      if (formErrorText == null) {
        return;
      }
      loginButton.setEnabled(true);
      formErrorText.setText(paramString);
      formErrorText.setVisibility(0);
    }
  }
  
  public void setInformation()
  {
    formErrorText.setVisibility(8);
  }
  
  public void setVideo(boolean paramBoolean)
  {
    if (loginText != null)
    {
      if (loginLoading == null) {
        return;
      }
      if (paramBoolean)
      {
        loginText.setVisibility(8);
        loginLoading.setVisibility(0);
        return;
      }
      loginText.setVisibility(0);
      loginLoading.setVisibility(8);
    }
  }
}
