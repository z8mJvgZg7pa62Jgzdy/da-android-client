package com.deviantart.android.damobile.util.notifications;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessageStack;
import com.deviantart.android.damobile.util.opt.ItemDeleteType;
import com.deviantart.android.damobile.util.opt.ItemDeleteUtils;
import com.deviantart.android.damobile.util.opt.NotificationItemDeleteHelper;

public class NotificationItemDeleteClick
{
  public NotificationItemDeleteClick() {}
  
  public static boolean a(Context paramContext, NotificationItemData paramNotificationItemData)
  {
    int i;
    int j;
    if ((paramNotificationItemData.get()) && (paramNotificationItemData.getString().getCount() != null) && (paramNotificationItemData.getString().getCount().intValue() > 1))
    {
      i = 1;
      if (i == 0) {
        break label139;
      }
      j = 2131231139;
      label40:
      if (i == 0) {
        break label145;
      }
    }
    label139:
    label145:
    for (int k = 2131231138;; k = 2131231136)
    {
      String str2 = paramContext.getString(k);
      String str1 = str2;
      if (i != 0) {
        str1 = str2.replace("{num}", paramNotificationItemData.getString().getCount().toString());
      }
      new AlertDialog.Builder(paramContext).setTitle(j).setMessage(str1).setPositiveButton(2131230852, new NotificationItemDeleteClick.2(paramContext, paramNotificationItemData)).setNegativeButton(2131230820, new NotificationItemDeleteClick.1()).create().show();
      return true;
      i = 0;
      break;
      j = 2131231137;
      break label40;
    }
  }
  
  private static NotificationItemDeleteHelper b()
  {
    return (NotificationItemDeleteHelper)ItemDeleteUtils.a(ItemDeleteType.b);
  }
}
