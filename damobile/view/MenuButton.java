package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import com.deviantart.android.damobile.util.ProcessMenuListener;

public abstract class MenuButton<T>
  extends FrameLayout
{
  protected ProcessMenuListener J;
  protected MenuButton.onAnimationFinishListener view;
  
  public MenuButton(Activity paramActivity)
  {
    super(paramActivity);
  }
  
  abstract boolean b();
  
  public Animation getOnSelectedAnimation()
  {
    return null;
  }
  
  public void setOnAnimationFinishListener(MenuButton.onAnimationFinishListener paramOnAnimationFinishListener)
  {
    view = paramOnAnimationFinishListener;
  }
  
  public void setProcessMenuListener(ProcessMenuListener paramProcessMenuListener)
  {
    J = paramProcessMenuListener;
  }
}
