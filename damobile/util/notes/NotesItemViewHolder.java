package com.deviantart.android.damobile.util.notes;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTNote;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.DynamicDialogItemBuilder;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.util.UserDisplay;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;

public class NotesItemViewHolder
  extends RecyclerView.ViewHolder
  implements NotesItemData.Observer
{
  @Bind({2131689769})
  SimpleDraweeView avatarView;
  private DVNTUser c;
  private NotesItemData.Observable d;
  @Bind({2131689772})
  TextView dateView;
  private final String i;
  @Bind({2131689988})
  TextView messageView;
  @Bind({2131689984})
  FrameLayout readStateBgLayout;
  @Bind({2131689989})
  ImageView starIconView;
  @Bind({2131689766})
  TextView subjectView;
  private NotesItemData text;
  @Bind({2131689986})
  TextView userView;
  
  public NotesItemViewHolder(View paramView, String paramString)
  {
    super(paramView);
    i = paramString;
    ButterKnife.bind(this, paramView);
  }
  
  private void displayInfo(Resources paramResources)
  {
    readStateBgLayout.setBackgroundResource(2131558523);
    subjectView.setTextColor(paramResources.getColor(2131558524));
    userView.setTextColor(paramResources.getColor(2131558524));
    messageView.setTextColor(paramResources.getColor(2131558525));
  }
  
  private void onCreate(Activity paramActivity)
  {
    NotesItemData.Observable localObservable = d;
    boolean bool = localObservable.a().getContext().getIsUnread().booleanValue();
    DynamicDialogItemBuilder localDynamicDialogItemBuilder = new DynamicDialogItemBuilder();
    if (!text.getContext().getIsSent().booleanValue()) {
      if (!bool) {
        break label138;
      }
    }
    label138:
    for (int j = 2131231113;; j = 2131231114)
    {
      localDynamicDialogItemBuilder.add(0, paramActivity.getString(j));
      localDynamicDialogItemBuilder.add(1, paramActivity.getString(2131231111));
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramActivity);
      localBuilder.setTitle(2131231112);
      localBuilder.setItems(localDynamicDialogItemBuilder.getTitles(), new NotesItemViewHolder.2(this, paramActivity, localDynamicDialogItemBuilder, bool, localObservable)).setNegativeButton(2131230820, new NotesItemViewHolder.1(this)).create().show();
      return;
    }
  }
  
  private void updateFragments(Resources paramResources)
  {
    readStateBgLayout.setBackgroundResource(0);
    subjectView.setTextColor(paramResources.getColor(2131558521));
    userView.setTextColor(paramResources.getColor(2131558521));
    messageView.setTextColor(paramResources.getColor(2131558521));
  }
  
  public void b(NotesItemData paramNotesItemData)
  {
    text = paramNotesItemData;
    DVNTNote localDVNTNote = paramNotesItemData.getContext();
    if (localDVNTNote.getIsSent().booleanValue()) {}
    for (c = ((DVNTUser)localDVNTNote.getRecipients().get(0));; c = localDVNTNote.getUser())
    {
      d = new NotesItemData.Observable(paramNotesItemData);
      d.a(this);
      onCreate();
      return;
    }
  }
  
  public void doRefresh()
  {
    onCreate();
  }
  
  void onClickAvatar(View paramView)
  {
    UserUtils.c((Activity)paramView.getContext(), c.getUserName());
  }
  
  void onClickItem(View paramView)
  {
    NavigationUtils.b((Activity)paramView.getContext(), d, i);
  }
  
  void onClickStar(View paramView)
  {
    if (text.getContext().getIsStarred().booleanValue()) {}
    for (Mark localMark = Mark.d;; localMark = Mark.b)
    {
      if (Mark.b.equals(localMark)) {
        TrackerUtil.get((Activity)paramView.getContext(), EventKeys.Category.d, "star_a_note", i);
      }
      NotesAction.a(paramView.getContext(), d, localMark);
      return;
    }
  }
  
  public void onCreate()
  {
    Context localContext = itemView.getContext();
    Resources localResources = localContext.getResources();
    DVNTNote localDVNTNote = text.getContext();
    if ((localDVNTNote.getIsUnread().booleanValue()) && (!localDVNTNote.getIsSent().booleanValue()))
    {
      displayInfo(localResources);
      if (!localDVNTNote.getIsStarred().booleanValue()) {
        break label154;
      }
      starIconView.setImageResource(2130837739);
      label66:
      ImageUtils.isEmpty(avatarView, Uri.parse(c.getUserIconURL()));
      dateView.setText(DAFormatUtils.get(localContext, localDVNTNote.getTimeStamp()));
      subjectView.setText(localDVNTNote.getSubject());
      if (!localDVNTNote.getIsSent().booleanValue()) {
        break label167;
      }
      userView.setText(UserDisplay.getText(localContext, localDVNTNote.getRecipients()));
    }
    for (;;)
    {
      messageView.setText(localDVNTNote.getPreview());
      return;
      updateFragments(localResources);
      break;
      label154:
      starIconView.setImageResource(2130837740);
      break label66;
      label167:
      userView.setText(UserDisplay.add(localContext, localDVNTNote.getUser()));
    }
  }
  
  boolean onLongClickItem(View paramView)
  {
    onCreate((Activity)paramView.getContext());
    return true;
  }
}
