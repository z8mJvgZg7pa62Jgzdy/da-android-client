package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTStashFetchItemRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTStashItem>
{
  private final Boolean extCamera;
  private final Boolean extStats;
  private final Boolean extSubmission;
  private final Long itemId;
  
  public DVNTStashFetchItemRequestV1(Long paramLong, Boolean paramBoolean1, Boolean paramBoolean2, Boolean paramBoolean3)
  {
    super(com.deviantart.android.sdk.api.model.DVNTStashItem.class);
    itemId = paramLong;
    extSubmission = paramBoolean1;
    extCamera = paramBoolean2;
    extStats = paramBoolean3;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTStashFetchItemRequestV1)paramObject;
    return (Objects.append(itemId, itemId)) && (Objects.append(extSubmission, extSubmission)) && (Objects.append(extCamera, extCamera)) && (Objects.append(extStats, extStats));
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), itemId, extSubmission, extCamera, extStats });
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTStashItem sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).stashFetchItem(paramString, itemId, extSubmission, extCamera, extStats, Boolean.valueOf(true));
  }
}
