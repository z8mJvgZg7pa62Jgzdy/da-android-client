package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.activity.SubmitActivity.IntentBuilder;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;

public class RepostButton
  extends DeviationMenuButton
{
  public RepostButton(Activity paramActivity, DVNTDeviation paramDVNTDeviation)
  {
    super(paramActivity, paramDVNTDeviation);
    init(paramActivity);
  }
  
  public RepostButton(Activity paramActivity, String paramString)
  {
    super(paramActivity, paramString);
    init(paramActivity);
  }
  
  private void init(Activity paramActivity)
  {
    paramActivity = new ImageView(paramActivity);
    paramActivity.setImageResource(2130837835);
    paramActivity.setContentDescription("TapHoldRepost");
    addView(paramActivity, new FrameLayout.LayoutParams(-1, -1));
  }
  
  public void b(DVNTDeviation paramDVNTDeviation)
  {
    TrackerUtil.a((Activity)getContext(), EventKeys.Category.y, "tap_share");
    ((Activity)getContext()).startActivityForResult(new SubmitActivity.IntentBuilder().a(paramDVNTDeviation).a(getContext()), 108);
  }
  
  public boolean b()
  {
    if (i != null)
    {
      b(i);
      return true;
    }
    DVNTCommonAsyncAPI.deviationInfo(t).call(getContext(), new RepostButton.1(this));
    return true;
  }
}
