package com.deviantart.android.damobile.view.dialogs;

import android.app.DialogFragment;

public class DABaseDialog
  extends DialogFragment
{
  protected DABaseDialog.DADialogListener d;
  
  public DABaseDialog() {}
  
  public void b(DABaseDialog.DADialogListener paramDADialogListener)
  {
    d = paramDADialogListener;
  }
}
