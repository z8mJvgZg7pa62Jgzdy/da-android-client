package com.deviantart.android.damobile.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class JournalThumbView
  extends RelativeLayout
{
  @Bind({2131689943})
  TextView preview;
  @Bind({2131689942})
  TextView title;
  
  public JournalThumbView(Context paramContext, DVNTDeviation paramDVNTDeviation)
  {
    this(paramContext, paramDVNTDeviation, true);
  }
  
  public JournalThumbView(Context paramContext, DVNTDeviation paramDVNTDeviation, boolean paramBoolean)
  {
    super(paramContext);
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(2130968725, this, true);
    ButterKnife.bind(this);
    paramContext = Jsoup.get(paramDVNTDeviation.getExcerpt()).get();
    preview.setText(paramContext);
    if (paramBoolean)
    {
      title.setText(paramDVNTDeviation.getTitle());
      return;
    }
    title.setVisibility(8);
  }
}
