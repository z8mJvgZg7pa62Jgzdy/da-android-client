package com.deviantart.android.damobile.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.deviantart.android.damobile.fragment.CommentFragment;
import com.deviantart.android.damobile.util.CommentType;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.threaditem.CommentItem;

public class CommentActivity
  extends BaseActivity
{
  public CommentActivity() {}
  
  public void onBackPressed()
  {
    finish();
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    Object localObject1 = null;
    super.onCreate(paramBundle);
    setContentView(2130968602);
    setSupportActionBar((Toolbar)findViewById(2131689845));
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    if (paramBundle != null) {
      return;
    }
    Object localObject2 = getIntent().getExtras();
    if (localObject2 != null)
    {
      paramBundle = (CommentType)((Bundle)localObject2).getSerializable("comment_type");
      localObject1 = (CommentItem)((Bundle)localObject2).getSerializable("reply_comment");
      localObject2 = ((BaseBundle)localObject2).getString("comment_itemid");
    }
    for (;;)
    {
      ScreenFlowManager.get(this, CommentFragment.a(paramBundle, (String)localObject2, (CommentItem)localObject1), "comment", false);
      return;
      localObject2 = null;
      Object localObject3 = null;
      paramBundle = (Bundle)localObject1;
      localObject1 = localObject3;
    }
  }
  
  public boolean onSupportNavigateUp()
  {
    onBackPressed();
    return true;
  }
}
