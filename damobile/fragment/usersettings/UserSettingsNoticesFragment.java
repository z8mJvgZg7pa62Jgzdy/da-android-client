package com.deviantart.android.damobile.fragment.usersettings;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.deviantart.android.damobile.activity.UserSettingsActivity;

public class UserSettingsNoticesFragment
  extends UserSettingsBaseFragment
{
  public UserSettingsNoticesFragment() {}
  
  private static void setupWebView(WebSettings paramWebSettings)
  {
    paramWebSettings.setCacheMode(2);
    paramWebSettings.setAppCacheEnabled(false);
    paramWebSettings.setBlockNetworkImage(true);
    paramWebSettings.setLoadsImagesAutomatically(true);
    paramWebSettings.setGeolocationEnabled(false);
    paramWebSettings.setNeedInitialFocus(false);
    paramWebSettings.setSaveFormData(false);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = new WebView(getActivity());
    setupWebView(paramLayoutInflater.getSettings());
    paramViewGroup = new DisplayMetrics();
    getActivity().getWindowManager().getDefaultDisplay().getMetrics(paramViewGroup);
    paramLayoutInflater.setInitialScale((int)(widthPixels / 560.0F * 100.0F));
    paramLayoutInflater.loadUrl("file:///android_asset/docs/notices.html");
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
  }
  
  public void onResume()
  {
    super.onResume();
    ((UserSettingsActivity)getActivity()).onProgressChanged(2131231293);
  }
}
