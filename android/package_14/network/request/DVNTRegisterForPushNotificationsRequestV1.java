package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTRegisterForPushNotificationsRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  String registrationId;
  
  public DVNTRegisterForPushNotificationsRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    registrationId = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTRegisterForPushNotificationsRequestV1 localDVNTRegisterForPushNotificationsRequestV1 = (DVNTRegisterForPushNotificationsRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(registrationId, registrationId).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(registrationId).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).registerForPushNotifications(paramString, registrationId);
  }
}
