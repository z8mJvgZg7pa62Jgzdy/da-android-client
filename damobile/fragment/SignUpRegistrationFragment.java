package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Resources;
import android.os.BaseBundle;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.activity.HomeActivity;
import com.deviantart.android.damobile.activity.HomeActivity.HomeActivityPages;
import com.deviantart.android.damobile.util.DAClickableSpan;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import samples.grantland.widget.AutofitHelper;

public class SignUpRegistrationFragment
  extends MultiStepFragment
{
  private String a;
  @Bind({2131689806})
  RelativeLayout afterRegistrationView;
  private boolean b;
  private String c;
  @Bind({2131689692})
  TextView changeEmailAddress;
  @Bind({2131689805})
  TextView creatingAccount;
  private String d;
  @Bind({2131689808})
  TextView emailTextView;
  private String f;
  private String g;
  @Bind({2131689809})
  TextView resendVerification;
  @Bind({2131689553})
  TextView title;
  private boolean w = false;
  
  public SignUpRegistrationFragment() {}
  
  private void c()
  {
    DVNTAsyncAPI.createAccount(f, a, c, d, g, b, true, true).call(getActivity(), new SignUpRegistrationFragment.3(this));
  }
  
  private void init(String paramString1, TextView paramTextView, String paramString2, View.OnClickListener paramOnClickListener)
  {
    paramString1 = Pattern.compile(paramString1).matcher(paramString2);
    paramString2 = new SpannableStringBuilder(paramString2);
    while (paramString1.find()) {
      paramString2.setSpan(new DAClickableSpan(getActivity(), paramOnClickListener), paramString1.start(), paramString1.end(), 33);
    }
    paramTextView.setText(paramString2);
    paramTextView.setMovementMethod(LinkMovementMethod.getInstance());
  }
  
  public void b()
  {
    com.deviantart.android.damobile.util.UserUtils.a = f;
    com.deviantart.android.damobile.util.UserUtils.c = a;
    com.deviantart.android.damobile.util.UserUtils.e = false;
    com.deviantart.android.damobile.util.UserUtils.h = true;
    title.setTextColor(getResources().getColor(2131558484));
    title.setText(2131230804);
    creatingAccount.setVisibility(8);
    afterRegistrationView.setVisibility(0);
    emailTextView.setText(a);
    w = true;
  }
  
  public void b(Bundle paramBundle)
  {
    f = paramBundle.getString("username");
    c = paramBundle.getString("password");
    d = paramBundle.getString("birth_date");
    g = paramBundle.getString("sex");
    a = paramBundle.getString("email");
    b = paramBundle.getBoolean("send_news");
    c();
  }
  
  public void clickGoToProfile()
  {
    Intent localIntent = new Intent(getActivity(), HomeActivity.class);
    localIntent.putExtra("home_start_page", HomeActivity.HomeActivityPages.b);
    localIntent.putExtra("home_profile_username", f);
    localIntent.putExtra("home_profile_is_owner", true);
    localIntent.putExtra("home_profile_add_discovery", true);
    startActivity(localIntent);
    getActivity().setResult(-1);
    getActivity().finish();
  }
  
  public void clickStartBrowsing()
  {
    startActivity(new Intent(getActivity(), HomeActivity.class));
    getActivity().setResult(-1);
    getActivity().finish();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setRetainInstance(true);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = (LinearLayout)paramLayoutInflater.inflate(2130968680, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    init("Change Email Address", changeEmailAddress, getString(2131231410), new SignUpRegistrationFragment.1(this));
    init("Resend Email", resendVerification, getString(2131231418), new SignUpRegistrationFragment.2(this));
    AutofitHelper.doInit(emailTextView);
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public void saveAndExit()
  {
    if (w)
    {
      getActivity().setResult(1);
      getActivity().finish();
      return;
    }
    Toast.makeText(getActivity(), 2131230810, 0).show();
  }
  
  public Bundle updateSettings()
  {
    return new Bundle();
  }
}
