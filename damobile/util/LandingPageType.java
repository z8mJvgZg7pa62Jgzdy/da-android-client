package com.deviantart.android.damobile.util;

import android.content.Context;
import java.util.ArrayList;

public enum LandingPageType
{
  private int descr;
  
  private LandingPageType(int paramInt)
  {
    descr = paramInt;
  }
  
  public static LandingPageType findByName(Context paramContext, String paramString)
  {
    LandingPageType[] arrayOfLandingPageType = values();
    int j = arrayOfLandingPageType.length;
    int i = 0;
    while (i < j)
    {
      LandingPageType localLandingPageType = arrayOfLandingPageType[i];
      if (paramContext.getString(localLandingPageType.getTitle()).equals(paramString)) {
        return localLandingPageType;
      }
      i += 1;
    }
    return null;
  }
  
  public static LandingPageType get(int paramInt)
  {
    LandingPageType[] arrayOfLandingPageType = values();
    int i;
    if (paramInt < arrayOfLandingPageType.length)
    {
      i = paramInt;
      if (paramInt >= 0) {}
    }
    else
    {
      i = 0;
    }
    return arrayOfLandingPageType[i];
  }
  
  public static CharSequence[] getTitles(Context paramContext)
  {
    ArrayList localArrayList = new ArrayList();
    LandingPageType[] arrayOfLandingPageType = values();
    int j = arrayOfLandingPageType.length;
    int i = 0;
    while (i < j)
    {
      localArrayList.add(paramContext.getString(arrayOfLandingPageType[i].getTitle()));
      i += 1;
    }
    return (CharSequence[])localArrayList.toArray(new CharSequence[localArrayList.size()]);
  }
  
  public int getTitle()
  {
    return descr;
  }
}
