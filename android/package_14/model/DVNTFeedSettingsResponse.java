package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTFeedSettingsResponse
  implements Serializable
{
  @SerializedName("collections")
  boolean isShowingCollections;
  @SerializedName("deviations")
  boolean isShowingDeviations;
  @SerializedName("group_deviations")
  boolean isShowingGroupDeviations;
  @SerializedName("journals")
  boolean isShowingJournals;
  @SerializedName("statuses")
  boolean isShowingStatuses;
  
  public DVNTFeedSettingsResponse() {}
  
  public boolean isShowingCollections()
  {
    return isShowingCollections;
  }
  
  public boolean isShowingDeviations()
  {
    return isShowingDeviations;
  }
  
  public boolean isShowingGroupDeviations()
  {
    return isShowingGroupDeviations;
  }
  
  public boolean isShowingJournals()
  {
    return isShowingJournals;
  }
  
  public boolean isShowingStatuses()
  {
    return isShowingStatuses;
  }
  
  public void setShowingCollections(boolean paramBoolean)
  {
    isShowingCollections = paramBoolean;
  }
  
  public void setShowingDeviations(boolean paramBoolean)
  {
    isShowingDeviations = paramBoolean;
  }
  
  public void setShowingGroupDeviations(boolean paramBoolean)
  {
    isShowingGroupDeviations = paramBoolean;
  }
  
  public void setShowingJournals(boolean paramBoolean)
  {
    isShowingJournals = paramBoolean;
  }
  
  public void setShowingStatuses(boolean paramBoolean)
  {
    isShowingStatuses = paramBoolean;
  }
}
