package com.deviantart.android.damobile.util;

import android.content.Context;
import com.deviantart.android.damobile.stream.Stream;

public abstract interface DeviationTorpedoPreviewItem
{
  public abstract String a();
  
  public abstract String a(Context paramContext);
  
  public abstract String b();
  
  public abstract Stream c();
  
  public abstract String f();
  
  public abstract String format();
  
  public abstract String getMain();
  
  public abstract String getPropertyName();
  
  public abstract String getRawValue();
  
  public abstract String i();
  
  public abstract boolean isSystem();
}
