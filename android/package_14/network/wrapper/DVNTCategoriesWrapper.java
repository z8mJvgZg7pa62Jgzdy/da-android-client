package com.deviantart.android.android.package_14.network.wrapper;

import com.google.gson.annotations.SerializedName;

public class DVNTCategoriesWrapper<T>
{
  @SerializedName("categories")
  private T categories;
  
  public DVNTCategoriesWrapper() {}
  
  public Object getCategories()
  {
    return categories;
  }
}
