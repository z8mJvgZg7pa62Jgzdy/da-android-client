package com.deviantart.android.android.package_14.config;

public class DVNTAPIConfig
  extends DVNTAbstractAPIConfig
{
  private static final String VERSION_RCONLY_SUFFIX = "-rconly";
  private Boolean showRCContent;
  private DVNTAbstractAPIConfig.TempBuildType tempBuildType;
  private String userAgent;
  
  public DVNTAPIConfig() {}
  
  public String buildRequestVersion()
  {
    String str2 = super.buildRequestVersion();
    String str1 = str2;
    if (showRCContent != null)
    {
      str1 = str2;
      if (showRCContent.booleanValue()) {
        str1 = str2 + "-rconly";
      }
    }
    return str1;
  }
  
  public Boolean getShowRCContent()
  {
    return showRCContent;
  }
  
  public DVNTAbstractAPIConfig.TempBuildType getTempBuildType()
  {
    return tempBuildType;
  }
  
  public String getUserAgent()
  {
    return userAgent;
  }
  
  public void setShowRCContent(Boolean paramBoolean)
  {
    showRCContent = paramBoolean;
  }
  
  public void setTempBuildType(DVNTAbstractAPIConfig.TempBuildType paramTempBuildType)
  {
    tempBuildType = paramTempBuildType;
  }
  
  public void setUserAgent(String paramString)
  {
    userAgent = paramString;
  }
}
