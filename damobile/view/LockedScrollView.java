package com.deviantart.android.damobile.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;

public class LockedScrollView
  extends ScrollView
{
  public LockedScrollView(Context paramContext)
  {
    super(paramContext);
    setUpListView();
  }
  
  public LockedScrollView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setUpListView();
  }
  
  public LockedScrollView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    setUpListView();
  }
  
  private void setUpListView()
  {
    setVerticalScrollBarEnabled(false);
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    return false;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    return false;
  }
}
