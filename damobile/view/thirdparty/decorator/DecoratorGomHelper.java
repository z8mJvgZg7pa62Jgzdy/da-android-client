package com.deviantart.android.damobile.view.thirdparty.decorator;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;

public class DecoratorGomHelper
{
  private static final HashMap<com.deviantart.android.damobile.view.gom.decorator.DecoratorType, com.deviantart.android.damobile.view.gom.decorator.DecoratorGom> c = new HashMap();
  private ArrayList<com.deviantart.android.damobile.view.gom.decorator.DecoratorType> a = new ArrayList();
  
  public DecoratorGomHelper(DecoratorType... paramVarArgs)
  {
    a.addAll(Arrays.asList(paramVarArgs));
  }
  
  public static RecyclerView.ViewHolder b(DecoratorViewHolder paramDecoratorViewHolder)
  {
    RecyclerView.ViewHolder localViewHolder = paramDecoratorViewHolder.c();
    paramDecoratorViewHolder = localViewHolder;
    if ((localViewHolder instanceof DecoratorViewHolder)) {
      paramDecoratorViewHolder = b((DecoratorViewHolder)localViewHolder);
    }
    return paramDecoratorViewHolder;
  }
  
  private static DecoratorGom b(DecoratorType paramDecoratorType)
  {
    if (c.containsKey(paramDecoratorType)) {
      return (DecoratorGom)c.get(paramDecoratorType);
    }
    switch (DecoratorGomHelper.1.d[paramDecoratorType.ordinal()])
    {
    default: 
      break;
    }
    for (;;)
    {
      return (DecoratorGom)c.get(paramDecoratorType);
      c.put(paramDecoratorType, new DeviationBottomBarDecoratorGom());
      continue;
      c.put(paramDecoratorType, new DailyDeviationInfoDecoratorGom());
      continue;
      c.put(paramDecoratorType, new WatchRecoDecoratorGom());
    }
  }
  
  public RecyclerView.ViewHolder a(Context paramContext, ViewGroup paramViewGroup, RecyclerView.ViewHolder paramViewHolder)
  {
    Iterator localIterator = a.iterator();
    while (localIterator.hasNext()) {
      paramViewHolder = b((DecoratorType)localIterator.next()).a(paramContext, paramViewGroup, paramViewHolder);
    }
    return paramViewHolder;
  }
  
  public void a(Context paramContext, DecoratorViewHolder paramDecoratorViewHolder, Object paramObject)
  {
    ListIterator localListIterator = a.listIterator(a.size());
    while (localListIterator.hasPrevious())
    {
      DecoratorGom localDecoratorGom = b((DecoratorType)localListIterator.previous());
      if (localDecoratorGom.getComponentType().isAssignableFrom(paramObject.getClass()))
      {
        localDecoratorGom.f(paramContext, paramDecoratorViewHolder, paramObject);
        if (!(paramDecoratorViewHolder.c() instanceof DecoratorViewHolder)) {
          return;
        }
        paramDecoratorViewHolder = (DecoratorViewHolder)paramDecoratorViewHolder.c();
      }
    }
  }
}
