package com.deviantart.android.damobile.util;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.text.style.ReplacementSpan;

public class TextOverlaySpan
  extends ReplacementSpan
{
  private CharSequence a;
  private Integer b;
  private Integer c;
  
  public TextOverlaySpan(CharSequence paramCharSequence, Integer paramInteger1, Integer paramInteger2)
  {
    a = paramCharSequence;
    c = paramInteger1;
    b = paramInteger2;
  }
  
  private static void a(Canvas paramCanvas, float paramFloat, int paramInt1, Paint paramPaint, CharSequence paramCharSequence, int paramInt2, int paramInt3, Integer paramInteger)
  {
    if (paramInteger != null)
    {
      int i = paramPaint.getColor();
      paramPaint.setColor(paramInteger.intValue());
      paramCanvas.drawText(paramCharSequence, paramInt2, paramInt3, paramFloat, paramInt1, paramPaint);
      paramPaint.setColor(i);
      return;
    }
    paramCanvas.drawText(paramCharSequence, paramInt2, paramInt3, paramFloat, paramInt1, paramPaint);
  }
  
  public void draw(Canvas paramCanvas, CharSequence paramCharSequence, int paramInt1, int paramInt2, float paramFloat, int paramInt3, int paramInt4, int paramInt5, Paint paramPaint)
  {
    if ("?".equals(paramCharSequence.subSequence(paramInt1, paramInt2).toString()))
    {
      a(paramCanvas, paramFloat, paramInt4, paramPaint, paramCharSequence, paramInt1, paramInt2, null);
      return;
    }
    a(paramCanvas, paramFloat, paramInt4, paramPaint, a, 0, a.length(), c);
    a(paramCanvas, paramFloat, paramInt4, paramPaint, paramCharSequence, paramInt1, paramInt2, b);
  }
  
  public int getSize(Paint paramPaint, CharSequence paramCharSequence, int paramInt1, int paramInt2, Paint.FontMetricsInt paramFontMetricsInt)
  {
    if (paramFontMetricsInt != null)
    {
      Paint.FontMetricsInt localFontMetricsInt = paramPaint.getFontMetricsInt();
      ascent = ascent;
      bottom = bottom;
      descent = ascent;
      top = top;
      leading = leading;
    }
    return (int)paramPaint.measureText(paramCharSequence, paramInt1, paramInt2);
  }
}
