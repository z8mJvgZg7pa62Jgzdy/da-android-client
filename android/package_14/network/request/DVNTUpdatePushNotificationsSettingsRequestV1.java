package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.model.DVNTPushNotificationsSettings;
import com.deviantart.android.android.package_14.model.DVNTPushNotificationsSettings.DVNTSendPushNotificationsSettings;
import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.HashMap;

public class DVNTUpdatePushNotificationsSettingsRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  public static final String SEND_COMMENT_REPLY = "send[comment_reply]";
  public static final String SEND_FAVOURITE = "send[favourite]";
  public static final String SEND_MENTION = "send[mention]";
  public static final String SEND_NOTE = "send[note]";
  public static final String SEND_WATCHER = "send[watcher]";
  HashMap<String, Object> settingsToUpdate = new HashMap();
  
  public DVNTUpdatePushNotificationsSettingsRequestV1(DVNTPushNotificationsSettings paramDVNTPushNotificationsSettings)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    if ((paramDVNTPushNotificationsSettings != null) && (paramDVNTPushNotificationsSettings.getSendSettings() != null))
    {
      settingsToUpdate.put("send[note]", paramDVNTPushNotificationsSettings.getSendSettings().getNote());
      settingsToUpdate.put("send[comment_reply]", paramDVNTPushNotificationsSettings.getSendSettings().getCommentReply());
      settingsToUpdate.put("send[watcher]", paramDVNTPushNotificationsSettings.getSendSettings().getWatcher());
      settingsToUpdate.put("send[mention]", paramDVNTPushNotificationsSettings.getSendSettings().getMention());
      settingsToUpdate.put("send[favourite]", paramDVNTPushNotificationsSettings.getSendSettings().getFavourite());
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()) && (super.equals(paramObject)))
    {
      paramObject = (DVNTUpdatePushNotificationsSettingsRequestV1)paramObject;
      return Objects.append(settingsToUpdate, settingsToUpdate);
    }
    return false;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), settingsToUpdate });
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).updatePushNotificationsSettings(paramString, settingsToUpdate);
  }
}
