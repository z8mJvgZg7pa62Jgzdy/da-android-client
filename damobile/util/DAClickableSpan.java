package com.deviantart.android.damobile.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.View.OnClickListener;

public class DAClickableSpan
  extends ClickableSpan
{
  View.OnClickListener mClickListener;
  int mTextColor;
  
  public DAClickableSpan(int paramInt, View.OnClickListener paramOnClickListener)
  {
    mTextColor = paramInt;
    mClickListener = paramOnClickListener;
  }
  
  public DAClickableSpan(Context paramContext, View.OnClickListener paramOnClickListener)
  {
    mTextColor = paramContext.getResources().getColor(2131558484);
    mClickListener = paramOnClickListener;
  }
  
  public void onClick(View paramView)
  {
    mClickListener.onClick(paramView);
  }
  
  public void updateDrawState(TextPaint paramTextPaint)
  {
    paramTextPaint.setColor(mTextColor);
    paramTextPaint.setUnderlineText(false);
  }
}
