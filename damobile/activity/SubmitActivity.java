package com.deviantart.android.damobile.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTUserStatus;
import com.deviantart.android.damobile.fragment.SubmitMainFragment.InstanceBuilder;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.ShareUtils;
import com.deviantart.android.damobile.util.submit.SimpleTextStatus;
import com.deviantart.android.damobile.util.submit.Submission;
import com.deviantart.android.damobile.util.submit.SubmitType;
import com.deviantart.datoolkit.logger.DVNTLog;
import java.util.Iterator;
import java.util.Set;

public class SubmitActivity
  extends LoggedInActivity
{
  private static final String[] title = { "repost_deviation_info", "repost_status_info", "android.intent.extra.TEXT", "android.intent.extra.SUBJECT", "mode" };
  
  public SubmitActivity() {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if ((getIntent() != null) && (getIntent().getExtras() != null) && (!getIntent().getExtras().isEmpty()))
    {
      paramBundle = getIntent().getExtras().keySet().iterator();
      while (paramBundle.hasNext())
      {
        String str = (String)paramBundle.next();
        DVNTLog.get("submit extra : [" + str + "=" + getIntent().getExtras().get(str) + "]", new Object[0]);
      }
    }
    setContentView(2130968609);
    setSupportActionBar((Toolbar)findViewById(2131689845));
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    if (Submission.getInstance().isEmpty())
    {
      Toast.makeText(this, 2131230955, 0).show();
      finish();
      return;
    }
    ButterKnife.bind(this);
    if (getIntent().hasExtra("repost_deviation_info"))
    {
      ScreenFlowManager.get(this, new SubmitMainFragment.InstanceBuilder().a((com.deviantart.android.android.package_14.model.DVNTDeviation)ShareUtils.fromJson(getIntent().getStringExtra("repost_deviation_info"), com.deviantart.android.sdk.api.model.DVNTDeviation.class)).a(), "submit_fragment", false);
      return;
    }
    if (getIntent().hasExtra("repost_status_info"))
    {
      ScreenFlowManager.get(this, new SubmitMainFragment.InstanceBuilder().a((DVNTUserStatus)getIntent().getSerializableExtra("repost_status_info")).a(), "submit_fragment", false);
      return;
    }
    if (getIntent().hasExtra("android.intent.extra.TEXT"))
    {
      paramBundle = new SimpleTextStatus();
      if (getIntent().hasExtra("android.intent.extra.SUBJECT")) {
        paramBundle.goTo(getIntent().getStringExtra("android.intent.extra.SUBJECT"));
      }
      paramBundle.d(getIntent().getStringExtra("android.intent.extra.TEXT"));
      ScreenFlowManager.get(this, new SubmitMainFragment.InstanceBuilder().a(paramBundle).a(SubmitType.c.getText()).a(), "submit_fragment", false);
      return;
    }
    if (getIntent().hasExtra("android.intent.extra.STREAM"))
    {
      ScreenFlowManager.get(this, new SubmitMainFragment.InstanceBuilder().a((Uri)getIntent().getExtras().get("android.intent.extra.STREAM")).a(SubmitType.a.getText()).a(), "submit_fragment", false);
      return;
    }
    paramBundle = null;
    if (getIntent().hasExtra("mode")) {
      paramBundle = getIntent().getStringExtra("mode");
    }
    ScreenFlowManager.get(this, new SubmitMainFragment.InstanceBuilder().a(paramBundle).a(), "submit_fragment", false);
  }
  
  public boolean onSupportNavigateUp()
  {
    onBackPressed();
    return true;
  }
}
