package com.deviantart.android.android.package_14.network.request;

import com.google.common.reflect.TypeToken;
import java.util.HashMap;

public abstract interface GroupableRequest
  extends VersionedRequest
{
  public abstract HashMap getGETArguments();
  
  public abstract String getGroupEndpoint();
  
  public abstract HashMap getPOSTArguments();
  
  public abstract Class getResultType();
  
  public abstract TypeToken getWrapperTypeToken();
}
