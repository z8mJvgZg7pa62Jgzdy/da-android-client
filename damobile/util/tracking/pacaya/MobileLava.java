package com.deviantart.android.damobile.util.tracking.pacaya;

import android.content.Context;
import com.deviantart.pacaya.Pacaya;
import com.deviantart.pacaya.Pacaya.Builder;
import com.deviantart.sdk.log.DVNTTopicEvent;

public class MobileLava
{
  private static Pacaya<DVNTTopicEvent> j;
  
  public MobileLava() {}
  
  public static void a(Context paramContext)
  {
    j = new Pacaya.Builder().b(new SqlBriteDataService(paramContext)).a(new LavaNetworkService()).a(new LavaReportService()).b(1000).a(100000L).b(4096000L).a(100000).init();
  }
  
  public static Pacaya e()
  {
    return j;
  }
}
