package com.deviantart.android.damobile.view.opt;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ProgressBar;
import com.deviantart.android.damobile.adapter.HeaderFooterRecyclerViewAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.Stream.Notifiable;
import com.deviantart.android.damobile.util.mc.ItemPendable;
import com.deviantart.android.damobile.util.opt.ItemDeleteHelperBase;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public abstract class MCListAdapterBase<ITEM_TYPE, PAGE_TYPE extends ItemPendable>
  extends HeaderFooterRecyclerViewAdapter
{
  private int b = 0;
  protected Stream<ITEM_TYPE> c;
  protected boolean i = false;
  protected int k = 0;
  private boolean p = false;
  
  public MCListAdapterBase() {}
  
  private int execute(int paramInt)
  {
    Object localObject1 = next();
    int m = 0;
    HashMap localHashMap = new HashMap();
    int j = k;
    Object localObject2;
    if (j < paramInt)
    {
      localObject2 = c.get(j);
      if ((get().containsKey(localObject2)) || (((ItemDeleteHelperBase)localObject1).getName().contains(localObject2)) || ((((ItemDeleteHelperBase)localObject1).getValue() != null) && (((ItemDeleteHelperBase)localObject1).getValue().equals(localObject2)))) {
        localHashMap.put(localObject2, Integer.valueOf(j));
      }
      for (;;)
      {
        j += 1;
        break;
        m += 1;
      }
    }
    localObject1 = localHashMap.entrySet().iterator();
    while (((Iterator)localObject1).hasNext())
    {
      localObject2 = (Map.Entry)((Iterator)localObject1).next();
      c.close(((Integer)((Map.Entry)localObject2).getValue()).intValue());
    }
    get().putAll(localHashMap);
    return m;
  }
  
  public void a()
  {
    int j = k;
    try
    {
      add(0, j);
      if (c != null) {
        c.a();
      }
      k = 0;
      i = true;
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        new Handler().post(MCListAdapterBase..Lambda.3.c(this));
      }
    }
  }
  
  protected void a(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    paramViewHolder = (MCListAdapterBase.LoadingViewHolder)paramViewHolder;
    if (p)
    {
      progressView.setVisibility(0);
      return;
    }
    progressView.setVisibility(8);
  }
  
  public void a(Object paramObject)
  {
    int j = c.get().indexOf(paramObject);
    if (j == -1) {
      return;
    }
    get().put(paramObject, Integer.valueOf(j));
    try
    {
      add(j);
      k -= 1;
      c.close(j);
      return;
    }
    catch (Exception paramObject)
    {
      for (;;)
      {
        new Handler().post(MCListAdapterBase..Lambda.5.c(this));
      }
    }
  }
  
  protected int add()
  {
    return 1;
  }
  
  public void b()
  {
    if (c == null) {
      return;
    }
    if (i) {
      i = false;
    }
    try
    {
      c();
      int m = c.size();
      int j = k;
      if ((m > 0) && (j < m))
      {
        m = get(m, j);
        k += m;
        try
        {
          append(j, m);
          return;
        }
        catch (Exception localException1)
        {
          new Handler().post(MCListAdapterBase..Lambda.2.c(this));
          return;
        }
      }
    }
    catch (Exception localException2)
    {
      for (;;)
      {
        new Handler().post(MCListAdapterBase..Lambda.1.c(this));
      }
    }
  }
  
  public void b(Object paramObject)
  {
    Integer localInteger = (Integer)get().remove(paramObject);
    if (localInteger == null) {
      return;
    }
    localInteger = Integer.valueOf(Math.max(0, Math.min(k, localInteger.intValue())));
    k += 1;
    c.a(localInteger.intValue(), paramObject);
    try
    {
      close(localInteger.intValue());
      return;
    }
    catch (Exception paramObject)
    {
      new Handler().post(MCListAdapterBase..Lambda.6.c(this));
    }
  }
  
  protected RecyclerView.ViewHolder c(ViewGroup paramViewGroup, int paramInt)
  {
    return new MCListAdapterBase.LoadingViewHolder(LayoutInflater.from(paramViewGroup.getContext()).inflate(2130968732, paramViewGroup, false));
  }
  
  protected void c(RecyclerView.ViewHolder paramViewHolder, int paramInt) {}
  
  public void clear(boolean paramBoolean)
  {
    p = paramBoolean;
    getItemCount();
    try
    {
      e(0);
      return;
    }
    catch (Exception localException)
    {
      new Handler().post(MCListAdapterBase..Lambda.4.c(this));
    }
  }
  
  public int e()
  {
    return k;
  }
  
  protected int get(int paramInt1, int paramInt2)
  {
    if (((get() == null) || (get().isEmpty())) && (next().getName().isEmpty()) && (next().getValue() == null)) {
      return paramInt1 - paramInt2;
    }
    return execute(paramInt1);
  }
  
  public Object get(int paramInt)
  {
    if (paramInt >= c.size()) {
      return null;
    }
    return c.get(paramInt);
  }
  
  protected abstract HashMap get();
  
  protected abstract ItemDeleteHelperBase next();
  
  protected RecyclerView.ViewHolder onCreateView(ViewGroup paramViewGroup, int paramInt)
  {
    paramViewGroup = new View(paramViewGroup.getContext());
    paramViewGroup.setLayoutParams(new ViewGroup.LayoutParams(-1, b));
    return new MCListAdapterBase.1(this, paramViewGroup);
  }
  
  public void putShort(int paramInt)
  {
    b = paramInt;
  }
  
  protected int size()
  {
    if (b != 0) {
      return 1;
    }
    return 0;
  }
  
  public void write(Context paramContext, Stream.Notifiable paramNotifiable, boolean paramBoolean)
  {
    if (c == null) {
      return;
    }
    c.add(paramContext, paramNotifiable, paramBoolean);
  }
}
