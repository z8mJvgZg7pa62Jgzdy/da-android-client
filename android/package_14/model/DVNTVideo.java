package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTVideo
  implements Serializable
{
  private Integer duration;
  @SerializedName("filesize")
  private Integer fileSize;
  private String quality;
  @SerializedName("src")
  private String source;
  
  public DVNTVideo() {}
  
  public Integer getDuration()
  {
    return duration;
  }
  
  public Integer getFileSize()
  {
    return fileSize;
  }
  
  public String getQuality()
  {
    return quality;
  }
  
  public String getSource()
  {
    return source;
  }
  
  public void setDuration(Integer paramInteger)
  {
    duration = paramInteger;
  }
  
  public void setFileSize(Integer paramInteger)
  {
    fileSize = paramInteger;
  }
  
  public void setQuality(String paramString)
  {
    quality = paramString;
  }
  
  public void setSource(String paramString)
  {
    source = paramString;
  }
}
