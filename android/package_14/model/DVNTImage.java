package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTImage
  implements Serializable
{
  private Integer height;
  @SerializedName("transparency")
  private Boolean isTransparent;
  private String src;
  private Integer width;
  
  public DVNTImage() {}
  
  public int getHeight()
  {
    return height.intValue();
  }
  
  public String getSrc()
  {
    return src;
  }
  
  public int getWidth()
  {
    return width.intValue();
  }
  
  public Boolean isTransparent()
  {
    return isTransparent;
  }
  
  public void setHeight(Integer paramInteger)
  {
    height = paramInteger;
  }
  
  public void setSrc(String paramString)
  {
    src = paramString;
  }
  
  public void setWidth(Integer paramInteger)
  {
    width = paramInteger;
  }
}
