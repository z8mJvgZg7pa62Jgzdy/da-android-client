package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceAnonymous;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthHelper;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTLoginRequest
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestAnonymous<com.deviantart.android.sdk.api.model.DVNTAccessToken>
{
  private String apiScope;
  private String clientId;
  private String clientSecret;
  private String grantType;
  private String userName;
  private String userPassword;
  
  public DVNTLoginRequest(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    super(com.deviantart.android.sdk.api.model.DVNTAccessToken.class);
    userName = paramString1;
    userPassword = paramString2;
    clientId = paramString3;
    clientSecret = paramString4;
    apiScope = paramString5;
    grantType = paramString6;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTLoginRequest localDVNTLoginRequest = (DVNTLoginRequest)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(userName, userName).append(userPassword, userPassword).append(clientId, clientId).append(clientSecret, clientSecret).append(apiScope, apiScope).append(grantType, grantType).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(userName).append(userPassword).append(clientId).append(clientSecret).append(apiScope).append(grantType).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTAccessToken sendRequest(String paramString)
  {
    return ((DVNTServiceAnonymous)getService()).login(userName, userPassword, new DVNTOAuthHelper().encodeCredentialsForBasicAuthorization(clientId, clientSecret), apiScope, grantType);
  }
}
