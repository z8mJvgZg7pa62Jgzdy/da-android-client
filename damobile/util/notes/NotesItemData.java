package com.deviantart.android.damobile.util.notes;

import com.deviantart.android.android.package_14.model.DVNTNote;
import com.google.common.base.Objects;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class NotesItemData
  implements Serializable
{
  private DVNTNote mContext;
  
  public NotesItemData(DVNTNote paramDVNTNote)
  {
    mContext = paramDVNTNote;
  }
  
  public static ArrayList a(ArrayList paramArrayList)
  {
    ArrayList localArrayList = new ArrayList(paramArrayList.size());
    paramArrayList = paramArrayList.iterator();
    while (paramArrayList.hasNext()) {
      localArrayList.add(new NotesItemData((DVNTNote)paramArrayList.next()));
    }
    return localArrayList;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    paramObject = (NotesItemData)paramObject;
    return Objects.append(mContext.getNoteId(), mContext.getNoteId());
  }
  
  public DVNTNote getContext()
  {
    return mContext;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { mContext.getNoteId() });
  }
}
