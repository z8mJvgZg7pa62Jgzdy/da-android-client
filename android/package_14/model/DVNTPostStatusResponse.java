package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTPostStatusResponse
  implements Serializable
{
  @SerializedName("statusid")
  String statusUUID;
  
  public DVNTPostStatusResponse() {}
  
  public String getStatusUUID()
  {
    return statusUUID;
  }
  
  public void setStatusUUID(String paramString)
  {
    statusUUID = paramString;
  }
}
