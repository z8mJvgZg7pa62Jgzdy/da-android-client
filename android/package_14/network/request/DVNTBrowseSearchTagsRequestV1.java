package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.android.package_14.network.wrapper.DVNTResultsWrapper;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTBrowseSearchTagsRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTSearchedTag.List>
{
  final String tagName;
  
  public DVNTBrowseSearchTagsRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSearchedTag.List.class);
    tagName = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTBrowseSearchTagsRequestV1 localDVNTBrowseSearchTagsRequestV1 = (DVNTBrowseSearchTagsRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(tagName, tagName).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "browsesearchtags" + tagName;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 3600000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(tagName).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSearchedTag.List sendRequest(String paramString)
  {
    return (com.deviantart.android.android.package_14.model.DVNTSearchedTag.List)((DVNTServiceV1)getService()).browseSearchTags(paramString, tagName).getResults();
  }
}
