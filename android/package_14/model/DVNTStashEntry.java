package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTStashEntry
  implements Serializable
{
  @SerializedName("itemid")
  private Long itemId;
  @SerializedName("metadata")
  private DVNTStashItem metadata;
  @SerializedName("position")
  private Short position;
  @SerializedName("stackid")
  private Long stackId;
  
  public DVNTStashEntry() {}
  
  public Long getItemId()
  {
    return itemId;
  }
  
  public DVNTStashItem getMetadata()
  {
    return metadata;
  }
  
  public Short getPosition()
  {
    return position;
  }
  
  public Long getStackId()
  {
    return stackId;
  }
  
  public void setItemId(Long paramLong)
  {
    itemId = paramLong;
  }
  
  public void setMetadata(DVNTStashItem paramDVNTStashItem)
  {
    metadata = paramDVNTStashItem;
  }
  
  public void setPosition(Short paramShort)
  {
    position = paramShort;
  }
  
  public void setStackId(Long paramLong)
  {
    stackId = paramLong;
  }
}
