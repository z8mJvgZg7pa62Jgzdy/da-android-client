package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTFeedSettingsUpdateRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  boolean includeCollections;
  boolean includeDeviations;
  boolean includeGroupDeviations;
  boolean includeJournals;
  boolean includeStatuses;
  
  public DVNTFeedSettingsUpdateRequestV1(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    includeStatuses = paramBoolean1;
    includeDeviations = paramBoolean2;
    includeJournals = paramBoolean3;
    includeGroupDeviations = paramBoolean4;
    includeCollections = paramBoolean5;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTFeedSettingsUpdateRequestV1 localDVNTFeedSettingsUpdateRequestV1 = (DVNTFeedSettingsUpdateRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(includeStatuses, includeStatuses).append(includeDeviations, includeDeviations).append(includeJournals, includeJournals).append(includeGroupDeviations, includeGroupDeviations).append(includeCollections, includeCollections).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(includeStatuses).append(includeDeviations).append(includeJournals).append(includeGroupDeviations).append(includeCollections).toHashCode();
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).feedSettingsUpdate(paramString, includeStatuses, includeDeviations, includeJournals, includeGroupDeviations, includeCollections);
  }
}
