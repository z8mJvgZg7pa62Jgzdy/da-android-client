package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTTag
  implements Serializable
{
  DVNTAbstractDeviation.List deviations;
  @SerializedName("sponsor")
  String sponsorName;
  @SerializedName("sponsored")
  Boolean sponsored;
  @SerializedName("tag_name")
  String tagName;
  @SerializedName("tracking")
  String tracking;
  
  public DVNTTag() {}
  
  public boolean equals(Object paramObject)
  {
    if ((paramObject == null) || (paramObject.getClass() != com.deviantart.android.sdk.api.model.DVNTTag.class)) {
      return false;
    }
    return ((DVNTTag)paramObject).getTagName().equals(tagName);
  }
  
  public DVNTAbstractDeviation.List getDeviations()
  {
    return deviations;
  }
  
  public String getSponsorName()
  {
    return sponsorName;
  }
  
  public Boolean getSponsored()
  {
    return sponsored;
  }
  
  public String getTagName()
  {
    return tagName;
  }
  
  public String getTracking()
  {
    return tracking;
  }
  
  public void setDeviations(DVNTAbstractDeviation.List paramList)
  {
    deviations = paramList;
  }
  
  public void setSponsorName(String paramString)
  {
    sponsorName = paramString;
  }
  
  public void setSponsored(Boolean paramBoolean)
  {
    sponsored = paramBoolean;
  }
  
  public void setTagName(String paramString)
  {
    tagName = paramString;
  }
  
  public void setTracking(String paramString)
  {
    tracking = paramString;
  }
  
  public String toString()
  {
    return "#" + tagName;
  }
}
