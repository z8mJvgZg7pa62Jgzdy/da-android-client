package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.model.DVNTEnrichedToken;
import com.deviantart.android.android.package_14.network.DVNTRetryPolicy;
import com.deviantart.android.android.package_14.session.DVNTTokenSessionManager;
import com.deviantart.android.android.utils.DVNTUtils;
import com.deviantart.android.sdk.api.network.request.VersionedRequest;
import com.google.common.reflect.TypeToken;
import com.octo.android.robospice.request.SpiceRequest;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class DVNTBaseAsyncRequest<T, V>
  extends RetrofitSpiceRequest<T, V>
  implements VersionedRequest
{
  private Class<T> responseClass;
  private DVNTTokenSessionManager sessionManager;
  private String version;
  private boolean wipeCacheFirst;
  
  protected DVNTBaseAsyncRequest(Class paramClass1, Class paramClass2)
  {
    super(paramClass1, paramClass2);
    setRetryPolicy(new DVNTRetryPolicy());
    responseClass = paramClass1;
  }
  
  private Object cancelAndSendNullTokenRequest()
  {
    cancel();
    return sendRequest(null);
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      paramObject = (DVNTBaseAsyncRequest)paramObject;
      return new EqualsBuilder().append(responseClass, responseClass).append(version, version).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return String.valueOf(hashCode());
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return -1L;
  }
  
  public final String getCacheUniqueId()
  {
    Object localObject = "";
    try
    {
      String str = DVNTUtils.sha1hex(getVersion());
      localObject = str;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
    return getCacheKey() + "-" + (String)localObject;
  }
  
  public Class getResponseClass()
  {
    return responseClass;
  }
  
  public String getVersion()
  {
    return version;
  }
  
  public TypeToken getWrapperTypeToken()
  {
    return null;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().append(responseClass).append(version).toHashCode();
  }
  
  public boolean isCancellable()
  {
    return true;
  }
  
  public Object loadDataFromNetwork()
  {
    if (sessionManager == null) {
      return cancelAndSendNullTokenRequest();
    }
    if (sessionManager.getToken() != null) {
      return sendRequest(sessionManager.getToken().getToken());
    }
    throw new RuntimeException("DOESNT WORK");
  }
  
  public boolean needsGraduation()
  {
    return true;
  }
  
  protected abstract Object sendRequest(String paramString);
  
  public void setTokenSessionManager(DVNTTokenSessionManager paramDVNTTokenSessionManager)
  {
    sessionManager = paramDVNTTokenSessionManager;
  }
  
  public void setVersion(String paramString)
  {
    version = paramString;
  }
  
  public void setWipeCacheFirst(boolean paramBoolean)
  {
    wipeCacheFirst = paramBoolean;
  }
  
  public boolean shouldWipeCacheFirst()
  {
    return wipeCacheFirst;
  }
}
