package com.deviantart.android.damobile.util.tracking.pacaya;

import com.deviantart.upgrade.labs.DVNTTopicEvent;
import org.apache.commons.lang.StringUtils;

public class UxTopicEventCreator
  extends TopicEventCreator
{
  private String[] content = new String[6];
  
  public UxTopicEventCreator()
  {
    super("ux");
    content[0] = "daandroid";
    content[5] = "pageview";
  }
  
  public DVNTTopicEvent b()
  {
    add("e", StringUtils.join(content, ":"));
    return super.b();
  }
  
  public UxTopicEventCreator get(String paramString)
  {
    content[1] = paramString;
    return this;
  }
  
  public UxTopicEventCreator getString(String paramString)
  {
    content[3] = paramString;
    return this;
  }
}
