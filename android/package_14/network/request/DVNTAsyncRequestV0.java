package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.sdk.api.network.service.DVNTServiceV0;

public abstract class DVNTAsyncRequestV0<T>
  extends com.deviantart.android.sdk.api.network.request.DVNTBaseAsyncRequest<T, DVNTServiceV0>
{
  protected DVNTAsyncRequestV0(Class paramClass)
  {
    super(paramClass, DVNTServiceV0.class);
  }
}
