package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;

public class DVNTImageChallengeMedia
  extends DVNTTemplateChallengeMedia
{
  @SerializedName("ratio")
  String ratio;
  
  public DVNTImageChallengeMedia() {}
  
  public String getRatio()
  {
    return ratio;
  }
  
  public void setRatio(String paramString)
  {
    ratio = paramString;
  }
}
