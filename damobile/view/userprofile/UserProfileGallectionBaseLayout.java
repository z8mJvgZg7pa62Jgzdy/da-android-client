package com.deviantart.android.damobile.view.userprofile;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.deviantart.android.damobile.adapter.recyclerview.BaseGallectionAdapter;
import com.deviantart.android.damobile.fragment.UserProfileFragment;
import com.deviantart.android.damobile.view.DAStateRecyclerView;
import com.deviantart.android.damobile.view.EndlessScrollListener;
import com.deviantart.android.damobile.view.ViewState.Helper;

public abstract class UserProfileGallectionBaseLayout
  extends DAStateRecyclerView
  implements EndlessScrollListener
{
  protected String a;
  protected Boolean b;
  protected boolean c;
  
  public UserProfileGallectionBaseLayout(Context paramContext, String paramString, boolean paramBoolean, Boolean paramBoolean1)
  {
    super(paramContext);
    a = paramString;
    c = paramBoolean;
    b = paramBoolean1;
    setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    setPadding(getPaddingLeft() + (int)getResources().getDimension(2131361990), getPaddingTop(), getPaddingRight() + (int)getResources().getDimension(2131361990), getPaddingBottom() + (int)getResources().getDimension(2131361990));
    ViewState.Helper.a(b, paramString, c(paramContext), b(paramContext));
    setAdapter(visitLabel());
    b(UserProfileFragment.init(paramContext));
    setAdapter();
    onViewCreated();
  }
  
  public abstract String b(Context paramContext);
  
  public abstract String c(Context paramContext);
  
  protected abstract String getType();
  
  public void setHasSubmittedDeviation(Boolean paramBoolean)
  {
    b = paramBoolean;
    ((BaseGallectionAdapter)getAdapter()).a(paramBoolean);
  }
  
  public abstract BaseGallectionAdapter visitLabel();
}
