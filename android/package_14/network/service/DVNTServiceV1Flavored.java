package com.deviantart.android.android.package_14.network.service;

import com.deviantart.android.android.package_14.model.DVNTAccountValidateResponse;
import com.deviantart.android.android.package_14.model.DVNTFeedbackCursoredPage;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessages;
import com.deviantart.android.android.package_14.model.DVNTFeedbackStackedPage;
import com.deviantart.android.android.package_14.model.DVNTFeedbackType;
import com.deviantart.android.android.package_14.model.DVNTNote;
import com.deviantart.android.android.package_14.model.DVNTNote.Mark;
import com.deviantart.android.android.package_14.model.DVNTNotes;
import com.deviantart.android.android.package_14.model.DVNTNotesFolders;
import com.deviantart.android.android.package_14.model.DVNTPushNotificationsSettings;
import com.deviantart.android.android.package_14.model.DVNTSendNoteResults;
import com.deviantart.android.android.package_14.model.DVNTStatus;
import com.deviantart.android.android.package_14.model.DVNTSuccess;
import com.deviantart.android.android.package_14.model.DVNTWatchRecommendationResponse;
import com.deviantart.android.android.package_14.network.wrapper.DVNTCategoriesWrapper;
import com.deviantart.android.android.package_14.network.wrapper.DVNTResultsWrapper;
import java.util.List;
import java.util.Map;

public abstract interface DVNTServiceV1Flavored
{
  public static final String VALIDATE_ACCOUNT_ENDPOINT = "/api/v1/oauth2/user/account/validate";
  public static final String VERSION_PREFIX = "/api/v1/oauth2";
  
  public abstract DVNTResultsWrapper aggregate(String paramString1, String paramString2, Boolean paramBoolean);
  
  public abstract DVNTSuccess createAccount(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3);
  
  public abstract DVNTSuccess deleteDeviation(String paramString1, String paramString2);
  
  public abstract DVNTSuccess deleteNotes(String paramString, List paramList);
  
  public abstract DVNTStatus errorUtil(String paramString1, String paramString2);
  
  public abstract DVNTFeedbackCursoredPage getAllFeedbackMessages(String paramString1, String paramString2, Boolean paramBoolean1, String paramString3, Integer paramInteger, Boolean paramBoolean2);
  
  public abstract DVNTCategoriesWrapper getCategoryTree(String paramString1, String paramString2, Boolean paramBoolean);
  
  public abstract DVNTFeedbackMessages getFeedbackMentions(String paramString1, String paramString2, Boolean paramBoolean1, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Boolean paramBoolean2);
  
  public abstract DVNTFeedbackStackedPage getFeedbackMessages(String paramString1, DVNTFeedbackType paramDVNTFeedbackType, String paramString2, Boolean paramBoolean1, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Boolean paramBoolean2);
  
  public abstract DVNTFeedbackMessages getFeedbackMessagesForStack(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Boolean paramBoolean);
  
  public abstract DVNTNote getNote(String paramString1, String paramString2, Boolean paramBoolean);
  
  public abstract DVNTNotesFolders getNotesFolders(String paramString);
  
  public abstract DVNTNotes getNotesInFolder(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2, Boolean paramBoolean);
  
  public abstract DVNTPushNotificationsSettings getPushNotificationsSettings(String paramString);
  
  public abstract DVNTFeedbackMessages getUnstackedFeedbackMessages(String paramString1, DVNTFeedbackType paramDVNTFeedbackType, String paramString2, Boolean paramBoolean1, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3, Boolean paramBoolean2);
  
  public abstract DVNTWatchRecommendationResponse getWatchRecommendations(String paramString1, List paramList, String paramString2, Integer paramInteger);
  
  public abstract DVNTSuccess markNotes(String paramString, List paramList, DVNTNote.Mark paramMark);
  
  public abstract DVNTSuccess moveNotesIntoFolder(String paramString1, List paramList, String paramString2);
  
  public abstract DVNTSuccess recoverAccount(String paramString1, String paramString2, String paramString3);
  
  public abstract DVNTSuccess registerForPushNotifications(String paramString1, String paramString2);
  
  public abstract DVNTSuccess resendVerificationEmail(String paramString1, String paramString2, String paramString3);
  
  public abstract DVNTSendNoteResults sendNotes(String paramString1, List paramList, String paramString2, String paramString3, String paramString4);
  
  public abstract DVNTSuccess unsuggestArtist(String paramString1, String paramString2);
  
  public abstract DVNTSuccess updateAccount(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, Boolean paramBoolean);
  
  public abstract DVNTSuccess updatePushNotificationsSettings(String paramString, Map paramMap);
  
  public abstract DVNTAccountValidateResponse validateAccount(String paramString1, String paramString2, String paramString3, String paramString4);
}
