package com.deviantart.android.damobile.util.notifications;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessage;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessageStack;
import com.deviantart.android.android.package_14.model.DVNTFeedbackSubject;
import com.deviantart.android.android.package_14.model.DVNTGallection;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.package_14.model.DVNTUserStatus;
import com.deviantart.android.damobile.util.DAFont;
import com.squareup.phrase.Phrase;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;

public class NotificationItemText
{
  public NotificationItemText() {}
  
  public static CharSequence a(Context paramContext, NotificationItemData paramNotificationItemData)
  {
    NotificationItemType localNotificationItemType = paramNotificationItemData.b();
    Phrase localPhrase = Phrase.add(paramContext.getString(localNotificationItemType.a()));
    NotificationItemText.TextParam[] arrayOfTextParam = localNotificationItemType.getItem();
    int j = arrayOfTextParam.length;
    int i = 0;
    if (i < j)
    {
      NotificationItemText.TextParam localTextParam = arrayOfTextParam[i];
      CharSequence localCharSequence = null;
      switch (NotificationItemText.1.c[localTextParam.ordinal()])
      {
      default: 
        break;
      }
      for (;;)
      {
        Object localObject = localCharSequence;
        if (localCharSequence == null)
        {
          Log.e("Notifications", "Missing data for text param " + localTextParam);
          localObject = paramContext.getString(2131231213);
        }
        localPhrase.add(localTextParam.b(), build(paramContext, (CharSequence)localObject));
        i += 1;
        break;
        localCharSequence = getPageTitle(paramNotificationItemData);
        continue;
        localCharSequence = b(paramNotificationItemData);
        continue;
        localCharSequence = a(paramNotificationItemData);
        continue;
        localCharSequence = f(paramNotificationItemData);
        continue;
        localCharSequence = getItem(paramNotificationItemData);
        continue;
        localCharSequence = getDescriptor(paramNotificationItemData);
        continue;
        localCharSequence = toString(paramNotificationItemData);
        continue;
        localCharSequence = i(paramNotificationItemData);
        continue;
        localCharSequence = s(paramNotificationItemData);
      }
    }
    try
    {
      paramContext = localPhrase.b();
      return paramContext;
    }
    catch (IllegalArgumentException paramContext)
    {
      throw new RuntimeException("Notification text formatting failed for item type " + localNotificationItemType + ": " + paramContext.getMessage(), paramContext);
    }
  }
  
  private static CharSequence a(NotificationItemData paramNotificationItemData)
  {
    if (paramNotificationItemData.a().getDeviation() == null) {
      return null;
    }
    return paramNotificationItemData.a().getDeviation().getTitle();
  }
  
  private static CharSequence b(NotificationItemData paramNotificationItemData)
  {
    if (paramNotificationItemData.a().getOriginator() == null) {
      return null;
    }
    return paramNotificationItemData.a().getOriginator().getUserName();
  }
  
  private static SpannableString build(Context paramContext, CharSequence paramCharSequence)
  {
    Typeface localTypeface = DAFont.b.get(paramContext);
    SpannableString localSpannableString = new SpannableString(paramCharSequence);
    localSpannableString.setSpan(new ForegroundColorSpan(paramContext.getResources().getColor(2131558522)), 0, paramCharSequence.length(), 33);
    localSpannableString.setSpan(new CalligraphyTypefaceSpan(localTypeface), 0, paramCharSequence.length(), 33);
    return localSpannableString;
  }
  
  private static CharSequence f(NotificationItemData paramNotificationItemData)
  {
    if (paramNotificationItemData.a().getProfile() == null) {
      return null;
    }
    return paramNotificationItemData.a().getProfile().getUserName();
  }
  
  private static CharSequence getDescriptor(NotificationItemData paramNotificationItemData)
  {
    if (paramNotificationItemData.a().getCollection() == null) {
      return null;
    }
    return paramNotificationItemData.a().getCollection().getName();
  }
  
  private static CharSequence getItem(NotificationItemData paramNotificationItemData)
  {
    if ((paramNotificationItemData.a().getStatus() == null) || (paramNotificationItemData.a().getStatus().getAuthor() == null)) {
      return null;
    }
    return paramNotificationItemData.a().getStatus().getAuthor().getUserName();
  }
  
  private static CharSequence getPageTitle(NotificationItemData paramNotificationItemData)
  {
    if (!paramNotificationItemData.get()) {
      return null;
    }
    return Integer.toString(paramNotificationItemData.getString().getCount().intValue());
  }
  
  private static CharSequence i(NotificationItemData paramNotificationItemData)
  {
    if (paramNotificationItemData.a().getSubject() != null) {}
    for (paramNotificationItemData = paramNotificationItemData.a().getSubject(); paramNotificationItemData != null; paramNotificationItemData = null)
    {
      if (paramNotificationItemData.getSubjectProfile() == null) {
        return null;
      }
      return paramNotificationItemData.getSubjectProfile().getUserName();
    }
    return null;
  }
  
  private static CharSequence s(NotificationItemData paramNotificationItemData)
  {
    if (paramNotificationItemData.a().getSubject() != null) {}
    for (paramNotificationItemData = paramNotificationItemData.a().getSubject(); (paramNotificationItemData != null) && (paramNotificationItemData.getSubjectStatus() != null); paramNotificationItemData = null)
    {
      if (paramNotificationItemData.getSubjectStatus().getAuthor() == null) {
        return null;
      }
      return paramNotificationItemData.getSubjectStatus().getAuthor().getUserName();
    }
    return null;
  }
  
  private static CharSequence toString(NotificationItemData paramNotificationItemData)
  {
    if (paramNotificationItemData.a().getSubject() != null) {}
    for (paramNotificationItemData = paramNotificationItemData.a().getSubject(); paramNotificationItemData != null; paramNotificationItemData = null)
    {
      if (paramNotificationItemData.getSubjectDeviation() == null) {
        return null;
      }
      return paramNotificationItemData.getSubjectDeviation().getTitle();
    }
    return null;
  }
}
