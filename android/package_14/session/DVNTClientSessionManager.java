package com.deviantart.android.android.package_14.session;

import android.content.Context;
import com.deviantart.android.android.package_14.config.DVNTAPIConfig;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthHelper;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthPreferencesManager;
import com.octo.android.robospice.SpiceManager;

public class DVNTClientSessionManager
  extends DVNTTokenSessionManager
{
  public DVNTClientSessionManager(DVNTRestTokenService paramDVNTRestTokenService, SpiceManager paramSpiceManager, DVNTOAuthPreferencesManager paramDVNTOAuthPreferencesManager, DVNTOAuthHelper paramDVNTOAuthHelper, DVNTAPIConfig paramDVNTAPIConfig)
  {
    super(paramDVNTRestTokenService, paramSpiceManager, paramDVNTOAuthPreferencesManager, paramDVNTOAuthHelper, paramDVNTAPIConfig);
  }
  
  public boolean callGetNewToken(Context paramContext)
  {
    tokenService.newClientToken(apiConfig.getClientId(), apiConfig.getClientSecret(), new DVNTTokenSessionManager.DVNTAccessTokenCallback(this, paramContext));
    return true;
  }
  
  protected boolean callRefreshToken(Context paramContext)
  {
    return callGetNewToken(paramContext);
  }
  
  protected String getTokenManagerName()
  {
    return "DVNTClientSessionManager";
  }
  
  public String getTokenPreferenceKey()
  {
    return "client_accessToken";
  }
}
