package com.deviantart.android.android.package_14.session;

import android.content.Context;
import com.deviantart.android.android.package_14.config.DVNTAPIConfig;
import com.deviantart.android.android.package_14.network.DVNTRequestManager;
import com.deviantart.android.android.package_14.network.request.DVNTBaseAsyncRequest;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthHelper;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthPreferencesManager;
import com.deviantart.datoolkit.logger.DVNTLog;
import com.google.gson.Gson;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.listener.RequestListener;
import java.lang.ref.WeakReference;
import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public abstract class DVNTTokenSessionManager
  extends DVNTSessionManager
{
  protected DVNTAPIConfig apiConfig;
  protected String format;
  protected boolean isAlreadyBuildingSession = false;
  protected DVNTOAuthHelper oAuthHelper;
  protected DVNTOAuthPreferencesManager oAuthPreferencesManager;
  protected com.deviantart.android.android.package_14.model.DVNTEnrichedToken token;
  protected DVNTRestTokenService tokenService;
  
  public DVNTTokenSessionManager(DVNTRestTokenService paramDVNTRestTokenService, SpiceManager paramSpiceManager, DVNTOAuthPreferencesManager paramDVNTOAuthPreferencesManager, DVNTOAuthHelper paramDVNTOAuthHelper, DVNTAPIConfig paramDVNTAPIConfig)
  {
    super(paramSpiceManager);
    oAuthPreferencesManager = paramDVNTOAuthPreferencesManager;
    oAuthHelper = paramDVNTOAuthHelper;
    tokenService = paramDVNTRestTokenService;
    apiConfig = paramDVNTAPIConfig;
    format = (getTokenManagerName() + " - {}");
    DVNTLog.get(format, new Object[] { "create session manager" });
  }
  
  public void asyncExecute(DVNTBaseAsyncRequest paramDVNTBaseAsyncRequest, RequestListener paramRequestListener)
  {
    paramDVNTBaseAsyncRequest.setTokenSessionManager(this);
    super.asyncExecute(paramDVNTBaseAsyncRequest, paramRequestListener);
  }
  
  public abstract boolean callGetNewToken(Context paramContext);
  
  protected abstract boolean callRefreshToken(Context paramContext);
  
  public void cancelAllRequests()
  {
    Iterator localIterator = DVNTRequestManager.getRequests().values().iterator();
    while (localIterator.hasNext())
    {
      WeakReference localWeakReference = (WeakReference)localIterator.next();
      if ((localWeakReference.get() != null) && (((DVNTBaseAsyncRequest)localWeakReference.get()).isCancellable())) {
        spiceManager.cancel(((DVNTBaseAsyncRequest)localWeakReference.get()).getResponseClass(), ((DVNTBaseAsyncRequest)localWeakReference.get()).getCacheUniqueId());
      }
    }
    DVNTRequestManager.clear();
  }
  
  public void cancelRequest(UUID paramUUID)
  {
    DVNTBaseAsyncRequest localDVNTBaseAsyncRequest = DVNTRequestManager.getRequest(paramUUID);
    if (localDVNTBaseAsyncRequest == null) {
      return;
    }
    spiceManager.cancel(localDVNTBaseAsyncRequest.getResponseClass(), localDVNTBaseAsyncRequest.getCacheUniqueId());
    DVNTRequestManager.remove(paramUUID);
  }
  
  public void clearCache()
  {
    spiceManager.removeAllDataFromCache();
  }
  
  public void clearCache(UUID paramUUID)
  {
    paramUUID = DVNTRequestManager.getRequest(paramUUID);
    if (paramUUID == null) {
      return;
    }
    spiceManager.removeDataFromCache(paramUUID.getResponseClass(), paramUUID.getCacheUniqueId());
  }
  
  public void clearTokenFromPreferences(Context paramContext)
  {
    if (oAuthPreferencesManager.removeKey(paramContext, getTokenPreferenceKey()))
    {
      DVNTLog.append("cleared token from prefs", new Object[0]);
      return;
    }
    DVNTLog.append("could not clear token from prefs", new Object[0]);
  }
  
  public com.deviantart.android.android.package_14.model.DVNTEnrichedToken getToken()
  {
    return token;
  }
  
  protected String getTokenManagerName()
  {
    return "DVNTTokenSessionManager";
  }
  
  public abstract String getTokenPreferenceKey();
  
  public boolean hasTokenExpired()
  {
    return oAuthHelper.hasTokenExpired(token);
  }
  
  public boolean hasTokenValidFormat()
  {
    return (token != null) && (token.getToken() != null);
  }
  
  public void loadTokenFromPreferences(Context paramContext)
  {
    paramContext = oAuthPreferencesManager.loadString(paramContext, getTokenPreferenceKey());
    Gson localGson = new Gson();
    if (paramContext != null)
    {
      DVNTLog.get(format, new Object[] { "found stored token : " + paramContext });
      setToken((com.deviantart.android.android.package_14.model.DVNTEnrichedToken)localGson.fromJson(paramContext, com.deviantart.android.sdk.api.model.DVNTEnrichedToken.class));
      return;
    }
    DVNTLog.get(format, new Object[] { "no stored token could be found" });
  }
  
  public boolean openSession(Context paramContext)
  {
    return openSession(paramContext, true);
  }
  
  public boolean openSession(Context paramContext, boolean paramBoolean)
  {
    DVNTLog.get(">" + format, new Object[] { " Opening session" });
    if (isAlreadyBuildingSession)
    {
      DVNTLog.get(format, new Object[] { "already building session - stop" });
      return paramBoolean;
    }
    if (token == null)
    {
      DVNTLog.get(format, new Object[] { "token is null - loading it from prefs" });
      loadTokenFromPreferences(paramContext);
    }
    if (!hasTokenValidFormat())
    {
      String str2 = format;
      StringBuilder localStringBuilder = new StringBuilder().append("token has invalid format, trying to get a new one :");
      if (token != null) {}
      for (String str1 = token.toString();; str1 = "token is null")
      {
        DVNTLog.get(str2, new Object[] { str1 });
        stop();
        isAlreadyBuildingSession = callGetNewToken(paramContext);
        setToken(null);
        clearTokenFromPreferences(paramContext);
        if ((!isAlreadyBuildingSession) || (!paramBoolean)) {
          break;
        }
        return true;
      }
      return false;
    }
    if (hasTokenExpired())
    {
      DVNTLog.get(format, new Object[] { "token has expired, refreshing" });
      stop();
      isAlreadyBuildingSession = callRefreshToken(paramContext);
      setToken(null);
      clearTokenFromPreferences(paramContext);
      if ((!isAlreadyBuildingSession) || (!paramBoolean)) {
        return false;
      }
    }
    else
    {
      start(paramContext);
    }
    return true;
  }
  
  public void reset()
  {
    DVNTLog.get(format, new Object[] { "reset manager" });
    clearCache();
    stop();
    setToken(null);
  }
  
  public void saveTokenIntoPreferences(Context paramContext)
  {
    String str = new Gson().toJson(token);
    if (!oAuthPreferencesManager.saveString(paramContext, getTokenPreferenceKey(), str))
    {
      DVNTLog.append("could not save token : ", new Object[] { str });
      return;
    }
    DVNTLog.get(format, new Object[] { "saved token as json : " + str });
  }
  
  public DVNTTokenSessionManager setToken(com.deviantart.android.android.package_14.model.DVNTEnrichedToken paramDVNTEnrichedToken)
  {
    DVNTLog.get(format, new Object[] { "token has been set : " + paramDVNTEnrichedToken });
    token = paramDVNTEnrichedToken;
    return this;
  }
}
