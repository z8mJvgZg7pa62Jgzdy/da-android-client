package com.deviantart.android.damobile.pushnotifications;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import com.deviantart.datoolkit.logger.DVNTLog;
import com.google.android.com.ClickListeners.GoogleCloudMessaging;
import java.util.Random;

public class GcmIntentService
  extends IntentService
{
  private static final Uri alert = RingtoneManager.getDefaultUri(2);
  private static final long[] id = { 1000L, 1000L, 1000L };
  
  public GcmIntentService()
  {
    super("GcmIntentService");
  }
  
  public static void showNotification(Context paramContext, Bundle paramBundle)
  {
    PushNotification localPushNotification = PushNotification.b(paramContext, paramBundle);
    if (localPushNotification == null)
    {
      Log.e("GCM", "invalid push notification (type = " + paramBundle + ") : ");
      return;
    }
    paramBundle = (NotificationManager)paramContext.getSystemService("notification");
    PendingIntent localPendingIntent = PendingIntent.getActivity(paramContext, (int)(Math.random() * 1000.0D), localPushNotification.getIntent(), 0);
    String str = localPushNotification.replace();
    paramContext = new NotificationCompat.Builder(paramContext).setSmallIcon(2130837794).setColor(paramContext.getResources().getColor(2131558419)).setAutoCancel(true).setSound(alert).setVibrate(id).setContentTitle(localPushNotification.c()).setStyle(new NotificationCompat.BigTextStyle().bigText(str)).setContentText(str);
    paramContext.setContentIntent(localPendingIntent);
    paramContext = paramContext.build();
    try
    {
      paramBundle.notify(new Random().nextInt(), paramContext);
      return;
    }
    catch (Exception paramContext)
    {
      DVNTLog.append("Error sending notification " + paramContext.getMessage(), new Object[0]);
    }
  }
  
  protected void onHandleIntent(Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    Bundle localBundle = paramIntent.getExtras();
    if (localBundle != null)
    {
      String str1 = GoogleCloudMessaging.getInstance(this).getMessageType(paramIntent);
      String str2 = localBundle.toString();
      Log.i("GCM", "Received : " + str2);
      if (localBundle.isEmpty())
      {
        WakefulBroadcastReceiver.completeWakefulIntent(paramIntent);
        return;
      }
      if ("send_error".equals(str1)) {
        Log.i("GCM", "Message error : " + str2);
      }
      for (;;)
      {
        WakefulBroadcastReceiver.completeWakefulIntent(paramIntent);
        return;
        if ("deleted_messages".equals(str1)) {
          Log.i("GCM", "Message deleted : " + str2);
        } else if ("gcm".equals(str1)) {
          showNotification(this, localBundle);
        }
      }
    }
  }
}
