package com.deviantart.android.damobile.util.torpedo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.widget.GridLayoutManager.LayoutParams;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.adapter.HeaderFooterRecyclerViewAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.DeviationUtils.ProcessMenuAddable;
import com.deviantart.android.damobile.util.GalleryType;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.view.ViewState;
import com.deviantart.android.damobile.view.ViewState.Helper;
import com.deviantart.android.damobile.view.ViewState.State;
import com.deviantart.android.damobile.view.thirdparty.ByteVector;
import com.deviantart.android.damobile.view.thirdparty.GomFactory;
import com.deviantart.android.damobile.view.thirdparty.GomType;
import com.deviantart.android.damobile.view.thirdparty.deviation.DeviationGom;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class TorpedoAdapter
  extends HeaderFooterRecyclerViewAdapter
  implements DeviationUtils.ProcessMenuAddable
{
  private final boolean a;
  private TorpedoAdapter.DeviationTorpedoDescriptor b;
  private final Stream<com.deviantart.android.sdk.api.model.DVNTDeviation> c;
  private String d;
  private boolean e = false;
  private String f;
  private int g = 0;
  private boolean h = true;
  private boolean i;
  private int index = 0;
  private Integer issue_id;
  private Torpedo j;
  private ViewState k = new ViewState();
  private WeakReference<TorpedoRecyclerView> l;
  private ArrayList<TorpedoAdapter.HeaderInfo> n = new ArrayList();
  private boolean s = false;
  private boolean w = false;
  
  public TorpedoAdapter(TorpedoRecyclerView paramTorpedoRecyclerView, Stream paramStream)
  {
    c = paramStream;
    l = new WeakReference(paramTorpedoRecyclerView);
    a = paramTorpedoRecyclerView.b();
    if (c.size() <= 0) {
      return;
    }
    next();
    ((TorpedoRecyclerView)l.get()).scrollToPosition(c.e());
    ((TorpedoRecyclerView)l.get()).forceLayout();
    if ((!c.iterator()) && (e)) {
      a();
    }
  }
  
  private void setAlpha(View paramView)
  {
    if (!(paramView instanceof ViewGroup)) {
      return;
    }
    paramView = (ViewGroup)paramView;
    int m = 0;
    while (m < paramView.getChildCount())
    {
      setAlpha(paramView.getChildAt(m));
      m += 1;
    }
  }
  
  private void setStartOnBoot(Context paramContext, boolean paramBoolean)
  {
    paramContext = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    paramContext.putBoolean(f, paramBoolean);
    paramContext.apply();
  }
  
  public void a()
  {
    Log.d("twoview", "sealTorpedo - start sealing");
    if (b == null)
    {
      Log.e("twoview", "tried to seal an imaginary torpedo descriptor");
      return;
    }
    if (j == null) {}
    for (int m = 0;; m = j.a())
    {
      Log.d("twoview", "sealTorpedo - sealing grid");
      b.e();
      j = b.d();
      if (j == null) {
        return;
      }
      if (!b.c()) {
        break;
      }
      Log.d("twoview", "sealTorpedo - noswipetorpedo, returning");
      try
      {
        c();
        return;
      }
      catch (Exception localException1)
      {
        new Handler().post(TorpedoAdapter..Lambda.3.c(this));
        return;
      }
    }
    int i1 = j.a();
    if (i1 > m)
    {
      Log.d("twoview", "sealTorpedo - oldThumbCount < newThumbCount : " + m + "/" + i1);
      try
      {
        append(m, i1 - m);
        return;
      }
      catch (Exception localException2)
      {
        new Handler().post(TorpedoAdapter..Lambda.4.c(this));
      }
    }
  }
  
  public void a(int paramInt)
  {
    g = paramInt;
    if (paramInt >= 0) {}
    int m;
    for (paramInt = 1;; paramInt = 0)
    {
      m = n.indexOf(TorpedoAdapter.HeaderInfo.b);
      if ((m != -1) || (paramInt == 0)) {
        break;
      }
      n.add(0, TorpedoAdapter.HeaderInfo.b);
      append(0);
      return;
    }
    if (paramInt != 0)
    {
      remove(m);
      return;
    }
    clear(m);
    n.remove(m);
  }
  
  protected void a(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    ViewGroup.LayoutParams localLayoutParams = null;
    if (getItem() == null) {
      return;
    }
    FrameLayout localFrameLayout = (FrameLayout)itemView;
    localFrameLayout.removeAllViews();
    paramViewHolder = (Activity)localFrameLayout.getContext();
    if ((!DVNTContextUtils.isContextDead(paramViewHolder)) && (l != null) && (l.get() != null))
    {
      switch (TorpedoAdapter.5.c[getItem().ordinal()])
      {
      default: 
        paramViewHolder = localLayoutParams;
        localLayoutParams = localFrameLayout.getLayoutParams();
        if ((e() <= 0) && (!a)) {
          width = ((TorpedoRecyclerView)l.get()).getDim2ScreenHeight();
        }
        break;
      }
      for (;;)
      {
        localFrameLayout.addView(paramViewHolder);
        return;
        paramViewHolder = ViewState.Helper.a(paramViewHolder, null, localFrameLayout);
        break;
        if (w)
        {
          Toast.makeText(paramViewHolder, paramViewHolder.getString(2131230920), 0).show();
          paramViewHolder.onBackPressed();
          return;
        }
        paramViewHolder = ViewState.Helper.a(paramViewHolder, null, localFrameLayout, k);
        break;
        if (w)
        {
          Toast.makeText(paramViewHolder, paramViewHolder.getString(2131230922), 0).show();
          paramViewHolder.onBackPressed();
          return;
        }
        paramViewHolder = ViewState.Helper.b(paramViewHolder, null, localFrameLayout, k);
        break;
        if (a)
        {
          width = ((TorpedoRecyclerView)l.get()).getDim1ScreenHeight();
        }
        else
        {
          width = -2;
          height = ((TorpedoRecyclerView)l.get()).getDim1ScreenHeight();
        }
      }
    }
  }
  
  public void a(Integer paramInteger)
  {
    issue_id = paramInteger;
  }
  
  public void a(String paramString)
  {
    k.a(paramString);
  }
  
  public void a(String paramString1, String paramString2)
  {
    d = paramString1;
    f = paramString2;
    if (d != null) {}
    int i1;
    for (int m = 1;; m = 0)
    {
      i1 = n.indexOf(TorpedoAdapter.HeaderInfo.g);
      if ((i1 != -1) || (m == 0)) {
        break;
      }
      n.add(TorpedoAdapter.HeaderInfo.g);
      append(n.size() - 1);
      return;
    }
    if (m != 0)
    {
      remove(i1);
      return;
    }
    clear(i1);
    n.remove(i1);
  }
  
  public void a(List paramList)
  {
    Object localObject;
    if (b == null)
    {
      localObject = (TorpedoRecyclerView)l.get();
      if (localObject == null) {
        return;
      }
      if (a)
      {
        localObject = new TorpedoAdapter.VerticalDeviationTorpedoDescriptor(this, ((TorpedoRecyclerView)localObject).getDim1Span(), ((TorpedoRecyclerView)localObject).getDim2Span(), 2);
        b = ((TorpedoAdapter.DeviationTorpedoDescriptor)localObject);
      }
    }
    else
    {
      if (j != null) {
        break label113;
      }
    }
    label113:
    for (int m = 0;; m = j.a())
    {
      if (b.a(paramList) > 0) {
        break label124;
      }
      Log.d("twoview", "addItems - none of the items have been accepted, returning");
      return;
      localObject = new TorpedoAdapter.HorizontalDeviationTorpedoDescriptor(this, ((TorpedoRecyclerView)localObject).getDim1Span(), ((TorpedoRecyclerView)localObject).getDim2Span(), 2);
      break;
    }
    label124:
    if ((getIssueId() != null) && (c.iterator()) && (remove() < getIssueId().intValue()))
    {
      b(true);
      Log.d("twoview", "addItems - waiting for more items before building torpedo");
      return;
    }
    j = b.b();
    int i2 = j.a();
    int i1 = Math.max(0, m);
    m = i2 - m;
    if (m == 0)
    {
      Log.d("twoview", "addItems - notifyContentItemRangeInserted - 0 thumbs to add (probably still in current column");
      return;
    }
    Log.d("twoview", "addItems - notifyContentItemRangeInserted position/count : " + i1 + "/" + m);
    try
    {
      append(i1, m);
      return;
    }
    catch (Exception paramList)
    {
      new Handler().post(TorpedoAdapter..Lambda.2.c(this));
    }
  }
  
  protected int add()
  {
    if (getItem() == null) {
      return 0;
    }
    return 1;
  }
  
  protected int b(int paramInt)
  {
    if ((b == null) || (!e)) {
      return 0;
    }
    return GomFactory.a(((DeviationDescription)b.m().get(paramInt)).a()).b().ordinal();
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    TorpedoRecyclerView localTorpedoRecyclerView = (TorpedoRecyclerView)l.get();
    return GomType.b(paramInt).b(localTorpedoRecyclerView.getContext(), paramViewGroup);
  }
  
  public void b()
  {
    if (l != null)
    {
      if (l.get() == null) {
        return;
      }
      ((TorpedoRecyclerView)l.get()).smoothScrollToPosition(size() + c.e());
    }
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    ImageUtils.a(GalleryType.b);
    TorpedoRecyclerView localTorpedoRecyclerView = (TorpedoRecyclerView)l.get();
    Activity localActivity = (Activity)localTorpedoRecyclerView.getContext();
    if (DVNTContextUtils.isContextDead(localActivity)) {
      return;
    }
    Log.d("adapter", c.getValue() + " - binding viewholder, position " + paramInt);
    Object localObject2 = itemView;
    Object localObject1 = (GridLayoutManager.LayoutParams)((View)localObject2).getLayoutParams();
    Torpedo.TorpedoThumb localTorpedoThumb = j.a(paramInt);
    int i1 = localTorpedoRecyclerView.getDim1ScreenHeight() / localTorpedoRecyclerView.getDim1Span() * x;
    int m = localTorpedoRecyclerView.getDim2ScreenHeight() / localTorpedoRecyclerView.getDim2Span() * y;
    int i2;
    if (a)
    {
      i2 = i1;
      if (a) {
        i1 = m;
      }
      if (localObject1 != null) {
        break label253;
      }
      localObject1 = new GridLayoutManager.LayoutParams(i2, i1);
    }
    for (;;)
    {
      ((View)localObject2).setLayoutParams((ViewGroup.LayoutParams)localObject1);
      localObject1 = (DeviationDescription)b.m().get(paramInt);
      localObject2 = GomFactory.a(((DeviationDescription)localObject1).a());
      ((DeviationGom)localObject2).b(new TorpedoAdapter.4(this, paramInt));
      ((DeviationGom)localObject2).b(i2, i1);
      ((DeviationGom)localObject2).b(localActivity, (DeviationDescription)localObject1, paramViewHolder, h);
      return;
      i2 = m;
      break;
      label253:
      width = i2;
      height = i1;
    }
  }
  
  public void b(TorpedoRecyclerView paramTorpedoRecyclerView, Stream paramStream)
  {
    l.clear();
    l = new WeakReference(paramTorpedoRecyclerView);
    if (paramStream.size() > index) {
      next();
    }
  }
  
  public void b(ViewState.State paramState)
  {
    int i1 = 1;
    if (paramState == k.a()) {
      return;
    }
    int m;
    if (getItem() != null)
    {
      m = 1;
      if (paramState == null) {
        break label75;
      }
    }
    ViewState localViewState;
    for (;;)
    {
      if ((m == 0) || (i1 == 0)) {
        break label80;
      }
      localViewState = k;
      try
      {
        localViewState.a(paramState);
        e(0);
        return;
      }
      catch (Exception paramState)
      {
        new Handler().post(TorpedoAdapter..Lambda.6.c(this));
        return;
      }
      m = 0;
      break;
      label75:
      i1 = 0;
    }
    label80:
    if (m != 0)
    {
      c(0);
      paramState = k;
      paramState.a(null);
      return;
    }
    if (i1 != 0)
    {
      localViewState = k;
      localViewState.a(paramState);
      replace(0);
    }
  }
  
  public void b(ViewState.State paramState, Bundle paramBundle)
  {
    k.a(paramState, paramBundle);
  }
  
  public void b(boolean paramBoolean)
  {
    i = paramBoolean;
  }
  
  protected RecyclerView.ViewHolder c(ViewGroup paramViewGroup, int paramInt)
  {
    paramViewGroup = new FrameLayout(((TorpedoRecyclerView)l.get()).getContext());
    paramViewGroup.setLayoutParams(new GridLayoutManager.LayoutParams(-2, -2));
    return new TorpedoAdapter.3(this, paramViewGroup);
  }
  
  protected void c(RecyclerView.ViewHolder paramViewHolder, int paramInt) {}
  
  public void d()
  {
    e = true;
    if (l != null)
    {
      if (l.get() == null) {
        return;
      }
      if (b != null)
      {
        b.g(((TorpedoRecyclerView)l.get()).getDim1Span());
        b.b(((TorpedoRecyclerView)l.get()).getDim2Span());
        b.a();
      }
      next();
      if (!c.iterator()) {
        a();
      }
    }
  }
  
  protected int e()
  {
    if ((j == null) || (!e)) {
      return 0;
    }
    return j.a();
  }
  
  public boolean f()
  {
    return (b != null) && (!b.c());
  }
  
  public Stream get()
  {
    return c;
  }
  
  protected int getIntValue(int paramInt)
  {
    return ((TorpedoAdapter.HeaderInfo)n.get(paramInt)).ordinal();
  }
  
  public Integer getIssueId()
  {
    return issue_id;
  }
  
  public Torpedo.TorpedoThumb getItem(int paramInt)
  {
    if ((j == null) || (!e)) {
      return null;
    }
    return j.a(paramInt);
  }
  
  public ViewState.State getItem()
  {
    return k.a();
  }
  
  public boolean i()
  {
    return i;
  }
  
  public void next()
  {
    if (!e) {
      return;
    }
    if (s) {
      s = false;
    }
    try
    {
      c();
      ArrayList localArrayList1 = c.get();
      i1 = localArrayList1.size();
      Log.d("twoview", "notifyDeviationsAdded - lastKnownStreamSize/newStreamSize : " + index + "/" + i1);
      if ((i1 > 0) && (index < i1))
      {
        localArrayList2 = new ArrayList();
        int m = index;
        while (m < i1)
        {
          localArrayList2.add(new DeviationDescription((com.deviantart.android.android.package_14.model.DVNTDeviation)localArrayList1.get(m), c.length(), m));
          m += 1;
        }
      }
    }
    catch (Exception localException)
    {
      int i1;
      ArrayList localArrayList2;
      for (;;)
      {
        new Handler().post(TorpedoAdapter..Lambda.1.c(this));
      }
      a(localArrayList2);
      index = i1;
    }
  }
  
  protected RecyclerView.ViewHolder onCreateView(ViewGroup paramViewGroup, int paramInt)
  {
    TorpedoRecyclerView localTorpedoRecyclerView = (TorpedoRecyclerView)l.get();
    GridLayoutManager.LayoutParams localLayoutParams = new GridLayoutManager.LayoutParams(-2, -2);
    TorpedoAdapter.HeaderInfo localHeaderInfo = TorpedoAdapter.HeaderInfo.values()[paramInt];
    switch (TorpedoAdapter.5.b[localHeaderInfo.ordinal()])
    {
    default: 
      Log.e("ExplanationHeader", "Unhandled RecyclerView header type");
      paramViewGroup = new View(localTorpedoRecyclerView.getContext());
    case 1: 
      for (;;)
      {
        return new TorpedoAdapter.2(this, paramViewGroup);
        paramViewGroup = new View(localTorpedoRecyclerView.getContext());
        localLayoutParams.setMargins(0, g, 0, 0);
        paramViewGroup.setLayoutParams(localLayoutParams);
      }
    }
    paramViewGroup = LayoutInflater.from(localTorpedoRecyclerView.getContext()).inflate(2130968650, paramViewGroup, false);
    height = Graphics.add(localTorpedoRecyclerView.getContext(), 80);
    paramViewGroup.setLayoutParams(localLayoutParams);
    paramViewGroup = new TorpedoAdapter.ExplanationHeaderViewHolder(this, paramViewGroup);
    explanationTextView.setText(d);
    cancelButton.setOnClickListener(new TorpedoAdapter.1(this));
    return paramViewGroup;
  }
  
  public void onViewRecycled(RecyclerView.ViewHolder paramViewHolder)
  {
    setAlpha(itemView);
    super.onViewRecycled(paramViewHolder);
  }
  
  public int remove()
  {
    if ((b != null) && (b.m() != null)) {
      return b.m().size();
    }
    return 0;
  }
  
  public void setDisplayMode(boolean paramBoolean)
  {
    w = paramBoolean;
  }
  
  public void setVisible(boolean paramBoolean)
  {
    h = paramBoolean;
  }
  
  protected int size()
  {
    if (!a) {
      return 0;
    }
    return n.size();
  }
  
  public void visitMaxs()
  {
    int m;
    if (j == null) {
      m = 0;
    }
    try
    {
      for (;;)
      {
        add(0, m);
        b = null;
        j = null;
        s = true;
        index = 0;
        return;
        m = j.a();
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        new Handler().post(TorpedoAdapter..Lambda.5.c(this));
      }
    }
  }
}
