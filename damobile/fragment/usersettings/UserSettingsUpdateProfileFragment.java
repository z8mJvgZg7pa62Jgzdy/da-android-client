package com.deviantart.android.damobile.fragment.usersettings;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.package_14.model.DVNTUser.DVNTUserDetails;
import com.deviantart.android.android.package_14.model.DVNTUserProfile;
import com.deviantart.android.android.package_14.model.DVNTWhoAmIResponse;
import com.deviantart.android.android.package_14.model.DVNTWhoIsResponse;
import com.deviantart.android.damobile.activity.UserSettingsActivity;
import com.deviantart.android.damobile.activity.UserSettingsActivity.OnBackResult;
import com.deviantart.android.damobile.util.MemberType;
import com.deviantart.android.damobile.util.ResourceUtils;
import com.deviantart.android.damobile.util.SexType;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.view.dialogs.ChangeEmailDialog;
import com.deviantart.android.damobile.view.dialogs.DABaseDialog;
import com.deviantart.android.damobile.view.dialogs.UserSettingsEditTextDialog;
import com.deviantart.android.damobile.view.dialogs.UserSettingsPasswordDialog;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Set;

public class UserSettingsUpdateProfileFragment
  extends UserSettingsBaseFragment
{
  private boolean AutoStart = false;
  @Bind({2131689900})
  TextView artistLevel;
  @Bind({2131689898})
  RelativeLayout artistLevelBlock;
  @Bind({2131689903})
  TextView artistSpecialty;
  @Bind({2131689901})
  RelativeLayout artistSpecialtyBlock;
  String c;
  @Bind({2131689890})
  TextView email;
  @Bind({2131689896})
  SwitchCompat emailNews;
  @Bind({2131689897})
  SwitchCompat isArtistSwitch;
  @Bind({2131689909})
  TextView location;
  private LinkedHashMap<String, String> mMap;
  @Bind({2131689914})
  EditText mPasswordView;
  private LinkedHashMap<String, String> mTabs;
  @Bind({2131689895})
  TextView mUsernameView;
  @Bind({2131689906})
  TextView realName;
  @Bind({2131689913})
  EditText tagLine;
  private LinkedHashMap<String, String> this$0;
  @Bind({2131689886})
  TextView userName;
  @Bind({2131689912})
  TextView website;
  
  public UserSettingsUpdateProfileFragment() {}
  
  private void visitAttribute(String paramString1, String paramString2)
  {
    add(paramString1, null, paramString2, null, null);
  }
  
  private void visitField(String paramString1, String paramString2)
  {
    add(paramString1, paramString2, null, null, null);
  }
  
  public UserSettingsActivity.OnBackResult a()
  {
    if (d) {
      if (artistLevel.getTag() == null) {
        break label272;
      }
    }
    label227:
    label272:
    for (Object localObject = Integer.valueOf(Integer.parseInt((String)artistLevel.getTag()));; localObject = null)
    {
      if (artistSpecialty.getTag() != null) {}
      for (Integer localInteger1 = Integer.valueOf(Integer.parseInt((String)artistSpecialty.getTag()));; localInteger1 = null)
      {
        if (location.getTag() != null) {}
        for (Integer localInteger2 = Integer.valueOf(Integer.parseInt((String)location.getTag()));; localInteger2 = null)
        {
          newInstance(Boolean.valueOf(isArtistSwitch.isChecked()), (Integer)localObject, localInteger1, realName.getText().toString(), tagLine.getText().toString(), localInteger2, website.getText().toString(), mPasswordView.getText().toString());
          ((UserSettingsActivity)getActivity()).setColor(true);
          d = false;
          if (e)
          {
            localObject = mUsernameView.getText().toString();
            if (!((String)localObject).equals("Male")) {
              break label227;
            }
            localObject = "m";
          }
          for (;;)
          {
            add(null, null, null, (String)localObject, Boolean.valueOf(emailNews.isChecked()));
            e = false;
            DVNTAbstractAsyncAPI.clearCache();
            return UserSettingsActivity.OnBackResult.o;
            if (((String)localObject).equals("Female")) {
              localObject = "f";
            } else if (((String)localObject).equals("Hidden")) {
              localObject = null;
            } else {
              localObject = "o";
            }
          }
        }
      }
    }
  }
  
  protected void b()
  {
    if (c == null) {
      return;
    }
    if (email != null) {
      email.setText(c);
    }
    UserUtils.c = c;
    c = null;
  }
  
  public void b(DVNTWhoAmIResponse paramDVNTWhoAmIResponse)
  {
    userName.setText(paramDVNTWhoAmIResponse.getUserName());
    email.setText(paramDVNTWhoAmIResponse.getEmail());
    emailNews.setChecked(paramDVNTWhoAmIResponse.isNewsletterRecipient());
    d = false;
    e = false;
  }
  
  public void onArtistLevelClick()
  {
    if (AutoStart)
    {
      if (!isArtistSwitch.isChecked()) {
        return;
      }
      String[] arrayOfString1 = (String[])mTabs.keySet().toArray(new String[mTabs.size()]);
      String[] arrayOfString2 = (String[])mTabs.values().toArray(new String[mTabs.size()]);
      int i = 0;
      if (i < arrayOfString2.length) {
        if (!arrayOfString2[i].equals(artistLevel.getText().toString())) {}
      }
      for (;;)
      {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
        localBuilder.setTitle(2131231259).setSingleChoiceItems(arrayOfString2, i, new UserSettingsUpdateProfileFragment.6(this, arrayOfString2, arrayOfString1)).setNegativeButton(2131230820, new UserSettingsUpdateProfileFragment.5(this));
        localBuilder.create().show();
        return;
        i += 1;
        break;
        i = -1;
      }
    }
  }
  
  public void onCheckedChange(boolean paramBoolean)
  {
    int j = 0;
    RelativeLayout localRelativeLayout = artistLevelBlock;
    if (paramBoolean)
    {
      i = 0;
      localRelativeLayout.setVisibility(i);
      localRelativeLayout = artistSpecialtyBlock;
      if (!paramBoolean) {
        break label70;
      }
    }
    label70:
    for (int i = j;; i = 8)
    {
      localRelativeLayout.setVisibility(i);
      if (!paramBoolean)
      {
        artistLevel.setText(null);
        artistSpecialty.setText(null);
      }
      d = true;
      return;
      i = 8;
      break;
    }
  }
  
  public void onClick(DVNTUserProfile paramDVNTUserProfile)
  {
    if (paramDVNTUserProfile == null) {
      return;
    }
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (paramDVNTUserProfile.getUser() != null)
    {
      localObject1 = localObject2;
      if (paramDVNTUserProfile.getUser().getDetails() != null) {
        localObject1 = SexType.get(paramDVNTUserProfile.getUser().getDetails().getSex());
      }
    }
    if (localObject1 != null) {
      mUsernameView.setText(((SexType)localObject1).getValue());
    }
    for (;;)
    {
      isArtistSwitch.setChecked(paramDVNTUserProfile.getIsArtist().booleanValue());
      if (paramDVNTUserProfile.getIsArtist().booleanValue())
      {
        artistLevel.setText(paramDVNTUserProfile.getArtistLevel());
        artistSpecialty.setText(paramDVNTUserProfile.getArtistSpecialty());
      }
      realName.setText(paramDVNTUserProfile.getRealName());
      location.setText(paramDVNTUserProfile.getCountry());
      website.setText(paramDVNTUserProfile.getWebsite());
      tagLine.setText(paramDVNTUserProfile.getTagLine());
      mPasswordView.setText(paramDVNTUserProfile.getBio());
      AutoStart = true;
      d = false;
      e = false;
      return;
      mUsernameView.setText("Hidden");
    }
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setRetainInstance(true);
    if (this$0 == null) {
      this$0 = ResourceUtils.parse(getActivity(), 2131623939);
    }
    if (mTabs == null) {
      mTabs = ResourceUtils.parse(getActivity(), 2131623937);
    }
    if (mMap == null) {
      mMap = ResourceUtils.parse(getActivity(), 2131623938);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = (ScrollView)paramLayoutInflater.inflate(2130968694, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    if (!isArtistSwitch.isChecked())
    {
      artistLevelBlock.setVisibility(8);
      artistSpecialtyBlock.setVisibility(8);
    }
    filterSelected();
    loadNotifications();
    return paramLayoutInflater;
  }
  
  public void onEmailClick()
  {
    ChangeEmailDialog localChangeEmailDialog = new ChangeEmailDialog();
    localChangeEmailDialog.b(new UserSettingsUpdateProfileFragment.1(this, localChangeEmailDialog));
    localChangeEmailDialog.show(getFragmentManager(), "email");
  }
  
  public void onLocationClick()
  {
    if (!AutoStart) {
      return;
    }
    String[] arrayOfString1 = (String[])this$0.keySet().toArray(new String[this$0.size()]);
    String[] arrayOfString2 = (String[])this$0.values().toArray(new String[this$0.size()]);
    int i = 0;
    if (i < arrayOfString2.length) {
      if (!arrayOfString2[i].equals(location.getText().toString())) {}
    }
    for (;;)
    {
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
      localBuilder.setTitle(2131231285).setSingleChoiceItems(arrayOfString2, i, new UserSettingsUpdateProfileFragment.11(this, arrayOfString2, arrayOfString1)).setNegativeButton(2131230820, new UserSettingsUpdateProfileFragment.10(this));
      localBuilder.create().show();
      return;
      i += 1;
      break;
      i = -1;
    }
  }
  
  public void onNewsCheckedChange(boolean paramBoolean)
  {
    e = true;
  }
  
  public void onPasswordClick()
  {
    if ((UserUtils.g != null) && (MemberType.E == UserUtils.g))
    {
      Toast.makeText(getActivity(), 2131230902, 0).show();
      return;
    }
    UserSettingsPasswordDialog localUserSettingsPasswordDialog = new UserSettingsPasswordDialog();
    localUserSettingsPasswordDialog.b(new UserSettingsUpdateProfileFragment.2(this, localUserSettingsPasswordDialog));
    localUserSettingsPasswordDialog.show(getFragmentManager(), "password");
  }
  
  public void onRealNameClick()
  {
    UserSettingsEditTextDialog localUserSettingsEditTextDialog = UserSettingsEditTextDialog.a(getString(2131231311), getString(2131231310));
    localUserSettingsEditTextDialog.b(new UserSettingsUpdateProfileFragment.9(this, localUserSettingsEditTextDialog));
    localUserSettingsEditTextDialog.show(getFragmentManager(), "realname");
  }
  
  public void onResume()
  {
    super.onResume();
    ((UserSettingsActivity)getActivity()).onProgressChanged(2131231322);
  }
  
  public void onSexClick()
  {
    if (AutoStart)
    {
      if (mUsernameView.getText().toString().equals("Hidden")) {
        return;
      }
      Object localObject1 = new ArrayList();
      Object localObject2 = SexType.values();
      int j = localObject2.length;
      int i = 0;
      while (i < j)
      {
        ((ArrayList)localObject1).add(localObject2[i].getValue());
        i += 1;
      }
      i = ((ArrayList)localObject1).indexOf(mUsernameView.getText().toString());
      localObject1 = (CharSequence[])((ArrayList)localObject1).toArray(new CharSequence[((ArrayList)localObject1).size()]);
      localObject2 = new AlertDialog.Builder(getActivity());
      ((AlertDialog.Builder)localObject2).setTitle(getString(2131231314));
      ((AlertDialog.Builder)localObject2).setSingleChoiceItems((CharSequence[])localObject1, i, new UserSettingsUpdateProfileFragment.3(this, (CharSequence[])localObject1));
      ((AlertDialog.Builder)localObject2).setNegativeButton(2131230820, new UserSettingsUpdateProfileFragment.4(this));
      ((AlertDialog.Builder)localObject2).create().show();
    }
  }
  
  public void onSpecialityClick()
  {
    if (AutoStart)
    {
      if (!isArtistSwitch.isChecked()) {
        return;
      }
      String[] arrayOfString1 = (String[])mMap.keySet().toArray(new String[mMap.size()]);
      String[] arrayOfString2 = (String[])mMap.values().toArray(new String[mMap.size()]);
      int i = 0;
      if (i < arrayOfString2.length) {
        if (!arrayOfString2[i].equals(artistSpecialty.getText().toString())) {}
      }
      for (;;)
      {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
        localBuilder.setTitle(2131231261).setSingleChoiceItems(arrayOfString2, i, new UserSettingsUpdateProfileFragment.8(this, arrayOfString2, arrayOfString1)).setNegativeButton(2131230820, new UserSettingsUpdateProfileFragment.7(this));
        localBuilder.create().show();
        return;
        i += 1;
        break;
        i = -1;
      }
    }
  }
  
  public void onTextChanged(CharSequence paramCharSequence)
  {
    d = true;
  }
  
  public void onWebsiteClick()
  {
    UserSettingsEditTextDialog localUserSettingsEditTextDialog = UserSettingsEditTextDialog.a(getString(2131231329), getString(2131231328));
    localUserSettingsEditTextDialog.b(new UserSettingsUpdateProfileFragment.12(this, localUserSettingsEditTextDialog));
    localUserSettingsEditTextDialog.show(getFragmentManager(), "website");
  }
}
