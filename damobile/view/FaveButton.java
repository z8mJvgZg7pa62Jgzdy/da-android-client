package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnGraduateCancelEvent;
import com.deviantart.android.damobile.util.ProcessMenuListener;
import com.deviantart.android.damobile.util.ProcessMenuType;
import com.deviantart.android.damobile.util.ViewHelper;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.squareup.otto.Bus;

public class FaveButton
  extends DeviationMenuButton
{
  private boolean b;
  private boolean d = false;
  private boolean e = false;
  
  public FaveButton(Activity paramActivity, DVNTDeviation paramDVNTDeviation)
  {
    super(paramActivity, paramDVNTDeviation);
    d(paramActivity);
  }
  
  public FaveButton(Activity paramActivity, String paramString)
  {
    super(paramActivity, paramString);
    d(paramActivity);
  }
  
  private void a()
  {
    ImageView localImageView = (ImageView)findViewById(2131689734);
    if (b)
    {
      localImageView.setImageResource(2130837823);
      return;
    }
    localImageView.setImageResource(2130837833);
  }
  
  private void d()
  {
    boolean bool;
    if (!b)
    {
      bool = true;
      b = bool;
      a();
      if (!b) {
        break label77;
      }
    }
    label77:
    for (ProcessMenuType localProcessMenuType = ProcessMenuType.c;; localProcessMenuType = ProcessMenuType.b)
    {
      if (i != null) {
        i.setFavourited(Boolean.valueOf(b));
      }
      if (J == null) {
        return;
      }
      J.a(localProcessMenuType, t);
      return;
      bool = false;
      break;
    }
  }
  
  private void d(Activity paramActivity)
  {
    boolean bool = false;
    setClipChildren(false);
    LayoutInflater.from(paramActivity).inflate(2130968665, this);
    if (i != null) {
      bool = i.isFavourited().booleanValue();
    }
    b = bool;
    a();
  }
  
  private void onPause()
  {
    try
    {
      BusStation.get().unregister(this);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      Log.e("FaveButton", localIllegalArgumentException.toString());
    }
  }
  
  public boolean b()
  {
    if (!e)
    {
      if (DVNTContextUtils.isContextDead(getContext())) {
        return false;
      }
      e = true;
      int i;
      label53:
      FaveButton.1 local1;
      if (DVNTAbstractAsyncAPI.isUserSession(getContext()))
      {
        d();
        d = false;
        if (!b) {
          break label140;
        }
        i = 2131230857;
        local1 = new FaveButton.1(this, i);
        if (((!b) || (d)) && ((b) || (!d))) {
          break label146;
        }
        TrackerUtil.a((Activity)getContext(), EventKeys.Category.y, "add_favourite");
        DVNTCommonAsyncAPI.addToFavourites(t, null).call(getContext(), local1);
      }
      for (;;)
      {
        return true;
        d = true;
        BusStation.get().register(this);
        break;
        label140:
        i = 2131230870;
        break label53;
        label146:
        TrackerUtil.a((Activity)getContext(), EventKeys.Category.y, "remove_favourite");
        DVNTCommonAsyncAPI.removeFromFavourites(t, null).call(getContext(), local1);
      }
    }
    return false;
  }
  
  public Animation getOnSelectedAnimation()
  {
    TextView localTextView = (TextView)findViewById(2131689733);
    if ((DVNTContextUtils.isContextDead(getContext())) || (localTextView == null)) {
      return null;
    }
    if (b)
    {
      localTextView.setText(getContext().getString(2131231386));
      ViewHelper.a(localTextView, getContext().getResources().getDrawable(2130837677));
      localTextView.setTextColor(-16777216);
    }
    for (int i = 2131034133;; i = 2131034155)
    {
      Animation localAnimation = AnimationUtils.loadAnimation(getContext(), i);
      localAnimation.setAnimationListener(new FaveButton.2(this, localTextView));
      return localAnimation;
      localTextView.setText(getContext().getString(2131231387));
      ViewHelper.a(localTextView, getContext().getResources().getDrawable(2130837852));
      localTextView.setTextColor(-1);
    }
  }
  
  public void onGraduateCancel(BusStation.OnGraduateCancelEvent paramOnGraduateCancelEvent)
  {
    e = false;
    onPause();
  }
  
  public void startAnimation(Animation paramAnimation)
  {
    TextView localTextView = (TextView)findViewById(2131689733);
    localTextView.setVisibility(0);
    localTextView.startAnimation(paramAnimation);
  }
}
