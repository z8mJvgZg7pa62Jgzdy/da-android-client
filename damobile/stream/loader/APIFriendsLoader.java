package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTFriend;

public class APIFriendsLoader
  extends StreamLoader<DVNTFriend>
{
  private String d;
  
  public APIFriendsLoader(String paramString)
  {
    d = paramString;
    a = 20;
  }
  
  public String b()
  {
    return "friendsloader" + d;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.getFriends(d, Integer.valueOf(paramInt), Integer.valueOf(a), true, false, false, true);
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APIFriendsLoader.1(this, paramStreamLoadListener));
  }
}
