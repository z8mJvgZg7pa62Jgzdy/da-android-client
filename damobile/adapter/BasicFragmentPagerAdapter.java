package com.deviantart.android.damobile.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import java.util.ArrayList;

public class BasicFragmentPagerAdapter
  extends FragmentPagerAdapter
{
  private ArrayList<Fragment> collection_list;
  
  public BasicFragmentPagerAdapter(FragmentManager paramFragmentManager, ArrayList paramArrayList)
  {
    super(paramFragmentManager);
    collection_list = paramArrayList;
  }
  
  public int getCount()
  {
    return collection_list.size();
  }
  
  public Fragment getItem(int paramInt)
  {
    return (Fragment)collection_list.get(paramInt);
  }
}
