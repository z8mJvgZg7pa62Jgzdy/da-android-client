package com.deviantart.android.damobile.view.thirdparty.deviation.viewholder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.view.DAFaveView;
import com.facebook.drawee.view.SimpleDraweeView;

public class ImageViewHolder
  extends RecyclerView.ViewHolder
{
  @Bind({2131689920})
  public SimpleDraweeView draweeView;
  @Bind({2131689919})
  public DAFaveView faveView;
  
  public ImageViewHolder(View paramView)
  {
    super(paramView);
    ButterKnife.bind(this, paramView);
  }
}
