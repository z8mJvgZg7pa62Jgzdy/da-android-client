package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTCategory
  implements Serializable
{
  @SerializedName("catpath")
  String catPath;
  @SerializedName("has_subcategory")
  Boolean hasSubCategory;
  @SerializedName("parent_catpath")
  String parentPath;
  String title;
  
  public DVNTCategory() {}
  
  public String getCatPath()
  {
    return catPath;
  }
  
  public Boolean getHasSubCategory()
  {
    return hasSubCategory;
  }
  
  public String getParentPath()
  {
    return parentPath;
  }
  
  public String getTitle()
  {
    return title;
  }
  
  public void setCatPath(String paramString)
  {
    catPath = paramString;
  }
  
  public void setHasSubCategory(Boolean paramBoolean)
  {
    hasSubCategory = paramBoolean;
  }
  
  public void setParentPath(String paramString)
  {
    parentPath = paramString;
  }
  
  public void setTitle(String paramString)
  {
    title = paramString;
  }
}
