package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTUserStatus
  implements Serializable
{
  @SerializedName("author")
  DVNTUser author;
  @SerializedName("body")
  DVNTDAMLComponent.List body;
  @SerializedName("comments_count")
  Integer commentsCount;
  @SerializedName("is_deleted")
  Boolean isDeleted;
  @SerializedName("is_share")
  Boolean isShare;
  @SerializedName("items")
  DVNTRepostItem.List repostItems;
  @SerializedName("statusid")
  String statusId;
  @SerializedName("ts")
  String time;
  @SerializedName("url")
  String urlString;
  
  public DVNTUserStatus() {}
  
  public DVNTUser getAuthor()
  {
    return author;
  }
  
  public DVNTDAMLComponent.List getBody()
  {
    return body;
  }
  
  public Integer getCommentsCount()
  {
    return commentsCount;
  }
  
  public Boolean getIsDeleted()
  {
    return isDeleted;
  }
  
  public Boolean getIsShare()
  {
    return isShare;
  }
  
  public DVNTRepostItem.List getRepostItems()
  {
    return repostItems;
  }
  
  public String getStatusId()
  {
    return statusId;
  }
  
  public String getTime()
  {
    return time;
  }
  
  public String getUrl()
  {
    return urlString;
  }
  
  public Boolean isDeleted()
  {
    return isDeleted;
  }
  
  public Boolean isShare()
  {
    return isShare;
  }
  
  public void setAuthor(DVNTUser paramDVNTUser)
  {
    author = paramDVNTUser;
  }
  
  public void setBody(DVNTDAMLComponent.List paramList)
  {
    body = paramList;
  }
  
  public void setCommentsCount(Integer paramInteger)
  {
    commentsCount = paramInteger;
  }
  
  public void setIsDeleted(Boolean paramBoolean)
  {
    isDeleted = paramBoolean;
  }
  
  public void setIsShare(Boolean paramBoolean)
  {
    isShare = paramBoolean;
  }
  
  public void setRepostItems(DVNTRepostItem.List paramList)
  {
    repostItems = paramList;
  }
  
  public void setStatusId(String paramString)
  {
    statusId = paramString;
  }
  
  public void setTime(String paramString)
  {
    time = paramString;
  }
  
  public void setUrl(String paramString)
  {
    urlString = paramString;
  }
}
