package com.deviantart.android.damobile.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class NonSwipeViewPager
  extends ViewPager
{
  private boolean vertical = true;
  
  public NonSwipeViewPager(Context paramContext)
  {
    super(paramContext);
  }
  
  public NonSwipeViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    if (vertical) {
      return false;
    }
    return super.onInterceptTouchEvent(paramMotionEvent);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (vertical) {
      return false;
    }
    return super.onTouchEvent(paramMotionEvent);
  }
  
  public void setDisableSwipe(boolean paramBoolean)
  {
    vertical = paramBoolean;
  }
}
