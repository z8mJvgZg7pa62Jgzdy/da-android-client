package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTStashDeltaRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTStashDelta>
{
  private String cursor;
  private Boolean includeCameraExifInfo;
  private Boolean includeStats;
  private Boolean includeSubmissionInfo;
  private Integer limit;
  private Integer offset;
  
  public DVNTStashDeltaRequestV1(String paramString, Integer paramInteger1, Integer paramInteger2, Boolean paramBoolean1, Boolean paramBoolean2, Boolean paramBoolean3)
  {
    super(com.deviantart.android.sdk.api.model.DVNTStashDelta.class);
    cursor = paramString;
    offset = paramInteger1;
    limit = paramInteger2;
    includeSubmissionInfo = paramBoolean1;
    includeCameraExifInfo = paramBoolean2;
    includeStats = paramBoolean3;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTStashDeltaRequestV1 localDVNTStashDeltaRequestV1 = (DVNTStashDeltaRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(cursor, cursor).append(offset, offset).append(limit, limit).append(includeSubmissionInfo, includeSubmissionInfo).append(includeCameraExifInfo, includeCameraExifInfo).append(includeStats, includeStats).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(cursor).append(offset).append(limit).append(includeSubmissionInfo).append(includeCameraExifInfo).append(includeStats).toHashCode();
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTStashDelta sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).stashDelta(paramString, cursor, offset, limit, includeSubmissionInfo, includeCameraExifInfo, includeStats, Boolean.valueOf(true));
  }
}
