package com.deviantart.android.android.package_14.session;

import android.content.Context;
import com.deviantart.android.android.package_14.config.DVNTAPIConfig;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig;
import com.deviantart.android.android.package_14.config.DVNTGraduateHandler;
import com.deviantart.android.android.package_14.config.DVNTGraduateHandler.DVNTOnGraduationEndedListener;
import com.deviantart.android.android.package_14.model.DVNTEnrichedToken;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthHelper;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthPreferencesManager;
import com.deviantart.datoolkit.logger.DVNTLog;
import com.octo.android.robospice.SpiceManager;

public class DVNTUserSessionManager
  extends DVNTTokenSessionManager
  implements DVNTGraduateHandler.DVNTOnGraduationEndedListener
{
  String authorizationCode;
  
  public DVNTUserSessionManager(DVNTRestTokenService paramDVNTRestTokenService, SpiceManager paramSpiceManager, DVNTOAuthPreferencesManager paramDVNTOAuthPreferencesManager, DVNTOAuthHelper paramDVNTOAuthHelper, DVNTAPIConfig paramDVNTAPIConfig)
  {
    super(paramDVNTRestTokenService, paramSpiceManager, paramDVNTOAuthPreferencesManager, paramDVNTOAuthHelper, paramDVNTAPIConfig);
    paramDVNTAPIConfig.getGraduateHandler().init(paramDVNTOAuthPreferencesManager, paramDVNTOAuthHelper, this);
  }
  
  public boolean callGetNewToken(Context paramContext)
  {
    if (authorizationCode == null) {
      return false;
    }
    tokenService.newUserToken(apiConfig.getClientId(), apiConfig.getClientSecret(), authorizationCode, apiConfig.getRedirectURI(), new DVNTTokenSessionManager.DVNTAccessTokenCallback(this, paramContext));
    return true;
  }
  
  public boolean callRefreshToken(Context paramContext)
  {
    tokenService.refreshUserToken(apiConfig.getClientId(), apiConfig.getClientSecret(), token.getRefreshToken(), new DVNTTokenSessionManager.DVNTAccessTokenCallback(this, paramContext));
    return true;
  }
  
  public void close(Context paramContext)
  {
    oAuthPreferencesManager.clearStoredTokenAndCookies(paramContext);
    if ((token != null) && (token.getToken() != null))
    {
      if (token.getRefreshToken() == null) {
        return;
      }
      tokenService.revoke(token.getRefreshToken(), Boolean.valueOf(true), new DVNTUserSessionManager.1(this));
    }
  }
  
  protected String getTokenManagerName()
  {
    return "DVNTUserSessionManager";
  }
  
  public String getTokenPreferenceKey()
  {
    return "user_accessToken";
  }
  
  public boolean hasTokenValidFormat()
  {
    return (super.hasTokenValidFormat()) && (token.getRefreshToken() != null) && (apiConfig != null) && (apiConfig.getScope() != null) && (!oAuthHelper.hasTokenIncompatibleScope(token, apiConfig.getScope()));
  }
  
  public void onGraduationCancelled(Context paramContext)
  {
    DVNTLog.get(format, new Object[] { "graduation cancelled" });
    isAlreadyBuildingSession = false;
    apiConfig.getGraduateHandler().onGraduateCancelled(paramContext);
  }
  
  public void onGraduationFailed(Context paramContext, String paramString)
  {
    DVNTLog.get(format, new Object[] { "graduation failed :" + paramString });
    isAlreadyBuildingSession = false;
    apiConfig.getGraduateHandler().onGraduationFail(paramContext, paramString);
  }
  
  public void onGraduationSucceededWithToken(Context paramContext, DVNTEnrichedToken paramDVNTEnrichedToken)
  {
    DVNTLog.get(format, new Object[] { "graduation - successWithToken :" + paramDVNTEnrichedToken.getToken() });
    setToken(paramDVNTEnrichedToken);
    saveTokenIntoPreferences(paramContext);
    isAlreadyBuildingSession = false;
    start(paramContext);
    apiConfig.getGraduateHandler().onGraduationSuccess(paramContext);
  }
  
  public void onGraduationSucceededdWithAuthorizationCode(Context paramContext, String paramString)
  {
    DVNTLog.get(format, new Object[] { "graduation - successWithAuthorizationCode :" + paramString });
    setAuthorizationCode(paramString);
    isAlreadyBuildingSession = false;
    openSession(paramContext);
  }
  
  public void reset()
  {
    super.reset();
    authorizationCode = null;
  }
  
  public DVNTUserSessionManager setAuthorizationCode(String paramString)
  {
    DVNTLog.write(format, new Object[] { "authorization code has been set : " + paramString });
    authorizationCode = paramString;
    return this;
  }
  
  public void startGraduationProcess(Context paramContext)
  {
    if (isAlreadyBuildingSession)
    {
      DVNTLog.get(format, new Object[] { "Skip graduate already graduating" });
      return;
    }
    isAlreadyBuildingSession = true;
    stop();
    setToken(null);
    DVNTLog.get(format, new Object[] { "start graduation :" });
    apiConfig.getGraduateHandler().handle(paramContext);
  }
}
