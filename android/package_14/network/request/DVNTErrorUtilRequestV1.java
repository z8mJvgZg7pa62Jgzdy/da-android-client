package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTErrorUtilRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTStatus>
{
  private final String code;
  
  public DVNTErrorUtilRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTStatus.class);
    code = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTErrorUtilRequestV1 localDVNTErrorUtilRequestV1 = (DVNTErrorUtilRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(code, code).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(code).toHashCode();
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTStatus sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).errorUtil(paramString, code);
  }
}
