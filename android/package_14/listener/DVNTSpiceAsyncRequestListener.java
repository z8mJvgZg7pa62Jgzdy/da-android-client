package com.deviantart.android.android.package_14.listener;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.network.DVNTRequestManager;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.datoolkit.logger.DVNTLog;
import com.octo.android.robospice.exception.RequestCancelledException;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import java.lang.ref.WeakReference;
import java.util.UUID;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DVNTSpiceAsyncRequestListener<T>
  implements RequestListener<T>
{
  private static final String FORMAT = "DVNTSpiceAsyncRequestListener - {}";
  private com.deviantart.android.sdk.api.listener.DVNTAsyncRequestListener<T> asyncRequestListener;
  private final UUID requestIdent;
  private WeakReference<Context> weakContext;
  
  public DVNTSpiceAsyncRequestListener(Context paramContext, DVNTAsyncRequestListener paramDVNTAsyncRequestListener, UUID paramUUID)
  {
    weakContext = new WeakReference(paramContext);
    asyncRequestListener = paramDVNTAsyncRequestListener;
    requestIdent = paramUUID;
  }
  
  public void onRequestFailure(SpiceException paramSpiceException)
  {
    DVNTLog.get("DVNTSpiceAsyncRequestListener - {}", new Object[] { "onRequestFailure with exception " + paramSpiceException });
    DVNTRequestManager.remove(requestIdent);
    if ((weakContext == null) || (DVNTContextUtils.isContextDead((Context)weakContext.get())))
    {
      DVNTLog.get("DVNTSpiceAsyncRequestListener - {}", new Object[] { "request has been cancelled" });
      return;
    }
    RetrofitError localRetrofitError;
    Object localObject;
    if ((paramSpiceException.getCause() instanceof RetrofitError))
    {
      DVNTLog.get("DVNTSpiceAsyncRequestListener - {}", new Object[] { "cause is a retrofit error" });
      localRetrofitError = (RetrofitError)paramSpiceException.getCause();
      localObject = new com.deviantart.android.android.package_14.model.DVNTEndpointError("retrofit", localRetrofitError.getMessage());
      ((com.deviantart.android.android.package_14.model.DVNTEndpointError)localObject).setHttpStatusCode(Integer.valueOf(500));
      if (localRetrofitError.getResponse() == null) {
        break label276;
      }
    }
    label276:
    for (;;)
    {
      try
      {
        paramSpiceException = (com.deviantart.android.android.package_14.model.DVNTEndpointError)localRetrofitError.getBodyAs(com.deviantart.android.sdk.api.model.DVNTEndpointError.class);
        if (paramSpiceException != null) {
          localObject = paramSpiceException;
        }
        if ((localObject != null) && (localRetrofitError.getResponse() != null) && (localRetrofitError.getResponse().getStatus() > 0) && (localRetrofitError.getResponse().getStatus() == 401)) {
          DVNTAbstractAsyncAPI.logout((Context)weakContext.get());
        }
        ((com.deviantart.android.android.package_14.model.DVNTEndpointError)localObject).setHttpStatusCode(Integer.valueOf(localRetrofitError.getResponse().getStatus()));
        asyncRequestListener.onFailure((com.deviantart.android.android.package_14.model.DVNTEndpointError)localObject);
        return;
      }
      catch (RuntimeException paramSpiceException)
      {
        paramSpiceException = null;
        continue;
      }
      if ((paramSpiceException instanceof RequestCancelledException))
      {
        DVNTLog.get("DVNTSpiceAsyncRequestListener - {}", new Object[] { "request has been cancelled" });
        return;
      }
      DVNTLog.get("DVNTSpiceAsyncRequestListener - {}", new Object[] { "unknown exception, calling onException" });
      asyncRequestListener.onException(paramSpiceException);
      return;
    }
  }
  
  public void onRequestSuccess(Object paramObject)
  {
    DVNTRequestManager.remove(requestIdent);
    if (weakContext != null)
    {
      if (asyncRequestListener.shouldCancel()) {
        return;
      }
      asyncRequestListener.onSuccess(paramObject);
    }
  }
}
