package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.activity.HomeActivity.BottomBarButton;
import com.deviantart.android.damobile.adapter.DiscoveryPagerAdapter;
import com.deviantart.android.damobile.stream.loader.APIBrowseHotLoader;
import com.deviantart.android.damobile.stream.loader.APIBrowseUndiscoveredLoader;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.ZoomOutPageTransformer;
import com.deviantart.android.damobile.util.discovery.DiscoveryPageInfo;
import com.deviantart.android.damobile.util.discovery.pages.DiscoveryCategoryBrowsePage;
import com.deviantart.android.damobile.util.discovery.pages.DiscoveryCuratedTagPage;
import com.deviantart.android.damobile.util.discovery.pages.DiscoveryDailyDeviationPage;
import com.deviantart.android.damobile.util.discovery.pages.DiscoveryPage;
import com.deviantart.android.damobile.util.discovery.pages.DiscoveryTorpedoPage;
import com.deviantart.android.damobile.util.tourhelper.TapAndHoldTourHelper;
import com.deviantart.android.damobile.util.tourhelper.TourHelperBase;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.pacaya.MobileLava;
import com.deviantart.android.damobile.util.tracking.pacaya.UxTopicEventCreator;
import com.deviantart.android.damobile.view.viewpageindicator.CustomizableTabPageIndicator;
import com.deviantart.android.damobile.view.viewpageindicator.DATextTabIndicator;
import com.deviantart.pacaya.Pacaya;
import java.util.Date;

public class DiscoveryFragment
  extends HomeBaseFragment
{
  private DiscoveryPagerAdapter a;
  private Date d;
  @Bind({2131689759})
  DATextTabIndicator indicator;
  private TapAndHoldTourHelper l;
  @Bind({2131689758})
  ViewPager pager;
  
  public DiscoveryFragment() {}
  
  public static DiscoveryFragment a(DiscoveryPageInfo paramDiscoveryPageInfo)
  {
    DiscoveryFragment localDiscoveryFragment = new DiscoveryFragment();
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("mode", paramDiscoveryPageInfo);
    localDiscoveryFragment.setArguments(localBundle);
    return localDiscoveryFragment;
  }
  
  private void a(int paramInt)
  {
    DiscoveryPageInfo localDiscoveryPageInfo = DiscoveryPageInfo.b(paramInt);
    if (localDiscoveryPageInfo != null) {}
    for (EventKeys.Category localCategory = localDiscoveryPageInfo.a();; localCategory = null)
    {
      com.deviantart.android.damobile.util.tracking.TrackerUtil.b = localCategory;
      ((DiscoveryPagerAdapter)pager.getAdapter()).a(getActivity(), paramInt);
      if (localDiscoveryPageInfo != null) {
        break;
      }
      return;
    }
    MobileLava.e().setText(new UxTopicEventCreator().get(localDiscoveryPageInfo.getString()).b());
    if ((getView() != null) && (localDiscoveryPageInfo != DiscoveryPageInfo.o) && (!TapAndHoldTourHelper.putInt(getActivity())))
    {
      l = new TapAndHoldTourHelper(getActivity(), 2131689752);
      l.b(getView(), onSuccess());
    }
  }
  
  private boolean connect()
  {
    return new Date().getTime() - d.getTime() >= 3600000L;
  }
  
  private void onCreateView()
  {
    int i = (int)getActivity().getResources().getDimension(2131361971);
    a = new DiscoveryPagerAdapter();
    DiscoveryTorpedoPage localDiscoveryTorpedoPage = new DiscoveryTorpedoPage(DiscoveryPageInfo.c.getString(), DiscoveryPageInfo.c.get(getActivity()), getString() + "/" + DiscoveryPageInfo.c.b(), APIBrowseUndiscoveredLoader.class);
    localDiscoveryTorpedoPage.a(EventKeys.Category.g);
    localDiscoveryTorpedoPage.b(i);
    localDiscoveryTorpedoPage.c(getString(2131230994));
    localDiscoveryTorpedoPage.d("explanation_undiscovered_visible");
    a.a(localDiscoveryTorpedoPage);
    localDiscoveryTorpedoPage = new DiscoveryTorpedoPage(DiscoveryPageInfo.a.getString(), DiscoveryPageInfo.a.get(getActivity()), getString() + "/" + DiscoveryPageInfo.a.b(), APIBrowseHotLoader.class);
    localDiscoveryTorpedoPage.a(EventKeys.Category.l);
    localDiscoveryTorpedoPage.b(i);
    localDiscoveryTorpedoPage.c(getString(2131230995));
    localDiscoveryTorpedoPage.d("explanation_whats_hot_visible");
    a.a(localDiscoveryTorpedoPage);
    a.a(new DiscoveryDailyDeviationPage(DiscoveryPageInfo.b.getString(), DiscoveryPageInfo.b.get(getActivity()), getString() + "/" + DiscoveryPageInfo.b.b()));
    a.a(new DiscoveryCuratedTagPage(DiscoveryPageInfo.l.getString(), DiscoveryPageInfo.l.get(getActivity()), getString() + "/" + DiscoveryPageInfo.l.b()));
    a.a(new DiscoveryCategoryBrowsePage(DiscoveryPageInfo.o.getString(), DiscoveryPageInfo.o.get(getActivity()), getString() + "/" + DiscoveryPageInfo.o.b()));
  }
  
  protected HomeActivity.BottomBarButton c()
  {
    return HomeActivity.BottomBarButton.i;
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    paramMenuInflater.inflate(2131820548, paramMenu);
    if (DVNTAbstractAsyncAPI.isUserSession(getActivity())) {
      a(paramMenu, paramMenuInflater);
    }
    while ("debug".equals("store"))
    {
      if (DVNTAbstractAsyncAPI.isUserSession(getActivity())) {
        paramMenuInflater.inflate(2131820549, paramMenu);
      }
      paramMenuInflater.inflate(2131820546, paramMenu);
      return;
      paramMenuInflater.inflate(2131820547, paramMenu);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    if (a == null) {
      onCreateView();
    }
    paramBundle = (ViewGroup)paramLayoutInflater.inflate(2130968670, paramViewGroup, false);
    ButterKnife.bind(this, paramBundle);
    if (DVNTAbstractAsyncAPI.isUserSession(getActivity())) {
      onOptionsItemSelected();
    }
    for (;;)
    {
      initUI();
      ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
      toolbar.setLogo(2130837640);
      setExpanded();
      pager.setAdapter(a);
      pager.setPageTransformer(true, new ZoomOutPageTransformer());
      indicator.setViewPager(pager);
      indicator.setOnPageChangeListener(new DiscoveryFragment.1(this));
      Bundle localBundle = getArguments();
      paramViewGroup = null;
      paramLayoutInflater = paramViewGroup;
      if (localBundle != null)
      {
        paramLayoutInflater = paramViewGroup;
        if (localBundle.containsKey("mode"))
        {
          paramLayoutInflater = (DiscoveryPageInfo)localBundle.getSerializable("mode");
          paramLayoutInflater = a.read(paramLayoutInflater.getString());
        }
      }
      paramViewGroup = paramLayoutInflater;
      if (paramLayoutInflater == null) {
        paramViewGroup = Integer.valueOf(a.get(getActivity()));
      }
      pager.setCurrentItem(paramViewGroup.intValue());
      return paramBundle;
      updateContent();
    }
  }
  
  public void onDestroy()
  {
    super.onDestroy();
    a = null;
  }
  
  public void onDestroyView()
  {
    getActivity().getPreferences(0).edit().putInt("last_visited_screen", pager.getCurrentItem()).apply();
    super.onDestroyView();
    ButterKnife.unbind(this);
    if (l != null)
    {
      l.d();
      l = null;
    }
  }
  
  public void onOptionsItemSelected()
  {
    super.onOptionsItemSelected();
    if (UserUtils.d)
    {
      pager.setCurrentItem(a.get(getActivity()));
      UserUtils.d = false;
    }
  }
  
  public void onPause()
  {
    super.onPause();
    com.deviantart.android.damobile.util.tracking.TrackerUtil.b = null;
    d = new Date();
  }
  
  public void onResume()
  {
    super.onResume();
    if ((d != null) && (connect()))
    {
      pager.setAdapter(a);
      pager.setCurrentItem(a.get(getActivity()));
    }
    if (pager.getCurrentItem() == 0) {
      a(0);
    }
  }
}
