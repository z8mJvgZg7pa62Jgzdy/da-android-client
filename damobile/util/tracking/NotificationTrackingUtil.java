package com.deviantart.android.damobile.util.tracking;

import com.deviantart.android.android.package_14.model.DVNTFeedbackMessage;
import com.deviantart.android.android.package_14.model.DVNTFeedbackSubject;
import com.deviantart.android.damobile.util.DeviationType;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.notifications.NotificationItemType;

public class NotificationTrackingUtil
{
  public NotificationTrackingUtil() {}
  
  public static String getString(NotificationItemType paramNotificationItemType, DVNTFeedbackMessage paramDVNTFeedbackMessage)
  {
    switch (NotificationTrackingUtil.1.a[paramNotificationItemType.ordinal()])
    {
    default: 
      return "Unknown";
    case 1: 
    case 2: 
      return "Fave";
    case 3: 
    case 4: 
      return "Collection";
    case 5: 
    case 6: 
      if (paramDVNTFeedbackMessage.getSubject() != null)
      {
        paramNotificationItemType = DeviationUtils.a(paramDVNTFeedbackMessage.getSubject().getSubjectDeviation());
        if (DeviationType.e.equals(paramNotificationItemType)) {
          return "Comment_Journal";
        }
      }
      return "Comment_Deviation";
    case 7: 
    case 8: 
      return "Comment_Profile";
    case 9: 
    case 10: 
      return "Comment_Status";
    case 11: 
    case 12: 
    case 13: 
    case 14: 
    case 15: 
      return "Reply";
    case 16: 
    case 17: 
    case 18: 
      return "Mention_UsernameInComment";
    case 19: 
      return "Mention_UsernameInStatus";
    case 20: 
      paramNotificationItemType = DeviationUtils.a(paramDVNTFeedbackMessage.getDeviation());
      if (DeviationType.e.equals(paramNotificationItemType)) {
        return "Mention_UsernameInJournal";
      }
      return "Mention_UsernameInDeviation";
    case 21: 
    case 22: 
    case 23: 
      return "Mention_DeviationInComment";
    case 24: 
      return "Mention_DeviationInStatus";
    case 25: 
      paramNotificationItemType = DeviationUtils.a(paramDVNTFeedbackMessage.getDeviation());
      if (DeviationType.e.equals(paramNotificationItemType)) {
        return "Mention_DeviationInJournal";
      }
      return "Mention_DeviationInDeviation";
    case 26: 
    case 27: 
      return "Watch";
    case 28: 
      return "DailyDeviation_Received";
    }
    return "DailyDeviation_SuggestionAccepted";
  }
}
