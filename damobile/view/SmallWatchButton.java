package com.deviantart.android.damobile.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import butterknife.Bind;
import butterknife.ButterKnife;

public class SmallWatchButton
  extends AbstractWatchButton
{
  private Boolean e = null;
  @Bind({2131690131})
  ImageView watchButton;
  
  public SmallWatchButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    ButterKnife.bind(this);
  }
  
  private void a(AbstractWatchButton.ButtonState paramButtonState)
  {
    if (e != null) {
      return;
    }
    switch (SmallWatchButton.1.d[paramButtonState.ordinal()])
    {
    default: 
      break;
    }
    while (e != null)
    {
      watchButton.setSelected(e.booleanValue());
      return;
      e = Boolean.valueOf(true);
      continue;
      e = Boolean.valueOf(false);
    }
  }
  
  private void b(AbstractWatchButton.ButtonState paramButtonState)
  {
    if (e == null) {
      return;
    }
    if (((paramButtonState == AbstractWatchButton.ButtonState.d) && (e.booleanValue())) || ((paramButtonState == AbstractWatchButton.ButtonState.c) && (!e.booleanValue())))
    {
      e = null;
      return;
    }
    paramButtonState = watchButton;
    if (!e.booleanValue()) {}
    for (boolean bool = true;; bool = false)
    {
      paramButtonState.setSelected(bool);
      e = null;
      return;
    }
  }
  
  protected void b(AbstractWatchButton.ButtonState paramButtonState1, AbstractWatchButton.ButtonState paramButtonState2)
  {
    if (paramButtonState2 == AbstractWatchButton.ButtonState.b)
    {
      watchButton.setSelected(false);
      return;
    }
    if (!k)
    {
      switch (i.j)
      {
      default: 
        return;
      case 2131231424: 
        watchButton.setSelected(false);
        return;
      }
      watchButton.setSelected(true);
      return;
    }
    if (paramButtonState2 == AbstractWatchButton.ButtonState.i)
    {
      a(paramButtonState1);
      return;
    }
    if (paramButtonState1 == AbstractWatchButton.ButtonState.i) {
      b(paramButtonState2);
    }
  }
  
  protected int getLayoutRes()
  {
    return 2130968800;
  }
  
  protected boolean l()
  {
    return false;
  }
  
  protected void setVisibility() {}
  
  protected void switchToLoadingView() {}
}
