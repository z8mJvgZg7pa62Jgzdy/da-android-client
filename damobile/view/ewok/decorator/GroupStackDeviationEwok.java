package com.deviantart.android.damobile.view.ewok.decorator;

import com.deviantart.android.android.package_14.model.DVNTFeedItem;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APIFeedBucketLoader;
import com.deviantart.android.damobile.view.ewok.WatchFeedCollectionEwok;

public class GroupStackDeviationEwok
  extends WatchFeedCollectionEwok
{
  public GroupStackDeviationEwok(DVNTFeedItem paramDVNTFeedItem)
  {
    super(paramDVNTFeedItem);
  }
  
  public Stream c()
  {
    return StreamCacher.a(new APIFeedBucketLoader(b.getBucketId()), StreamCacheStrategy.a);
  }
  
  public String execute()
  {
    return null;
  }
  
  protected String getTranslation()
  {
    return "Group";
  }
  
  public Integer size()
  {
    return b.getBucketTotal();
  }
}
