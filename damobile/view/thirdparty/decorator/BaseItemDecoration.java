package com.deviantart.android.damobile.view.thirdparty.decorator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;

public class BaseItemDecoration
  extends RecyclerView.ItemDecoration
{
  private Drawable mDivider = null;
  private int size;
  
  private BaseItemDecoration(Context paramContext, int paramInt1, int paramInt2)
  {
    if (paramInt1 != -1) {
      mDivider = ContextCompat.getDrawable(paramContext, paramInt1);
    }
    size = paramInt2;
  }
  
  public void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    super.getItemOffsets(paramRect, paramView, paramRecyclerView, paramState);
    paramRect.set(0, 0, 0, size);
  }
  
  public void onDraw(Canvas paramCanvas, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    super.onDraw(paramCanvas, paramRecyclerView, paramState);
    if (mDivider == null) {
      return;
    }
    int j = paramRecyclerView.getPaddingLeft();
    int k = paramRecyclerView.getWidth();
    int m = paramRecyclerView.getPaddingRight();
    int n = paramRecyclerView.getChildCount();
    int i = 0;
    while (i < n)
    {
      paramState = paramRecyclerView.getChildAt(i);
      RecyclerView.LayoutParams localLayoutParams = (RecyclerView.LayoutParams)paramState.getLayoutParams();
      int i1 = paramState.getBottom();
      i1 = bottomMargin + i1;
      int i2 = mDivider.getIntrinsicHeight();
      mDivider.setBounds(j, i1, k - m, i2 + i1);
      mDivider.draw(paramCanvas);
      i += 1;
    }
  }
}
