package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.os.Bundle;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.MultiStepActivity;

public abstract class MultiStepFragment
  extends DABaseFragment
{
  public MultiStepFragment() {}
  
  public void b(Bundle paramBundle) {}
  
  public void saveAndExit()
  {
    if (DVNTContextUtils.isContextDead(getActivity())) {
      return;
    }
    ((MultiStepActivity)getActivity()).showImage();
  }
  
  public Bundle updateSettings()
  {
    return new Bundle();
  }
}
