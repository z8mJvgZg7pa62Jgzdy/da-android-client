package com.deviantart.android.damobile.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import java.io.Serializable;

public class SubmitTag
  implements SubmitSecondaryFragment.SubmitOptionalElement, Serializable
{
  private String dateString;
  
  public SubmitTag(String paramString)
  {
    dateString = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    return ((paramObject instanceof SubmitTag)) && (((SubmitTag)paramObject).getValue().equals(getValue()));
  }
  
  public String getValue()
  {
    return dateString;
  }
  
  public View getView(Context paramContext)
  {
    TextView localTextView = (TextView)LayoutInflater.from(paramContext).inflate(2130968778, null, false);
    LinearLayout.LayoutParams localLayoutParams = new LinearLayout.LayoutParams(-2, -2);
    localLayoutParams.setMargins(0, 0, paramContext.getResources().getDimensionPixelSize(2131361974), 0);
    localTextView.setLayoutParams(localLayoutParams);
    localTextView.setText(dateString);
    return localTextView;
  }
}
