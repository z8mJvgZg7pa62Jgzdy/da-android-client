package com.deviantart.android.damobile.view.notes;

import android.content.Context;
import com.deviantart.android.damobile.fragment.NotesFragment;
import com.deviantart.android.damobile.stream.FilteredStream;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.filter.NotesModelFilter;
import com.deviantart.android.damobile.stream.filter.StreamFilter;
import com.deviantart.android.damobile.stream.loader.APINotesFolderLoader;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnNotesRefreshAll;
import com.deviantart.android.damobile.util.notes.NotesPage;
import com.deviantart.android.damobile.view.DASwipeRefreshLayout;
import com.deviantart.android.damobile.view.opt.MCListAdapterBase;
import com.deviantart.android.damobile.view.opt.MCListFrameBase;
import com.deviantart.android.damobile.view.opt.MCListRecyclerView;
import com.squareup.otto.Bus;
import java.util.HashSet;

public class NotesListFrame
  extends MCListFrameBase
{
  private int u;
  
  public NotesListFrame(Context paramContext)
  {
    super(paramContext);
  }
  
  public NotesListRecyclerView getApiUrl(Context paramContext)
  {
    return new NotesListRecyclerView(paramContext);
  }
  
  public NotesListRecyclerView getRecyclerView()
  {
    return (NotesListRecyclerView)d;
  }
  
  public void refreshView()
  {
    BusStation.get().post(new BusStation.OnNotesRefreshAll());
  }
  
  public void setScrollUnderSize(int paramInt)
  {
    refreshLayout.setProgressViewOffset(paramInt);
    u = paramInt;
  }
  
  public void visitAttribute()
  {
    super.refreshView();
  }
  
  public void visitFrame(NotesPage paramNotesPage)
  {
    Object localObject = paramNotesPage.b();
    NotesFragment.c.add(((APINotesFolderLoader)localObject).b());
    localObject = new FilteredStream(StreamCacher.a((StreamLoader)localObject), new StreamFilter[] { new NotesModelFilter() });
    paramNotesPage = new NotesListAdapter((Stream)localObject, paramNotesPage.getString());
    paramNotesPage.putShort(u);
    getRecyclerView().setAdapterSafe(paramNotesPage);
    if (((Stream)localObject).size() <= 0) {
      visitFrame();
    }
  }
}
