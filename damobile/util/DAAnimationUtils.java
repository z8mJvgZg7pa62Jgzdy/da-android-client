package com.deviantart.android.damobile.util;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class DAAnimationUtils
{
  public DAAnimationUtils() {}
  
  public static void show(View paramView, int paramInt, boolean paramBoolean, DAAnimationUtils.AnimationEndListener paramAnimationEndListener)
  {
    Animation localAnimation = AnimationUtils.loadAnimation(paramView.getContext(), paramInt);
    localAnimation.setAnimationListener(new DAAnimationUtils.1(paramBoolean, paramView, paramAnimationEndListener));
    paramView.startAnimation(localAnimation);
  }
}
