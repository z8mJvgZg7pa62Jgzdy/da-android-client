package com.deviantart.android.damobile.view.thirdparty.deviation;

import com.deviantart.android.damobile.view.thirdparty.GomType;

public class UnknownGom
  extends PlaceholderGom
{
  public UnknownGom() {}
  
  public GomType b()
  {
    return GomType.r;
  }
}
