package com.deviantart.android.damobile.fragment;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.BaseBundle;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;

public class WelcomeDialogFragment
  extends DialogFragment
{
  public WelcomeDialogFragment() {}
  
  public static WelcomeDialogFragment access$000(String paramString, boolean paramBoolean)
  {
    WelcomeDialogFragment localWelcomeDialogFragment = new WelcomeDialogFragment();
    Bundle localBundle = new Bundle();
    localBundle.putString("username", paramString);
    localBundle.putBoolean("welcome_dialog_type", paramBoolean);
    localWelcomeDialogFragment.setArguments(localBundle);
    return localWelcomeDialogFragment;
  }
  
  private Spannable getTitle()
  {
    String str = getActivity().getString(2131231420);
    if (getArguments().getString("username") == null) {
      return new SpannableString(str);
    }
    int i = str.length();
    str = str + " " + getArguments().getString("username");
    SpannableString localSpannableString = new SpannableString(str);
    localSpannableString.setSpan(new StyleSpan(1), i + 1, str.length(), 33);
    return localSpannableString;
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    boolean bool = getArguments().getBoolean("welcome_dialog_type");
    paramBundle = new AlertDialog.Builder(getActivity());
    paramBundle.setTitle(2131231419).setMessage(getTitle());
    if (bool) {
      paramBundle.setPositiveButton(2131231183, new WelcomeDialogFragment.1(this));
    }
    for (;;)
    {
      return paramBundle.create();
      paramBundle.setNegativeButton(2131230820, new WelcomeDialogFragment.3(this)).setPositiveButton(2131231054, new WelcomeDialogFragment.2(this));
    }
  }
}
