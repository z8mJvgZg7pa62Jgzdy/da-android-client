package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTPaginatedResult<T>
  implements Serializable
{
  @SerializedName("has_more")
  protected boolean hasMore;
  @SerializedName("next_offset")
  protected Integer nextOffset;
  @SerializedName("results")
  protected T results;
  
  public DVNTPaginatedResult() {}
  
  public boolean getHasMore()
  {
    return hasMore;
  }
  
  public Integer getNextOffset()
  {
    return nextOffset;
  }
  
  public Object getResults()
  {
    return results;
  }
  
  public boolean isHasMore()
  {
    return hasMore;
  }
  
  public void setHasMore(boolean paramBoolean)
  {
    hasMore = paramBoolean;
  }
  
  public void setNextOffset(Integer paramInteger)
  {
    nextOffset = paramInteger;
  }
  
  public void setResults(Object paramObject)
  {
    results = paramObject;
  }
}
