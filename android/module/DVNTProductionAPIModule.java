package com.deviantart.android.android.module;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTAPIClient;
import com.deviantart.android.android.package_14.config.DVNTAPIConfig;
import com.deviantart.android.android.package_14.config.DVNTAPIModule;
import com.deviantart.android.android.package_14.network.DVNTRequestInterceptor;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthHelper;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthPreferencesManager;
import com.deviantart.android.android.package_14.session.DVNTClientSessionManager;
import com.deviantart.android.android.package_14.session.DVNTSessionManager;
import com.deviantart.android.android.package_14.session.DVNTUserSessionManager;
import com.deviantart.android.sdk.api.network.DVNTRetrofitSpiceService;
import com.octo.android.robospice.SpiceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit.RestAdapter;
import retrofit.RestAdapter.Builder;

public class DVNTProductionAPIModule
  implements DVNTAPIModule
{
  private DVNTAPIConfig apiConfig;
  
  public DVNTProductionAPIModule() {}
  
  public void init(Context paramContext, DVNTAPIConfig paramDVNTAPIConfig)
  {
    apiConfig = paramDVNTAPIConfig;
  }
  
  DVNTOAuthPreferencesManager provideesOauthPrefsManager()
  {
    return new DVNTOAuthPreferencesManager();
  }
  
  DVNTAPIClient providesAPIClient(DVNTUserSessionManager paramDVNTUserSessionManager, DVNTClientSessionManager paramDVNTClientSessionManager, DVNTSessionManager paramDVNTSessionManager)
  {
    return new DVNTAPIClient(paramDVNTSessionManager, paramDVNTUserSessionManager, paramDVNTClientSessionManager);
  }
  
  DVNTSessionManager providesAnonSessionManager(SpiceManager paramSpiceManager)
  {
    return new DVNTSessionManager(paramSpiceManager);
  }
  
  DVNTClientSessionManager providesClientSessionManager(com.deviantart.android.android.package_14.session.DVNTRestTokenService paramDVNTRestTokenService, SpiceManager paramSpiceManager, DVNTOAuthPreferencesManager paramDVNTOAuthPreferencesManager, DVNTOAuthHelper paramDVNTOAuthHelper)
  {
    return new DVNTClientSessionManager(paramDVNTRestTokenService, paramSpiceManager, paramDVNTOAuthPreferencesManager, paramDVNTOAuthHelper, apiConfig);
  }
  
  Logger providesLogger()
  {
    return LoggerFactory.getLogger("DVNT-SDK");
  }
  
  com.deviantart.android.android.package_14.session.DVNTRestTokenService providesRestTokenService(String paramString)
  {
    return (com.deviantart.android.android.package_14.session.DVNTRestTokenService)new RestAdapter.Builder().setEndpoint(paramString).setRequestInterceptor(new DVNTRequestInterceptor()).build().create(com.deviantart.android.sdk.api.session.DVNTRestTokenService.class);
  }
  
  String providesServerURL()
  {
    return "https://www.deviantart.com";
  }
  
  SpiceManager providesSpiceManager()
  {
    return new SpiceManager(DVNTRetrofitSpiceService.class);
  }
  
  DVNTUserSessionManager providesUserSessionManager(com.deviantart.android.android.package_14.session.DVNTRestTokenService paramDVNTRestTokenService, SpiceManager paramSpiceManager, DVNTOAuthPreferencesManager paramDVNTOAuthPreferencesManager, DVNTOAuthHelper paramDVNTOAuthHelper)
  {
    return new DVNTUserSessionManager(paramDVNTRestTokenService, paramSpiceManager, paramDVNTOAuthPreferencesManager, paramDVNTOAuthHelper, apiConfig);
  }
  
  DVNTOAuthHelper providesoAuthHelper()
  {
    return new DVNTOAuthHelper();
  }
}
