package com.deviantart.android.damobile.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.TextView;

public class RecipientEditText
  extends EditText
{
  public RecipientEditText(Context paramContext)
  {
    super(paramContext);
  }
  
  public RecipientEditText(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public RecipientEditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public void listFiles()
  {
    setText(" ");
  }
  
  protected void onSelectionChanged(int paramInt1, int paramInt2)
  {
    super.onSelectionChanged(paramInt1, paramInt2);
    if ((paramInt1 == 0) && (getText().length() > 0)) {
      setSelection(1, paramInt2);
    }
  }
}
