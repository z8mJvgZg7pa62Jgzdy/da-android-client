package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTDeviationDownloadInfoRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTDeviationDownloadInfo>
{
  String deviationId;
  
  public DVNTDeviationDownloadInfoRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTDeviationDownloadInfo.class);
    deviationId = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()) && (super.equals(paramObject)))
    {
      paramObject = (DVNTDeviationDownloadInfoRequestV1)paramObject;
      return Objects.append(deviationId, deviationId);
    }
    return false;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), deviationId });
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTDeviationDownloadInfo sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).deviationDownload(paramString, deviationId);
  }
}
