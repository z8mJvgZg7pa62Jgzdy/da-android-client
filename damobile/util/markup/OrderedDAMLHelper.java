package com.deviantart.android.damobile.util.markup;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDAMLComponent;
import com.deviantart.android.android.package_14.model.DVNTDAMLComponent.List;
import com.deviantart.android.android.package_14.model.DVNTDAMLComponent.Type;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTStashItem;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StaticStreamLoader;
import com.deviantart.android.damobile.util.DAFont;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.PlaceholderUtil;
import com.deviantart.android.damobile.util.ProcessMenuListener;
import com.deviantart.android.damobile.view.DAFaveView;
import com.deviantart.android.damobile.view.LongPressItem;
import com.deviantart.android.damobile.view.ewok.Ewok;
import com.deviantart.android.damobile.view.ewok.EwokFactory;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;
import java.util.Iterator;

public class OrderedDAMLHelper
{
  public OrderedDAMLHelper() {}
  
  private static SimpleDraweeView a(Context paramContext)
  {
    paramContext = new SimpleDraweeView(paramContext);
    paramContext.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
    return paramContext;
  }
  
  public static void a(Activity paramActivity, ViewGroup paramViewGroup, DVNTDAMLComponent.List paramList, boolean paramBoolean, ProcessMenuListener paramProcessMenuListener)
  {
    StaticStreamLoader localStaticStreamLoader = new StaticStreamLoader("staticstreamloader-deviations-daml" + paramList.hashCode());
    Stream localStream = StreamCacher.a(localStaticStreamLoader);
    ArrayList localArrayList = new ArrayList();
    Resources localResources = paramActivity.getResources();
    paramList = paramList.iterator();
    while (paramList.hasNext())
    {
      Object localObject1 = (DVNTDAMLComponent)paramList.next();
      if ((!localArrayList.isEmpty()) && (((DVNTDAMLComponent)localObject1).getType() != DVNTDAMLComponent.Type.HTML)) {
        a(paramActivity, paramViewGroup, localArrayList, paramBoolean);
      }
      Log.d("OrderedDAML", "Encountered component type: " + ((DVNTDAMLComponent)localObject1).getType().name());
      Object localObject2;
      switch (OrderedDAMLHelper.2.H[localObject1.getType().ordinal()])
      {
      default: 
        Log.d("OrderedDAML", "Unhandled component type was encountered");
        break;
      case 1: 
        localArrayList.add(localObject1);
        break;
      case 2: 
        localObject1 = ((DVNTDAMLComponent)localObject1).getStashMetadata();
        if (localObject1 != null)
        {
          localObject2 = a(paramActivity);
          ((View)localObject2).setPadding(0, (int)localResources.getDimension(2131361963), 0, (int)localResources.getDimension(2131361963));
          if (MarkupHelper.a(paramActivity, (SimpleDraweeView)localObject2, (DVNTStashItem)localObject1))
          {
            b(paramViewGroup, (View)localObject2);
          }
          else
          {
            localObject2 = b(paramActivity, paramBoolean);
            if (MarkupHelper.a(paramActivity, (TextView)localObject2, (DVNTStashItem)localObject1)) {
              b(paramViewGroup, (View)localObject2);
            }
          }
        }
        break;
      case 3: 
      case 4: 
        localObject1 = ((DVNTDAMLComponent)localObject1).getDeviation();
        localObject2 = new DAFaveView(paramActivity);
        ((View)localObject2).setPadding(0, (int)localResources.getDimension(2131361963), 0, (int)localResources.getDimension(2131361963));
        if (((DVNTAbstractDeviation)localObject1).isDeleted().booleanValue())
        {
          createDialog(paramActivity, (DAFaveView)localObject2);
          b(paramViewGroup, (View)localObject2);
          return;
        }
        localStaticStreamLoader.a(localObject1);
        int i = Math.max(localStaticStreamLoader.c() - 1, 0);
        View localView = EwokFactory.b((DVNTDeviation)localObject1).a(paramActivity, (ViewGroup)localObject2);
        localView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        ((ViewGroup)localObject2).addView(localView, 0);
        DeviationUtils.b((DAFaveView)localObject2, (DVNTDeviation)localObject1);
        ((View)localObject2).setTag(((DVNTAbstractDeviation)localObject1).getId());
        localObject1 = LongPressItem.a((View)localObject2, DeviationUtils.b(paramActivity, (DVNTDeviation)localObject1));
        ((LongPressItem)localObject1).b(new OrderedDAMLHelper.1(paramActivity, localStream, i));
        ((LongPressItem)localObject1).b(DeviationUtils.b(paramProcessMenuListener));
        b(paramViewGroup, ((LongPressItem)localObject1).b());
        break;
      case 5: 
        b(paramViewGroup, setContentView(paramActivity));
      }
    }
    if (!localArrayList.isEmpty()) {
      a(paramActivity, paramViewGroup, localArrayList, paramBoolean);
    }
  }
  
  private static void a(Activity paramActivity, ViewGroup paramViewGroup, ArrayList paramArrayList, boolean paramBoolean)
  {
    Iterator localIterator = paramArrayList.iterator();
    DVNTDAMLComponent localDVNTDAMLComponent;
    for (String str = ""; localIterator.hasNext(); str = str + localDVNTDAMLComponent.getFallbackHTML()) {
      localDVNTDAMLComponent = (DVNTDAMLComponent)localIterator.next();
    }
    paramArrayList.clear();
    paramArrayList = b(paramActivity, paramBoolean);
    MarkupHelper.onPostExecute(paramActivity, str, paramArrayList, false);
    b(paramViewGroup, paramArrayList);
  }
  
  private static TextView b(Context paramContext, boolean paramBoolean)
  {
    TextView localTextView = new TextView(paramContext);
    localTextView.setTypeface(DAFont.l.get(paramContext));
    localTextView.setTextSize(0, paramContext.getResources().getDimension(2131361979));
    localTextView.setClickable(true);
    localTextView.setLinksClickable(true);
    localTextView.setMovementMethod(LinkMovementMethod.getInstance());
    if (paramBoolean) {
      localTextView.setTextColor(paramContext.getResources().getColor(2131558419));
    }
    return localTextView;
  }
  
  private static void b(ViewGroup paramViewGroup, View paramView)
  {
    paramViewGroup.addView(paramView, new ViewGroup.LayoutParams(-1, -2));
  }
  
  private static void createDialog(Activity paramActivity, DAFaveView paramDAFaveView)
  {
    paramActivity = PlaceholderUtil.createView((LayoutInflater)paramActivity.getSystemService("layout_inflater"), 2131231215);
    paramActivity.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
    paramDAFaveView.setVisibility(0);
    paramDAFaveView.addView(paramActivity, 0);
  }
  
  private static TextView setContentView(Activity paramActivity)
  {
    return PlaceholderUtil.createView((LayoutInflater)paramActivity.getSystemService("layout_inflater"), 2131231216);
  }
  
  public static void update(Activity paramActivity, ViewGroup paramViewGroup, DVNTDAMLComponent.List paramList, ProcessMenuListener paramProcessMenuListener)
  {
    a(paramActivity, paramViewGroup, paramList, false, paramProcessMenuListener);
  }
}
