package com.deviantart.android.damobile.view.thirdparty.comment;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.deviantart.android.android.package_14.model.DVNTComment;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.markup.FallbackDAMLContent;
import com.deviantart.android.damobile.util.markup.FallbackDAMLContent.Builder;
import com.deviantart.android.damobile.util.markup.MarkupHelper;
import com.deviantart.android.damobile.util.threaditem.CommentItem;
import com.deviantart.android.damobile.view.thirdparty.GomType;

public class RegularCommentGom
  extends com.deviantart.android.damobile.view.gom.comment.CommentGom<com.deviantart.android.damobile.view.gom.comment.viewholder.RegularCommentViewHolder>
{
  public RegularCommentGom() {}
  
  public com.deviantart.android.damobile.view.thirdparty.comment.viewholder.RegularCommentViewHolder a(Context paramContext, ViewGroup paramViewGroup)
  {
    return new com.deviantart.android.damobile.view.thirdparty.comment.viewholder.RegularCommentViewHolder(LayoutInflater.from(paramContext).inflate(2130968620, paramViewGroup, false));
  }
  
  public void a(Context paramContext, CommentItem paramCommentItem, com.deviantart.android.damobile.view.thirdparty.comment.viewholder.RegularCommentViewHolder paramRegularCommentViewHolder)
  {
    super.a(paramContext, paramCommentItem, paramRegularCommentViewHolder);
    Object localObject2 = (Activity)paramContext;
    if (DVNTContextUtils.isContextDead((Context)localObject2)) {
      return;
    }
    Object localObject1 = paramCommentItem.intValue();
    MarkupHelper.onPostExecute((Activity)localObject2, new FallbackDAMLContent.Builder().c(((DVNTComment)localObject1).getBody()).a().c(), commentContent, false);
    commentContent.setOnTouchListener(new RegularCommentGom.FixTextViewTouchStealingFromListView());
    commentAuthor.setText(((DVNTComment)localObject1).getUser().getUserName());
    if ((((DVNTComment)localObject1).getUser().getUserName() != null) && (((DVNTComment)localObject1).getUser().getUserName().equals(s())))
    {
      commentAuthor.setTextColor(paramContext.getResources().getColor(2131558438));
      commentAuthor.setOnClickListener(new RegularCommentGom.1(this, (DVNTComment)localObject1));
      commentAuthorAvatar.setOnClickListener(new RegularCommentGom.2(this, (DVNTComment)localObject1));
      ImageUtils.isEmpty(commentAuthorAvatar, Uri.parse(((DVNTComment)localObject1).getUser().getUserIconURL()));
      localObject2 = DAFormatUtils.format(paramContext, ((DVNTComment)localObject1).getSubmissionDate());
      if ((localObject2 != null) && (!((String)localObject2).equals(((DVNTComment)localObject1).getSubmissionDate()))) {
        break label353;
      }
      commentDate.setText("");
      label223:
      localObject1 = paramContext.getResources().getIntArray(2131623936);
      if (paramCommentItem.get().intValue() <= 0) {
        break label415;
      }
      if (paramCommentItem.get().intValue() >= localObject1.length - 1) {
        break label365;
      }
      commentLayout.setBackgroundColor(localObject1[(paramCommentItem.get().intValue() + 1)]);
    }
    for (;;)
    {
      if ((c() <= 0) || (paramCommentItem.get().intValue() < c())) {
        break label380;
      }
      commentContainer.setPadding(Graphics.add(paramContext, 25), commentContainer.getPaddingTop(), commentContainer.getPaddingRight(), commentContainer.getPaddingBottom());
      return;
      commentAuthor.setTextColor(paramContext.getResources().getColor(2131558439));
      break;
      label353:
      commentDate.setText((CharSequence)localObject2);
      break label223;
      label365:
      commentLayout.setBackgroundColor(localObject1[7]);
    }
    label380:
    commentContainer.setPadding(Graphics.add(paramContext, 10), commentContainer.getPaddingTop(), commentContainer.getPaddingRight(), commentContainer.getPaddingBottom());
    return;
    label415:
    if (paramCommentItem.visitAnnotation().booleanValue()) {
      commentLayout.setBackgroundColor(localObject1[1]);
    }
    for (;;)
    {
      commentContainer.setPadding(Graphics.add(paramContext, 10), commentContainer.getPaddingTop(), commentContainer.getPaddingRight(), commentContainer.getPaddingBottom());
      return;
      commentLayout.setBackgroundColor(localObject1[0]);
    }
  }
  
  public GomType b()
  {
    return GomType.g;
  }
}
