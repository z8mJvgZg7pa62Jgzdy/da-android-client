package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.damobile.util.notifications.NotificationItemData;

public class APINotificationsAllLoader
  extends StreamLoader<NotificationItemData>
{
  private String e = "";
  
  public APINotificationsAllLoader() {}
  
  public String b()
  {
    return "allnotifications";
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    if (paramBoolean) {
      e = "";
    }
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTAsyncAPI.feedbackAllStackedMessages(null, e, Integer.valueOf(2));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APINotificationsAllLoader.1(this, paramStreamLoadListener));
  }
}
