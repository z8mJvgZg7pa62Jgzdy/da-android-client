package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.FrameLayout;
import com.deviantart.android.android.package_14.model.DVNTFeedItem;
import com.deviantart.android.damobile.util.FeedItemType;
import com.deviantart.android.damobile.util.ProcessMenuListener;
import com.deviantart.android.damobile.view.ewok.EwokFactory;
import com.deviantart.android.damobile.view.ewok.decorator.WatchFeedItemEwok;

public class WatchFeedItemLayout
  extends FrameLayout
{
  ProcessMenuListener J;
  FeedItemType d;
  
  public WatchFeedItemLayout(Context paramContext)
  {
    super(paramContext);
    setLayoutParams(new AbsListView.LayoutParams(-1, -2));
  }
  
  public void a(DVNTFeedItem paramDVNTFeedItem, ViewGroup paramViewGroup)
  {
    Log.d("watchfeed", "populate - start");
    if (paramDVNTFeedItem == null)
    {
      Log.d("watchfeed", "populate - item is null, returning");
      return;
    }
    FeedItemType localFeedItemType = FeedItemType.getValue(paramDVNTFeedItem.getType());
    if (localFeedItemType == null)
    {
      Log.d("watchfeed", "populate - feed item type is null, returning");
      return;
    }
    paramDVNTFeedItem = EwokFactory.b(paramDVNTFeedItem);
    if (paramDVNTFeedItem == null)
    {
      Log.d("watchfeed", "populate - feed item ewok is null, returning");
      return;
    }
    paramDVNTFeedItem.b(new WatchFeedItemLayout.1(this));
    if ((d == null) || (!d.equals(localFeedItemType)) || (d.equals(FeedItemType.e)) || (d.equals(FeedItemType.b)))
    {
      d = localFeedItemType;
      Log.d("watchfeed", "populate - upodateFeedView - canrecycle = false");
      paramDVNTFeedItem.onCreateView((Activity)getContext(), paramViewGroup, this, false);
      return;
    }
    Log.d("watchfeed", "populate - upodateFeedView - canrecycle = true");
    paramDVNTFeedItem.onCreateView((Activity)getContext(), paramViewGroup, this, true);
  }
  
  public void setProcessMenuListener(ProcessMenuListener paramProcessMenuListener)
  {
    J = paramProcessMenuListener;
  }
}
