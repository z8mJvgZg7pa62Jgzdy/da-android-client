package com.deviantart.android.damobile.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

public class ScrollDetectableScrollView
  extends ScrollView
{
  private ScrollDetectedListener mCurrentView;
  
  public ScrollDetectableScrollView(Context paramContext)
  {
    super(paramContext);
  }
  
  public ScrollDetectableScrollView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public ScrollDetectableScrollView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onScrollChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (mCurrentView != null) {
      mCurrentView.reuse(this, paramInt1, paramInt2, paramInt3, paramInt4);
    }
  }
  
  public void setScrollDetectedListener(ScrollDetectedListener paramScrollDetectedListener)
  {
    mCurrentView = paramScrollDetectedListener;
  }
}
