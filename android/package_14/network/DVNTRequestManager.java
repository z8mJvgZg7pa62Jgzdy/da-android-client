package com.deviantart.android.android.package_14.network;

import java.lang.ref.WeakReference;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class DVNTRequestManager
{
  private static ConcurrentHashMap<UUID, WeakReference<com.deviantart.android.sdk.api.network.request.DVNTBaseAsyncRequest>> requests = new ConcurrentHashMap();
  
  public DVNTRequestManager() {}
  
  public static void clear()
  {
    requests.clear();
  }
  
  public static UUID create(com.deviantart.android.android.package_14.network.request.DVNTBaseAsyncRequest paramDVNTBaseAsyncRequest)
  {
    UUID localUUID = UUID.randomUUID();
    requests.put(localUUID, new WeakReference(paramDVNTBaseAsyncRequest));
    return localUUID;
  }
  
  public static com.deviantart.android.android.package_14.network.request.DVNTBaseAsyncRequest getRequest(UUID paramUUID)
  {
    if (requests.containsKey(paramUUID)) {
      return (com.deviantart.android.android.package_14.network.request.DVNTBaseAsyncRequest)((WeakReference)requests.get(paramUUID)).get();
    }
    return null;
  }
  
  public static ConcurrentHashMap getRequests()
  {
    return requests;
  }
  
  public static void remove(UUID paramUUID)
  {
    requests.remove(paramUUID);
  }
}
