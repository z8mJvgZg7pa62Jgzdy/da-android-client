package com.deviantart.android.damobile.adapter.recyclerview;

import android.os.Handler;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.TextView;
import com.deviantart.android.android.package_14.model.DVNTDeviationMetadata;
import com.deviantart.android.android.package_14.model.DVNTGallection;
import com.deviantart.android.damobile.stream.Stream;
import java.util.ArrayList;

public class DeviationFavePanelAdapter
  extends DAStateRecyclerViewAdapter<com.deviantart.android.sdk.api.model.DVNTCollection>
{
  private DeviationFavePanelAdapter.ItemSelectedListener j;
  private DVNTDeviationMetadata o;
  
  public DeviationFavePanelAdapter(Stream paramStream, DeviationFavePanelAdapter.ItemSelectedListener paramItemSelectedListener)
  {
    super(paramStream);
    j = paramItemSelectedListener;
  }
  
  public void a(DVNTDeviationMetadata paramDVNTDeviationMetadata)
  {
    o = paramDVNTDeviationMetadata;
    try
    {
      c();
      return;
    }
    catch (Exception paramDVNTDeviationMetadata)
    {
      new Handler().post(DeviationFavePanelAdapter..Lambda.1.c(this));
    }
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    return new DeviationFavePanelAdapter.FolderViewHolder(LayoutInflater.from(paramViewGroup.getContext()).inflate(2130968664, paramViewGroup, false));
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    com.deviantart.android.android.package_14.model.DVNTCollection localDVNTCollection = (com.deviantart.android.android.package_14.model.DVNTCollection)get().get(paramInt);
    if (o == null) {
      return;
    }
    boolean bool = o.getCollections().contains(localDVNTCollection);
    paramViewHolder = (DeviationFavePanelAdapter.FolderViewHolder)paramViewHolder;
    favFolderName.setText(localDVNTCollection.getName());
    favFolderName.setSelected(bool);
    favFolderName.setChecked(bool);
    if (j != null) {
      itemView.setOnClickListener(DeviationFavePanelAdapter..Lambda.2.finish(this, localDVNTCollection));
    }
  }
  
  public DVNTDeviationMetadata getOrigin()
  {
    return o;
  }
}
