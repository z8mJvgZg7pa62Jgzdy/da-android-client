package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTCommentsDeviationRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTDeviationCommentsThread>
{
  private final String commentId;
  private final String deviationId;
  private final Integer limit;
  private final Integer offset;
  
  public DVNTCommentsDeviationRequestV1(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTDeviationCommentsThread.class);
    offset = paramInteger1;
    limit = paramInteger2;
    commentId = paramString2;
    deviationId = paramString1;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTCommentsDeviationRequestV1 localDVNTCommentsDeviationRequestV1 = (DVNTCommentsDeviationRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(deviationId, deviationId).append(commentId, commentId).append(offset, offset).append(limit, limit).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "comment_deviation" + deviationId + commentId + offset + limit;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(deviationId).append(commentId).append(offset).append(limit).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTDeviationCommentsThread sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).commentsDeviation(paramString, deviationId, commentId, offset, limit, Boolean.valueOf(true));
  }
}
