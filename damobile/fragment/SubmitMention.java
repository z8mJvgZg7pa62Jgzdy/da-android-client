package com.deviantart.android.damobile.fragment;

import android.content.Context;
import android.view.View;
import com.deviantart.android.damobile.util.UserUtils;
import java.io.Serializable;

public class SubmitMention
  implements SubmitSecondaryFragment.SubmitOptionalElement, Serializable
{
  private String b;
  private String d;
  
  public SubmitMention(String paramString1, String paramString2)
  {
    b = paramString1;
    d = paramString2;
  }
  
  public String e()
  {
    return d;
  }
  
  public boolean equals(Object paramObject)
  {
    return ((paramObject instanceof SubmitMention)) && (((SubmitMention)paramObject).getValue().equals(getValue()));
  }
  
  public String getValue()
  {
    return b;
  }
  
  public View getView(Context paramContext)
  {
    return UserUtils.a(paramContext, b, d);
  }
}
