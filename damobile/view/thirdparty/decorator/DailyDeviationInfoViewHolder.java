package com.deviantart.android.damobile.view.thirdparty.decorator;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;

public class DailyDeviationInfoViewHolder
  extends DecoratorViewHolder
{
  @Bind({2131689645})
  public TextView commentView;
  @Bind({2131689643})
  public ViewGroup ddCommentContainer;
  @Bind({2131689649})
  public TextView ddHeader;
  @Bind({2131689646})
  public ViewGroup ddUserInfoContainer;
  @Bind({2131689648})
  public TextView featurerView;
  @Bind({2131689647})
  public TextView suggesterView;
  
  public DailyDeviationInfoViewHolder(RecyclerView.ViewHolder paramViewHolder, View paramView)
  {
    super(paramViewHolder, paramView);
    ButterKnife.bind(this, paramView);
  }
}
