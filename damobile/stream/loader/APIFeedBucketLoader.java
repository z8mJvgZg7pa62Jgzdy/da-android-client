package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTDeviation;

public class APIFeedBucketLoader
  extends StreamLoader<DVNTDeviation>
{
  String d;
  
  public APIFeedBucketLoader(String paramString)
  {
    d = paramString;
  }
  
  public String b()
  {
    return "feedbucketloader" + d;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.feedBucket(d, Integer.valueOf(paramInt), Integer.valueOf(a));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APIFeedBucketLoader.1(this, paramStreamLoadListener));
  }
}
