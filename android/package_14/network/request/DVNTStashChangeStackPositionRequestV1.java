package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTStashChangeStackPositionRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  private Long movedStackId;
  private Integer position;
  
  public DVNTStashChangeStackPositionRequestV1(Long paramLong, Integer paramInteger)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    movedStackId = paramLong;
    position = paramInteger;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTStashChangeStackPositionRequestV1)paramObject;
    return (Objects.append(movedStackId, movedStackId)) && (Objects.append(position, position));
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), movedStackId, position });
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).stashChangeStackPosition(paramString, movedStackId, position);
  }
}
