package com.deviantart.android.damobile.view.notifications;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessageStack;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APINotificationsStackLoader;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.notifications.NotificationItemData;
import com.deviantart.android.damobile.util.notifications.NotificationsItemViewHolder;
import com.deviantart.android.damobile.util.notifications.NotificationsPage;
import com.deviantart.android.damobile.util.opt.ItemDeleteHelperBase;
import com.deviantart.android.damobile.util.opt.ItemDeleteType;
import com.deviantart.android.damobile.util.opt.ItemDeleteUtils;
import com.deviantart.android.damobile.util.opt.NotificationItemDeleteHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class NotificationsListAdapter
  extends com.deviantart.android.damobile.view.mc.MCListAdapterBase<NotificationItemData, NotificationsPage>
{
  private String g;
  private NotificationsPage t;
  
  public NotificationsListAdapter(Stream paramStream, NotificationsPage paramNotificationsPage)
  {
    c = paramStream;
    t = paramNotificationsPage;
    b();
  }
  
  public NotificationsListAdapter(Stream paramStream, String paramString)
  {
    c = paramStream;
    g = paramString;
    b();
  }
  
  public void a()
  {
    if (c == null)
    {
      super.a();
      return;
    }
    Iterator localIterator = c.get().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (NotificationItemData)localIterator.next();
      if (((NotificationItemData)localObject).get())
      {
        localObject = new APINotificationsStackLoader(((NotificationItemData)localObject).getString().getStackId());
        if (StreamCacher.b((StreamLoader)localObject)) {
          StreamCacher.a((StreamLoader)localObject).a();
        }
      }
    }
    super.a();
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    return new NotificationsItemViewHolder(LayoutInflater.from(paramViewGroup.getContext()).inflate(2130968743, paramViewGroup, false));
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    ((NotificationsItemViewHolder)paramViewHolder).onCreateView((NotificationItemData)c.get(paramInt));
  }
  
  protected NotificationItemDeleteHelper cursor()
  {
    return (NotificationItemDeleteHelper)ItemDeleteUtils.a(ItemDeleteType.b);
  }
  
  protected HashMap get()
  {
    if (t != null) {
      return (HashMap)cursor().get().get(t.getString());
    }
    return (HashMap)cursor().get().get(g);
  }
}
