package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.content.res.Resources;
import android.net.Uri;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTComment;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnChangedCommentContext;
import com.deviantart.android.damobile.util.CommentType;
import com.deviantart.android.damobile.util.DAFormatUtils.DateFormats;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.markup.FallbackDAMLContent;
import com.deviantart.android.damobile.util.markup.FallbackDAMLContent.Builder;
import com.deviantart.android.damobile.util.markup.MarkupHelper;
import com.deviantart.android.damobile.util.threaditem.CommentItem;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.util.tracking.TrackerUtil.EventLabelBuilder;
import com.deviantart.android.damobile.view.CommentsLayout.CommentContextState;
import com.deviantart.android.damobile.view.LoadMoreButton;
import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.otto.Bus;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommentFragment
  extends DABaseFragment
{
  private Menu c;
  @Bind({2131689635})
  TextView commentAuthor;
  @Bind({2131689633})
  SimpleDraweeView commentAuthorAvatar;
  @Bind({2131689637})
  TextView commentContent;
  @Bind({2131689636})
  TextView commentDate;
  @Bind({2131689736})
  EditText commentInput;
  @Bind({2131689627})
  LoadMoreButton commentLoadMoreButton;
  @Bind({2131689624})
  ProgressBar commentLoading;
  @Bind({2131689735})
  ScrollView replyContainer;
  
  public CommentFragment() {}
  
  public static CommentFragment a(CommentType paramCommentType, String paramString, CommentItem paramCommentItem)
  {
    CommentFragment localCommentFragment = new CommentFragment();
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("reply_comment", paramCommentItem);
    localBundle.putSerializable("comment_type", paramCommentType);
    localBundle.putString("comment_itemid", paramString);
    localCommentFragment.setArguments(localBundle);
    return localCommentFragment;
  }
  
  private void save()
  {
    CommentType localCommentType = (CommentType)getArguments().getSerializable("comment_type");
    String str = getArguments().getString("comment_itemid");
    Object localObject = (CommentItem)getArguments().getSerializable("reply_comment");
    if (str == null)
    {
      Toast.makeText(getActivity(), getResources().getString(2131230904), 1).show();
      ScreenFlowManager.hideKeyboard(getActivity());
      return;
    }
    if (localObject != null) {}
    for (localObject = ((CommentItem)localObject).intValue().getCommentId();; localObject = "")
    {
      CommentFragment.1 local1 = new CommentFragment.1(this);
      TrackerUtil.EventLabelBuilder localEventLabelBuilder = new TrackerUtil.EventLabelBuilder();
      commentLoading.setVisibility(0);
      c.clear();
      switch (CommentFragment.2.c[localCommentType.ordinal()])
      {
      default: 
        Toast.makeText(getActivity(), getResources().getString(2131230905), 1).show();
        ScreenFlowManager.hideKeyboard(getActivity());
      }
      for (;;)
      {
        localObject = localEventLabelBuilder.getValue();
        if (((String)localObject).isEmpty()) {
          break;
        }
        TrackerUtil.get(getActivity(), EventKeys.Category.k, "comment_posted", (String)localObject);
        return;
        localEventLabelBuilder.get("commenttype", "deviation");
        DVNTCommonAsyncAPI.commentPostToDeviation(str, (String)localObject, commentInput.getText().toString()).call(getActivity(), local1);
        continue;
        localEventLabelBuilder.get("commenttype", "user");
        DVNTCommonAsyncAPI.commentPostToUserProfile(str, (String)localObject, commentInput.getText().toString()).call(getActivity(), local1);
        continue;
        localEventLabelBuilder.get("commenttype", "status");
        DVNTCommonAsyncAPI.commentPostToStatus(str, (String)localObject, commentInput.getText().toString()).call(getActivity(), local1);
      }
    }
  }
  
  public void onCommentChanged(CharSequence paramCharSequence)
  {
    if (c == null)
    {
      Log.d("Comment writer", "Menu not ready");
      return;
    }
    MenuItem localMenuItem = c.findItem(2131690147);
    if (localMenuItem == null)
    {
      Log.d("Comment writer", "Menu item not ready");
      return;
    }
    if (paramCharSequence.length() == 0)
    {
      localMenuItem.setIcon(2130837780);
      return;
    }
    localMenuItem.setIcon(2130837781);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setHasOptionsMenu(true);
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    c = paramMenu;
    paramMenuInflater.inflate(2131820544, paramMenu);
    saveTask();
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    BusStation.get().post(new BusStation.OnChangedCommentContext(CommentsLayout.CommentContextState.a));
    paramViewGroup = (LinearLayout)paramLayoutInflater.inflate(2130968666, paramViewGroup, false);
    ButterKnife.bind(this, paramViewGroup);
    paramLayoutInflater = (CommentItem)getArguments().getSerializable("reply_comment");
    if (paramLayoutInflater != null)
    {
      paramBundle = paramLayoutInflater.intValue();
      Object localObject1 = paramBundle.getUser();
      if (localObject1 != null) {}
      for (paramLayoutInflater = ((DVNTUser)localObject1).getUserName();; paramLayoutInflater = getString(2131231214))
      {
        commentLoadMoreButton.setVisibility(8);
        Object localObject2 = new FallbackDAMLContent.Builder().c(paramBundle.getBody()).a().c();
        if (localObject2 != null) {
          MarkupHelper.onPostExecute(getActivity(), (String)localObject2, commentContent, false);
        }
        commentAuthor.setText(paramLayoutInflater);
        if (localObject1 != null) {
          ImageUtils.isEmpty(commentAuthorAvatar, Uri.parse(((DVNTUser)localObject1).getUserIconURL()));
        }
        localObject1 = DAFormatUtils.DateFormats.c;
        try
        {
          long l = ((SimpleDateFormat)localObject1).parse(paramBundle.getSubmissionDate()).getTime();
          localObject1 = DateUtils.getRelativeTimeSpanString(l, new Date().getTime(), 0L);
          localObject2 = commentDate;
          ((TextView)localObject2).setText((CharSequence)localObject1);
        }
        catch (ParseException localParseException)
        {
          for (;;)
          {
            commentDate.setText(paramBundle.getSubmissionDate());
          }
        }
        ((BaseActivity)getActivity()).getSupportActionBar().setTitle(2131231235);
        replyContainer.setVisibility(0);
        commentInput.setHint(getString(2131231224) + " " + paramLayoutInflater);
        return paramViewGroup;
      }
    }
    ((BaseActivity)getActivity()).getSupportActionBar().setTitle(2131230786);
    replyContainer.setVisibility(8);
    commentInput.setHint(getString(2131231221));
    return paramViewGroup;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      return super.onOptionsItemSelected(paramMenuItem);
    }
    save();
    return true;
  }
  
  public void onOptionsMenuClosed(Menu paramMenu)
  {
    c = null;
  }
  
  public void saveTask()
  {
    if (commentInput == null) {
      return;
    }
    onCommentChanged(commentInput.getText());
  }
}
