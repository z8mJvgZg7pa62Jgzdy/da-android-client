package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTMoreLikeThisRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse>
{
  final String categoryPath;
  final String deviationId;
  final Integer limit;
  final Integer offset;
  
  public DVNTMoreLikeThisRequestV1(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse.class);
    categoryPath = paramString2;
    offset = paramInteger1;
    limit = paramInteger2;
    deviationId = paramString1;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTMoreLikeThisRequestV1 localDVNTMoreLikeThisRequestV1 = (DVNTMoreLikeThisRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(deviationId, deviationId).append(categoryPath, categoryPath).append(offset, offset).append(limit, limit).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    StringBuilder localStringBuilder = new StringBuilder().append("browse_mlt").append(deviationId);
    if (categoryPath != null) {}
    for (String str = categoryPath.replace("/", "");; str = "") {
      return str + offset + limit;
    }
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 1800000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(deviationId).append(categoryPath).append(offset).append(limit).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTPaginatedResultResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).moreLikeThis(paramString, deviationId, categoryPath, offset, limit);
  }
}
