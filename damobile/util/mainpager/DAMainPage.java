package com.deviantart.android.damobile.util.mainpager;

import android.view.View;
import android.view.ViewGroup;

public abstract class DAMainPage
{
  public DAMainPage() {}
  
  public abstract View a(ViewGroup paramViewGroup);
  
  public abstract void b();
  
  public CharSequence getFormatted()
  {
    return null;
  }
  
  public CharSequence getPageTitle()
  {
    return null;
  }
  
  public int intValue()
  {
    return 0;
  }
}
