package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.google.common.reflect.TypeToken;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTGalleryRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse>
{
  final String folderId;
  final Integer limit;
  final Integer offset;
  final String userName;
  
  public DVNTGalleryRequestV1(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse.class);
    if (paramString1 == null) {}
    for (folderId = "";; folderId = paramString1)
    {
      userName = paramString2;
      offset = paramInteger1;
      limit = paramInteger2;
      return;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTGalleryRequestV1 localDVNTGalleryRequestV1 = (DVNTGalleryRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(folderId, folderId).append(userName, userName).append(offset, offset).append(limit, limit).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "gallery" + folderId + userName + offset + limit;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public TypeToken getWrapperTypeToken()
  {
    return new DVNTGalleryRequestV1.1(this);
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(folderId).append(userName).append(offset).append(limit).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTPaginatedResultResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).gallery(paramString, folderId, userName, offset, limit);
  }
}
