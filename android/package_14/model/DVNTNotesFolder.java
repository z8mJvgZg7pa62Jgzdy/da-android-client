package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTNotesFolder
  implements Serializable
{
  @SerializedName("count")
  Integer count;
  @SerializedName("folder")
  String folderId;
  @SerializedName("parentid")
  String parentFolderId;
  @SerializedName("title")
  String title;
  
  public DVNTNotesFolder() {}
  
  public Integer getCount()
  {
    return count;
  }
  
  public String getFolderId()
  {
    return folderId;
  }
  
  public String getParentFolderId()
  {
    return parentFolderId;
  }
  
  public String getTitle()
  {
    return title;
  }
  
  public DVNTNotesFolder setCount(Integer paramInteger)
  {
    count = paramInteger;
    return this;
  }
  
  public DVNTNotesFolder setFolderId(String paramString)
  {
    folderId = paramString;
    return this;
  }
  
  public DVNTNotesFolder setParentFolderId(String paramString)
  {
    parentFolderId = paramString;
    return this;
  }
  
  public DVNTNotesFolder setTitle(String paramString)
  {
    title = paramString;
    return this;
  }
}
