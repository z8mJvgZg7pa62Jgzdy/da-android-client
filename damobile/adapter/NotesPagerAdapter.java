package com.deviantart.android.damobile.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.SpannableStringInternal;
import android.text.style.ForegroundColorSpan;
import android.text.style.SuperscriptSpan;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.DAMobileApplication;
import com.deviantart.android.damobile.util.notes.NotesDefaultPage;
import com.deviantart.android.damobile.util.notes.NotesPage;
import com.deviantart.android.damobile.util.opt.NoteItemDeleteHelper;
import com.deviantart.android.damobile.view.notes.NotesListFrame;
import com.deviantart.android.damobile.view.opt.MCListFrameBase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class NotesPagerAdapter
  extends StatefulPagerAdapter
{
  private ArrayList<NotesPage> n;
  private int o;
  
  public NotesPagerAdapter(int paramInt)
  {
    o = paramInt;
    n = new ArrayList(Arrays.asList(NotesDefaultPage.values()));
    Iterator localIterator = n.iterator();
    while (localIterator.hasNext())
    {
      NotesPage localNotesPage = (NotesPage)localIterator.next();
      NoteItemDeleteHelper.a.add(localNotesPage);
    }
  }
  
  public Object a(ViewGroup paramViewGroup, int paramInt)
  {
    NotesListFrame localNotesListFrame = ((NotesPage)n.get(paramInt)).a(paramViewGroup.getContext(), o);
    b(localNotesListFrame, localNotesListFrame.getPageTag());
    paramViewGroup.addView(localNotesListFrame);
    return localNotesListFrame;
  }
  
  public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    paramViewGroup.removeView((View)paramObject);
  }
  
  public int getCount()
  {
    return n.size();
  }
  
  public CharSequence getPageTitle(int paramInt)
  {
    Context localContext = DAMobileApplication.getContext();
    Object localObject1 = (NotesPage)n.get(paramInt);
    Object localObject2 = ((NotesPage)localObject1).get(localContext).toString().toUpperCase();
    if (((NotesPage)localObject1).getColumnIndex() > 0)
    {
      localObject2 = new SpannableStringBuilder((CharSequence)localObject2);
      localObject1 = new SpannableString(Integer.toString(((NotesPage)localObject1).getColumnIndex()));
      ((SpannableString)localObject1).setSpan(new SuperscriptSpan(), 0, ((SpannableStringInternal)localObject1).length(), 33);
      ((SpannableString)localObject1).setSpan(new ForegroundColorSpan(localContext.getResources().getColor(2131558526)), 0, ((SpannableStringInternal)localObject1).length(), 33);
      ((SpannableStringBuilder)localObject2).append((CharSequence)localObject1);
      return localObject2;
    }
    return localObject2;
  }
  
  public boolean isViewFromObject(View paramView, Object paramObject)
  {
    return paramView == paramObject;
  }
}
