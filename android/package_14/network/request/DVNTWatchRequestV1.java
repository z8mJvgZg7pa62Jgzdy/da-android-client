package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTWatchRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  boolean addFriend;
  private String userName;
  boolean watchActivity;
  boolean watchCollections;
  boolean watchCritiques;
  boolean watchDeviations;
  boolean watchForumThreads;
  boolean watchJournals;
  boolean watchScraps;
  
  public DVNTWatchRequestV1(String paramString, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, boolean paramBoolean7, boolean paramBoolean8)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    userName = paramString;
    addFriend = paramBoolean1;
    watchDeviations = paramBoolean2;
    watchJournals = paramBoolean3;
    watchForumThreads = paramBoolean4;
    watchCritiques = paramBoolean5;
    watchScraps = paramBoolean6;
    watchActivity = paramBoolean7;
    watchCollections = paramBoolean8;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTWatchRequestV1 localDVNTWatchRequestV1 = (DVNTWatchRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(userName, userName).append(addFriend, addFriend).append(watchDeviations, watchDeviations).append(watchJournals, watchJournals).append(watchForumThreads, watchForumThreads).append(watchCritiques, watchCritiques).append(watchScraps, watchScraps).append(watchActivity, watchActivity).append(watchCollections, watchCollections).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(userName).append(addFriend).append(watchDeviations).append(watchJournals).append(watchForumThreads).append(watchCritiques).append(watchScraps).append(watchActivity).append(watchCollections).toHashCode();
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).watch(paramString, userName, addFriend, watchDeviations, watchJournals, watchForumThreads, watchCritiques, watchScraps, watchActivity, watchCollections);
  }
}
