package com.deviantart.android.damobile.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.damobile.util.ConnectivityUtils;
import com.viewpagerindicator.CirclePageIndicator;

public class IntroActivity
  extends BaseActivity
{
  private static int[] color = { 2130837724 };
  private static int[] messages = { 2130968720, 2130968721, 2130968722, 2130968723, 2130968724 };
  @Bind({2131689597})
  ImageView background;
  @Bind({2131689599})
  TextView backgroundAuthor;
  @Bind({2131689598})
  TextView backgroundTitle;
  private IntroActivity.IntroBackground i;
  @Bind({2131689600})
  ViewPager slidePager;
  @Bind({2131689601})
  CirclePageIndicator slidePagerIndicator;
  private int titles;
  
  public IntroActivity() {}
  
  public void a()
  {
    com.deviantart.android.damobile.util.UserUtils.d = true;
    super.a();
    finish();
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if ((paramInt1 == 104) && ((paramInt2 == -1) || (paramInt2 == 1))) {
      finish();
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968606);
    ButterKnife.bind(this);
    titles = color[new java.util.Random().nextInt(color.length)];
    i = IntroActivity.IntroBackground.fromUri();
    background.setImageResource(i.e);
    backgroundTitle.setText(i.b);
    backgroundAuthor.setText(i.d);
    paramBundle = new IntroActivity.IntroSlidePagerAdapter(this, getLayoutInflater());
    slidePager.setAdapter(paramBundle);
    slidePagerIndicator.setViewPager(slidePager, 0);
    slidePagerIndicator.setOnPageChangeListener(new IntroActivity.1(this));
  }
  
  protected void onJoinClick()
  {
    startActivityForResult(new Intent(this, SignUpActivity.class), 104);
  }
  
  public void onLoginClick()
  {
    DVNTAbstractAsyncAPI.graduate(this);
  }
  
  protected void onResume()
  {
    ConnectivityUtils.showErrorDialog(this);
    super.onResume();
  }
  
  public void onSkipClick()
  {
    finish();
  }
}
