package com.deviantart.android.damobile.view.tooltip;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;

public class DAToolTipRelativeLayout
  extends ToolTipRelativeLayout
{
  public DAToolTipRelativeLayout(Context paramContext)
  {
    super(paramContext);
  }
  
  public DAToolTipRelativeLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public DAToolTipRelativeLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }
  
  public DAToolTipView a(DAToolTip paramDAToolTip, View paramView)
  {
    DAToolTipView localDAToolTipView = new DAToolTipView(getContext());
    localDAToolTipView.init(paramDAToolTip, paramView);
    addView(localDAToolTipView);
    return localDAToolTipView;
  }
}
