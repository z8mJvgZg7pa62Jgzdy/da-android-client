package com.deviantart.android.damobile.util.notifications;

import android.content.Context;
import android.util.Log;
import android.view.View;
import com.deviantart.android.android.package_14.model.DVNTFeedbackType;
import com.deviantart.android.damobile.fragment.NotificationsFragment;
import com.deviantart.android.damobile.stream.FilteredStream;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.filter.NotificationsModelFilter;
import com.deviantart.android.damobile.stream.filter.StreamFilter;
import com.deviantart.android.damobile.stream.loader.APINotificationsAllLoader;
import com.deviantart.android.damobile.stream.loader.APINotificationsLoader;
import com.deviantart.android.damobile.stream.loader.APINotificationsMentionLoader;
import com.deviantart.android.damobile.stream.loader.APINotificationsReplyLoader;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.mc.ItemPendable;
import com.deviantart.android.damobile.util.opt.ItemDeleteHelperBase;
import com.deviantart.android.damobile.util.opt.ItemDeleteType;
import com.deviantart.android.damobile.util.opt.ItemDeleteUtils;
import com.deviantart.android.damobile.util.opt.NotificationItemDeleteHelper;
import com.deviantart.android.damobile.view.notifications.NotificationsListAdapter;
import com.deviantart.android.damobile.view.notifications.NotificationsListFrame;
import com.deviantart.android.damobile.view.opt.MCListFrameBase;
import com.deviantart.android.damobile.view.opt.MCListRecyclerView;
import java.util.HashMap;
import java.util.HashSet;

public enum NotificationsPage
  implements ItemPendable
{
  public static final NotificationsPage[] c = values();
  private final int k;
  private final int m;
  private final String s;
  
  static
  {
    d = new NotificationsPage("COMMENTS", 1, 2131231155, 2131231156, "notification_comments");
    g = new NotificationsPage("REPLIES", 2, 2131231178, 2131231179, "notification_replies");
    b = new NotificationsPage("ACTIVITY", 3, 2131231143, 2131231144, "notification_activity");
    e = new NotificationsPage("MENTIONS", 4, 2131231171, 2131231172, "notification_mentions");
  }
  
  private NotificationsPage(int paramInt1, int paramInt2, String paramString)
  {
    m = paramInt1;
    k = paramInt2;
    s = paramString;
    ((NotificationItemDeleteHelper)ItemDeleteUtils.a(ItemDeleteType.b)).get().put(paramString, new HashMap());
  }
  
  public static StreamLoader b(NotificationsPage paramNotificationsPage)
  {
    switch (NotificationsPage.1.d[paramNotificationsPage.ordinal()])
    {
    default: 
      Log.e("Notifications", "Unhandled NotificationsPage type. Could not create stream loader.");
      return null;
    case 1: 
      return new APINotificationsAllLoader();
    case 2: 
      return new APINotificationsLoader(DVNTFeedbackType.COMMENTS);
    case 3: 
      return new APINotificationsReplyLoader();
    case 4: 
      return new APINotificationsLoader(DVNTFeedbackType.ACTIVITY);
    }
    return new APINotificationsMentionLoader();
  }
  
  public NotificationsListFrame a(Context paramContext, int paramInt1, int paramInt2)
  {
    Object localObject = b(this);
    ((StreamLoader)localObject).add(paramInt1);
    NotificationsFragment.a.add(((StreamLoader)localObject).b());
    localObject = new FilteredStream(StreamCacher.a((StreamLoader)localObject), new StreamFilter[] { new NotificationsModelFilter() });
    NotificationsListFrame localNotificationsListFrame = new NotificationsListFrame(paramContext);
    localNotificationsListFrame.setTag(getString());
    localNotificationsListFrame.setEmptyMessage(c(paramContext));
    localNotificationsListFrame.getRecyclerView().setAdapterSafe(new NotificationsListAdapter((Stream)localObject, this));
    localNotificationsListFrame.setScrollUnderSize(paramInt2);
    if (((Stream)localObject).size() <= 0) {
      localNotificationsListFrame.visitFrame();
    }
    return localNotificationsListFrame;
  }
  
  public String c(Context paramContext)
  {
    return paramContext.getString(k);
  }
  
  public String get(Context paramContext)
  {
    return paramContext.getString(m);
  }
  
  public String getString()
  {
    return s;
  }
}
