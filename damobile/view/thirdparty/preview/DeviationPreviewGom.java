package com.deviantart.android.damobile.view.thirdparty.preview;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.util.DeviationTorpedoPreviewItem;
import com.deviantart.android.damobile.util.torpedo.TorpedoPreview;
import com.deviantart.android.damobile.view.gom.Gom;
import com.deviantart.android.damobile.view.thirdparty.GomType;

public class DeviationPreviewGom
  implements Gom<DeviationTorpedoPreviewItem, RecyclerView.ViewHolder>
{
  public DeviationPreviewGom() {}
  
  public RecyclerView.ViewHolder b(Context paramContext, ViewGroup paramViewGroup)
  {
    return new DeviationPreviewGom.1(this, (TorpedoPreview)LayoutInflater.from(paramViewGroup.getContext()).inflate(2130968648, paramViewGroup, false));
  }
  
  public GomType b()
  {
    return GomType.b;
  }
  
  public void f(Context paramContext, DeviationTorpedoPreviewItem paramDeviationTorpedoPreviewItem, RecyclerView.ViewHolder paramViewHolder)
  {
    paramViewHolder = (TorpedoPreview)itemView;
    paramViewHolder.d();
    paramViewHolder.setTitle(paramDeviationTorpedoPreviewItem.b());
    paramViewHolder.setGridTitle(paramDeviationTorpedoPreviewItem.format());
    paramViewHolder.setSubTitle(paramDeviationTorpedoPreviewItem.f());
    paramViewHolder.a(paramDeviationTorpedoPreviewItem.getPropertyName());
    paramViewHolder.setEventLabel(paramDeviationTorpedoPreviewItem.getRawValue());
    paramViewHolder.setTorpedoDetail(paramDeviationTorpedoPreviewItem.a(paramContext));
    paramViewHolder.setDeviationStream(paramDeviationTorpedoPreviewItem.c());
    paramViewHolder.setHeaderTitle(paramDeviationTorpedoPreviewItem.getMain());
    paramViewHolder.setHeaderSubTitle(paramDeviationTorpedoPreviewItem.i());
    paramViewHolder.setHeaderImageURL(paramDeviationTorpedoPreviewItem.a());
    paramViewHolder.setAddFaveMark(paramDeviationTorpedoPreviewItem.isSystem());
  }
}
