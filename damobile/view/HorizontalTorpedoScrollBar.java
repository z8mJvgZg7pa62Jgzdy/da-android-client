package com.deviantart.android.damobile.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class HorizontalTorpedoScrollBar
  extends View
{
  private float b;
  private float d;
  private int e = 1;
  private Paint f;
  private int i;
  private int m;
  private Paint p;
  private float w;
  
  public HorizontalTorpedoScrollBar(Context paramContext)
  {
    super(paramContext);
    a();
  }
  
  public HorizontalTorpedoScrollBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a();
  }
  
  public HorizontalTorpedoScrollBar(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a();
  }
  
  private void a()
  {
    m = 0;
    w = 0.0F;
    b = 0.0F;
    i = 0;
    f = new Paint();
    f.setAntiAlias(true);
    f.setStyle(Paint.Style.STROKE);
    f.setStrokeWidth(5.0F);
    f.setColor(getResources().getColor(2131558497));
    p = new Paint();
    p.setAntiAlias(true);
    p.setStyle(Paint.Style.STROKE);
    p.setStrokeWidth(7.0F);
    p.setColor(getResources().getColor(2131558496));
  }
  
  private void load()
  {
    ObjectAnimator localObjectAnimator = ObjectAnimator.ofFloat(this, "barPositionStep", new float[] { b, w });
    localObjectAnimator.setDuration(100L);
    localObjectAnimator.start();
  }
  
  private void setBarPositionStep(float paramFloat)
  {
    d = paramFloat;
    invalidate();
  }
  
  public void b(int paramInt1, int paramInt2, int paramInt3)
  {
    Log.d("scroll&shit", "newPosition : " + paramInt2 + " / maxPosition : " + e);
    i += paramInt3;
    float f1 = e * ((getWidth() + i) / Math.max(1, paramInt2));
    Log.d("scroll&shit", "torpedoLastScrollX : " + paramInt3);
    Log.d("scroll&shit", "torpedoTotalScrollx : " + i);
    m = paramInt2;
    if ((i <= 0) || (paramInt1 == 0))
    {
      w = 0.0F;
      m = 0;
    }
    for (;;)
    {
      load();
      b = w;
      return;
      if (paramInt2 >= e)
      {
        w = getWidth();
      }
      else
      {
        float f2 = w;
        w = (paramInt3 / (f1 / getWidth()) + f2);
      }
    }
  }
  
  public int getMaxPosition()
  {
    return e;
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    float f2 = getWidth() / Math.min(e, 15);
    paramCanvas.drawLine(0.0F, getHeight() / 2, getWidth(), getHeight() / 2, f);
    float f1 = Math.max(0.0F, Math.min(getWidth() - f2, d));
    f2 = Math.min(getWidth(), f1 + f2);
    paramCanvas.drawLine(f1, getHeight() / 2, f2, getHeight() / 2, p);
  }
  
  public void setMaxPosition(int paramInt)
  {
    e = Math.max(paramInt, 1);
    invalidate();
  }
}
