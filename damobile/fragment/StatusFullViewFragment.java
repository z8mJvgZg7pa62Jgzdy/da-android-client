package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.os.BaseBundle;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTComment;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.package_14.model.DVNTUserStatus;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnCommentPostedEvent;
import com.deviantart.android.damobile.util.BusStation.OnLoadedFocusedComment;
import com.deviantart.android.damobile.util.CommentType;
import com.deviantart.android.damobile.util.CommentUtils.OnClickOpenCommentListener;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.FloatingButtonDescription;
import com.deviantart.android.damobile.util.ProcessMenuListener;
import com.deviantart.android.damobile.util.ProcessMenuType;
import com.deviantart.android.damobile.util.StatusViewHelper;
import com.deviantart.android.damobile.view.CommentsLayout;
import com.deviantart.android.damobile.view.CommentsLayout.InstanceBuilder;
import com.deviantart.android.damobile.view.DAFaveView;
import com.deviantart.android.damobile.view.ewok.EwokFactory;
import com.deviantart.android.damobile.view.ewok.StatusEwok;
import com.deviantart.android.damobile.view.ewok.decorator.WatchFeedItemEwok;
import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.otto.Bus;

public class StatusFullViewFragment
  extends HomeFullViewFragment
  implements ProcessMenuListener
{
  private String a;
  @Bind({2131689769})
  SimpleDraweeView avatar;
  CommentsLayout b;
  private boolean c;
  View.OnClickListener k = new StatusFullViewFragment.1(this);
  private DVNTUserStatus l;
  @Bind({2131689748})
  View loading;
  @Bind({2131689779})
  FrameLayout mainLayout;
  @Bind({2131690030})
  TextView postedTime;
  @Bind({2131690027})
  ImageView shareButton;
  @Bind({2131689812})
  View statusView;
  @Bind({2131689840})
  TextView username;
  
  public StatusFullViewFragment() {}
  
  private void load(String paramString)
  {
    statusView.setVisibility(8);
    loading.setVisibility(0);
    DVNTCommonAsyncAPI.getStatus(paramString).call(getActivity(), new StatusFullViewFragment.2(this));
  }
  
  private void onCreateView()
  {
    if (l == null) {
      return;
    }
    Object localObject1 = EwokFactory.a(l);
    StatusViewHelper.create(getActivity(), l.getAuthor(), avatar, username, c);
    ((WatchFeedItemEwok)localObject1).b(this);
    localObject1 = ((StatusEwok)localObject1).init(getActivity());
    if (l.isShare().booleanValue())
    {
      localObject2 = (TextView)ButterKnife.findById((View)localObject1, 2131689730);
      if ((localObject2 != null) && (((TextView)localObject2).getText() != null)) {
        ((View)localObject2).setVisibility(0);
      }
    }
    Object localObject2 = StatusViewHelper.add(getActivity(), l);
    if (localObject2 == null) {
      shareButton.setVisibility(8);
    }
    for (;;)
    {
      localObject2 = l.getTime();
      String str = DAFormatUtils.format(getActivity(), (String)localObject2);
      if ((str != null) && (!str.equals(localObject2))) {
        postedTime.setText(str);
      }
      localObject1 = new CommentsLayout.InstanceBuilder().a(CommentType.b).a(l.getStatusId()).b(l.getAuthor().getUserName()).a((View)localObject1);
      if (a != null) {
        ((CommentsLayout.InstanceBuilder)localObject1).c(a).a(k);
      }
      b = ((CommentsLayout.InstanceBuilder)localObject1).a(getActivity());
      mainLayout.addView(b);
      return;
      shareButton.setOnClickListener((View.OnClickListener)localObject2);
    }
  }
  
  public void a(ProcessMenuType paramProcessMenuType, String paramString)
  {
    if (mainLayout == null) {
      return;
    }
    paramString = mainLayout.findViewWithTag(paramString);
    if ((paramString != null) && ((paramString instanceof DAFaveView)))
    {
      if (ProcessMenuType.c.equals(paramProcessMenuType))
      {
        ((DAFaveView)paramString).show(true, true);
        return;
      }
      if (ProcessMenuType.b.equals(paramProcessMenuType)) {
        ((DAFaveView)paramString).show(false, true);
      }
    }
  }
  
  public void b(BusStation.OnCommentPostedEvent paramOnCommentPostedEvent)
  {
    super.b(paramOnCommentPostedEvent);
    paramOnCommentPostedEvent = (DVNTComment)paramOnCommentPostedEvent.getArguments().getSerializable("comment_posted");
    if (b == null) {
      return;
    }
    b.b(paramOnCommentPostedEvent);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    a = getArguments().getString("notification_comment_id");
    c = getArguments().getBoolean("username_click_enabled");
    paramLayoutInflater = paramLayoutInflater.inflate(2130968682, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    paramViewGroup = getArguments().getString("statusid", null);
    if (paramViewGroup != null)
    {
      load(paramViewGroup);
      return paramLayoutInflater;
    }
    l = ((DVNTUserStatus)getArguments().getSerializable("status_info"));
    onCreateView();
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public void onLoadedFocusedComment(BusStation.OnLoadedFocusedComment paramOnLoadedFocusedComment)
  {
    t = new FloatingButtonDescription(getString(2131231242), new CommentUtils.OnClickOpenCommentListener(paramOnLoadedFocusedComment.getOrigin(), l.getStatusId(), CommentType.b));
    init();
  }
  
  public void onStart()
  {
    super.onStart();
    BusStation.get().register(this);
  }
  
  public void onStop()
  {
    BusStation.get().unregister(this);
    super.onStop();
  }
}
