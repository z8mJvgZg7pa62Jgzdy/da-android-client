package com.deviantart.android.damobile.util.notes;

import android.content.Context;
import android.view.View;
import com.deviantart.android.android.package_14.model.DVNTNotesFolder;
import com.deviantart.android.damobile.stream.loader.APINotesFolderLoader;
import com.deviantart.android.damobile.util.opt.ItemDeleteHelperBase;
import com.deviantart.android.damobile.util.opt.ItemDeleteType;
import com.deviantart.android.damobile.util.opt.ItemDeleteUtils;
import com.deviantart.android.damobile.util.opt.NoteItemDeleteHelper;
import com.deviantart.android.damobile.view.notes.NotesListFrame;
import com.deviantart.android.damobile.view.opt.MCListFrameBase;
import java.util.HashMap;

public enum NotesDefaultPage
  implements NotesPage
{
  private final String cache;
  private final boolean data;
  private final int id;
  private final int resource;
  
  static
  {
    l = new NotesDefaultPage("UNREAD", 1, 2131231132, "2E23AB5F-02FC-0EAD-F27A-BD5A251529FE", 2131231133, true);
    b = new NotesDefaultPage("STARRED", 2, 2131231127, "BC7EADE4-C74C-591A-A8F8-09FD5A57100E", 2131231128, false);
    d = new NotesDefaultPage("SENT", 3, 2131231124, "CF98ADAD-C51F-66A1-B1B4-D325CC9EAEB2", 2131231125, false);
  }
  
  private NotesDefaultPage(int paramInt1, String paramString, int paramInt2, boolean paramBoolean)
  {
    resource = paramInt1;
    cache = paramString;
    id = paramInt2;
    data = paramBoolean;
    ((NoteItemDeleteHelper)ItemDeleteUtils.a(ItemDeleteType.c)).get().put(getString(), new HashMap());
  }
  
  private DVNTNotesFolder createContact()
  {
    return (DVNTNotesFolder)NotesFolders.get().get(get());
  }
  
  private boolean have()
  {
    return NotesFolders.get().containsKey(get());
  }
  
  public NotesListFrame a(Context paramContext, int paramInt)
  {
    NotesListFrame localNotesListFrame = new NotesListFrame(paramContext);
    localNotesListFrame.setTag(getString());
    localNotesListFrame.setEmptyMessage(b(paramContext));
    localNotesListFrame.setScrollUnderSize(paramInt);
    localNotesListFrame.visitFrame(this);
    return localNotesListFrame;
  }
  
  public String a()
  {
    return "notesfolder|" + get();
  }
  
  public APINotesFolderLoader b()
  {
    return new APINotesFolderLoader(get());
  }
  
  public String b(Context paramContext)
  {
    return paramContext.getString(id);
  }
  
  public String get()
  {
    return cache;
  }
  
  public String get(Context paramContext)
  {
    return paramContext.getString(resource);
  }
  
  public int getColumnIndex()
  {
    if ((!data) || (!have())) {
      return -1;
    }
    return createContact().getCount().intValue();
  }
  
  public String getString()
  {
    return name().toLowerCase();
  }
}
