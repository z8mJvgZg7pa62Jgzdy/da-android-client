package com.deviantart.android.damobile.util;

import com.deviantart.android.android.package_14.model.DVNTGallection;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import java.util.ArrayList;

public abstract class GallectionItem
  extends DefaultDeviationTorpedoPreviewItem
{
  private String name;
  protected String orderType;
  private boolean system;
  protected DVNTGallection this$0;
  
  public GallectionItem(DVNTGallection paramDVNTGallection, String paramString1, String paramString2, boolean paramBoolean)
  {
    this$0 = paramDVNTGallection;
    orderType = paramString1;
    name = paramString2;
    system = paramBoolean;
  }
  
  public String b()
  {
    return this$0.getName();
  }
  
  public Stream c()
  {
    Stream localStream = StreamCacher.a(navigateNext(), StreamCacheStrategy.d);
    if ((this$0.getDeviations() != null) && (!this$0.getDeviations().isEmpty()) && (localStream.size() == 0)) {
      localStream.a(this$0.getDeviations());
    }
    return localStream;
  }
  
  public String f()
  {
    return DAFormatUtils.format(this$0.getSize());
  }
  
  public String getPropertyName()
  {
    return name;
  }
  
  public boolean isSystem()
  {
    return !system;
  }
  
  public abstract StreamLoader navigateNext();
}
