package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.SubmitActivity;
import com.deviantart.android.damobile.util.DelayedUserInputTask;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public class SubmitSecondaryFragment<T extends SubmitSecondaryFragment.SubmitOptionalElement>
  extends DABaseFragment
{
  private ArrayList<T> a = new ArrayList();
  private HashMap<String, RecyclerView> c = new HashMap();
  private SubmitSecondaryFragment<T>.ResultItemAdapter h;
  int index;
  private SubmitSecondaryFragment.SubmitOptionsListFragmentListener j;
  @Bind({2131689816})
  ProgressBar loader;
  private HashMap<String, List<T>> m = new HashMap();
  private ArrayList<T> q = new ArrayList();
  @Bind({2131689817})
  LinearLayout recent;
  @Bind({2131689818})
  RecyclerView resultsRecyclerView;
  @Bind({2131689813})
  HorizontalScrollView scroll;
  protected DelayedUserInputTask searchTask;
  @Bind({2131689814})
  LinearLayout submitSearchBarItems;
  
  public SubmitSecondaryFragment() {}
  
  public static SubmitSecondaryFragment a(String paramString, int paramInt, HashMap paramHashMap, ArrayList paramArrayList)
  {
    SubmitSecondaryFragment localSubmitSecondaryFragment = new SubmitSecondaryFragment();
    Bundle localBundle = new Bundle();
    localBundle.putSerializable("initial_lists", paramHashMap);
    paramHashMap = paramArrayList;
    if (paramArrayList == null) {
      paramHashMap = new ArrayList();
    }
    localBundle.putSerializable("already_checked", paramHashMap);
    localBundle.putInt("search_triggering_threshold", paramInt);
    localBundle.putString("title", paramString);
    localSubmitSecondaryFragment.setArguments(localBundle);
    return localSubmitSecondaryFragment;
  }
  
  private void a(SubmitSecondaryFragment.SubmitOptionalElement paramSubmitOptionalElement, boolean paramBoolean)
  {
    Iterator localIterator = m.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (Map.Entry)localIterator.next();
      RecyclerView localRecyclerView = (RecyclerView)c.get(((Map.Entry)localObject).getKey());
      localObject = (List)((Map.Entry)localObject).getValue();
      if (((List)localObject).contains(paramSubmitOptionalElement)) {
        a(paramBoolean, ((List)localObject).indexOf(paramSubmitOptionalElement), localRecyclerView);
      }
    }
    if ((q != null) && (!q.isEmpty()) && (q.contains(paramSubmitOptionalElement))) {
      a(paramBoolean, q.indexOf(paramSubmitOptionalElement), resultsRecyclerView);
    }
  }
  
  private void a(boolean paramBoolean, int paramInt, RecyclerView paramRecyclerView)
  {
    LinearLayoutManager localLinearLayoutManager = (LinearLayoutManager)paramRecyclerView.getLayoutManager();
    if ((paramInt >= localLinearLayoutManager.findFirstVisibleItemPosition()) && (paramInt <= localLinearLayoutManager.findLastVisibleItemPosition()))
    {
      paramRecyclerView = (CheckBox)paramRecyclerView.getChildAt(paramInt - localLinearLayoutManager.findFirstVisibleItemPosition());
      if (paramRecyclerView == null) {
        return;
      }
      paramRecyclerView.setChecked(paramBoolean);
    }
  }
  
  public void a()
  {
    if (submitSearchBarItems != null)
    {
      if (scroll == null) {
        return;
      }
      submitSearchBarItems.removeAllViews();
      if (j != null) {
        j.a(a);
      }
      Iterator localIterator = a.iterator();
      while (localIterator.hasNext())
      {
        SubmitSecondaryFragment.SubmitOptionalElement localSubmitOptionalElement = (SubmitSecondaryFragment.SubmitOptionalElement)localIterator.next();
        View localView = localSubmitOptionalElement.getView(getActivity());
        localView.setOnClickListener(SubmitSecondaryFragment..Lambda.1.d(this, localSubmitOptionalElement));
        submitSearchBarItems.addView(localView);
      }
      scroll.post(SubmitSecondaryFragment..Lambda.2.c(this));
    }
  }
  
  public void b(SubmitSecondaryFragment.SubmitOptionsListFragmentListener paramSubmitOptionsListFragmentListener)
  {
    j = paramSubmitOptionsListFragmentListener;
  }
  
  public void d(ArrayList paramArrayList)
  {
    if ((q != null) && (h != null) && (recent != null) && (resultsRecyclerView != null))
    {
      if (loader == null) {
        return;
      }
      loader.setVisibility(8);
      if (paramArrayList != null)
      {
        q = paramArrayList;
        h.a(paramArrayList);
        recent.setVisibility(8);
        resultsRecyclerView.setVisibility(0);
      }
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = (LinearLayout)LayoutInflater.from(getActivity()).inflate(2130968683, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    update();
    a = ((ArrayList)getArguments().getSerializable("already_checked"));
    index = getArguments().getInt("search_triggering_threshold");
    h = new SubmitSecondaryFragment.ResultItemAdapter(this, null);
    resultsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    resultsRecyclerView.setAdapter(h);
    ((SubmitActivity)getActivity()).getSupportActionBar().setTitle(getArguments().getString("title"));
    a();
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public void onSearchTextEntered(CharSequence paramCharSequence)
  {
    if (!DVNTContextUtils.isContextDead(getActivity()))
    {
      if (!isResumed()) {
        return;
      }
      if (searchTask != null) {
        searchTask.cancel(true);
      }
      if (paramCharSequence.length() < index)
      {
        resultsRecyclerView.setVisibility(8);
        recent.setVisibility(0);
        loader.setVisibility(8);
        return;
      }
      loader.setVisibility(0);
      if (j == null)
      {
        Log.e("Runtime", "no search box listener has been defined for this fragment");
        return;
      }
      searchTask = new SubmitSecondaryFragment.1(this);
      searchTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[] { paramCharSequence.toString() });
    }
  }
  
  public void update()
  {
    m = ((HashMap)getArguments().getSerializable("initial_lists"));
    Iterator localIterator = m.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if (!((List)m.get(str)).isEmpty())
      {
        TextView localTextView = (TextView)LayoutInflater.from(getActivity()).inflate(2130968777, null, false);
        localTextView.setText(str);
        RecyclerView localRecyclerView = new RecyclerView(getActivity());
        localRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        SubmitSecondaryFragment.ResultItemAdapter localResultItemAdapter = new SubmitSecondaryFragment.ResultItemAdapter(this, null);
        localRecyclerView.setAdapter(localResultItemAdapter);
        localResultItemAdapter.a((List)m.get(str));
        c.put(str, localRecyclerView);
        recent.addView(localTextView);
        recent.addView(localRecyclerView);
      }
    }
  }
}
