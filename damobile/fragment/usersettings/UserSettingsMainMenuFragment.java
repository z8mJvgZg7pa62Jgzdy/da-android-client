package com.deviantart.android.damobile.fragment.usersettings;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.config.DVNTAPIConfig;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig;
import com.deviantart.android.android.package_14.model.DVNTWhoAmIResponse;
import com.deviantart.android.damobile.activity.UserSettingsActivity;
import com.deviantart.android.damobile.activity.UserSettingsActivity.OnBackResult;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnUserSettingChangeEvent;
import com.deviantart.android.damobile.util.ImageQualityType;
import com.deviantart.android.damobile.util.SharedPreferenceUtil;
import com.deviantart.android.damobile.util.UserSettingUpdateType;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.view.dialogs.AlertDialogsBuilders;
import com.squareup.otto.Bus;

public class UserSettingsMainMenuFragment
  extends UserSettingsBaseFragment
{
  @Bind({2131689878})
  TextView photoQualityText;
  @Bind({2131689881})
  RelativeLayout rcOnlyWrapper;
  @Bind({2131689880})
  SwitchCompat viewMatureContentSwitch;
  @Bind({2131689883})
  SwitchCompat viewRcOnlySwitch;
  
  public UserSettingsMainMenuFragment() {}
  
  private void showMessageDetails()
  {
    ImageQualityType localImageQualityType = ImageQualityType.get(getActivity());
    photoQualityText.setText(localImageQualityType.getTitle(getActivity()));
  }
  
  public UserSettingsActivity.OnBackResult a()
  {
    if (e)
    {
      DVNTAbstractAsyncAPI.clearCache();
      StreamCacher.clear();
      BusStation.get().post(new BusStation.OnUserSettingChangeEvent(UserSettingUpdateType.e));
    }
    return UserSettingsActivity.OnBackResult.d;
  }
  
  protected void b(DVNTWhoAmIResponse paramDVNTWhoAmIResponse)
  {
    e = false;
  }
  
  public void changeProfilePicturesClick()
  {
    onPostExecute(new UserSettingsChangeProfilePicsFragment(), "change_profile_pictures");
  }
  
  public void editAccountSettingsClick()
  {
    onPostExecute(new UserSettingsUpdateAccountFragment(), "update_account");
  }
  
  public void logoutClick()
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
    localBuilder.setTitle(getString(2131231286)).setMessage(getString(2131231287)).setPositiveButton(getString(2131230873), new UserSettingsMainMenuFragment.4(this)).setNegativeButton(getString(2131230872), new UserSettingsMainMenuFragment.3(this));
    localBuilder.create().show();
  }
  
  public void manageWatchFeedClick()
  {
    TrackerUtil.a(getActivity(), "open_manage_watchfeed");
    onPostExecute(new UserSettingsWatchFeedManageFragment(), "manage_watch_feed");
  }
  
  public void onChangeMatureContent(boolean paramBoolean)
  {
    if (UserUtils.i)
    {
      viewMatureContentSwitch.setChecked(false);
      AlertDialogsBuilders.showConfirmationDialog(getActivity()).create().show();
      return;
    }
    if (paramBoolean) {
      TrackerUtil.a(getActivity(), EventKeys.Category.e, "enabled_mature_content");
    }
    for (;;)
    {
      UserUtils.saveBoolean(getActivity(), paramBoolean);
      DVNTAbstractAsyncAPI.getConfig().setShowMatureContent(Boolean.valueOf(paramBoolean));
      e = true;
      return;
      TrackerUtil.a(getActivity(), EventKeys.Category.e, "disabled_mature_content");
    }
  }
  
  public void onChangeRConly(boolean paramBoolean)
  {
    SharedPreferenceUtil.saveBoolean(getActivity(), "setting_rc_only_content", paramBoolean);
    DVNTAbstractAsyncAPI.getConfig().setShowRCContent(Boolean.valueOf(paramBoolean));
    DVNTAbstractAsyncAPI.clearCache();
    com.deviantart.android.damobile.activity.BaseActivity.index = true;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setRetainInstance(true);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = (ScrollView)paramLayoutInflater.inflate(2130968693, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    filterSelected();
    showMessageDetails();
    viewMatureContentSwitch.setChecked(UserUtils.get(getActivity()));
    int i = -1;
    switch ("store".hashCode())
    {
    default: 
      break;
    }
    for (;;)
    {
      switch (i)
      {
      default: 
        return paramLayoutInflater;
        if ("store".equals("rc"))
        {
          i = 0;
          continue;
          if ("store".equals("debug")) {
            i = 1;
          }
        }
        break;
      }
    }
    rcOnlyWrapper.setVisibility(0);
    viewRcOnlySwitch.setChecked(UserUtils.getValue(getActivity()));
    return paramLayoutInflater;
  }
  
  public void onResume()
  {
    super.onResume();
    ((UserSettingsActivity)getActivity()).onProgressChanged(2131231320);
  }
  
  public void photoUploadQualityClick()
  {
    CharSequence[] arrayOfCharSequence = ImageQualityType.getTitles(getActivity());
    int i = ImageQualityType.findByName(getActivity(), photoQualityText.getText().toString()).ordinal();
    new AlertDialog.Builder(getActivity()).setTitle(2131231015).setSingleChoiceItems(arrayOfCharSequence, i, new UserSettingsMainMenuFragment.2(this, arrayOfCharSequence)).setNegativeButton(2131230820, new UserSettingsMainMenuFragment.1(this)).show();
  }
  
  public void updatePersonalInfoClick()
  {
    onPostExecute(new UserSettingsUpdateProfileFragment(), "update_profile");
  }
}
