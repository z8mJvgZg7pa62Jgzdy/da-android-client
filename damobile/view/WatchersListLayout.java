package com.deviantart.android.damobile.view;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.adapter.recyclerview.WatchersAdapter;
import com.deviantart.android.damobile.fragment.UserProfileFragment;
import com.deviantart.android.damobile.stream.FilteredStream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.filter.FriendFilter;
import com.deviantart.android.damobile.stream.filter.StreamFilter;
import com.deviantart.android.damobile.stream.filter.WatcherFilter;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.OnWatchRecoClickListener;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.UserWatchStateChangeListener;

public class WatchersListLayout
  extends DAStateRecyclerView
{
  private OnWatchRecoClickListener m;
  private String rm;
  
  private WatchersListLayout(Context paramContext, String paramString, StreamLoader paramStreamLoader, OnWatchRecoClickListener paramOnWatchRecoClickListener)
  {
    super(paramContext);
    setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    setPadding((int)getResources().getDimension(2131361964), 0, (int)getResources().getDimension(2131361964), 0);
    m = paramOnWatchRecoClickListener;
    setEndlessScrollListener(8);
    onViewCreated();
    rm = paramString;
    if (paramString == null)
    {
      paramOnWatchRecoClickListener = new WatchPageEmptyStateView(paramContext, WatchPageEmptyStateView.State.b);
      if (m != null) {
        paramOnWatchRecoClickListener.setOnWatchRecoClickListener(m);
      }
      setEmptyStateView(paramOnWatchRecoClickListener);
      if (paramString != null) {
        break label222;
      }
    }
    label222:
    for (paramStreamLoader = new FilteredStream(StreamCacher.a(paramStreamLoader), new StreamFilter[] { new WatcherFilter(), new FriendFilter() });; paramStreamLoader = new FilteredStream(StreamCacher.a(paramStreamLoader), new StreamFilter[] { new WatcherFilter() }))
    {
      setAdapter(new WatchersAdapter(paramStreamLoader));
      if (paramString != null) {
        break label251;
      }
      d(paramContext);
      d(true);
      return;
      ViewState.Helper.a(b, paramString, getContext().getString(2131230896), paramString + " " + getContext().getString(2131230897));
      break;
    }
    label251:
    b(UserProfileFragment.init(paramContext));
  }
  
  private void d(Context paramContext)
  {
    if (!DVNTContextUtils.isContextDead(paramContext))
    {
      if (UserUtils.b <= 0) {
        return;
      }
      paramContext = LayoutInflater.from(paramContext).inflate(2130968739, this, false);
      paramContext.setOnClickListener(WatchersListLayout..Lambda.1.d(this));
      a(paramContext);
    }
  }
  
  public WatchersAdapter getAdapter()
  {
    return (WatchersAdapter)super.getAdapter();
  }
  
  public void refreshView()
  {
    if (rm == null)
    {
      d();
      d(getContext());
    }
    super.refreshView();
  }
  
  public void setEventSource(String paramString)
  {
    if (getAdapter() == null) {
      return;
    }
    getAdapter().setNamespace(paramString);
  }
  
  public void setUserWatchStateChangeListener(UserWatchStateChangeListener paramUserWatchStateChangeListener)
  {
    if (getAdapter() == null) {
      return;
    }
    getAdapter().setDisplayMode(paramUserWatchStateChangeListener);
  }
}
