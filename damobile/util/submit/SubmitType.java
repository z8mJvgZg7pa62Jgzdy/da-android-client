package com.deviantart.android.damobile.util.submit;

public enum SubmitType
{
  private String name;
  
  private SubmitType(String paramString)
  {
    name = paramString;
  }
  
  public static SubmitType findByName(String paramString)
  {
    SubmitType[] arrayOfSubmitType = values();
    int k = arrayOfSubmitType.length;
    int j = 0;
    while (j < k)
    {
      SubmitType localSubmitType = arrayOfSubmitType[j];
      if (name.equals(paramString)) {
        return localSubmitType;
      }
      j += 1;
    }
    return null;
  }
  
  public String getText()
  {
    return name;
  }
}
