package com.deviantart.android.damobile.util;

public enum GalleryType
{
  static
  {
    b = new GalleryType("TORPEDO", 1);
    d = new GalleryType("FULLVIEW", 2);
    g = new GalleryType("FULLWIDTH", 3);
  }
}
