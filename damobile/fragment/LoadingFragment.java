package com.deviantart.android.damobile.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.activity.HomeActivity.BottomBarButton;

public class LoadingFragment
  extends HomeBaseFragment
{
  public LoadingFragment() {}
  
  protected HomeActivity.BottomBarButton c()
  {
    return HomeActivity.BottomBarButton.o;
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    return paramLayoutInflater.inflate(2130968731, paramViewGroup, false);
  }
  
  public boolean refresh()
  {
    return false;
  }
}
