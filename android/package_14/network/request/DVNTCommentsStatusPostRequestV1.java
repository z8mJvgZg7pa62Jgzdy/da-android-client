package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTCommentsStatusPostRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTComment>
{
  final String body;
  final String commentId;
  final String statusId;
  
  public DVNTCommentsStatusPostRequestV1(String paramString1, String paramString2, String paramString3)
  {
    super(com.deviantart.android.sdk.api.model.DVNTComment.class);
    statusId = paramString1;
    commentId = paramString2;
    body = paramString3;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTCommentsStatusPostRequestV1 localDVNTCommentsStatusPostRequestV1 = (DVNTCommentsStatusPostRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(statusId, statusId).append(commentId, commentId).append(body, body).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(statusId).append(commentId).append(body).toHashCode();
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTComment sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).commentsStatusPost(paramString, statusId, commentId, body, Boolean.valueOf(true));
  }
}
