package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.deviantart.android.sdk.api.network.request.GroupableRequest;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.HashMap;

public class DVNTAccountValidationRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTAccountValidateResponse>
  implements GroupableRequest
{
  private String email;
  private String password;
  private String userName;
  
  public DVNTAccountValidationRequestV1(String paramString1, String paramString2, String paramString3)
  {
    super(com.deviantart.android.sdk.api.model.DVNTAccountValidateResponse.class);
    userName = paramString1;
    password = paramString2;
    email = paramString3;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTAccountValidationRequestV1)paramObject;
    return (Objects.append(userName, userName)) && (Objects.append(password, password)) && (Objects.append(email, email));
  }
  
  public HashMap getGETArguments()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("username", userName);
    localHashMap.put("password", password);
    localHashMap.put("email", email);
    return localHashMap;
  }
  
  public String getGroupEndpoint()
  {
    return "/api/v1/oauth2/user/account/validate";
  }
  
  public HashMap getPOSTArguments()
  {
    return null;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), userName, password, email });
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTAccountValidateResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).validateAccount(paramString, userName, password, email);
  }
}
