package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.fragment.FullTorpedoFragment;
import com.deviantart.android.damobile.fragment.FullTorpedoFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.HomeBaseFragment.HomeFragmentInstanceBuilder;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APIMoreLikeThisLoader;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;

public class MLTButton
  extends DeviationMenuButton
{
  public MLTButton(Activity paramActivity, DVNTDeviation paramDVNTDeviation)
  {
    super(paramActivity, paramDVNTDeviation);
    init(paramActivity);
  }
  
  public MLTButton(Activity paramActivity, String paramString)
  {
    super(paramActivity, paramString);
    init(paramActivity);
  }
  
  private void init(Activity paramActivity)
  {
    paramActivity = new ImageView(paramActivity);
    paramActivity.setImageResource(2130837834);
    paramActivity.setContentDescription("TapHoldMLT");
    addView(paramActivity, new FrameLayout.LayoutParams(-1, -1));
  }
  
  public void b(DVNTDeviation paramDVNTDeviation)
  {
    TrackerUtil.a((Activity)getContext(), EventKeys.Category.y, "tap_morelikethis");
    String str = paramDVNTDeviation.getId();
    Stream localStream = StreamCacher.a(new APIMoreLikeThisLoader(str, null), StreamCacheStrategy.d);
    Object localObject = DeviationUtils.b(paramDVNTDeviation);
    if (localObject != null) {}
    for (localObject = ((DVNTImage)localObject).getSrc();; localObject = null)
    {
      paramDVNTDeviation = (FullTorpedoFragment)new FullTorpedoFragment.InstanceBuilder().a(localStream).a(getContext().getString(2131231090)).b("mlt").putShort(paramDVNTDeviation.getTitle()).putInt(paramDVNTDeviation.getAuthor().getUserName()).c((String)localObject).a();
      ScreenFlowManager.a(getActivityContext(), paramDVNTDeviation, "fullmlt" + str);
      return;
    }
  }
  
  public boolean b()
  {
    if (i != null)
    {
      b(i);
      return true;
    }
    DVNTCommonAsyncAPI.deviationInfo(t).call(getContext(), new MLTButton.1(this));
    return true;
  }
  
  public Activity getActivityContext()
  {
    return (Activity)getContext();
  }
}
