package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.model.DVNTFeedbackCursoredPage;
import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTFeedbackAllMessagesRequestV1
  extends DVNTAbstractFeedbackAllMessagesRequestV1
{
  private Integer filterVersion;
  
  public DVNTFeedbackAllMessagesRequestV1(String paramString1, Boolean paramBoolean, String paramString2, Integer paramInteger)
  {
    super(paramString1, paramBoolean, paramString2);
    filterVersion = paramInteger;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()) && (super.equals(paramObject)))
    {
      paramObject = (DVNTFeedbackAllMessagesRequestV1)paramObject;
      return Objects.append(filterVersion, filterVersion);
    }
    return false;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), filterVersion });
  }
  
  protected DVNTFeedbackCursoredPage sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).getAllFeedbackMessages(paramString, messageFolderId, returnStackedMessages, cursor, filterVersion, Boolean.valueOf(true));
  }
}
