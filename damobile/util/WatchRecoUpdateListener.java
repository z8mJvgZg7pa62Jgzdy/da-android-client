package com.deviantart.android.damobile.util;

import android.content.Context;
import com.deviantart.android.android.package_14.model.DVNTWatchRecommendationItem;

public abstract interface WatchRecoUpdateListener
{
  public abstract void a(boolean paramBoolean, DVNTWatchRecommendationItem paramDVNTWatchRecommendationItem);
  
  public abstract void b(Context paramContext, DVNTWatchRecommendationItem paramDVNTWatchRecommendationItem, int paramInt);
}
