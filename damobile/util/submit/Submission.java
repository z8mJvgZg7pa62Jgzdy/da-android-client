package com.deviantart.android.damobile.util.submit;

import android.util.Log;
import com.deviantart.android.android.package_14.model.DVNTCategory;
import com.deviantart.android.damobile.fragment.SubmitGallery;
import com.deviantart.android.damobile.fragment.SubmitMention;
import com.deviantart.android.damobile.fragment.SubmitTag;
import com.deviantart.android.damobile.util.DAFormatUtils;
import java.io.File;
import java.util.ArrayList;

public class Submission
{
  private static Submission mInstance;
  private ArrayList<SubmitMention> a = new ArrayList();
  private boolean b = false;
  private String category;
  private String code;
  private File directory;
  private SubmitType e;
  private boolean first = false;
  private Boolean hidden = Boolean.valueOf(true);
  private String language;
  private Boolean mHidden;
  private SubmitOptions mList = new SubmitOptions();
  private DVNTCategory mMessage;
  private MimeType mimeType;
  private ArrayList<SubmitTag> n = new ArrayList();
  private ArrayList<SubmitGallery> q = new ArrayList();
  private Boolean saved = Boolean.valueOf(true);
  private File source;
  private MimeType tags;
  private transient Submission.CompletionChangeListener title;
  private String url;
  
  public Submission() {}
  
  public static boolean add(String paramString)
  {
    return DAFormatUtils.a(paramString) <= 50;
  }
  
  public static Submission getInstance()
  {
    if (mInstance == null) {
      mInstance = new Submission();
    }
    return mInstance;
  }
  
  public static void setTags()
  {
    mInstance = null;
  }
  
  public void add(ArrayList paramArrayList)
  {
    n = paramArrayList;
  }
  
  public boolean add()
  {
    return (directory != null) && (tags != null);
  }
  
  public void append(boolean paramBoolean)
  {
    first = paramBoolean;
  }
  
  public boolean apply()
  {
    return mMessage != null;
  }
  
  public boolean create()
  {
    if (e == null)
    {
      Log.e("Submission", "Submission type is null when checking isReady");
      return false;
    }
    switch (Submission.1.c[e.ordinal()])
    {
    default: 
      Log.e("Submission", "Reached unreachable code. Unhandled submission type?");
      return false;
    case 1: 
      return (load()) && (apply()) && (add());
    case 2: 
    case 3: 
      return (load()) && (apply()) && (equals());
    }
    return parse();
  }
  
  public String doInBackground()
  {
    if (e == null) {
      return ".jpg";
    }
    switch (Submission.1.c[e.ordinal()])
    {
    default: 
      return ".jpg";
    case 1: 
      return ".jpg";
    case 2: 
    case 3: 
      return ".txt";
    }
    return ".txt";
  }
  
  public void download(File paramFile)
  {
    source = paramFile;
  }
  
  public boolean equals()
  {
    return (source != null) && (mimeType != null) && (!url.isEmpty());
  }
  
  public ArrayList get()
  {
    return a;
  }
  
  public String getCategory()
  {
    return category;
  }
  
  public ArrayList getKey()
  {
    return n;
  }
  
  public SubmitOptions getList()
  {
    return mList;
  }
  
  public DVNTCategory getMessage()
  {
    return mMessage;
  }
  
  public MimeType getMimeType()
  {
    return mimeType;
  }
  
  public String getName()
  {
    return language;
  }
  
  public File getParent()
  {
    return source;
  }
  
  public SubmitType getString()
  {
    return e;
  }
  
  public MimeType getTags()
  {
    return tags;
  }
  
  public String getTitle()
  {
    return code;
  }
  
  public String getUrl()
  {
    return url;
  }
  
  public boolean getValue()
  {
    return b;
  }
  
  public void init(ArrayList paramArrayList)
  {
    q = paramArrayList;
  }
  
  public boolean isEmpty()
  {
    return first;
  }
  
  public Boolean isHidden()
  {
    return mHidden;
  }
  
  public boolean load()
  {
    return (category == null) || (add(category));
  }
  
  public boolean parse()
  {
    return (add()) || (setLanguage()) || ((source != null) && (!url.isEmpty()));
  }
  
  public ArrayList remove()
  {
    return q;
  }
  
  public void setCategory(String paramString)
  {
    category = paramString;
  }
  
  public void setHidden(Boolean paramBoolean)
  {
    mHidden = paramBoolean;
  }
  
  public void setLanguage(String paramString)
  {
    language = paramString;
  }
  
  public boolean setLanguage()
  {
    return code != null;
  }
  
  public void setTitle()
  {
    if (title != null) {
      title.a(this);
    }
  }
  
  public void setTitle(DVNTCategory paramDVNTCategory)
  {
    mMessage = paramDVNTCategory;
  }
  
  public void setTitle(MimeType paramMimeType)
  {
    tags = paramMimeType;
  }
  
  public void setTitle(Submission.CompletionChangeListener paramCompletionChangeListener)
  {
    title = paramCompletionChangeListener;
  }
  
  public void setTitle(SubmitOptions paramSubmitOptions)
  {
    mList = paramSubmitOptions;
  }
  
  public void setTitle(SubmitType paramSubmitType)
  {
    e = paramSubmitType;
  }
  
  public void setTitle(File paramFile)
  {
    directory = paramFile;
  }
  
  public void setTitle(String paramString)
  {
    code = paramString;
  }
  
  public void setTitle(ArrayList paramArrayList)
  {
    a = paramArrayList;
  }
  
  public void setTitle(boolean paramBoolean)
  {
    b = paramBoolean;
  }
  
  public void setUrl(MimeType paramMimeType)
  {
    mimeType = paramMimeType;
  }
  
  public void setUrl(String paramString)
  {
    url = paramString;
  }
  
  public File size()
  {
    return directory;
  }
}
