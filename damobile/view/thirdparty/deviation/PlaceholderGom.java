package com.deviantart.android.damobile.view.thirdparty.deviation;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.view.gom.deviation.DeviationGom;
import com.deviantart.android.damobile.view.thirdparty.GomType;

public class PlaceholderGom
  extends DeviationGom<RecyclerView.ViewHolder>
{
  public PlaceholderGom() {}
  
  public RecyclerView.ViewHolder b(Context paramContext, ViewGroup paramViewGroup)
  {
    paramContext = new View(paramContext);
    paramContext.setLayoutParams(new ViewGroup.LayoutParams(100, 100));
    return new PlaceholderGom.1(this, paramContext);
  }
  
  public GomType b()
  {
    return GomType.i;
  }
  
  public void b(Context paramContext, DeviationDescription paramDeviationDescription, RecyclerView.ViewHolder paramViewHolder, boolean paramBoolean) {}
}
