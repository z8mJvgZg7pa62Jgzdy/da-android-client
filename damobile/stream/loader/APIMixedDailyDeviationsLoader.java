package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.damobile.util.DAFormatUtils.DateFormats;
import com.deviantart.android.damobile.util.DailyDeviationItem;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class APIMixedDailyDeviationsLoader
  extends StreamLoader<DailyDeviationItem>
{
  private boolean b = false;
  private int d = 0;
  
  public APIMixedDailyDeviationsLoader() {}
  
  public void add(Context paramContext, int paramInt, StreamLoadListener paramStreamLoadListener)
  {
    Object localObject = Calendar.getInstance();
    ((Calendar)localObject).setTime(new Date());
    ((Calendar)localObject).add(5, -paramInt);
    localObject = StreamCacher.a(new APIBrowseDailyDeviationsLoader(DAFormatUtils.DateFormats.OUTPUT_TIME.format(((Calendar)localObject).getTime())), StreamCacheStrategy.d);
    if (paramInt <= 365) {}
    for (boolean bool = true;; bool = false)
    {
      ((Stream)localObject).a(paramContext, new APIMixedDailyDeviationsLoader.1(this, (Stream)localObject, paramStreamLoadListener, bool));
      return;
    }
  }
  
  public String b()
  {
    return "mixeddailydeviationsloader";
  }
  
  public void b(Context paramContext, StreamLoadListener paramStreamLoadListener)
  {
    Stream localStream = StreamCacher.a(new APIBrowseDailyDeviationsLoader(null), StreamCacheStrategy.b);
    localStream.a(paramContext, new APIMixedDailyDeviationsLoader.2(this, localStream, paramStreamLoadListener));
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    if (b) {
      return;
    }
    b = true;
    if (d == 0)
    {
      b(paramContext, paramStreamLoadListener);
      return;
    }
    add(paramContext, d, paramStreamLoadListener);
  }
}
