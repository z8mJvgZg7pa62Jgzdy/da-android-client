package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTFeedItem
  implements Serializable
{
  @SerializedName("added_count")
  Integer addedCount;
  @SerializedName("bucketid")
  String bucketId;
  @SerializedName("bucket_total")
  Integer bucketTotal;
  DVNTCollection collection;
  @SerializedName("comment")
  DVNTComment comment;
  @SerializedName("comment_deviation")
  DVNTDeviation commentedDeviation;
  @SerializedName("comment_profile")
  DVNTUser commentedProfile;
  @SerializedName("comment_status")
  DVNTUserStatus commentedStatus;
  DVNTAbstractDeviation.List deviations;
  @SerializedName("formerly")
  String formerUserName;
  @SerializedName("comment_parent")
  DVNTComment parentComment;
  @SerializedName("status")
  DVNTUserStatus status;
  @SerializedName("ts")
  String time;
  String type;
  @SerializedName("by_user")
  DVNTUser user;
  
  public DVNTFeedItem() {}
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      paramObject = (DVNTFeedItem)paramObject;
      return new EqualsBuilder().append(time, time).append(type, type).append(user, user).append(deviations, deviations).append(comment, comment).append(parentComment, parentComment).append(commentedDeviation, commentedDeviation).append(commentedProfile, commentedProfile).append(commentedStatus, commentedStatus).append(status, status).append(formerUserName, formerUserName).append(addedCount, addedCount).append(collection, collection).append(bucketTotal, bucketTotal).isEquals();
    }
    return false;
  }
  
  public Integer getAddedCount()
  {
    return addedCount;
  }
  
  public String getBucketId()
  {
    return bucketId;
  }
  
  public Integer getBucketTotal()
  {
    return bucketTotal;
  }
  
  public DVNTCollection getCollection()
  {
    return collection;
  }
  
  public DVNTComment getComment()
  {
    return comment;
  }
  
  public DVNTDeviation getCommentedDeviation()
  {
    return commentedDeviation;
  }
  
  public DVNTUser getCommentedProfile()
  {
    return commentedProfile;
  }
  
  public DVNTUserStatus getCommentedStatus()
  {
    return commentedStatus;
  }
  
  public DVNTAbstractDeviation.List getDeviations()
  {
    return deviations;
  }
  
  public String getFormerUserName()
  {
    return formerUserName;
  }
  
  public DVNTComment getParentComment()
  {
    return parentComment;
  }
  
  public DVNTUserStatus getStatus()
  {
    return status;
  }
  
  public String getTime()
  {
    return time;
  }
  
  public String getType()
  {
    return type;
  }
  
  public DVNTUser getUser()
  {
    return user;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().append(time).append(type).append(user).append(deviations).append(comment).append(parentComment).append(commentedDeviation).append(commentedProfile).append(commentedStatus).append(status).append(formerUserName).append(addedCount).append(collection).append(bucketTotal).toHashCode();
  }
  
  public void setAddedCount(Integer paramInteger)
  {
    addedCount = paramInteger;
  }
  
  public void setBucketId(String paramString)
  {
    bucketId = paramString;
  }
  
  public void setBucketTotal(Integer paramInteger)
  {
    bucketTotal = paramInteger;
  }
  
  public void setCollection(DVNTCollection paramDVNTCollection)
  {
    collection = paramDVNTCollection;
  }
  
  public void setComment(DVNTComment paramDVNTComment)
  {
    comment = paramDVNTComment;
  }
  
  public void setCommentedDeviation(DVNTDeviation paramDVNTDeviation)
  {
    commentedDeviation = paramDVNTDeviation;
  }
  
  public void setCommentedProfile(DVNTUser paramDVNTUser)
  {
    commentedProfile = paramDVNTUser;
  }
  
  public void setCommentedStatus(DVNTUserStatus paramDVNTUserStatus)
  {
    commentedStatus = paramDVNTUserStatus;
  }
  
  public void setDeviations(DVNTAbstractDeviation.List paramList)
  {
    deviations = paramList;
  }
  
  public void setFormerUserName(String paramString)
  {
    formerUserName = paramString;
  }
  
  public void setParentComment(DVNTComment paramDVNTComment)
  {
    parentComment = paramDVNTComment;
  }
  
  public void setStatus(DVNTUserStatus paramDVNTUserStatus)
  {
    status = paramDVNTUserStatus;
  }
  
  public void setTime(String paramString)
  {
    time = paramString;
  }
  
  public void setType(String paramString)
  {
    type = paramString;
  }
  
  public void setUser(DVNTUser paramDVNTUser)
  {
    user = paramDVNTUser;
  }
}
