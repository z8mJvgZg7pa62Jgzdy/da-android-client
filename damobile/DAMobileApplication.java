package com.deviantart.android.damobile;

import android.app.Application;
import android.content.Context;
import android.content.ContextWrapper;
import android.support.multidex.MultiDex;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.config.DVNTAPIConfig;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig.TempBuildType;
import com.deviantart.android.damobile.util.DeveloperUtils;
import com.deviantart.android.damobile.util.DeviceIdUtil;
import com.deviantart.android.damobile.util.FileCache;
import com.deviantart.android.damobile.util.GraduateHandler;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.tracking.InternalPixelTracker;
import com.deviantart.android.damobile.util.tracking.pacaya.MobileLava;
import com.deviantart.caccao.Caccao;
import com.deviantart.caccao.Caccao.Builder;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.core.ImagePipelineConfig.Builder;
import com.google.android.com.analytics.GoogleAnalytics;
import com.google.android.com.analytics.Logger;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import java.util.HashMap;
import rx.Observable;

public class DAMobileApplication
  extends Application
{
  private static Caccao j;
  private static Context sContext;
  private HashMap<DAMobileApplication.TrackerName, com.google.android.gms.analytics.Tracker> m = new HashMap();
  private RefWatcher refWatcher;
  
  public DAMobileApplication() {}
  
  private void a()
  {
    Observable localObservable = Observable.create(DAMobileApplication..Lambda.1.b(this, DeveloperUtils.b(this)));
    j = new Caccao.Builder().a(2801).a("1UwFq1ZxfCcsvCXsMvuCfs9XjzYcrOcT").delete(localObservable).write(getPackageName()).startActionMode();
  }
  
  public static Caccao b()
  {
    return j;
  }
  
  private void d()
  {
    InternalPixelTracker.b(this);
  }
  
  private void e()
  {
    GoogleAnalytics.getInstance(this).getLogger().setLogLevel(0);
  }
  
  private void f()
  {
    Fresco.b(this, ImagePipelineConfig.b(this).a(true).b());
  }
  
  public static Context getContext()
  {
    return sContext;
  }
  
  public static RefWatcher getRefWatcher(Context paramContext)
  {
    return getApplicationContextrefWatcher;
  }
  
  private void putInt()
  {
    MobileLava.a(this);
  }
  
  private void setSetting()
  {
    int i = 0;
    DVNTAPIConfig localDVNTAPIConfig = new DVNTAPIConfig();
    localDVNTAPIConfig.setGraduateHandler(new GraduateHandler());
    localDVNTAPIConfig.setShowMatureContent(Boolean.valueOf(false));
    localDVNTAPIConfig.setClientId("1700");
    localDVNTAPIConfig.setClientSecret("9f66b7b5b0b2eaf9fa776c8ad3ec9428");
    localDVNTAPIConfig.setScope("basic email push publish group daprivate user stash account browse browse.mlt collection comment.manage comment.post feed gallery message note user.manage deviation.manage challenge");
    localDVNTAPIConfig.setShowRCContent(Boolean.valueOf(UserUtils.getValue(this)));
    switch ("store".hashCode())
    {
    default: 
      i = -1;
      label109:
      switch (i)
      {
      }
      break;
    }
    for (;;)
    {
      localDVNTAPIConfig.setUserAgent("DeviantArt-Android/1.12.3");
      localDVNTAPIConfig.setSessionId(DeviceIdUtil.getDeviceId(this));
      DVNTAbstractAsyncAPI.start(this, localDVNTAPIConfig);
      putInt();
      return;
      if (!"store".equals("debug")) {
        break;
      }
      break label109;
      if (!"store".equals("rc")) {
        break;
      }
      i = 1;
      break label109;
      if (!"store".equals("sandbox")) {
        break;
      }
      i = 2;
      break label109;
      if (!"store".equals("partner")) {
        break;
      }
      i = 3;
      break label109;
      localDVNTAPIConfig.setTempBuildType(DVNTAbstractAPIConfig.TempBuildType.PREVIEW);
      continue;
      localDVNTAPIConfig.setTempBuildType(DVNTAbstractAPIConfig.TempBuildType.SELECT);
      continue;
      localDVNTAPIConfig.setTempBuildType(DVNTAbstractAPIConfig.TempBuildType.SANDBOX);
      continue;
      localDVNTAPIConfig.setTempBuildType(DVNTAbstractAPIConfig.TempBuildType.PARTNER);
    }
  }
  
  protected void attachBaseContext(Context paramContext)
  {
    super.attachBaseContext(paramContext);
    MultiDex.install(this);
  }
  
  public com.google.android.com.analytics.Tracker get(DAMobileApplication.TrackerName paramTrackerName)
  {
    for (;;)
    {
      try
      {
        if (!m.containsKey(paramTrackerName)) {
          localObject = GoogleAnalytics.getInstance(this);
        }
        switch (DAMobileApplication.1.a[paramTrackerName.ordinal()])
        {
        case 1: 
          paramTrackerName = (com.google.android.com.analytics.Tracker)m.get(paramTrackerName);
          return paramTrackerName;
        }
      }
      catch (Throwable paramTrackerName)
      {
        Object localObject;
        throw paramTrackerName;
      }
      localObject = ((GoogleAnalytics)localObject).newTracker(2131099648);
      ((com.google.android.com.analytics.Tracker)localObject).enableAdvertisingIdCollection(true);
      m.put(paramTrackerName, localObject);
    }
  }
  
  public void onCreate()
  {
    super.onCreate();
    refWatcher = LeakCanary.install(this);
    sContext = getApplicationContext();
    FileCache.initialize(this);
    setSetting();
    e();
    f();
    d();
    a();
  }
}
