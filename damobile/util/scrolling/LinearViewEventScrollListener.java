package com.deviantart.android.damobile.util.scrolling;

public class LinearViewEventScrollListener
  extends RecyclerViewScrollListenerBase
{
  private LinearViewEventScrollListener.LinearScrollEventListener m;
  private int n = 5;
  
  public LinearViewEventScrollListener(LinearViewEventScrollListener.LinearScrollEventListener paramLinearScrollEventListener)
  {
    m = paramLinearScrollEventListener;
  }
  
  public void b(int paramInt1, int paramInt2, int paramInt3)
  {
    if ((n <= 30) && (paramInt3 >= n))
    {
      m.a(n, 30);
      n += 5;
    }
  }
}
