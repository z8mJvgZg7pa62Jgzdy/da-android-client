package com.deviantart.android.damobile.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import com.deviantart.android.damobile.util.DAFont;

public class WatchRecoWatchButton
  extends WatchButton
{
  public WatchRecoWatchButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    checkMark.setTypeface(DAFont.g.get(getContext()));
  }
  
  protected void b(AbstractWatchButton.ButtonState paramButtonState1, AbstractWatchButton.ButtonState paramButtonState2)
  {
    super.b(paramButtonState1, paramButtonState2);
    if (paramButtonState2 == AbstractWatchButton.ButtonState.d)
    {
      checkMark.setVisibility(0);
      return;
    }
    checkMark.setVisibility(8);
  }
}
