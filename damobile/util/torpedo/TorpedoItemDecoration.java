package com.deviantart.android.damobile.util.torpedo;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;

public class TorpedoItemDecoration
  extends RecyclerView.ItemDecoration
{
  private int mSpanCount;
  
  public TorpedoItemDecoration(Context paramContext)
  {
    mSpanCount = paramContext.getResources().getDimensionPixelOffset(2131361991);
  }
  
  public void getItemOffsets(Rect paramRect, View paramView, RecyclerView paramRecyclerView, RecyclerView.State paramState)
  {
    super.getItemOffsets(paramRect, paramView, paramRecyclerView, paramState);
    paramRect.set(mSpanCount, mSpanCount, mSpanCount, mSpanCount);
  }
}
