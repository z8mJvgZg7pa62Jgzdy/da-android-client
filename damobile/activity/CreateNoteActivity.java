package com.deviantart.android.damobile.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTNote;
import com.deviantart.android.damobile.fragment.CreateNoteFragment.InstanceBuilder;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import java.util.ArrayList;

public class CreateNoteActivity
  extends LoggedInActivity
{
  public CreateNoteActivity() {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968603);
    setSupportActionBar((Toolbar)findViewById(2131689845));
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    ButterKnife.bind(this);
    paramBundle = new CreateNoteFragment.InstanceBuilder();
    if (getIntent().hasExtra("deviation")) {
      paramBundle.a((DVNTDeviation)getIntent().getExtras().getSerializable("deviation"));
    }
    if (getIntent().hasExtra("username")) {
      paramBundle.a((ArrayList)getIntent().getSerializableExtra("username"));
    }
    if (getIntent().hasExtra("subject")) {
      paramBundle.a(getIntent().getStringExtra("subject"));
    }
    if (getIntent().hasExtra("note_data")) {
      paramBundle.a((DVNTNote)getIntent().getSerializableExtra("note_data"));
    }
    ScreenFlowManager.get(this, paramBundle.a(), "submit_fragment", false);
  }
}
