package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTUpdateUserProfileRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  private Integer artistLevel;
  private Integer artistSpecialty;
  private String copyright;
  private Integer countryCode;
  private Boolean isArtist;
  private String realName;
  private String tagLine;
  private String website;
  
  public DVNTUpdateUserProfileRequestV1(Boolean paramBoolean, Integer paramInteger1, Integer paramInteger2, String paramString1, String paramString2, Integer paramInteger3, String paramString3, String paramString4)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    isArtist = paramBoolean;
    artistLevel = paramInteger1;
    artistSpecialty = paramInteger2;
    realName = paramString1;
    tagLine = paramString2;
    countryCode = paramInteger3;
    website = paramString3;
    copyright = paramString4;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTUpdateUserProfileRequestV1 localDVNTUpdateUserProfileRequestV1 = (DVNTUpdateUserProfileRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(isArtist, isArtist).append(artistLevel, artistLevel).append(artistSpecialty, artistSpecialty).append(realName, realName).append(tagLine, tagLine).append(countryCode, countryCode).append(website, website).append(copyright, copyright).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(isArtist).append(artistLevel).append(artistSpecialty).append(realName).append(tagLine).append(countryCode).append(website).append(copyright).toHashCode();
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).updateUserProfile(paramString, isArtist, artistLevel, artistSpecialty, realName, tagLine, countryCode, website, copyright);
  }
}
