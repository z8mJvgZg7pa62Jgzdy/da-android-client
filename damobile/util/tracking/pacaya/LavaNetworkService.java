package com.deviantart.android.damobile.util.tracking.pacaya;

import com.deviantart.android.android.BuildConfig;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.config.DVNTAPIConfig;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig;
import com.deviantart.android.android.package_14.model.DVNTEnrichedToken;
import com.deviantart.android.android.package_14.oauth.DVNTOAuthHelper;
import com.deviantart.pacaya.FlushService;
import com.deviantart.sdk.log.DVNTTopicEvent;
import com.deviantart.upgrade.DVNTSDK;
import com.deviantart.upgrade.DVNTSDK.Builder;
import com.deviantart.upgrade.DVNTToken;
import com.deviantart.upgrade.labs.DVNTLogRequest;
import java.util.List;
import java.util.Random;
import rx.Observable;

public class LavaNetworkService
  implements FlushService<DVNTTopicEvent>
{
  DVNTSDK j;
  DVNTOAuthHelper o = new DVNTOAuthHelper();
  
  public LavaNetworkService()
  {
    DVNTAPIConfig localDVNTAPIConfig = DVNTAbstractAsyncAPI.getConfig();
    DVNTSDK.Builder localBuilder1 = new DVNTSDK.Builder().a("1700").b("9f66b7b5b0b2eaf9fa776c8ad3ec9428").putByte("basic email push publish group daprivate user stash account browse browse.mlt collection comment.manage comment.post feed gallery message note user.manage deviation.manage challenge").a("dA-session-id", localDVNTAPIConfig.getSessionId()).a("dA-minor-version", BuildConfig.API_VERSION.toString());
    if (localDVNTAPIConfig.getTempBuildType() != null) {
      localBuilder1.a("dA-app-build", localDVNTAPIConfig.getTempBuildType().toString().toLowerCase());
    }
    DVNTSDK.Builder localBuilder2 = localBuilder1.a("User-Agent", localDVNTAPIConfig.getUserAgent());
    if (localDVNTAPIConfig.getShowMatureContent().booleanValue()) {}
    for (Object localObject = "1";; localObject = "0")
    {
      localBuilder2.a("mature_content", (String)localObject);
      localObject = localDVNTAPIConfig.getShowRCContent();
      if ((localObject != null) && (((Boolean)localObject).booleanValue())) {
        localBuilder1.e("rconly", "1");
      }
      j = localBuilder1.b();
      return;
    }
  }
  
  public Observable b(List paramList)
  {
    Object localObject2 = null;
    Object localObject1 = DVNTAsyncAPI.getClientToken();
    DVNTEnrichedToken localDVNTEnrichedToken = DVNTAsyncAPI.getUserToken();
    new Random();
    DVNTToken localDVNTToken;
    if ((localObject1 != null) && (!o.hasTokenExpired((DVNTEnrichedToken)localObject1)))
    {
      localDVNTToken = new DVNTToken();
      z = ((DVNTEnrichedToken)localObject1).getToken();
    }
    for (;;)
    {
      localObject1 = localObject2;
      if (localDVNTEnrichedToken != null)
      {
        localObject1 = localObject2;
        if (!o.hasTokenExpired(localDVNTEnrichedToken))
        {
          localObject1 = new DVNTToken();
          z = localDVNTEnrichedToken.getToken();
        }
      }
      j.b(localDVNTToken);
      j.a((DVNTToken)localObject1);
      return j.flatMap(new DVNTLogRequest(paramList)).map(LavaNetworkService..Lambda.1.getCovers()).onErrorResumeNext(LavaNetworkService..Lambda.2.getCovers());
      localDVNTToken = null;
    }
  }
}
