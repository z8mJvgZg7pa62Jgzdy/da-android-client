package com.deviantart.android.damobile.view.userprofile;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.fragment.UserBioOverlayFragment;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.ViewHelper;
import com.deviantart.android.damobile.util.markup.MarkupHelper;

public class UserProfileCardBio
  extends FrameLayout
  implements UserProfileCardBase
{
  private boolean b;
  private UserCardData mQuery;
  
  public UserProfileCardBio(Context paramContext, boolean paramBoolean)
  {
    super(paramContext);
    b = paramBoolean;
    LayoutInflater.from(paramContext).inflate(2130968789, this, true);
    ButterKnife.bind(this);
  }
  
  public void onClickReadFull()
  {
    if (mQuery == null) {
      return;
    }
    ScreenFlowManager.a((Activity)getContext(), UserBioOverlayFragment.doSearch(mQuery), "user_bio_card_overlay|" + Integer.toString(mQuery.hashCode()));
  }
  
  public void onCreateView(UserCardData paramUserCardData)
  {
    mQuery = paramUserCardData;
    UserProfileCardBio.ViewHolder localViewHolder = new UserProfileCardBio.ViewHolder(this);
    if (b) {
      ViewHelper.a(cardRight, cardRight.getPaddingTop() + Graphics.add(getContext(), 20));
    }
    String str = paramUserCardData.getId();
    if ((str != null) && (str.length() > 2000)) {
      str = str.substring(0, 2000);
    }
    for (int i = 1;; i = 0)
    {
      username.setMovementMethod(LinkMovementMethod.getInstance());
      if (TextUtils.isEmpty(str)) {
        username.setVisibility(8);
      }
      for (;;)
      {
        ViewHelper.setText(website, paramUserCardData.getOverview());
        if (!TextUtils.isEmpty(str)) {
          break;
        }
        return;
        MarkupHelper.onPostExecute((Activity)getContext(), str, username, false);
      }
      if (i != 0)
      {
        readFullBio.setVisibility(0);
        return;
      }
      username.getViewTreeObserver().addOnGlobalLayoutListener(new UserProfileCardBio.1(this, localViewHolder));
      return;
    }
  }
}
