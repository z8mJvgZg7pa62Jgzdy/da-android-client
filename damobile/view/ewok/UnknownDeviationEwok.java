package com.deviantart.android.damobile.view.ewok;

import android.app.Activity;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.damobile.util.DeviationType;

public class UnknownDeviationEwok
  extends ImageDeviationEwok
{
  public UnknownDeviationEwok(DVNTDeviation paramDVNTDeviation)
  {
    super(paramDVNTDeviation);
  }
  
  public DVNTImage b(Activity paramActivity)
  {
    return null;
  }
  
  public DeviationType b()
  {
    return DeviationType.d;
  }
}
