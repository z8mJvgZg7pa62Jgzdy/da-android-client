package com.deviantart.android.damobile.util.submit;

import com.google.common.base.Objects;
import com.google.common.base.Objects.ToStringHelper;
import java.io.Serializable;

public class SimpleTextStatus
  implements Serializable
{
  private String b;
  private String d;
  
  public SimpleTextStatus() {}
  
  public String b()
  {
    return d;
  }
  
  public String c()
  {
    return b;
  }
  
  public void d(String paramString)
  {
    d = paramString;
  }
  
  public void goTo(String paramString)
  {
    b = paramString;
  }
  
  public String toString()
  {
    return Objects.toStringHelper(this).add("subject", b).add("content", d).toString();
  }
}
