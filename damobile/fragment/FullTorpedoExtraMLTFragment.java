package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.loader.APIMoreLikeThisLoader;

public class FullTorpedoExtraMLTFragment
  extends FullTorpedoFragment
{
  public FullTorpedoExtraMLTFragment() {}
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    DVNTCommonAsyncAPI.deviationInfo(((APIMoreLikeThisLoader)a.length()).c()).call(getActivity(), new FullTorpedoExtraMLTFragment.1(this));
    return paramLayoutInflater;
  }
}
