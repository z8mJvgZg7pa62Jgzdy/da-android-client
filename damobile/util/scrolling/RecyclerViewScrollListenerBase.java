package com.deviantart.android.damobile.util.scrolling;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView.OnScrollListener;

public abstract class RecyclerViewScrollListenerBase
  extends RecyclerView.OnScrollListener
{
  public RecyclerViewScrollListenerBase() {}
  
  public void a(RecyclerView paramRecyclerView, int paramInt1, int paramInt2)
  {
    paramRecyclerView = (LinearLayoutManager)paramRecyclerView.getLayoutManager();
    b(paramRecyclerView.getChildCount(), paramRecyclerView.getItemCount(), paramRecyclerView.findFirstVisibleItemPosition());
  }
  
  public abstract void b(int paramInt1, int paramInt2, int paramInt3);
}
