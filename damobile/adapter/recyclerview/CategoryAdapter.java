package com.deviantart.android.damobile.adapter.recyclerview;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.DAMobileApplication;
import com.deviantart.android.damobile.adapter.HeaderFooterRecyclerViewAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.stream.loader.StreamLoader.ErrorType;
import com.deviantart.android.damobile.util.threaditem.CategoryItem;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class CategoryAdapter
  extends ThreadedItemsAdapter<com.deviantart.android.sdk.api.model.DVNTCategory, com.deviantart.android.sdk.api.model.DVNTCategory, CategoryItem>
{
  private String a;
  private boolean c = false;
  private CategoryAdapter.CategorySelectedListener context;
  private boolean f = false;
  private String name;
  private String num;
  
  private CategoryAdapter(Context paramContext, StreamLoader paramStreamLoader, String paramString, boolean paramBoolean1, boolean paramBoolean2, CategoryAdapter.CategorySelectedListener paramCategorySelectedListener)
  {
    super(StreamCacher.a(paramStreamLoader));
    name = paramContext.getString(2131231244);
    num = paramContext.getString(2131230822);
    f = paramBoolean1;
    a = paramString;
    c = paramBoolean2;
    context = paramCategorySelectedListener;
  }
  
  public int a(List paramList)
  {
    if (c)
    {
      localObject = new com.deviantart.android.android.package_14.model.DVNTCategory();
      ((com.deviantart.android.android.package_14.model.DVNTCategory)localObject).setTitle(name);
      ((com.deviantart.android.android.package_14.model.DVNTCategory)localObject).setCatPath("/");
      ((com.deviantart.android.android.package_14.model.DVNTCategory)localObject).setParentPath("");
      ((com.deviantart.android.android.package_14.model.DVNTCategory)localObject).setHasSubCategory(Boolean.valueOf(false));
      paramList.add(0, localObject);
    }
    Object localObject = paramList.iterator();
    while (((Iterator)localObject).hasNext())
    {
      CategoryItem localCategoryItem = new CategoryItem((com.deviantart.android.android.package_14.model.DVNTCategory)((Iterator)localObject).next());
      c.add(localCategoryItem);
    }
    return paramList.size();
  }
  
  public void a(Context paramContext, CategoryItem paramCategoryItem, int paramInt)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: fail exe a5 = a4\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.exec(BaseAnalyze.java:92)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.exec(BaseAnalyze.java:1)\n\tat com.googlecode.dex2jar.ir.ts.Cfg.dfs(Cfg.java:255)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.analyze0(BaseAnalyze.java:75)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.analyze(BaseAnalyze.java:69)\n\tat com.googlecode.dex2jar.ir.ts.UnSSATransformer.transform(UnSSATransformer.java:274)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:163)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\nCaused by: java.lang.NullPointerException\n\tat com.googlecode.dex2jar.ir.ts.UnSSATransformer$LiveA.onUseLocal(UnSSATransformer.java:552)\n\tat com.googlecode.dex2jar.ir.ts.UnSSATransformer$LiveA.onUseLocal(UnSSATransformer.java:1)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.onUse(BaseAnalyze.java:166)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.onUse(BaseAnalyze.java:1)\n\tat com.googlecode.dex2jar.ir.ts.Cfg.travel(Cfg.java:331)\n\tat com.googlecode.dex2jar.ir.ts.Cfg.travel(Cfg.java:387)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.exec(BaseAnalyze.java:90)\n\t... 17 more\n");
  }
  
  public void a(String paramString, CategoryItem paramCategoryItem, int paramInt)
  {
    paramString = (Stream)a.get(paramString);
    if (paramString == null) {
      return;
    }
    paramInt = b(paramCategoryItem, paramInt);
    try
    {
      d(paramInt);
      i = paramInt + 1;
      if ((!c.isEmpty()) && (paramCategoryItem.visitAnnotation().booleanValue()))
      {
        paramString = paramString.get();
        if (c)
        {
          Object localObject = paramCategoryItem.get();
          com.deviantart.android.android.package_14.model.DVNTCategory localDVNTCategory = new com.deviantart.android.android.package_14.model.DVNTCategory();
          localDVNTCategory.setTitle(((com.deviantart.android.android.package_14.model.DVNTCategory)localObject).getTitle());
          localDVNTCategory.setCatPath(((com.deviantart.android.android.package_14.model.DVNTCategory)localObject).getCatPath());
          localDVNTCategory.setParentPath(((com.deviantart.android.android.package_14.model.DVNTCategory)localObject).getCatPath());
          localDVNTCategory.setHasSubCategory(Boolean.valueOf(false));
          localObject = new CategoryItem(localDVNTCategory);
          ((CategoryItem)localObject).b(true);
          ((CategoryItem)localObject).a(Integer.valueOf(paramCategoryItem.getValue().intValue() + 1));
          c.add(0 + i, localObject);
          paramInt = 1;
          paramString = paramString.iterator();
          while (paramString.hasNext())
          {
            localObject = new CategoryItem((com.deviantart.android.android.package_14.model.DVNTCategory)paramString.next());
            ((CategoryItem)localObject).a(Integer.valueOf(paramCategoryItem.getValue().intValue() + 1));
            c.add(i + paramInt, localObject);
            paramInt += 1;
          }
        }
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        int i;
        new Handler().post(CategoryAdapter..Lambda.1.c(this));
        continue;
        try
        {
          append(i, paramInt);
          return;
        }
        catch (Exception paramString)
        {
          new Handler().post(CategoryAdapter..Lambda.2.c(this));
          return;
        }
        paramInt = 0;
      }
    }
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    return new CategoryAdapter.CategoryViewHolder(this, LayoutInflater.from(paramViewGroup.getContext()).inflate(2130968615, paramViewGroup, false));
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    CategoryAdapter.CategoryViewHolder localCategoryViewHolder = (CategoryAdapter.CategoryViewHolder)paramViewHolder;
    CategoryItem localCategoryItem = (CategoryItem)c.get(paramInt);
    com.deviantart.android.android.package_14.model.DVNTCategory localDVNTCategory = localCategoryItem.get();
    if (!localCategoryItem.a())
    {
      paramViewHolder = localDVNTCategory.getTitle();
      name.setText(paramViewHolder);
      int i = itemView.getContext().getResources().getColor(2131558441);
      int j = localCategoryItem.getValue().intValue();
      paramViewHolder = new float[3];
      Color.colorToHSV(Integer.valueOf(i).intValue(), paramViewHolder);
      paramViewHolder[2] = ((float)Math.pow(paramViewHolder[2], 1.0D / Integer.valueOf(j + 1).intValue()));
      i = Color.HSVToColor(paramViewHolder);
      categoryLayout.setBackgroundColor(Integer.valueOf(i).intValue());
      loader.setVisibility(8);
      if ((!localDVNTCategory.getHasSubCategory().booleanValue()) || (localCategoryItem.a())) {
        break label254;
      }
      hasSubItemsIcon.setVisibility(0);
    }
    for (;;)
    {
      if (localCategoryViewHolder.getKeyAlgorithName() != null) {
        a.remove(localCategoryViewHolder.getKeyAlgorithName());
      }
      localCategoryViewHolder.f(localCategoryItem.f());
      itemView.setOnClickListener(new CategoryAdapter.1(this, localCategoryItem, paramInt));
      return;
      paramViewHolder = num + localDVNTCategory.getTitle();
      break;
      label254:
      hasSubItemsIcon.setVisibility(8);
    }
  }
  
  public void d(CategoryItem paramCategoryItem, int paramInt)
  {
    Context localContext = DAMobileApplication.getContext();
    if (DVNTContextUtils.isContextDead(localContext)) {
      return;
    }
    paramInt = b(paramCategoryItem, paramInt);
    try
    {
      d(paramInt);
      if (f)
      {
        Toast.makeText(localContext, 2131230901, 0).show();
        return;
      }
    }
    catch (Exception paramCategoryItem)
    {
      for (;;)
      {
        new Handler().post(CategoryAdapter..Lambda.3.c(this));
      }
      Toast.makeText(localContext, 2131230900, 0).show();
    }
  }
  
  public void d(CategoryItem paramCategoryItem, int paramInt, StreamLoader.ErrorType paramErrorType, String paramString)
  {
    paramErrorType = DAMobileApplication.getContext();
    if (DVNTContextUtils.isContextDead(paramErrorType)) {
      return;
    }
    paramInt = b(paramCategoryItem, paramInt);
    try
    {
      d(paramInt);
      Toast.makeText(paramErrorType, 2131230900, 0).show();
      return;
    }
    catch (Exception paramCategoryItem)
    {
      for (;;)
      {
        new Handler().post(CategoryAdapter..Lambda.4.c(this));
      }
    }
  }
}
