package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.activity.HomeActivity;
import com.deviantart.android.damobile.adapter.DiscoveryPagerAdapter;
import com.deviantart.android.damobile.stream.loader.APIBrowseHotLoader;
import com.deviantart.android.damobile.stream.loader.APIBrowseNewestLoader;
import com.deviantart.android.damobile.stream.loader.APIBrowsePopularLoader;
import com.deviantart.android.damobile.stream.loader.APIBrowseUndiscoveredLoader;
import com.deviantart.android.damobile.util.ZoomOutPageTransformer;
import com.deviantart.android.damobile.util.discovery.DiscoveryPageInfo;
import com.deviantart.android.damobile.util.discovery.pages.DiscoveryCategoryBrowsePage;
import com.deviantart.android.damobile.util.discovery.pages.DiscoveryCategorySelectorPage;
import com.deviantart.android.damobile.util.discovery.pages.DiscoveryPage;
import com.deviantart.android.damobile.util.discovery.pages.DiscoveryTorpedoPage;
import com.deviantart.android.damobile.util.tourhelper.TapAndHoldTourHelper;
import com.deviantart.android.damobile.util.tourhelper.TourHelperBase;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.view.viewpageindicator.CustomizableTabPageIndicator;
import com.deviantart.android.damobile.view.viewpageindicator.DATextTabIndicator;
import samples.grantland.widget.AutofitHelper;

public class DiscoverySearchFragment
  extends HomeFullViewFragment
{
  protected static String a;
  protected static String c;
  @Bind({2131689921})
  TextView browseCategoryTitle;
  @Bind({2131690023})
  LinearLayout categorySelectorLayout;
  protected boolean header;
  private String i;
  @Bind({2131689756})
  DATextTabIndicator indicator;
  protected DiscoveryCategorySelectorPage j;
  private TapAndHoldTourHelper l;
  protected boolean more = false;
  @Bind({2131689755})
  ViewPager pager;
  @Bind({2131689925})
  TextView searchedCategoryBarTitle;
  @Bind({2131689927})
  TextView searchedText;
  
  public DiscoverySearchFragment() {}
  
  private void c(DiscoveryPagerAdapter paramDiscoveryPagerAdapter, boolean paramBoolean)
  {
    DiscoveryTorpedoPage localDiscoveryTorpedoPage = new DiscoveryTorpedoPage(getActivity(), DiscoveryPageInfo.q, getString() + "/Newest", false, APIBrowseNewestLoader.class);
    localDiscoveryTorpedoPage.a(EventKeys.Category.f);
    localDiscoveryTorpedoPage.a(getString(2131230891));
    paramDiscoveryPagerAdapter.b(localDiscoveryTorpedoPage, paramBoolean);
    if (c == null)
    {
      localDiscoveryTorpedoPage = new DiscoveryTorpedoPage(getActivity(), DiscoveryPageInfo.c, getString() + "/Undiscovered", false, APIBrowseUndiscoveredLoader.class);
      localDiscoveryTorpedoPage.a(EventKeys.Category.g);
      paramDiscoveryPagerAdapter.b(localDiscoveryTorpedoPage, paramBoolean);
    }
    localDiscoveryTorpedoPage = new DiscoveryTorpedoPage(getActivity(), DiscoveryPageInfo.d.getString(), DiscoveryPageInfo.d.get(getActivity()), getString() + "/Popular", APIBrowsePopularLoader.class, "24hr");
    localDiscoveryTorpedoPage.a(getString(2131230891));
    localDiscoveryTorpedoPage.a(EventKeys.Category.f);
    paramDiscoveryPagerAdapter.b(localDiscoveryTorpedoPage, paramBoolean);
    if (c == null)
    {
      localDiscoveryTorpedoPage = new DiscoveryTorpedoPage(getActivity(), DiscoveryPageInfo.a, getString() + "/WhatsHot", false, APIBrowseHotLoader.class);
      localDiscoveryTorpedoPage.a(EventKeys.Category.l);
      paramDiscoveryPagerAdapter.b(localDiscoveryTorpedoPage, paramBoolean);
    }
    if (c != null)
    {
      localDiscoveryTorpedoPage = new DiscoveryTorpedoPage(getActivity(), DiscoveryPageInfo.g, getString() + "/AllTime", true, APIBrowsePopularLoader.class, "alltime");
      localDiscoveryTorpedoPage.a(getString(2131230891));
      localDiscoveryTorpedoPage.a(EventKeys.Category.f);
      paramDiscoveryPagerAdapter.b(localDiscoveryTorpedoPage, paramBoolean);
    }
  }
  
  private int onCreateView()
  {
    return (int)getActivity().getResources().getDimension(2131361971);
  }
  
  public boolean b()
  {
    if ((categorySelectorLayout.getVisibility() == 0) && (isResumed()))
    {
      closeCategoryBurgerMenu();
      return true;
    }
    return false;
  }
  
  public void closeCategoryBurgerMenu()
  {
    categorySelectorLayout.setVisibility(8);
    ((DiscoveryPagerAdapter)pager.getAdapter()).a(getActivity(), pager.getCurrentItem());
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setRetainInstance(true);
  }
  
  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    if (getArguments().getString("search_query") == null) {
      paramMenuInflater.inflate(2131820548, paramMenu);
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramBundle = getArguments().getString("search_category_classpath");
    Object localObject = getArguments().getString("search_query");
    i = getArguments().getString("search_page_label");
    header = getArguments().getBoolean("search_from_category");
    boolean bool;
    if (((paramBundle != null) && (!paramBundle.equals(a))) || ((localObject != null) && (!((String)localObject).equals(c))))
    {
      bool = true;
      a = paramBundle;
      c = (String)localObject;
      paramBundle = new DiscoveryPagerAdapter();
      paramBundle.d(onCreateView());
      c(paramBundle, bool);
      paramViewGroup = (ViewGroup)paramLayoutInflater.inflate(2130968669, paramViewGroup, false);
      localObject = (AppBarLayout)paramViewGroup.findViewById(2131689754);
      if (c != null) {
        break label403;
      }
      paramLayoutInflater.inflate(2130968703, (ViewGroup)localObject, true);
    }
    for (;;)
    {
      ButterKnife.bind(this, paramViewGroup);
      initUI();
      ((BaseActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      if (a != null)
      {
        paramBundle.b(a);
        if (browseCategoryTitle != null)
        {
          AutofitHelper.doInit(browseCategoryTitle);
          browseCategoryTitle.setText(getArguments().getString("search_category_title"));
        }
        if (searchedCategoryBarTitle != null) {
          searchedCategoryBarTitle.setText(getArguments().getString("search_category_title"));
        }
      }
      if (c != null)
      {
        paramBundle.a(c);
        if (searchedText != null)
        {
          searchedText.setText(c);
          searchedText.clearFocus();
        }
      }
      pager.setAdapter(paramBundle);
      pager.setPageTransformer(true, new ZoomOutPageTransformer());
      indicator.setViewPager(pager);
      indicator.setOnPageChangeListener(new DiscoverySearchFragment.1(this));
      pager.setCurrentItem(paramBundle.get(getActivity()));
      if (TapAndHoldTourHelper.putInt(getActivity())) {
        return paramViewGroup;
      }
      l = new TapAndHoldTourHelper(getActivity(), 2131689752);
      l.b(paramViewGroup, onSuccess());
      return paramViewGroup;
      bool = false;
      break;
      label403:
      paramLayoutInflater.inflate(2130968709, (ViewGroup)localObject, true);
      paramLayoutInflater.inflate(2130968708, (ViewGroup)localObject, true);
    }
    return paramViewGroup;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    if (j != null) {
      j.b();
    }
    a = null;
    c = null;
    ButterKnife.unbind(this);
    if (l != null)
    {
      l.d();
      l = null;
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      break;
    }
    for (;;)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      getActivity().onBackPressed();
    }
  }
  
  public void onResume()
  {
    super.onResume();
    if (pager.getCurrentItem() == 0) {
      ((DiscoveryPagerAdapter)pager.getAdapter()).a(getActivity(), 0);
    }
  }
  
  public void onSearchInputClick()
  {
    getActivity().onBackPressed();
  }
  
  public void openCategoryBurgerMenu()
  {
    categorySelectorLayout.setVisibility(0);
    ((HomeActivity)getActivity()).show(false);
    if (!more)
    {
      j = new DiscoveryCategorySelectorPage("categorySelect", "", getString() + "/CategorySelector");
      categorySelectorLayout.addView(j.a(categorySelectorLayout));
      j.b(new DiscoverySearchFragment.2(this));
      more = true;
    }
    TrackerUtil.run(getActivity(), j.getDescriptor());
  }
}
