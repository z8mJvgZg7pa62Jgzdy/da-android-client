package com.deviantart.android.damobile.view.viewpageindicator;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.viewpagerindicator.IconPagerAdapter;
import com.viewpagerindicator.PageIndicator;

public class CustomizableTabPageIndicator
  extends HorizontalScrollView
  implements PageIndicator
{
  protected static final CharSequence k = "";
  protected final View.OnClickListener a = new CustomizableTabPageIndicator.1(this);
  protected ViewPager.OnPageChangeListener mListener;
  protected int mMaxTabWidth;
  protected CustomizableTabPageIndicator.OnTabReselectedListener mMessage;
  protected int mSelectedTabIndex;
  protected final IcsLinearLayout mTabLayout;
  protected Runnable mTabSelector;
  protected ViewPager mViewPager;
  
  public CustomizableTabPageIndicator(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public CustomizableTabPageIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setHorizontalScrollBarEnabled(false);
    mTabLayout = new IcsLinearLayout(paramContext, getDefaultStyleAttr());
    addView(mTabLayout, getTabContainerLayout());
  }
  
  protected void a(int paramInt1, CharSequence paramCharSequence, int paramInt2)
  {
    CustomizableTabPageIndicator.TextViewTab localTextViewTab = new CustomizableTabPageIndicator.TextViewTab(this, getContext());
    z = paramInt1;
    localTextViewTab.setFocusable(true);
    localTextViewTab.setOnClickListener(a);
    localTextViewTab.setText(paramCharSequence);
    if (paramInt2 != 0) {
      localTextViewTab.setCompoundDrawablesWithIntrinsicBounds(paramInt2, 0, 0, 0);
    }
    mTabLayout.addView(localTextViewTab, b(paramInt1, paramCharSequence, paramInt2));
  }
  
  protected void animateToTab(int paramInt)
  {
    View localView = mTabLayout.getChildAt(paramInt);
    if (mTabSelector != null) {
      removeCallbacks(mTabSelector);
    }
    mTabSelector = new CustomizableTabPageIndicator.2(this, localView);
    post(mTabSelector);
  }
  
  protected LinearLayout.LayoutParams b(int paramInt1, CharSequence paramCharSequence, int paramInt2)
  {
    return new LinearLayout.LayoutParams(0, -1, 1.0F);
  }
  
  public void b(int paramInt)
  {
    setCurrentItem(paramInt);
    if (mListener != null) {
      mListener.b(paramInt);
    }
  }
  
  protected int getDefaultStyleAttr()
  {
    return 2130772341;
  }
  
  protected ViewGroup.LayoutParams getTabContainerLayout()
  {
    return new ViewGroup.LayoutParams(-2, -1);
  }
  
  protected int measureHeight(int paramInt1, int paramInt2)
  {
    if (paramInt1 > 2) {
      return (int)(View.MeasureSpec.getSize(paramInt2) * 0.4F);
    }
    return View.MeasureSpec.getSize(paramInt2) / 2;
  }
  
  public void notifyDataSetChanged()
  {
    mTabLayout.removeAllViews();
    PagerAdapter localPagerAdapter = mViewPager.getAdapter();
    IconPagerAdapter localIconPagerAdapter = null;
    if ((localPagerAdapter instanceof IconPagerAdapter)) {
      localIconPagerAdapter = (IconPagerAdapter)localPagerAdapter;
    }
    int m = localPagerAdapter.getCount();
    int i = 0;
    CharSequence localCharSequence;
    if (i < m)
    {
      localCharSequence = localPagerAdapter.getPageTitle(i);
      if (localCharSequence != null) {
        break label127;
      }
      localCharSequence = k;
    }
    label127:
    for (;;)
    {
      if (localIconPagerAdapter != null) {}
      for (int j = localIconPagerAdapter.getIcon(i);; j = 0)
      {
        a(i, localCharSequence, j);
        i += 1;
        break;
        if (mSelectedTabIndex > m) {
          mSelectedTabIndex = (m - 1);
        }
        setCurrentItem(mSelectedTabIndex);
        requestLayout();
        return;
      }
    }
  }
  
  public void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    if (mTabSelector != null) {
      post(mTabSelector);
    }
  }
  
  public void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    if (mTabSelector != null) {
      removeCallbacks(mTabSelector);
    }
  }
  
  public void onMeasure(int paramInt1, int paramInt2)
  {
    int i = View.MeasureSpec.getMode(paramInt1);
    boolean bool;
    int j;
    if (i == 1073741824)
    {
      bool = true;
      setFillViewport(bool);
      j = mTabLayout.getChildCount();
      if ((j <= 1) || ((i != 1073741824) && (i != Integer.MIN_VALUE))) {
        break label99;
      }
    }
    label99:
    for (mMaxTabWidth = measureHeight(j, paramInt1);; mMaxTabWidth = -1)
    {
      i = getMeasuredWidth();
      super.onMeasure(paramInt1, paramInt2);
      paramInt1 = getMeasuredWidth();
      if ((!bool) || (i == paramInt1)) {
        return;
      }
      setCurrentItem(mSelectedTabIndex);
      return;
      bool = false;
      break;
    }
  }
  
  public void onPageScrollStateChanged(int paramInt)
  {
    if (mListener != null) {
      mListener.onPageScrollStateChanged(paramInt);
    }
  }
  
  public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2)
  {
    if (mListener != null) {
      mListener.onPageScrolled(paramInt1, paramFloat, paramInt2);
    }
  }
  
  public void setCurrentItem(int paramInt)
  {
    if (mViewPager == null) {
      throw new IllegalStateException("ViewPager has not been bound.");
    }
    mSelectedTabIndex = paramInt;
    mViewPager.setCurrentItem(paramInt);
    int j = mTabLayout.getChildCount();
    int i = 0;
    if (i < j)
    {
      View localView = mTabLayout.getChildAt(i);
      if (i == paramInt) {}
      for (boolean bool = true;; bool = false)
      {
        localView.setSelected(bool);
        if (bool) {
          animateToTab(paramInt);
        }
        i += 1;
        break;
      }
    }
  }
  
  public void setOnPageChangeListener(ViewPager.OnPageChangeListener paramOnPageChangeListener)
  {
    mListener = paramOnPageChangeListener;
  }
  
  public void setOnTabReselectedListener(CustomizableTabPageIndicator.OnTabReselectedListener paramOnTabReselectedListener)
  {
    mMessage = paramOnTabReselectedListener;
  }
  
  public void setViewPager(ViewPager paramViewPager)
  {
    if (mViewPager == paramViewPager) {
      return;
    }
    if (mViewPager != null) {
      mViewPager.setOnPageChangeListener(null);
    }
    if (paramViewPager.getAdapter() == null) {
      throw new IllegalStateException("ViewPager does not have adapter instance.");
    }
    mViewPager = paramViewPager;
    paramViewPager.setOnPageChangeListener(this);
    notifyDataSetChanged();
  }
}
