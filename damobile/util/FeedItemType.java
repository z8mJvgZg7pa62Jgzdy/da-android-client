package com.deviantart.android.damobile.util;

public enum FeedItemType
{
  private int o;
  
  static
  {
    b = new FeedItemType("JOURNAL_SUBMITTED", 1, 2131231222);
    d = new FeedItemType("STATUS", 2, 2131231225);
    g = new FeedItemType("COLLECTION_UPDATE", 3, 2131230792);
  }
  
  private FeedItemType(int paramInt)
  {
    o = paramInt;
  }
  
  public static FeedItemType getValue(String paramString)
  {
    if (paramString == null) {
      return null;
    }
    try
    {
      paramString = valueOf(paramString.toUpperCase());
      return paramString;
    }
    catch (IllegalArgumentException paramString) {}
    return null;
  }
  
  public int a()
  {
    return o;
  }
}
