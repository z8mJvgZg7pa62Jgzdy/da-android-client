package com.deviantart.android.damobile.util.markup;

import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation.List;
import com.deviantart.android.android.package_14.model.DVNTStashItem.List;

public class SimpleDAMLContent
{
  private String a = "";
  private DVNTAbstractDeviation.List d = new DVNTAbstractDeviation.List();
  private DVNTAbstractDeviation.List e = new DVNTAbstractDeviation.List();
  private DVNTStashItem.List j = new DVNTStashItem.List();
  private boolean l = false;
  
  public SimpleDAMLContent() {}
  
  public DVNTAbstractDeviation.List a()
  {
    return d;
  }
  
  public void b(String paramString)
  {
    a += paramString;
  }
  
  public void b(boolean paramBoolean)
  {
    l = paramBoolean;
  }
  
  public boolean b()
  {
    return l;
  }
  
  public String c()
  {
    return a;
  }
  
  public DVNTAbstractDeviation.List e()
  {
    return e;
  }
  
  public DVNTStashItem.List getResources()
  {
    return j;
  }
}
