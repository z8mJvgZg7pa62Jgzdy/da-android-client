package com.deviantart.android.damobile.util.opt;

import android.app.Activity;
import android.content.Context;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTNote;
import com.deviantart.android.damobile.stream.FilteredStream;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.filter.NotesModelFilter;
import com.deviantart.android.damobile.stream.filter.StreamFilter;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnNoteItemChangeEvent;
import com.deviantart.android.damobile.util.mc.ItemDeleteHelperBase;
import com.deviantart.android.damobile.util.notes.NotesItemData;
import com.deviantart.android.damobile.util.notes.NotesPage;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.HashSet;

public class NoteItemDeleteHelper
  extends ItemDeleteHelperBase<NotesItemData, NotesPage>
{
  public static HashSet<NotesPage> a = new HashSet();
  
  public NoteItemDeleteHelper() {}
  
  private ItemDeleteHelperBase.ItemDeleteRequestListener lift(NotesItemData paramNotesItemData)
  {
    return new NoteItemDeleteHelper.1(this, paramNotesItemData);
  }
  
  void addView(Activity paramActivity, NotesItemData paramNotesItemData) {}
  
  public Stream b(NotesPage paramNotesPage)
  {
    paramNotesPage = paramNotesPage.b();
    if ((paramNotesPage == null) || (!StreamCacher.b(paramNotesPage))) {
      return null;
    }
    return new FilteredStream(StreamCacher.a(paramNotesPage), new StreamFilter[] { new NotesModelFilter() });
  }
  
  void b(Activity paramActivity, NotesItemData paramNotesItemData)
  {
    TrackerUtil.get(paramActivity, EventKeys.Category.d, "undo_delete", "undo_delete");
  }
  
  void b(NotesItemData paramNotesItemData)
  {
    BusStation.get().post(new BusStation.OnNoteItemChangeEvent(paramNotesItemData, true));
  }
  
  int c(NotesItemData paramNotesItemData)
  {
    return 2131231131;
  }
  
  void deleteChannel(NotesItemData paramNotesItemData)
  {
    BusStation.get().post(new BusStation.OnNoteItemChangeEvent(paramNotesItemData, false));
  }
  
  void readMessage(Context paramContext, NotesItemData paramNotesItemData)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramNotesItemData.getContext().getNoteId());
    DVNTAsyncAPI.deleteNotes(localArrayList).call(paramContext, lift(paramNotesItemData));
  }
}
