package com.deviantart.android.damobile.view.userprofile;

import com.deviantart.android.android.package_14.model.DVNTUserProfile;

public class UserCardDataFactory
{
  public UserCardDataFactory() {}
  
  public static UserCardData initValues(DVNTUserProfile paramDVNTUserProfile)
  {
    return new UserCardDataFactory.UserProfileCardData(paramDVNTUserProfile, null);
  }
}
