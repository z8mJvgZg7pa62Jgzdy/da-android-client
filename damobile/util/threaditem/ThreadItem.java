package com.deviantart.android.damobile.util.threaditem;

public abstract interface ThreadItem<MODEL_TYPE>
{
  public abstract String b();
  
  public abstract String c();
  
  public abstract void d(Boolean paramBoolean);
  
  public abstract String f();
  
  public abstract Boolean visitAnnotation();
}
