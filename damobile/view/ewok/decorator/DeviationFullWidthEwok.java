package com.deviantart.android.damobile.view.ewok.decorator;

import android.app.Activity;
import android.content.res.Resources;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTFeedItem;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.GalleryType;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.PlaceholderUtil;
import com.deviantart.android.damobile.util.StatusViewHelper;
import com.deviantart.android.damobile.util.ViewHelper;
import com.deviantart.android.damobile.view.DAFaveView;
import com.deviantart.android.damobile.view.DeviationEwokBottomBarView;
import com.deviantart.android.damobile.view.LongPressItem;
import com.deviantart.android.damobile.view.ewok.DeviationEwok;
import com.deviantart.android.damobile.view.ewok.Ewok;
import com.deviantart.android.damobile.view.ewok.EwokFactory;
import com.deviantart.android.damobile.view.ewok.RepostEwok;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;

public class DeviationFullWidthEwok
  extends WatchFeedItemEwok
  implements RepostEwok
{
  private final DeviationEwok b;
  private boolean c = false;
  private DVNTFeedItem l;
  
  public DeviationFullWidthEwok(DVNTDeviation paramDVNTDeviation)
  {
    b = EwokFactory.b(paramDVNTDeviation);
  }
  
  public DeviationFullWidthEwok(DVNTFeedItem paramDVNTFeedItem)
  {
    l = paramDVNTFeedItem;
    b = EwokFactory.b((DVNTDeviation)paramDVNTFeedItem.getDeviations().get(0));
  }
  
  private View.OnClickListener findViewById(Activity paramActivity)
  {
    return new DeviationFullWidthEwok.3(this, paramActivity);
  }
  
  private FrameLayout onCreateView(Activity paramActivity, View paramView, ViewGroup paramViewGroup)
  {
    paramViewGroup = (FrameLayout)LayoutInflater.from(paramActivity).inflate(2130968657, paramViewGroup, false);
    FrameLayout localFrameLayout = (FrameLayout)ButterKnife.findById(paramViewGroup, 2131689619);
    TextView localTextView = (TextView)ButterKnife.findById(paramViewGroup, 2131689660);
    LinearLayout localLinearLayout = (LinearLayout)ButterKnife.findById(paramViewGroup, 2131689718);
    localFrameLayout.addView(paramView, -1, -2);
    localFrameLayout.setOnClickListener(findViewById(paramActivity));
    paramView = b.a();
    DeviationEwokBottomBarView localDeviationEwokBottomBarView = new DeviationEwokBottomBarView(paramActivity, b);
    localDeviationEwokBottomBarView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
    localLinearLayout.addView(localDeviationEwokBottomBarView);
    switch (DeviationFullWidthEwok.4.c[b.b().ordinal()])
    {
    default: 
      localTextView.setText(paramView.getTitle());
      return paramViewGroup;
    }
    localTextView.setVisibility(8);
    paramViewGroup.setPadding(0, 0, 0, (int)paramActivity.getResources().getDimension(2131361950));
    localFrameLayout.setPadding(localFrameLayout.getPaddingLeft(), localFrameLayout.getPaddingTop(), localFrameLayout.getPaddingRight(), (int)paramActivity.getResources().getDimension(2131361951));
    localLinearLayout.findViewById(2131689710).setBackgroundColor(paramActivity.getResources().getColor(2131558419));
    localDeviationEwokBottomBarView.setBackgroundColor(paramActivity.getResources().getColor(2131558419));
    return paramViewGroup;
  }
  
  public View onCreateView(Activity paramActivity)
  {
    if (b.a().isDeleted().booleanValue()) {
      return PlaceholderUtil.createView((LayoutInflater)paramActivity.getSystemService("layout_inflater"), 2131231215);
    }
    Object localObject1 = b.a(paramActivity, null);
    Object localObject2 = b.a();
    Object localObject3 = b.b();
    switch (DeviationFullWidthEwok.4.c[localObject3.ordinal()])
    {
    default: 
      return b.b(paramActivity);
    case 1: 
    case 2: 
      localObject3 = (LinearLayout)((LayoutInflater)paramActivity.getSystemService("layout_inflater")).inflate(2130968769, null, false);
      Object localObject4 = (SimpleDraweeView)ButterKnife.findById((View)localObject3, 2131689729);
      Object localObject5 = (TextView)ButterKnife.findById((View)localObject3, 2131689731);
      if (localObject2 == null)
      {
        ((ImageView)localObject4).setVisibility(8);
        ((View)localObject5).setVisibility(8);
      }
      for (;;)
      {
        localObject4 = LongPressItem.a((View)localObject1, DeviationUtils.b(paramActivity, b.a()));
        ((LongPressItem)localObject4).b(DeviationUtils.b(b));
        localObject5 = ButterKnife.findById((View)localObject1, 2131689940);
        ((View)localObject5).setBackgroundColor(0);
        ((LongPressItem)localObject4).b(new DeviationFullWidthEwok.1(this, paramActivity, (View)localObject5));
        ((FrameLayout)ButterKnife.findById((View)localObject3, 2131690032)).addView((View)localObject1);
        paramActivity = new DAFaveView(paramActivity);
        if (localObject2 != null) {
          paramActivity.setTag(((DVNTAbstractDeviation)localObject2).getId());
        }
        paramActivity.addView((View)localObject3, 0);
        DeviationUtils.b(paramActivity, b.a());
        return paramActivity;
        StatusViewHelper.setText(paramActivity, ((DVNTAbstractDeviation)localObject2).getAuthor(), (SimpleDraweeView)localObject4, (TextView)localObject5);
      }
    }
    localObject2 = new DAFaveView(paramActivity);
    ((View)localObject2).setTag(b.a().getId());
    ((View)localObject1).setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
    ((ViewGroup)localObject2).addView((View)localObject1, 0);
    DeviationUtils.b((DAFaveView)localObject2, b.a());
    localObject2 = onCreateView(paramActivity, (View)localObject2, null);
    localObject3 = ButterKnife.findById((View)localObject2, 2131689619);
    localObject1 = LongPressItem.a((View)localObject1, DeviationUtils.b(paramActivity, b.a()));
    ((LongPressItem)localObject1).b(DeviationUtils.b(b));
    ((LongPressItem)localObject1).b(new DeviationFullWidthEwok.2(this, (View)localObject3));
    ButterKnife.findById((View)localObject2, 2131689710).setVisibility(8);
    ViewHelper.a((View)localObject2, paramActivity.getResources().getDrawable(2130837795));
    return localObject2;
  }
  
  public void onCreateView(Activity paramActivity, ViewGroup paramViewGroup1, ViewGroup paramViewGroup2, boolean paramBoolean)
  {
    ImageUtils.a(GalleryType.g);
    if (!paramBoolean)
    {
      paramViewGroup2.removeAllViews();
      paramViewGroup2.addView((LinearLayout)((LayoutInflater)paramActivity.getSystemService("layout_inflater")).inflate(2130968658, paramViewGroup1, false));
    }
    a(paramActivity, paramViewGroup2, l);
    paramViewGroup2 = (FrameLayout)paramViewGroup2.findViewById(2131689719);
    if (paramViewGroup2 == null) {
      return;
    }
    paramViewGroup2.removeAllViews();
    paramViewGroup1 = onCreateView(paramActivity, b.a(paramActivity, paramViewGroup1), paramViewGroup1);
    LongPressItem localLongPressItem = LongPressItem.a(paramViewGroup1.findViewById(2131689619), DeviationUtils.b(paramActivity, b.a()));
    paramActivity = new DAFaveView(paramActivity);
    paramActivity.addView(paramViewGroup1, 0);
    DeviationUtils.b(paramActivity, b.a());
    localLongPressItem.b(DeviationUtils.b(b));
    paramViewGroup2.addView(paramActivity);
  }
}
