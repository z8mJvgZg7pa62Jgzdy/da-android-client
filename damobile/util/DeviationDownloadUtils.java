package com.deviantart.android.damobile.util;

import android.app.AlertDialog.Builder;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.text.format.Formatter;
import android.webkit.MimeTypeMap;
import android.widget.Toast;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviationDownloadInfo;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.util.tracking.InternalPixelTracker;
import com.deviantart.datoolkit.logger.DVNTLog;
import com.deviantart.datoolkit.pixeltracker.DVNTPixelTracker;
import com.deviantart.datoolkit.pixeltracker.DVNTPixelTrackerEvent.Builder;
import java.io.File;
import java.util.ArrayList;

public class DeviationDownloadUtils
{
  private static final Integer a = Integer.valueOf(5242880);
  
  public DeviationDownloadUtils() {}
  
  private static boolean a(DVNTDeviation paramDVNTDeviation)
  {
    return (paramDVNTDeviation.getDownloadFileSize() != null) && (paramDVNTDeviation.getDownloadFileSize().intValue() >= a.intValue());
  }
  
  public static void b(Context paramContext, DVNTDeviation paramDVNTDeviation)
  {
    if ((!ConnectivityUtils.isWifiConnected(paramContext)) && (a(paramDVNTDeviation)))
    {
      String str = paramContext.getString(2131231043).replace("{image_size}", format(paramContext, paramDVNTDeviation));
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
      localBuilder.setMessage(str).setPositiveButton(2131231183, new DeviationDownloadUtils.2(paramContext, paramDVNTDeviation)).setNegativeButton(2131230820, new DeviationDownloadUtils.1());
      localBuilder.show();
      return;
    }
    d(paramContext, paramDVNTDeviation);
  }
  
  private static void d(Context paramContext, DVNTDeviation paramDVNTDeviation)
  {
    if (DVNTContextUtils.isContextDead(paramContext)) {
      return;
    }
    DVNTCommonAsyncAPI.getDeviationDownloadInfo(paramDVNTDeviation.getId()).call(paramContext, new DeviationDownloadUtils.3(paramContext, paramDVNTDeviation));
  }
  
  public static String format(Context paramContext, DVNTDeviation paramDVNTDeviation)
  {
    if (paramDVNTDeviation.getDownloadFileSize() == null) {
      return "";
    }
    return Formatter.formatShortFileSize(paramContext, paramDVNTDeviation.getDownloadFileSize().intValue());
  }
  
  public static PermissionUtil.CheckResult onRequestPermissionsResult(Fragment paramFragment)
  {
    return PermissionUtil.checkPermission(paramFragment, "android.permission.WRITE_EXTERNAL_STORAGE", 2, 2131231200);
  }
  
  private static void onSuccess(Context paramContext, DVNTDeviationDownloadInfo paramDVNTDeviationDownloadInfo, DVNTDeviation paramDVNTDeviation)
  {
    if (DVNTContextUtils.isContextDead(paramContext))
    {
      Toast.makeText(paramContext, 2131230913, 0).show();
      return;
    }
    if (ContextCompat.checkSelfPermission(paramContext, "android.permission.WRITE_EXTERNAL_STORAGE") != 0)
    {
      Toast.makeText(paramContext, 2131230916, 0).show();
      return;
    }
    if ((paramDVNTDeviationDownloadInfo == null) || (paramDVNTDeviationDownloadInfo.getSrc() == null) || (paramDVNTDeviationDownloadInfo.getSrc().isEmpty()))
    {
      Toast.makeText(paramContext, 2131230915, 0).show();
      return;
    }
    Object localObject = new DVNTPixelTrackerEvent.Builder().c("tap").a("deviation").e("download_option").a("deviationid", paramDVNTDeviation.getId());
    if (UserUtils.f != null) {
      ((DVNTPixelTrackerEvent.Builder)localObject).a("userid", UserUtils.f);
    }
    InternalPixelTracker.b().a(((DVNTPixelTrackerEvent.Builder)localObject).e());
    localObject = paramDVNTDeviationDownloadInfo.getSrc();
    paramDVNTDeviationDownloadInfo = MimeTypeMap.getFileExtensionFromUrl((String)localObject);
    paramDVNTDeviationDownloadInfo = DeviationUtils.toString(paramDVNTDeviation).replaceAll(" ", "_") + "." + paramDVNTDeviationDownloadInfo;
    new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/DeviantArt/").mkdirs();
    try
    {
      paramDVNTDeviation = paramContext.getSystemService("download");
      paramDVNTDeviation = (DownloadManager)paramDVNTDeviation;
      localObject = Uri.parse((String)localObject);
      localObject = new DownloadManager.Request((Uri)localObject).setNotificationVisibility(1);
      String str = Environment.DIRECTORY_PICTURES;
      long l = paramDVNTDeviation.enqueue(((DownloadManager.Request)localObject).setDestinationInExternalPublicDir(str, "/DeviantArt/" + paramDVNTDeviationDownloadInfo));
      paramDVNTDeviationDownloadInfo = DeviationDownloadManager.this$0;
      paramDVNTDeviationDownloadInfo.add(Long.valueOf(l));
      return;
    }
    catch (Exception paramDVNTDeviationDownloadInfo)
    {
      DVNTLog.append("Failed to start download manager: " + paramDVNTDeviationDownloadInfo.toString(), new Object[0]);
      Toast.makeText(paramContext, 2131230914, 0).show();
    }
  }
}
