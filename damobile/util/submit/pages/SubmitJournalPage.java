package com.deviantart.android.damobile.util.submit.pages;

import android.content.Context;
import com.deviantart.android.damobile.util.submit.SubmitType;

public class SubmitJournalPage
  extends SubmitTextPage
{
  public SubmitJournalPage(String paramString)
  {
    super(paramString);
  }
  
  public SubmitType c()
  {
    return SubmitType.i;
  }
  
  public String formatBytes()
  {
    return getContext().getString(2131231380);
  }
  
  public CharSequence getFormatted()
  {
    return "SubmitJournalTab";
  }
  
  protected int newHandle()
  {
    return 2130968752;
  }
  
  public boolean setContentHeight()
  {
    return true;
  }
}
