package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTUserFeaturesAndAgreementsRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTFeaturesAndAgreements>
{
  public DVNTUserFeaturesAndAgreementsRequestV1()
  {
    super(com.deviantart.android.sdk.api.model.DVNTFeaturesAndAgreements.class);
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTFeaturesAndAgreements sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).stashPublishUserData(paramString);
  }
}
