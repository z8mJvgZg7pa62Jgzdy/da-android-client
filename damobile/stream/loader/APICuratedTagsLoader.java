package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.damobile.util.CuratedTagPreviewItem;

public class APICuratedTagsLoader
  extends StreamLoader<CuratedTagPreviewItem>
{
  public APICuratedTagsLoader() {}
  
  public String b()
  {
    return "curatedtagsloader";
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTCommonAsyncAPI.curatedTags(false).call(paramContext, new APICuratedTagsLoader.1(this, paramStreamLoadListener));
  }
}
