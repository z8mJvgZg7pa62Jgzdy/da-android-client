package com.deviantart.android.damobile.view.thirdparty;

public enum GomType
{
  static
  {
    a = new GomType("DEVIATION_LITERATURE", 3);
    i = new GomType("DEVIATION_PLACEHOLDER", 4);
    r = new GomType("DEVIATION_UNKNOWN", 5);
    g = new GomType("COMMENT_REGULAR", 6);
    l = new GomType("COMMENT_HIDDEN", 7);
  }
  
  public static ByteVector b(int paramInt)
  {
    return GomFactory.a(c(paramInt));
  }
  
  public static GomType c(int paramInt)
  {
    return values()[paramInt];
  }
}
