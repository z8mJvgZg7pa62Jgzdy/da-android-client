package com.deviantart.android.damobile.adapter.recyclerview;

import android.app.Activity;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.FrameLayout;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.view.ewok.EwokFactory;
import com.deviantart.android.damobile.view.ewok.StatusEwok;
import com.deviantart.android.damobile.view.ewok.decorator.WatchFeedItemEwok;
import java.util.ArrayList;

public class UserStatusAdapter
  extends DAStateRecyclerViewAdapter<com.deviantart.android.sdk.api.model.DVNTUserStatus>
{
  public UserStatusAdapter(Stream paramStream)
  {
    super(paramStream);
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    paramViewGroup = new FrameLayout(paramViewGroup.getContext());
    paramViewGroup.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
    return new UserStatusAdapter.1(this, paramViewGroup);
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    StatusEwok localStatusEwok = EwokFactory.a((com.deviantart.android.android.package_14.model.DVNTUserStatus)c.get().get(paramInt));
    localStatusEwok.b(false);
    paramViewHolder = (FrameLayout)itemView;
    paramViewHolder.removeAllViews();
    localStatusEwok.b(UserStatusAdapter..Lambda.1.b(this));
    paramViewHolder.addView(localStatusEwok.a((Activity)paramViewHolder.getContext(), null));
  }
}
