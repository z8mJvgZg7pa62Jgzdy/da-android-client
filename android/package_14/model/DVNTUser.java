package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTUser
  implements Serializable
{
  DVNTUser.DVNTUserGeo desc;
  DVNTUser.DVNTUserDetails details;
  @SerializedName("is_watching")
  Boolean isWatching;
  DVNTUserProfile profile;
  DVNTUser.DVNTUserStats stats;
  String type;
  @SerializedName("usericon")
  String userIconURL;
  @SerializedName("username")
  String userName;
  @SerializedName("userid")
  String userUUID;
  
  public DVNTUser() {}
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      paramObject = (DVNTUser)paramObject;
      return new EqualsBuilder().append(userUUID, userUUID).append(userName, userName).append(userIconURL, userIconURL).append(type, type).append(details, details).append(desc, desc).append(stats, stats).append(profile, profile).isEquals();
    }
    return false;
  }
  
  public DVNTUser.DVNTUserDetails getDetails()
  {
    return details;
  }
  
  public DVNTUser.DVNTUserGeo getGeo()
  {
    return desc;
  }
  
  public Boolean getIsWatching()
  {
    return isWatching;
  }
  
  public DVNTUserProfile getProfile()
  {
    return profile;
  }
  
  public DVNTUser.DVNTUserStats getStats()
  {
    return stats;
  }
  
  public String getType()
  {
    return type;
  }
  
  public String getUserIconURL()
  {
    return userIconURL;
  }
  
  public String getUserName()
  {
    return userName;
  }
  
  public String getUserUUID()
  {
    return userUUID;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().append(userUUID).append(userName).append(userIconURL).append(type).append(details).append(desc).append(stats).append(profile).toHashCode();
  }
  
  public void setDetails(DVNTUser.DVNTUserDetails paramDVNTUserDetails)
  {
    details = paramDVNTUserDetails;
  }
  
  public void setGeo(DVNTUser.DVNTUserGeo paramDVNTUserGeo)
  {
    desc = paramDVNTUserGeo;
  }
  
  public void setIsWatching(Boolean paramBoolean)
  {
    isWatching = paramBoolean;
  }
  
  public void setProfile(DVNTUserProfile paramDVNTUserProfile)
  {
    profile = paramDVNTUserProfile;
  }
  
  public void setStats(DVNTUser.DVNTUserStats paramDVNTUserStats)
  {
    stats = paramDVNTUserStats;
  }
  
  public void setType(String paramString)
  {
    type = paramString;
  }
  
  public void setUserIconURL(String paramString)
  {
    userIconURL = paramString;
  }
  
  public void setUserName(String paramString)
  {
    userName = paramString;
  }
  
  public void setUserUUID(String paramString)
  {
    userUUID = paramString;
  }
}
