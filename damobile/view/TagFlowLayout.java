package com.deviantart.android.damobile.view;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

public class TagFlowLayout
  extends ViewGroup
{
  private int mLabelsMargin;
  private int mMaxButtonWidth;
  
  public TagFlowLayout(Context paramContext)
  {
    super(paramContext);
    bindView();
  }
  
  public TagFlowLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public TagFlowLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    bindView();
  }
  
  private void bindView()
  {
    try
    {
      mMaxButtonWidth = getResources().getDimensionPixelSize(2131361975);
      mLabelsMargin = getResources().getDimensionPixelSize(2131361976);
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      mMaxButtonWidth = 10;
      mLabelsMargin = 10;
    }
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramInt4 = getPaddingLeft();
    paramInt2 = getPaddingTop();
    int i = 0;
    int k = 0;
    if (k < getChildCount())
    {
      View localView = getChildAt(k);
      int j;
      if (localView.getVisibility() == 8) {
        j = i;
      }
      for (;;)
      {
        k += 1;
        i = j;
        break;
        int i1 = localView.getMeasuredWidth();
        int i2 = localView.getMeasuredHeight();
        int n = Math.max(i2, i);
        j = n;
        int m = paramInt4;
        i = paramInt2;
        if (i1 + paramInt4 + getPaddingRight() > paramInt3 - paramInt1)
        {
          m = getPaddingLeft();
          i = paramInt2 + (n + mLabelsMargin);
          j = 0;
        }
        localView.layout(m, i, m + i1, i2 + i);
        paramInt4 = m + (mMaxButtonWidth + i1);
        paramInt2 = i;
      }
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    int j = getPaddingLeft();
    int i = getPaddingTop();
    int i3 = View.resolveSize(100, paramInt1);
    int n = 0;
    int k = 0;
    if (n < getChildCount())
    {
      View localView = getChildAt(n);
      if (localView.getVisibility() == 8) {}
      for (;;)
      {
        n += 1;
        break;
        localView.measure(ViewGroup.getChildMeasureSpec(paramInt1, getPaddingLeft() + getPaddingRight(), getLayoutParamswidth), ViewGroup.getChildMeasureSpec(paramInt2, getPaddingTop() + getPaddingBottom(), getLayoutParamsheight));
        int i4 = localView.getMeasuredWidth();
        int i2 = Math.max(localView.getMeasuredHeight(), k);
        k = i2;
        int m = i;
        int i1 = j;
        if (i4 + j + getPaddingRight() > i3)
        {
          i1 = getPaddingLeft();
          m = i + (i2 + mLabelsMargin);
          k = 0;
        }
        j = i1 + (mMaxButtonWidth + i4);
        i = m;
      }
    }
    setMeasuredDimension(i3, View.resolveSize(i + k + getPaddingBottom() + 0, paramInt2));
  }
}
