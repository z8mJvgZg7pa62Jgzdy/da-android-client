package com.deviantart.android.damobile.view.image;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

public class SubsamplingImageView
  extends SubsamplingScaleImageView
{
  public SubsamplingImageView(Context paramContext)
  {
    super(paramContext);
  }
  
  public SubsamplingImageView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public void setImage()
  {
    float f1 = getWidth() / getSWidth();
    float f2 = getHeight() / getSHeight();
    if (f1 > f2)
    {
      setDoubleTapZoomScale(f1);
      return;
    }
    setDoubleTapZoomScale(f2);
  }
}
