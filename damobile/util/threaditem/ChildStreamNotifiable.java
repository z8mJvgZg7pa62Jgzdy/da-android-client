package com.deviantart.android.damobile.util.threaditem;

import com.deviantart.android.damobile.stream.Stream.Notifiable;
import com.deviantart.android.damobile.stream.loader.StreamLoader.ErrorType;

public class ChildStreamNotifiable<THREAD_ITEM extends ThreadItem>
  implements Stream.Notifiable
{
  private String d;
  private THREAD_ITEM f;
  private ChildStreamListener<THREAD_ITEM> i;
  private int o;
  
  public ChildStreamNotifiable(ChildStreamListener paramChildStreamListener, String paramString, ThreadItem paramThreadItem, int paramInt)
  {
    i = paramChildStreamListener;
    d = paramString;
    f = paramThreadItem;
    o = paramInt;
  }
  
  public void b()
  {
    i.setText(f, o);
  }
  
  public void b(StreamLoader.ErrorType paramErrorType, String paramString)
  {
    i.a(f, o, paramErrorType, paramString);
  }
  
  public void c()
  {
    i.a(d, f, o);
  }
  
  public void e() {}
  
  public void f() {}
}
