package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.net.Uri;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.activity.HomeActivity.BottomBarButton;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.torpedo.TorpedoAdapter;
import com.deviantart.android.damobile.util.torpedo.TorpedoItemDecoration;
import com.deviantart.android.damobile.util.torpedo.TorpedoLayout;
import com.deviantart.android.damobile.util.torpedo.TorpedoRecyclerView;
import com.deviantart.android.damobile.util.torpedo.TorpedoUtils.TorpedoEndlessScrollListener;
import com.deviantart.android.damobile.util.torpedo.TorpedoUtils.TorpedoNotifiable;
import com.deviantart.android.damobile.util.tourhelper.TapAndHoldTourHelper;
import com.deviantart.android.damobile.util.tourhelper.TourHelperBase;
import com.deviantart.android.damobile.util.tracking.pacaya.MobileLava;
import com.deviantart.android.damobile.util.tracking.pacaya.UxTopicEventCreator;
import com.deviantart.android.damobile.view.EndlessScrollListener;
import com.deviantart.android.sdk.api.model.DVNTDeviation;
import com.deviantart.pacaya.Pacaya;
import com.facebook.drawee.view.SimpleDraweeView;

public class FullTorpedoFragment
  extends HomeBaseFragment
{
  protected Stream<DVNTDeviation> a;
  @Bind({2131689835})
  LinearLayout header;
  @Bind({2131689838})
  TextView headerAuthor;
  @Bind({2131689836})
  SimpleDraweeView headerImage;
  @Bind({2131689837})
  TextView headerTitle;
  private TapAndHoldTourHelper mMultiSelectionController;
  @Bind({2131689834})
  TorpedoLayout torpedoLayout;
  @Bind({2131690078})
  TorpedoRecyclerView torpedoRecyclerView;
  
  public FullTorpedoFragment() {}
  
  protected void a(String paramString1, String paramString2, String paramString3)
  {
    headerTitle.setText(paramString1);
    headerAuthor.setText(paramString2);
    if (paramString3 != null)
    {
      ImageUtils.isEmpty(headerImage, Uri.parse(paramString3));
      return;
    }
    header.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
    header.setWeightSum(2.0F);
    headerImage.setVisibility(8);
  }
  
  protected HomeActivity.BottomBarButton c()
  {
    return HomeActivity.BottomBarButton.o;
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    a = StreamCacher.a((StreamLoader)getArguments().getSerializable("stream_loader"), StreamCacheStrategy.b);
    a.a(0);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramBundle = (LinearLayout)paramLayoutInflater.inflate(2130968687, paramViewGroup, false);
    ButterKnife.bind(this, paramBundle);
    Bundle localBundle = getArguments();
    torpedoRecyclerView.addItemDecoration(new TorpedoItemDecoration(getActivity()));
    paramLayoutInflater = new TorpedoAdapter(torpedoRecyclerView, a);
    paramLayoutInflater.setVisible(localBundle.getBoolean("add_fave_mark", true));
    torpedoRecyclerView.setAdapter(paramLayoutInflater);
    paramLayoutInflater.b();
    torpedoLayout.setIsFromDeepLink(localBundle.getBoolean("is_deep_link", false));
    paramViewGroup = new TorpedoUtils.TorpedoNotifiable(torpedoLayout);
    a.a(getActivity(), paramViewGroup);
    Object localObject = new TorpedoUtils.TorpedoEndlessScrollListener(a, getActivity());
    ((TorpedoUtils.TorpedoEndlessScrollListener)localObject).b(paramViewGroup);
    int i = getResources().getInteger(2131492889);
    torpedoRecyclerView.b(i, (EndlessScrollListener)localObject);
    torpedoLayout.setRefreshListener(new FullTorpedoFragment.1(this, paramViewGroup, paramLayoutInflater));
    paramViewGroup = localBundle.getString("type");
    paramLayoutInflater = paramViewGroup;
    localObject = MobileLava.e();
    UxTopicEventCreator localUxTopicEventCreator = new UxTopicEventCreator();
    if (paramViewGroup != null)
    {
      ((Pacaya)localObject).setText(localUxTopicEventCreator.get(paramLayoutInflater).getString("page").b());
      initUI();
      ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
      ((BaseActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      paramLayoutInflater = localBundle.getString("title");
      if (paramLayoutInflater != null) {
        ((BaseActivity)getActivity()).getSupportActionBar().setTitle(paramLayoutInflater);
      }
      if (localBundle.getString("deviation_title") == null) {
        break label385;
      }
      a(localBundle.getString("deviation_title"), localBundle.getString("deviation_author"), localBundle.getString("deviation_image_src"));
    }
    for (;;)
    {
      if (TapAndHoldTourHelper.putInt(getActivity())) {
        return paramBundle;
      }
      mMultiSelectionController = new TapAndHoldTourHelper(getActivity(), 2131689752);
      mMultiSelectionController.b(paramBundle, onSuccess());
      return paramBundle;
      paramLayoutInflater = "unknwown";
      break;
      label385:
      header.setVisibility(8);
    }
    return paramBundle;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
    if (mMultiSelectionController != null)
    {
      mMultiSelectionController.d();
      mMultiSelectionController = null;
    }
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      break;
    }
    for (;;)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      getActivity().onBackPressed();
    }
  }
}
