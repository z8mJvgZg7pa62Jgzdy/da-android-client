package com.deviantart.android.damobile.view.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.util.SignUpHelper.ValidateUserInfoTask;

public class ChangeEmailDialog
  extends DABaseDialog
{
  @Bind({2131690117})
  ProgressBar changeEmailProgressBar;
  @Bind({2131690112})
  EditText currentPassword;
  private SignUpHelper.ValidateUserInfoTask currentTask;
  String d;
  @Bind({2131690115})
  EditText newEmail;
  @Bind({2131690114})
  ProgressBar newEmailProgressBar;
  
  public ChangeEmailDialog() {}
  
  private void addTask(String paramString)
  {
    DVNTAsyncAPI.validateAccount(null, null, paramString).call(getActivity(), new ChangeEmailDialog.4(this));
  }
  
  public String a()
  {
    return newEmail.getText().toString();
  }
  
  public void emailTextChanged(CharSequence paramCharSequence)
  {
    if (currentTask != null) {
      currentTask.cancel(true);
    }
    if (paramCharSequence.length() == 0)
    {
      newEmailProgressBar.setVisibility(8);
      onPostExecute();
      return;
    }
    newEmail.setError(null);
    newEmailProgressBar.setVisibility(0);
    currentTask = new ChangeEmailDialog.3(this);
    currentTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[] { paramCharSequence.toString() });
  }
  
  public String getValue()
  {
    return currentPassword.getText().toString();
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new AlertDialog.Builder(getActivity());
    View localView = getActivity().getLayoutInflater().inflate(2130968795, null);
    ButterKnife.bind(this, localView);
    if (d == null) {
      d = getString(2131231267);
    }
    paramBundle.setTitle(d).setView(localView).setPositiveButton(2131231183, null).setNegativeButton(2131230820, new ChangeEmailDialog.1(this));
    paramBundle = paramBundle.create();
    paramBundle.setOnShowListener(new ChangeEmailDialog.2(this));
    return paramBundle;
  }
  
  public void onDestroyView()
  {
    ButterKnife.unbind(this);
    if (currentTask != null) {
      currentTask.cancel(true);
    }
    super.onDestroyView();
  }
  
  public void onPostExecute()
  {
    if ((currentPassword.getText() != null) && (!currentPassword.getText().toString().isEmpty()) && (newEmail.getText() != null) && (!newEmail.getText().toString().isEmpty()) && (newEmail.getError() == null)) {}
    for (boolean bool = true;; bool = false)
    {
      ((AlertDialog)getDialog()).getButton(-1).setEnabled(bool);
      return;
    }
  }
  
  public void shortToast(String paramString)
  {
    d = paramString;
  }
  
  public void show(boolean paramBoolean)
  {
    boolean bool2 = true;
    if (isDetached()) {
      return;
    }
    Object localObject;
    boolean bool1;
    if (paramBoolean)
    {
      changeEmailProgressBar.setVisibility(0);
      localObject = newEmail;
      if (paramBoolean) {
        break label96;
      }
      bool1 = true;
      label34:
      ((TextView)localObject).setEnabled(bool1);
      localObject = currentPassword;
      if (paramBoolean) {
        break label101;
      }
      bool1 = true;
      label52:
      ((TextView)localObject).setEnabled(bool1);
      localObject = ((AlertDialog)getDialog()).getButton(-1);
      if (paramBoolean) {
        break label106;
      }
    }
    label96:
    label101:
    label106:
    for (paramBoolean = bool2;; paramBoolean = false)
    {
      ((TextView)localObject).setEnabled(paramBoolean);
      return;
      changeEmailProgressBar.setVisibility(8);
      break;
      bool1 = false;
      break label34;
      bool1 = false;
      break label52;
    }
  }
}
