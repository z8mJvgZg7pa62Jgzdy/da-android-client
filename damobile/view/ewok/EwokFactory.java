package com.deviantart.android.damobile.view.ewok;

import android.util.Log;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTFeedItem;
import com.deviantart.android.android.package_14.model.DVNTRepostItem;
import com.deviantart.android.android.package_14.model.DVNTUserStatus;
import com.deviantart.android.damobile.util.DeviationType;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.FeedItemType;
import com.deviantart.android.damobile.util.ProcessMenuListener;
import com.deviantart.android.damobile.view.ewok.decorator.DeviationFullWidthEwok;
import com.deviantart.android.damobile.view.ewok.decorator.GroupStackDeviationEwok;
import com.deviantart.android.damobile.view.ewok.decorator.WatchFeedItemEwok;
import java.util.ArrayList;

public class EwokFactory
{
  public EwokFactory() {}
  
  public static StatusEwok a(DVNTUserStatus paramDVNTUserStatus)
  {
    return new StatusEwok(paramDVNTUserStatus);
  }
  
  public static DeviationEwok b(DVNTDeviation paramDVNTDeviation)
  {
    DeviationType localDeviationType = DeviationUtils.a(paramDVNTDeviation);
    switch (EwokFactory.1.d[localDeviationType.ordinal()])
    {
    default: 
      Log.e("EwokFactory", "unknown deviation type for deviation : " + paramDVNTDeviation.getTitle());
      return new UnknownDeviationEwok(paramDVNTDeviation);
    case 1: 
      return new ImageDeviationEwok(paramDVNTDeviation);
    case 2: 
      return new LiteratureDeviationEwok(paramDVNTDeviation);
    case 3: 
      return new JournalDeviationEwok(paramDVNTDeviation);
    }
    return new PlaceholderDeviationEwok(paramDVNTDeviation);
  }
  
  public static RepostEwok b(DVNTRepostItem paramDVNTRepostItem, ProcessMenuListener paramProcessMenuListener)
  {
    String str = paramDVNTRepostItem.getType();
    if (str.equals("deviation"))
    {
      paramDVNTRepostItem = paramDVNTRepostItem.getDeviation();
      if (paramDVNTRepostItem == null) {
        return null;
      }
      paramDVNTRepostItem = new DeviationFullWidthEwok(paramDVNTRepostItem);
      paramDVNTRepostItem.b(paramProcessMenuListener);
      return paramDVNTRepostItem;
    }
    if (str.equals("status"))
    {
      paramDVNTRepostItem = paramDVNTRepostItem.getStatus();
      if (paramDVNTRepostItem != null)
      {
        paramDVNTRepostItem = new StatusEwok(paramDVNTRepostItem, true);
        paramDVNTRepostItem.b(paramProcessMenuListener);
        return paramDVNTRepostItem;
      }
    }
    return null;
  }
  
  public static WatchFeedItemEwok b(DVNTFeedItem paramDVNTFeedItem)
  {
    if (paramDVNTFeedItem == null) {
      return null;
    }
    FeedItemType localFeedItemType = FeedItemType.getValue(paramDVNTFeedItem.getType());
    if (localFeedItemType == null)
    {
      Log.e("EwokFactory", "Unhandled feed item type");
      return null;
    }
    switch (EwokFactory.1.c[localFeedItemType.ordinal()])
    {
    default: 
      Log.e("Runtime", "Ewok ActivityFeedItemEwok wat");
      return null;
    case 1: 
    case 2: 
      if (paramDVNTFeedItem.getDeviations() != null)
      {
        if (paramDVNTFeedItem.getDeviations().size() > 1) {
          return new GroupStackDeviationEwok(paramDVNTFeedItem);
        }
        return new DeviationFullWidthEwok(paramDVNTFeedItem);
      }
      break;
    case 3: 
      return new StatusEwok(paramDVNTFeedItem);
    case 4: 
      return new WatchFeedCollectionEwok(paramDVNTFeedItem);
    }
    return null;
  }
}
