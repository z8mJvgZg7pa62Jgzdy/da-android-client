package com.deviantart.android.damobile.util;

import java.util.ArrayList;
import java.util.HashMap;

public class DynamicDialogItemBuilder
{
  private final HashMap<Integer, Integer> b = new HashMap();
  private final ArrayList<CharSequence> items = new ArrayList();
  
  public DynamicDialogItemBuilder() {}
  
  public int a(int paramInt)
  {
    return ((Integer)b.get(Integer.valueOf(paramInt))).intValue();
  }
  
  public void add(int paramInt, CharSequence paramCharSequence)
  {
    items.add(paramCharSequence);
    b.put(Integer.valueOf(items.size() - 1), Integer.valueOf(paramInt));
  }
  
  public CharSequence[] getTitles()
  {
    CharSequence[] arrayOfCharSequence = new CharSequence[items.size()];
    return (CharSequence[])items.toArray(arrayOfCharSequence);
  }
}
