package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.activity.HomeActivity;
import com.deviantart.android.damobile.activity.SignUpActivity;
import com.deviantart.android.damobile.view.LoginMainLayout;
import com.deviantart.android.damobile.view.NonSwipeViewPager;

public class LoginDialogFragment
  extends DialogFragment
{
  LoginDialogFragment.LoginDialogListener a;
  LoginDialogFragment.OnCancelListener l;
  @Bind({2131689761})
  View loginFooter;
  private LoginDialogFragment.OnDismissListener mPresenter;
  @Bind({2131689762})
  NonSwipeViewPager pager;
  
  public LoginDialogFragment() {}
  
  public void a(LoginDialogFragment.LoginTab paramLoginTab)
  {
    pager.setCurrentItem(paramLoginTab.a(), false);
  }
  
  public void addTab(boolean paramBoolean)
  {
    if (pager == null) {
      return;
    }
    if (pager.getCurrentItem() == 0) {
      ((LoginMainLayout)pager.findViewById(pager.getCurrentItem())).setVideo(paramBoolean);
    }
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if ((paramInt1 == 104) && ((paramInt2 == -1) || (paramInt2 == 1)))
    {
      dismiss();
      if ((paramInt2 == -1) && (getActivity().getClass() == HomeActivity.class)) {
        getActivity().finish();
      }
    }
  }
  
  public void onAttach(Activity paramActivity)
  {
    super.onAttach(paramActivity);
    try
    {
      LoginDialogFragment.LoginDialogListener localLoginDialogListener = (LoginDialogFragment.LoginDialogListener)paramActivity;
      a = localLoginDialogListener;
      return;
    }
    catch (ClassCastException localClassCastException)
    {
      throw new ClassCastException(paramActivity.toString() + " must implement LoginDialogListener");
    }
  }
  
  public void onClose()
  {
    dismiss();
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setStyle(2, 2131427577);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = paramLayoutInflater.inflate(2130968671, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    pager.setAdapter(new LoginDialogFragment.LoginPagerAdapter(this, null));
    getDialog().setOnKeyListener(new LoginDialogFragment.1(this));
    l = new LoginDialogFragment.2(this);
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public void onDismiss(DialogInterface paramDialogInterface)
  {
    if (mPresenter != null) {
      mPresenter.d();
    }
    BaseActivity localBaseActivity = (BaseActivity)getActivity();
    if (localBaseActivity != null) {
      localBaseActivity.selectItem();
    }
    super.onDismiss(paramDialogInterface);
  }
  
  public void onPageChange(int paramInt)
  {
    if (paramInt == LoginDialogFragment.LoginTab.b.a())
    {
      loginFooter.setVisibility(4);
      return;
    }
    loginFooter.setVisibility(0);
  }
  
  public void onSaveInstanceState(Bundle paramBundle) {}
  
  public void openJoinActivity()
  {
    startActivityForResult(new Intent(getActivity(), SignUpActivity.class), 104);
  }
  
  public void sendMessage(String paramString)
  {
    if (pager == null) {
      return;
    }
    if (pager.getCurrentItem() == 0) {
      ((LoginMainLayout)pager.findViewById(pager.getCurrentItem())).setFrom(paramString);
    }
  }
  
  public LoginDialogFragment setSongs(LoginDialogFragment.OnDismissListener paramOnDismissListener)
  {
    mPresenter = paramOnDismissListener;
    return this;
  }
  
  public void updateTabStyles()
  {
    if (pager == null) {
      return;
    }
    if (pager.getCurrentItem() == 0) {
      ((LoginMainLayout)pager.findViewById(pager.getCurrentItem())).setInformation();
    }
  }
}
