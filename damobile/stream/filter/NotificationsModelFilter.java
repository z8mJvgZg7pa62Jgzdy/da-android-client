package com.deviantart.android.damobile.stream.filter;

import com.deviantart.android.android.package_14.model.DVNTFeedbackMessage;
import com.deviantart.android.damobile.util.notifications.NotificationItemData;
import com.deviantart.android.damobile.util.opt.ItemDeleteHelperBase;
import com.deviantart.android.damobile.util.opt.ItemDeleteType;
import com.deviantart.android.damobile.util.opt.ItemDeleteUtils;
import com.deviantart.android.damobile.util.opt.NotificationItemDeleteHelper;
import java.util.ArrayList;

public class NotificationsModelFilter
  implements StreamFilter<NotificationItemData>
{
  public NotificationsModelFilter() {}
  
  public boolean a(NotificationItemData paramNotificationItemData)
  {
    return (paramNotificationItemData.b() == null) || (paramNotificationItemData.a() == null) || (paramNotificationItemData.a().isOrphaned().booleanValue()) || (paramNotificationItemData.a().getOriginator() == null) || (((NotificationItemDeleteHelper)ItemDeleteUtils.a(ItemDeleteType.b)).i().contains(paramNotificationItemData));
  }
}
