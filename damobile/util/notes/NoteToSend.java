package com.deviantart.android.damobile.util.notes;

import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.util.Recipient;
import com.deviantart.android.damobile.util.Recipient.RecipientStatus;
import com.google.common.base.Objects;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NoteToSend
  implements Serializable
{
  private final List<Recipient> a = new ArrayList();
  private String buffer;
  private String i;
  private DVNTDeviation j;
  private String l;
  
  public NoteToSend() {}
  
  public NoteToSend a(DVNTDeviation paramDVNTDeviation)
  {
    j = paramDVNTDeviation;
    return this;
  }
  
  public NoteToSend a(String paramString)
  {
    l = paramString;
    return this;
  }
  
  public boolean a()
  {
    return new NoteToSend().equals(this);
  }
  
  public void append(String paramString)
  {
    i = paramString;
  }
  
  public boolean b()
  {
    if ((a == null) || (a.isEmpty())) {
      return false;
    }
    Iterator localIterator = a.iterator();
    while (localIterator.hasNext())
    {
      Recipient localRecipient = (Recipient)localIterator.next();
      if (Recipient.RecipientStatus.e.equals(localRecipient.get())) {
        return true;
      }
    }
    return false;
  }
  
  public boolean b(String paramString)
  {
    return open(paramString) != null;
  }
  
  public boolean c()
  {
    if ((a == null) || (a.isEmpty())) {
      return false;
    }
    Iterator localIterator = a.iterator();
    while (localIterator.hasNext())
    {
      Recipient localRecipient = (Recipient)localIterator.next();
      if (Recipient.RecipientStatus.b.equals(localRecipient.get())) {
        return true;
      }
    }
    return false;
  }
  
  public boolean d()
  {
    return b();
  }
  
  public String e()
  {
    Object localObject2 = "";
    Object localObject1 = localObject2;
    if (l != null)
    {
      localObject1 = localObject2;
      if (!l.isEmpty()) {
        localObject1 = "" + l;
      }
    }
    localObject2 = localObject1;
    if (j != null) {
      localObject2 = (String)localObject1 + "</br> <da:deviation id=" + j.getId() + " />";
    }
    return localObject2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    paramObject = (NoteToSend)paramObject;
    return (Objects.append(buffer, buffer)) && (Objects.append(a, a)) && (Objects.append(j, j)) && (Objects.append(l, l)) && (Objects.append(i, i));
  }
  
  public String f()
  {
    return i;
  }
  
  public List getValue()
  {
    return a;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { buffer, a, j, l, i });
  }
  
  public boolean m()
  {
    return j != null;
  }
  
  public Recipient open(String paramString)
  {
    Iterator localIterator = a.iterator();
    while (localIterator.hasNext())
    {
      Recipient localRecipient = (Recipient)localIterator.next();
      if (localRecipient.values().getUserName().equals(paramString)) {
        return localRecipient;
      }
    }
    return null;
  }
  
  public String read()
  {
    if ((buffer == null) || (buffer.isEmpty())) {
      return buffer;
    }
    return buffer.replaceAll("\\n+", " ");
  }
  
  public NoteToSend trim(String paramString)
  {
    buffer = paramString;
    return this;
  }
}
