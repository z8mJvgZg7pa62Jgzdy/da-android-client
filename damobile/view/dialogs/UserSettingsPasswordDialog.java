package com.deviantart.android.damobile.view.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.util.SignUpHelper.ValidateUserInfoTask;

public class UserSettingsPasswordDialog
  extends DABaseDialog
{
  @Bind({2131690112})
  EditText currentPassword;
  private SignUpHelper.ValidateUserInfoTask currentTask;
  @Bind({2131690120})
  EditText newPassword;
  @Bind({2131690119})
  ProgressBar newPasswordProgressBar;
  @Bind({2131690121})
  EditText retypePassword;
  
  public UserSettingsPasswordDialog() {}
  
  private void addTask(String paramString)
  {
    DVNTAsyncAPI.validateAccount(null, paramString, null).call(getActivity(), new UserSettingsPasswordDialog.5(this));
  }
  
  public String b()
  {
    return currentPassword.getText().toString();
  }
  
  public String getText()
  {
    return newPassword.getText().toString();
  }
  
  public Dialog onCreateDialog(Bundle paramBundle)
  {
    paramBundle = new AlertDialog.Builder(getActivity());
    View localView = getActivity().getLayoutInflater().inflate(2130968796, null);
    ButterKnife.bind(this, localView);
    paramBundle.setTitle(2131231270).setView(localView).setPositiveButton(2131231183, new UserSettingsPasswordDialog.2(this)).setNegativeButton(2131230820, new UserSettingsPasswordDialog.1(this));
    paramBundle = paramBundle.create();
    paramBundle.setOnShowListener(new UserSettingsPasswordDialog.3(this));
    return paramBundle;
  }
  
  public void onDestroyView()
  {
    ButterKnife.unbind(this);
    if (currentTask != null) {
      currentTask.cancel(true);
    }
    super.onDestroyView();
  }
  
  public void onPostExecute()
  {
    if ((currentPassword.getText() != null) && (!currentPassword.getText().toString().isEmpty()) && (newPassword.getText() != null) && (!newPassword.getText().toString().isEmpty()) && (newPassword.getError() == null) && (retypePassword.getText() != null) && (!retypePassword.getText().toString().isEmpty()) && (retypePassword.getError() == null)) {}
    for (boolean bool = true;; bool = false)
    {
      ((AlertDialog)getDialog()).getButton(-1).setEnabled(bool);
      return;
    }
  }
  
  public void passwordTextChanged(CharSequence paramCharSequence)
  {
    if (currentTask != null) {
      currentTask.cancel(true);
    }
    if (paramCharSequence.length() == 0)
    {
      newPasswordProgressBar.setVisibility(8);
      onPostExecute();
      return;
    }
    newPasswordProgressBar.setVisibility(0);
    currentTask = new UserSettingsPasswordDialog.4(this);
    currentTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[] { paramCharSequence.toString() });
  }
  
  public void retypePasswordTextChanged(CharSequence paramCharSequence)
  {
    if (paramCharSequence.toString().equals(newPassword.getText().toString())) {
      retypePassword.setError(null);
    }
    for (;;)
    {
      onPostExecute();
      return;
      retypePassword.setError(getString(2131231241));
    }
  }
}
