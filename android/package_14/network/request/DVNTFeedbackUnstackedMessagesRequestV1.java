package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.model.DVNTFeedbackMessages;
import com.deviantart.android.android.package_14.model.DVNTFeedbackType;
import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTFeedbackUnstackedMessagesRequestV1
  extends DVNTAbstractFeedbackUnstackedMessagesRequestV1
{
  private Integer filterVersion;
  
  public DVNTFeedbackUnstackedMessagesRequestV1(DVNTFeedbackType paramDVNTFeedbackType, String paramString, Integer paramInteger1, Integer paramInteger2, Integer paramInteger3)
  {
    super(paramDVNTFeedbackType, paramString, paramInteger1, paramInteger2);
    filterVersion = paramInteger3;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()) && (super.equals(paramObject)))
    {
      paramObject = (DVNTFeedbackUnstackedMessagesRequestV1)paramObject;
      return Objects.append(filterVersion, filterVersion);
    }
    return false;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), filterVersion });
  }
  
  protected DVNTFeedbackMessages sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).getUnstackedFeedbackMessages(paramString, type, messageFolderId, Boolean.valueOf(false), offset, limit, filterVersion, Boolean.valueOf(true));
  }
}
