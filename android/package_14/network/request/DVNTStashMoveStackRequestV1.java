package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTStashMoveStackRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTStashMoveStackResponse>
{
  private Long movedStashid;
  private Long targetStashId;
  
  public DVNTStashMoveStackRequestV1(Long paramLong1, Long paramLong2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTStashMoveStackResponse.class);
    movedStashid = paramLong1;
    targetStashId = paramLong2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTStashMoveStackRequestV1)paramObject;
    return (Objects.append(movedStashid, movedStashid)) && (Objects.append(targetStashId, targetStashId));
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), movedStashid, targetStashId });
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTStashMoveStackResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).stashMoveStack(paramString, movedStashid, targetStashId);
  }
}
