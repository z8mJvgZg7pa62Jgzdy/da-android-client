package com.deviantart.android.damobile.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.deviantart.android.android.utils.DVNTUtils;

public class DeviceIdUtil
{
  public DeviceIdUtil() {}
  
  public static String getDeviceId(Context paramContext)
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramContext);
    String str2 = localSharedPreferences.getString("device_id", null);
    String str1 = str2;
    if (TextUtils.isEmpty(str2))
    {
      str1 = DVNTUtils.generateSecureAndroidHash(paramContext);
      localSharedPreferences.edit().putString("device_id", str1).apply();
    }
    return str1;
  }
}
