package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.model.DVNTCategory.List;
import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.deviantart.android.android.package_14.network.wrapper.DVNTCategoriesWrapper;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTCategoryTreeRequestV1
  extends DVNTAbstractCategoryTreeRequestV1
{
  Boolean filtered;
  
  public DVNTCategoryTreeRequestV1(String paramString, Boolean paramBoolean)
  {
    super(paramString);
    filtered = paramBoolean;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTCategoryTreeRequestV1 localDVNTCategoryTreeRequestV1 = (DVNTCategoryTreeRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(filtered, filtered).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return super.getCacheKey() + filtered;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(filtered).toHashCode();
  }
  
  protected DVNTCategory.List sendRequest(String paramString)
  {
    return (DVNTCategory.List)((DVNTServiceV1Flavored)getService()).getCategoryTree(paramString, catPath, filtered).getCategories();
  }
}
