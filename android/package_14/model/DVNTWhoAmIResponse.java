package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;

public class DVNTWhoAmIResponse
  extends DVNTWhoIsResponse
{
  @SerializedName("email")
  private String email;
  @SerializedName("is_minor")
  private boolean isMinor;
  @SerializedName("newsletter")
  private boolean isNewsletterRecipient;
  @SerializedName("verified")
  private boolean verified;
  @SerializedName("mature_content")
  private boolean viewMatureContent;
  
  public DVNTWhoAmIResponse() {}
  
  public String getEmail()
  {
    return email;
  }
  
  public boolean isMinor()
  {
    return isMinor;
  }
  
  public boolean isNewsletterRecipient()
  {
    return isNewsletterRecipient;
  }
  
  public boolean isVerified()
  {
    return verified;
  }
  
  public boolean isViewMatureContent()
  {
    return viewMatureContent;
  }
  
  public void setEmail(String paramString)
  {
    email = paramString;
  }
  
  public void setMinor(boolean paramBoolean)
  {
    isMinor = paramBoolean;
  }
  
  public void setNewsletterRecipient(boolean paramBoolean)
  {
    isNewsletterRecipient = paramBoolean;
  }
  
  public void setVerified(boolean paramBoolean)
  {
    verified = paramBoolean;
  }
  
  public void setViewMatureContent(boolean paramBoolean)
  {
    viewMatureContent = paramBoolean;
  }
}
