package com.deviantart.android.damobile.view;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import com.deviantart.android.android.package_14.model.DVNTComment;
import com.deviantart.android.damobile.adapter.recyclerview.BaseCommentAdapter;
import com.deviantart.android.damobile.adapter.recyclerview.ContextualCommentAdapter.Builder;
import com.deviantart.android.damobile.adapter.recyclerview.RegularCommentAdapter.Builder;
import com.deviantart.android.damobile.stream.loader.APICommentContextLoader;
import com.deviantart.android.damobile.stream.loader.APICommentsLoader;
import com.deviantart.android.damobile.util.CommentType;
import com.deviantart.android.damobile.util.Graphics;

public class CommentsLayout
  extends DAStateRecyclerView
  implements NewParentCommentListener
{
  private View.OnClickListener a;
  View c;
  String g;
  BaseCommentAdapter l;
  
  private CommentsLayout(Context paramContext)
  {
    super(paramContext);
  }
  
  private void onCreateView(Context paramContext, CommentType paramCommentType, String paramString1, String paramString2, String paramString3, View paramView, boolean paramBoolean)
  {
    if (paramCommentType != CommentType.g) {
      b(false);
    }
    int i;
    label106:
    int j;
    if (g != null)
    {
      i = 1;
      ViewState.Helper.a(b, paramString3, paramContext.getString(2131230881), paramContext.getString(2131230879));
      if (i == 0) {
        break label290;
      }
      l = new ContextualCommentAdapter.Builder().a(new APICommentContextLoader(paramString2)).b(this).c(g).a(this).b(false).a(paramCommentType).a(paramString1).b(paramString3).c();
      setAdapter(l);
      j = getResources().getColor(2131558417);
      if (paramView != null)
      {
        j = 0;
        a(paramView);
      }
      setBackgroundColor(j);
      if (paramBoolean)
      {
        paramString2 = new View(paramContext);
        paramString2.setLayoutParams(new ViewGroup.LayoutParams(-1, paramContext.getResources().getDimensionPixelOffset(2131361936)));
        b(paramString2);
      }
      if (i == 0) {
        break label333;
      }
      paramString2 = LayoutInflater.from(getContext()).inflate(2130968697, this, false);
      View localView = paramString2.findViewById(2131689918);
      c = paramString2.findViewById(2131689917);
      localView.setOnClickListener(CommentsLayout..Lambda.1.lambdaFactory$(this, paramContext, paramCommentType, paramString1, paramString3, paramView, paramBoolean));
      c.setOnClickListener(CommentsLayout..Lambda.2.lambdaFactory$(this, paramContext, paramCommentType, paramString1, paramString3, paramView, paramBoolean));
      a(paramString2);
    }
    for (;;)
    {
      if (i == 0) {
        break label408;
      }
      setOnRefreshListener(null);
      return;
      i = 0;
      break;
      label290:
      l = new RegularCommentAdapter.Builder().a(new APICommentsLoader(paramCommentType, paramString1, null)).b(paramCommentType).a(paramString1).b(paramString3).c();
      break label106;
      label333:
      paramString2 = LayoutInflater.from(paramContext).inflate(2130968617, this, false);
      if (paramView != null)
      {
        j = Graphics.add(paramContext, 15);
        int k = Graphics.add(paramContext, 20);
        paramString2.setPadding(j, k, j, k);
      }
      ((Button)paramString2.findViewById(2131689620)).setOnClickListener(CommentsLayout..Lambda.3.finish(paramCommentType, paramString1));
      a(paramString2);
    }
    label408:
    setEndlessScrollListener(getResources().getInteger(2131492877));
    onViewCreated();
  }
  
  public void b(DVNTComment paramDVNTComment)
  {
    if (l == null) {
      return;
    }
    l.a(getContext(), paramDVNTComment);
  }
  
  public void c(DVNTComment paramDVNTComment)
  {
    if (c != null)
    {
      if ((paramDVNTComment != null) && (paramDVNTComment.getParentId() != null))
      {
        c.setVisibility(0);
        return;
      }
      c.setVisibility(4);
    }
  }
}
