package com.deviantart.android.damobile.adapter.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.DailyDeviationItem;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.DeviationUtils.ProcessMenuAddable;
import com.deviantart.android.damobile.util.TodayDailyDeviationItem;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.view.thirdparty.ByteVector;
import com.deviantart.android.damobile.view.thirdparty.GomFactory;
import com.deviantart.android.damobile.view.thirdparty.GomType;
import com.deviantart.android.damobile.view.thirdparty.decorator.DecoratorGomHelper;
import com.deviantart.android.damobile.view.thirdparty.decorator.DecoratorType;
import com.deviantart.android.damobile.view.thirdparty.decorator.DecoratorViewHolder;
import com.deviantart.android.damobile.view.thirdparty.deviation.DeviationGom;
import com.deviantart.android.damobile.view.thirdparty.preview.DeviationPreviewGom;

public class DailyDeviationAdapter
  extends DAStateRecyclerViewAdapter<DailyDeviationItem>
  implements DeviationUtils.ProcessMenuAddable
{
  private DecoratorGomHelper o = new DecoratorGomHelper(new DecoratorType[] { DecoratorType.c, DecoratorType.i });
  
  public DailyDeviationAdapter(Stream paramStream)
  {
    super(paramStream);
  }
  
  protected int b(int paramInt)
  {
    return GomFactory.b((DailyDeviationItem)get().get(paramInt)).b().ordinal();
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    Object localObject = GomType.b(paramInt);
    if ((localObject instanceof DeviationPreviewGom)) {
      return ((ByteVector)localObject).b(paramViewGroup.getContext(), paramViewGroup);
    }
    localObject = ((ByteVector)localObject).b(paramViewGroup.getContext(), paramViewGroup);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
    return o.a(paramViewGroup.getContext(), paramViewGroup, (RecyclerView.ViewHolder)localObject);
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    Context localContext = itemView.getContext();
    Object localObject1 = (DailyDeviationItem)get().get(paramInt);
    Object localObject2 = GomFactory.b((DailyDeviationItem)localObject1);
    if ((localObject2 instanceof DeviationPreviewGom))
    {
      ((ByteVector)localObject2).b(localContext, localObject1, paramViewHolder);
      return;
    }
    if (((paramViewHolder instanceof DecoratorViewHolder)) && ((localObject1 instanceof TodayDailyDeviationItem)))
    {
      localObject1 = (TodayDailyDeviationItem)localObject1;
      paramViewHolder = (DecoratorViewHolder)paramViewHolder;
      localObject2 = ((DeviationDescription)localObject1).a();
      if (localObject2 != null)
      {
        localObject2 = GomFactory.a((DVNTDeviation)localObject2);
        ((DeviationGom)localObject2).b(0, 0);
        o.a(localContext, paramViewHolder, localObject1);
        ((DeviationGom)localObject2).b(DeviationUtils.b(this));
        ((DeviationGom)localObject2).a(localContext, (DeviationDescription)localObject1, DecoratorGomHelper.b(paramViewHolder));
      }
    }
  }
}
