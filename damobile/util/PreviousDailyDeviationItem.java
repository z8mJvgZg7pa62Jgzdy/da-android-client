package com.deviantart.android.damobile.util;

import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.sdk.api.model.DVNTDeviation;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PreviousDailyDeviationItem
  extends DefaultDeviationTorpedoPreviewItem
  implements DailyDeviationItem
{
  private Date d;
  private StreamLoader<DVNTDeviation> i;
  
  public PreviousDailyDeviationItem(Stream paramStream)
  {
    i = paramStream.length();
    d = DAFormatUtils.get(paramStream);
  }
  
  public String b()
  {
    return DAFormatUtils.DateFormats.A.format(d);
  }
  
  public Stream c()
  {
    return StreamCacher.a(i, StreamCacheStrategy.d);
  }
  
  public String format()
  {
    return DAFormatUtils.DateFormats.d.format(d);
  }
  
  public String getPropertyName()
  {
    return "previous_daily_deviations";
  }
  
  public DailyDeviationItem.ItemType getState()
  {
    return DailyDeviationItem.ItemType.STATE_DELETED;
  }
}
