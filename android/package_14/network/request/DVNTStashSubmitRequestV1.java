package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.io.File;
import java.util.ArrayList;
import retrofit.mime.TypedFile;

public class DVNTStashSubmitRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTStashSubmitResponse>
{
  private final String description;
  private final File file1;
  private File file2;
  private final String folderName;
  private final String mimeType1;
  private String mimeType2;
  private final ArrayList<String> tags;
  private final String title;
  
  public DVNTStashSubmitRequestV1(String paramString1, String paramString2, String paramString3, ArrayList paramArrayList, File paramFile, String paramString4)
  {
    super(com.deviantart.android.sdk.api.model.DVNTStashSubmitResponse.class);
    title = paramString1;
    description = paramString2;
    mimeType1 = paramString4;
    file1 = paramFile;
    folderName = paramString3;
    tags = paramArrayList;
  }
  
  public DVNTStashSubmitRequestV1(String paramString1, String paramString2, String paramString3, ArrayList paramArrayList, File paramFile1, String paramString4, File paramFile2, String paramString5)
  {
    super(com.deviantart.android.sdk.api.model.DVNTStashSubmitResponse.class);
    tags = paramArrayList;
    folderName = paramString3;
    file1 = paramFile1;
    file2 = paramFile2;
    mimeType1 = paramString4;
    mimeType2 = paramString5;
    title = paramString1;
    description = paramString2;
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTStashSubmitResponse sendRequest(String paramString)
  {
    TypedFile localTypedFile = new TypedFile(mimeType1, file1);
    Object localObject2 = null;
    Object localObject1 = localObject2;
    if (mimeType2 != null)
    {
      localObject1 = localObject2;
      if (file2 != null) {
        localObject1 = new TypedFile(mimeType2, file2);
      }
    }
    return ((DVNTServiceV1)getService()).stashSubmit(paramString, title, description, folderName, tags, localTypedFile, (TypedFile)localObject1);
  }
}
