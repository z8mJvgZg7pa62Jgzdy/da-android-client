package com.deviantart.android.damobile.view.deviationtab;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class CommentsDisabledLayout
  extends FrameLayout
{
  public CommentsDisabledLayout(Context paramContext, ViewGroup paramViewGroup)
  {
    super(paramContext);
    addView(LayoutInflater.from(paramContext).inflate(2130968643, paramViewGroup, false));
  }
}
