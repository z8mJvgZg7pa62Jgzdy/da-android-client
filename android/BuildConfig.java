package com.deviantart.android.android;

public final class BuildConfig
{
  public static final Integer API_VERSION = Integer.valueOf(20151202);
  public static final String APPLICATION_ID = "com.deviantart.android.sdk";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "internalFlavor";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "-unspecified-internal";
  
  public BuildConfig() {}
}
