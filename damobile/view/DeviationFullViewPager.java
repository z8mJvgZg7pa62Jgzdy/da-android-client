package com.deviantart.android.damobile.view;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.adapter.DeviationFullViewPagerAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.Stream.Notifiable;
import com.deviantart.android.damobile.stream.loader.StreamLoader.ErrorType;
import com.deviantart.android.damobile.util.DeviationAdController;
import com.deviantart.android.damobile.util.ZoomOutPageTransformer;
import com.deviantart.datoolkit.logger.DVNTLog;

public class DeviationFullViewPager
  extends BoundsListeningViewPager
  implements Stream.Notifiable
{
  private String a;
  private int g = -1;
  private GestureDetector gestureDetector;
  private boolean k = false;
  private DeviationFullViewPager.DeviationFullViewPagerListener l;
  private DeviationAdController o;
  
  public DeviationFullViewPager(Context paramContext)
  {
    super(paramContext);
    init();
  }
  
  public DeviationFullViewPager(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init();
  }
  
  private void init()
  {
    setPageTransformer(true, new ZoomOutPageTransformer());
    setOffscreenPageLimit(2);
    setThreshold(getResources().getInteger(2131492884));
    addOnPageChangeListener(new DeviationFullViewPager.1(this));
  }
  
  public void a(int paramInt, DVNTDeviation paramDVNTDeviation)
  {
    DVNTDeviation localDVNTDeviation2 = null;
    DeviationFullViewPagerAdapter localDeviationFullViewPagerAdapter = getAdapter();
    if (o != null)
    {
      if (localDeviationFullViewPagerAdapter == null) {
        return;
      }
      DVNTDeviation localDVNTDeviation1;
      if (paramInt > 0)
      {
        localDVNTDeviation1 = localDeviationFullViewPagerAdapter.get(paramInt - 1);
        if (paramInt + 1 < localDeviationFullViewPagerAdapter.remove().size()) {
          break label74;
        }
      }
      for (;;)
      {
        o.b(paramDVNTDeviation, localDVNTDeviation1, localDVNTDeviation2);
        o.b(true);
        return;
        localDVNTDeviation1 = null;
        break;
        label74:
        localDVNTDeviation2 = localDeviationFullViewPagerAdapter.get(paramInt + 1);
      }
    }
  }
  
  public void b()
  {
    getAdapter().getItem();
  }
  
  public void b(StreamLoader.ErrorType paramErrorType, String paramString)
  {
    DVNTLog.append(paramString, new Object[0]);
    if (DVNTContextUtils.isContextDead(getContext())) {
      return;
    }
    Toast.makeText(getContext(), getContext().getString(2131230930), 0).show();
    setLoading(false);
  }
  
  public void c()
  {
    getAdapter().getItem();
    if (!k)
    {
      i = getAdapter().get(a);
      if (i == -1) {
        return;
      }
      getAdapter().add(i);
      setCurrentItem(i, false);
      return;
    }
    if (g == 0) {}
    for (int i = getAdapter().get(a) - 1; i != -1; i = g)
    {
      DVNTDeviation localDVNTDeviation = getAdapter().get(i);
      if ((localDVNTDeviation == null) || (l == null)) {
        break;
      }
      getAdapter().add(i);
      setCurrentItem(i);
      l.a(i, localDVNTDeviation);
      g = -1;
      k = false;
      return;
    }
  }
  
  public void d()
  {
    o.a();
    o = null;
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    GestureDetector localGestureDetector;
    if (gestureDetector != null) {
      localGestureDetector = gestureDetector;
    }
    try
    {
      localGestureDetector.onTouchEvent(paramMotionEvent);
      boolean bool = super.dispatchTouchEvent(paramMotionEvent);
      return bool;
    }
    catch (IllegalArgumentException paramMotionEvent)
    {
      paramMotionEvent.printStackTrace();
    }
    return false;
  }
  
  public void e() {}
  
  public void f()
  {
    setLoading(false);
  }
  
  public boolean flagActionItems()
  {
    if (!getAdapter().get()) {
      return false;
    }
    getAdapter().add(getContext(), this, true);
    return true;
  }
  
  public DeviationFullViewPagerAdapter getAdapter()
  {
    return (DeviationFullViewPagerAdapter)super.getAdapter();
  }
  
  public boolean load()
  {
    if (!getAdapter().add()) {
      return false;
    }
    getAdapter().read(getContext(), this);
    return true;
  }
  
  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    try
    {
      boolean bool = super.onInterceptTouchEvent(paramMotionEvent);
      return bool;
    }
    catch (IllegalArgumentException paramMotionEvent)
    {
      paramMotionEvent.printStackTrace();
    }
    return false;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    try
    {
      boolean bool = super.onTouchEvent(paramMotionEvent);
      return bool;
    }
    catch (IllegalArgumentException paramMotionEvent)
    {
      paramMotionEvent.printStackTrace();
    }
    return false;
  }
  
  public void refreshView()
  {
    int i = getCurrentItem();
    setAdapter(new DeviationFullViewPagerAdapter(getAdapter().remove()));
    getAdapter().notifyDataSetChanged();
    setCurrentItem(i);
  }
  
  public void setAdController(DeviationAdController paramDeviationAdController)
  {
    o = paramDeviationAdController;
  }
  
  public void setCurrentItem(int paramInt)
  {
    int i = paramInt;
    if (getAdapter().get()) {
      i = paramInt + 1;
    }
    super.setCurrentItem(i);
  }
  
  public void setCurrentItem(int paramInt, boolean paramBoolean)
  {
    int i = paramInt;
    if (getAdapter().get()) {
      i = paramInt + 1;
    }
    super.setCurrentItem(i, paramBoolean);
  }
  
  public void setFullViewPagerListener(DeviationFullViewPager.DeviationFullViewPagerListener paramDeviationFullViewPagerListener)
  {
    l = paramDeviationFullViewPagerListener;
  }
  
  public void setGestureDetector(GestureDetector paramGestureDetector)
  {
    gestureDetector = paramGestureDetector;
  }
}
