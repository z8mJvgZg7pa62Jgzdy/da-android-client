package com.deviantart.android.android.package_14.model;

public class DVNTGallery
  extends DVNTGallection
{
  String parent;
  
  public DVNTGallery() {}
  
  public String getParent()
  {
    return parent;
  }
  
  public void setParent(String paramString)
  {
    parent = paramString;
  }
}
