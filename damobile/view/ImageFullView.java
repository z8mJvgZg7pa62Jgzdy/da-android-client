package com.deviantart.android.damobile.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView.OnImageEventListener;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.util.FileCache;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.view.image.SubsamplingImageView;
import com.deviantart.android.damobile.view.image.ZoomableDraweeView;
import com.deviantart.android.damobile.view.image.ZoomableDraweeView.DataSourceReadyListener;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.AbstractDraweeControllerBuilder;
import com.facebook.drawee.drawable.ScalingUtils.ScaleType;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.DraweeView;
import com.facebook.drawee.view.GenericDraweeView;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.memory.PooledByteBuffer;
import com.facebook.imagepipeline.memory.PooledByteBufferInputStream;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;

public class ImageFullView
  extends FrameLayout
  implements SubsamplingScaleImageView.OnImageEventListener, ZoomableDraweeView.DataSourceReadyListener
{
  private DataSource<CloseableReference<PooledByteBuffer>> c;
  private boolean found = false;
  private boolean i = false;
  @Bind({2131689720})
  ZoomableDraweeView imageView;
  private CloseableReference<PooledByteBuffer> j;
  @Bind({2131689722})
  ImageView powerZoom;
  @Bind({2131689721})
  SubsamplingImageView subsamplingImageView;
  private String x;
  
  public ImageFullView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public ImageFullView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ImageFullView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    LayoutInflater.from(paramContext).inflate(2130968659, this, true);
    ButterKnife.bind(this);
    paramContext = new GenericDraweeHierarchyBuilder(getContext().getResources()).a(100).a(new ColorDrawable(paramContext.getResources().getColor(2131558499))).a(ScalingUtils.ScaleType.g).c();
    imageView.setHierarchy(paramContext);
    subsamplingImageView.setOnImageEventListener(this);
    subsamplingImageView.setMaxScale(10.0F);
  }
  
  public void a()
  {
    if (DVNTContextUtils.isContextDead(getContext())) {
      return;
    }
    String str = String.valueOf(x.hashCode());
    try
    {
      j = ((CloseableReference)c.get());
      Object localObject = j;
      File localFile;
      if (localObject != null)
      {
        localObject = new BufferedInputStream(new PooledByteBufferInputStream((PooledByteBuffer)j.get()));
        localFile = new File(FileCache.create(getContext()), str);
      }
      try
      {
        boolean bool = localFile.createNewFile();
        if (bool) {
          IOUtils.copy((InputStream)localObject, new FileOutputStream(localFile));
        }
      }
      catch (IOException localIOException)
      {
        for (;;)
        {
          localIOException.printStackTrace();
        }
      }
      c.close();
      CloseableReference.append(j);
      subsamplingImageView.setVisibility(0);
      subsamplingImageView.setImage(ImageSource.uri(FileCache.create(getContext()).getAbsolutePath() + "/" + str));
      return;
    }
    catch (Throwable localThrowable)
    {
      c.close();
      CloseableReference.append(j);
      throw localThrowable;
    }
  }
  
  public void a(DVNTImage paramDVNTImage1, DVNTImage paramDVNTImage2, boolean paramBoolean1, boolean paramBoolean2)
  {
    if ((paramBoolean1) && (!UserUtils.get(getContext())))
    {
      imageView.setAspectRatio(1.0F);
      ImageUtils.a(imageView, 2130837731);
      imageView.setMaxZoom(1.0F);
      return;
    }
    if (((paramDVNTImage1.getWidth() > 1500) || (paramDVNTImage1.getHeight() > 1500)) && (!Graphics.setIcon(paramDVNTImage1.getSrc()))) {}
    for (paramBoolean1 = true;; paramBoolean1 = false)
    {
      i = paramBoolean1;
      imageView.setUseSubsampling(i);
      imageView.setAspectRatio(paramDVNTImage1.getWidth() / paramDVNTImage1.getHeight());
      x = paramDVNTImage1.getSrc();
      ImageRequest localImageRequest = ImageRequestBuilder.write(Uri.parse(x)).c(true).a();
      paramDVNTImage1 = null;
      if (paramDVNTImage2 != null) {
        paramDVNTImage1 = ImageRequestBuilder.write(Uri.parse(paramDVNTImage2.getSrc())).a();
      }
      b(localImageRequest, paramDVNTImage1, paramBoolean2);
      return;
    }
  }
  
  public void b()
  {
    imageView.setVisibility(8);
  }
  
  public void b(Uri paramUri, boolean paramBoolean)
  {
    b(ImageRequestBuilder.write(paramUri).c(true).a(), null, paramBoolean);
  }
  
  public void b(ImageRequest paramImageRequest1, ImageRequest paramImageRequest2, boolean paramBoolean)
  {
    if (paramBoolean) {}
    PipelineDraweeControllerBuilder localPipelineDraweeControllerBuilder = (PipelineDraweeControllerBuilder)((PipelineDraweeControllerBuilder)((PipelineDraweeControllerBuilder)Fresco.b().a(imageView.getController())).a(true)).a(paramImageRequest1);
    if (paramImageRequest2 != null) {
      localPipelineDraweeControllerBuilder.b(paramImageRequest2);
    }
    if (i)
    {
      c = Fresco.putShort().c(paramImageRequest1, this);
      imageView.setDataSourceReadyListener(this);
    }
    imageView.setController(localPipelineDraweeControllerBuilder.visitAnnotation());
  }
  
  public void b(Exception paramException) {}
  
  public void c(Exception paramException) {}
  
  public void i(Exception paramException) {}
  
  public void setPowerZoomSrc(String paramString)
  {
    x = paramString;
  }
  
  public void update()
  {
    float f1 = subsamplingImageView.getScale();
    float f2 = imageView.getScaleFactor();
    PointF localPointF = imageView.getCenter();
    x *= subsamplingImageView.getSWidth();
    y *= subsamplingImageView.getSHeight();
    subsamplingImageView.setImage();
    subsamplingImageView.setScaleAndCenter(f1 * f2, localPointF);
  }
}
