package com.deviantart.android.damobile.util;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.Behavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.CoordinatorLayout.Behavior;
import android.support.design.widget.HeaderBehavior;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class ProfileCardBehavior
  extends AppBarLayout.Behavior
{
  private static int b = -1;
  private float a = -1.0F;
  private boolean isOnPause;
  
  public ProfileCardBehavior()
  {
    fling();
  }
  
  public ProfileCardBehavior(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    fling();
  }
  
  private boolean onLayoutChild(AppBarLayout paramAppBarLayout)
  {
    return Math.abs(getTopAndBottomOffset()) >= paramAppBarLayout.getTotalScrollRange();
  }
  
  protected void fling()
  {
    fling(new ProfileCardBehavior.1(this));
  }
  
  public boolean onInterceptTouchEvent(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, MotionEvent paramMotionEvent)
  {
    if (b == -1) {
      b = Graphics.add(paramCoordinatorLayout.getContext(), 40);
    }
    switch (MotionEventCompat.getActionMasked(paramMotionEvent))
    {
    default: 
      break;
    }
    while ((paramMotionEvent.getY() > a - b) && (paramMotionEvent.getY() < a))
    {
      return false;
      a = paramMotionEvent.getY();
      continue;
      a = -1.0F;
    }
    return super.onInterceptTouchEvent(paramCoordinatorLayout, paramAppBarLayout, paramMotionEvent);
  }
  
  public boolean onNestedFling(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView, float paramFloat1, float paramFloat2, boolean paramBoolean)
  {
    if (((paramFloat2 > 0.0F) && (!isOnPause)) || ((paramFloat2 < 0.0F) && (isOnPause))) {
      paramFloat2 *= -1.0F;
    }
    for (;;)
    {
      if ((paramFloat2 < 0.0F) && (onLayoutChild(paramAppBarLayout))) {
        return false;
      }
      return super.onNestedFling(paramCoordinatorLayout, paramAppBarLayout, paramView, paramFloat1, paramFloat2, paramBoolean);
    }
  }
  
  public void onNestedPreScroll(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView, int paramInt1, int paramInt2, int[] paramArrayOfInt)
  {
    super.onNestedPreScroll(paramCoordinatorLayout, paramAppBarLayout, paramView, paramInt1, paramInt2, paramArrayOfInt);
    if (paramInt2 > 0) {}
    for (boolean bool = true;; bool = false)
    {
      isOnPause = bool;
      return;
    }
  }
  
  public void onNestedScroll(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (onLayoutChild(paramAppBarLayout)) {
      return;
    }
    super.onNestedScroll(paramCoordinatorLayout, paramAppBarLayout, paramView, paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public boolean onStartNestedScroll(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView1, View paramView2, int paramInt)
  {
    if (onLayoutChild(paramAppBarLayout)) {
      return false;
    }
    return super.onStartNestedScroll(paramCoordinatorLayout, paramAppBarLayout, paramView1, paramView2, paramInt);
  }
  
  public boolean onTouchEvent(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, View paramView, float paramFloat1, float paramFloat2)
  {
    if (((paramFloat2 > 0.0F) && (!isOnPause)) || ((paramFloat2 < 0.0F) && (isOnPause))) {
      paramFloat2 *= -1.0F;
    }
    for (;;)
    {
      if (paramFloat2 < 0.0F) {
        return super.onNestedPreFling(paramCoordinatorLayout, paramAppBarLayout, paramView, paramFloat1, paramFloat2);
      }
      if (onLayoutChild(paramAppBarLayout)) {
        return super.onNestedPreFling(paramCoordinatorLayout, paramAppBarLayout, paramView, paramFloat1, paramFloat2);
      }
      super.onNestedFling(paramCoordinatorLayout, paramAppBarLayout, paramView, paramFloat1, paramFloat2, false);
      return true;
    }
  }
  
  public void scroll(CoordinatorLayout paramCoordinatorLayout, AppBarLayout paramAppBarLayout, float paramFloat)
  {
    super.onNestedFling(paramCoordinatorLayout, paramAppBarLayout, null, 0.0F, paramFloat, false);
  }
}
