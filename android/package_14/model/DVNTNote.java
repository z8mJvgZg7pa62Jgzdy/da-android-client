package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTNote
  implements Serializable
{
  @SerializedName("body")
  DVNTDAMLComponent.List body;
  @SerializedName("sent")
  Boolean isSent;
  @SerializedName("starred")
  Boolean isStarred;
  @SerializedName("unread")
  Boolean isUnread;
  @SerializedName("noteid")
  String noteId;
  @SerializedName("preview")
  String preview;
  @SerializedName("recipients")
  DVNTUser.List recipients;
  @SerializedName("subject")
  String subject;
  @SerializedName("ts")
  String timeStamp;
  @SerializedName("user")
  DVNTUser user;
  
  public DVNTNote() {}
  
  public DVNTDAMLComponent.List getBody()
  {
    return body;
  }
  
  public Boolean getIsSent()
  {
    return isSent;
  }
  
  public Boolean getIsStarred()
  {
    return isStarred;
  }
  
  public Boolean getIsUnread()
  {
    return isUnread;
  }
  
  public String getNoteId()
  {
    return noteId;
  }
  
  public String getPreview()
  {
    return preview;
  }
  
  public DVNTUser.List getRecipients()
  {
    return recipients;
  }
  
  public String getSubject()
  {
    return subject;
  }
  
  public String getTimeStamp()
  {
    return timeStamp;
  }
  
  public DVNTUser getUser()
  {
    return user;
  }
  
  public DVNTNote setBody(DVNTDAMLComponent.List paramList)
  {
    body = paramList;
    return this;
  }
  
  public DVNTNote setIsSent(Boolean paramBoolean)
  {
    isSent = paramBoolean;
    return this;
  }
  
  public DVNTNote setIsStarred(Boolean paramBoolean)
  {
    isStarred = paramBoolean;
    return this;
  }
  
  public DVNTNote setIsUnread(Boolean paramBoolean)
  {
    isUnread = paramBoolean;
    return this;
  }
  
  public DVNTNote setNoteId(String paramString)
  {
    noteId = paramString;
    return this;
  }
  
  public DVNTNote setPreview(String paramString)
  {
    preview = paramString;
    return this;
  }
  
  public DVNTNote setRecipients(DVNTUser.List paramList)
  {
    recipients = paramList;
    return this;
  }
  
  public DVNTNote setSubject(String paramString)
  {
    subject = paramString;
    return this;
  }
  
  public DVNTNote setTimeStamp(String paramString)
  {
    timeStamp = paramString;
    return this;
  }
  
  public DVNTNote setUser(DVNTUser paramDVNTUser)
  {
    user = paramDVNTUser;
    return this;
  }
}
