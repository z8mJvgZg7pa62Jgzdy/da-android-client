package com.deviantart.android.android.package_14.config;

import android.content.Context;

public abstract interface DVNTAPIModule
{
  public abstract void init(Context paramContext, DVNTAPIConfig paramDVNTAPIConfig);
}
