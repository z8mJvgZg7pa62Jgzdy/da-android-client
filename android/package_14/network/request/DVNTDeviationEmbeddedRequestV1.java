package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTDeviationEmbeddedRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse>
{
  final String containerId;
  final Integer limit;
  final Integer offset;
  final String offsetDeviationId;
  
  public DVNTDeviationEmbeddedRequestV1(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse.class);
    containerId = paramString1;
    offsetDeviationId = paramString2;
    offset = paramInteger1;
    limit = paramInteger2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTDeviationEmbeddedRequestV1 localDVNTDeviationEmbeddedRequestV1 = (DVNTDeviationEmbeddedRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(containerId, containerId).append(offsetDeviationId, offsetDeviationId).append(offset, offset).append(limit, limit).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "deviation_embedded" + containerId + offsetDeviationId + offset + limit;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 600000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(containerId).append(offsetDeviationId).append(offset).append(limit).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTPaginatedResultResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).deviationEmbedded(paramString, containerId, offsetDeviationId, offset, limit);
  }
}
