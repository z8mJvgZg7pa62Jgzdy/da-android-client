package com.deviantart.android.damobile.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.Stream.Notifiable;
import com.deviantart.android.damobile.view.ewok.DeviationEwok;
import com.deviantart.android.damobile.view.ewok.EwokFactory;

public class DeviationFullViewPagerAdapter
  extends PagerAdapter
{
  private final Stream<com.deviantart.android.sdk.api.model.DVNTDeviation> a;
  private boolean d = false;
  private int n = 0;
  private boolean z = false;
  
  public DeviationFullViewPagerAdapter(Stream paramStream)
  {
    a = paramStream;
    a();
    notifyDataSetChanged();
  }
  
  private void a()
  {
    if (!a.read()) {
      z = true;
    }
    if (!a.iterator()) {
      d = true;
    }
  }
  
  public void add(int paramInt)
  {
    if (a == null) {
      return;
    }
    a.a(Math.min(paramInt, a.size() - 1));
  }
  
  public void add(Context paramContext, Stream.Notifiable paramNotifiable, boolean paramBoolean)
  {
    a.a(paramContext, paramNotifiable, false, paramBoolean);
  }
  
  public boolean add()
  {
    return (a.iterator()) && (!d);
  }
  
  public View b(ViewGroup paramViewGroup, int paramInt)
  {
    Object localObject2 = paramViewGroup.getContext();
    Object localObject1;
    if (write(paramInt))
    {
      localObject1 = LayoutInflater.from((Context)localObject2).inflate(2130968731, paramViewGroup, false);
      paramViewGroup.addView((View)localObject1);
      return localObject1;
    }
    if (a != null)
    {
      paramInt = Math.min(d(paramInt), a.size() - 1);
      localObject1 = (com.deviantart.android.android.package_14.model.DVNTDeviation)a.get(paramInt);
      if (localObject1 != null)
      {
        localObject2 = EwokFactory.b((com.deviantart.android.android.package_14.model.DVNTDeviation)localObject1).b((Context)localObject2);
        if (((View)localObject2).getParent() != null) {
          ((ViewGroup)((View)localObject2).getParent()).removeView((View)localObject2);
        }
        paramViewGroup.addView((View)localObject2);
        ((View)localObject2).setTag(((DVNTAbstractDeviation)localObject1).getId());
        return localObject2;
      }
    }
    return paramViewGroup;
  }
  
  public int d(int paramInt)
  {
    if (write(paramInt)) {
      return -1;
    }
    int i = paramInt;
    if (get()) {
      i = paramInt - 1;
    }
    return i;
  }
  
  public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    paramViewGroup.removeView((View)paramObject);
  }
  
  public int get(String paramString)
  {
    if (paramString == null) {
      return -1;
    }
    int i = 0;
    while (i < remove().size())
    {
      if (paramString.equals(get(i).getId())) {
        return i;
      }
      i += 1;
    }
    return -1;
    return i;
  }
  
  public com.deviantart.android.android.package_14.model.DVNTDeviation get(int paramInt)
  {
    if (a == null) {
      return null;
    }
    return (com.deviantart.android.android.package_14.model.DVNTDeviation)a.get(paramInt);
  }
  
  public boolean get()
  {
    return (a.read()) && (!z);
  }
  
  public int getCount()
  {
    if ((a == null) || (n == 0)) {
      return 0;
    }
    int j = n;
    int i = j;
    if (get()) {
      i = j + 1;
    }
    j = i;
    if (add()) {
      j = i + 1;
    }
    return j;
  }
  
  public void getItem()
  {
    a();
    notifyDataSetChanged();
  }
  
  public int getItemPosition(Object paramObject)
  {
    paramObject = (String)((View)paramObject).getTag();
    if (paramObject == null) {
      return -2;
    }
    int j = get(paramObject);
    if (j == -1) {
      return -2;
    }
    int i = j;
    if (get()) {
      i = j + 1;
    }
    return i;
  }
  
  public boolean isViewFromObject(View paramView, Object paramObject)
  {
    return paramView == paramObject;
  }
  
  public void notifyDataSetChanged()
  {
    if (a == null) {}
    for (n = 0;; n = a.size())
    {
      super.notifyDataSetChanged();
      return;
    }
  }
  
  public void read(Context paramContext, Stream.Notifiable paramNotifiable)
  {
    add(paramContext, paramNotifiable, false);
  }
  
  public Stream remove()
  {
    return a;
  }
  
  public boolean write(int paramInt)
  {
    return ((get()) && (paramInt == 0)) || ((add()) && (paramInt == getCount() - 1));
  }
}
