package com.deviantart.android.damobile.util.discovery.pages;

import android.content.res.Resources;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView.LayoutParams;
import com.deviantart.android.damobile.adapter.recyclerview.DailyDeviationAdapter;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APIMixedDailyDeviationsLoader;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.scrolling.LinearViewEventScrollListener.LinearScrollEventListener;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.view.DAStateRecyclerView;

public class DiscoveryDailyDeviationPage
  extends DiscoveryPage
  implements LinearViewEventScrollListener.LinearScrollEventListener
{
  public DiscoveryDailyDeviationPage(String paramString1, String paramString2, String paramString3)
  {
    super(paramString1, paramString2, paramString3);
  }
  
  public View a(ViewGroup paramViewGroup)
  {
    super.a(paramViewGroup);
    paramViewGroup = new APIMixedDailyDeviationsLoader();
    paramViewGroup.add(1);
    Object localObject = StreamCacher.a(paramViewGroup, StreamCacheStrategy.b);
    paramViewGroup = new DAStateRecyclerView(a);
    paramViewGroup.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    paramViewGroup.setAdapter(new DailyDeviationAdapter((Stream)localObject));
    paramViewGroup.setAdapter();
    paramViewGroup.b(true);
    paramViewGroup.a(((Stream)localObject).e());
    paramViewGroup.setOnScrollListener(new DiscoveryDailyDeviationPage.1(this, this, (Stream)localObject));
    int i = a.getResources().getDimensionPixelOffset(2131361971);
    localObject = new View(get());
    ((View)localObject).setLayoutParams(new AbsListView.LayoutParams(-1, i));
    paramViewGroup.a((View)localObject);
    paramViewGroup.setPadding(paramViewGroup.getPaddingLeft() + (int)get().getResources().getDimension(2131361964), paramViewGroup.getPaddingTop(), paramViewGroup.getPaddingRight() + (int)get().getResources().getDimension(2131361964), paramViewGroup.getPaddingBottom());
    return paramViewGroup;
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    TrackerUtil.append(get(), EventKeys.Category.p, paramInt1, paramInt2);
  }
  
  public void b()
  {
    super.b();
  }
}
