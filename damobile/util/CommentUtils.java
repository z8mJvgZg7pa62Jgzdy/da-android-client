package com.deviantart.android.damobile.util;

import com.deviantart.android.android.package_14.model.DVNTComment;
import com.deviantart.android.android.package_14.model.DVNTUser;

public class CommentUtils
{
  public CommentUtils() {}
  
  public static boolean b(DVNTComment paramDVNTComment, String paramString)
  {
    return (UserUtils.a.equals(paramDVNTComment.getUser().getUserName())) || (UserUtils.a.equals(paramString));
  }
  
  public static boolean i(DVNTComment paramDVNTComment, String paramString)
  {
    return (paramDVNTComment.getHidden() != null) && (paramDVNTComment.getRepliesCount().intValue() == 0) && (b(paramDVNTComment, paramString));
  }
}
