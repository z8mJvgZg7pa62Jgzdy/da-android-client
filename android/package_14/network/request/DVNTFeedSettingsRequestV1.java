package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.android.package_14.network.wrapper.DVNTIncludeWrapper;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTFeedSettingsRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTFeedSettingsResponse>
{
  public DVNTFeedSettingsRequestV1()
  {
    super(com.deviantart.android.sdk.api.model.DVNTFeedSettingsResponse.class);
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass()) {
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "feed_settings";
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).toHashCode();
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTFeedSettingsResponse sendRequest(String paramString)
  {
    return (com.deviantart.android.android.package_14.model.DVNTFeedSettingsResponse)((DVNTServiceV1)getService()).feedSettings(paramString).getInclude();
  }
}
