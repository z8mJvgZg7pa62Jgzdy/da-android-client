package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTComment
  implements Serializable
{
  public static final String HIDDEN_BY_ADMIN = "hidden_by_admin";
  public static final String HIDDEN_BY_COMMENTER = "hidden_by_commenter";
  public static final String HIDDEN_BY_OWNER = "hidden_by_owner";
  DVNTDAMLComponent.List body;
  @SerializedName("commentid")
  private String commentId;
  private String hidden;
  private Integer level;
  @SerializedName("parentid")
  private String parentId;
  @SerializedName("replies")
  private Integer repliesCount;
  @SerializedName("posted")
  private String submissionDate;
  private DVNTUser user;
  
  public DVNTComment() {}
  
  public DVNTDAMLComponent.List getBody()
  {
    return body;
  }
  
  public String getCommentId()
  {
    return commentId;
  }
  
  public String getHidden()
  {
    return hidden;
  }
  
  public Integer getLevel()
  {
    return level;
  }
  
  public String getParentId()
  {
    return parentId;
  }
  
  public Integer getRepliesCount()
  {
    return repliesCount;
  }
  
  public String getSubmissionDate()
  {
    return submissionDate;
  }
  
  public DVNTUser getUser()
  {
    return user;
  }
  
  public void setBody(DVNTDAMLComponent.List paramList)
  {
    body = paramList;
  }
  
  public void setCommentId(String paramString)
  {
    commentId = paramString;
  }
  
  public void setHidden(String paramString)
  {
    hidden = paramString;
  }
  
  public void setLevel(Integer paramInteger)
  {
    level = paramInteger;
  }
  
  public void setParentId(String paramString)
  {
    parentId = paramString;
  }
  
  public void setRepliesCount(Integer paramInteger)
  {
    repliesCount = paramInteger;
  }
  
  public void setSubmissionDate(String paramString)
  {
    submissionDate = paramString;
  }
  
  public void setUser(DVNTUser paramDVNTUser)
  {
    user = paramDVNTUser;
  }
}
