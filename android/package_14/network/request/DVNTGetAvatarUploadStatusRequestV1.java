package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTGetAvatarUploadStatusRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTAvatarResponse>
{
  private String uploadId;
  
  public DVNTGetAvatarUploadStatusRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTAvatarResponse.class);
    uploadId = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTGetAvatarUploadStatusRequestV1 localDVNTGetAvatarUploadStatusRequestV1 = (DVNTGetAvatarUploadStatusRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(uploadId, uploadId).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(uploadId).toHashCode();
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTAvatarResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).getAvatarUploadStatus(paramString, uploadId);
  }
}
