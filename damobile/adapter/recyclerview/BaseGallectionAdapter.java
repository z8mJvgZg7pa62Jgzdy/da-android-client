package com.deviantart.android.damobile.adapter.recyclerview;

import android.os.Handler;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.GallectionItem;
import com.deviantart.android.damobile.util.torpedo.TorpedoPreview;
import com.deviantart.android.damobile.view.ViewState.State;
import com.deviantart.android.damobile.view.thirdparty.ByteVector;
import com.deviantart.android.damobile.view.thirdparty.GomFactory;
import com.deviantart.android.damobile.view.thirdparty.GomType;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class BaseGallectionAdapter
  extends DAStateRecyclerViewAdapter<com.deviantart.android.sdk.api.model.DVNTGallection>
{
  protected Boolean b = null;
  private String d;
  private String f;
  private boolean g;
  private View i = null;
  
  public BaseGallectionAdapter(Stream paramStream, String paramString1, String paramString2, boolean paramBoolean)
  {
    super(paramStream);
    d = paramString1;
    f = paramString2;
    g = paramBoolean;
  }
  
  private boolean d()
  {
    return (isDebug()) && (l()) && (!c.iterator());
  }
  
  private boolean isDebug()
  {
    return (b != null) && (b.booleanValue());
  }
  
  public abstract GallectionItem a(com.deviantart.android.android.package_14.model.DVNTGallection paramDVNTGallection, String paramString1, String paramString2, boolean paramBoolean);
  
  public void a()
  {
    Iterator localIterator = get().get().iterator();
    while (localIterator.hasNext()) {
      b(((com.deviantart.android.android.package_14.model.DVNTGallection)localIterator.next()).getFolderId(), d).a();
    }
    super.a();
  }
  
  public void a(Boolean paramBoolean)
  {
    b = paramBoolean;
    try
    {
      c();
      if (i != null)
      {
        b(i);
        return;
      }
    }
    catch (Exception paramBoolean)
    {
      for (;;)
      {
        new Handler().post(BaseGallectionAdapter..Lambda.3.c(this));
      }
    }
  }
  
  protected int b(int paramInt)
  {
    if ((d()) && (paramInt == e() - 1)) {
      return 1;
    }
    return 0;
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    switch (paramInt)
    {
    default: 
      throw new RuntimeException("Unhandled contentViewType in GallectionAdapter");
    case 0: 
      return GomFactory.a(GomType.b).b(paramViewGroup.getContext(), paramViewGroup);
    }
    return new BaseGallectionAdapter.1(this, LayoutInflater.from(paramViewGroup.getContext()).inflate(2130968798, paramViewGroup, false));
  }
  
  public abstract Stream b(String paramString1, String paramString2);
  
  public void b()
  {
    super.b();
    if (d()) {
      try
      {
        c();
        return;
      }
      catch (Exception localException)
      {
        new Handler().post(BaseGallectionAdapter..Lambda.2.c(this));
      }
    }
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    Object localObject = itemView;
    if ((localObject instanceof TorpedoPreview))
    {
      localObject = a((com.deviantart.android.android.package_14.model.DVNTGallection)get().get(paramInt), d, f, g);
      GomFactory.a(GomType.b).b(itemView.getContext(), localObject, paramViewHolder);
      return;
    }
    if ((localObject instanceof TextView)) {
      ((TextView)localObject).setOnClickListener(BaseGallectionAdapter..Lambda.4.finish(this));
    }
  }
  
  public void b(View paramView)
  {
    if ((paramView == null) || (!paramView.getTag().equals(ViewState.State.b)) || (!l()))
    {
      super.b(paramView);
      return;
    }
    if (b == null)
    {
      i = paramView;
      return;
    }
    if (d())
    {
      super.b(null);
      try
      {
        c();
        return;
      }
      catch (Exception paramView)
      {
        new Handler().post(BaseGallectionAdapter..Lambda.1.c(this));
        return;
      }
    }
    super.b(paramView);
  }
  
  public int e()
  {
    int k = super.e();
    int j = k;
    if (d()) {
      j = k + 1;
    }
    return j;
  }
  
  protected boolean l()
  {
    return false;
  }
}
