package com.deviantart.android.damobile.util.torpedo;

import android.util.Log;
import java.util.ArrayList;

public class Torpedo
{
  private ArrayList<Torpedo.TorpedoLane> a = new ArrayList();
  private ArrayList<Torpedo.TorpedoThumb> b = new ArrayList();
  private int c = 0;
  private int d = 0;
  private int e = 0;
  private final int r;
  
  public Torpedo(int paramInt)
  {
    r = paramInt;
  }
  
  public int a()
  {
    return e;
  }
  
  public Torpedo.TorpedoThumb a(int paramInt)
  {
    return (Torpedo.TorpedoThumb)b.get(paramInt);
  }
  
  public void a(Torpedo.TorpedoLane paramTorpedoLane)
  {
    if (paramTorpedoLane == null)
    {
      Log.e("torpedo", "can't add null lane");
      return;
    }
    e += paramTorpedoLane.c();
    d += paramTorpedoLane.e();
    b.addAll(paramTorpedoLane.get());
    if ((d <= r) || (a.size() < c)) {
      a.add(paramTorpedoLane);
    }
  }
  
  public int b()
  {
    return d;
  }
  
  public void b(int paramInt)
  {
    c = paramInt;
  }
  
  public void d(int paramInt)
  {
    d = paramInt;
  }
  
  public ArrayList get()
  {
    return a;
  }
}
