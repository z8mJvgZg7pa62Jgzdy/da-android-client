package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTDeviation;

public class APIBrowseUserJournalsLoader
  extends StreamLoader<DVNTDeviation>
{
  Boolean b;
  String c;
  
  public APIBrowseUserJournalsLoader(String paramString, Boolean paramBoolean)
  {
    c = paramString;
    b = paramBoolean;
  }
  
  public String b()
  {
    return "browseuserjournalsloader" + c + "|" + b;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.browseUserJournals(c, b, Integer.valueOf(paramInt), Integer.valueOf(a));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APIBrowseUserJournalsLoader.1(this, paramStreamLoadListener));
  }
}
