package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;

public class DVNTPushNotificationsSettings
{
  @SerializedName("send")
  DVNTPushNotificationsSettings.DVNTSendPushNotificationsSettings sendSettings;
  
  public DVNTPushNotificationsSettings() {}
  
  public DVNTPushNotificationsSettings.DVNTSendPushNotificationsSettings getSendSettings()
  {
    return sendSettings;
  }
  
  public DVNTPushNotificationsSettings setSendSettings(DVNTPushNotificationsSettings.DVNTSendPushNotificationsSettings paramDVNTSendPushNotificationsSettings)
  {
    sendSettings = paramDVNTSendPushNotificationsSettings;
    return this;
  }
}
