package com.deviantart.android.damobile.view.viewpageindicator;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import com.viewpagerindicator.IconPagerAdapter;

public class DeviationTabIndicator
  extends DATabIndicator
{
  private boolean l;
  
  public DeviationTabIndicator(Context paramContext)
  {
    super(paramContext);
  }
  
  public DeviationTabIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  protected void a(int paramInt1, CharSequence paramCharSequence1, int paramInt2, CharSequence paramCharSequence2)
  {
    DeviationTabIndicator.TabViewFrame localTabViewFrame = new DeviationTabIndicator.TabViewFrame(this, getContext());
    z = paramInt1;
    localTabViewFrame.setFocusable(true);
    localTabViewFrame.setOnClickListener(finish(paramInt1));
    localTabViewFrame.setContentDescription(paramCharSequence2);
    paramCharSequence2 = new CustomizableTabPageIndicator.TextViewTab(this, getContext());
    paramCharSequence2.setBackgroundResource(0);
    a(paramCharSequence2, paramCharSequence1, paramInt2);
    FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-2, -1);
    gravity = 17;
    localTabViewFrame.addView(paramCharSequence2, localLayoutParams);
    mTabLayout.addView(localTabViewFrame, b(paramInt1, paramCharSequence1, paramInt2));
  }
  
  protected void a(TextView paramTextView, CharSequence paramCharSequence, int paramInt)
  {
    paramTextView.setText(paramCharSequence);
    if (paramInt != 0)
    {
      paramTextView.setCompoundDrawablesWithIntrinsicBounds(paramInt, 0, 0, 0);
      paramTextView.setPadding(0, 0, 0, 0);
      if (paramCharSequence.length() != 0) {
        paramTextView.setCompoundDrawablePadding((int)TypedValue.applyDimension(1, 3.0F, getResources().getDisplayMetrics()));
      }
    }
  }
  
  public boolean d()
  {
    return l;
  }
  
  protected int getDefaultStyleAttr()
  {
    return 2130771979;
  }
  
  public void onDraw()
  {
    PagerAdapter localPagerAdapter = mViewPager.getAdapter();
    if ((localPagerAdapter instanceof IconPagerAdapter)) {}
    for (IconPagerAdapter localIconPagerAdapter = (IconPagerAdapter)localPagerAdapter;; localIconPagerAdapter = null)
    {
      int k = mTabLayout.getChildCount();
      int i = 0;
      TextView localTextView;
      CharSequence localCharSequence;
      if (i < k)
      {
        localTextView = (TextView)((DeviationTabIndicator.TabViewFrame)mTabLayout.getChildAt(i)).getChildAt(0);
        localCharSequence = localPagerAdapter.getPageTitle(i);
        if (localCharSequence != null) {
          break label113;
        }
        localCharSequence = CustomizableTabPageIndicator.k;
      }
      label113:
      for (;;)
      {
        if (localIconPagerAdapter != null) {}
        for (int j = localIconPagerAdapter.getIcon(i);; j = 0)
        {
          a(localTextView, localCharSequence, j);
          i += 1;
          break;
          return;
        }
      }
    }
  }
  
  public void setIsAnimated(boolean paramBoolean)
  {
    l = paramBoolean;
  }
}
