package com.deviantart.android.damobile.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;

public class SharedPreferenceUtil
{
  public SharedPreferenceUtil() {}
  
  public static SharedPreferences get(Context paramContext)
  {
    String str = getString(paramContext, "recent_username", null);
    if (str == null)
    {
      Log.w("SharedPreferenceUtil", "Could not get user specific preferences. Falling back to default prefs for logged out user.");
      return PreferenceManager.getDefaultSharedPreferences(paramContext);
    }
    return paramContext.getApplicationContext().getSharedPreferences(str, 0);
  }
  
  public static int getKey(Context paramContext, String paramString, int paramInt)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getInt(paramString, paramInt);
  }
  
  public static String getString(Context paramContext, String paramString1, String paramString2)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getString(paramString1, paramString2);
  }
  
  public static boolean getValue(Context paramContext, String paramString, boolean paramBoolean)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean(paramString, paramBoolean);
  }
  
  public static void putString(Context paramContext, String paramString, int paramInt)
  {
    paramContext = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    paramContext.putInt(paramString, paramInt);
    paramContext.apply();
  }
  
  public static void putString(Context paramContext, String paramString1, String paramString2)
  {
    paramContext = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    paramContext.putString(paramString1, paramString2);
    paramContext.apply();
  }
  
  public static void saveBoolean(Context paramContext, String paramString, boolean paramBoolean)
  {
    paramContext = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    paramContext.putBoolean(paramString, paramBoolean);
    paramContext.apply();
  }
}
