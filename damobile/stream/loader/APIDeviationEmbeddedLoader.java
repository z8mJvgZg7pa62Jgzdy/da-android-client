package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTDeviation;

public class APIDeviationEmbeddedLoader
  extends StreamLoader<DVNTDeviation>
{
  String b;
  String c;
  
  public APIDeviationEmbeddedLoader(String paramString1, String paramString2)
  {
    c = paramString1;
    b = paramString2;
  }
  
  public String b()
  {
    return "deviationembeddedloader" + c + "|" + b;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.deviationEmbedded(c, b, Integer.valueOf(paramInt), Integer.valueOf(a));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APIDeviationEmbeddedLoader.1(this, paramStreamLoadListener));
  }
}
