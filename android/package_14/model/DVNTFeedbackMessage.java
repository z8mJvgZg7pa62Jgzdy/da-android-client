package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTFeedbackMessage
  implements Serializable
{
  @SerializedName("collection")
  DVNTCollection collection;
  @SerializedName("comment")
  DVNTComment comment;
  @SerializedName("deviation")
  DVNTDeviation deviation;
  @SerializedName("messageid")
  String messageId;
  @SerializedName("originator")
  DVNTUser originator;
  @SerializedName("orphaned")
  Boolean orphaned;
  @SerializedName("profile")
  DVNTUser profile;
  @SerializedName("status")
  DVNTUserStatus status;
  @SerializedName("subject")
  DVNTFeedbackSubject subject;
  @SerializedName("ts")
  String timestamp;
  @SerializedName("type")
  DVNTFeedbackMessageType type;
  
  public DVNTFeedbackMessage() {}
  
  public DVNTCollection getCollection()
  {
    return collection;
  }
  
  public DVNTComment getComment()
  {
    return comment;
  }
  
  public DVNTDeviation getDeviation()
  {
    return deviation;
  }
  
  public String getMessageId()
  {
    return messageId;
  }
  
  public DVNTUser getOriginator()
  {
    return originator;
  }
  
  public DVNTUser getProfile()
  {
    return profile;
  }
  
  public DVNTUserStatus getStatus()
  {
    return status;
  }
  
  public DVNTFeedbackSubject getSubject()
  {
    return subject;
  }
  
  public String getTimestamp()
  {
    return timestamp;
  }
  
  public DVNTFeedbackMessageType getType()
  {
    return type;
  }
  
  public Boolean isOrphaned()
  {
    return orphaned;
  }
  
  public void setCollection(DVNTCollection paramDVNTCollection)
  {
    collection = paramDVNTCollection;
  }
  
  public void setComment(DVNTComment paramDVNTComment)
  {
    comment = paramDVNTComment;
  }
  
  public void setDeviation(DVNTDeviation paramDVNTDeviation)
  {
    deviation = paramDVNTDeviation;
  }
  
  public void setMessageId(String paramString)
  {
    messageId = paramString;
  }
  
  public void setOriginator(DVNTUser paramDVNTUser)
  {
    originator = paramDVNTUser;
  }
  
  public void setOrphaned(Boolean paramBoolean)
  {
    orphaned = paramBoolean;
  }
  
  public void setProfile(DVNTUser paramDVNTUser)
  {
    profile = paramDVNTUser;
  }
  
  public void setStatus(DVNTUserStatus paramDVNTUserStatus)
  {
    status = paramDVNTUserStatus;
  }
  
  public void setSubject(DVNTFeedbackSubject paramDVNTFeedbackSubject)
  {
    subject = paramDVNTFeedbackSubject;
  }
  
  public void setTimestamp(String paramString)
  {
    timestamp = paramString;
  }
  
  public void setType(DVNTFeedbackMessageType paramDVNTFeedbackMessageType)
  {
    type = paramDVNTFeedbackMessageType;
  }
}
