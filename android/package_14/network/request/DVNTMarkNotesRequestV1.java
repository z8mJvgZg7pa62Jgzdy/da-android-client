package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.model.DVNTNote.Mark;
import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.List;

public class DVNTMarkNotesRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  private final DVNTNote.Mark markAs;
  private final List<String> noteIds;
  
  public DVNTMarkNotesRequestV1(List paramList, DVNTNote.Mark paramMark)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    noteIds = paramList;
    markAs = paramMark;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTMarkNotesRequestV1)paramObject;
    return (Objects.append(noteIds, noteIds)) && (Objects.append(markAs, markAs));
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), noteIds, markAs });
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).markNotes(paramString, noteIds, markAs);
  }
}
