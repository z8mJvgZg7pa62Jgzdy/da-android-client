package com.deviantart.android.damobile.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.util.DAFormatUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class LiteratureThumbView
  extends LinearLayout
{
  public LiteratureThumbView(Context paramContext, DVNTDeviation paramDVNTDeviation)
  {
    super(paramContext);
    Object localObject = LayoutInflater.from(paramContext).inflate(2130968730, this, true);
    paramContext = (TextView)ButterKnife.findById((View)localObject, 2131689959);
    TextView localTextView = (TextView)ButterKnife.findById((View)localObject, 2131689960);
    localObject = (TextView)ButterKnife.findById((View)localObject, 2131689961);
    paramContext.setText(DAFormatUtils.getTitle(paramDVNTDeviation.getCategoryPath()));
    localTextView.setText(paramDVNTDeviation.getTitle());
    ((TextView)localObject).setText(Jsoup.get(paramDVNTDeviation.getExcerpt()).get());
  }
}
