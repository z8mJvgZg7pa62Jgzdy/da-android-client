package com.deviantart.android.damobile.util;

import android.support.v4.view.ViewPager.PageTransformer;
import android.view.View;

public class ZoomOutPageTransformer
  implements ViewPager.PageTransformer
{
  public ZoomOutPageTransformer() {}
  
  public void transformPage(View paramView, float paramFloat)
  {
    int i = paramView.getWidth();
    int j = paramView.getHeight();
    if (paramFloat < -1.0F)
    {
      paramView.setAlpha(0.0F);
      return;
    }
    if (paramFloat <= 1.0F)
    {
      float f1 = Math.max(0.8875F, 1.0F - Math.abs(paramFloat));
      float f2 = j * (1.0F - f1) / 2.0F;
      float f3 = i * (1.0F - f1) / 2.0F;
      if (paramFloat < 0.0F) {
        paramView.setTranslationX(f3 - f2 / 2.0F);
      }
      for (;;)
      {
        paramView.setScaleX(f1);
        paramView.setScaleY(f1);
        paramView.setAlpha((f1 - 0.8875F) / 0.11250001F * 0.5F + 0.5F);
        return;
        paramView.setTranslationX(-f3 + f2 / 2.0F);
      }
    }
    paramView.setAlpha(0.0F);
  }
}
