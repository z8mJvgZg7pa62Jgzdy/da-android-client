package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.sdk.api.network.service.DVNTServiceV1Flavored;

public abstract class DVNTAsyncRequestV1Extra<T>
  extends com.deviantart.android.sdk.api.network.request.DVNTBaseAsyncRequest<T, DVNTServiceV1Flavored>
{
  protected DVNTAsyncRequestV1Extra(Class paramClass)
  {
    super(paramClass, DVNTServiceV1Flavored.class);
  }
}
