package com.deviantart.android.damobile.activity;

public abstract class MultiStepActivity
  extends BaseActivity
{
  public MultiStepActivity() {}
  
  public abstract void onOptionsItemSelected();
  
  public abstract void showImage();
}
