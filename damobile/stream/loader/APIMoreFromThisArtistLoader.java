package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTDeviation;

public class APIMoreFromThisArtistLoader
  extends StreamLoader<DVNTDeviation>
{
  private final String d;
  
  public APIMoreFromThisArtistLoader(String paramString)
  {
    d = paramString;
  }
  
  public String b()
  {
    return "mfaloader" + d;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.moreLikeThisPreview(d);
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APIMoreFromThisArtistLoader.1(this, paramStreamLoadListener));
  }
}
