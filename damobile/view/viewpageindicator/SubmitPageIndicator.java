package com.deviantart.android.damobile.view.viewpageindicator;

import android.content.Context;
import android.util.AttributeSet;

public class SubmitPageIndicator
  extends DATextTabIndicator
{
  public SubmitPageIndicator(Context paramContext)
  {
    super(paramContext);
  }
  
  public SubmitPageIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  protected int getUnselectedColorResId()
  {
    return 2131558552;
  }
}
