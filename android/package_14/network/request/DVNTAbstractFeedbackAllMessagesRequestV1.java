package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.sdk.api.model.DVNTFeedbackCursoredPage;
import com.google.common.base.Objects;

public abstract class DVNTAbstractFeedbackAllMessagesRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<DVNTFeedbackCursoredPage>
{
  protected String cursor;
  protected String messageFolderId;
  protected Boolean returnStackedMessages;
  
  public DVNTAbstractFeedbackAllMessagesRequestV1(String paramString1, Boolean paramBoolean, String paramString2)
  {
    super(DVNTFeedbackCursoredPage.class);
    messageFolderId = paramString1;
    returnStackedMessages = paramBoolean;
    cursor = paramString2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTAbstractFeedbackAllMessagesRequestV1)paramObject;
    return (Objects.append(messageFolderId, messageFolderId)) && (Objects.append(returnStackedMessages, returnStackedMessages)) && (Objects.append(cursor, cursor));
  }
  
  public String getCacheKey()
  {
    return "feedbackallmessages" + messageFolderId + returnStackedMessages + cursor;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), messageFolderId, returnStackedMessages, cursor });
  }
}
