package com.deviantart.android.damobile.util;

import android.app.Activity;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.android.package_14.model.DVNTThumbable;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.fragment.DeviationFullViewFragment;
import com.deviantart.android.damobile.fragment.DeviationFullViewFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.HomeBaseFragment.HomeFragmentInstanceBuilder;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StaticStreamLoader;
import com.deviantart.android.damobile.util.deviation.DeviationPanelTab;
import com.deviantart.android.damobile.view.DAFaveView;
import com.deviantart.android.damobile.view.FaveButton;
import com.deviantart.android.damobile.view.MLTButton;
import com.deviantart.android.damobile.view.RepostButton;
import java.util.ArrayList;
import java.util.Iterator;

public class DeviationUtils
{
  public static ProcessMenuType b;
  public static String c = null;
  public static final String[] s = { "journals", "personaljournal" };
  
  public DeviationUtils() {}
  
  public static DVNTImage a(DVNTThumbable paramDVNTThumbable)
  {
    return a(paramDVNTThumbable, true);
  }
  
  public static DVNTImage a(DVNTThumbable paramDVNTThumbable, boolean paramBoolean)
  {
    DVNTImage localDVNTImage = null;
    if (paramDVNTThumbable.getThumbs() != null)
    {
      if (paramDVNTThumbable.getThumbs().isEmpty()) {
        return null;
      }
      Iterator localIterator = paramDVNTThumbable.getThumbs().iterator();
      int i = -1;
      paramDVNTThumbable = localDVNTImage;
      if (localIterator.hasNext())
      {
        localDVNTImage = (DVNTImage)localIterator.next();
        int j = localDVNTImage.getHeight() * localDVNTImage.getWidth();
        if ((i == -1) || ((paramBoolean) && (j > i)) || ((!paramBoolean) && (j < i)))
        {
          i = j;
          paramDVNTThumbable = localDVNTImage;
        }
        for (;;)
        {
          break;
        }
      }
    }
    else
    {
      return null;
    }
    return paramDVNTThumbable;
  }
  
  public static DeviationType a(DVNTDeviation paramDVNTDeviation)
  {
    if (paramDVNTDeviation.getId().equals("placeholder")) {
      return DeviationType.g;
    }
    if (paramDVNTDeviation.getVideos() != null) {
      return DeviationType.d;
    }
    if (paramDVNTDeviation.getContent() != null) {
      return DeviationType.c;
    }
    if (paramDVNTDeviation.getExcerpt() != null)
    {
      String[] arrayOfString = s;
      int j = arrayOfString.length;
      int i = 0;
      while (i < j)
      {
        String str = arrayOfString[i];
        if (paramDVNTDeviation.getCategoryPath().startsWith(str)) {
          return DeviationType.e;
        }
        i += 1;
      }
      return DeviationType.b;
    }
    return DeviationType.d;
  }
  
  public static DVNTImage b(DVNTDeviation paramDVNTDeviation)
  {
    return b(paramDVNTDeviation, true);
  }
  
  public static DVNTImage b(DVNTDeviation paramDVNTDeviation, boolean paramBoolean)
  {
    Object localObject = paramDVNTDeviation.getPreview();
    if (localObject != null) {
      return localObject;
    }
    if (paramBoolean)
    {
      DVNTImage localDVNTImage = a(paramDVNTDeviation);
      localObject = localDVNTImage;
      if (localDVNTImage != null) {}
    }
    else
    {
      paramDVNTDeviation = paramDVNTDeviation.getContent();
      localObject = paramDVNTDeviation;
      if (paramDVNTDeviation == null) {
        return null;
      }
    }
    return localObject;
  }
  
  public static DVNTImage b(DVNTThumbable paramDVNTThumbable)
  {
    return a(paramDVNTThumbable, false);
  }
  
  public static ProcessMenuListener b(DeviationUtils.ProcessMenuAddable paramProcessMenuAddable)
  {
    return b(paramProcessMenuAddable, null);
  }
  
  public static ProcessMenuListener b(DeviationUtils.ProcessMenuAddable paramProcessMenuAddable, ProcessMenuListener paramProcessMenuListener)
  {
    return new DeviationUtils.1(paramProcessMenuListener, paramProcessMenuAddable);
  }
  
  public static ProcessMenuListener b(ProcessMenuListener paramProcessMenuListener)
  {
    return b(null, paramProcessMenuListener);
  }
  
  public static ArrayList b(Activity paramActivity, DVNTDeviation paramDVNTDeviation)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new FaveButton(paramActivity, paramDVNTDeviation));
    localArrayList.add(new RepostButton(paramActivity, paramDVNTDeviation));
    localArrayList.add(new MLTButton(paramActivity, paramDVNTDeviation));
    return localArrayList;
  }
  
  public static void b(DAFaveView paramDAFaveView, DVNTDeviation paramDVNTDeviation)
  {
    boolean bool2 = false;
    boolean bool1 = bool2;
    if (c != null)
    {
      bool1 = bool2;
      if (c.equals(paramDVNTDeviation.getId()))
      {
        c = null;
        bool1 = true;
      }
    }
    paramDAFaveView.show(paramDVNTDeviation.isFavourited().booleanValue(), bool1);
  }
  
  public static void c(Activity paramActivity, DVNTDeviation paramDVNTDeviation, DeviationPanelTab paramDeviationPanelTab)
  {
    if (DVNTContextUtils.isContextDead(paramActivity)) {
      return;
    }
    Stream localStream = StreamCacher.a(new StaticStreamLoader(paramDVNTDeviation, "staticstreamloader-deviation-" + paramDVNTDeviation.getId()));
    ScreenFlowManager.a(paramActivity, (DeviationFullViewFragment)new DeviationFullViewFragment.InstanceBuilder().a(localStream).a(paramDeviationPanelTab).a(), "deviation_fullview_single" + paramDVNTDeviation.getId());
  }
  
  public static String e(DVNTDeviation paramDVNTDeviation)
  {
    return formatString(paramDVNTDeviation) + "\n" + paramDVNTDeviation.getUrl();
  }
  
  public static String formatString(DVNTDeviation paramDVNTDeviation)
  {
    return toString(paramDVNTDeviation) + " on @DeviantArt";
  }
  
  public static DVNTImage operate(DVNTDeviation paramDVNTDeviation)
  {
    if (paramDVNTDeviation.getContent() != null) {
      return paramDVNTDeviation.getContent();
    }
    if (paramDVNTDeviation.getPreview() != null) {
      return paramDVNTDeviation.getPreview();
    }
    return null;
  }
  
  public static ArrayList replaceAll(Activity paramActivity, String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new FaveButton(paramActivity, paramString));
    localArrayList.add(new RepostButton(paramActivity, paramString));
    localArrayList.add(new MLTButton(paramActivity, paramString));
    return localArrayList;
  }
  
  public static String toString(DVNTDeviation paramDVNTDeviation)
  {
    return paramDVNTDeviation.getTitle() + " by " + paramDVNTDeviation.getAuthor().getUserName();
  }
}
