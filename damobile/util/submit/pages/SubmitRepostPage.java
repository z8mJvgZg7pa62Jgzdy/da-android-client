package com.deviantart.android.damobile.util.submit.pages;

import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.view.RepostView;

public abstract interface SubmitRepostPage
{
  public abstract View b(ViewGroup paramViewGroup, RepostView paramRepostView);
}
