package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTStashMetadata
  implements Serializable
{
  @SerializedName("description")
  String description;
  @SerializedName("parentid")
  Long parentId;
  @SerializedName("path")
  String path;
  @SerializedName("stackid")
  Long stackId;
  @SerializedName("title")
  String title;
  
  public DVNTStashMetadata() {}
  
  public String getDescription()
  {
    return description;
  }
  
  public Long getParentId()
  {
    return parentId;
  }
  
  public String getPath()
  {
    return path;
  }
  
  public Long getStackId()
  {
    return stackId;
  }
  
  public String getTitle()
  {
    return title;
  }
  
  public void setDescription(String paramString)
  {
    description = paramString;
  }
  
  public void setParentId(Long paramLong)
  {
    parentId = paramLong;
  }
  
  public void setPath(String paramString)
  {
    path = paramString;
  }
  
  public void setStackId(Long paramLong)
  {
    stackId = paramLong;
  }
  
  public void setTitle(String paramString)
  {
    title = paramString;
  }
}
