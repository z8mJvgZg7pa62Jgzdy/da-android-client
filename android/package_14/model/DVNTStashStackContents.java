package com.deviantart.android.android.package_14.model;

import com.deviantart.android.sdk.api.model.DVNTPaginatedResult;
import com.deviantart.android.sdk.api.model.DVNTStashMetadata.List;

public class DVNTStashStackContents
  extends DVNTPaginatedResult<DVNTStashMetadata.List>
{
  public DVNTStashStackContents() {}
}
