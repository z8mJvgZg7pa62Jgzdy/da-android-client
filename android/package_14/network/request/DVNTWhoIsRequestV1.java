package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.sdk.api.network.request.GroupableRequest;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.HashMap;
import java.util.List;

public class DVNTWhoIsRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTWhoIsResponses>
  implements GroupableRequest
{
  List<String> userNames;
  
  public DVNTWhoIsRequestV1(List paramList)
  {
    super(com.deviantart.android.sdk.api.model.DVNTWhoIsResponses.class);
    userNames = paramList;
  }
  
  public String getCacheKey()
  {
    return "whois" + userNames;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public HashMap getGETArguments()
  {
    return null;
  }
  
  public String getGroupEndpoint()
  {
    return "/api/v1/oauth2/user/whois";
  }
  
  public HashMap getPOSTArguments()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("usernames", userNames);
    return localHashMap;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTWhoIsResponses sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).whoIs(paramString, userNames);
  }
}
