package com.deviantart.android.damobile.util;

import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TodayDailyDeviationItem
  extends DeviationDescription
  implements DailyDeviationItem
{
  private Date d;
  private boolean j;
  
  public TodayDailyDeviationItem(DVNTDeviation paramDVNTDeviation, boolean paramBoolean, Stream paramStream, int paramInt)
  {
    super(paramDVNTDeviation, paramStream.length(), paramInt);
    d = DAFormatUtils.set(paramDVNTDeviation);
    j = paramBoolean;
  }
  
  public String b()
  {
    return DAFormatUtils.DateFormats.A.format(d);
  }
  
  public DailyDeviationItem.ItemType getState()
  {
    return DailyDeviationItem.ItemType.STATE_INVALID;
  }
  
  public boolean h()
  {
    return j;
  }
}
