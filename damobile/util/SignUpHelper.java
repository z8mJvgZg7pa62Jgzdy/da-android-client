package com.deviantart.android.damobile.util;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.view.EditTextWithClearFocus;

public class SignUpHelper
{
  public SignUpHelper() {}
  
  public static void onItemSelected(boolean paramBoolean, EditTextWithClearFocus paramEditTextWithClearFocus, TextView paramTextView, int paramInt)
  {
    if (paramEditTextWithClearFocus != null)
    {
      if (paramTextView == null) {
        return;
      }
      if (paramBoolean)
      {
        paramEditTextWithClearFocus.setHint(null);
        paramTextView.setVisibility(0);
        return;
      }
      paramEditTextWithClearFocus.setHint(paramInt);
      if (paramEditTextWithClearFocus.getText().length() == 0) {
        paramTextView.setVisibility(8);
      }
    }
  }
  
  public static void set(Context paramContext, String paramString1, String paramString2, String paramString3, SignUpHelper.APIValidationType paramAPIValidationType, SignUpHelper.ValidationListener paramValidationListener)
  {
    if (DVNTContextUtils.isContextDead(paramContext)) {
      return;
    }
    DVNTAsyncAPI.validateAccount(paramString1, paramString2, paramString3).call(paramContext, new SignUpHelper.1(paramValidationListener, paramContext, paramAPIValidationType));
  }
  
  public static void showHelpDialog(Context paramContext, TextView paramTextView1, TextView paramTextView2, String paramString)
  {
    if (paramString == null)
    {
      paramTextView1.setActivated(true);
      paramTextView1.setTextColor(paramContext.getResources().getColor(2131558484));
      paramTextView2.setVisibility(8);
      return;
    }
    paramTextView1.setActivated(false);
    paramTextView1.setTextColor(paramContext.getResources().getColor(2131558547));
    paramTextView2.setTextColor(paramContext.getResources().getColor(2131558547));
    paramTextView2.setText(paramString);
    paramTextView2.setVisibility(0);
  }
}
