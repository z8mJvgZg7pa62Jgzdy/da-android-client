package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTUpdateAccountRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  private String currentPassword;
  private String newEmail;
  private String newPassword;
  private String scope;
  private Boolean subscribeEmail;
  
  public DVNTUpdateAccountRequestV1(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    currentPassword = paramString1;
    newPassword = paramString2;
    newEmail = paramString3;
    scope = paramString4;
    subscribeEmail = paramBoolean;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTUpdateAccountRequestV1 localDVNTUpdateAccountRequestV1 = (DVNTUpdateAccountRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(currentPassword, currentPassword).append(newPassword, newPassword).append(newEmail, newEmail).append(scope, scope).append(subscribeEmail, subscribeEmail).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(currentPassword).append(newPassword).append(newEmail).append(scope).append(subscribeEmail).toHashCode();
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).updateAccount(paramString, currentPassword, newPassword, newEmail, scope, subscribeEmail);
  }
}
