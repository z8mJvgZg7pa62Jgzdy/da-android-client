package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTChallengeEntriesRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse>
{
  private String deviationId;
  private Integer limit;
  private Integer offset;
  
  public DVNTChallengeEntriesRequestV1(String paramString, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse.class);
    offset = paramInteger1;
    limit = paramInteger2;
    deviationId = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTChallengeEntriesRequestV1 localDVNTChallengeEntriesRequestV1 = (DVNTChallengeEntriesRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(offset, offset).append(limit, limit).append(deviationId, deviationId).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "challenge_entries" + deviationId + offset + limit;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(offset).append(limit).append(deviationId).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTPaginatedResultResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).challengeEntries(paramString, deviationId, offset, limit);
  }
}
