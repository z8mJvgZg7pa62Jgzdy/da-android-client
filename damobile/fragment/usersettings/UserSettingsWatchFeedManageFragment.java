package com.deviantart.android.damobile.fragment.usersettings;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.activity.HomeActivity;
import com.deviantart.android.damobile.activity.HomeActivity.HomeActivityPages;
import com.deviantart.android.damobile.activity.UserSettingsActivity;
import com.deviantart.android.damobile.activity.UserSettingsActivity.OnBackResult;
import com.deviantart.android.damobile.fragment.PhotoFragment;
import com.deviantart.android.damobile.util.OnWatchRecoClickListener;
import com.deviantart.android.damobile.view.WatchFeedManageContentLayout;
import com.deviantart.android.damobile.view.viewpageindicator.CustomizableTabPageIndicator;
import com.deviantart.android.damobile.view.viewpageindicator.DATextTabIndicator;

public class UserSettingsWatchFeedManageFragment
  extends UserSettingsBaseFragment
  implements OnWatchRecoClickListener
{
  private boolean b = false;
  @Bind({2131689763})
  DATextTabIndicator indicator;
  private int l = 0;
  @Bind({2131689764})
  ViewPager pager;
  
  public UserSettingsWatchFeedManageFragment() {}
  
  public UserSettingsActivity.OnBackResult a()
  {
    WatchFeedManageContentLayout localWatchFeedManageContentLayout = (WatchFeedManageContentLayout)pager.findViewWithTag("watch_feed_manage_tag" + UserSettingsWatchFeedManageFragment.WatchFeedManageTab.d.ordinal());
    if ((localWatchFeedManageContentLayout != null) && (localWatchFeedManageContentLayout.c())) {
      ((UserSettingsActivity)getActivity()).b(localWatchFeedManageContentLayout.getContentsSetting());
    }
    if (l > 0) {
      com.deviantart.android.damobile.fragment.WatchFeedFragment.p = true;
    }
    return UserSettingsActivity.OnBackResult.o;
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if ((paramInt1 == 112) && (pager != null)) {
      pager.getAdapter().notifyDataSetChanged();
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = paramLayoutInflater.inflate(2130968672, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    pager.setAdapter(new UserSettingsWatchFeedManageFragment.WatchFeedManageAdapter(this));
    pager.setOnPageChangeListener(new UserSettingsWatchFeedManageFragment.1(this));
    indicator.setViewPager(pager);
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public void onResume()
  {
    super.onResume();
    ((UserSettingsActivity)getActivity()).onProgressChanged(2131231288);
  }
  
  public void showFavorites()
  {
    Intent localIntent = new Intent(getActivity(), HomeActivity.class);
    localIntent.putExtra("home_start_page", HomeActivity.HomeActivityPages.d);
    startActivityForResult(localIntent, 112);
  }
}
