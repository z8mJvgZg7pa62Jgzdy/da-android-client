package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTFeedNotificationsRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTFeedResponse>
{
  private String cursor;
  
  public DVNTFeedNotificationsRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTFeedResponse.class);
    cursor = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTFeedNotificationsRequestV1 localDVNTFeedNotificationsRequestV1 = (DVNTFeedNotificationsRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(cursor, cursor).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "notifications" + cursor;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(cursor).toHashCode();
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTFeedResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).notifications(paramString, cursor, Boolean.valueOf(true));
  }
}
