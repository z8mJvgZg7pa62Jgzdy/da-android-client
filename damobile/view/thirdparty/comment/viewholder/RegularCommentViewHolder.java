package com.deviantart.android.damobile.view.thirdparty.comment.viewholder;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.facebook.drawee.view.SimpleDraweeView;

public class RegularCommentViewHolder
  extends CommentViewHolderBase
{
  private String actionDescription;
  @Bind({2131689635})
  public TextView commentAuthor;
  @Bind({2131689633})
  public SimpleDraweeView commentAuthorAvatar;
  @Bind({2131689630})
  public RelativeLayout commentContainer;
  @Bind({2131689637})
  public TextView commentContent;
  @Bind({2131689636})
  public TextView commentDate;
  @Bind({2131689632})
  public View commentHighlight;
  @Bind({2131689629})
  public RelativeLayout commentLayout;
  
  public RegularCommentViewHolder(View paramView)
  {
    super(paramView);
    ButterKnife.bind(this, paramView);
  }
  
  public String buildKey()
  {
    return actionDescription;
  }
  
  public void from(String paramString)
  {
    actionDescription = paramString;
  }
}
