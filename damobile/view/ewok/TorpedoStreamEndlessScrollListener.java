package com.deviantart.android.damobile.view.ewok;

import android.app.Activity;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.view.EndlessScrollListener;
import com.deviantart.android.sdk.api.model.DVNTDeviation;

public abstract class TorpedoStreamEndlessScrollListener
  implements EndlessScrollListener
{
  protected final Activity b;
  protected final Stream<DVNTDeviation> o;
  
  public TorpedoStreamEndlessScrollListener(Stream paramStream, Activity paramActivity)
  {
    o = paramStream;
    b = paramActivity;
  }
}
