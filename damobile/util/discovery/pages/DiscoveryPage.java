package com.deviantart.android.damobile.util.discovery.pages;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.util.mainpager.DAMainPage;

public abstract class DiscoveryPage
  extends DAMainPage
{
  protected Activity a;
  protected CharSequence c;
  private String d;
  private String desc;
  private String h;
  protected int k = 0;
  protected boolean l = false;
  protected String s;
  
  public DiscoveryPage(String paramString, CharSequence paramCharSequence)
  {
    s = paramString;
    c = paramCharSequence;
    l = false;
  }
  
  public DiscoveryPage(String paramString1, CharSequence paramCharSequence, String paramString2)
  {
    this(paramString1, paramCharSequence);
    desc = paramString2;
  }
  
  public DiscoveryPage(String paramString1, CharSequence paramCharSequence, String paramString2, boolean paramBoolean)
  {
    this(paramString1, paramCharSequence, paramString2);
    l = paramBoolean;
  }
  
  public View a(ViewGroup paramViewGroup)
  {
    a = ((Activity)paramViewGroup.getContext());
    return null;
  }
  
  public void b()
  {
    a = null;
  }
  
  public void b(int paramInt)
  {
    k = paramInt;
  }
  
  public void b(String paramString)
  {
    d = paramString;
  }
  
  public String c()
  {
    return h;
  }
  
  public boolean d()
  {
    return l;
  }
  
  public void f(String paramString)
  {
    h = paramString;
  }
  
  protected Activity get()
  {
    return a;
  }
  
  public String getDescriptor()
  {
    return desc;
  }
  
  public CharSequence getPageTitle()
  {
    return c;
  }
  
  public String read()
  {
    return s;
  }
  
  public String s()
  {
    return d;
  }
}
