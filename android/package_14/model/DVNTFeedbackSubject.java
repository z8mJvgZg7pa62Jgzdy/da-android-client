package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTFeedbackSubject
  implements Serializable
{
  @SerializedName("comment")
  DVNTComment subjectComment;
  @SerializedName("deviation")
  DVNTDeviation subjectDeviation;
  @SerializedName("profile")
  DVNTUser subjectProfile;
  @SerializedName("status")
  DVNTUserStatus subjectStatus;
  
  public DVNTFeedbackSubject() {}
  
  public DVNTComment getSubjectComment()
  {
    return subjectComment;
  }
  
  public DVNTDeviation getSubjectDeviation()
  {
    return subjectDeviation;
  }
  
  public DVNTUser getSubjectProfile()
  {
    return subjectProfile;
  }
  
  public DVNTUserStatus getSubjectStatus()
  {
    return subjectStatus;
  }
  
  public void setSubjectComment(DVNTComment paramDVNTComment)
  {
    subjectComment = paramDVNTComment;
  }
  
  public void setSubjectDeviation(DVNTDeviation paramDVNTDeviation)
  {
    subjectDeviation = paramDVNTDeviation;
  }
  
  public void setSubjectProfile(DVNTUser paramDVNTUser)
  {
    subjectProfile = paramDVNTUser;
  }
  
  public void setSubjectStatus(DVNTUserStatus paramDVNTUserStatus)
  {
    subjectStatus = paramDVNTUserStatus;
  }
}
