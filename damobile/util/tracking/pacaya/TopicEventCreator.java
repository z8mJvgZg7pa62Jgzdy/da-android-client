package com.deviantart.android.damobile.util.tracking.pacaya;

import com.deviantart.upgrade.labs.DVNTTopicEvent;
import java.util.Date;
import java.util.HashMap;

public class TopicEventCreator
{
  private static long c = 0L;
  private static long d;
  private HashMap<String, Object> a = new HashMap();
  private String b;
  
  public TopicEventCreator(String paramString)
  {
    b = paramString;
  }
  
  protected String a()
  {
    long l = new Date().getTime() / 1000L;
    if (d != l)
    {
      d = l;
      c = 0L;
    }
    c += 1L;
    return Long.toHexString(d << 31 | c & 0x7FFFFFFF);
  }
  
  public TopicEventCreator add(String paramString, Object paramObject)
  {
    if ((paramString != null) && (paramObject != null)) {
      a.put(paramString, paramObject);
    }
    return this;
  }
  
  public DVNTTopicEvent b()
  {
    DVNTTopicEvent localDVNTTopicEvent = new DVNTTopicEvent();
    f = a();
    c = b;
    a = a;
    return localDVNTTopicEvent;
  }
}
