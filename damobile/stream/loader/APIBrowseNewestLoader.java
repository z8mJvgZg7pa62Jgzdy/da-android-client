package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTDeviation;

public class APIBrowseNewestLoader
  extends StreamLoader<DVNTDeviation>
{
  String c;
  String d;
  Boolean e;
  
  public APIBrowseNewestLoader(String paramString1, String paramString2, Boolean paramBoolean)
  {
    c = paramString1;
    d = paramString2;
    e = paramBoolean;
  }
  
  public String b()
  {
    return "browsenewestloader" + c + "|" + d + "|" + e;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.browseNewest(c, d, e, Integer.valueOf(paramInt), Integer.valueOf(a));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APIBrowseNewestLoader.1(this, paramStreamLoadListener));
  }
}
