package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

public class DVNTSuggestFavouriteToGroupRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  private final String deviationId;
  private final Integer folderId;
  private final String groupName;
  
  public DVNTSuggestFavouriteToGroupRequestV1(String paramString1, String paramString2, Integer paramInteger)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    groupName = paramString1;
    deviationId = paramString2;
    folderId = paramInteger;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTSuggestFavouriteToGroupRequestV1)paramObject;
    return (Objects.append(groupName, groupName)) && (Objects.append(deviationId, deviationId)) && (Objects.append(folderId, folderId));
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), groupName, deviationId, folderId });
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).suggestFaveToGroup(paramString, groupName, deviationId, folderId);
  }
}
