package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTCollection;

public class APICollectionFoldersLoader
  extends FolderListStreamLoader<DVNTCollection>
{
  private final boolean b;
  private final boolean c;
  private final String d;
  
  public APICollectionFoldersLoader(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    d = paramString;
    c = paramBoolean1;
    b = paramBoolean2;
  }
  
  public String b()
  {
    return "collectionfoldersloader" + d + "|" + c + "|" + b;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.getCollectionFolders(d, c, b, Integer.valueOf(paramInt), Integer.valueOf(a));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APICollectionFoldersLoader.1(this, paramStreamLoadListener));
  }
}
