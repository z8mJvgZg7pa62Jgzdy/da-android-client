package com.deviantart.android.android.package_14.network.request;

public abstract interface VersionedRequest
{
  public abstract String getVersion();
  
  public abstract void setVersion(String paramString);
}
