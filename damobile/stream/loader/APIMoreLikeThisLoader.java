package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTDeviation;

public class APIMoreLikeThisLoader
  extends StreamLoader<DVNTDeviation>
{
  private final String c;
  private final String d;
  
  public APIMoreLikeThisLoader(String paramString1, String paramString2)
  {
    c = paramString1;
    d = paramString2;
  }
  
  public String b()
  {
    return "mltloader" + c;
  }
  
  public String c()
  {
    return c;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTCommonAsyncAPI.moreLikeThis(c, d, Integer.valueOf(paramInt), Integer.valueOf(a));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APIMoreLikeThisLoader.1(this, paramStreamLoadListener));
  }
}
