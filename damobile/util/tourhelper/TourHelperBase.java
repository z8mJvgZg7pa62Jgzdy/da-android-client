package com.deviantart.android.damobile.util.tourhelper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.view.View;
import com.deviantart.android.damobile.view.tooltip.DAToolTipRelativeLayout;
import com.deviantart.android.damobile.view.tooltip.tour.DAToolTipTour;
import com.deviantart.android.damobile.view.tooltip.tour.DAToolTipTour.TourListener;
import com.deviantart.android.damobile.view.tooltip.tour.DATourSlideDescriptor;

public abstract class TourHelperBase
  implements DAToolTipTour.TourListener
{
  protected Activity a;
  private DAToolTipTour b;
  
  public TourHelperBase(Activity paramActivity)
  {
    a = paramActivity;
  }
  
  protected static boolean update(Context paramContext, String paramString)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean(paramString, false);
  }
  
  protected void b() {}
  
  public void b(View paramView, DAToolTipRelativeLayout paramDAToolTipRelativeLayout)
  {
    b = new DAToolTipTour(getObjects(), this, paramView, paramDAToolTipRelativeLayout);
    b.f();
    b();
  }
  
  public void d()
  {
    if ((b != null) && (b.b())) {
      b.a();
    }
    b = null;
    a = null;
  }
  
  protected abstract String getName();
  
  protected abstract DATourSlideDescriptor[] getObjects();
  
  public void putLong()
  {
    PreferenceManager.getDefaultSharedPreferences(a).edit().putBoolean(getName(), true).apply();
    b = null;
  }
}
