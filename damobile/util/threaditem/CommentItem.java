package com.deviantart.android.damobile.util.threaditem;

import java.io.Serializable;

public class CommentItem
  implements ThreadItem<com.deviantart.android.sdk.api.model.DVNTComment>, Serializable
{
  protected boolean a = false;
  protected Integer c = Integer.valueOf(0);
  protected String d;
  protected boolean highlight;
  protected com.deviantart.android.android.package_14.model.DVNTComment i;
  protected boolean l = false;
  
  public CommentItem(com.deviantart.android.android.package_14.model.DVNTComment paramDVNTComment, boolean paramBoolean)
  {
    i = paramDVNTComment;
    highlight = paramBoolean;
  }
  
  public String a()
  {
    if (d != null) {
      return d;
    }
    if (i.getParentId() == null) {
      return c();
    }
    return i.getParentId();
  }
  
  public void add(Integer paramInteger)
  {
    c = paramInteger;
  }
  
  public String b()
  {
    return i.getParentId();
  }
  
  public void b(String paramString)
  {
    d = paramString;
  }
  
  public void b(boolean paramBoolean)
  {
    l = paramBoolean;
  }
  
  public String c()
  {
    return i.getCommentId();
  }
  
  public String d()
  {
    return "comment_sub_thread_" + b();
  }
  
  public void d(Boolean paramBoolean)
  {
    a = paramBoolean.booleanValue();
  }
  
  public String f()
  {
    return "comment_sub_thread_" + c();
  }
  
  public Integer get()
  {
    return c;
  }
  
  public com.deviantart.android.android.package_14.model.DVNTComment intValue()
  {
    return i;
  }
  
  public Boolean isUnread()
  {
    return Boolean.valueOf(highlight);
  }
  
  public boolean n()
  {
    return l;
  }
  
  public Integer next()
  {
    return i.getRepliesCount();
  }
  
  public Boolean visitAnnotation()
  {
    return Boolean.valueOf(a);
  }
}
