package com.deviantart.android.damobile.view;

public abstract interface ScrollDetectedListener
{
  public abstract void reuse(ScrollDetectableScrollView paramScrollDetectableScrollView, int paramInt1, int paramInt2, int paramInt3, int paramInt4);
}
