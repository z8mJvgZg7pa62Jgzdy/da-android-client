package com.deviantart.android.damobile.util.scrolling;

import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnSignificantScrollEvent;
import com.squareup.otto.Bus;

public class RecyclerViewSignificantScrollListener
  extends RecyclerViewScrollListenerBase
{
  private Integer a = null;
  private boolean l = true;
  
  public RecyclerViewSignificantScrollListener() {}
  
  private void b(boolean paramBoolean)
  {
    l = paramBoolean;
    BusStation.get().post(new BusStation.OnSignificantScrollEvent(paramBoolean));
  }
  
  public void b(int paramInt1, int paramInt2, int paramInt3)
  {
    if (a == null)
    {
      a = Integer.valueOf(paramInt3);
      return;
    }
    if ((l) && (paramInt3 > a.intValue())) {
      b(false);
    }
    for (;;)
    {
      a = Integer.valueOf(paramInt3);
      return;
      if ((!l) && (paramInt3 < a.intValue())) {
        b(true);
      }
    }
  }
}
