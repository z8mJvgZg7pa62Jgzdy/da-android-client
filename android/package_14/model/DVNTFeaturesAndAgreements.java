package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

public class DVNTFeaturesAndAgreements
  implements Serializable
{
  @SerializedName("agreements")
  private List<String> agreements;
  @SerializedName("features")
  private List<String> features;
  
  public DVNTFeaturesAndAgreements() {}
  
  public List getAgreements()
  {
    return agreements;
  }
  
  public List getFeatures()
  {
    return features;
  }
  
  public DVNTFeaturesAndAgreements setAgreements(List paramList)
  {
    agreements = paramList;
    return this;
  }
  
  public DVNTFeaturesAndAgreements setFeatures(List paramList)
  {
    features = paramList;
    return this;
  }
}
