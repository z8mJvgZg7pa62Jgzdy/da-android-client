package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.android.package_14.network.wrapper.DVNTResultsWrapper;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTChallengeProgressionalRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTAbstractDeviation.List>
{
  final String deviationId;
  
  public DVNTChallengeProgressionalRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTAbstractDeviation.List.class);
    deviationId = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTChallengeProgressionalRequestV1 localDVNTChallengeProgressionalRequestV1 = (DVNTChallengeProgressionalRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(deviationId, deviationId).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "challenge_progressional" + deviationId;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(deviationId).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTAbstractDeviation.List sendRequest(String paramString)
  {
    return (com.deviantart.android.android.package_14.model.DVNTAbstractDeviation.List)((DVNTServiceV1)getService()).challengeProgressional(paramString, deviationId).getResults();
  }
}
