package com.deviantart.android.damobile.stream.listener;

import com.deviantart.android.damobile.stream.loader.StreamLoader.ErrorType;
import java.util.ArrayList;

public abstract class StreamLoadListener<T>
{
  long curPos;
  
  protected StreamLoadListener(long paramLong)
  {
    curPos = paramLong;
  }
  
  public abstract void a(StreamLoader.ErrorType paramErrorType, String paramString);
  
  public void a(ArrayList paramArrayList, boolean paramBoolean, Integer paramInteger)
  {
    a(paramArrayList, paramBoolean, paramInteger, false, null);
  }
  
  public abstract void a(ArrayList paramArrayList, boolean paramBoolean1, Integer paramInteger1, boolean paramBoolean2, Integer paramInteger2);
  
  public long position()
  {
    return curPos;
  }
}
