package com.deviantart.android.damobile.stream;

import android.content.Context;
import android.util.Log;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.stream.loader.StreamLoader.ErrorType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

public class Stream<T>
  implements Serializable
{
  private static int id = 3;
  private static int type = 3;
  protected ArrayList<T> a = new ArrayList();
  private Integer b;
  private boolean c = true;
  private boolean d = false;
  private int e = 0;
  private StreamLoader<T> f;
  private int g = 0;
  private Integer h;
  private int i = 0;
  private boolean k = false;
  private long l = new Random().nextLong();
  private boolean m = false;
  private int p = 0;
  private boolean s = true;
  
  public Stream()
  {
    s = false;
    c = false;
  }
  
  public Stream(StreamLoader paramStreamLoader)
  {
    f = paramStreamLoader;
  }
  
  private void close(Stream.Notifiable paramNotifiable)
  {
    if (paramNotifiable == null)
    {
      Log.e("stream", l + " read - notifyDataSetChanged - adapter is null");
      return;
    }
    paramNotifiable.c();
  }
  
  private void close(boolean paramBoolean, Stream.Notifiable paramNotifiable)
  {
    d = paramBoolean;
    if (paramNotifiable == null) {
      return;
    }
    if (paramBoolean)
    {
      paramNotifiable.e();
      return;
    }
    paramNotifiable.f();
  }
  
  private void d(Stream.Notifiable paramNotifiable)
  {
    m = true;
    if (paramNotifiable != null) {
      paramNotifiable.b();
    }
  }
  
  private void mark(StreamLoader.ErrorType paramErrorType, String paramString, Stream.Notifiable paramNotifiable)
  {
    paramNotifiable.b(paramErrorType, paramString);
  }
  
  public int a(Object paramObject)
  {
    if ((a == null) || (a.isEmpty())) {
      return -1;
    }
    return a.indexOf(paramObject);
  }
  
  public void a()
  {
    a.clear();
    e = 0;
    i = 0;
    h = null;
    b = null;
    k = false;
    c = true;
    s = true;
    p = 0;
    g = 0;
  }
  
  public void a(int paramInt)
  {
    e = paramInt;
  }
  
  public void a(int paramInt, Object paramObject)
  {
    a.add(paramInt, paramObject);
  }
  
  public void a(Context paramContext, Stream.Notifiable paramNotifiable)
  {
    a(paramContext, paramNotifiable, false, false);
  }
  
  public void a(Context paramContext, Stream.Notifiable paramNotifiable, boolean paramBoolean1, boolean paramBoolean2)
  {
    Log.d("stream", l + " read - start");
    if (paramBoolean1)
    {
      k = false;
      c = true;
      s = true;
      p = 0;
      g = 0;
      h = null;
      b = null;
    }
    if ((paramBoolean2) && (b == null)) {
      return;
    }
    if (d)
    {
      Log.d("stream", l + " read - already loading, returning");
      return;
    }
    if (f == null)
    {
      Log.e("stream", l + " read - no loader defined, returning");
      return;
    }
    if (k)
    {
      if (a.size() == 0) {
        d(paramNotifiable);
      }
    }
    else
    {
      if (get().size() > i) {
        i = get().size();
      }
      close(true, paramNotifiable);
      int j;
      long l1;
      if (paramBoolean1)
      {
        j = 0;
        l1 = l;
        if (h != null)
        {
          j = h.intValue();
          i = h.intValue();
          h = null;
        }
        if ((b == null) || (!paramBoolean2)) {
          break label338;
        }
        j = b.intValue();
      }
      label338:
      for (;;)
      {
        Log.d("stream", l + " read - load");
        f.saveSession(paramContext, j, paramBoolean1, new Stream.2(this, l1, l1, paramNotifiable, paramBoolean1, paramContext, paramBoolean2));
        return;
        j = i;
        break;
      }
    }
  }
  
  public void a(Collection paramCollection)
  {
    if (paramCollection == null) {
      return;
    }
    a.addAll(paramCollection);
    i += paramCollection.size();
  }
  
  public void a(boolean paramBoolean)
  {
    s = paramBoolean;
  }
  
  public void add(Context paramContext, Stream.Notifiable paramNotifiable, boolean paramBoolean)
  {
    a(paramContext, paramNotifiable, paramBoolean, false);
  }
  
  public void c()
  {
    k = true;
    s = false;
    c = false;
  }
  
  public Object close(int paramInt)
  {
    if (paramInt >= a.size()) {
      return null;
    }
    return a.remove(paramInt);
  }
  
  public void close()
  {
    d = false;
  }
  
  public void close(Context paramContext, Stream.Notifiable paramNotifiable, int paramInt)
  {
    if (length() == null) {
      return;
    }
    a(paramContext, new Stream.1(this, paramInt, paramContext, paramNotifiable));
  }
  
  public void close(Object paramObject)
  {
    a.add(paramObject);
    i += 1;
  }
  
  public void close(boolean paramBoolean)
  {
    c = paramBoolean;
  }
  
  public boolean d()
  {
    return m;
  }
  
  public int e()
  {
    return e;
  }
  
  public boolean equals()
  {
    return k;
  }
  
  public Object get(int paramInt)
  {
    if (paramInt >= size())
    {
      Log.e("Runtime", "Asked for stream item outside of bounds");
      return null;
    }
    return a.get(paramInt);
  }
  
  public ArrayList get()
  {
    return a;
  }
  
  public long getValue()
  {
    return l;
  }
  
  public boolean iterator()
  {
    return c;
  }
  
  public StreamLoader length()
  {
    return f;
  }
  
  public void next()
  {
    Log.d("stream", "old id - " + l);
    l = new Random().nextLong();
    Log.d("stream", "new id - " + l);
  }
  
  public boolean read()
  {
    return s;
  }
  
  public boolean remove(Object paramObject)
  {
    return a.remove(paramObject);
  }
  
  public int size()
  {
    return a.size();
  }
}
