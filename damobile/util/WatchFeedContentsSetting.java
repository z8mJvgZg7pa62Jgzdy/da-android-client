package com.deviantart.android.damobile.util;

import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil.EventLabelBuilder;
import java.io.Serializable;

public class WatchFeedContentsSetting
  implements Serializable
{
  private boolean a;
  private final boolean b;
  private final boolean c;
  private boolean d;
  private final boolean h;
  private boolean i;
  private boolean k;
  private boolean l;
  private EventKeys.Category logs;
  private final boolean o;
  private final boolean s;
  
  public WatchFeedContentsSetting(EventKeys.Category paramCategory, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5)
  {
    logs = paramCategory;
    b = paramBoolean1;
    c = paramBoolean2;
    o = paramBoolean4;
    s = paramBoolean5;
    h = paramBoolean3;
    k = paramBoolean1;
    l = paramBoolean2;
    a = paramBoolean4;
    i = paramBoolean5;
    d = paramBoolean3;
  }
  
  private TrackerUtil.EventLabelBuilder a(TrackerUtil.EventLabelBuilder paramEventLabelBuilder, String paramString, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramEventLabelBuilder.get(paramString, "On");
      return paramEventLabelBuilder;
    }
    paramEventLabelBuilder.get(paramString, "Off");
    return paramEventLabelBuilder;
  }
  
  public void a(boolean paramBoolean)
  {
    d = paramBoolean;
  }
  
  public boolean a()
  {
    return l;
  }
  
  public void b(boolean paramBoolean)
  {
    a = paramBoolean;
  }
  
  public boolean b()
  {
    return (b != k) || (c != l) || (o != a) || (s != i) || (h != d);
  }
  
  public void c(boolean paramBoolean)
  {
    k = paramBoolean;
  }
  
  public boolean c()
  {
    return k;
  }
  
  public String d()
  {
    Object localObject2 = new TrackerUtil.EventLabelBuilder();
    Object localObject1 = localObject2;
    if (b != k) {
      localObject1 = a((TrackerUtil.EventLabelBuilder)localObject2, "Status", k);
    }
    localObject2 = localObject1;
    if (c != l) {
      localObject2 = a((TrackerUtil.EventLabelBuilder)localObject1, "Deviations", l);
    }
    localObject1 = localObject2;
    if (h != d) {
      localObject1 = a((TrackerUtil.EventLabelBuilder)localObject2, "Collections", d);
    }
    localObject2 = localObject1;
    if (o != a) {
      localObject2 = a((TrackerUtil.EventLabelBuilder)localObject1, "Journal", a);
    }
    localObject1 = localObject2;
    if (s != i) {
      localObject1 = a((TrackerUtil.EventLabelBuilder)localObject2, "Groups", i);
    }
    return ((TrackerUtil.EventLabelBuilder)localObject1).getValue();
  }
  
  public void d(boolean paramBoolean)
  {
    i = paramBoolean;
  }
  
  public void f(boolean paramBoolean)
  {
    l = paramBoolean;
  }
  
  public boolean f()
  {
    return i;
  }
  
  public EventKeys.Category getLogs()
  {
    return logs;
  }
  
  public boolean getValue()
  {
    return a;
  }
  
  public boolean hasNext()
  {
    return (k) || (l) || (a) || (d) || (i);
  }
  
  public boolean q()
  {
    return d;
  }
}
