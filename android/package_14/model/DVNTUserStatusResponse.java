package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTUserStatusResponse
  implements Serializable
{
  @SerializedName("has_more")
  boolean hasMore;
  @SerializedName("results")
  DVNTUserStatus.List items;
  @SerializedName("next_offset")
  int nextOffset;
  
  public DVNTUserStatusResponse() {}
  
  public boolean getHasMore()
  {
    return hasMore;
  }
  
  public DVNTUserStatus.List getItems()
  {
    return items;
  }
  
  public int getNextOffset()
  {
    return nextOffset;
  }
  
  public boolean isHasMore()
  {
    return hasMore;
  }
  
  public void setHasMore(boolean paramBoolean)
  {
    hasMore = paramBoolean;
  }
  
  public void setItems(DVNTUserStatus.List paramList)
  {
    items = paramList;
  }
  
  public void setNextOffset(int paramInt)
  {
    nextOffset = paramInt;
  }
}
