package com.deviantart.android.damobile.view;

import android.view.GestureDetector;
import android.view.View;
import com.deviantart.android.damobile.util.ProcessMenuListener;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class LongPressItem
{
  private LongPressItem.onSimpleTapListener j;
  private ArrayList<MenuButton> m;
  private LongPressItem.TorpedoThumbGestureListener o = new LongPressItem.TorpedoThumbGestureListener(this, null);
  private WeakReference<View> v;
  
  public LongPressItem() {}
  
  public static LongPressItem a(View paramView, ArrayList paramArrayList)
  {
    LongPressItem localLongPressItem = new LongPressItem();
    localLongPressItem.add(new WeakReference(paramView));
    paramView.setOnTouchListener(new LongPressItem.1(paramArrayList, new GestureDetector(paramView.getContext(), localLongPressItem.a())));
    paramView.setLongClickable(false);
    paramView.setOnLongClickListener(new LongPressItem.2());
    m = paramArrayList;
    return localLongPressItem;
  }
  
  public LongPressItem.TorpedoThumbGestureListener a()
  {
    return o;
  }
  
  public void add(WeakReference paramWeakReference)
  {
    v = paramWeakReference;
  }
  
  public View b()
  {
    if (v == null) {
      return null;
    }
    return (View)v.get();
  }
  
  public void b(ProcessMenuListener paramProcessMenuListener)
  {
    if (m != null)
    {
      if (m.isEmpty()) {
        return;
      }
      Iterator localIterator = m.iterator();
      while (localIterator.hasNext())
      {
        MenuButton localMenuButton = (MenuButton)localIterator.next();
        if (localMenuButton != null) {
          localMenuButton.setProcessMenuListener(paramProcessMenuListener);
        }
      }
    }
  }
  
  public void b(LongPressItem.onSimpleTapListener paramOnSimpleTapListener)
  {
    j = paramOnSimpleTapListener;
  }
}
