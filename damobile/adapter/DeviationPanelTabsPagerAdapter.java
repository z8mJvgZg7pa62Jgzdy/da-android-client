package com.deviantart.android.damobile.adapter;

import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation.DVNTDeviationBaseStats;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.DeviationCompositeModel;
import com.deviantart.android.damobile.util.deviation.DeviationPanelTab;
import com.deviantart.android.damobile.view.viewpageindicator.DAPagerAdapter;
import com.deviantart.datoolkit.logger.DVNTLog;

public class DeviationPanelTabsPagerAdapter
  extends DAPagerAdapter
{
  private DeviationCompositeModel a;
  private boolean d = false;
  private String g;
  
  public DeviationPanelTabsPagerAdapter() {}
  
  public void a(String paramString)
  {
    g = paramString;
  }
  
  public void a(boolean paramBoolean)
  {
    d = paramBoolean;
  }
  
  public View b(ViewGroup paramViewGroup, int paramInt)
  {
    throw new Runtime("d2j fail translate: java.lang.RuntimeException: fail exe a5 = a4\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.exec(BaseAnalyze.java:92)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.exec(BaseAnalyze.java:1)\n\tat com.googlecode.dex2jar.ir.ts.Cfg.dfs(Cfg.java:255)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.analyze0(BaseAnalyze.java:75)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.analyze(BaseAnalyze.java:69)\n\tat com.googlecode.dex2jar.ir.ts.UnSSATransformer.transform(UnSSATransformer.java:274)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:163)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:414)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:128)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:509)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:406)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:422)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:172)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:272)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:108)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:288)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:32)\nCaused by: java.lang.NullPointerException\n\tat com.googlecode.dex2jar.ir.ts.UnSSATransformer$LiveA.onUseLocal(UnSSATransformer.java:552)\n\tat com.googlecode.dex2jar.ir.ts.UnSSATransformer$LiveA.onUseLocal(UnSSATransformer.java:1)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.onUse(BaseAnalyze.java:166)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.onUse(BaseAnalyze.java:1)\n\tat com.googlecode.dex2jar.ir.ts.Cfg.travel(Cfg.java:331)\n\tat com.googlecode.dex2jar.ir.ts.Cfg.travel(Cfg.java:387)\n\tat com.googlecode.dex2jar.ir.ts.an.BaseAnalyze.exec(BaseAnalyze.java:90)\n\t... 17 more\n");
  }
  
  public CharSequence b(int paramInt)
  {
    DeviationPanelTab localDeviationPanelTab = DeviationPanelTab.get(paramInt);
    if (localDeviationPanelTab == null) {
      return null;
    }
    return localDeviationPanelTab.b();
  }
  
  public void destroyItem(ViewGroup paramViewGroup, int paramInt, Object paramObject)
  {
    paramViewGroup.removeView((View)paramObject);
  }
  
  public int getCount()
  {
    return DeviationPanelTab.values().length;
  }
  
  public int getIcon(int paramInt)
  {
    switch (DeviationPanelTabsPagerAdapter.1.a[DeviationPanelTab.get(paramInt).ordinal()])
    {
    default: 
      DVNTLog.append("Don't know the icon type, using default", new Object[0]);
    case 1: 
      return 2130837663;
    case 2: 
      return 2130837660;
    case 3: 
      if ((a != null) && (a.getItem() != null) && (a.getItem().isFavourited() != null) && (a.getItem().isFavourited().booleanValue())) {
        return 2130837662;
      }
      return 2130837661;
    }
    return 2130837664;
  }
  
  public int getItemPosition(Object paramObject)
  {
    return -2;
  }
  
  public CharSequence getPageTitle(int paramInt)
  {
    if ((a == null) || (a.getItem() == null) || (a.getItem().getStats() == null))
    {
      DVNTLog.append("Expecting current deviation to be set", new Object[0]);
      return null;
    }
    DVNTAbstractDeviation.DVNTDeviationBaseStats localDVNTDeviationBaseStats = a.getItem().getStats();
    switch (DeviationPanelTabsPagerAdapter.1.a[DeviationPanelTab.get(paramInt).ordinal()])
    {
    default: 
      return null;
    case 2: 
      if (localDVNTDeviationBaseStats.getComments() != null) {
        return " " + DAFormatUtils.format(localDVNTDeviationBaseStats.getComments());
      }
      break;
    case 3: 
      if (localDVNTDeviationBaseStats.getFavourites() != null) {
        return " " + DAFormatUtils.format(localDVNTDeviationBaseStats.getFavourites());
      }
      break;
    }
    return null;
  }
  
  public boolean isViewFromObject(View paramView, Object paramObject)
  {
    return paramView == paramObject;
  }
  
  public void setColor(DeviationCompositeModel paramDeviationCompositeModel)
  {
    a = paramDeviationCompositeModel;
  }
}
