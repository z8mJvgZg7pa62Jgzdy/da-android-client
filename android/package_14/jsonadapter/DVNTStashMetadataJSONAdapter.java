package com.deviantart.android.android.package_14.jsonadapter;

import com.deviantart.android.sdk.api.model.DVNTStashItem;
import com.deviantart.android.sdk.api.model.DVNTStashStackMetadata;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;

public class DVNTStashMetadataJSONAdapter
  implements JsonDeserializer<com.deviantart.android.sdk.api.model.DVNTStashMetadata>, JsonSerializer<com.deviantart.android.sdk.api.model.DVNTStashMetadata>
{
  public DVNTStashMetadataJSONAdapter() {}
  
  public com.deviantart.android.android.package_14.model.DVNTStashMetadata deserialize(JsonElement paramJsonElement, Type paramType, JsonDeserializationContext paramJsonDeserializationContext)
  {
    if (paramJsonElement.getAsJsonObject().add("size")) {
      return (com.deviantart.android.android.package_14.model.DVNTStashMetadata)paramJsonDeserializationContext.deserialize(paramJsonElement, DVNTStashStackMetadata.class);
    }
    return (com.deviantart.android.android.package_14.model.DVNTStashMetadata)paramJsonDeserializationContext.deserialize(paramJsonElement, DVNTStashItem.class);
  }
  
  public JsonElement serialize(com.deviantart.android.android.package_14.model.DVNTStashMetadata paramDVNTStashMetadata, Type paramType, JsonSerializationContext paramJsonSerializationContext)
  {
    return paramJsonSerializationContext.serialize(paramDVNTStashMetadata);
  }
}
