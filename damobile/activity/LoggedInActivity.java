package com.deviantart.android.damobile.activity;

import android.app.Activity;
import android.os.Bundle;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;

public abstract class LoggedInActivity
  extends BaseActivity
{
  public LoggedInActivity() {}
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (DVNTAbstractAsyncAPI.isUserSession(this)) {
      DVNTAbstractAsyncAPI.graduate(this);
    }
  }
  
  public void selectItem()
  {
    super.selectItem();
    if (!DVNTAbstractAsyncAPI.isUserSession(this)) {
      finish();
    }
  }
}
