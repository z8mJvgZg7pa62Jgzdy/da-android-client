package com.deviantart.android.damobile.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.BaseBundle;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTAccountValidateResponse;
import com.deviantart.android.damobile.activity.MultiStepActivity;
import com.deviantart.android.damobile.util.Keyboard;
import com.deviantart.android.damobile.util.SignUpHelper;
import com.deviantart.android.damobile.util.SignUpHelper.APIValidationType;
import com.deviantart.android.damobile.util.SignUpHelper.ValidateUserInfoTask;
import com.deviantart.android.damobile.util.SignUpHelper.ValidationListener;
import com.deviantart.android.damobile.view.EditTextWithClearFocus;

public class SignUpLoginInfoFragment
  extends MultiStepFragment
  implements SignUpHelper.ValidationListener
{
  private SignUpHelper.ValidateUserInfoTask c;
  private SignUpHelper.ValidateUserInfoTask f;
  @Bind({2131689779})
  LinearLayout mainLayout;
  @Bind({2131689787})
  EditTextWithClearFocus passwordEditText;
  @Bind({2131689789})
  TextView passwordError;
  @Bind({2131689788})
  ProgressBar passwordProgressBar;
  @Bind({2131689786})
  TextView passwordValidator;
  @Bind({2131689781})
  ImageView signUpNextButton;
  @Bind({2131689783})
  EditTextWithClearFocus userNameEditText;
  @Bind({2131689785})
  TextView userNameError;
  @Bind({2131689784})
  ProgressBar userNameProgressBar;
  @Bind({2131689782})
  TextView userNameValidator;
  
  public SignUpLoginInfoFragment() {}
  
  private void a(CharSequence paramCharSequence, SignUpHelper.APIValidationType paramAPIValidationType, boolean paramBoolean)
  {
    TextView localTextView1;
    TextView localTextView2;
    ProgressBar localProgressBar1;
    SignUpHelper.APIValidationType localAPIValidationType;
    TextView localTextView3;
    EditTextWithClearFocus localEditTextWithClearFocus;
    TextView localTextView4;
    ProgressBar localProgressBar2;
    switch (SignUpLoginInfoFragment.2.H[paramAPIValidationType.ordinal()])
    {
    default: 
      Log.e("validateLoginFieldText", "Can't handle updated login field text. Unpexpected field type " + paramAPIValidationType);
      return;
    case 1: 
      localObject2 = c;
      localTextView1 = userNameValidator;
      localTextView2 = userNameError;
      localProgressBar1 = userNameProgressBar;
      localAPIValidationType = SignUpHelper.APIValidationType.b;
      localTextView3 = passwordValidator;
      localEditTextWithClearFocus = passwordEditText;
      localTextView4 = passwordError;
      localProgressBar2 = passwordProgressBar;
      localObject1 = paramCharSequence.toString();
    }
    for (String str = null;; str = paramCharSequence.toString())
    {
      if (localObject2 != null) {
        ((AsyncTask)localObject2).cancel(true);
      }
      if (paramCharSequence.length() != 0) {
        break;
      }
      localTextView1.setActivated(false);
      localTextView1.setTextColor(getResources().getColor(17170443));
      localTextView2.setVisibility(8);
      localProgressBar1.setVisibility(8);
      collapse();
      return;
      localObject2 = f;
      localTextView1 = passwordValidator;
      localTextView2 = passwordError;
      localProgressBar1 = passwordProgressBar;
      localAPIValidationType = SignUpHelper.APIValidationType.E;
      localTextView3 = userNameValidator;
      localEditTextWithClearFocus = userNameEditText;
      localTextView4 = userNameError;
      localProgressBar2 = userNameProgressBar;
      localObject1 = null;
    }
    Object localObject2 = getString(2131231405);
    if ((!paramBoolean) && (((String)localObject2).equals(localTextView4.getText().toString())))
    {
      localTextView3.setActivated(false);
      localTextView3.setTextColor(getResources().getColor(17170443));
      localTextView4.setVisibility(8);
      localProgressBar2.setVisibility(8);
      a(localEditTextWithClearFocus.getText().toString(), localAPIValidationType, true);
    }
    if ((!paramBoolean) && (toModel()))
    {
      localProgressBar1.setVisibility(8);
      SignUpHelper.showHelpDialog(getActivity(), localTextView1, localTextView2, (String)localObject2);
      collapse();
      return;
    }
    localProgressBar1.setVisibility(0);
    Object localObject1 = new SignUpLoginInfoFragment.1(this, (String)localObject1, str, paramAPIValidationType);
    ((AsyncTask)localObject1).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[] { paramCharSequence.toString() });
    switch (SignUpLoginInfoFragment.2.H[paramAPIValidationType.ordinal()])
    {
    default: 
      return;
    case 1: 
      c = ((SignUpHelper.ValidateUserInfoTask)localObject1);
      return;
    }
    f = ((SignUpHelper.ValidateUserInfoTask)localObject1);
  }
  
  private void collapse()
  {
    ImageView localImageView = signUpNextButton;
    if ((userNameValidator.isActivated()) && (passwordValidator.isActivated())) {}
    for (boolean bool = true;; bool = false)
    {
      localImageView.setActivated(bool);
      return;
    }
  }
  
  private boolean d()
  {
    return mainLayout == null;
  }
  
  private boolean toModel()
  {
    return userNameEditText.getText().toString().equals(passwordEditText.getText().toString());
  }
  
  public void clickNextButton()
  {
    if (!signUpNextButton.isActivated()) {
      return;
    }
    if (Keyboard.processRequest(mainLayout)) {
      Keyboard.hideKeyboard(getActivity());
    }
    ((MultiStepActivity)getActivity()).onOptionsItemSelected();
  }
  
  public void close()
  {
    getActivity().finish();
  }
  
  public boolean editTextEditorAction(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 6)
    {
      Keyboard.hideKeyboard(getActivity());
      mainLayout.requestFocus();
    }
    return false;
  }
  
  public void focusChangePasswordEditText(boolean paramBoolean)
  {
    SignUpHelper.onItemSelected(paramBoolean, passwordEditText, passwordValidator, 2131231186);
  }
  
  public void focusChangeUserNameEditText(boolean paramBoolean)
  {
    SignUpHelper.onItemSelected(paramBoolean, userNameEditText, userNameValidator, 2131231404);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setRetainInstance(true);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = (ScrollView)paramLayoutInflater.inflate(2130968678, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    mainLayout.requestFocus();
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    if (c != null)
    {
      c.cancel(true);
      c = null;
    }
    if (f != null)
    {
      f.cancel(true);
      f = null;
    }
    ButterKnife.unbind(this);
  }
  
  public void passwordTextChanged(CharSequence paramCharSequence)
  {
    a(paramCharSequence, SignUpHelper.APIValidationType.b, false);
  }
  
  public void setVideo(DVNTAccountValidateResponse paramDVNTAccountValidateResponse, SignUpHelper.APIValidationType paramAPIValidationType)
  {
    if (d()) {
      return;
    }
    switch (SignUpLoginInfoFragment.2.H[paramAPIValidationType.ordinal()])
    {
    default: 
      Log.e("SignUp", "Unhandled account validation type " + paramAPIValidationType);
      return;
    case 1: 
      userNameProgressBar.setVisibility(8);
      SignUpHelper.showHelpDialog(getActivity(), userNameValidator, userNameError, paramDVNTAccountValidateResponse.getUserNameValidationResult());
    }
    for (;;)
    {
      collapse();
      return;
      passwordProgressBar.setVisibility(8);
      SignUpHelper.showHelpDialog(getActivity(), passwordValidator, passwordError, paramDVNTAccountValidateResponse.getPasswordValidationResult());
    }
  }
  
  public Bundle updateSettings()
  {
    if (d()) {
      return null;
    }
    Bundle localBundle = new Bundle();
    localBundle.putString("username", userNameEditText.getText().toString());
    localBundle.putString("password", passwordEditText.getText().toString());
    return localBundle;
  }
  
  public void userNameTextChanged(CharSequence paramCharSequence)
  {
    a(paramCharSequence, SignUpHelper.APIValidationType.E, false);
  }
}
