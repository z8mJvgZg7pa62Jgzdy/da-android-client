package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTBrowseTagsRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse>
{
  final Boolean challengesOnly;
  final Integer limit;
  final Integer offset;
  final String source;
  
  public DVNTBrowseTagsRequestV1(String paramString, Integer paramInteger1, Integer paramInteger2, Boolean paramBoolean)
  {
    super(com.deviantart.android.sdk.api.model.DVNTPaginatedResultResponse.class);
    source = paramString;
    offset = paramInteger1;
    limit = paramInteger2;
    challengesOnly = paramBoolean;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTBrowseTagsRequestV1 localDVNTBrowseTagsRequestV1 = (DVNTBrowseTagsRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(source, source).append(offset, offset).append(limit, limit).append(challengesOnly, challengesOnly).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "browsetag" + source + offset + limit;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(source).append(offset).append(limit).append(challengesOnly).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTPaginatedResultResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).browseTag(paramString, source, offset, limit, challengesOnly);
  }
}
