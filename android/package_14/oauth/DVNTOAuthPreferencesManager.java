package com.deviantart.android.android.package_14.oauth;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

public class DVNTOAuthPreferencesManager
{
  public DVNTOAuthPreferencesManager() {}
  
  private SharedPreferences getSharedPreferences(Context paramContext)
  {
    if (paramContext == null) {
      return null;
    }
    return paramContext.getSharedPreferences("Oauth", 0);
  }
  
  public void clearStoredTokenAndCookies(Context paramContext)
  {
    CookieSyncManager.createInstance(paramContext);
    CookieManager.getInstance().removeAllCookie();
    paramContext.getSharedPreferences("Oauth", 0).edit().clear().commit();
  }
  
  public String loadString(Context paramContext, String paramString)
  {
    paramContext = getSharedPreferences(paramContext);
    if (paramContext == null) {
      return null;
    }
    return paramContext.getString(paramString, null);
  }
  
  public boolean removeKey(Context paramContext, String paramString)
  {
    paramContext = getSharedPreferences(paramContext);
    if (paramContext == null) {
      return false;
    }
    return paramContext.edit().remove(paramString).commit();
  }
  
  public boolean saveString(Context paramContext, String paramString1, String paramString2)
  {
    paramContext = getSharedPreferences(paramContext);
    if (paramContext == null) {
      return false;
    }
    return paramContext.edit().putString(paramString1, paramString2).commit();
  }
}
