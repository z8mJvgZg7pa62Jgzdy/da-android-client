package com.deviantart.android.damobile.view;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;

public class WatchButton
  extends AbstractWatchButton
{
  @Bind({2131690129})
  TextView checkMark;
  @Bind({2131690130})
  ProgressBar loadingView;
  @Bind({2131690128})
  TextView watchButton;
  
  public WatchButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    ButterKnife.bind(this);
  }
  
  protected void b(AbstractWatchButton.ButtonState paramButtonState1, AbstractWatchButton.ButtonState paramButtonState2)
  {
    if (j != 0)
    {
      watchButton.setText(getResources().getString(j));
      return;
    }
    watchButton.setText("");
  }
  
  protected int getLayoutRes()
  {
    return 2130968799;
  }
  
  protected void setVisibility()
  {
    loadingView.setVisibility(0);
  }
  
  protected void switchToLoadingView()
  {
    loadingView.setVisibility(8);
  }
}
