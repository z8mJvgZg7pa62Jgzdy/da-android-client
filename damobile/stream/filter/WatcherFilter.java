package com.deviantart.android.damobile.stream.filter;

import android.util.Log;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.util.MemberType;

public class WatcherFilter
  implements StreamFilter<com.deviantart.android.sdk.api.model.DVNTFriend>
{
  public WatcherFilter() {}
  
  public boolean c(com.deviantart.android.android.package_14.model.DVNTFriend paramDVNTFriend)
  {
    try
    {
      Object localObject = paramDVNTFriend.getUser();
      if (localObject == null) {
        return true;
      }
      localObject = paramDVNTFriend.getUser().getType();
      if (localObject != null)
      {
        boolean bool = ((String)localObject).isEmpty();
        if ((!bool) && (MemberType.toString((String)localObject) != null)) {
          return false;
        }
      }
    }
    catch (Exception localException)
    {
      Log.e("WatchersListAdapter", "Error parsing result: " + paramDVNTFriend.getUser());
    }
    return true;
  }
}
