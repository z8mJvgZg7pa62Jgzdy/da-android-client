package com.deviantart.android.android.package_14.model;

import com.deviantart.android.sdk.api.model.DVNTDeviation;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTFeedBucketResponse
  implements Serializable
{
  @SerializedName("has_more")
  boolean hasMore;
  @SerializedName("next_offset")
  private Integer nextOffset;
  @SerializedName("results")
  List<DVNTDeviation> results;
  
  public DVNTFeedBucketResponse() {}
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      paramObject = (DVNTFeedBucketResponse)paramObject;
      return new EqualsBuilder().append(hasMore, hasMore).append(nextOffset, nextOffset).append(results, results).isEquals();
    }
    return false;
  }
  
  public boolean getHasMore()
  {
    return hasMore;
  }
  
  public Integer getNextOffset()
  {
    return nextOffset;
  }
  
  public List getResults()
  {
    return results;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().append(hasMore).append(nextOffset).append(results).toHashCode();
  }
  
  public boolean isHasMore()
  {
    return hasMore;
  }
  
  public void setHasMore(boolean paramBoolean)
  {
    hasMore = paramBoolean;
  }
  
  public void setNextOffset(Integer paramInteger)
  {
    nextOffset = paramInteger;
  }
  
  public void setResults(List paramList)
  {
    results = paramList;
  }
}
