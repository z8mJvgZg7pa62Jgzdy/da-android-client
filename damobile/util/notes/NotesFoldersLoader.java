package com.deviantart.android.damobile.util.notes;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTNotesFolder;
import com.deviantart.android.android.package_14.model.DVNTNotesFolders;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnNotesFoldersUpdated;
import com.deviantart.android.damobile.util.BusStation.OnNotesUnreadCountLoaded;
import com.deviantart.android.damobile.util.SharedPreferenceUtil;
import com.deviantart.android.damobile.util.UserUtils;
import com.squareup.otto.Bus;
import java.util.HashMap;
import java.util.UUID;

public class NotesFoldersLoader
{
  private UUID c = null;
  private boolean d = false;
  
  public NotesFoldersLoader() {}
  
  private void init(Context paramContext, DVNTNotesFolders paramDVNTNotesFolders)
  {
    NotesFolders.onRender(paramDVNTNotesFolders);
    paramDVNTNotesFolders = update(paramContext);
    BusStation.get().post(new BusStation.OnNotesFoldersUpdated());
    if (paramDVNTNotesFolders != null) {
      BusStation.get().post(new BusStation.OnNotesUnreadCountLoaded(paramDVNTNotesFolders.intValue()));
    }
    for (;;)
    {
      SharedPreferenceUtil.get(paramContext).edit().putLong("notes_folders_last_fetch", System.currentTimeMillis()).apply();
      return;
      Log.e("Notes", "Error sending OnNotesUnreadCountLoaded event. Expected Unread folder to exist after response.");
    }
  }
  
  public boolean a()
  {
    return d;
  }
  
  public void b()
  {
    DVNTAbstractAsyncAPI.cancelRequest(c);
    c = null;
  }
  
  public void b(Context paramContext)
  {
    if (!DVNTContextUtils.isContextDead(paramContext))
    {
      if (!UserUtils.e) {
        return;
      }
      if (delete(paramContext)) {
        c(paramContext);
      }
      for (;;)
      {
        d = true;
        return;
        int i = SharedPreferenceUtil.get(paramContext).getInt("notes_last_unread_count", -1);
        BusStation.get().post(new BusStation.OnNotesUnreadCountLoaded(i));
      }
    }
  }
  
  public void b(boolean paramBoolean)
  {
    d = paramBoolean;
  }
  
  public void c(Context paramContext)
  {
    if (!c())
    {
      if (!UserUtils.e) {
        return;
      }
      c = DVNTAsyncAPI.notesFolders().call(paramContext, new NotesFoldersLoader.1(this, paramContext));
    }
  }
  
  public boolean c()
  {
    return c != null;
  }
  
  public boolean delete(Context paramContext)
  {
    long l = SharedPreferenceUtil.get(paramContext).getLong("notes_folders_last_fetch", 0L);
    return System.currentTimeMillis() - l >= 120000L;
  }
  
  public Integer update(Context paramContext)
  {
    if (!NotesFolders.get().containsKey("2E23AB5F-02FC-0EAD-F27A-BD5A251529FE")) {
      return null;
    }
    int i = ((DVNTNotesFolder)NotesFolders.get().get("2E23AB5F-02FC-0EAD-F27A-BD5A251529FE")).getCount().intValue();
    SharedPreferenceUtil.get(paramContext).edit().putInt("notes_last_unread_count", i).apply();
    return Integer.valueOf(i);
  }
}
