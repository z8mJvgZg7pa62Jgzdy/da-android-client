package com.deviantart.android.damobile.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.PointF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.AbstractDraweeControllerBuilder;
import com.facebook.drawee.drawable.ScalingUtils.ScaleType;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.DraweeView;
import com.facebook.drawee.view.GenericDraweeView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ImageUtils
{
  public static GalleryType i = GalleryType.i;
  
  public ImageUtils() {}
  
  public static View a(Activity paramActivity, ViewGroup paramViewGroup, boolean paramBoolean, DVNTImage paramDVNTImage1, DVNTImage paramDVNTImage2)
  {
    SimpleDraweeView localSimpleDraweeView = new SimpleDraweeView(paramActivity);
    if (DVNTContextUtils.isContextDead(paramActivity)) {
      return localSimpleDraweeView;
    }
    if ((paramBoolean) && (!UserUtils.get(paramActivity)))
    {
      localSimpleDraweeView.setAspectRatio(1.0F);
      a(paramActivity, localSimpleDraweeView, 2130837731);
      return localSimpleDraweeView;
    }
    if (a(paramDVNTImage1)) {
      return onCreateView(paramActivity, paramViewGroup, paramDVNTImage1);
    }
    localSimpleDraweeView.setAspectRatio(paramDVNTImage1.getWidth() / paramDVNTImage1.getHeight());
    localSimpleDraweeView.setHierarchy(new GenericDraweeHierarchyBuilder(paramActivity.getResources()).a(100).a(new ColorDrawable(paramActivity.getResources().getColor(2131558499))).a(ScalingUtils.ScaleType.I).a(new PointF(0.5F, 0.0F)).c());
    paramActivity = (PipelineDraweeControllerBuilder)((PipelineDraweeControllerBuilder)((PipelineDraweeControllerBuilder)Fresco.b().a(localSimpleDraweeView.getController())).a(true)).a(ImageRequestBuilder.write(Uri.parse(paramDVNTImage1.getSrc())).c(true).a());
    if ((paramDVNTImage2 != null) && (!Graphics.setIcon(paramDVNTImage1.getSrc()))) {
      paramActivity.b(ImageRequest.fromUri(paramDVNTImage2.getSrc()));
    }
    localSimpleDraweeView.setController(paramActivity.visitAnnotation());
    return localSimpleDraweeView;
  }
  
  public static DVNTImage a(Activity paramActivity, DVNTDeviation paramDVNTDeviation, DVNTImage paramDVNTImage, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    if (paramDVNTDeviation == null) {
      return paramDVNTImage;
    }
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(paramDVNTImage);
    if (paramDVNTDeviation.getPreview() != null) {
      localArrayList.add(paramDVNTDeviation.getPreview());
    }
    if ((paramDVNTDeviation.getThumbs() != null) && (!paramDVNTDeviation.getThumbs().isEmpty())) {
      localArrayList.addAll(paramDVNTDeviation.getThumbs());
    }
    Log.d("thumbs_info", "deviation title : " + paramDVNTDeviation.getTitle());
    return draw(paramActivity, localArrayList, paramDVNTImage, paramInt1, paramInt2, paramBoolean);
  }
  
  public static void a(Context paramContext, GenericDraweeView paramGenericDraweeView, int paramInt)
  {
    onCreateView(paramContext, paramGenericDraweeView);
    a(paramGenericDraweeView, paramInt);
  }
  
  public static void a(GalleryType paramGalleryType)
  {
    Log.d("track", "changed gallery type " + paramGalleryType);
    i = paramGalleryType;
  }
  
  public static void a(GenericDraweeView paramGenericDraweeView, int paramInt)
  {
    b(paramGenericDraweeView, ImageRequestBuilder.send(paramInt).a());
  }
  
  public static boolean a(DVNTImage paramDVNTImage)
  {
    return (paramDVNTImage.getWidth() <= 80) || (paramDVNTImage.getHeight() <= 80);
  }
  
  public static void b(GenericDraweeView paramGenericDraweeView, ImageRequest paramImageRequest)
  {
    paramGenericDraweeView.setController(((PipelineDraweeControllerBuilder)((PipelineDraweeControllerBuilder)Fresco.b().a(true)).a(paramImageRequest)).visitAnnotation());
  }
  
  public static DVNTImage draw(Activity paramActivity, List paramList, DVNTImage paramDVNTImage, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    int j = paramInt1;
    int k = paramInt2;
    if (paramInt1 > 0)
    {
      j = paramInt1;
      k = paramInt2;
      if (paramInt2 > 0)
      {
        f1 = Graphics.toString(paramActivity);
        j = paramInt1;
        k = paramInt2;
        if (f1 > 3.0F)
        {
          j = (int)(paramInt1 * (3.0F / f1));
          f2 = paramInt2;
          k = (int)(3.0F / f1 * f2);
        }
      }
    }
    Log.d("thumbs_info", "retained w/h : " + j + "/" + k);
    if ((j == 0) || (k == 0))
    {
      Log.e("thumbs_info", "NO DIMENSIONS - return default");
      return paramDVNTImage;
    }
    paramActivity = null;
    float f1 = paramDVNTImage.getWidth() / paramDVNTImage.getHeight();
    float f2 = j / k;
    int m;
    if (f1 < f2)
    {
      paramInt2 = 1;
      if (paramBoolean) {
        break label367;
      }
      m = 1;
      label176:
      paramInt1 = j;
      if ((m ^ paramInt2) == 0)
      {
        paramInt1 = j;
        if (paramBoolean) {
          paramInt1 = (int)(j * (f1 / f2));
        }
      }
      double d = Math.ceil(paramInt1 / 100.0D);
      Log.d("thumbs_info", "requested width " + d * 100.0D);
      Iterator localIterator = paramList.iterator();
      for (;;)
      {
        label256:
        if (localIterator.hasNext())
        {
          paramList = (DVNTImage)localIterator.next();
          if (paramList != null)
          {
            f1 = Math.max(paramList.getWidth() / paramInt1, paramList.getHeight() / k);
            if (f1 < 0.5F)
            {
              paramInt2 = 1;
              label314:
              f1 = 1.0F - f1;
              if (paramInt2 == 0) {
                break label379;
              }
              f1 = 10.0F * f1 * f1;
              label336:
              if ((paramActivity != null) && (f1 >= paramActivity.floatValue())) {
                break label476;
              }
              paramDVNTImage = paramList;
              paramActivity = Float.valueOf(f1);
            }
          }
        }
      }
    }
    label367:
    label379:
    label476:
    for (;;)
    {
      break label256;
      paramInt2 = 0;
      break;
      m = 0;
      break label176;
      paramInt2 = 0;
      break label314;
      f1 = Math.abs(f1);
      break label336;
      paramList = "optimal thumb w/h(score): " + paramDVNTImage.getWidth() + "/" + paramDVNTImage.getHeight() + "(" + paramActivity + ")";
      if ((paramActivity != null) && (paramActivity.floatValue() > 0.4D))
      {
        Log.w("thumbs_info", paramList);
        return paramDVNTImage;
      }
      Log.d("thumbs_info", paramList);
      return paramDVNTImage;
    }
  }
  
  public static void isEmpty(GenericDraweeView paramGenericDraweeView, Uri paramUri)
  {
    b(paramGenericDraweeView, ImageRequestBuilder.write(paramUri).c(true).a(true).a());
  }
  
  private static View onCreateView(Activity paramActivity, ViewGroup paramViewGroup, DVNTImage paramDVNTImage)
  {
    paramViewGroup = (LinearLayout)paramActivity.getLayoutInflater().inflate(2130968765, paramViewGroup, false);
    SimpleDraweeView localSimpleDraweeView = (SimpleDraweeView)paramViewGroup.findViewById(2131689920);
    if ((paramDVNTImage.getWidth() < 50) && (paramDVNTImage.getHeight() < 50))
    {
      int j = Float.valueOf(Graphics.toString(paramActivity)).intValue() * 2;
      localSimpleDraweeView.setLayoutParams(new LinearLayout.LayoutParams(paramDVNTImage.getWidth() * j, j * paramDVNTImage.getHeight()));
      localSimpleDraweeView.invalidate();
    }
    localSimpleDraweeView.setAspectRatio(paramDVNTImage.getWidth() / paramDVNTImage.getHeight());
    ((GenericDraweeHierarchy)localSimpleDraweeView.getHierarchy()).c(ScalingUtils.ScaleType.g);
    isEmpty(localSimpleDraweeView, Uri.parse(paramDVNTImage.getSrc()));
    return paramViewGroup;
  }
  
  private static void onCreateView(Context paramContext, GenericDraweeView paramGenericDraweeView)
  {
    paramGenericDraweeView.setHierarchy(new GenericDraweeHierarchyBuilder(paramContext.getResources()).a(100).a(new ColorDrawable(paramContext.getResources().getColor(2131558499))).c());
  }
  
  public static void setImage(Context paramContext, GenericDraweeView paramGenericDraweeView, Uri paramUri)
  {
    onCreateView(paramContext, paramGenericDraweeView);
    isEmpty(paramGenericDraweeView, paramUri);
  }
}
