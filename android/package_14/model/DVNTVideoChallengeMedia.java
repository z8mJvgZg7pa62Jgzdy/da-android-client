package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;

public class DVNTVideoChallengeMedia
  extends DVNTChallengeMedia
{
  @SerializedName("duration")
  Integer duration;
  @SerializedName("filesize")
  Integer filesize;
  
  public DVNTVideoChallengeMedia() {}
  
  public Integer getDuration()
  {
    return duration;
  }
  
  public Integer getFilesize()
  {
    return filesize;
  }
  
  public void setDuration(Integer paramInteger)
  {
    duration = paramInteger;
  }
  
  public void setFilesize(Integer paramInteger)
  {
    filesize = paramInteger;
  }
}
