package com.deviantart.android.damobile.util.markup;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.android.package_14.model.DVNTStashItem;
import com.deviantart.android.android.package_14.model.DVNTStashMetadata;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.DAClickableSpan;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.ProcessMenuListener;
import com.deviantart.android.damobile.util.URLImageParser;
import com.deviantart.android.damobile.view.LongPressItem;
import com.facebook.drawee.view.GenericDraweeView;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

public class MarkupHelper
{
  private static final Pattern path = Pattern.compile("@mention:([a-zA-Z0-9\\-]+)");
  
  public MarkupHelper() {}
  
  static boolean a(Activity paramActivity, TextView paramTextView, DVNTStashItem paramDVNTStashItem)
  {
    if (paramDVNTStashItem == null) {
      return false;
    }
    Long localLong = paramDVNTStashItem.getItemId();
    paramDVNTStashItem = paramDVNTStashItem.getTitle();
    if ((localLong != null) && (paramDVNTStashItem != null))
    {
      paramTextView.setOnClickListener(new MarkupHelper.4(localLong));
      paramTextView.setText(paramDVNTStashItem);
      paramTextView.setTextColor(paramActivity.getResources().getColor(2131558484));
      return true;
    }
    return false;
  }
  
  static boolean a(Activity paramActivity, SimpleDraweeView paramSimpleDraweeView, DVNTDeviation paramDVNTDeviation, Stream paramStream, int paramInt, ProcessMenuListener paramProcessMenuListener)
  {
    int i = 0;
    Boolean localBoolean = DVNTAbstractAsyncAPI.getConfig().getShowMatureContent();
    DVNTImage localDVNTImage = DeviationUtils.b(paramDVNTDeviation);
    if (localDVNTImage == null) {
      return false;
    }
    paramSimpleDraweeView.setOnClickListener(new MarkupHelper.2(paramStream, paramInt, paramActivity));
    paramInt = i;
    if (paramDVNTDeviation.isMature().booleanValue())
    {
      paramInt = i;
      if (!localBoolean.booleanValue()) {
        paramInt = 1;
      }
    }
    if (paramInt != 0)
    {
      paramStream = "https://st.deviantart.net/misc/noentrythumb.gif";
      paramSimpleDraweeView.setAspectRatio(1.0F);
    }
    for (;;)
    {
      update(paramActivity, paramSimpleDraweeView, paramStream);
      LongPressItem.a(paramSimpleDraweeView, DeviationUtils.b(paramActivity, paramDVNTDeviation)).b(DeviationUtils.b(paramProcessMenuListener));
      return true;
      paramStream = localDVNTImage.getSrc();
      paramSimpleDraweeView.setAspectRatio(localDVNTImage.getWidth() / localDVNTImage.getHeight());
    }
  }
  
  static boolean a(Activity paramActivity, SimpleDraweeView paramSimpleDraweeView, DVNTStashItem paramDVNTStashItem)
  {
    if (paramDVNTStashItem == null) {
      return false;
    }
    Long localLong = paramDVNTStashItem.getItemId();
    paramDVNTStashItem = DeviationUtils.a(paramDVNTStashItem);
    if ((localLong != null) && (paramDVNTStashItem != null))
    {
      paramSimpleDraweeView.setOnClickListener(new MarkupHelper.3(localLong));
      paramSimpleDraweeView.setAspectRatio(paramDVNTStashItem.getWidth() / paramDVNTStashItem.getHeight());
      update(paramActivity, paramSimpleDraweeView, paramDVNTStashItem.getSrc());
      return true;
    }
    return false;
  }
  
  static boolean b(Activity paramActivity, SimpleDraweeView paramSimpleDraweeView, DVNTDeviation paramDVNTDeviation, Stream paramStream, int paramInt)
  {
    return a(paramActivity, paramSimpleDraweeView, paramDVNTDeviation, paramStream, paramInt, null);
  }
  
  private static SpannableStringBuilder doInBackground(Activity paramActivity, Spanned paramSpanned)
  {
    String str = paramSpanned.toString();
    SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(paramSpanned);
    for (paramSpanned = path.matcher(str); paramSpanned.find(); paramSpanned = path.matcher(paramSpanned))
    {
      str = paramSpanned.group(1);
      localSpannableStringBuilder.setSpan(new DAClickableSpan(paramActivity.getResources().getColor(2131558425), new MarkupHelper.1(paramActivity, str)), paramSpanned.start(), paramSpanned.end(), 17);
      localSpannableStringBuilder.replace(paramSpanned.start(), paramSpanned.end(), str);
      paramSpanned = paramSpanned.replaceFirst(str);
    }
    paramActivity = (URLSpan[])localSpannableStringBuilder.getSpans(0, localSpannableStringBuilder.length(), URLSpan.class);
    int j = paramActivity.length;
    int i = 0;
    while (i < j)
    {
      paramSpanned = paramActivity[i];
      int k = localSpannableStringBuilder.getSpanStart(paramSpanned);
      int m = localSpannableStringBuilder.getSpanEnd(paramSpanned);
      localSpannableStringBuilder.removeSpan(paramSpanned);
      localSpannableStringBuilder.setSpan(new MarkupHelper.URLSpanNoUnderline(paramSpanned.getURL()), k, m, 0);
      i += 1;
    }
    return localSpannableStringBuilder;
  }
  
  private static Document loadInBackground(Document paramDocument)
  {
    Object localObject1 = paramDocument.select("span[data-embed-type=deviation]");
    if (localObject1 != null)
    {
      if (((Elements)localObject1).isEmpty()) {
        return paramDocument;
      }
      localObject1 = ((Elements)localObject1).iterator();
      break label64;
      break label64;
      label25:
      Element localElement;
      do
      {
        if (!((Iterator)localObject1).hasNext()) {
          break;
        }
        localElement = (Element)((Iterator)localObject1).next();
        localObject2 = localElement.getElementsByTag("a");
      } while (((Elements)localObject2).isEmpty());
      Object localObject2 = ((Elements)localObject2).iterator();
      for (;;)
      {
        label64:
        if (!((Iterator)localObject2).hasNext()) {
          break label25;
        }
        Object localObject3 = (Element)((Iterator)localObject2).next();
        if ((((Node)localObject3).attr("class") == null) || (!((Node)localObject3).attr("class").contains("lit"))) {
          break;
        }
        Object localObject4 = localElement.getElementsByTag("img");
        if (!((Elements)localObject4).isEmpty()) {
          ((Elements)localObject4).append();
        }
        localObject3 = ((Element)localObject3).getElementsByClass("wrap");
        if (((Elements)localObject3).isEmpty()) {
          break;
        }
        localObject3 = ((Elements)localObject3).iterator();
        while (((Iterator)localObject3).hasNext())
        {
          localObject4 = (Element)((Iterator)localObject3).next();
          Elements localElements = ((Element)localObject4).getElementsByTag("strong");
          if ((localElements == null) || (localElements.isEmpty()))
          {
            Log.e("TextHelper", "lit thumb with no title");
            ((Node)localObject4).append();
          }
          else
          {
            ((Node)localObject4).replaceWith(new TextNode(localElements.equals(0).get(), ""));
          }
        }
      }
    }
    return paramDocument;
  }
  
  static Document onPostExecute(Document paramDocument)
  {
    Object localObject1 = paramDocument.select("span[data-embed-type=deviation]");
    label19:
    Object localObject2;
    if (!((Elements)localObject1).isEmpty())
    {
      localObject1 = ((Elements)localObject1).iterator();
      break label56;
      break label56;
      do
      {
        if (!((Iterator)localObject1).hasNext()) {
          break;
        }
        localObject2 = ((Element)((Iterator)localObject1).next()).getElementsByTag("a");
      } while (((Elements)localObject2).isEmpty());
      localObject2 = ((Elements)localObject2).iterator();
      for (;;)
      {
        label56:
        if (!((Iterator)localObject2).hasNext()) {
          break label19;
        }
        Object localObject3 = (Element)((Iterator)localObject2).next();
        if ((((Node)localObject3).attr("class") == null) || (!((Node)localObject3).attr("class").contains("ismature"))) {
          break;
        }
        localObject3 = ((Element)localObject3).getElementsByTag("img");
        if (((Elements)localObject3).isEmpty()) {
          break;
        }
        localObject3 = ((Elements)localObject3).iterator();
        while (((Iterator)localObject3).hasNext())
        {
          Element localElement = (Element)((Iterator)localObject3).next();
          localElement.append("src", "https://st.deviantart.net/misc/noentrythumb.gif");
          localElement.append("data-src", "https://st.deviantart.net/misc/noentrythumb.gif");
        }
      }
    }
    localObject1 = paramDocument.select("a[class^=embedded-deviation]");
    if ((localObject1 != null) && (!((Elements)localObject1).isEmpty()))
    {
      localObject1 = ((Elements)localObject1).iterator();
      while (((Iterator)localObject1).hasNext())
      {
        localObject2 = (Element)((Iterator)localObject1).next();
        if (((Element)localObject2).remove().contains("mature")) {
          ((Element)localObject2).select("img").attr("src", "https://st.deviantart.net/misc/noentrythumb.gif");
        }
      }
    }
    return paramDocument;
  }
  
  public static void onPostExecute(Activity paramActivity, String paramString, TextView paramTextView, boolean paramBoolean)
  {
    paramString = Jsoup.get(paramString);
    Object localObject1 = paramString.select("span[class^=username-with-symbol]").iterator();
    Element localElement1;
    while (((Iterator)localObject1).hasNext())
    {
      localElement1 = (Element)((Iterator)localObject1).next();
      if (!select(localElement1)) {
        localElement1.text("@mention:");
      }
      localElement1.select("a").remove();
      localElement1.select("span").remove();
    }
    if (paramBoolean)
    {
      localObject1 = paramString.select("span[data-embed-type=deviation]");
      if ((localObject1 != null) && (!((Elements)localObject1).isEmpty())) {
        localObject1 = ((Elements)localObject1).iterator();
      }
    }
    label173:
    label392:
    label393:
    for (;;)
    {
      int i;
      if (((Iterator)localObject1).hasNext())
      {
        localElement1 = (Element)((Iterator)localObject1).next();
        Object localObject2 = localElement1.getElementsByTag("a");
        if (((Elements)localObject2).isEmpty())
        {
          localElement1.append();
          continue;
        }
        localObject2 = ((Elements)localObject2).iterator();
        i = 0;
        if (((Iterator)localObject2).hasNext())
        {
          Element localElement2 = (Element)((Iterator)localObject2).next();
          if ((localElement2.attr("class") == null) || (!localElement2.attr("class").contains("lit"))) {
            break label392;
          }
          i = 1;
        }
      }
      for (;;)
      {
        break label173;
        if (i != 0) {
          break label393;
        }
        localElement1.append();
        break;
        localObject1 = paramString.select("a[class^=embedded-deviation]");
        if ((localObject1 != null) && (!((Elements)localObject1).isEmpty())) {
          ((Elements)localObject1).append();
        }
        localObject1 = loadInBackground(paramString);
        paramString = (String)localObject1;
        if (!DVNTAbstractAsyncAPI.getConfig().getShowMatureContent().booleanValue()) {
          paramString = onPostExecute((Document)localObject1);
        }
        paramTextView.setText(doInBackground(paramActivity, Html.fromHtml(Jsoup.clean(paramString.html(), Whitelist.basicWithImages().addProtocols("a", "href", new String[] { "DeviantArt", "deviantart", "DeviantArt-RC", "deviantart-rc", "DeviantArt-Preview", "deviantart-preview" })), new URLImageParser(paramTextView, paramActivity), null)));
        paramTextView.setLinkTextColor(paramActivity.getResources().getColor(2131558484));
        return;
      }
    }
  }
  
  static void replaceAll(Activity paramActivity, View paramView, String paramString)
  {
    paramString = Pattern.compile(paramActivity.getString(2131230844) + ":\\/\\/deviation\\/(.*)").matcher(paramString);
    if ((paramString.find()) && (paramString.groupCount() >= 1)) {
      LongPressItem.a(paramView, DeviationUtils.replaceAll(paramActivity, paramString.group(1)));
    }
  }
  
  private static boolean select(Element paramElement)
  {
    paramElement = paramElement.select("a").iterator();
    while (paramElement.hasNext()) {
      if (((Element)paramElement.next()).remove().contains("group")) {
        return true;
      }
    }
    return false;
  }
  
  static boolean update(Activity paramActivity, SimpleDraweeView paramSimpleDraweeView, String paramString)
  {
    if (paramString != null)
    {
      if (paramString.isEmpty()) {
        return false;
      }
      if (paramSimpleDraweeView.getVisibility() != 0) {
        paramSimpleDraweeView.setVisibility(0);
      }
      ImageUtils.setImage(paramActivity, paramSimpleDraweeView, Uri.parse(paramString));
      return true;
    }
    return false;
  }
}
