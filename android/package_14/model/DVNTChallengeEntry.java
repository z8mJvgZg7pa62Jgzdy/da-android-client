package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTChallengeEntry
  implements Serializable
{
  @SerializedName("challenge")
  DVNTDeviation challenge;
  @SerializedName("challengeid")
  String challengeDeviationId;
  @SerializedName("challenge_title")
  String challengeTitle;
  @SerializedName("submission_time")
  String submissionDate;
  @SerializedName("timed_duration")
  Integer timedDuration;
  
  public DVNTChallengeEntry() {}
  
  public DVNTDeviation getChallenge()
  {
    return challenge;
  }
  
  public String getChallengeDeviationId()
  {
    return challengeDeviationId;
  }
  
  public String getChallengeTitle()
  {
    return challengeTitle;
  }
  
  public String getSubmissionDate()
  {
    return submissionDate;
  }
  
  public Integer getTimedDuration()
  {
    return timedDuration;
  }
  
  public void setChallenge(DVNTDeviation paramDVNTDeviation)
  {
    challenge = paramDVNTDeviation;
  }
  
  public void setChallengeDeviationId(String paramString)
  {
    challengeDeviationId = paramString;
  }
  
  public void setChallengeTitle(String paramString)
  {
    challengeTitle = paramString;
  }
  
  public void setSubmissionDate(String paramString)
  {
    submissionDate = paramString;
  }
  
  public void setTimedDuration(Integer paramInteger)
  {
    timedDuration = paramInteger;
  }
}
