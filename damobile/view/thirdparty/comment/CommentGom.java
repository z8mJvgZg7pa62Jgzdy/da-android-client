package com.deviantart.android.damobile.view.thirdparty.comment;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.deviantart.android.damobile.util.threaditem.CommentItem;
import com.deviantart.android.damobile.view.gom.Gom;

public abstract class CommentGom<VIEW_HOLDER extends com.deviantart.android.damobile.view.gom.comment.viewholder.CommentViewHolderBase>
  implements Gom<CommentItem, VIEW_HOLDER>
{
  private String d;
  private int k;
  
  public CommentGom() {}
  
  public void a(int paramInt)
  {
    k = paramInt;
  }
  
  public void a(Context paramContext, CommentItem paramCommentItem, com.deviantart.android.damobile.view.thirdparty.comment.viewholder.CommentViewHolderBase paramCommentViewHolderBase)
  {
    loader.setVisibility(8);
    if (paramCommentItem.next().intValue() > 0) {
      if (paramCommentItem.visitAnnotation().booleanValue())
      {
        commentRepliesTag.setVisibility(8);
        commentCollapseTag.setVisibility(0);
        commentRightCol.setVisibility(0);
      }
    }
    for (;;)
    {
      commentLoadMoreButton.setVisibility(8);
      if ((!paramCommentItem.n()) || (!paramCommentItem.isUnread().booleanValue())) {
        return;
      }
      commentLoadMoreButton.setVisibility(0);
      return;
      commentCollapseTag.setVisibility(8);
      commentRepliesTag.setText(paramCommentItem.next().toString());
      commentRepliesTag.setVisibility(0);
      break;
      commentRightCol.setVisibility(8);
    }
  }
  
  public void b(String paramString)
  {
    d = paramString;
  }
  
  public int c()
  {
    return k;
  }
  
  public String s()
  {
    return d;
  }
}
