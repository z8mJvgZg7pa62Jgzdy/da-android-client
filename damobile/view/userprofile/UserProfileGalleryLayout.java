package com.deviantart.android.damobile.view.userprofile;

import android.content.Context;
import com.deviantart.android.damobile.adapter.recyclerview.BaseGallectionAdapter;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APIGalleryFoldersLoader;

public class UserProfileGalleryLayout
  extends UserProfileGallectionBaseLayout
{
  public UserProfileGalleryLayout(Context paramContext, String paramString, boolean paramBoolean, Boolean paramBoolean1)
  {
    super(paramContext, paramString, paramBoolean, paramBoolean1);
  }
  
  public String b(Context paramContext)
  {
    return paramContext.getString(2131230884);
  }
  
  public String c(Context paramContext)
  {
    return paramContext.getString(2131230885);
  }
  
  protected String getType()
  {
    return "gallery";
  }
  
  public BaseGallectionAdapter visitLabel()
  {
    return new UserProfileGalleryLayout.GalleryAdapter(StreamCacher.a(new APIGalleryFoldersLoader(a, true, true)), a, getType(), c, b);
  }
}
