package com.deviantart.android.damobile.util;

public enum ProcessMenuType
{
  static
  {
    b = new ProcessMenuType("PROCESS_UNFAVE", 1);
    i = new ProcessMenuType("PROCESS_MLT", 2);
    d = new ProcessMenuType("PROCESS_SHARE", 3);
  }
}
