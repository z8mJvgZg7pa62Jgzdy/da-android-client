package com.deviantart.android.damobile.view.viewpageindicator;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.AbsSavedState;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import com.deviantart.android.damobile.R.styleable;
import com.deviantart.android.damobile.util.ViewHelper;
import com.viewpagerindicator.PageIndicator;

public class UserProfileCardIndicator
  extends View
  implements PageIndicator
{
  private int mActivePointerId = -1;
  private boolean mCenter;
  private boolean mCentered;
  private int mCurrentPage;
  private boolean mIsDragging;
  private float mLastMotionX = -1.0F;
  private ViewPager.OnPageChangeListener mListener;
  private int mOrientation;
  private float mPageOffset;
  private final Paint mPaintFill = new Paint(1);
  private final Paint mPaintPageFill = new Paint(1);
  private final Paint mPaintStroke = new Paint(1);
  private float mRadius;
  private int mScrollState;
  private boolean mSnap;
  private int mSnapPage;
  private int mTouchSlop;
  private ViewPager mViewPager;
  
  public UserProfileCardIndicator(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public UserProfileCardIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 2130772337);
  }
  
  public UserProfileCardIndicator(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    if (isInEditMode()) {
      return;
    }
    Object localObject = getResources();
    int i = ((Resources)localObject).getColor(2131558460);
    int j = ((Resources)localObject).getColor(2131558459);
    int k = ((Resources)localObject).getInteger(2131492879);
    int m = ((Resources)localObject).getColor(2131558461);
    float f1 = ((Resources)localObject).getDimension(2131361889);
    float f2 = ((Resources)localObject).getDimension(2131361888);
    boolean bool1 = ((Resources)localObject).getBoolean(2131296264);
    boolean bool2 = ((Resources)localObject).getBoolean(2131296265);
    paramAttributeSet = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.CirclePageIndicator, paramInt, 0);
    mCentered = paramAttributeSet.getBoolean(2, bool1);
    mOrientation = paramAttributeSet.getInt(0, k);
    mPaintPageFill.setStyle(Paint.Style.FILL);
    mPaintPageFill.setColor(paramAttributeSet.getColor(5, i));
    mPaintStroke.setStyle(Paint.Style.STROKE);
    mPaintStroke.setColor(paramAttributeSet.getColor(8, m));
    mPaintStroke.setStrokeWidth(paramAttributeSet.getDimension(3, f1));
    mPaintFill.setStyle(Paint.Style.FILL);
    mPaintFill.setColor(paramAttributeSet.getColor(4, j));
    mRadius = paramAttributeSet.getDimension(6, f2);
    mSnap = paramAttributeSet.getBoolean(7, bool2);
    localObject = paramAttributeSet.getDrawable(1);
    if (localObject != null) {
      ViewHelper.a(this, (Drawable)localObject);
    }
    paramAttributeSet.recycle();
    mTouchSlop = ViewConfigurationCompat.getScaledPagingTouchSlop(ViewConfiguration.get(paramContext));
  }
  
  private int measureLong(int paramInt)
  {
    int i = View.MeasureSpec.getMode(paramInt);
    paramInt = View.MeasureSpec.getSize(paramInt);
    if ((i == 1073741824) || (mViewPager == null)) {
      return paramInt;
    }
    int j = mViewPager.getAdapter().getCount();
    float f1 = getPaddingLeft() + getPaddingRight();
    float f2 = j * 2;
    float f3 = mRadius;
    j = (int)((j - 1) * mRadius + (f1 + f2 * f3) + 1.0F);
    if (i == Integer.MIN_VALUE) {
      return Math.min(j, paramInt);
    }
    return j;
  }
  
  private int measureShort(int paramInt)
  {
    int i = View.MeasureSpec.getMode(paramInt);
    paramInt = View.MeasureSpec.getSize(paramInt);
    if (i == 1073741824) {
      return paramInt;
    }
    int j = (int)(2.0F * mRadius + getPaddingTop() + getPaddingBottom() + 1.0F);
    if (i == Integer.MIN_VALUE) {
      return Math.min(j, paramInt);
    }
    return j;
  }
  
  public void b(int paramInt)
  {
    if ((mSnap) || (mScrollState == 0))
    {
      mCurrentPage = paramInt;
      mSnapPage = paramInt;
      invalidate();
    }
    if (mListener != null) {
      mListener.b(paramInt);
    }
  }
  
  public int getFillColor()
  {
    return mPaintFill.getColor();
  }
  
  public int getOrientation()
  {
    return mOrientation;
  }
  
  public int getPageColor()
  {
    return mPaintPageFill.getColor();
  }
  
  public float getRadius()
  {
    return mRadius;
  }
  
  public int getStrokeColor()
  {
    return mPaintStroke.getColor();
  }
  
  public float getStrokeWidth()
  {
    return mPaintStroke.getStrokeWidth();
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (mViewPager == null) {
      return;
    }
    int n = mViewPager.getAdapter().getCount();
    if (n != 0)
    {
      if (mCurrentPage >= n)
      {
        setCurrentItem(n - 1);
        return;
      }
      int i;
      int j;
      int k;
      int m;
      float f6;
      float f1;
      float f3;
      float f2;
      float f4;
      label205:
      float f5;
      if (mOrientation == 0)
      {
        i = getWidth();
        j = getPaddingLeft();
        k = getPaddingRight();
        m = getPaddingTop();
        f6 = mRadius * 3.0F;
        f1 = m;
        f1 = mRadius + f1;
        f3 = j + mRadius;
        f2 = f3;
        if (mCentered) {
          f2 = f3 + ((i - j - k) / 2.0F - n * f6 / 2.0F);
        }
        if (mCenter) {
          f2 = i - k - n * f6 / 2.0F;
        }
        f4 = mRadius;
        f3 = f4;
        if (mPaintStroke.getStrokeWidth() > 0.0F) {
          f3 = f4 - mPaintStroke.getStrokeWidth() / 2.0F;
        }
        i = 0;
        if (i >= n) {
          break label328;
        }
        f4 = i * f6 + f2;
        if (mOrientation != 0) {
          break label322;
        }
        f5 = f4;
        f4 = f1;
      }
      for (;;)
      {
        if (mPaintPageFill.getAlpha() > 0) {
          paramCanvas.drawCircle(f5, f4, f3, mPaintPageFill);
        }
        if (f3 != mRadius) {
          paramCanvas.drawCircle(f5, f4, mRadius, mPaintStroke);
        }
        i += 1;
        break label205;
        i = getHeight();
        j = getPaddingTop();
        k = getPaddingBottom();
        m = getPaddingLeft();
        break;
        label322:
        f5 = f1;
      }
      label328:
      if (mSnap)
      {
        i = mSnapPage;
        f4 = i * f6;
        f3 = f4;
        if (!mSnap) {
          f3 = f4 + mPageOffset * f6;
        }
        if (mOrientation != 0) {
          break label412;
        }
        f3 = f2 + f3;
        f2 = f1;
      }
      for (;;)
      {
        paramCanvas.drawCircle(f3, f2, mRadius, mPaintFill);
        return;
        i = mCurrentPage;
        break;
        label412:
        f2 += f3;
        f3 = f1;
      }
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    if (mOrientation == 0)
    {
      setMeasuredDimension(measureLong(paramInt1), measureShort(paramInt2));
      return;
    }
    setMeasuredDimension(measureShort(paramInt1), measureLong(paramInt2));
  }
  
  public void onPageScrollStateChanged(int paramInt)
  {
    mScrollState = paramInt;
    if (mListener != null) {
      mListener.onPageScrollStateChanged(paramInt);
    }
  }
  
  public void onPageScrolled(int paramInt1, float paramFloat, int paramInt2)
  {
    mCurrentPage = paramInt1;
    mPageOffset = paramFloat;
    invalidate();
    if (mListener != null) {
      mListener.onPageScrolled(paramInt1, paramFloat, paramInt2);
    }
  }
  
  public void onRestoreInstanceState(Parcelable paramParcelable)
  {
    paramParcelable = (UserProfileCardIndicator.SavedState)paramParcelable;
    super.onRestoreInstanceState(paramParcelable.getSuperState());
    mCurrentPage = currentPage;
    mSnapPage = currentPage;
    requestLayout();
  }
  
  public Parcelable onSaveInstanceState()
  {
    UserProfileCardIndicator.SavedState localSavedState = new UserProfileCardIndicator.SavedState(super.onSaveInstanceState());
    currentPage = mCurrentPage;
    return localSavedState;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = 0;
    if (super.onTouchEvent(paramMotionEvent)) {
      return true;
    }
    if ((mViewPager == null) || (mViewPager.getAdapter().getCount() == 0)) {
      return false;
    }
    int j = paramMotionEvent.getAction() & 0xFF;
    float f1;
    float f2;
    switch (j)
    {
    default: 
      break;
    case 4: 
      return true;
    case 0: 
      mActivePointerId = MotionEventCompat.getPointerId(paramMotionEvent, 0);
      mLastMotionX = paramMotionEvent.getX();
      return true;
    case 2: 
      f1 = MotionEventCompat.getX(paramMotionEvent, MotionEventCompat.findPointerIndex(paramMotionEvent, mActivePointerId));
      f2 = f1 - mLastMotionX;
      if ((!mIsDragging) && (Math.abs(f2) > mTouchSlop)) {
        mIsDragging = true;
      }
      if (!mIsDragging) {
        break label454;
      }
      mLastMotionX = f1;
      if ((!mViewPager.isFakeDragging()) && (!mViewPager.beginFakeDrag())) {
        break label454;
      }
      mViewPager.fakeDragBy(f2);
      return true;
    case 1: 
    case 3: 
      if (!mIsDragging)
      {
        i = mViewPager.getAdapter().getCount();
        int k = getWidth();
        f1 = k / 2.0F;
        f2 = k / 6.0F;
        if ((mCurrentPage > 0) && (paramMotionEvent.getX() < f1 - f2))
        {
          if (j == 3) {
            break label454;
          }
          mViewPager.setCurrentItem(mCurrentPage - 1);
          return true;
        }
        if ((mCurrentPage < i - 1) && (paramMotionEvent.getX() > f2 + f1))
        {
          if (j == 3) {
            break label454;
          }
          mViewPager.setCurrentItem(mCurrentPage + 1);
          return true;
        }
      }
      mIsDragging = false;
      mActivePointerId = -1;
      if (!mViewPager.isFakeDragging()) {
        break label454;
      }
      mViewPager.endFakeDrag();
      return true;
    case 5: 
      i = MotionEventCompat.getActionIndex(paramMotionEvent);
      mLastMotionX = MotionEventCompat.getX(paramMotionEvent, i);
      mActivePointerId = MotionEventCompat.getPointerId(paramMotionEvent, i);
      return true;
    }
    j = MotionEventCompat.getActionIndex(paramMotionEvent);
    if (MotionEventCompat.getPointerId(paramMotionEvent, j) == mActivePointerId)
    {
      if (j == 0) {
        i = 1;
      }
      mActivePointerId = MotionEventCompat.getPointerId(paramMotionEvent, i);
    }
    mLastMotionX = MotionEventCompat.getX(paramMotionEvent, MotionEventCompat.findPointerIndex(paramMotionEvent, mActivePointerId));
    label454:
    return true;
  }
  
  public void setCentered(boolean paramBoolean)
  {
    mCentered = paramBoolean;
    if (mCentered) {
      mCenter = false;
    }
    invalidate();
  }
  
  public void setCurrentItem(int paramInt)
  {
    if (mViewPager == null) {
      throw new IllegalStateException("ViewPager has not been bound.");
    }
    mViewPager.setCurrentItem(paramInt);
    mCurrentPage = paramInt;
    invalidate();
  }
  
  public void setFillColor(int paramInt)
  {
    mPaintFill.setColor(paramInt);
    invalidate();
  }
  
  public void setOnPageChangeListener(ViewPager.OnPageChangeListener paramOnPageChangeListener)
  {
    mListener = paramOnPageChangeListener;
  }
  
  public void setOrientation(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      throw new IllegalArgumentException("Orientation must be either HORIZONTAL or VERTICAL.");
    }
    mOrientation = paramInt;
    requestLayout();
  }
  
  public void setPageColor(int paramInt)
  {
    mPaintPageFill.setColor(paramInt);
    invalidate();
  }
  
  public void setRadius(float paramFloat)
  {
    mRadius = paramFloat;
    invalidate();
  }
  
  public void setRight(boolean paramBoolean)
  {
    mCenter = paramBoolean;
    if (mCenter) {
      mCentered = false;
    }
    invalidate();
  }
  
  public void setSnap(boolean paramBoolean)
  {
    mSnap = paramBoolean;
    invalidate();
  }
  
  public void setStrokeColor(int paramInt)
  {
    mPaintStroke.setColor(paramInt);
    invalidate();
  }
  
  public void setStrokeWidth(float paramFloat)
  {
    mPaintStroke.setStrokeWidth(paramFloat);
    invalidate();
  }
  
  public void setViewPager(ViewPager paramViewPager)
  {
    if (mViewPager == paramViewPager) {
      return;
    }
    if (mViewPager != null) {
      mViewPager.setOnPageChangeListener(null);
    }
    if (paramViewPager.getAdapter() == null) {
      throw new IllegalStateException("ViewPager does not have adapter instance.");
    }
    mViewPager = paramViewPager;
    mViewPager.setOnPageChangeListener(this);
    invalidate();
  }
}
