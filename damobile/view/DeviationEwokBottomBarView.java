package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewAnimator;
import android.widget.ViewFlipper;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation.DVNTDeviationBaseStats;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.activity.SubmitActivity.IntentBuilder;
import com.deviantart.android.damobile.fragment.FullTorpedoFragment;
import com.deviantart.android.damobile.fragment.FullTorpedoFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.HomeBaseFragment.HomeFragmentInstanceBuilder;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APIMoreLikeThisLoader;
import com.deviantart.android.damobile.util.DAFormatUtils;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.MemberType;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.deviation.DeviationPanelTab;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.view.ewok.DeviationEwok;
import com.facebook.drawee.view.SimpleDraweeView;

public class DeviationEwokBottomBarView
  extends LinearLayout
{
  @Bind({2131689708})
  SimpleDraweeView authorAvatar;
  @Bind({2131689711})
  TextView commentCount;
  protected DeviationEwok d;
  @Bind({2131689712})
  TextView favCount;
  @Bind({2131689710})
  ViewFlipper flipper;
  @Bind({2131689709})
  TextView username;
  
  public DeviationEwokBottomBarView(Activity paramActivity, DeviationEwok paramDeviationEwok)
  {
    super(paramActivity);
    d = paramDeviationEwok;
    ((LayoutInflater)getContext().getSystemService("layout_inflater")).inflate(2130968656, this, true);
    ButterKnife.bind(this);
    commentCount.setText(DAFormatUtils.format(paramDeviationEwok.a().getStats().getComments()));
    favCount.setText(DAFormatUtils.format(paramDeviationEwok.a().getStats().getFavourites()));
    username.setText(paramDeviationEwok.a().getAuthor().getUserName());
    ImageUtils.isEmpty(authorAvatar, Uri.parse(paramDeviationEwok.a().getAuthor().getUserIconURL()));
  }
  
  public void onClickAvatar()
  {
    Object localObject1 = d.a();
    if (localObject1 == null) {
      return;
    }
    Object localObject2 = ((DVNTAbstractDeviation)localObject1).getAuthor();
    if (localObject2 != null)
    {
      localObject1 = ((DVNTUser)localObject2).getUserName();
      localObject2 = MemberType.toString(((DVNTUser)localObject2).getType());
      if ((localObject1 != null) && (localObject2 != null)) {
        UserUtils.c((Activity)getContext(), d.a().getAuthor().getUserName());
      }
    }
  }
  
  public void onClickComments()
  {
    NavigationUtils.b((Activity)getContext(), d, DeviationPanelTab.i);
  }
  
  public void onClickFavs()
  {
    TrackerUtil.a((Activity)getContext(), "tap_favourite_star");
    NavigationUtils.b((Activity)getContext(), d, DeviationPanelTab.b);
  }
  
  public void onClickMlt()
  {
    String str = null;
    Stream localStream = StreamCacher.a(new APIMoreLikeThisLoader(d.a().getId(), null), StreamCacheStrategy.d);
    Activity localActivity = (Activity)getContext();
    if ((d != null) && (d.a() != null))
    {
      if (d.a().getAuthor() == null) {
        return;
      }
      DVNTImage localDVNTImage = d.b(localActivity);
      if (localDVNTImage != null) {
        str = localDVNTImage.getSrc();
      }
      ScreenFlowManager.a(localActivity, (FullTorpedoFragment)new FullTorpedoFragment.InstanceBuilder().a(localStream).a(getContext().getString(2131231090)).b("mlt").putShort(d.a().getTitle()).putInt(d.a().getAuthor().getUserName()).c(str).a(), "fullmlt" + d.a().getId());
    }
  }
  
  public void onClickNext()
  {
    flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), 2131034136));
    flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), 2131034135));
    flipper.showNext();
  }
  
  public void onClickPrev()
  {
    flipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), 2131034137));
    flipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), 2131034134));
    flipper.showPrevious();
  }
  
  public void onClickShare()
  {
    TrackerUtil.a((Activity)getContext(), "tap_share");
    Intent localIntent = new SubmitActivity.IntentBuilder().a(d.a()).a(getContext());
    ((Activity)getContext()).startActivityForResult(localIntent, 108);
  }
  
  public void onClickUsername()
  {
    onClickAvatar();
  }
}
