package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.io.File;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import retrofit.mime.TypedFile;

public class DVNTUpdateAvatarRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTAvatarResponse>
{
  private final File file;
  private final String mimeType;
  
  public DVNTUpdateAvatarRequestV1(String paramString, File paramFile)
  {
    super(com.deviantart.android.sdk.api.model.DVNTAvatarResponse.class);
    file = paramFile;
    mimeType = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTUpdateAvatarRequestV1 localDVNTUpdateAvatarRequestV1 = (DVNTUpdateAvatarRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(mimeType, mimeType).append(file, file).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(mimeType).append(file).toHashCode();
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTAvatarResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).updateAvatar(paramString, new TypedFile(mimeType, file));
  }
}
