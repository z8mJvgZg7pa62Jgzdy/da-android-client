package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.sdk.api.network.service.DVNTServiceAnonymous;

public abstract class DVNTAsyncRequestAnonymous<T>
  extends com.deviantart.android.sdk.api.network.request.DVNTBaseAsyncRequest<T, DVNTServiceAnonymous>
{
  protected DVNTAsyncRequestAnonymous(Class paramClass)
  {
    super(paramClass, DVNTServiceAnonymous.class);
  }
  
  public Object loadDataFromNetwork()
  {
    return sendRequest(null);
  }
}
