package com.deviantart.android.damobile.view.thirdparty.deviation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.view.thirdparty.GomType;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class JournalGom
  extends com.deviantart.android.damobile.view.gom.deviation.DeviationGom<com.deviantart.android.damobile.view.gom.deviation.viewholder.JournalViewHolder>
{
  public JournalGom() {}
  
  public GomType b()
  {
    return GomType.e;
  }
  
  public void c(Context paramContext, DeviationDescription paramDeviationDescription, com.deviantart.android.damobile.view.thirdparty.deviation.viewholder.JournalViewHolder paramJournalViewHolder, boolean paramBoolean)
  {
    DVNTDeviation localDVNTDeviation = paramDeviationDescription.a();
    String str = Jsoup.get(localDVNTDeviation.getExcerpt()).get();
    preview.setText(str);
    title.setText(localDVNTDeviation.getTitle());
    if (paramBoolean) {
      DeviationUtils.b(faveView, localDVNTDeviation);
    }
    super.b(paramContext, paramDeviationDescription, paramJournalViewHolder, paramBoolean);
  }
  
  public com.deviantart.android.damobile.view.thirdparty.deviation.viewholder.JournalViewHolder inflate(Context paramContext, ViewGroup paramViewGroup)
  {
    return new com.deviantart.android.damobile.view.thirdparty.deviation.viewholder.JournalViewHolder(LayoutInflater.from(paramContext).inflate(2130968700, paramViewGroup, false));
  }
}
