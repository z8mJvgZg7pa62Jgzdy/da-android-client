package com.deviantart.android.damobile.util.notes;

import android.content.Context;
import com.deviantart.android.damobile.stream.loader.APINotesFolderLoader;
import com.deviantart.android.damobile.util.opt.ItemPendable;
import com.deviantart.android.damobile.view.notes.NotesListFrame;

public abstract interface NotesPage
  extends ItemPendable
{
  public abstract NotesListFrame a(Context paramContext, int paramInt);
  
  public abstract APINotesFolderLoader b();
  
  public abstract String get(Context paramContext);
  
  public abstract int getColumnIndex();
  
  public abstract String getString();
}
