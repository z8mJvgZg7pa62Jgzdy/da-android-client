package com.deviantart.android.damobile.util.submit;

public class SubmitOptions
{
  Boolean a = Boolean.valueOf(true);
  Boolean c = Boolean.valueOf(true);
  Boolean pinned = Boolean.valueOf(false);
  SubmitLicenseOptions s = new SubmitLicenseOptions();
  Boolean visible = Boolean.valueOf(true);
  
  public SubmitOptions() {}
  
  public Boolean get()
  {
    return a;
  }
  
  public Boolean getSize()
  {
    return c;
  }
  
  public SubmitLicenseOptions getString()
  {
    return s;
  }
  
  public Boolean isPinned()
  {
    return pinned;
  }
  
  public Boolean isVisible()
  {
    return visible;
  }
  
  public void setIs24HourView(Boolean paramBoolean)
  {
    a = paramBoolean;
  }
  
  public void setPinned(Boolean paramBoolean)
  {
    pinned = paramBoolean;
  }
  
  public void setSystem(Boolean paramBoolean)
  {
    c = paramBoolean;
  }
  
  public void setUnread(Boolean paramBoolean)
  {
    visible = paramBoolean;
  }
}
