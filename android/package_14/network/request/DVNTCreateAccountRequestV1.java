package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTCreateAccountRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  private boolean agreeEtiquette;
  private boolean agreeTos;
  private String dateOfBirth;
  private String email;
  private boolean newsLetter;
  private String password;
  private String userName;
  private String word1;
  
  public DVNTCreateAccountRequestV1(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    userName = paramString1;
    email = paramString2;
    password = paramString3;
    dateOfBirth = paramString4;
    word1 = paramString5;
    newsLetter = paramBoolean1;
    agreeTos = paramBoolean2;
    agreeEtiquette = paramBoolean3;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTCreateAccountRequestV1 localDVNTCreateAccountRequestV1 = (DVNTCreateAccountRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(userName, userName).append(email, email).append(password, password).append(dateOfBirth, dateOfBirth).append(word1, word1).append(newsLetter, newsLetter).append(agreeTos, agreeTos).append(agreeEtiquette, agreeEtiquette).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(userName).append(email).append(password).append(dateOfBirth).append(word1).append(newsLetter).append(agreeTos).append(agreeEtiquette).toHashCode();
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).createAccount(paramString, userName, email, password, dateOfBirth, word1, newsLetter, agreeTos, agreeEtiquette);
  }
}
