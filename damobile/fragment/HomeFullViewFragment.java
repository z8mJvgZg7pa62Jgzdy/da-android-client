package com.deviantart.android.damobile.fragment;

import com.deviantart.android.damobile.activity.HomeActivity.BottomBarButton;

public abstract class HomeFullViewFragment
  extends HomeBaseFragment
{
  public HomeFullViewFragment() {}
  
  protected HomeActivity.BottomBarButton c()
  {
    return HomeActivity.BottomBarButton.o;
  }
  
  public boolean refresh()
  {
    return false;
  }
}
