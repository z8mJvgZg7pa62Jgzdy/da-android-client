package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.List;

public class DVNTSendNoteRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTSendNoteResults>
{
  String body;
  List<String> recipients;
  String replyToNoteId;
  String subject;
  
  public DVNTSendNoteRequestV1(List paramList, String paramString1, String paramString2, String paramString3)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSendNoteResults.class);
    recipients = paramList;
    subject = paramString1;
    body = paramString2;
    replyToNoteId = paramString3;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTSendNoteRequestV1)paramObject;
    return (Objects.append(recipients, recipients)) && (Objects.append(subject, subject)) && (Objects.append(body, body)) && (Objects.append(replyToNoteId, replyToNoteId));
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), recipients, subject, body, replyToNoteId });
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSendNoteResults sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).sendNotes(paramString, recipients, subject, body, replyToNoteId);
  }
}
