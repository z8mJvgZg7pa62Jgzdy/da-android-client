package com.deviantart.android.damobile.view.ewok;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.damobile.util.DeviationType;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.GalleryType;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.view.ImageFullView;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.Iterator;
import java.util.List;

public class ImageDeviationEwok
  extends DeviationEwok
{
  private boolean a = false;
  private DVNTImage b;
  private DVNTImage d;
  private DVNTImage f;
  
  public ImageDeviationEwok(DVNTDeviation paramDVNTDeviation)
  {
    super(paramDVNTDeviation);
    d = DeviationUtils.b(paramDVNTDeviation);
    f = DeviationUtils.operate(paramDVNTDeviation);
    b = DeviationUtils.b(paramDVNTDeviation);
    if (paramDVNTDeviation.isMature() == null) {}
    for (;;)
    {
      a = bool;
      return;
      bool = paramDVNTDeviation.isMature().booleanValue();
    }
  }
  
  public View a(Activity paramActivity, ViewGroup paramViewGroup)
  {
    int j = 0;
    int i;
    if (paramViewGroup == null)
    {
      i = 0;
      if (paramViewGroup != null) {
        break label31;
      }
    }
    for (;;)
    {
      return b(paramActivity, paramViewGroup, i, j);
      i = paramViewGroup.getWidth();
      break;
      label31:
      j = paramViewGroup.getHeight();
    }
  }
  
  public DVNTImage a(Activity paramActivity, int paramInt1, int paramInt2)
  {
    DVNTImage localDVNTImage = d;
    paramActivity = d.getThumbs();
    if ((paramActivity != null) && (!paramActivity.isEmpty()) && (paramInt1 > 0))
    {
      Iterator localIterator = paramActivity.iterator();
      do
      {
        if (!localIterator.hasNext()) {
          break;
        }
        paramActivity = (DVNTImage)localIterator.next();
      } while (paramActivity.getWidth() < paramInt1);
    }
    for (;;)
    {
      if (paramActivity != null) {
        return paramActivity;
      }
      return localDVNTImage;
      paramActivity = null;
    }
  }
  
  public View b(Activity paramActivity, ViewGroup paramViewGroup, int paramInt1, int paramInt2)
  {
    if (d == null) {
      return new SimpleDraweeView(paramActivity);
    }
    DVNTImage localDVNTImage = ImageUtils.a(paramActivity, d, d, paramInt1, paramInt2, GalleryType.b.equals(ImageUtils.i));
    return ImageUtils.a(paramActivity, paramViewGroup, a, localDVNTImage, b);
  }
  
  public View b(Context paramContext)
  {
    ImageUtils.a(GalleryType.d);
    ImageFullView localImageFullView = new ImageFullView(paramContext);
    if (f != null) {
      paramContext = f;
    }
    for (;;)
    {
      if (paramContext == null)
      {
        return localImageFullView;
        if (d != null) {
          paramContext = ImageUtils.a((Activity)paramContext, d, d, Graphics.height(paramContext), Graphics.width(paramContext), true);
        }
      }
      else
      {
        DVNTImage localDVNTImage3 = DeviationUtils.a(d);
        DVNTImage localDVNTImage1 = localDVNTImage3;
        DVNTImage localDVNTImage2 = localDVNTImage1;
        if (localDVNTImage3 != null)
        {
          localDVNTImage2 = localDVNTImage1;
          if (localDVNTImage3.getWidth() == paramContext.getWidth()) {
            localDVNTImage2 = null;
          }
        }
        localImageFullView.a(paramContext, localDVNTImage2, a, true);
        return localImageFullView;
      }
      paramContext = null;
    }
  }
  
  public DVNTImage b(Activity paramActivity)
  {
    return a(paramActivity, 0, 0);
  }
  
  public DeviationType b()
  {
    return DeviationType.c;
  }
}
