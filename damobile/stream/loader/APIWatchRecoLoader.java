package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTWatchRecommendationItem;

public class APIWatchRecoLoader
  extends StreamLoader<DVNTWatchRecommendationItem>
{
  private String d = "";
  
  public APIWatchRecoLoader() {}
  
  public String b()
  {
    return "watch_recommendation";
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTAsyncAPI.getWatchRecommendations(d, Integer.valueOf(a()));
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APIWatchRecoLoader.1(this, paramStreamLoadListener, paramInt));
  }
}
