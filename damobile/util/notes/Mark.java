package com.deviantart.android.damobile.util.notes;

import com.deviantart.android.android.package_14.model.DVNTNote.Mark;

public enum Mark
{
  private final int e;
  private final String f;
  private final Mark.DataUpdater g;
  private final DVNTNote.Mark i;
  private final int j;
  private final int k;
  private Mark o;
  
  static
  {
    c = new Mark("READ", 2, 2131231118, 2131230936, DVNTNote.Mark.READ, "2E23AB5F-02FC-0EAD-F27A-BD5A251529FE", -1, new Mark.3());
    a = new Mark("UNREAD", 3, 2131231119, 2131230937, DVNTNote.Mark.UNREAD, "2E23AB5F-02FC-0EAD-F27A-BD5A251529FE", 1, new Mark.4());
    p = new Mark[] { b, d, c, a };
    bo = d;
    do = b;
    co = a;
    ao = c;
  }
  
  private Mark(int paramInt1, int paramInt2, DVNTNote.Mark paramMark, String paramString, int paramInt3, Mark.DataUpdater paramDataUpdater)
  {
    j = paramInt1;
    k = paramInt2;
    i = paramMark;
    f = paramString;
    e = paramInt3;
    g = paramDataUpdater;
  }
  
  public Mark a()
  {
    return o;
  }
  
  public Mark.DataUpdater b()
  {
    return g;
  }
  
  public int c()
  {
    return k;
  }
  
  public String d()
  {
    return f;
  }
  
  public int e()
  {
    return e;
  }
  
  public void f()
  {
    NotesFolders.a(d(), e());
  }
  
  public int h()
  {
    return j;
  }
  
  public DVNTNote.Mark i()
  {
    return i;
  }
}
