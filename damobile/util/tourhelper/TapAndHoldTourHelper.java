package com.deviantart.android.damobile.util.tourhelper;

import android.app.Activity;
import android.content.Context;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.view.tooltip.tour.DATourSlideDescriptor;

public class TapAndHoldTourHelper
  extends TourHelperBase
{
  int map;
  
  public TapAndHoldTourHelper(Activity paramActivity, int paramInt)
  {
    super(paramActivity);
    map = paramInt;
  }
  
  public static boolean putInt(Context paramContext)
  {
    return TourHelperBase.update(paramContext, "tap_and_hold_did_tour");
  }
  
  protected void b()
  {
    TrackerUtil.a(a, EventKeys.Category.y, "Render_Overlay");
  }
  
  protected String getName()
  {
    return "tap_and_hold_did_tour";
  }
  
  protected DATourSlideDescriptor[] getObjects()
  {
    return new DATourSlideDescriptor[] { new DATourSlideDescriptor(2131231389, 2130837829, 2131231388, Integer.valueOf(map), false) };
  }
}
