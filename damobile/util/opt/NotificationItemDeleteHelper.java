package com.deviantart.android.damobile.util.opt;

import android.app.Activity;
import android.content.Context;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessage;
import com.deviantart.android.android.package_14.model.DVNTFeedbackMessageStack;
import com.deviantart.android.damobile.stream.FilteredStream;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.filter.NotificationsModelFilter;
import com.deviantart.android.damobile.stream.filter.StreamFilter;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnNotificationItemChangeEvent;
import com.deviantart.android.damobile.util.mc.ItemDeleteHelperBase;
import com.deviantart.android.damobile.util.notifications.NotificationItemData;
import com.deviantart.android.damobile.util.notifications.NotificationsPage;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.squareup.otto.Bus;

public class NotificationItemDeleteHelper
  extends ItemDeleteHelperBase<NotificationItemData, NotificationsPage>
{
  public NotificationItemDeleteHelper() {}
  
  private boolean a(NotificationItemData paramNotificationItemData)
  {
    if ((paramNotificationItemData.get()) && (paramNotificationItemData.getString().getCount() != null)) {
      return paramNotificationItemData.getString().getCount().intValue() <= 1;
    }
    return true;
  }
  
  private ItemDeleteHelperBase.ItemDeleteRequestListener lift(NotificationItemData paramNotificationItemData)
  {
    return new NotificationItemDeleteHelper.1(this, paramNotificationItemData);
  }
  
  public Stream b(NotificationsPage paramNotificationsPage)
  {
    paramNotificationsPage = NotificationsPage.b(paramNotificationsPage);
    if ((paramNotificationsPage == null) || (!StreamCacher.b(paramNotificationsPage))) {
      return null;
    }
    return new FilteredStream(StreamCacher.a(paramNotificationsPage), new StreamFilter[] { new NotificationsModelFilter() });
  }
  
  void b(Activity paramActivity, NotificationItemData paramNotificationItemData)
  {
    if (a(paramNotificationItemData)) {}
    for (paramNotificationItemData = "Item";; paramNotificationItemData = "Stack")
    {
      TrackerUtil.get(paramActivity, EventKeys.Category.i, "TapUndoDelete", paramNotificationItemData);
      return;
    }
  }
  
  void b(NotificationItemData paramNotificationItemData)
  {
    BusStation.get().post(new BusStation.OnNotificationItemChangeEvent(paramNotificationItemData, true));
  }
  
  int c(NotificationItemData paramNotificationItemData)
  {
    if (a(paramNotificationItemData)) {
      return 2131231141;
    }
    return 2131231142;
  }
  
  void c(Activity paramActivity, NotificationItemData paramNotificationItemData)
  {
    if (a(paramNotificationItemData)) {}
    for (String str = "DeleteItem";; str = "DeleteStack")
    {
      TrackerUtil.get(paramActivity, EventKeys.Category.i, str, paramNotificationItemData.c());
      return;
    }
  }
  
  void c(Context paramContext, NotificationItemData paramNotificationItemData)
  {
    if (paramNotificationItemData.get()) {}
    for (String str = paramNotificationItemData.getString().getStackId();; str = null)
    {
      if (str != null)
      {
        DVNTCommonAsyncAPI.deleteStackedMessage(null, str).call(paramContext, lift(paramNotificationItemData));
        return;
      }
      DVNTCommonAsyncAPI.deleteSingleMessage(null, paramNotificationItemData.a().getMessageId()).call(paramContext, lift(paramNotificationItemData));
      return;
    }
  }
  
  void deleteChannel(NotificationItemData paramNotificationItemData)
  {
    BusStation.get().post(new BusStation.OnNotificationItemChangeEvent(paramNotificationItemData, false));
  }
}
