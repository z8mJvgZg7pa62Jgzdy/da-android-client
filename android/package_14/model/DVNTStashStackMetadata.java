package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTStashStackMetadata
  extends DVNTStashMetadata
  implements Serializable
{
  @SerializedName("size")
  private Integer size;
  @SerializedName("thumb")
  private DVNTImage thumb;
  
  public DVNTStashStackMetadata() {}
  
  public Integer getSize()
  {
    return size;
  }
  
  public DVNTImage getThumb()
  {
    return thumb;
  }
  
  public void setSize(Integer paramInteger)
  {
    size = paramInteger;
  }
  
  public void setThumb(DVNTImage paramDVNTImage)
  {
    thumb = paramDVNTImage;
  }
}
