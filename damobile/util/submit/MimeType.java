package com.deviantart.android.damobile.util.submit;

public enum MimeType
{
  private String subtype;
  
  static
  {
    c = new MimeType("TEXT_HTML", 1, "text/html");
  }
  
  private MimeType(String paramString)
  {
    subtype = paramString;
  }
  
  public String equals()
  {
    return subtype;
  }
}
