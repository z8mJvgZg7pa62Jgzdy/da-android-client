package com.deviantart.android.android.package_14.model;

import com.deviantart.android.sdk.api.model.DVNTNote.List;
import com.deviantart.android.sdk.api.model.DVNTPaginatedResult;

public class DVNTNotes
  extends DVNTPaginatedResult<DVNTNote.List>
{
  public DVNTNotes() {}
}
