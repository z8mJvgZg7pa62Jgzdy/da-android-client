package com.deviantart.android.damobile.view.viewpageindicator;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.viewpagerindicator.IconPagerAdapter;

public abstract class DATabIndicator
  extends CustomizableTabPageIndicator
{
  DATabIndicator.OnTabIconClickListener albums;
  DATabIndicator.OnTabClickListener o;
  
  public DATabIndicator(Context paramContext)
  {
    super(paramContext);
  }
  
  public DATabIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public void a()
  {
    if (mViewPager == null) {
      return;
    }
    DAIconPagerAdapter localDAIconPagerAdapter = (DAIconPagerAdapter)mViewPager.getAdapter();
    int j = mTabLayout.getChildCount();
    int i = 0;
    while (i < j)
    {
      CustomizableTabPageIndicator.TextViewTab localTextViewTab = (CustomizableTabPageIndicator.TextViewTab)mTabLayout.getChildAt(i);
      CharSequence localCharSequence2 = localDAIconPagerAdapter.getPageTitle(i);
      CharSequence localCharSequence1 = localCharSequence2;
      if (localCharSequence2 == null) {
        localCharSequence1 = CustomizableTabPageIndicator.k;
      }
      a(i, localTextViewTab, localCharSequence1, localDAIconPagerAdapter.getIcon(i), localDAIconPagerAdapter.b(i));
      i += 1;
    }
  }
  
  protected void a(int paramInt1, CustomizableTabPageIndicator.TextViewTab paramTextViewTab, CharSequence paramCharSequence1, int paramInt2, CharSequence paramCharSequence2)
  {
    z = paramInt1;
    paramTextViewTab.setOnClickListener(finish(paramInt1));
    paramTextViewTab.setText(paramCharSequence1);
    paramTextViewTab.setContentDescription(paramCharSequence2);
    if (paramInt2 != 0) {
      paramTextViewTab.setCompoundDrawablesWithIntrinsicBounds(paramInt2, 0, 0, 0);
    }
  }
  
  protected void a(int paramInt1, CharSequence paramCharSequence, int paramInt2)
  {
    Log.e("DATabIndicator", "Unexpected call to addTab(int,CharSequence,int). Only the signature with content description should be called.");
    a(paramInt1, paramCharSequence, paramInt2, null);
  }
  
  protected void a(int paramInt1, CharSequence paramCharSequence1, int paramInt2, CharSequence paramCharSequence2)
  {
    CustomizableTabPageIndicator.TextViewTab localTextViewTab = b();
    a(paramInt1, localTextViewTab, paramCharSequence1, paramInt2, paramCharSequence2);
    mTabLayout.addView(localTextViewTab, b(paramInt1, paramCharSequence1, paramInt2));
  }
  
  protected LinearLayout.LayoutParams b(int paramInt1, CharSequence paramCharSequence, int paramInt2)
  {
    return new LinearLayout.LayoutParams(-1, -1, 1.0F);
  }
  
  protected CustomizableTabPageIndicator.TextViewTab b()
  {
    CustomizableTabPageIndicator.TextViewTab localTextViewTab = new CustomizableTabPageIndicator.TextViewTab(this, getContext());
    localTextViewTab.setFocusable(true);
    return localTextViewTab;
  }
  
  public void b(int paramInt)
  {
    super.b(paramInt);
    if (o != null) {
      o.b(paramInt, false);
    }
  }
  
  public void d(int paramInt)
  {
    if (o != null) {
      o.b(paramInt, true);
    }
  }
  
  protected View.OnClickListener finish(int paramInt)
  {
    return new DATabIndicator.1(this, paramInt);
  }
  
  protected ViewGroup.LayoutParams getTabContainerLayout()
  {
    return new ViewGroup.LayoutParams(-1, -1);
  }
  
  public void notifyDataSetChanged()
  {
    if (mViewPager == null) {
      return;
    }
    mTabLayout.removeAllViews();
    DAIconPagerAdapter localDAIconPagerAdapter = (DAIconPagerAdapter)mViewPager.getAdapter();
    int j = localDAIconPagerAdapter.getCount();
    int i = 0;
    while (i < j)
    {
      CharSequence localCharSequence2 = localDAIconPagerAdapter.getPageTitle(i);
      CharSequence localCharSequence1 = localCharSequence2;
      if (localCharSequence2 == null) {
        localCharSequence1 = CustomizableTabPageIndicator.k;
      }
      a(i, localCharSequence1, localDAIconPagerAdapter.getIcon(i), localDAIconPagerAdapter.b(i));
      i += 1;
    }
    if (mSelectedTabIndex > j) {
      mSelectedTabIndex = (j - 1);
    }
    setCurrentItem(mSelectedTabIndex);
    requestLayout();
  }
  
  public void setOnTabClickListener(DATabIndicator.OnTabClickListener paramOnTabClickListener)
  {
    o = paramOnTabClickListener;
  }
  
  public void setOnTabIconClickListener(DATabIndicator.OnTabIconClickListener paramOnTabIconClickListener)
  {
    albums = paramOnTabIconClickListener;
  }
}
