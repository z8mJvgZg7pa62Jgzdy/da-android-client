package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTSearchedTag
  implements Serializable
{
  @SerializedName("tag_name")
  String tagName;
  
  public DVNTSearchedTag() {}
  
  public String getTagName()
  {
    return tagName;
  }
  
  public void setTagName(String paramString)
  {
    tagName = paramString;
  }
}
