package com.deviantart.android.damobile.adapter.recyclerview;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.UserWatchStateChangeListener;
import com.deviantart.android.damobile.view.WatchListUserLayout;

public class WatchersAdapter
  extends DAStateRecyclerViewAdapter<com.deviantart.android.sdk.api.model.DVNTFriend>
{
  private UserWatchStateChangeListener j;
  private String mNamespace;
  
  public WatchersAdapter(Stream paramStream)
  {
    super(paramStream);
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    return new WatchersAdapter.1(this, new WatchListUserLayout(paramViewGroup.getContext(), j));
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    paramViewHolder = itemView;
    if (!(paramViewHolder instanceof WatchListUserLayout)) {
      return;
    }
    paramViewHolder = (WatchListUserLayout)paramViewHolder;
    paramViewHolder.b((com.deviantart.android.android.package_14.model.DVNTFriend)get().get(paramInt));
    paramViewHolder.setEventSource(mNamespace);
  }
  
  public void setDisplayMode(UserWatchStateChangeListener paramUserWatchStateChangeListener)
  {
    j = paramUserWatchStateChangeListener;
  }
  
  public void setNamespace(String paramString)
  {
    mNamespace = paramString;
  }
}
