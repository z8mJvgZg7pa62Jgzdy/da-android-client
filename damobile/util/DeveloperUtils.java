package com.deviantart.android.damobile.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ComponentInfo;
import android.util.Log;
import com.google.android.com.common.GooglePlayServicesUtil;

public class DeveloperUtils
{
  public DeveloperUtils() {}
  
  public static boolean b(Context paramContext)
  {
    if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(paramContext) == 0) {
      return true;
    }
    Log.e("play services", "This device is not supported.");
    return false;
  }
  
  public static boolean isSystemApp(Context paramContext, Intent paramIntent)
  {
    paramContext = paramIntent.resolveActivityInfo(paramContext.getPackageManager(), paramIntent.getFlags());
    return (paramContext != null) && (exported);
  }
}
