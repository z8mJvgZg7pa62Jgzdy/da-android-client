package com.deviantart.android.damobile.fragment;

import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.activity.HomeActivity;
import com.deviantart.android.damobile.util.DelayedUserInputTask;
import com.deviantart.android.damobile.util.Keyboard;
import com.deviantart.android.damobile.util.Recent;
import com.deviantart.android.damobile.util.ScreenFlowManager;
import com.deviantart.android.damobile.util.tracking.pacaya.MobileLava;
import com.deviantart.android.damobile.util.tracking.pacaya.UxTopicEventCreator;
import com.deviantart.pacaya.Pacaya;

public class SearchFragment
  extends HomeFullViewFragment
{
  private SearchFragment.SearchAdapter a;
  private boolean i = false;
  private boolean l = true;
  private SearchFragment.SearchType r;
  @Bind({2131689777})
  ImageView recentClear;
  @Bind({2131689776})
  TextView recentTitle;
  private boolean s = false;
  @Bind({2131690025})
  EditText searchInput;
  @Bind({2131689778})
  RecyclerView searchList;
  @Bind({2131689816})
  ProgressBar searchProgressBar;
  private DelayedUserInputTask task;
  
  public SearchFragment() {}
  
  public boolean b()
  {
    if (!searchInput.getText().toString().isEmpty())
    {
      searchInput.setText("");
      return true;
    }
    return super.b();
  }
  
  public void clearRecent()
  {
    new AlertDialog.Builder(getActivity()).setMessage(2131231246).setPositiveButton(2131230873, SearchFragment..Lambda.2.lambdaFactory$(this)).setNegativeButton(2131230820, null).show();
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = paramLayoutInflater.inflate(2130968677, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    searchList.setLayoutManager(new LinearLayoutManager(getActivity()));
    l = false;
    paramViewGroup = new Recent(getActivity(), "recent_searches");
    a = new SearchFragment.SearchAdapter(null);
    a.b(SearchFragment..Lambda.1.b(this));
    searchList.setAdapter(a);
    a.a(paramViewGroup.read());
    i = true;
    initUI();
    ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
    ((BaseActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    MobileLava.e().setText(new UxTopicEventCreator().get("search").b());
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    if (task != null)
    {
      task.cancel(true);
      task = null;
    }
    ButterKnife.unbind(this);
    l = true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default: 
      break;
    }
    for (;;)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      Keyboard.hideKeyboard(getActivity());
      ScreenFlowManager.hideKeyboard(getActivity());
    }
  }
  
  public void onResume()
  {
    s = false;
    Keyboard.showKeyboard(getActivity(), searchInput);
    super.onResume();
  }
  
  public boolean performSearch(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
  {
    if (DVNTContextUtils.isContextDead(getActivity())) {
      return false;
    }
    if ((paramInt == 3) || (paramInt == 5))
    {
      Keyboard.hideKeyboard(getActivity());
      ((HomeActivity)getActivity()).b(paramTextView.getText().toString());
      return true;
    }
    return false;
  }
  
  public void search(String paramString, SearchFragment.SearchType paramSearchType)
  {
    if ((isAdded()) && (searchProgressBar != null))
    {
      if (searchList == null) {
        return;
      }
      searchProgressBar.setVisibility(0);
      DVNTCommonAsyncAPI.browseSearchTags(paramString).call(getActivity(), new SearchFragment.2(this, paramSearchType, paramString));
    }
  }
  
  public void searchTextChanged(CharSequence paramCharSequence)
  {
    if (task != null) {
      task.cancel(true);
    }
    if (!l)
    {
      if (s) {
        return;
      }
      update(true);
      paramCharSequence = paramCharSequence.toString().trim();
      r = SearchFragment.SearchType.a(paramCharSequence);
      paramCharSequence = r.b(paramCharSequence);
      if (paramCharSequence.length() > 2)
      {
        task = new SearchFragment.1(this);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[] { paramCharSequence });
        return;
      }
      if (paramCharSequence.length() > 0)
      {
        a.run(r.get(paramCharSequence));
        return;
      }
      update(false);
    }
  }
  
  public void update(boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      if (!i)
      {
        Recent localRecent = new Recent(getActivity(), "recent_searches");
        a.a(localRecent.read());
        i = true;
      }
      recentTitle.setVisibility(0);
      recentClear.setVisibility(0);
      return;
    }
    i = false;
    recentTitle.setVisibility(8);
    recentClear.setVisibility(8);
  }
}
