package com.deviantart.android.android.package_14.oauth;

import android.util.Base64;
import com.deviantart.android.android.package_14.config.DVNTAPIConfig;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig;
import com.deviantart.android.android.package_14.model.DVNTEnrichedToken;
import com.google.common.base.Preconditions;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

public class DVNTOAuthHelper
{
  private final String CHARSET = "UTF-8";
  private final Map<String, String> ENCODING_REPLACE_MAP;
  public final double LIFETIME_MODIFIER = 0.75D;
  public final List<String> SCOPE_WHITE_LIST = Arrays.asList(new String[] { "challenge" });
  
  public DVNTOAuthHelper()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("+", "%20");
    localHashMap.put("%7E", "~");
    localHashMap.put("*", "%2A");
    ENCODING_REPLACE_MAP = Collections.unmodifiableMap(localHashMap);
  }
  
  private String encode(String paramString1, String paramString2, String paramString3)
  {
    return paramString1.replaceAll(Pattern.quote(paramString2), paramString3);
  }
  
  private String encodeValue(String paramString)
  {
    Preconditions.checkNotNull(paramString, "Cannot encode null object");
    try
    {
      paramString = URLEncoder.encode(paramString, "UTF-8");
      Iterator localIterator = ENCODING_REPLACE_MAP.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        paramString = encode(paramString, (String)localEntry.getKey(), (String)localEntry.getValue());
      }
      return paramString;
    }
    catch (UnsupportedEncodingException paramString)
    {
      throw new RuntimeException("Charset not found while encoding string: UTF-8", paramString);
    }
  }
  
  public String buildAuthorizationURL(DVNTAPIConfig paramDVNTAPIConfig)
  {
    return String.format("https://www.deviantart.com/oauth2/authorize?response_type=code&client_id=%s&redirect_uri=%s&scope=%s", new Object[] { paramDVNTAPIConfig.getClientId(), encodeValue(paramDVNTAPIConfig.getRedirectURI()), encodeValue(paramDVNTAPIConfig.getScope()) });
  }
  
  public String encodeCredentialsForBasicAuthorization(String paramString1, String paramString2)
  {
    paramString1 = paramString1 + ":" + paramString2;
    return "Basic " + Base64.encodeToString(paramString1.getBytes(), 0);
  }
  
  public boolean hasTokenExpired(DVNTEnrichedToken paramDVNTEnrichedToken)
  {
    return new Date().getTime() > Math.round(paramDVNTEnrichedToken.getLifeTimeInSeconds().longValue() * 1000L * 0.75D) + paramDVNTEnrichedToken.getRetrievalTimeInMillis().longValue();
  }
  
  public boolean hasTokenIncompatibleScope(DVNTEnrichedToken paramDVNTEnrichedToken, String paramString)
  {
    paramDVNTEnrichedToken = paramDVNTEnrichedToken.getScope();
    if (paramDVNTEnrichedToken == null) {
      return true;
    }
    paramString = new ArrayList(Arrays.asList(paramString.split(" ")));
    paramString.removeAll(SCOPE_WHITE_LIST);
    return !Arrays.asList(paramDVNTEnrichedToken.split(" ")).containsAll(paramString);
  }
}
