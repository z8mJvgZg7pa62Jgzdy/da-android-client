package com.deviantart.android.damobile.adapter.recyclerview;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.DeviationTorpedoPreviewItem;
import com.deviantart.android.damobile.view.thirdparty.ByteVector;
import com.deviantart.android.damobile.view.thirdparty.GomFactory;
import com.deviantart.android.damobile.view.thirdparty.GomType;

public class DeviationPreviewAdapter
  extends DAStateRecyclerViewAdapter<DeviationTorpedoPreviewItem>
{
  public DeviationPreviewAdapter(Stream paramStream)
  {
    super(paramStream);
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    return GomFactory.a(GomType.b).b(paramViewGroup.getContext(), paramViewGroup);
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    View localView = itemView;
    DeviationTorpedoPreviewItem localDeviationTorpedoPreviewItem = (DeviationTorpedoPreviewItem)c.get(paramInt);
    GomFactory.a(GomType.b).b(localView.getContext(), localDeviationTorpedoPreviewItem, paramViewHolder);
  }
}
