package com.deviantart.android.damobile.adapter.recyclerview;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import com.deviantart.android.android.package_14.model.DVNTComment;
import com.deviantart.android.android.package_14.model.DVNTCommentContextResponse.DVNTCommentContext;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.Stream.Notifiable;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnLoadedFocusedComment;
import com.deviantart.android.damobile.util.CommentType;
import com.deviantart.android.damobile.util.threaditem.CommentItem;
import com.deviantart.android.damobile.view.LoadMoreSiblingsButton;
import com.deviantart.android.damobile.view.NewParentCommentListener;
import com.deviantart.android.damobile.view.thirdparty.comment.viewholder.CommentViewHolderBase;
import com.deviantart.android.damobile.view.thirdparty.comment.viewholder.RegularCommentViewHolder;
import com.deviantart.datoolkit.logger.DVNTLog;
import com.squareup.otto.Bus;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class ContextualCommentAdapter
  extends BaseCommentAdapter<com.deviantart.android.sdk.api.model.DVNTCommentContextResponse>
{
  private HashSet<String> a = new HashSet();
  private boolean b;
  private boolean c = true;
  private boolean d;
  private boolean e;
  private boolean f;
  private DVNTComment g;
  private NewParentCommentListener l;
  private Stream.Notifiable o;
  private String q;
  private boolean r = false;
  
  private ContextualCommentAdapter(StreamLoader paramStreamLoader, String paramString1, NewParentCommentListener paramNewParentCommentListener, Stream.Notifiable paramNotifiable, boolean paramBoolean, CommentType paramCommentType, String paramString2, String paramString3)
  {
    super(paramStreamLoader, paramCommentType, paramString2, paramString3);
    q = paramString1;
    o = paramNotifiable;
    l = paramNewParentCommentListener;
    b = paramBoolean;
    if (paramString1 != null) {
      StreamCacher.loadAllResources(paramStreamLoader);
    }
  }
  
  private boolean a(int paramInt)
  {
    return (e) && (paramInt == e() - 1);
  }
  
  private void e(Context paramContext)
  {
    if (DVNTContextUtils.isContextDead(paramContext)) {
      return;
    }
    get().a(paramContext, o, false, d);
  }
  
  private boolean f(int paramInt)
  {
    if ((g != null) && (f) && (paramInt == 1)) {
      return true;
    }
    return (g == null) && (f) && (paramInt == 0);
  }
  
  public int a(List paramList)
  {
    Object localObject1 = new ArrayList();
    label130:
    Object localObject2;
    if (d)
    {
      paramList = (com.deviantart.android.android.package_14.model.DVNTCommentContextResponse)paramList.get(0);
      f = paramList.getHasLess();
      if (c)
      {
        f = paramList.getHasLess();
        e = paramList.getHasMore();
        c = false;
      }
      g = paramList.getContext().getParent();
      l.c(g);
      if (g != null)
      {
        g.setLevel(Integer.valueOf(0));
        ((ArrayList)localObject1).add(g);
      }
      if (b) {
        break label241;
      }
      paramList = paramList.getThread().iterator();
      if (!paramList.hasNext()) {
        break label241;
      }
      localObject2 = (DVNTComment)paramList.next();
      if (g == null) {
        break label229;
      }
      ((DVNTComment)localObject2).setParentId(g.getCommentId());
      ((DVNTComment)localObject2).setLevel(Integer.valueOf(g.getLevel().intValue() + 1));
    }
    for (;;)
    {
      ((ArrayList)localObject1).add(localObject2);
      break label130;
      paramList = (com.deviantart.android.android.package_14.model.DVNTCommentContextResponse)paramList.get(paramList.size() - 1);
      e = paramList.getHasMore();
      break;
      label229:
      ((DVNTComment)localObject2).setLevel(Integer.valueOf(0));
    }
    label241:
    paramList = ((ArrayList)localObject1).iterator();
    int i = 0;
    while (paramList.hasNext())
    {
      localObject1 = (DVNTComment)paramList.next();
      boolean bool;
      if (!c.equals())
      {
        bool = true;
        label281:
        localObject2 = new CommentItem((DVNTComment)localObject1, bool);
        if (a.contains(((CommentItem)localObject2).c())) {
          continue;
        }
        ((CommentItem)localObject2).add(((DVNTComment)localObject1).getLevel());
        if (!d) {
          break label380;
        }
        if (g == null) {
          break label367;
        }
        c.add(1, localObject2);
      }
      for (;;)
      {
        a.add(((CommentItem)localObject2).c());
        i += 1;
        break;
        bool = false;
        break label281;
        label367:
        c.add(0, localObject2);
        continue;
        label380:
        c.add(localObject2);
      }
    }
    return i;
  }
  
  public void a()
  {
    super.a();
    a.clear();
  }
  
  public void b()
  {
    if ((c != null) && (e < c.size()))
    {
      if (c == null) {
        return;
      }
      int i = a(c.get());
      try
      {
        c();
        e = c.size();
        d = (i + d);
        return;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          new Handler().post(ContextualCommentAdapter..Lambda.1.c(this));
        }
      }
    }
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    super.b(paramViewHolder, paramInt);
    paramViewHolder = (CommentViewHolderBase)paramViewHolder;
    DVNTComment localDVNTComment = ((CommentItem)c.get(paramInt)).intValue();
    View localView;
    if ((paramViewHolder instanceof RegularCommentViewHolder))
    {
      localView = commentHighlight;
      if ((q != null) && (localDVNTComment.getCommentId().equals(q)))
      {
        DVNTLog.get("focus", new Object[] { "found comment!" });
        localView.setVisibility(0);
        BusStation.get().post(new BusStation.OnLoadedFocusedComment(localDVNTComment));
      }
    }
    else
    {
      loadMoreSiblingsTop.dismissProgress();
      loadMoreSiblingsBottom.dismissProgress();
      if (!f(paramInt)) {
        break label179;
      }
      loadMoreSiblingsTop.setVisibility(0);
      loadMoreSiblingsTop.setOnClickListener(ContextualCommentAdapter..Lambda.2.finish(this, paramViewHolder));
    }
    for (;;)
    {
      if (!a(paramInt)) {
        break label191;
      }
      loadMoreSiblingsBottom.setVisibility(0);
      loadMoreSiblingsBottom.setOnClickListener(ContextualCommentAdapter..Lambda.3.finish(this, paramViewHolder));
      return;
      localView.setVisibility(4);
      break;
      label179:
      loadMoreSiblingsTop.setVisibility(8);
    }
    label191:
    loadMoreSiblingsBottom.setVisibility(8);
  }
  
  protected void b(CommentItem paramCommentItem)
  {
    if ((!b) && (!r) && (g != null) && (paramCommentItem.c().equals(g.getCommentId())))
    {
      paramCommentItem.d(Boolean.valueOf(true));
      r = true;
    }
  }
  
  public String getAttributeValue()
  {
    if (g != null) {
      return g.getCommentId();
    }
    return null;
  }
}
