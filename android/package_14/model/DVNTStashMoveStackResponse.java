package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTStashMoveStackResponse
  implements Serializable
{
  @SerializedName("changes")
  DVNTStashItem.List changes;
  @SerializedName("target")
  DVNTStashItem target;
  
  public DVNTStashMoveStackResponse() {}
  
  public DVNTStashItem.List getChanges()
  {
    return changes;
  }
  
  public DVNTStashItem getTarget()
  {
    return target;
  }
  
  public void setChanges(DVNTStashItem.List paramList)
  {
    changes = paramList;
  }
  
  public void setTarget(DVNTStashItem paramDVNTStashItem)
  {
    target = paramDVNTStashItem;
  }
}
