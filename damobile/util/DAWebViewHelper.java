package com.deviantart.android.damobile.util;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig;
import java.util.ArrayList;

@SuppressLint({"SetJavaScriptEnabled"})
public class DAWebViewHelper
{
  public DAWebViewHelper() {}
  
  public static void onCreateView(WebView paramWebView, String paramString1, String paramString2, DAWebViewHelper.WebViewParams paramWebViewParams)
  {
    String str1 = paramString2;
    if (paramString2 == null) {
      str1 = "";
    }
    paramString2 = paramWebViewParams;
    if (paramWebViewParams == null) {
      paramString2 = new DAWebViewHelper.WebViewParams.Builder().a();
    }
    String str2 = "";
    paramWebViewParams = str2;
    if (DAWebViewHelper.WebViewParams.restoreData(paramString2) != null)
    {
      paramWebViewParams = str2;
      if (DAWebViewHelper.WebViewParams.restoreData(paramString2).size() > 0) {
        paramWebViewParams = "<link href=\"http://fonts.googleapis.com/css?family=" + TextUtils.join("|", DAWebViewHelper.WebViewParams.restoreData(paramString2).toArray()) + "\" rel=\"stylesheet\" type=\"text/css\">\n";
      }
    }
    str2 = "";
    if (str1.contains("Depot New")) {
      str2 = "@font-face {\n    font-family: 'Depot New';\n    src: url('fonts/style_190340.ttf') format('truetype'); \n}\n@font-face {\n    font-family: 'Depot New Light';\n    src: url('fonts/style_190342.ttf') format('truetype'); \n}\n@font-face {\n    font-family: 'Depot New Bold';\n    src: url('fonts/style_190314.ttf') format('truetype'); \n}\n";
    }
    String str3 = "";
    if (!DVNTAbstractAsyncAPI.getConfig().getShowMatureContent().booleanValue()) {
      str3 = "maturefilter maturehide";
    }
    String str4 = "";
    if (paramString2.isImportant().booleanValue()) {
      str4 = "<link href=\"http://st.deviantart.net/native/mobile_native.css?1\" rel=\"stylesheet\" type=\"text/css\">\n";
    }
    paramString1 = "<html><head><meta name=\"viewport\" content=\"width=device-width, user-scalable=no\">" + str4 + paramWebViewParams + "<style type=\"text/css\">" + str1 + str2 + "@font-face {font-family: 'Calibre';src: url('fonts/Calibre-Regular.otf') format('opentype');}body {color: " + paramString2.c() + ";background-color: " + paramString2.b() + ";font-family: 'Calibre';padding: " + paramString2.getCount() + "px " + paramString2.a() + "px " + paramString2.getSecond() + "px " + paramString2.d() + "px ;}a { color: " + paramString2.c() + "; }</style></head><body class=\"" + str3 + "\" style = \"margin: 0px\">" + paramString1 + "</body></html>";
    paramWebViewParams = paramWebView.getSettings();
    try
    {
      paramWebViewParams.setDefaultFontSize(paramString2.get().intValue());
      paramWebViewParams.setSupportZoom(false);
      paramWebViewParams.setJavaScriptEnabled(true);
      paramWebView.setBackgroundColor(0);
      paramWebView.setWebViewClient(new DAWebViewHelper.1());
      paramWebView.loadDataWithBaseURL("file:///android_asset/", paramString1, "text/html", null, null);
      return;
    }
    catch (NullPointerException paramString2)
    {
      for (;;)
      {
        Log.e("DAWebViewHelper", "Error during updating app settings: " + paramString2.toString());
      }
    }
  }
}
