package com.deviantart.android.damobile.adapter.recyclerview;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.view.WatchFeedItemLayout;

public class WatchFeedAdapter
  extends DAStateRecyclerViewAdapter<com.deviantart.android.sdk.api.model.DVNTFeedItem>
{
  public WatchFeedAdapter(Stream paramStream)
  {
    super(paramStream);
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    return new WatchFeedAdapter.WatchFeedViewHolder(this, new WatchFeedItemLayout(paramViewGroup.getContext()));
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    paramViewHolder = (WatchFeedAdapter.WatchFeedViewHolder)paramViewHolder;
    com.deviantart.android.android.package_14.model.DVNTFeedItem localDVNTFeedItem = (com.deviantart.android.android.package_14.model.DVNTFeedItem)c.get(paramInt);
    j.a(localDVNTFeedItem, null);
  }
}
