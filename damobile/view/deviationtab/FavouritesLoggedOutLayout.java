package com.deviantart.android.damobile.view.deviationtab;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.damobile.activity.HomeActivity;
import com.deviantart.android.damobile.activity.SignUpActivity;

public class FavouritesLoggedOutLayout
  extends FrameLayout
{
  public FavouritesLoggedOutLayout(Context paramContext, ViewGroup paramViewGroup)
  {
    super(paramContext);
    paramContext = LayoutInflater.from(paramContext).inflate(2130968736, paramViewGroup, false);
    ButterKnife.bind(this, paramContext);
    ((TextView)ButterKnife.findById(paramContext, 2131689971)).setText(2131231064);
    addView(paramContext);
  }
  
  public void onJoinClick()
  {
    Intent localIntent = new Intent(getContext(), SignUpActivity.class);
    Activity localActivity = (Activity)getContext();
    if (localActivity.getClass() == HomeActivity.class)
    {
      localActivity.startActivityForResult(localIntent, 104);
      return;
    }
    localActivity.startActivity(localIntent);
  }
  
  public void onLoginClick()
  {
    DVNTAbstractAsyncAPI.graduate(getContext());
  }
}
