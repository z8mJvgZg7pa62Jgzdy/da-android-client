package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.widget.Toast;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.util.PermissionUtil;
import com.deviantart.android.damobile.util.PhotoUtils;
import com.deviantart.android.damobile.util.PhotoUtils.PhotoSourceDialogFragment;
import com.deviantart.android.damobile.util.PhotoUtils.PhotoSourceDialogFragment.DialogBuilder;
import com.deviantart.datoolkit.logger.DVNTLog;
import java.io.File;

public abstract class PhotoFragment
  extends DABaseFragment
{
  protected PhotoFragment.OnImageFileReadyListener a;
  private File b;
  
  public PhotoFragment() {}
  
  public void a(View paramView, PhotoFragment.OnImageFileReadyListener paramOnImageFileReadyListener)
  {
    paramView.setOnClickListener(new PhotoFragment.2(this, paramOnImageFileReadyListener));
  }
  
  public void a(View paramView, boolean paramBoolean, PhotoFragment.OnImageFileReadyListener paramOnImageFileReadyListener)
  {
    if (b != null) {
      paramOnImageFileReadyListener.b(b);
    }
    paramView.setOnClickListener(new PhotoFragment.1(this, paramOnImageFileReadyListener, paramBoolean));
  }
  
  public void a(File paramFile)
  {
    b = paramFile;
  }
  
  public void b(boolean paramBoolean, PhotoFragment.OnImageFileReadyListener paramOnImageFileReadyListener)
  {
    a = paramOnImageFileReadyListener;
    new PhotoUtils.PhotoSourceDialogFragment.DialogBuilder().b(paramBoolean).a().a(this, "photo_source_fragment");
  }
  
  public File c()
  {
    return b;
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt2 == -1)
    {
      if ((paramInt1 != 103) && (paramInt1 != 102)) {
        return;
      }
      if (paramInt1 == 103) {}
      try
      {
        paramIntent = PhotoUtils.getBitmap(getActivity().getContentResolver(), paramIntent.getData());
        b = paramIntent;
        if (a == null)
        {
          DVNTLog.get("listener has not been defined", new Object[0]);
          return;
        }
      }
      catch (Exception paramIntent)
      {
        Toast.makeText(getActivity(), getResources().getString(2131230942) + paramIntent.getMessage(), 0).show();
        return;
      }
      a.b(b);
    }
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    if (DVNTContextUtils.isContextDead(getActivity())) {
      return;
    }
    switch (paramInt)
    {
    default: 
      
    case 0: 
      if (PermissionUtil.execute(this, paramArrayOfString, paramArrayOfInt, 2131231189, 2131231196))
      {
        PhotoUtils.onOptionsItemSelected(this);
        return;
      }
      break;
    case 1: 
      if (PermissionUtil.execute(this, paramArrayOfString, paramArrayOfInt, 2131231190, 2131231197)) {
        PhotoUtils.onActivityResult(this);
      }
      break;
    }
  }
  
  public void saveSession()
  {
    if (a == null) {
      return;
    }
    a.addSession();
  }
  
  public void updated()
  {
    b = null;
    if (a == null) {
      return;
    }
    a.onPageSelected();
  }
}
