package com.deviantart.android.damobile.adapter.recyclerview;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.deviantart.android.damobile.adapter.HeaderFooterRecyclerViewAdapter;
import com.deviantart.android.damobile.stream.Stream;
import java.util.ArrayList;

public abstract class DAStateRecyclerViewAdapter<T>
  extends HeaderFooterRecyclerViewAdapter
{
  private View a;
  protected final Stream<T> c;
  protected int e = 0;
  private ArrayList<View> m = new ArrayList();
  private ArrayList<View> n = new ArrayList();
  
  public DAStateRecyclerViewAdapter(Stream paramStream)
  {
    c = paramStream;
    b();
  }
  
  private DAStateRecyclerViewAdapter.HeaderFooterViewHolder a(Context paramContext)
  {
    paramContext = new FrameLayout(paramContext);
    paramContext.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
    return new DAStateRecyclerViewAdapter.HeaderFooterViewHolder(paramContext);
  }
  
  public void a()
  {
    e = 0;
    c.a();
    try
    {
      c();
      return;
    }
    catch (Exception localException)
    {
      new Handler().post(DAStateRecyclerViewAdapter..Lambda.8.c(this));
    }
  }
  
  protected void a(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    if (a != null) {}
    for (int i = 1; (i == 0) && (m.isEmpty()); i = 0) {
      return;
    }
    DAStateRecyclerViewAdapter.HeaderFooterViewHolder localHeaderFooterViewHolder = (DAStateRecyclerViewAdapter.HeaderFooterViewHolder)paramViewHolder;
    g.removeAllViews();
    if ((paramInt == 0) && (i != 0))
    {
      View localView = a;
      paramViewHolder = this;
      if (localView.getParent() != null)
      {
        Log.e("StateRecyclerView", "state view was not removed from parent.");
        ((ViewGroup)a.getParent()).removeView(a);
      }
      g.addView(a);
      return;
    }
    int j = paramInt;
    if (i != 0) {
      j = paramInt - 1;
    }
    if (m.size() >= j)
    {
      paramViewHolder = (View)m.get(j);
      if (paramViewHolder.getParent() != null)
      {
        Log.e("StateRecyclerView", "footer view was not removed from parent.");
        ((ViewGroup)paramViewHolder.getParent()).removeView(paramViewHolder);
      }
      g.addView(paramViewHolder);
    }
  }
  
  public void a(View paramView)
  {
    n.add(paramView);
    try
    {
      c();
      return;
    }
    catch (Exception paramView)
    {
      new Handler().post(DAStateRecyclerViewAdapter..Lambda.1.c(this));
    }
  }
  
  public int add()
  {
    if (a == null) {}
    for (int i = 0;; i = 1) {
      return i + m.size();
    }
  }
  
  public void b()
  {
    if (c != null)
    {
      if (e >= c.size()) {
        return;
      }
      int i = c.size();
      int j = e;
      int k = e;
      e = c.size();
      try
      {
        append(k, i - j);
        return;
      }
      catch (Exception localException)
      {
        new Handler().post(DAStateRecyclerViewAdapter..Lambda.7.c(this));
      }
    }
  }
  
  public void b(View paramView)
  {
    int j = 1;
    int i;
    if (a != null)
    {
      i = 1;
      if (paramView == null) {
        break label39;
      }
    }
    for (;;)
    {
      if ((i == 0) || (j == 0)) {
        break label44;
      }
      a = paramView;
      try
      {
        e(0);
        return;
      }
      catch (Exception paramView)
      {
        label39:
        new Handler().post(DAStateRecyclerViewAdapter..Lambda.9.c(this));
        return;
      }
      i = 0;
      break;
      j = 0;
    }
    label44:
    if (i != 0)
    {
      c(0);
      a = null;
      return;
    }
    if (j != 0)
    {
      a = paramView;
      replace(0);
    }
  }
  
  protected RecyclerView.ViewHolder c(ViewGroup paramViewGroup, int paramInt)
  {
    return a(paramViewGroup.getContext());
  }
  
  protected void c(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    if (n != null)
    {
      if (paramInt >= n.size()) {
        return;
      }
      View localView = (View)n.get(paramInt);
      if (localView.getParent() != null)
      {
        Log.e("StateRecyclerView", "header view was not removed from parent.");
        ((ViewGroup)localView.getParent()).removeView(localView);
      }
      paramViewHolder = (DAStateRecyclerViewAdapter.HeaderFooterViewHolder)paramViewHolder;
      g.removeAllViews();
      g.addView(localView);
    }
  }
  
  public void clear()
  {
    n.clear();
    try
    {
      c();
      return;
    }
    catch (Exception localException)
    {
      new Handler().post(DAStateRecyclerViewAdapter..Lambda.5.c(this));
    }
  }
  
  public void clear(View paramView)
  {
    m.add(paramView);
    try
    {
      c();
      return;
    }
    catch (Exception paramView)
    {
      new Handler().post(DAStateRecyclerViewAdapter..Lambda.4.c(this));
    }
  }
  
  public int e()
  {
    return e;
  }
  
  public Stream get()
  {
    return c;
  }
  
  public boolean next()
  {
    if (c == null) {
      return false;
    }
    if (c.size() != e) {
      a();
    }
    if (c.size() == 0)
    {
      e = 0;
      return true;
    }
    return false;
  }
  
  protected RecyclerView.ViewHolder onCreateView(ViewGroup paramViewGroup, int paramInt)
  {
    return a(paramViewGroup.getContext());
  }
  
  public void onViewRecycled(RecyclerView.ViewHolder paramViewHolder)
  {
    if ((paramViewHolder instanceof DAStateRecyclerViewAdapter.HeaderFooterViewHolder)) {
      g.removeAllViews();
    }
    super.onViewRecycled(paramViewHolder);
  }
  
  public void readMessage()
  {
    int i = add();
    try
    {
      a(0, i);
      m.clear();
      a = null;
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        new Handler().post(DAStateRecyclerViewAdapter..Lambda.6.readMessage(this));
      }
    }
  }
  
  public int size()
  {
    return n.size();
  }
  
  public void visitMaxs()
  {
    int i = 0;
    if (a != null) {
      i = 1;
    }
    if (i != 0) {}
    try
    {
      c(0);
      a = null;
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        new Handler().post(DAStateRecyclerViewAdapter..Lambda.10.c(this));
      }
    }
  }
}
