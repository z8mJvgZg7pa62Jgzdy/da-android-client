package com.deviantart.android.damobile.view;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.util.DAFont;
import com.deviantart.android.damobile.util.UserUtils;
import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;

public class VerificationBanner
  extends RelativeLayout
{
  VerificationBanner.BannerStatusListener hand;
  
  public VerificationBanner(Context paramContext)
  {
    super(paramContext);
    b();
  }
  
  public VerificationBanner(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    b();
  }
  
  private Spannable a(String paramString)
  {
    String str = getContext().getString(2131231407).replace("{email}", paramString);
    Typeface localTypeface = DAFont.o.get(getContext());
    SpannableString localSpannableString = new SpannableString(str);
    localSpannableString.setSpan(new CalligraphyTypefaceSpan(localTypeface), str.indexOf(paramString), str.indexOf(paramString) + paramString.length(), 33);
    return localSpannableString;
  }
  
  public void b()
  {
    LayoutInflater.from(getContext()).inflate(2130968797, this, true);
    TextView localTextView1 = (TextView)ButterKnife.findById(this, 2131690125);
    ImageView localImageView = (ImageView)ButterKnife.findById(this, 2131690123);
    View localView = ButterKnife.findById(this, 2131690124);
    TextView localTextView2 = (TextView)ButterKnife.findById(this, 2131690126);
    TextView localTextView3 = (TextView)ButterKnife.findById(this, 2131690127);
    if (UserUtils.c != null) {
      localTextView1.setText(a(UserUtils.c));
    }
    localImageView.setSelected(false);
    localImageView.setOnClickListener(new VerificationBanner.1(this, localImageView, localView));
    localTextView2.setOnClickListener(new VerificationBanner.3(this, new VerificationBanner.2(this, localTextView2)));
    localTextView3.setOnClickListener(new VerificationBanner.4(this));
  }
  
  public void setBannerStatusListener(VerificationBanner.BannerStatusListener paramBannerStatusListener)
  {
    hand = paramBannerStatusListener;
  }
}
