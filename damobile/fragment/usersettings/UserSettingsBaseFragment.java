package com.deviantart.android.damobile.fragment.usersettings;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTUserProfile;
import com.deviantart.android.android.package_14.model.DVNTWhoAmIResponse;
import com.deviantart.android.damobile.activity.UserSettingsActivity;
import com.deviantart.android.damobile.activity.UserSettingsActivity.OnBackResult;
import com.deviantart.android.damobile.fragment.DABaseFragment;
import com.deviantart.android.damobile.fragment.PhotoFragment;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnUserSettingChangeEvent;
import com.squareup.otto.Bus;

public abstract class UserSettingsBaseFragment
  extends PhotoFragment
{
  private UserSettingsBaseFragment.Subscriber ctx = new UserSettingsBaseFragment.Subscriber(this, null);
  protected boolean d = false;
  protected boolean e = false;
  
  public UserSettingsBaseFragment() {}
  
  public UserSettingsActivity.OnBackResult a()
  {
    return UserSettingsActivity.OnBackResult.o;
  }
  
  protected void add(String paramString1, String paramString2, String paramString3, String paramString4, Boolean paramBoolean)
  {
    DVNTAsyncAPI.updateAccount(paramString1, paramString2, paramString3, paramString4, paramBoolean).call(getActivity(), new UserSettingsBaseFragment.3(this));
  }
  
  protected void b() {}
  
  protected void b(DVNTWhoAmIResponse paramDVNTWhoAmIResponse) {}
  
  public void clear(BusStation.OnUserSettingChangeEvent paramOnUserSettingChangeEvent)
  {
    ((UserSettingsActivity)getActivity()).b(paramOnUserSettingChangeEvent.getValue());
  }
  
  protected void filterSelected()
  {
    DVNTCommonAsyncAPI.whoAmI().call(getActivity(), new UserSettingsBaseFragment.2(this));
  }
  
  protected void loadNotifications()
  {
    DVNTCommonAsyncAPI.userProfile("", true, true).call(getActivity(), new UserSettingsBaseFragment.1(this));
  }
  
  protected void newInstance(Boolean paramBoolean, Integer paramInteger1, Integer paramInteger2, String paramString1, String paramString2, Integer paramInteger3, String paramString3, String paramString4)
  {
    DVNTCommonAsyncAPI.updateUserProfile(paramBoolean.booleanValue(), paramInteger1, paramInteger2, paramString1, paramString2, paramInteger3, paramString3, paramString4).call(getActivity(), new UserSettingsBaseFragment.4(this));
  }
  
  protected void onClick(DVNTUserProfile paramDVNTUserProfile) {}
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    BusStation.get().register(ctx);
    return super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    BusStation.get().unregister(ctx);
  }
  
  protected void onPostExecute(UserSettingsBaseFragment paramUserSettingsBaseFragment, String paramString)
  {
    FragmentManager localFragmentManager = getActivity().getFragmentManager();
    Fragment localFragment = localFragmentManager.findFragmentByTag(paramString);
    UserSettingsBaseFragment localUserSettingsBaseFragment = paramUserSettingsBaseFragment;
    if (paramString != null)
    {
      localUserSettingsBaseFragment = paramUserSettingsBaseFragment;
      if (!paramString.isEmpty())
      {
        localUserSettingsBaseFragment = paramUserSettingsBaseFragment;
        if (localFragment != null) {
          localUserSettingsBaseFragment = (UserSettingsBaseFragment)localFragment;
        }
      }
    }
    localFragmentManager.beginTransaction().addToBackStack(paramString).setCustomAnimations(2131034139, 2131034143, 2131034141, 2131034145).replace(2131689591, localUserSettingsBaseFragment, paramString).commitAllowingStateLoss();
  }
  
  public void onResume()
  {
    super.onResume();
    ((UserSettingsActivity)getActivity()).a(this);
  }
}
