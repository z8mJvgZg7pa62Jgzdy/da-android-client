package com.deviantart.android.damobile.view.ewok;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ProgressBar;
import com.deviantart.android.android.package_14.DVNTCommonAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.damobile.util.DAWebViewHelper.WebViewParams;
import com.deviantart.android.damobile.util.DAWebViewHelper.WebViewParams.Builder;
import com.deviantart.android.damobile.util.DeviationType;
import com.deviantart.android.damobile.util.ViewHelper;
import com.deviantart.android.damobile.view.LiteratureThumbView;

public class LiteratureDeviationEwok
  extends DeviationEwok
{
  public LiteratureDeviationEwok(DVNTDeviation paramDVNTDeviation)
  {
    super(paramDVNTDeviation);
  }
  
  public View a(Activity paramActivity, ViewGroup paramViewGroup)
  {
    return b(paramActivity, null, 0, 0);
  }
  
  public View b(Activity paramActivity, ViewGroup paramViewGroup, int paramInt1, int paramInt2)
  {
    return new LiteratureThumbView(paramActivity, d);
  }
  
  public View b(Context paramContext)
  {
    View localView = LayoutInflater.from(paramContext).inflate(2130968662, null);
    ViewHelper.format(localView);
    WebView localWebView = (WebView)localView.findViewById(2131689725);
    ProgressBar localProgressBar = (ProgressBar)localView.findViewById(2131689726);
    DAWebViewHelper.WebViewParams localWebViewParams = new DAWebViewHelper.WebViewParams.Builder().b("transparent").a("#" + Integer.toHexString(paramContext.getResources().getColor(2131558505))).b(Integer.valueOf(20)).a(Boolean.valueOf(true)).a();
    localProgressBar.setVisibility(0);
    DVNTCommonAsyncAPI.deviationContent(d.getId()).call(paramContext, new LiteratureDeviationEwok.1(this, paramContext, localWebView, localProgressBar, localWebViewParams));
    return localView;
  }
  
  public DeviationType b()
  {
    return DeviationType.b;
  }
}
