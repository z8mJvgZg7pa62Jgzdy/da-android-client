package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.android.package_14.network.wrapper.DVNTResultsWrapper;
import com.deviantart.android.sdk.api.network.request.GroupableRequest;
import com.google.common.base.Objects;
import com.google.common.reflect.TypeToken;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.HashMap;

public class DVNTSearchFriendsRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTUser.List>
  implements GroupableRequest
{
  final String userQuery;
  
  public DVNTSearchFriendsRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTUser.List.class);
    userQuery = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject != null) && (getClass() == paramObject.getClass()) && (super.equals(paramObject)))
    {
      paramObject = (DVNTSearchFriendsRequestV1)paramObject;
      return Objects.append(userQuery, userQuery);
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "searchfriends" + userQuery;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 60000L;
  }
  
  public HashMap getGETArguments()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("query", userQuery);
    return localHashMap;
  }
  
  public String getGroupEndpoint()
  {
    return "/api/v1/oauth2/user/friends/search";
  }
  
  public HashMap getPOSTArguments()
  {
    return null;
  }
  
  public TypeToken getWrapperTypeToken()
  {
    return new DVNTSearchFriendsRequestV1.1(this);
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), userQuery });
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTUser.List sendRequest(String paramString)
  {
    return (com.deviantart.android.android.package_14.model.DVNTUser.List)((DVNTServiceV1)getService()).searchFriends(paramString, userQuery).getResults();
  }
}
