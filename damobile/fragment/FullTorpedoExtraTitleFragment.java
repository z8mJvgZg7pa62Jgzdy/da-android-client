package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.loader.FolderStreamLoader;

public class FullTorpedoExtraTitleFragment
  extends FullTorpedoFragment
{
  private String a;
  
  public FullTorpedoExtraTitleFragment() {}
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    if (DVNTContextUtils.isContextDead(getActivity())) {
      return paramLayoutInflater;
    }
    paramViewGroup = ((BaseActivity)getActivity()).getSupportActionBar();
    if (paramViewGroup == null) {
      return paramLayoutInflater;
    }
    if (a != null) {
      paramViewGroup.setTitle(a);
    }
    ((FolderStreamLoader)a.length()).d(FullTorpedoExtraTitleFragment..Lambda.1.a(this));
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    if (a != null) {
      ((FolderStreamLoader)a.length()).d(null);
    }
    super.onDestroyView();
  }
}
