package com.deviantart.android.damobile.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;

public class LoadMoreSiblingsButton
  extends FrameLayout
{
  @Bind({2131689611})
  ProgressBar progressBar;
  @Bind({2131689612})
  TextView textView;
  
  public LoadMoreSiblingsButton(Context paramContext)
  {
    super(paramContext);
    initLayout();
  }
  
  public LoadMoreSiblingsButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    initLayout();
  }
  
  public LoadMoreSiblingsButton(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    initLayout();
  }
  
  private void initLayout()
  {
    LayoutInflater.from(getContext()).inflate(2130968614, this, true);
    ButterKnife.bind(this);
  }
  
  public void dismissProgress()
  {
    progressBar.setVisibility(8);
  }
  
  public void setFrom()
  {
    progressBar.setVisibility(0);
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener)
  {
    textView.setOnClickListener(paramOnClickListener);
  }
}
