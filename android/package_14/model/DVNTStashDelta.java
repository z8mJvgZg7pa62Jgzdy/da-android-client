package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTStashDelta
  implements Serializable
{
  @SerializedName("cursor")
  private String cursor;
  @SerializedName("entries")
  private DVNTStashEntry.List entries;
  @SerializedName("has_more")
  private Boolean hasMore;
  @SerializedName("next_offset")
  private Integer nextOffset;
  @SerializedName("reset")
  private Boolean reset;
  
  public DVNTStashDelta() {}
  
  public String getCursor()
  {
    return cursor;
  }
  
  public DVNTStashEntry.List getEntries()
  {
    return entries;
  }
  
  public Boolean getHasMore()
  {
    return hasMore;
  }
  
  public Integer getNextOffset()
  {
    return nextOffset;
  }
  
  public Boolean getReset()
  {
    return reset;
  }
  
  public void setCursor(String paramString)
  {
    cursor = paramString;
  }
  
  public void setEntries(DVNTStashEntry.List paramList)
  {
    entries = paramList;
  }
  
  public void setHasMore(Boolean paramBoolean)
  {
    hasMore = paramBoolean;
  }
  
  public void setNextOffset(Integer paramInteger)
  {
    nextOffset = paramInteger;
  }
  
  public void setReset(Boolean paramBoolean)
  {
    reset = paramBoolean;
  }
}
