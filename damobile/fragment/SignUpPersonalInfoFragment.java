package com.deviantart.android.damobile.fragment;

import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.Fragment;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTAccountValidateResponse;
import com.deviantart.android.damobile.activity.MultiStepActivity;
import com.deviantart.android.damobile.util.DAClickableSpan;
import com.deviantart.android.damobile.util.Keyboard;
import com.deviantart.android.damobile.util.SignUpHelper;
import com.deviantart.android.damobile.util.SignUpHelper.APIValidationType;
import com.deviantart.android.damobile.util.SignUpHelper.BirthDateDialogFragment;
import com.deviantart.android.damobile.util.SignUpHelper.ValidateUserInfoTask;
import com.deviantart.android.damobile.util.SignUpHelper.ValidationListener;
import com.deviantart.android.damobile.view.EditTextWithClearFocus;
import java.util.Calendar;

public class SignUpPersonalInfoFragment
  extends MultiStepFragment
  implements DatePickerDialog.OnDateSetListener, SignUpHelper.ValidationListener
{
  private final Integer b = Integer.valueOf(13);
  @Bind({2131689799})
  EditTextWithClearFocus birthDateEditText;
  @Bind({2131689802})
  TextView birthDateError;
  @Bind({2131689798})
  TextView birthDateValidator;
  private final String[] c = { "Male", "Female", "Other" };
  private SignUpHelper.ValidateUserInfoTask currentTask;
  @Bind({2131689795})
  EditTextWithClearFocus emailEditText;
  @Bind({2131689797})
  TextView emailError;
  @Bind({2131689796})
  ProgressBar emailProgressBar;
  @Bind({2131689794})
  TextView emailValidator;
  private SignUpHelper.ValidationListener i = this;
  @Bind({2131689779})
  LinearLayout mainLayout;
  private String password;
  @Bind({2131689803})
  SwitchCompat sendEmailNewsButton;
  @Bind({2131689801})
  EditTextWithClearFocus sexEditText;
  @Bind({2131689800})
  TextView sexValidator;
  @Bind({2131689792})
  ImageView signUpDoneButton;
  @Bind({2131689804})
  TextView tosTextView;
  @Bind({2131689793})
  TextView userName;
  
  public SignUpPersonalInfoFragment() {}
  
  private void collapse()
  {
    ImageView localImageView = signUpDoneButton;
    if ((emailValidator.isActivated()) && (birthDateValidator.isActivated()) && (sexValidator.isActivated())) {}
    for (boolean bool = true;; bool = false)
    {
      localImageView.setActivated(bool);
      return;
    }
  }
  
  private String encode(String paramString)
  {
    if (paramString.equals(c[0])) {
      return "m";
    }
    if (paramString.equals(c[1])) {
      return "f";
    }
    if (paramString.equals(c[2])) {
      return "o";
    }
    Log.e("SignUp-encodeSex", "Unknown sex type found");
    return "";
  }
  
  private boolean putBoolean()
  {
    return mainLayout == null;
  }
  
  private boolean update(int paramInt1, int paramInt2, int paramInt3)
  {
    Calendar localCalendar = Calendar.getInstance();
    if (localCalendar.get(1) - b.intValue() != paramInt1) {
      return localCalendar.get(1) - b.intValue() > paramInt1;
    }
    if (localCalendar.get(2) != paramInt2)
    {
      if (localCalendar.get(2) <= paramInt2) {
        return false;
      }
    }
    else if (localCalendar.get(5) < paramInt3) {
      return false;
    }
    return true;
  }
  
  public void b(Bundle paramBundle)
  {
    userName.setText(paramBundle.getString("username"));
    password = paramBundle.getString("password");
  }
  
  public void clickBackButton()
  {
    ((MultiStepActivity)getActivity()).showImage();
  }
  
  public void clickBirthDate()
  {
    SignUpHelper.BirthDateDialogFragment.log1p().showMessage(getFragmentManager(), "signup_birthdate", this);
  }
  
  public void clickDoneButton()
  {
    if (!signUpDoneButton.isActivated()) {
      return;
    }
    if (Keyboard.processRequest(mainLayout)) {
      Keyboard.hideKeyboard(getActivity());
    }
    ((MultiStepActivity)getActivity()).onOptionsItemSelected();
  }
  
  public void clickSex()
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(getActivity());
    localBuilder.setTitle(getString(2131231331)).setItems(c, new SignUpPersonalInfoFragment.2(this));
    localBuilder.create().show();
  }
  
  public void editTextChanged(CharSequence paramCharSequence)
  {
    if (currentTask != null) {
      currentTask.cancel(true);
    }
    if (paramCharSequence.length() == 0)
    {
      emailValidator.setActivated(false);
      emailValidator.setTextColor(getResources().getColor(17170443));
      emailError.setVisibility(8);
      emailProgressBar.setVisibility(8);
    }
    for (;;)
    {
      collapse();
      return;
      emailProgressBar.setVisibility(0);
      currentTask = new SignUpPersonalInfoFragment.3(this);
      currentTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new String[] { paramCharSequence.toString() });
    }
  }
  
  public boolean emailEditTextEditorAction(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 6)
    {
      Keyboard.hideKeyboard(getActivity());
      mainLayout.requestFocus();
    }
    return false;
  }
  
  public void focusChangeEmailEditText(boolean paramBoolean)
  {
    SignUpHelper.onItemSelected(paramBoolean, emailEditText, emailValidator, 2131230876);
  }
  
  public void format(SpannableStringBuilder paramSpannableStringBuilder, String paramString1, String paramString2, String paramString3)
  {
    int j = paramString1.indexOf(paramString2);
    paramSpannableStringBuilder.setSpan(new DAClickableSpan(getActivity(), new SignUpPersonalInfoFragment.1(this, paramString3)), j, paramString2.length() + j, 17);
  }
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setRetainInstance(true);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramLayoutInflater = (ScrollView)paramLayoutInflater.inflate(2130968679, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    paramViewGroup = getString(2131231340);
    paramBundle = new SpannableStringBuilder(paramViewGroup);
    format(paramBundle, paramViewGroup, getString(2131231341), getString(2131231319));
    format(paramBundle, paramViewGroup, getString(2131231338), getString(2131230975));
    tosTextView.setText(paramBundle);
    tosTextView.setMovementMethod(LinkMovementMethod.getInstance());
    mainLayout.requestFocus();
    return paramLayoutInflater;
  }
  
  public void onDateSet(DatePicker paramDatePicker, int paramInt1, int paramInt2, int paramInt3)
  {
    paramDatePicker = paramInt1 + "-" + (paramInt2 + 1) + "-" + paramInt3;
    birthDateValidator.setVisibility(0);
    birthDateEditText.setText(paramDatePicker);
    if (update(paramInt1, paramInt2, paramInt3))
    {
      birthDateValidator.setTextColor(getResources().getColor(2131558484));
      birthDateValidator.setActivated(true);
      birthDateError.setVisibility(8);
    }
    for (;;)
    {
      collapse();
      return;
      birthDateValidator.setTextColor(getResources().getColor(2131558547));
      birthDateValidator.setActivated(false);
      birthDateError.setVisibility(0);
    }
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public void setVideo(DVNTAccountValidateResponse paramDVNTAccountValidateResponse, SignUpHelper.APIValidationType paramAPIValidationType)
  {
    if (emailProgressBar == null) {
      return;
    }
    switch (SignUpPersonalInfoFragment.4.T[paramAPIValidationType.ordinal()])
    {
    default: 
      Log.e("onValidationResult", "unhandled validation type " + paramAPIValidationType);
    }
    for (;;)
    {
      collapse();
      return;
      emailProgressBar.setVisibility(8);
      SignUpHelper.showHelpDialog(getActivity(), emailValidator, emailError, paramDVNTAccountValidateResponse.getEmailValidationResult());
    }
  }
  
  public Bundle updateSettings()
  {
    if (putBoolean()) {
      return null;
    }
    Bundle localBundle = new Bundle();
    localBundle.putString("username", userName.getText().toString());
    localBundle.putString("password", password);
    localBundle.putString("birth_date", birthDateEditText.getText().toString());
    localBundle.putString("sex", encode(sexEditText.getText().toString()));
    localBundle.putString("email", emailEditText.getText().toString());
    localBundle.putBoolean("send_news", sendEmailNewsButton.isSelected());
    return localBundle;
  }
}
