package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTPostStatusRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTPostStatusResponse>
{
  final String body;
  final String repostUUID;
  final String stashId;
  
  public DVNTPostStatusRequestV1(String paramString1, String paramString2, String paramString3)
  {
    super(com.deviantart.android.sdk.api.model.DVNTPostStatusResponse.class);
    body = paramString1;
    repostUUID = paramString2;
    stashId = paramString3;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTPostStatusRequestV1 localDVNTPostStatusRequestV1 = (DVNTPostStatusRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(body, body).append(repostUUID, repostUUID).append(stashId, stashId).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(body).append(repostUUID).append(stashId).toHashCode();
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTPostStatusResponse sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).postStatus(paramString, body, repostUUID, stashId);
  }
}
