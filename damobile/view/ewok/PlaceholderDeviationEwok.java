package com.deviantart.android.damobile.view.ewok;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.damobile.util.DeviationType;

public class PlaceholderDeviationEwok
  extends ImageDeviationEwok
{
  public PlaceholderDeviationEwok(DVNTDeviation paramDVNTDeviation)
  {
    super(paramDVNTDeviation);
  }
  
  public View a(Activity paramActivity, ViewGroup paramViewGroup)
  {
    return b(paramActivity, paramViewGroup, 0, 0);
  }
  
  public View b(Activity paramActivity, ViewGroup paramViewGroup, int paramInt1, int paramInt2)
  {
    return new ImageView(paramActivity);
  }
  
  public View b(Context paramContext)
  {
    return new View(paramContext);
  }
  
  public DVNTImage b(Activity paramActivity)
  {
    return super.b(paramActivity);
  }
  
  public DeviationType b()
  {
    return DeviationType.g;
  }
}
