package com.deviantart.android.damobile.view.ewok;

import android.app.Activity;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.deviantart.android.android.package_14.model.DVNTFeedItem;
import com.deviantart.android.android.package_14.model.DVNTGallection;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APICollectionLoader;
import com.deviantart.android.damobile.util.torpedo.TorpedoAdapter;
import com.deviantart.android.damobile.util.torpedo.TorpedoItemDecoration;
import com.deviantart.android.damobile.util.torpedo.TorpedoLayout;
import com.deviantart.android.damobile.util.torpedo.TorpedoRecyclerView;
import com.deviantart.android.damobile.util.torpedo.TorpedoUtils.TorpedoEndlessScrollListener;
import com.deviantart.android.damobile.util.torpedo.TorpedoUtils.TorpedoNotifiable;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil.ScrollEventHelper;
import com.deviantart.android.damobile.view.HorizontalTorpedoScrollBar;
import com.deviantart.android.damobile.view.ewok.decorator.WatchFeedItemEwok;
import samples.grantland.widget.AutofitTextView;

public class WatchFeedCollectionEwok
  extends WatchFeedItemEwok
{
  protected DVNTFeedItem b;
  
  public WatchFeedCollectionEwok(DVNTFeedItem paramDVNTFeedItem)
  {
    b = paramDVNTFeedItem;
  }
  
  public Stream c()
  {
    return StreamCacher.a(new APICollectionLoader(b.getCollection().getFolderId(), b.getUser().getUserName()), StreamCacheStrategy.a);
  }
  
  public String execute()
  {
    return b.getCollection().getName();
  }
  
  protected String getTranslation()
  {
    return "Collection";
  }
  
  public void onCreateView(Activity paramActivity, ViewGroup paramViewGroup1, ViewGroup paramViewGroup2, boolean paramBoolean)
  {
    Stream localStream = c();
    Log.d("collection", "getting stream for " + execute() + " - unique ID = " + localStream.getValue());
    if (!paramBoolean)
    {
      paramViewGroup2.removeAllViews();
      paramViewGroup2.addView((LinearLayout)((LayoutInflater)paramActivity.getSystemService("layout_inflater")).inflate(2130968616, paramViewGroup1, false));
    }
    a(paramActivity, paramViewGroup2, b);
    paramViewGroup1 = LayoutInflater.from(paramActivity).inflate(2130968651, paramViewGroup2, false);
    Object localObject1 = (AutofitTextView)paramViewGroup2.findViewById(2131689618);
    Object localObject2 = execute();
    label187:
    TorpedoRecyclerView localTorpedoRecyclerView;
    if (localObject2 != null)
    {
      ((TextView)localObject1).setText((CharSequence)localObject2);
      ((View)localObject1).setOnClickListener(new WatchFeedCollectionEwok.1(this, paramActivity, (String)localObject2));
      localObject1 = (TorpedoLayout)paramViewGroup1.findViewById(2131689699);
      localObject2 = (HorizontalTorpedoScrollBar)paramViewGroup1.findViewById(2131689698);
      if (!localStream.iterator()) {
        break label499;
      }
      i = size().intValue() - 1;
      ((HorizontalTorpedoScrollBar)localObject2).setMaxPosition(i);
      localTorpedoRecyclerView = (TorpedoRecyclerView)((View)localObject1).findViewById(2131690078);
      localTorpedoRecyclerView.setTorpedoType(getTranslation());
      localTorpedoRecyclerView.setCategory(EventKeys.Category.b);
      localTorpedoRecyclerView.setScrollEventHelper(new TrackerUtil.ScrollEventHelper(new Integer[] { Integer.valueOf(1), Integer.valueOf(5), Integer.valueOf(10), Integer.valueOf(15), Integer.valueOf(20), Integer.valueOf(25), Integer.valueOf(30) }));
      localTorpedoRecyclerView.setOrientation(0);
      localTorpedoRecyclerView.addItemDecoration(new TorpedoItemDecoration(paramActivity));
      TorpedoAdapter localTorpedoAdapter = new TorpedoAdapter(localTorpedoRecyclerView, localStream);
      localTorpedoAdapter.a(Integer.valueOf(8));
      localTorpedoAdapter.a(paramActivity.getResources().getString(2131230884));
      localTorpedoRecyclerView.setAdapter(localTorpedoAdapter);
      localTorpedoRecyclerView.setScrollCallback(new WatchFeedCollectionEwok.2(this, localStream, (HorizontalTorpedoScrollBar)localObject2));
      TorpedoUtils.TorpedoEndlessScrollListener localTorpedoEndlessScrollListener = new TorpedoUtils.TorpedoEndlessScrollListener(localStream, paramActivity);
      localTorpedoEndlessScrollListener.b(new TorpedoUtils.TorpedoNotifiable((TorpedoLayout)localObject1, (HorizontalTorpedoScrollBar)localObject2, localTorpedoEndlessScrollListener));
      localTorpedoRecyclerView.b(paramActivity.getResources().getInteger(2131492889), localTorpedoEndlessScrollListener);
      if (localStream.iterator()) {
        localTorpedoEndlessScrollListener.a();
      }
      if (!localTorpedoAdapter.f()) {
        break label511;
      }
    }
    label499:
    label511:
    for (int i = 0;; i = 8)
    {
      ((View)localObject2).setVisibility(i);
      localTorpedoRecyclerView.smoothScrollToPosition(localStream.e());
      paramActivity = (FrameLayout)paramViewGroup2.findViewById(2131689619);
      paramActivity.removeAllViews();
      paramActivity.addView(paramViewGroup1);
      return;
      if (localObject1 == null) {
        break;
      }
      ((View)localObject1).setVisibility(8);
      break;
      i = localStream.size() - 1;
      break label187;
    }
  }
  
  public Integer size()
  {
    return b.getCollection().getSize();
  }
}
