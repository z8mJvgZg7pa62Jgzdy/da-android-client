package com.deviantart.android.damobile.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.BaseBundle;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.view.ImageFullView;
import java.io.File;

public class SimpleImageActivity
  extends BaseActivity
{
  @Bind({2131689608})
  ImageFullView imageView;
  @Bind({2131689607})
  TextView titleView;
  
  public SimpleImageActivity() {}
  
  public static Intent a(Context paramContext, String paramString, File paramFile)
  {
    paramContext = new Intent(paramContext, SimpleImageActivity.class);
    paramContext.putExtra("image_file", paramFile);
    paramContext.putExtra("image_title", paramString);
    return paramContext;
  }
  
  public static Intent newIntent(Context paramContext, String paramString1, String paramString2)
  {
    paramContext = new Intent(paramContext, SimpleImageActivity.class);
    paramContext.putExtra("image_url", paramString2);
    paramContext.putExtra("image_title", paramString1);
    return paramContext;
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130968608);
    ButterKnife.bind(this);
    paramBundle = getIntent().getStringExtra("image_title");
    String str = getIntent().getStringExtra("image_url");
    File localFile = (File)getIntent().getExtras().get("image_file");
    if ((str == null) && (localFile == null))
    {
      Log.e("SimpleImageActivity", "invalid intent values. Doesn't have image.");
      finish();
      return;
    }
    if (paramBundle == null)
    {
      titleView.setVisibility(8);
      if (str == null) {
        break label115;
      }
    }
    label115:
    for (paramBundle = Uri.parse(str);; paramBundle = Uri.fromFile(localFile))
    {
      imageView.b(paramBundle, false);
      return;
      titleView.setText(paramBundle);
      break;
    }
  }
  
  protected void onDestroy()
  {
    ButterKnife.unbind(this);
    super.onDestroy();
  }
}
