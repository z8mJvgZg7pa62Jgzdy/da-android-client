package com.deviantart.android.android.package_14.session;

import retrofit.Callback;

public abstract interface DVNTRestTokenService
{
  public abstract void newClientToken(String paramString1, String paramString2, Callback paramCallback);
  
  public abstract void newUserToken(String paramString1, String paramString2, String paramString3, String paramString4, Callback paramCallback);
  
  public abstract void refreshUserToken(String paramString1, String paramString2, String paramString3, Callback paramCallback);
  
  public abstract void revoke(String paramString, Boolean paramBoolean, Callback paramCallback);
}
