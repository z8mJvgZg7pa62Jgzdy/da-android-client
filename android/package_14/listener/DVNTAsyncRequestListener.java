package com.deviantart.android.android.package_14.listener;

import com.deviantart.android.android.package_14.model.DVNTEndpointError;

public abstract class DVNTAsyncRequestListener<T>
{
  public DVNTAsyncRequestListener() {}
  
  public void onCancellation() {}
  
  public void onException(Exception paramException)
  {
    paramException.printStackTrace();
  }
  
  public abstract void onFailure(DVNTEndpointError paramDVNTEndpointError);
  
  public abstract void onSuccess(Object paramObject);
  
  public boolean shouldCancel()
  {
    return false;
  }
}
