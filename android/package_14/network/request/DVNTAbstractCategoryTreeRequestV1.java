package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.sdk.api.model.DVNTCategory.List;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class DVNTAbstractCategoryTreeRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<DVNTCategory.List>
{
  final String catPath;
  
  public DVNTAbstractCategoryTreeRequestV1(String paramString)
  {
    super(DVNTCategory.List.class);
    catPath = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTAbstractCategoryTreeRequestV1 localDVNTAbstractCategoryTreeRequestV1 = (DVNTAbstractCategoryTreeRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(catPath, catPath).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    try
    {
      Object localObject = new StringBuilder().append("categorytree");
      String str = catPath;
      localObject = URLEncoder.encode(str, "UTF-8");
      return localObject;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException) {}
    return "categorytree" + catPath.replace("/", "");
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 86400000L;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(catPath).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
}
