package com.deviantart.android.android.utils;

import com.deviantart.android.android.package_14.network.request.DVNTExpandArgumentEnum;

public class DVNTExpandableArgumentBuilder
{
  public static final String EXPAND_ARG_SEPARATOR = ",";
  private StringBuilder expandableArgBuilder = new StringBuilder();
  
  public DVNTExpandableArgumentBuilder() {}
  
  public String build()
  {
    if (expandableArgBuilder.length() > 0) {
      return expandableArgBuilder.toString();
    }
    return null;
  }
  
  public DVNTExpandableArgumentBuilder loadInfo(DVNTExpandArgumentEnum paramDVNTExpandArgumentEnum, boolean paramBoolean)
  {
    if (!paramBoolean) {
      return this;
    }
    if (expandableArgBuilder.length() > 0) {
      expandableArgBuilder.append(",");
    }
    expandableArgBuilder.append(paramDVNTExpandArgumentEnum.getExpandableKey());
    return this;
  }
}
