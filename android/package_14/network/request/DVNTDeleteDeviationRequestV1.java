package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1Flavored;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTDeleteDeviationRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1Extra<com.deviantart.android.sdk.api.model.DVNTSuccess>
{
  private final String deviationId;
  
  public DVNTDeleteDeviationRequestV1(String paramString)
  {
    super(com.deviantart.android.sdk.api.model.DVNTSuccess.class);
    deviationId = paramString;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTDeleteDeviationRequestV1 localDVNTDeleteDeviationRequestV1 = (DVNTDeleteDeviationRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(deviationId, deviationId).isEquals();
    }
    return false;
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(deviationId).toHashCode();
  }
  
  public boolean isCancellable()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTSuccess sendRequest(String paramString)
  {
    return ((DVNTServiceV1Flavored)getService()).deleteDeviation(paramString, deviationId);
  }
}
