package com.deviantart.android.damobile.util;

public abstract interface DailyDeviationItem
{
  public abstract DailyDeviationItem.ItemType getState();
}
