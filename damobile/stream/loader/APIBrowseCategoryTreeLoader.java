package com.deviantart.android.damobile.stream.loader;

import android.content.Context;
import com.deviantart.android.android.package_14.DVNTAsyncAPI;
import com.deviantart.android.android.package_14.DVNTRequestExecutor;
import com.deviantart.android.damobile.stream.listener.StreamLoadListener;
import com.deviantart.android.sdk.api.model.DVNTCategory;

public class APIBrowseCategoryTreeLoader
  extends StreamLoader<DVNTCategory>
{
  private String d;
  
  public APIBrowseCategoryTreeLoader(String paramString)
  {
    d = paramString;
  }
  
  public String b()
  {
    return "categorytreeloader" + d;
  }
  
  public void saveSession(Context paramContext, int paramInt, boolean paramBoolean, StreamLoadListener paramStreamLoadListener)
  {
    DVNTRequestExecutor localDVNTRequestExecutor = DVNTAsyncAPI.categoryTree(d, true);
    if (paramBoolean) {
      localDVNTRequestExecutor.noCache();
    }
    localDVNTRequestExecutor.call(paramContext, new APIBrowseCategoryTreeLoader.1(this, paramStreamLoadListener));
  }
}
