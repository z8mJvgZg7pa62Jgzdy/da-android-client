package com.deviantart.android.android.package_14.network;

import android.content.Context;
import android.content.ContextWrapper;
import android.util.Log;
import com.deviantart.android.android.package_14.jsonadapter.DVNTChallengeMediaJSONAdapter;
import com.deviantart.android.android.package_14.jsonadapter.DVNTStashMetadataJSONAdapter;
import com.deviantart.android.sdk.api.model.DVNTChallengeMedia;
import com.deviantart.android.sdk.api.model.DVNTStashMetadata;
import com.google.gson.GsonBuilder;
import com.octo.android.robospice.request.CachedSpiceRequest;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import com.octo.android.robospice.retrofit.RetrofitSpiceService;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import java.util.Set;
import retrofit.RestAdapter.Builder;
import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;

public class DVNTRetrofitSpiceService
  extends RetrofitGsonSpiceService
{
  public DVNTRetrofitSpiceService() {}
  
  public void addRequest(CachedSpiceRequest paramCachedSpiceRequest, Set paramSet)
  {
    super.addRequest(paramCachedSpiceRequest, paramSet);
    Log.e("sendRequest", paramCachedSpiceRequest.toString());
  }
  
  protected Converter createConverter()
  {
    return new GsonConverter(new GsonBuilder().registerTypeAdapter(DVNTChallengeMedia.class, new DVNTChallengeMediaJSONAdapter()).registerTypeAdapter(DVNTStashMetadata.class, new DVNTStashMetadataJSONAdapter()).create());
  }
  
  protected RestAdapter.Builder createRestAdapterBuilder()
  {
    Object localObject = new OkHttpClient();
    try
    {
      ((OkHttpClient)localObject).setCache(new Cache(getApplicationContext().getCacheDir(), 50000000L));
      localObject = new OkClient((OkHttpClient)localObject);
      return super.createRestAdapterBuilder().setClient((Client)localObject).setRequestInterceptor(new DVNTRequestInterceptor());
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  protected String getServerUrl()
  {
    return "https://www.deviantart.com";
  }
  
  public int getThreadCount()
  {
    return Runtime.getRuntime().availableProcessors();
  }
}
