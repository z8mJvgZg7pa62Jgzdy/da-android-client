package com.deviantart.android.damobile.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;

public class Recent
{
  private SharedPreferences context;
  private ArrayList<String> data = new ArrayList();
  private String name;
  
  public Recent(Context paramContext, String paramString)
  {
    name = paramString;
    context = SharedPreferenceUtil.get(paramContext);
  }
  
  private SharedPreferences.Editor edit()
  {
    return context.edit();
  }
  
  public ArrayList read()
  {
    data = new ArrayList(Arrays.asList(StringUtils.split(context.getString(name, ""), "|\t")));
    return data;
  }
  
  public void write()
  {
    data.clear();
    edit().putString(name, null).commit();
  }
  
  public void write(String paramString)
  {
    read();
    data.remove(paramString);
    data.add(0, paramString);
    if (data.size() > 20) {
      data = new ArrayList(data.subList(0, 20));
    }
    edit().putString(name, StringUtils.join(data, "|\t")).commit();
  }
}
