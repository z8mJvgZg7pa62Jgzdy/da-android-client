package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;

public class DVNTStashSubmitResponse
  extends DVNTEndpointError
{
  @SerializedName("itemid")
  Long itemId;
  @SerializedName("stack")
  String stack;
  @SerializedName("stackid")
  Long stackId;
  @SerializedName("status")
  String status;
  
  public DVNTStashSubmitResponse() {}
  
  public Long getItemId()
  {
    return itemId;
  }
  
  public String getStack()
  {
    return stack;
  }
  
  public Long getStackId()
  {
    return stackId;
  }
  
  public String getStatus()
  {
    return status;
  }
  
  public void setItemId(Long paramLong)
  {
    itemId = paramLong;
  }
  
  public void setStack(String paramString)
  {
    stack = paramString;
  }
  
  public void setStackId(Long paramLong)
  {
    stackId = paramLong;
  }
  
  public void setStatus(String paramString)
  {
    status = paramString;
  }
}
