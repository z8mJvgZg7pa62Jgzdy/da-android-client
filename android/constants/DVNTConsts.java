package com.deviantart.android.android.constants;

public final class DVNTConsts
{
  public static final String AUTHORIZE_URL = "https://www.deviantart.com/oauth2/authorize?response_type=code&client_id=%s&redirect_uri=%s&scope=%s";
  public static final String BASIC_SCOPE = "basic";
  public static final long CACHE_LIFETIME_ALWAYS_EXPIRED = -1L;
  public static final String DEFAULT_REDIRECT_URI = "oauth://deviantart";
  public static final String DEFAULT_USER_AGENT_PREFIX = "AndroidSDK-based-";
  public static final String GRANT_TYPE_PASSWORD = "password";
  public static final String HEADER_APP_BUILD = "dA-app-build";
  public static final String HEADER_RC_CONTENT_ONLY = "rconly";
  public static final String HEADER_SESSIONID = "dA-session-id";
  public static final String HEADER_USER_AGENT = "User-Agent";
  public static final String HEADER_VERSION = "dA-minor-version";
  public static final String KEY_AUTHORIZE_URL = "authorizeURL";
  public static final String KEY_CLIENT_ACCESS_TOKEN = "client_accessToken";
  public static final String KEY_USER_ACCESS_TOKEN = "user_accessToken";
  public static final String OAUTH_SHARED_PREFERENCES_NAME = "Oauth";
  public static final String QUERY_MATURE = "mature_content";
  public static final String SERVER_BASE_URL = "https://www.deviantart.com";
  
  public DVNTConsts() {}
}
