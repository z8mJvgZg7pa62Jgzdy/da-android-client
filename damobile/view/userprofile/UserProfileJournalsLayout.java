package com.deviantart.android.damobile.view.userprofile;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import com.deviantart.android.damobile.adapter.recyclerview.DeviationFullWidthAdapter;
import com.deviantart.android.damobile.fragment.UserProfileFragment;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APIBrowseUserJournalsLoader;
import com.deviantart.android.damobile.util.Graphics;
import com.deviantart.android.damobile.view.DAStateRecyclerView;
import com.deviantart.android.damobile.view.ViewState.Helper;
import com.deviantart.android.damobile.view.thirdparty.decorator.BaseItemDecoration.InstanceBuilder;

public class UserProfileJournalsLayout
  extends DAStateRecyclerView
{
  public UserProfileJournalsLayout(Context paramContext, String paramString, boolean paramBoolean)
  {
    super(paramContext);
    setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    setAdapter();
    addItemDecoration(new BaseItemDecoration.InstanceBuilder().subtract(Graphics.add(paramContext, 8)).build(getContext()));
    onViewCreated();
    ViewState.Helper.a(b, paramString, getContext().getString(2131230888), paramString + " " + getContext().getString(2131230889));
    paramString = StreamCacher.a(new APIBrowseUserJournalsLoader(paramString, Boolean.valueOf(false)));
    if (!paramBoolean) {
      bool = true;
    }
    setAdapter(new DeviationFullWidthAdapter(paramString, bool));
    b(UserProfileFragment.init(paramContext));
  }
}
