package com.deviantart.android.damobile.activity;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Window;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.config.DVNTAbstractAPIConfig;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.BuildConfig;
import com.deviantart.android.damobile.drawable.SlashBackgroundDrawable;
import com.deviantart.android.damobile.fragment.LoginDialogFragment.LoginDialogListener;
import com.deviantart.android.damobile.pushnotifications.GCMRegistration;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.util.BusStation;
import com.deviantart.android.damobile.util.BusStation.OnBackPressedEvent;
import com.deviantart.android.damobile.util.BusStation.OnCommentPostedEvent;
import com.deviantart.android.damobile.util.BusStation.OnGraduateCancelEvent;
import com.deviantart.android.damobile.util.BusStation.OnLoggedInEvent;
import com.deviantart.android.damobile.util.BusStation.OnLoggedOutEvent;
import com.deviantart.android.damobile.util.ConnectivityUtils;
import com.deviantart.android.damobile.util.DeviationDownloadManager;
import com.deviantart.android.damobile.util.GraduateHandler;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.util.SharedPreferenceUtil;
import com.deviantart.android.damobile.util.UserUtils;
import com.deviantart.android.damobile.util.deeplink.DeepLinkController;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.util.tracking.pacaya.MobileLava;
import com.deviantart.pacaya.Pacaya;
import com.google.android.com.appindexing.AppIndex;
import com.google.android.com.common.aimsicd.GoogleApiClient;
import com.google.android.com.common.aimsicd.GoogleApiClient.Builder;
import com.squareup.otto.Bus;
import java.util.concurrent.TimeUnit;
import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity
  extends AppCompatActivity
  implements LoginDialogFragment.LoginDialogListener
{
  private static boolean a = false;
  public static Class handler = null;
  public static boolean index = false;
  private static boolean r = false;
  private boolean c = false;
  private boolean mStopped = false;
  protected GoogleApiClient mToolbar;
  
  public BaseActivity() {}
  
  private void onCreate()
  {
    CrashManager.f(this, "8aaca23a88c3cca1b2d7b824c07160e4");
    if (BuildConfig.DEBUG.booleanValue()) {
      UpdateManager.init(this, "8aaca23a88c3cca1b2d7b824c07160e4");
    }
  }
  
  private void onOptionsItemSelected()
  {
    if ((!a) && (!DVNTAbstractAsyncAPI.isUserSession(this)) && (NavigationUtils.processIntent(getIntent())))
    {
      TrackerUtil.b(false);
      startActivity(new Intent(this, IntroActivity.class));
      a = true;
    }
    while (DVNTAbstractAsyncAPI.isUserSession(this))
    {
      a = true;
      if (!c)
      {
        a();
        c = true;
        return;
        ConnectivityUtils.showErrorDialog(this);
      }
      else
      {
        GCMRegistration.b(this);
        return;
      }
    }
    if ((!DVNTAbstractAsyncAPI.isUserSession(this)) && (c))
    {
      b();
      c = false;
    }
  }
  
  private void onRequestPermissionsResult() {}
  
  public void a()
  {
    GCMRegistration.b(this);
    TrackerUtil.b(true);
    BusStation.get().post(new BusStation.OnLoggedInEvent());
  }
  
  protected void attachBaseContext(Context paramContext)
  {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(paramContext));
  }
  
  public void b()
  {
    SharedPreferenceUtil.putString(this, "recent_username", null);
    TrackerUtil.b(false);
    TrackerUtil.invoke(this, null);
    BusStation.get().post(new BusStation.OnLoggedOutEvent());
  }
  
  public GoogleApiClient getToolbar()
  {
    return mToolbar;
  }
  
  public void hyphenate(String paramString1, String paramString2)
  {
    ((GraduateHandler)DVNTAbstractAsyncAPI.getConfig().getGraduateHandler()).showInfoDialog(this, paramString1, paramString2);
  }
  
  public void onAccountChange()
  {
    UserUtils.reconfigure(this);
    onOptionsItemSelected();
  }
  
  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramIntent == null) {
      return;
    }
    if ((paramInt1 == 109) && (paramInt2 == -1))
    {
      Bundle localBundle = paramIntent.getBundleExtra("result");
      BusStation.get().post(new BusStation.OnCommentPostedEvent(localBundle));
    }
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }
  
  public void onBackPressed()
  {
    if (mStopped)
    {
      if (DVNTContextUtils.isContextDead(this)) {
        return;
      }
      BusStation.OnBackPressedEvent localOnBackPressedEvent = new BusStation.OnBackPressedEvent();
      BusStation.get().post(localOnBackPressedEvent);
      if (!localOnBackPressedEvent.i())
      {
        if (getFragmentManager().getBackStackEntryCount() > 0)
        {
          getFragmentManager().popBackStack();
          return;
        }
        super.onBackPressed();
      }
    }
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    c = DVNTAbstractAsyncAPI.isUserSession(this);
    if (c) {
      TrackerUtil.b(true);
    }
    for (;;)
    {
      paramBundle = getTheme().obtainStyledAttributes(0, new int[] { 2130771978 });
      if (paramBundle.getBoolean(0, false)) {
        getWindow().setBackgroundDrawable(SlashBackgroundDrawable.getDrawable(this));
      }
      paramBundle.recycle();
      mToolbar = new GoogleApiClient.Builder(this).addApi(AppIndex.t3).build();
      return;
      TrackerUtil.b(false);
    }
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    StreamCacher.a();
  }
  
  public void onLowMemory()
  {
    Log.d("gc", "had trigger onLowMemory dude");
    StreamCacher.clear();
    super.onLowMemory();
  }
  
  protected void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    DeepLinkController.loadData(this, paramIntent, true);
  }
  
  protected void onPause()
  {
    super.onPause();
    onRequestPermissionsResult();
    DeviationDownloadManager.unregisterReceiver(this);
    MobileLava.e().unsubscribe();
  }
  
  protected void onRefresh()
  {
    if (c == DVNTAbstractAsyncAPI.isUserSession(this))
    {
      if (!a) {
        return;
      }
      if (c)
      {
        a();
        return;
      }
      b();
    }
  }
  
  protected void onResume()
  {
    super.onResume();
    handler = getClass();
    MobileLava.e().call(5L, TimeUnit.MINUTES);
    if (!r)
    {
      onCreate();
      r = true;
    }
    onOptionsItemSelected();
    DeviationDownloadManager.registerReceiver(this);
  }
  
  protected void onStart()
  {
    super.onStart();
    mStopped = true;
  }
  
  protected void onStop()
  {
    super.onStop();
    mStopped = false;
  }
  
  public void selectItem() {}
  
  public void setCurrentTheme()
  {
    BusStation.get().post(new BusStation.OnGraduateCancelEvent());
  }
}
