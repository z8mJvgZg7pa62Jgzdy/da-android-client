package com.deviantart.android.damobile.view.thirdparty;

import android.content.Context;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;

public abstract interface ByteVector<DATA_TYPE, VIEW_HOLDER extends RecyclerView.ViewHolder>
{
  public abstract RecyclerView.ViewHolder b(Context paramContext, ViewGroup paramViewGroup);
  
  public abstract GomType b();
  
  public abstract void b(Context paramContext, Object paramObject, RecyclerView.ViewHolder paramViewHolder);
}
