package com.deviantart.android.android.package_14.model;

import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTGallection
  implements Serializable
{
  protected DVNTAbstractDeviation.List deviations;
  @SerializedName("folderid")
  protected String folderId;
  protected String name;
  protected Integer size;
  
  public DVNTGallection() {}
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    paramObject = (DVNTGallection)paramObject;
    return Objects.append(folderId, folderId);
  }
  
  public DVNTAbstractDeviation.List getDeviations()
  {
    return deviations;
  }
  
  public String getFolderId()
  {
    return folderId;
  }
  
  public String getName()
  {
    return name;
  }
  
  public Integer getSize()
  {
    return size;
  }
  
  public void setDeviations(DVNTAbstractDeviation.List paramList)
  {
    deviations = paramList;
  }
  
  public void setFolderId(String paramString)
  {
    folderId = paramString;
  }
  
  public void setName(String paramString)
  {
    name = paramString;
  }
  
  public void setSize(Integer paramInteger)
  {
    size = paramInteger;
  }
}
