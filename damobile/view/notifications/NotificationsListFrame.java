package com.deviantart.android.damobile.view.notifications;

import android.content.Context;
import android.util.AttributeSet;
import com.deviantart.android.damobile.view.opt.MCListFrameBase;

public class NotificationsListFrame
  extends MCListFrameBase
{
  public NotificationsListFrame(Context paramContext)
  {
    super(paramContext);
  }
  
  public NotificationsListFrame(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }
  
  public NotificationsListRecyclerView getApiUrl(Context paramContext)
  {
    return new NotificationsListRecyclerView(paramContext);
  }
  
  public NotificationsListRecyclerView getRecyclerView()
  {
    return (NotificationsListRecyclerView)d;
  }
}
