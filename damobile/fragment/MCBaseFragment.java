package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.content.res.Resources;
import android.os.BaseBundle;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.cocosw.undobar.UndoBarController.UndoBar;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.damobile.activity.BaseActivity;
import com.deviantart.android.damobile.adapter.StatefulPagerAdapter;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.util.NavigationUtils;
import com.deviantart.android.damobile.view.viewpageindicator.CustomizableTabPageIndicator;
import com.deviantart.android.damobile.view.viewpageindicator.DATextTabIndicator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public abstract class MCBaseFragment
  extends HomeBaseFragment
{
  private static final HashMap<String, Long> c = new HashMap();
  private HashMap<String, Parcelable> adapter;
  @Bind({2131689759})
  DATextTabIndicator indicator;
  @Bind({2131689758})
  ViewPager pager;
  
  public MCBaseFragment() {}
  
  public boolean b()
  {
    if (!isResumed()) {
      return false;
    }
    return NavigationUtils.c(getActivity());
  }
  
  abstract void c(int paramInt);
  
  abstract Set get();
  
  abstract StatefulPagerAdapter getParentFile(int paramInt);
  
  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if ((paramBundle != null) && (paramBundle.containsKey("page_states"))) {
      adapter = ((HashMap)paramBundle.getSerializable("page_states"));
    }
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    if ((c.containsKey(getTag())) && (new Date().getTime() - ((Long)c.get(getTag())).longValue() > 900000L)) {
      removeDuplicates();
    }
    paramLayoutInflater = (ViewGroup)paramLayoutInflater.inflate(2130968670, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    if (DVNTAbstractAsyncAPI.isUserSession(getActivity())) {
      onOptionsItemSelected();
    }
    for (;;)
    {
      initUI();
      ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(true);
      ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
      ((BaseActivity)getActivity()).getSupportActionBar().setCustomView(setupButtons());
      setExpanded();
      paramViewGroup = getParentFile((int)getActivity().getResources().getDimension(2131361971));
      if (adapter != null)
      {
        paramViewGroup.setOnValueChangedListener(adapter);
        adapter = null;
      }
      pager.setAdapter(paramViewGroup);
      indicator.setViewPager(pager);
      indicator.setOnPageChangeListener(new MCBaseFragment.1(this));
      return paramLayoutInflater;
      updateContent();
    }
  }
  
  public void onDestroyView()
  {
    adapter = StatefulPagerAdapter.saveData(pager);
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public void onPause()
  {
    new UndoBarController.UndoBar(getActivity()).clear();
    c.put(getTag(), Long.valueOf(new Date().getTime()));
    super.onPause();
  }
  
  public void onResume()
  {
    super.onResume();
    if (pager.getCurrentItem() == 0) {
      c(0);
    }
  }
  
  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (adapter != null) {
      paramBundle.putSerializable("page_states", adapter);
    }
  }
  
  protected void removeDuplicates()
  {
    Iterator localIterator = get().iterator();
    while (localIterator.hasNext()) {
      StreamCacher.d((String)localIterator.next());
    }
    get().clear();
  }
  
  abstract int setupButtons();
}
