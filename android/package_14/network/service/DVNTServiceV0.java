package com.deviantart.android.android.package_14.network.service;

import com.deviantart.android.android.package_14.model.DVNTDamnToken;
import com.deviantart.android.android.package_14.model.DVNTStashSpace;

public abstract interface DVNTServiceV0
{
  public static final String VERSION_PREFIX = "/api/oauth2";
  
  public abstract DVNTDamnToken damnToken(String paramString);
  
  public abstract DVNTStashSpace stashSpace(String paramString);
}
