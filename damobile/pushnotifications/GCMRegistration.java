package com.deviantart.android.damobile.pushnotifications;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.AsyncTask;
import android.util.Log;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.util.DeveloperUtils;
import com.deviantart.android.damobile.util.SharedPreferenceUtil;

public class GCMRegistration
{
  public GCMRegistration() {}
  
  public static void b(Activity paramActivity)
  {
    if (DVNTContextUtils.isContextDead(paramActivity)) {
      return;
    }
    Context localContext = paramActivity.getApplicationContext();
    if (DeveloperUtils.b(paramActivity)) {
      get(localContext, c(localContext));
    }
  }
  
  public static void b(Context paramContext, String paramString)
  {
    if (DVNTContextUtils.isContextDead(paramContext)) {
      return;
    }
    Log.d("GCM Registration", "Storing registration id " + paramString);
    SharedPreferenceUtil.putString(paramContext, "registration_id", paramString);
    SharedPreferenceUtil.putString(paramContext, "registration_version", "1.12.3");
  }
  
  private static String c(Context paramContext)
  {
    String str1 = SharedPreferenceUtil.getString(paramContext, "registration_id", "");
    String str2 = SharedPreferenceUtil.getString(paramContext, "registration_version", "");
    if ((str1.isEmpty()) || (str2.isEmpty()))
    {
      Log.i("GCM Registration", "Registration id or/and version not found");
      clearNotification(paramContext);
      return "";
    }
    if (!"1.12.3".equals(str2))
    {
      Log.i("GCM Registration", "Registration version has expired");
      clearNotification(paramContext);
      return "";
    }
    return str1;
  }
  
  public static void clearNotification(Context paramContext)
  {
    if (DVNTContextUtils.isContextDead(paramContext)) {
      return;
    }
    Log.d("GCM Registration", "Removing registration id");
    onRegistered(paramContext);
    paramContext = (NotificationManager)paramContext.getSystemService("notification");
    if (paramContext != null) {
      paramContext.cancelAll();
    }
  }
  
  private static void get(Context paramContext, String paramString)
  {
    new GCMRegistration.1(paramContext, paramString).execute(new Void[] { null, null, null });
  }
  
  protected static void onRegistered(Context paramContext)
  {
    SharedPreferenceUtil.putString(paramContext, "registration_id", "");
    SharedPreferenceUtil.putString(paramContext, "registration_version", "");
  }
}
