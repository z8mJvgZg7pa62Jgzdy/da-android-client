package com.deviantart.android.damobile.adapter.recyclerview;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.DeviationUtils.ProcessMenuAddable;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.view.thirdparty.ByteVector;
import com.deviantart.android.damobile.view.thirdparty.GomFactory;
import com.deviantart.android.damobile.view.thirdparty.GomType;
import com.deviantart.android.damobile.view.thirdparty.decorator.DecoratorGomHelper;
import com.deviantart.android.damobile.view.thirdparty.decorator.DecoratorType;
import com.deviantart.android.damobile.view.thirdparty.decorator.DecoratorViewHolder;
import com.deviantart.android.damobile.view.thirdparty.deviation.DeviationGom;

public class DeviationFullWidthAdapter
  extends DAStateRecyclerViewAdapter<com.deviantart.android.sdk.api.model.DVNTDeviation>
  implements DeviationUtils.ProcessMenuAddable
{
  private DecoratorGomHelper d = new DecoratorGomHelper(new DecoratorType[] { DecoratorType.c });
  private boolean h;
  
  public DeviationFullWidthAdapter(Stream paramStream, boolean paramBoolean)
  {
    super(paramStream);
    h = paramBoolean;
  }
  
  protected int b(int paramInt)
  {
    return GomFactory.a((com.deviantart.android.android.package_14.model.DVNTDeviation)get().get(paramInt)).b().ordinal();
  }
  
  protected RecyclerView.ViewHolder b(ViewGroup paramViewGroup, int paramInt)
  {
    RecyclerView.ViewHolder localViewHolder = GomType.b(paramInt).b(paramViewGroup.getContext(), paramViewGroup);
    itemView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
    return d.a(paramViewGroup.getContext(), paramViewGroup, localViewHolder);
  }
  
  protected void b(RecyclerView.ViewHolder paramViewHolder, int paramInt)
  {
    if (!(paramViewHolder instanceof DecoratorViewHolder)) {
      return;
    }
    DecoratorViewHolder localDecoratorViewHolder = (DecoratorViewHolder)paramViewHolder;
    Object localObject = (com.deviantart.android.android.package_14.model.DVNTDeviation)get().get(paramInt);
    DeviationGom localDeviationGom = GomFactory.a((com.deviantart.android.android.package_14.model.DVNTDeviation)localObject);
    localObject = new DeviationDescription((com.deviantart.android.android.package_14.model.DVNTDeviation)localObject, get().length(), paramInt);
    d.a(itemView.getContext(), localDecoratorViewHolder, localObject);
    localDeviationGom.b(DeviationUtils.b(this));
    localDeviationGom.b(itemView.getContext(), (DeviationDescription)localObject, DecoratorGomHelper.b(localDecoratorViewHolder), h);
  }
}
