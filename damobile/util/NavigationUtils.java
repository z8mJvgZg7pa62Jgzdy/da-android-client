package com.deviantart.android.damobile.util;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.BaseBundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;
import com.deviantart.android.android.package_14.DVNTAbstractAsyncAPI;
import com.deviantart.android.android.package_14.model.DVNTNote;
import com.deviantart.android.android.utils.DVNTContextUtils;
import com.deviantart.android.damobile.activity.CommentActivity.IntentBuilder;
import com.deviantart.android.damobile.activity.HomeActivity;
import com.deviantart.android.damobile.activity.HomeActivity.HomeActivityPages;
import com.deviantart.android.damobile.activity.SubmitActivity.IntentBuilder;
import com.deviantart.android.damobile.fragment.DeviationFullViewFragment;
import com.deviantart.android.damobile.fragment.DeviationFullViewFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.DiscoveryFragment;
import com.deviantart.android.damobile.fragment.FullTorpedoExtraMLTFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.FullTorpedoExtraTitleFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.FullTorpedoFragment;
import com.deviantart.android.damobile.fragment.FullTorpedoFragment.InstanceBuilder;
import com.deviantart.android.damobile.fragment.HomeBaseFragment.HomeFragmentInstanceBuilder;
import com.deviantart.android.damobile.fragment.NotesDetailFragment;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.stream.StreamCacheStrategy;
import com.deviantart.android.damobile.stream.StreamCacher;
import com.deviantart.android.damobile.stream.loader.APIBrowseTagLoader;
import com.deviantart.android.damobile.stream.loader.APICollectionLoader;
import com.deviantart.android.damobile.stream.loader.APIGalleryLoader;
import com.deviantart.android.damobile.stream.loader.APIMoreLikeThisLoader;
import com.deviantart.android.damobile.stream.loader.StreamLoader;
import com.deviantart.android.damobile.util.deviation.DeviationPanelTab;
import com.deviantart.android.damobile.util.discovery.DiscoveryPageInfo;
import com.deviantart.android.damobile.util.notes.Mark;
import com.deviantart.android.damobile.util.notes.NotesAction;
import com.deviantart.android.damobile.util.notes.NotesItemData;
import com.deviantart.android.damobile.util.notes.NotesItemData.Observable;
import com.deviantart.android.damobile.util.threaditem.CommentItem;
import com.deviantart.android.damobile.util.torpedo.DeviationDescription;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil;
import com.deviantart.android.damobile.view.ewok.DeviationEwok;

public class NavigationUtils
{
  public NavigationUtils() {}
  
  public static void a(Activity paramActivity, NotesItemData.Observable paramObservable, String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    DVNTNote localDVNTNote = paramObservable.a().getContext();
    if (localDVNTNote.getIsUnread().booleanValue()) {
      NotesAction.a(paramActivity, paramObservable, Mark.c);
    }
    if (paramString != null) {
      TrackerUtil.get(paramActivity, EventKeys.Category.d, "view_a_note", paramString);
    }
    ScreenFlowManager.add(paramActivity, NotesDetailFragment.getItem(paramObservable), "notes_detail|" + localDVNTNote.getNoteId(), paramBoolean1, paramBoolean2);
  }
  
  public static void b(Activity paramActivity, NotesItemData.Observable paramObservable, String paramString)
  {
    a(paramActivity, paramObservable, paramString, true, true);
  }
  
  public static void b(Activity paramActivity, DeviationDescription paramDeviationDescription, DeviationPanelTab paramDeviationPanelTab)
  {
    ScreenFlowManager.a(paramActivity, (DeviationFullViewFragment)new DeviationFullViewFragment.InstanceBuilder().a(paramDeviationDescription.d()).a(paramDeviationDescription.c()).a(paramDeviationPanelTab).a(), "deviation_fullview_stream" + paramDeviationDescription.d().b());
  }
  
  public static void b(Activity paramActivity, DeviationEwok paramDeviationEwok, DeviationPanelTab paramDeviationPanelTab)
  {
    DeviationFullViewFragment.InstanceBuilder localInstanceBuilder = new DeviationFullViewFragment.InstanceBuilder().a(paramDeviationEwok.c());
    if (paramDeviationEwok.getIssueId() != null) {}
    for (int i = paramDeviationEwok.getIssueId().intValue();; i = 0)
    {
      ScreenFlowManager.a(paramActivity, (DeviationFullViewFragment)localInstanceBuilder.a(i).a(paramDeviationPanelTab).a(), "deviation_fullview_stream" + paramDeviationEwok.c().length().b());
      return;
    }
  }
  
  public static void b(Activity paramActivity, String paramString)
  {
    c(paramActivity, paramString, true, false);
  }
  
  public static void b(Activity paramActivity, String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
  {
    paramString1 = StreamCacher.a(new APICollectionLoader(paramString2, paramString1), StreamCacheStrategy.d);
    ScreenFlowManager.get(paramActivity, (Fragment)new FullTorpedoExtraTitleFragment.InstanceBuilder().a(paramString1).b(paramBoolean2).b("collection").a(), "open_collection_fullview" + paramString2, paramBoolean1);
  }
  
  public static void b(HomeActivity paramHomeActivity, CommentType paramCommentType, String paramString)
  {
    b(paramHomeActivity, paramCommentType, paramString, null);
  }
  
  public static void b(HomeActivity paramHomeActivity, CommentType paramCommentType, String paramString, CommentItem paramCommentItem)
  {
    paramCommentType = new CommentActivity.IntentBuilder().a(paramString).b(paramCommentType).a(paramCommentItem).a(paramHomeActivity);
    if (UserUtils.a == null)
    {
      f = paramCommentType;
      a = Integer.valueOf(109);
      DVNTAbstractAsyncAPI.graduate(paramHomeActivity);
      return;
    }
    if (!UserUtils.e)
    {
      UserUtils.showInfoDialog(paramHomeActivity);
      return;
    }
    paramHomeActivity.startActivityForResult(paramCommentType, 109);
  }
  
  public static void c(Activity paramActivity, DiscoveryPageInfo paramDiscoveryPageInfo)
  {
    DVNTAbstractAsyncAPI.cancelAllRequests();
    ScreenFlowManager.goHome(paramActivity);
    ScreenFlowManager.get(paramActivity, DiscoveryFragment.a(paramDiscoveryPageInfo), HomeActivity.HomeActivityPages.g.a() + paramDiscoveryPageInfo.getString(), false);
  }
  
  public static void c(Activity paramActivity, String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
  {
    paramString1 = StreamCacher.a(new APIGalleryLoader(paramString2, paramString1), StreamCacheStrategy.d);
    ScreenFlowManager.get(paramActivity, (Fragment)new FullTorpedoExtraTitleFragment.InstanceBuilder().a(paramString1).b(paramBoolean2).b("gallery").a(), "open_collection_fullview" + paramString2, paramBoolean1);
  }
  
  public static void c(Activity paramActivity, String paramString, boolean paramBoolean)
  {
    Stream localStream = StreamCacher.a(new APIMoreLikeThisLoader(paramString, null), StreamCacheStrategy.d);
    ScreenFlowManager.get(paramActivity, (Fragment)new FullTorpedoExtraMLTFragment.InstanceBuilder().a(localStream).b("mlt").a(paramActivity.getString(2131231090)).b(true).a(), "fullmlt" + paramString, paramBoolean);
  }
  
  public static void c(Activity paramActivity, String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    Stream localStream = StreamCacher.a(new APIBrowseTagLoader(paramString), StreamCacheStrategy.d);
    ScreenFlowManager.get(paramActivity, (FullTorpedoFragment)new FullTorpedoFragment.InstanceBuilder().a(localStream).a("#" + paramString).b("tag").b(paramBoolean2).a(), "open_tag_fullview" + paramString, paramBoolean1);
  }
  
  public static boolean c(Activity paramActivity)
  {
    if (DVNTContextUtils.isContextDead(paramActivity)) {
      return false;
    }
    DVNTAbstractAsyncAPI.cancelAllRequests();
    ScreenFlowManager.goHome(paramActivity);
    ScreenFlowManager.get(paramActivity, new DiscoveryFragment(), HomeActivity.HomeActivityPages.g.a(), false);
    return true;
  }
  
  public static void e(Activity paramActivity, String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    paramString1 = StreamCacher.a(new APICollectionLoader(paramString2, paramString1), StreamCacheStrategy.d);
    ScreenFlowManager.get(paramActivity, (FullTorpedoFragment)new FullTorpedoFragment.InstanceBuilder().a(paramString1).a(paramString3).b("collection").a(), "open_collection_fullview" + paramString2, paramBoolean);
  }
  
  public static void load(Activity paramActivity, String paramString)
  {
    if (!DVNTAbstractAsyncAPI.isUserSession(paramActivity))
    {
      Toast.makeText(paramActivity, paramActivity.getString(2131230961), 1).show();
      return;
    }
    DVNTAbstractAsyncAPI.cancelAllRequests();
    paramActivity.startActivityForResult(new SubmitActivity.IntentBuilder().a(paramString).a(paramActivity), 101);
    paramActivity.overridePendingTransition(2131034146, 2131034132);
  }
  
  public static void navigate(Context paramContext, Uri paramUri)
  {
    paramContext.startActivity(new Intent("android.intent.action.VIEW", paramUri.buildUpon().appendQueryParameter("showbanner", "false").build()));
  }
  
  public static void openBrowser(Context paramContext, String paramString)
  {
    navigate(paramContext, Uri.parse(paramString));
  }
  
  public static boolean processIntent(Intent paramIntent)
  {
    return (paramIntent == null) || ((paramIntent.getData() == null) && ((paramIntent.getExtras() == null) || (paramIntent.getExtras().size() == 0)));
  }
}
