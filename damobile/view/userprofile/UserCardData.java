package com.deviantart.android.damobile.view.userprofile;

import com.deviantart.android.damobile.util.MemberType;
import java.io.Serializable;

public abstract interface UserCardData
  extends Serializable
{
  public abstract MemberType a();
  
  public abstract Boolean b();
  
  public abstract String d();
  
  public abstract String format();
  
  public abstract String get();
  
  public abstract Integer getApplicationContext();
  
  public abstract Integer getCanonicalPath();
  
  public abstract String getColor();
  
  public abstract String getDeveloperConnection();
  
  public abstract String getId();
  
  public abstract String getLocation();
  
  public abstract String getOverview();
  
  public abstract String getParent();
  
  public abstract String getPost();
  
  public abstract String getString();
  
  public abstract Integer getStringArray();
  
  public abstract Boolean isStarred();
  
  public abstract Integer update();
  
  public abstract Integer visitAnnotationDefault();
  
  public abstract String visitFrame();
  
  public abstract Integer visitParameterAnnotation();
}
