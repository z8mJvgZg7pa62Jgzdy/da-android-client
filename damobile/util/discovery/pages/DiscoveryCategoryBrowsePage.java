package com.deviantart.android.damobile.util.discovery.pages;

import android.content.res.Resources;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import com.deviantart.android.android.package_14.model.DVNTCategory;
import com.deviantart.android.damobile.activity.HomeActivity;
import com.deviantart.android.damobile.adapter.recyclerview.CategoryAdapter.Builder;
import com.deviantart.android.damobile.adapter.recyclerview.CategoryAdapter.CategorySelectedListener;
import com.deviantart.android.damobile.adapter.recyclerview.DAStateRecyclerViewAdapter;
import com.deviantart.android.damobile.view.DAStateRecyclerView;

public class DiscoveryCategoryBrowsePage
  extends DiscoveryPage
  implements CategoryAdapter.CategorySelectedListener
{
  public DiscoveryCategoryBrowsePage(String paramString1, CharSequence paramCharSequence, String paramString2)
  {
    super(paramString1, paramCharSequence, paramString2);
  }
  
  public View a(ViewGroup paramViewGroup)
  {
    super.a(paramViewGroup);
    paramViewGroup = new DAStateRecyclerView(a);
    Object localObject = new CategoryAdapter.Builder(a, "/").a(true).a(this).a();
    paramViewGroup.b(n());
    paramViewGroup.setAdapter((DAStateRecyclerViewAdapter)localObject);
    paramViewGroup.setBackgroundColor(a.getResources().getColor(2131558441));
    if (f())
    {
      int i = a.getResources().getDimensionPixelOffset(2131361971);
      paramViewGroup.setProgressViewOffset(i);
      localObject = new View(get());
      ((View)localObject).setLayoutParams(new AbsListView.LayoutParams(-1, i));
      paramViewGroup.a((View)localObject);
    }
    return paramViewGroup;
  }
  
  public void b()
  {
    super.b();
  }
  
  protected boolean f()
  {
    return true;
  }
  
  protected boolean n()
  {
    return true;
  }
  
  public void onItemClick(DVNTCategory paramDVNTCategory)
  {
    ((HomeActivity)get()).b(paramDVNTCategory);
  }
}
