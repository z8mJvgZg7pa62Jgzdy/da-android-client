package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTPublishStashItemResponse
  implements Serializable
{
  @SerializedName("deviationid")
  String deviationId;
  @SerializedName("url")
  String deviationURL;
  @SerializedName("status")
  String status;
  
  public DVNTPublishStashItemResponse() {}
  
  public String getDeviationId()
  {
    return deviationId;
  }
  
  public String getDeviationURL()
  {
    return deviationURL;
  }
  
  public String getStatus()
  {
    return status;
  }
  
  public void setDeviationId(String paramString)
  {
    deviationId = paramString;
  }
  
  public void setDeviationURL(String paramString)
  {
    deviationURL = paramString;
  }
  
  public void setStatus(String paramString)
  {
    status = paramString;
  }
}
