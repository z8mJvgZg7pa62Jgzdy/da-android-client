package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTStashSpace
  implements Serializable
{
  @SerializedName("available_space")
  Long availableSpace;
  @SerializedName("total_space")
  Long totalSpace;
  
  public DVNTStashSpace() {}
  
  public Long getAvailableSpace()
  {
    return availableSpace;
  }
  
  public Long getTotalSpace()
  {
    return totalSpace;
  }
  
  public void setAvailableSpace(Long paramLong)
  {
    availableSpace = paramLong;
  }
  
  public void setTotalSpace(Long paramLong)
  {
    totalSpace = paramLong;
  }
}
