package com.deviantart.android.damobile.view.notes;

import android.content.Context;

public class NotesListRecyclerView
  extends com.deviantart.android.damobile.view.mc.MCListRecyclerView<NotesListAdapter>
{
  public NotesListRecyclerView(Context paramContext)
  {
    super(paramContext);
  }
}
