package com.deviantart.android.damobile.view.tooltip.tour;

public class DATourSlideDescriptor
{
  public Integer b;
  public int c;
  public boolean d;
  public int description;
  public int x;
  
  public DATourSlideDescriptor(int paramInt1, int paramInt2, int paramInt3, Integer paramInteger, boolean paramBoolean)
  {
    description = paramInt1;
    x = paramInt2;
    c = paramInt3;
    b = paramInteger;
    d = paramBoolean;
  }
}
