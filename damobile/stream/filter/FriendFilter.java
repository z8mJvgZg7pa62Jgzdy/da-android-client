package com.deviantart.android.damobile.stream.filter;

import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.damobile.view.AbstractWatchButton;
import java.util.HashMap;

public class FriendFilter
  implements StreamFilter<com.deviantart.android.sdk.api.model.DVNTFriend>
{
  public FriendFilter() {}
  
  public boolean a(com.deviantart.android.android.package_14.model.DVNTFriend paramDVNTFriend)
  {
    if ((paramDVNTFriend == null) || (paramDVNTFriend.getUser() == null) || (paramDVNTFriend.getUser().getUserName() == null) || (!paramDVNTFriend.getYoureWatching())) {
      return true;
    }
    paramDVNTFriend = paramDVNTFriend.getUser().getUserName();
    if (AbstractWatchButton.c.containsKey(paramDVNTFriend)) {
      return !((Boolean)AbstractWatchButton.c.get(paramDVNTFriend)).booleanValue();
    }
    return false;
  }
}
