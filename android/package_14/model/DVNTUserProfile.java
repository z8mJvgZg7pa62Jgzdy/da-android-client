package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class DVNTUserProfile
  implements Serializable
{
  @SerializedName("artist_level")
  String artistLevel;
  @SerializedName("artist_specialty")
  String artistSpecialty;
  String bio;
  String country;
  @SerializedName("cover_photo")
  String coverPhotoURL;
  @SerializedName("user_is_artist")
  Boolean isArtist;
  @SerializedName("is_watching")
  Boolean isWatching;
  @SerializedName("last_status")
  DVNTUserStatus lastStatus;
  @SerializedName("profile_pic")
  DVNTDeviation profilePicture;
  @SerializedName("profile_url")
  String profileURL;
  @SerializedName("real_name")
  String realName;
  DVNTUserProfile.DVNTUserProfileStats stats;
  @SerializedName("tagline")
  String tagLine;
  DVNTUser user;
  String website;
  
  public DVNTUserProfile() {}
  
  public String getArtistLevel()
  {
    return artistLevel;
  }
  
  public String getArtistSpecialty()
  {
    return artistSpecialty;
  }
  
  public String getBio()
  {
    return bio;
  }
  
  public String getCountry()
  {
    return country;
  }
  
  public String getCoverPhotoURL()
  {
    return coverPhotoURL;
  }
  
  public Boolean getIsArtist()
  {
    return isArtist;
  }
  
  public Boolean getIsWatching()
  {
    return isWatching;
  }
  
  public DVNTUserStatus getLastStatus()
  {
    return lastStatus;
  }
  
  public DVNTDeviation getProfilePicture()
  {
    return profilePicture;
  }
  
  public String getProfileURL()
  {
    return profileURL;
  }
  
  public String getRealName()
  {
    return realName;
  }
  
  public DVNTUserProfile.DVNTUserProfileStats getStats()
  {
    return stats;
  }
  
  public String getTagLine()
  {
    return tagLine;
  }
  
  public DVNTUser getUser()
  {
    return user;
  }
  
  public String getWebsite()
  {
    return website;
  }
  
  public Boolean isWatchingOwner()
  {
    return isWatching;
  }
  
  public void setArtistLevel(String paramString)
  {
    artistLevel = paramString;
  }
  
  public void setArtistSpecialty(String paramString)
  {
    artistSpecialty = paramString;
  }
  
  public void setBio(String paramString)
  {
    bio = paramString;
  }
  
  public void setCountry(String paramString)
  {
    country = paramString;
  }
  
  public void setCoverPhotoURL(String paramString)
  {
    coverPhotoURL = paramString;
  }
  
  public void setIsArtist(Boolean paramBoolean)
  {
    isArtist = paramBoolean;
  }
  
  public void setIsWatching(Boolean paramBoolean)
  {
    isWatching = paramBoolean;
  }
  
  public void setLastStatus(DVNTUserStatus paramDVNTUserStatus)
  {
    lastStatus = paramDVNTUserStatus;
  }
  
  public void setProfilePicture(DVNTDeviation paramDVNTDeviation)
  {
    profilePicture = paramDVNTDeviation;
  }
  
  public void setProfileURL(String paramString)
  {
    profileURL = paramString;
  }
  
  public void setRealName(String paramString)
  {
    realName = paramString;
  }
  
  public void setStats(DVNTUserProfile.DVNTUserProfileStats paramDVNTUserProfileStats)
  {
    stats = paramDVNTUserProfileStats;
  }
  
  public void setTagLine(String paramString)
  {
    tagLine = paramString;
  }
  
  public void setUser(DVNTUser paramDVNTUser)
  {
    user = paramDVNTUser;
  }
  
  public void setWebsite(String paramString)
  {
    website = paramString;
  }
}
