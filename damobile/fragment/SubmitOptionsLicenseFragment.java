package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTPublishOptions.DVNTLicenseModificationOption;
import com.deviantart.android.damobile.activity.SubmitActivity;
import com.deviantart.android.damobile.util.submit.Submission;
import com.deviantart.android.damobile.util.submit.SubmitLicenseOptions;
import com.deviantart.android.damobile.util.submit.SubmitOptions;

public class SubmitOptionsLicenseFragment
  extends DABaseFragment
{
  @Bind({2131689828})
  RadioButton ccLicenseButton;
  @Bind({2131689830})
  SwitchCompat ccLicenseCommercial;
  @Bind({2131689829})
  LinearLayout ccLicenseSectionLayout;
  @Bind({2131689833})
  RadioButton ccModifyNoButton;
  @Bind({2131689832})
  RadioButton ccModifyShareButton;
  @Bind({2131689831})
  RadioButton ccModifyYesButton;
  @Bind({2131689827})
  RadioButton defaultLicenseButton;
  private SubmitLicenseOptions share;
  
  public SubmitOptionsLicenseFragment() {}
  
  private void onBindViewHolder()
  {
    LinearLayout localLinearLayout = ccLicenseSectionLayout;
    int i;
    if (share.equals())
    {
      i = 0;
      label17:
      localLinearLayout.setVisibility(i);
      if (!share.equals()) {
        break label111;
      }
      ccLicenseButton.setChecked(true);
    }
    for (;;)
    {
      ccLicenseCommercial.setChecked(share.a());
      switch (SubmitOptionsLicenseFragment.1.context[share.equalsIgnoreCase().ordinal()])
      {
      default: 
        return;
        i = 8;
        break label17;
        label111:
        defaultLicenseButton.setChecked(true);
      }
    }
    ccModifyNoButton.setChecked(true);
    return;
    ccModifyShareButton.setChecked(true);
    return;
    ccModifyYesButton.setChecked(true);
  }
  
  public void onCheckCCCommercial(boolean paramBoolean)
  {
    share.goTo(paramBoolean);
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    paramLayoutInflater = (ViewGroup)paramLayoutInflater.inflate(2130968686, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    ((SubmitActivity)getActivity()).getSupportActionBar().setTitle(2131231352);
    share = Submission.getInstance().getList().getString();
    onBindViewHolder();
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
  
  public void onSelectCCLicense()
  {
    share.inc(true);
    onBindViewHolder();
  }
  
  public void onSelectCCModifyNo()
  {
    share.put(DVNTPublishOptions.DVNTLicenseModificationOption.EXTERNAL);
  }
  
  public void onSelectCCModifyShare()
  {
    share.put(DVNTPublishOptions.DVNTLicenseModificationOption.SHARE);
  }
  
  public void onSelectCCModifyYes()
  {
    share.put(DVNTPublishOptions.DVNTLicenseModificationOption.UNSAVE);
  }
  
  public void onSelectDefaultLicense()
  {
    share.inc(false);
    onBindViewHolder();
  }
}
