package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.android.package_14.network.wrapper.DVNTCategoriesWrapper;
import com.google.common.base.Objects;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class DVNTSubmissionCategoryTreeRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTCategory.List>
{
  final boolean addFrequentlyUsedCategories;
  final String catPath;
  final String filetype;
  
  public DVNTSubmissionCategoryTreeRequestV1(String paramString1, String paramString2, Boolean paramBoolean)
  {
    super(com.deviantart.android.sdk.api.model.DVNTCategory.List.class);
    catPath = paramString1;
    filetype = paramString2;
    addFrequentlyUsedCategories = paramBoolean.booleanValue();
  }
  
  public boolean equals(Object paramObject)
  {
    if (this == paramObject) {
      return true;
    }
    if ((paramObject == null) || (getClass() != paramObject.getClass())) {
      return false;
    }
    if (!super.equals(paramObject)) {
      return false;
    }
    paramObject = (DVNTSubmissionCategoryTreeRequestV1)paramObject;
    return (Objects.append(Boolean.valueOf(addFrequentlyUsedCategories), Boolean.valueOf(addFrequentlyUsedCategories))) && (Objects.append(catPath, catPath)) && (Objects.append(filetype, filetype));
  }
  
  public String getCacheKey()
  {
    try
    {
      Object localObject = new StringBuilder().append("submitcategorytree");
      String str = catPath;
      localObject = ((StringBuilder)localObject).append(URLEncoder.encode(str, "UTF-8"));
      str = filetype;
      localObject = str;
      return localObject;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException) {}
    return "submitcategorytree" + catPath.replace("/", "") + filetype;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 86400000L;
  }
  
  public int hashCode()
  {
    return Objects.hashCode(new Object[] { Integer.valueOf(super.hashCode()), catPath, filetype, Boolean.valueOf(addFrequentlyUsedCategories) });
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTCategory.List sendRequest(String paramString)
  {
    return (com.deviantart.android.android.package_14.model.DVNTCategory.List)((DVNTServiceV1)getService()).getSubmissionCategoryTree(paramString, catPath, filetype, addFrequentlyUsedCategories).getCategories();
  }
}
