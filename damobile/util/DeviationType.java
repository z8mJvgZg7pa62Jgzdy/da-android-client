package com.deviantart.android.damobile.util;

public enum DeviationType
{
  static
  {
    b = new DeviationType("LITERATURE", 1);
    e = new DeviationType("JOURNAL", 2);
    d = new DeviationType("UNKNOWN", 3);
    g = new DeviationType("PLACEHOLDER", 4);
  }
}
