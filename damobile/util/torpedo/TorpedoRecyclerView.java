package com.deviantart.android.damobile.util.torpedo;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.util.AttributeSet;
import android.util.Log;
import com.deviantart.android.damobile.stream.Stream;
import com.deviantart.android.damobile.util.tracking.EventKeys.Category;
import com.deviantart.android.damobile.util.tracking.TrackerUtil.ScrollEventHelper;
import com.deviantart.android.damobile.view.EndlessScrollListener;
import com.deviantart.android.damobile.view.EndlessScrollOptions;

public class TorpedoRecyclerView
  extends RecyclerView
{
  Integer a;
  private boolean b = true;
  private EventKeys.Category bundle;
  private TorpedoRecyclerView.OnScrollCallback d;
  private int displayHeight;
  private int displayWidth;
  private int height;
  TrackerUtil.ScrollEventHelper l;
  private EndlessScrollOptions o;
  private final RecyclerView.AdapterDataObserver observer = new TorpedoRecyclerView.1(this);
  private String q;
  
  public TorpedoRecyclerView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }
  
  public TorpedoRecyclerView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }
  
  public TorpedoRecyclerView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramContext);
  }
  
  private void init(Context paramContext)
  {
    setOnScrollListener(new TorpedoRecyclerView.TorpedoScrollListener(this));
    setLayoutManager(new TorpedoLayoutManager(paramContext, 40, 1));
  }
  
  public void b(int paramInt, EndlessScrollListener paramEndlessScrollListener)
  {
    o = new EndlessScrollOptions(paramInt, paramEndlessScrollListener);
  }
  
  public boolean b()
  {
    return b;
  }
  
  public TorpedoAdapter getAdapter()
  {
    return (TorpedoAdapter)super.getAdapter();
  }
  
  public Stream getDeviationStream()
  {
    return getAdapter().get();
  }
  
  public int getDim1ScreenHeight()
  {
    return displayWidth;
  }
  
  public int getDim1Span()
  {
    return 40;
  }
  
  public int getDim2ScreenHeight()
  {
    return displayHeight;
  }
  
  public int getDim2Span()
  {
    return height;
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    Log.d("twoview", "sizes changed ow/oh => w/h | " + paramInt3 + "/" + paramInt4 + " => " + paramInt1 + "/" + paramInt2);
    float f;
    if (b)
    {
      f = paramInt2 / paramInt1;
      height = Float.valueOf(f * getDim1Span()).intValue();
      if (!b) {
        break label182;
      }
      paramInt3 = paramInt1;
      label103:
      displayWidth = paramInt3;
      if (!b) {
        break label187;
      }
    }
    TorpedoAdapter localTorpedoAdapter;
    for (;;)
    {
      displayHeight = paramInt2;
      Log.d("twoview", "dim1/dim2 : " + getDim1Span() + "/" + height);
      localTorpedoAdapter = getAdapter();
      if (localTorpedoAdapter != null) {
        break label192;
      }
      return;
      f = paramInt1 / paramInt2;
      break;
      label182:
      paramInt3 = paramInt2;
      break label103;
      label187:
      paramInt2 = paramInt1;
    }
    label192:
    localTorpedoAdapter.d();
  }
  
  public void setAdapter(RecyclerView.Adapter paramAdapter)
  {
    Object localObject = getAdapter();
    if (localObject != null) {
      ((RecyclerView.Adapter)localObject).unregisterAdapterDataObserver(observer);
    }
    super.setAdapter(paramAdapter);
    localObject = (TorpedoLayoutManager)getLayoutManager();
    ((GridLayoutManager)localObject).setSpanSizeLookup(new TorpedoRecyclerView.2(this, (TorpedoLayoutManager)localObject));
    if (paramAdapter != null)
    {
      paramAdapter.registerAdapterDataObserver(observer);
      ((TorpedoAdapter)paramAdapter).next();
    }
  }
  
  public void setCategory(EventKeys.Category paramCategory)
  {
    bundle = paramCategory;
  }
  
  public void setOrientation(int paramInt)
  {
    boolean bool = true;
    if (1 == paramInt) {}
    for (;;)
    {
      b = bool;
      ((GridLayoutManager)getLayoutManager()).setOrientation(paramInt);
      return;
      bool = false;
    }
  }
  
  public void setScrollCallback(TorpedoRecyclerView.OnScrollCallback paramOnScrollCallback)
  {
    d = paramOnScrollCallback;
  }
  
  public void setScrollEventHelper(TrackerUtil.ScrollEventHelper paramScrollEventHelper)
  {
    l = paramScrollEventHelper;
    a = l.a();
  }
  
  public void setTorpedoType(String paramString)
  {
    q = paramString;
  }
}
