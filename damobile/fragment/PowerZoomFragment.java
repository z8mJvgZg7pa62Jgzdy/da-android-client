package com.deviantart.android.damobile.fragment;

import android.app.Fragment;
import android.os.BaseBundle;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.damobile.util.ScreenFlowManager;

public class PowerZoomFragment
  extends HomeFullViewFragment
{
  @Bind({2131689775})
  LinearLayout progressBar;
  @Bind({2131689719})
  WebView webView;
  
  public PowerZoomFragment() {}
  
  public void clickClose()
  {
    ScreenFlowManager.hideKeyboard(getActivity());
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    paramBundle = getArguments().getString("power_zoom_src");
    paramLayoutInflater = (RelativeLayout)paramLayoutInflater.inflate(2130968676, paramViewGroup, false);
    ButterKnife.bind(this, paramLayoutInflater);
    webView.setBackgroundColor(0);
    webView.setVerticalScrollBarEnabled(false);
    webView.setHorizontalScrollBarEnabled(false);
    webView.getSettings().setBuiltInZoomControls(true);
    webView.getSettings().setDisplayZoomControls(false);
    webView.getSettings().setLoadWithOverviewMode(true);
    webView.getSettings().setUseWideViewPort(true);
    webView.setWebViewClient(new PowerZoomFragment.1(this));
    webView.loadDataWithBaseURL("", "<table width=100% height=100% border=\"0\"><tr><td style=\"text-align: center; vertical-align: middle;\"><img src = \"" + paramBundle + "\"/></td></tr></table>", "text/html", "utf-8", "");
    return paramLayoutInflater;
  }
  
  public void onDestroyView()
  {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
}
