package com.deviantart.android.damobile.util.opt;

import android.util.Log;

public class ItemDeleteUtils
{
  private static NoteItemDeleteHelper d = new NoteItemDeleteHelper();
  private static NotificationItemDeleteHelper o = new NotificationItemDeleteHelper();
  
  public ItemDeleteUtils() {}
  
  public static ItemDeleteHelperBase a(ItemDeleteType paramItemDeleteType)
  {
    switch (ItemDeleteUtils.2.o[paramItemDeleteType.ordinal()])
    {
    default: 
      Log.e("ItemDeleteUtil", "Unhandled Item Delete Util");
      return new ItemDeleteUtils.1();
    case 1: 
      return o;
    }
    return d;
  }
}
