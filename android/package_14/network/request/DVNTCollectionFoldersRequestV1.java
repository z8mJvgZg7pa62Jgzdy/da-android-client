package com.deviantart.android.android.package_14.network.request;

import com.deviantart.android.android.package_14.network.service.DVNTServiceV1;
import com.deviantart.android.sdk.api.network.request.GroupableRequest;
import com.google.common.reflect.TypeToken;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import java.util.HashMap;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class DVNTCollectionFoldersRequestV1
  extends com.deviantart.android.sdk.api.network.request.DVNTAsyncRequestV1<com.deviantart.android.sdk.api.model.DVNTPaginatedCollectionFolders>
  implements GroupableRequest
{
  private boolean calculateSize;
  private boolean getPreload;
  private Integer limit;
  private Integer offset;
  private String userName;
  
  public DVNTCollectionFoldersRequestV1(String paramString, boolean paramBoolean1, boolean paramBoolean2, Integer paramInteger1, Integer paramInteger2)
  {
    super(com.deviantart.android.sdk.api.model.DVNTPaginatedCollectionFolders.class);
    userName = paramString;
    calculateSize = paramBoolean1;
    getPreload = paramBoolean2;
    offset = paramInteger1;
    limit = paramInteger2;
  }
  
  public boolean equals(Object paramObject)
  {
    if (paramObject == null) {
      return false;
    }
    if (paramObject == this) {
      return true;
    }
    if (paramObject.getClass() == getClass())
    {
      DVNTCollectionFoldersRequestV1 localDVNTCollectionFoldersRequestV1 = (DVNTCollectionFoldersRequestV1)paramObject;
      return new EqualsBuilder().appendSuper(super.equals(paramObject)).append(userName, userName).append(calculateSize, calculateSize).append(getPreload, getPreload).append(offset, offset).append(limit, limit).isEquals();
    }
    return false;
  }
  
  public String getCacheKey()
  {
    return "collections" + userName + "_calcsize_" + calculateSize + "_getpreload_" + getPreload + "_offset_" + offset + "_limit_" + limit;
  }
  
  public long getCacheLifeTimeInMillis()
  {
    return 120000L;
  }
  
  public HashMap getGETArguments()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("username", userName);
    localHashMap.put("calculate_size", Boolean.valueOf(calculateSize));
    localHashMap.put("ext_preload", Boolean.valueOf(getPreload));
    localHashMap.put("offset", offset);
    localHashMap.put("limit", limit);
    return localHashMap;
  }
  
  public String getGroupEndpoint()
  {
    return "/api/v1/oauth2/collections/folders";
  }
  
  public HashMap getPOSTArguments()
  {
    return null;
  }
  
  public TypeToken getWrapperTypeToken()
  {
    return new DVNTCollectionFoldersRequestV1.1(this);
  }
  
  public int hashCode()
  {
    return new HashCodeBuilder().appendSuper(super.hashCode()).append(userName).append(calculateSize).append(getPreload).append(offset).append(limit).toHashCode();
  }
  
  public boolean needsGraduation()
  {
    return false;
  }
  
  protected com.deviantart.android.android.package_14.model.DVNTPaginatedCollectionFolders sendRequest(String paramString)
  {
    return ((DVNTServiceV1)getService()).collectionFolders(paramString, userName, calculateSize, getPreload, offset, limit);
  }
}
