package com.deviantart.android.android.package_14.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DVNTChallenge
  implements Serializable
{
  @SerializedName("credit_deviation")
  String creditDeviation;
  @SerializedName("completed")
  Boolean isCompleted;
  @SerializedName("locked")
  Boolean isLocked;
  @SerializedName("level_label")
  String levelLabel;
  @SerializedName("levels")
  List<String> levels;
  @SerializedName("media")
  DVNTChallengeMedia.List media;
  @SerializedName("tags")
  List<String> tags;
  @SerializedName("time_limit")
  Integer timeLimit;
  @SerializedName("type")
  List<String> types;
  
  public DVNTChallenge() {}
  
  private DVNTImageChallengeMedia getMediaForRatio(String paramString)
  {
    Iterator localIterator = getMedia().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (DVNTChallengeMedia)localIterator.next();
      if ((localObject != null) && ((localObject instanceof DVNTImageChallengeMedia)))
      {
        localObject = (DVNTImageChallengeMedia)localObject;
        if ((((DVNTImageChallengeMedia)localObject).getRatio() != null) && (((DVNTImageChallengeMedia)localObject).getRatio().equalsIgnoreCase(paramString))) {
          return localObject;
        }
      }
    }
    return null;
  }
  
  private DVNTVideoChallengeMedia getVideoForQuality(DVNTChallengeMediaType paramDVNTChallengeMediaType)
  {
    Iterator localIterator = getMedia().iterator();
    while (localIterator.hasNext())
    {
      Object localObject = (DVNTChallengeMedia)localIterator.next();
      if ((localObject != null) && ((localObject instanceof DVNTVideoChallengeMedia)))
      {
        localObject = (DVNTVideoChallengeMedia)localObject;
        if ((paramDVNTChallengeMediaType != null) && (paramDVNTChallengeMediaType.equals(((DVNTChallengeMedia)localObject).getType()))) {
          return localObject;
        }
      }
    }
    return null;
  }
  
  public String getCreditDeviation()
  {
    return creditDeviation;
  }
  
  public DVNTVideoChallengeMedia getDefaultVideo()
  {
    return getVideoForQuality(DVNTChallengeMediaType.VIDEO);
  }
  
  public Boolean getIsCompleted()
  {
    return isCompleted;
  }
  
  public Boolean getIsLocked()
  {
    return isLocked;
  }
  
  public String getLevelLabel()
  {
    return levelLabel;
  }
  
  public List getLevels()
  {
    return levels;
  }
  
  public DVNTChallengeMedia.List getMedia()
  {
    return media;
  }
  
  public DVNTImageChallengeMedia getMediaRatioA()
  {
    return getMediaForRatio("a");
  }
  
  public DVNTImageChallengeMedia getMediaRatioB()
  {
    return getMediaForRatio("b");
  }
  
  public DVNTImageChallengeMedia getMediaRatioC()
  {
    return getMediaForRatio("c");
  }
  
  public List getTags()
  {
    return tags;
  }
  
  public DVNTTemplateChallengeMedia getTemplate()
  {
    Iterator localIterator = getMedia().iterator();
    while (localIterator.hasNext())
    {
      DVNTChallengeMedia localDVNTChallengeMedia = (DVNTChallengeMedia)localIterator.next();
      if ((localDVNTChallengeMedia != null) && (localDVNTChallengeMedia.getClass().equals(com.deviantart.android.sdk.api.model.DVNTTemplateChallengeMedia.class))) {
        return (DVNTTemplateChallengeMedia)localDVNTChallengeMedia;
      }
    }
    return null;
  }
  
  public Integer getTimeLimit()
  {
    return timeLimit;
  }
  
  public List getTypes()
  {
    return types;
  }
  
  public DVNTVideoChallengeMedia getVideo1080p()
  {
    return getVideoForQuality(DVNTChallengeMediaType.VIDEO_1080P);
  }
  
  public DVNTVideoChallengeMedia getVideo360p()
  {
    return getVideoForQuality(DVNTChallengeMediaType.VIDEO_360P);
  }
  
  public DVNTVideoChallengeMedia getVideo720p()
  {
    return getVideoForQuality(DVNTChallengeMediaType.VIDEO_720P);
  }
  
  public void setCreditDeviation(String paramString)
  {
    creditDeviation = paramString;
  }
  
  public void setIsCompleted(Boolean paramBoolean)
  {
    isCompleted = paramBoolean;
  }
  
  public void setIsLocked(Boolean paramBoolean)
  {
    isLocked = paramBoolean;
  }
  
  public void setLevelLabel(String paramString)
  {
    levelLabel = paramString;
  }
  
  public void setLevels(List paramList)
  {
    levels = paramList;
  }
  
  public void setMedia(DVNTChallengeMedia.List paramList)
  {
    media = paramList;
  }
  
  public void setTags(List paramList)
  {
    tags = paramList;
  }
  
  public void setTimeLimit(Integer paramInteger)
  {
    timeLimit = paramInteger;
  }
  
  public void setTypes(List paramList)
  {
    types = paramList;
  }
}
