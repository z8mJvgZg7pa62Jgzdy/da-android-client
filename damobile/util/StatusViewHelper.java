package com.deviantart.android.damobile.util;

import android.app.Activity;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTRepostItem;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.package_14.model.DVNTUserStatus;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.ArrayList;

public class StatusViewHelper
{
  public StatusViewHelper() {}
  
  public static View.OnClickListener add(Activity paramActivity, DVNTUserStatus paramDVNTUserStatus)
  {
    if (!paramDVNTUserStatus.isShare().booleanValue()) {
      return new StatusViewHelper.3(paramActivity, paramDVNTUserStatus);
    }
    paramDVNTUserStatus = paramDVNTUserStatus.getRepostItems();
    if ((paramDVNTUserStatus == null) || (paramDVNTUserStatus.isEmpty())) {
      return null;
    }
    paramDVNTUserStatus = (DVNTRepostItem)paramDVNTUserStatus.get(0);
    Object localObject = paramDVNTUserStatus.getType();
    if (((String)localObject).equals("deviation"))
    {
      localObject = paramDVNTUserStatus.getDeviation();
      if ((localObject == null) || (((DVNTAbstractDeviation)localObject).isDeleted().booleanValue())) {
        return null;
      }
      return new StatusViewHelper.4(paramActivity, paramDVNTUserStatus);
    }
    if (((String)localObject).equals("status"))
    {
      paramDVNTUserStatus = paramDVNTUserStatus.getStatus();
      if ((paramDVNTUserStatus == null) || (paramDVNTUserStatus.isDeleted().booleanValue())) {
        return null;
      }
      return new StatusViewHelper.5(paramActivity, paramDVNTUserStatus);
    }
    return null;
  }
  
  public static void create(Activity paramActivity, DVNTUser paramDVNTUser, SimpleDraweeView paramSimpleDraweeView, TextView paramTextView, boolean paramBoolean)
  {
    if (paramDVNTUser == null)
    {
      paramSimpleDraweeView.setVisibility(8);
      paramTextView.setVisibility(8);
      return;
    }
    paramSimpleDraweeView.setVisibility(0);
    paramTextView.setVisibility(0);
    ImageUtils.isEmpty(paramSimpleDraweeView, Uri.parse(paramDVNTUser.getUserIconURL()));
    paramTextView.setText(UserDisplay.add(paramActivity, paramDVNTUser));
    if (MemberType.toString(paramDVNTUser.getType()) != null)
    {
      paramSimpleDraweeView.setOnClickListener(new StatusViewHelper.1(paramBoolean, paramActivity, paramDVNTUser.getUserName()));
      paramTextView.setOnClickListener(new StatusViewHelper.2(paramSimpleDraweeView));
    }
  }
  
  public static void setText(Activity paramActivity, DVNTUser paramDVNTUser, SimpleDraweeView paramSimpleDraweeView, TextView paramTextView)
  {
    create(paramActivity, paramDVNTUser, paramSimpleDraweeView, paramTextView, true);
  }
}
