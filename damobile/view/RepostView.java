package com.deviantart.android.damobile.view;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.deviantart.android.android.package_14.model.DVNTAbstractDeviation;
import com.deviantart.android.android.package_14.model.DVNTDeviation;
import com.deviantart.android.android.package_14.model.DVNTImage;
import com.deviantart.android.android.package_14.model.DVNTUser;
import com.deviantart.android.android.package_14.model.DVNTUserStatus;
import com.deviantart.android.damobile.util.DeviationUtils;
import com.deviantart.android.damobile.util.ImageUtils;
import com.deviantart.android.damobile.util.OnCloseListener;
import com.deviantart.android.damobile.util.UserDisplay;
import com.deviantart.android.damobile.util.markup.MarkupHelper;
import com.deviantart.android.damobile.util.markup.SimpleDAMLContent;
import com.deviantart.android.damobile.util.markup.SimpleDAMLContent.Builder;
import com.deviantart.android.damobile.util.markup.SimpleDAMLHelper;
import com.facebook.drawee.view.GenericDraweeView;
import com.facebook.drawee.view.SimpleDraweeView;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class RepostView
  extends LinearLayout
{
  @Bind({2131690052})
  SimpleDraweeView authorIcon;
  @Bind({2131690053})
  TextView authorName;
  @Bind({2131690054})
  ImageButton closeButton;
  @Bind({2131690056})
  SimpleDraweeView repostedImage;
  @Bind({2131690057})
  TextView repostedImageUnavailable;
  @Bind({2131690055})
  TextView repostedText;
  @Bind({2131690051})
  TextView title;
  
  public RepostView(Context paramContext)
  {
    super(paramContext);
    ((LayoutInflater)getContext().getSystemService("layout_inflater")).inflate(2130968776, this, true);
    ButterKnife.bind(this);
  }
  
  private void bind(DVNTUser paramDVNTUser)
  {
    if ((paramDVNTUser == null) || (paramDVNTUser.getUserName() == null))
    {
      authorIcon.setVisibility(8);
      authorName.setVisibility(8);
      return;
    }
    authorName.setVisibility(0);
    authorName.setText(UserDisplay.add(getContext(), paramDVNTUser));
    if (paramDVNTUser.getUserIconURL() == null)
    {
      authorIcon.setVisibility(8);
      return;
    }
    authorIcon.setVisibility(0);
    ImageUtils.isEmpty(authorIcon, Uri.parse(paramDVNTUser.getUserIconURL()));
  }
  
  public void b(DVNTUserStatus paramDVNTUserStatus)
  {
    bind(paramDVNTUserStatus.getAuthor());
    title.setVisibility(8);
    paramDVNTUserStatus = new SimpleDAMLContent.Builder().a(paramDVNTUserStatus.getBody()).b();
    String str = paramDVNTUserStatus.c();
    MarkupHelper.onPostExecute((Activity)getContext(), str, repostedText, true);
    SimpleDAMLHelper.clear((Activity)getContext(), paramDVNTUserStatus, repostedImage, repostedImageUnavailable);
  }
  
  public void b(OnCloseListener paramOnCloseListener)
  {
    if (paramOnCloseListener == null)
    {
      closeButton.setVisibility(8);
      closeButton.setOnClickListener(null);
      return;
    }
    closeButton.setVisibility(0);
    closeButton.setOnClickListener(new RepostView.1(this, paramOnCloseListener));
  }
  
  public void onCreateView(DVNTDeviation paramDVNTDeviation)
  {
    bind(paramDVNTDeviation.getAuthor());
    title.setVisibility(0);
    title.setText(paramDVNTDeviation.getTitle());
    DVNTImage localDVNTImage = DeviationUtils.b(paramDVNTDeviation);
    if (localDVNTImage == null) {
      repostedImage.setVisibility(8);
    }
    for (;;)
    {
      paramDVNTDeviation = paramDVNTDeviation.getExcerpt();
      if (paramDVNTDeviation != null) {
        break;
      }
      repostedText.setVisibility(8);
      return;
      repostedImage.setVisibility(0);
      float f = localDVNTImage.getWidth() / localDVNTImage.getHeight();
      repostedImage.setAspectRatio(f);
      ImageUtils.isEmpty(repostedImage, Uri.parse(localDVNTImage.getSrc()));
    }
    repostedText.setText(Jsoup.get(paramDVNTDeviation).get());
  }
}
